# DCC COMMAND STATION

## AUTORE

Paolo Calcagni

E-mail: paolo.calcagni@studenti.unimi.it

## DESCRIZIONE

DCC Command Station permette di comandare le locomotive dotate di un decoder digitale che abbiano adottato lo standard DCC definito da NMRA.

### HARDWARE UTILIZZATO

- Arduino Mega 1: dedicato alla gestione del protocollo DCC.
Riceve le istruzioni fornite dall'utente (tramite tastierino numerico o tramite sito web), compone ed invia i pacchetti corrispondenti sul tracciato (facendo uso di un segnale bipolare per l'invio dei bit).

- Arduino Mega 2: è connesso ai relay che controllano i motori degli scambi.
Quando riceve da Arduino Mega 1 l'istruzione di azionare il motore di uno scambio, eccita il relativo relay.

- ESP8266: gestisce un sito web attraverso il quale l'utente può modificare lo stato delle locomotive e degli scambi tramite architettura REST.

- Tastierino Numerico: permette all'utente di modificare lo stato delle locomotive e degli scambi.

- Display LCD: se il sito web è attivo mostra l'indirizzo IP a cui collegarsi; in caso contrario mostra un menù navigabile tramite tastierino numerico. 

- L298N: componente hardware necessario per alimentare il tracciato ed allo stesso tempo inviare le informazioni alle locomotive.
Nello specifico trasforma il segnale logico di Arduino 0-5 V ad un segnale bipolare DCC 12 V.

### LIBRERIE SOFTWARE UTILIZZATE

- Wire: permette la gestione del display LCD.

- Liquid Crystal I2C: permette la gestione del display LCD.

- EEPROM: permette la gestione memoria non-volatile EEPROM.

- Timer One: permette la gestione degli interrupt.

- Keypad: Libreria permette la gestione del tastierino numerico.

- Firmata Marshaller: permette la gestione della communicazione tra microcontroller.

## LINK REPOSITORY

[Repository](https://bitbucket.org/paolino625/dcc-command-station/src/master/)

## LICENZA SCELTA

GPLv3

## DATA DI PRESENTAZIONE

Dicembre 2019