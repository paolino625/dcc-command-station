# Configurazione Arduino Giga in Platformio

Fin tanto che Arduino Giga non viene supportato nativamente bisogna seguire questa [guida](https://github.com/platformio/platform-ststm32/issues/702#issuecomment-1871959957)

Nello specifico copiare unire la cartella "ststm32" presente qui in quella dentro .platformio/platforms presente nella cartella dell'utente.

# Arduino Giga senza bootloader

Arduino Giga arrivato senza Bootloader: https://forum.arduino.cc/t/giga-not-being-detected-on-windows-10/1167638/6

Guida da seguire per flashare bootloader: https://support.arduino.cc/hc/en-us/articles/7991505977116-Burn-the-bootloader-on-GIGA-R1-WiFi

# Risolvere Errore libusb not found

Installare st-link-server.pkg che è possibile trovare in stm32Cubelde.dmg

https://www.st.com/en/development-tools/stm32cubeide.html#st-get-software

Fonti: https://github.com/platformio/platform-ststm32/issues/595
https://community.platformio.org/t/tool-dfuutil-for-macos-m1-arm-64/23377