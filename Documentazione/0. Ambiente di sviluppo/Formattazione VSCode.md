- Aprire settings.json (impostazioni utente): aprire Palette VSCode e digitare ">settings.json".
Selezionare opzione "Apri impostazioni Utente JSON".
- Aggiungere le seguenti righe al file:
"editor.formatOnSave": true,
"C_Cpp.clang_format_fallbackStyle": "{BasedOnStyle: Google, IndentWidth: 4 }"