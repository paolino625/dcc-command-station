| Nome | Paese | Link |  Commento  |
| ----| ----| ---- | --|
| Treni storici | Italia | https://www.fondazionefs.it/content/fondazionefs/it/treni-storici.html?region=abruzzo&train-type=&month=december | |
| Verbano Express | Italia | https://www.verbanoexpress.com | |
| Bernina Express | Svizzera | https://tickets.rhb.ch/it/pages/bernina-express | Consigliato regionale e non Bernina Express. Costa di meno. Si può scendere e risalire quando si vuole. Si possono aprire i finestrini.
| Glacier Express | Svizzera | https://tickets.rhb.ch/it/pages/glacier-express | |