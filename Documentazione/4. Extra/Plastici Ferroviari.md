| Plastico | Città | Link |  Commento  |
| ----| ----| ---- | --|
| Miniature Wunderland | Amburgo | https://www.miniatur-wunderland.com | |
| HZERO | Firenze|  https://www.hzero.com| |
| Galleria baugate | Mendrisio |  https://www.galleriabaumgartner.ch/it/| |
| Museo del treno | Montesilvano |  https://www.acaf-montesilvano.com |  |
| Museo Nazionale Ferroviario Pietrarsa | Napoli   | https://www.fondazionefs.it/content/fondazionefs/it/esplora-il-museo/visita-pietrarsa.html| |
| Dopolavoro Ferroviario Pescara | Pescara |  https://www.dlfpescara.it | |
| Museo Nazionale dei Trasporti sede Taggia | Imperia | https://www.marklinfan.com/f/topic.asp?TOPIC_ID=4295#:~:text=Il%20plastico%20è%20allestito%20presso,Ingresso%20libero.&text=Taggia%20si%20trova%20a%209,Sanremo%2C%2025%20minuti%20da%20Ventimiglia. | |
| Gruppo Ferromodellistico Mestrino | Mestre | https://www.gruppofermodellisticomestrino.it | |
| MondoTreno | Bolzano | https://kidpass.it/treni-trenini-musei-modellini-italia/| |
| Le rotaie di Isernia | Isernia | http://www.lerotaie.com |  |
| Museo Ferroviario della Puglia | Lecce | https://www.museoferroviariodellapuglia.it | |
| Il Mondo dei Treni in Miniatura | Vicenza | https://kidpass.it/treni-trenini-musei-modellini-italia/ |
| Il Museo Ferroviario Piemontese | Cuneo | http://www.museoferroviariopiemontese.it |
| Il Mondo dei Treni in Miniatura a Schio | Schio (Veneto) | https://www.its4kids.it/consigli/musei-ferroviari-musei-modellismo/167.aspx |
| Museo di Campo Marzio a Trieste | Trieste  |  https://triestecampomarzio.com/campo-marzio/museo/|


# Fonti

- https://kidpass.it/treni-trenini-musei-modellini-italia/
- https://www.its4kids.it/consigli/musei-ferroviari-musei-modellismo/167.aspx