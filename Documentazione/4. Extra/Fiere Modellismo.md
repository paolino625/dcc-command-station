| Fiera | Città | Link |  Commento  |
| ----| ----| ---- | --|
| Hobby Model Expo | Milano  | https://parcoesposizioninovegro.it/fiere/hobby-model-expo/| Settembre circa|
| Verona Model Expo |Verona |https://www.modelexpoitaly.it | Marzo |
| Internationale Modellbahn Ausstellung | Friedrichshafen | https://www.ima-friedrichshafen.de/en/ | Novembre |
| Savoie Modelisme | Chambery | https://www.savoie-expo.fr/ | Ottobre |