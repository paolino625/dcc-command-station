- [Introduzione](#introduzione)
- [Tipologie di sezioni di blocco](#tipologie-di-sezioni-di-blocco)
  - [Sezione di blocco fissa](#sezione-di-blocco-fissa)
  - [Sezione di blocco dinamica](#sezione-di-blocco-dinamica)
- [Sistemi di distanziamento](#sistemi-di-distanziamento)
  - [Blocco telefonico](#blocco-telefonico)
  - [Blocco elettrico](#blocco-elettrico)
    - [Blocco Elettrico Manuale tipo F.S.](#blocco-elettrico-manuale-tipo-fs)
    - [Blocco Elettrico Automatico (BA)](#blocco-elettrico-automatico-ba)
    - [Blocco Elettrico Conta-Assi](#blocco-elettrico-conta-assi)
  - [Blocco radio](#blocco-radio)
- [Forme di interlocking](#forme-di-interlocking)
  - [Blocco elettrico](#blocco-elettrico-1)
  - [Blocco di sezione](#blocco-di-sezione)
  - [Blocco dell'itinerario](#blocco-dellitinerario)
  - [Blocco sezionale dell'itinerario](#blocco-sezionale-dellitinerario)
  - [Blocco in avvicinamento](#blocco-in-avvicinamento)
  - [Blocco a bastone](#blocco-a-bastone)
  - [Blocco delle indicazioni](#blocco-delle-indicazioni)
  - [Blocco di controllo o blocco del traffico](#blocco-di-controllo-o-blocco-del-traffico)
- [Fonti](#fonti)

# Introduzione

Un treno, per poter circolare in linea, deve essere sicuro che la stessa sia libera e che non si trovi, all'improvviso, un treno davanti o incontro. Questo perché, al contrario degli autoveicoli, i treni non viaggiano normalmente "a vista". Un autoveicolo, mantenendo una adeguata "distanza di sicurezza", riesce a fermarsi in uno spazio relativamente breve; un treno merci di 1.600 tonnellate che viaggia a 100 km/h, anche tirando la "rapida", ha uno spazio di frenatura misurabile in centinaia di metri. Potete capire quindi che i treni vanno opportunamente distanziati fra di loro. Questo si ottiene con i segnali.

Nelle linee a scarso traffico il distanziamento avviene normalmente fra una stazione e la successiva, nelle linee con medio ed alto traffico esistono anche dei segnali intermedi di distanziamento fra una stazione e l'altra. 

Questi segnali sono detti segnali di blocco e la tratta fra un segnale di blocco ed il successivo è detta sezione di blocco. Anche i segnali di partenza delle stazioni sono segnali di blocco. Nelle stazioni con "segnalamento plurimo" tale funzione è associata al segnale di partenza esterno.

Le stazioni formano una specie di "isola" fra le sezioni di blocco (eccetto le stazioni disabilitate nelle linee con blocco elettrico manuale e nelle stazioni impresenziate con blocco elettrico manuale o con blocco conta-assi): i confini di quest'isola sono segnalati dai segnali di protezione.

Pertanto quando si dice che "la sezione di blocco è libera" si intende che è libero il tratto fra il termine della stazione precedente (subito dopo l'ultimo circuito di binario di stazione) e l'inizio della stazione successiva (dopo il segnale di protezione unico o esterno). Non si comprende quindi il tratto fra il segnale di partenza e l'ultimo c.d.b. di stazione, tratto che si intende controllato (anche riguardo alla sua libertà da veicoli) dall'apparato di sicurezza di stazione e non dal sistema di distanziamento in linea (blocco).

# Tipologie di sezioni di blocco

Nelle ferrovie moderne ci sono due concetti di blocco, blocco fisso.

## Sezione di blocco fissa

Un segnale indica se un treno può entrare o meno una determinata sezione. 
La linea ferroviaria è divisa in sezioni, non più corte della distanza che ci vuole per far frenare il convoglio più veloce.

![Immagine](/Documentazione/Immagini/BloccoSezioni1.png)

Ci sono due modi per rilevare l'occupazione di un blocco:
- Ogni sezione è isolata elettricamente.
Una corrente viene fornita su entrambe le rotaie.
Quando un treno passa c'è un cortocircuito.
Questo serve anche per rilevare difetti come binario interrotto.
- Conteggio degli assi: conto del numero degli assi che entrano ed escono dal blocco. Se il numero degli assi in entrata è uguale a quello in uscita è ok.

![Immagine](/Documentazione/Immagini/BloccoSezioni2.png)

## Sezione di blocco dinamica

Un computer calcola una safe zone tra ogni convoglio circolante.

![Immagine](/Documentazione/Immagini/BloccoSezioni3.png)

Il sistema conosce la precisa location e velocità di ogni treno che è calcolata dalla combinazione di numerosi sensori: marker attivi e passivi sul tracciato e tachimetro sul treno.

![Immagine](/Documentazione/Immagini/BloccoSezioni4.png)

Con questo setupEncoder i segnali non sono più necessari e le istruzioni vengono direttamente mandate ai treni. Questo ha il vantaggio di incrementare la capacità della traccia fino al massimo limite teorico, consentendo a due treni di viaggiare molto vicini.

![Immagine](/Documentazione/Immagini/BloccoSezioni5.png)

# Sistemi di distanziamento

Il distanziamento in sicurezza dei treni in linea viene assicurato mediante i seguenti sistemi di distanziamento o "regimi".

I sistemi di distanziamento principalmente usati sono quelli del blocco elettrico e del blocco radio.

Le linee gestite con il blocco elettrico o con il blocco radio sono suddivise in tratti, denominati sezioni di blocco, delimitati da segnali. I segnali gestiscono la circolazione in modo che in ciascuna sezione non ci sia che un solo treno per volta.

## Blocco telefonico

È utilizzato solo in caso di guasto su linee gestiste con il blocco elettrico.

Il distanziamento dei treni avviene mediante dispacci telefonici registrati di "via libera" scambiati esclusivamente da agenti con funzioni di Dirigente Movimento, pertanto i treni vengono distanziati solo fra stazioni abilitate.

Per questo motivo, l'attivazione del blocco telefonico in sostituzione di quello elettrico comporta ritardi nella circolazione in quanto la tratta diventa un'"unica sezione".

## Blocco elettrico

Sulle linee con blocco elettrico i suddetti segnali sono di tipo luminoso (segnalamento laterale).

### Blocco Elettrico Manuale tipo F.S.

Il sistema di blocco elettrico "meno tecnologico" e ormai considerato desueto è il Blocco Elettrico Manuale tipo F.S. (B.E.M.) che, in quanto "manuale", richiede l'intervento del personale ferroviario per l'utilizzo dello stesso e pertanto permette il distanziamento solo fra stazioni. 

Il blocco elettrico manuale tipo F.S. è composto da due apparecchiature poste nelle due stazioni limitrofe (presenziate da Dirigente Movimento o meno) che distanziano i treni. Queste apparecchiature, dette "Istrumenti di blocco", sono collegate fra di loro tramite un cavo elettrico. Ogni apparecchiatura è collegata inoltre con i segnali di partenza (di blocco) della stazione ed è influenzata da alcuni "enti" di stazione che vedremo di seguito.

In alcune linee, quando la distanza fra due stazioni limitrofe è notevole, esistono ancora anche dei Posti di Blocco Intermedi in piena linea muniti di segnali di blocco e di apparecchiature di blocco che vengono manovrate da appositi agenti detti "Guardablocco". 

Questa tipologia di blocco è in disuso ed è in corso di sostituzione dal blocco conta-assi o quello automatico.

### Blocco Elettrico Automatico (BA)

Uno dei sistemi in uso in RFI è il Blocco Elettrico Automatico (BA). Questo tipo di distanziamento, pur se con tecnologia dell'epoca, esisteva già negli anni 30 del secolo scorso. 

Questo sistema viene usato nelle linee principali (anche nella linea Direttissima Firenze - Roma) e permette il distanziamento di più treni fra una stazione e l'altra mediante segnali di blocco intermedi a funzionamento, appunto, automatico. 

Tale sistema di blocco permette anche, in alcune linee, la ripetizione continua in cabina di guida dell'aspetto del segnale successivo (BAcc). In tal modo il macchinista riesce a sapere l'aspetto del segnale successivo (anche se è distante diversi chilometri) senza ancora "vederlo" fisicamente. Tale ripetizione si rende necessaria per i treni che viaggiano a velocità superiore di 150 km/h.

La ripetizione dei segnali continua può essere integrata anche con un sistema di ripetizione discontinuo detto SCMT (sistema di controllo della marcia del treno) che trasmette in macchina anche altre indicazioni.

Il Blocco Automatico permette anche la "banalizzazione" delle linee a doppio binario: ovvero la possibilità di far circolare i treni sul binario di destra con la sicurezza della protezione dei segnali. 

### Blocco Elettrico Conta-Assi

Per ovviare ai costi di installazione e manutenzione del Blocco Automatico sulle linee con traffico non elevato, è stato progettato il Blocco Elettrico Conta-assi (B.ca). Anche questo sistema è "automatico" ma utilizza dei pedali di elettromagnetici che contano il numero di assi dei veicoli che si inoltra o che proviene dalla linea. 

Se il conteggio degli assi "entrati" in linea da una stazione è uguale a quello degli assi "usciti" dalla linea (e che sono entrati nella stazione successiva) il blocco indica che la sezione è libera, altrimenti la sezione resta occupata. Questo sistema permette anche l'utilizzo di segnali intermedi di blocco impresenziati a funzionamento automatico.

I due pedali sono "bidirezionali" ed effettuano un conteggio di tipo "algebrico", cioè lo stesso pedale conta "positivamente" in un senso e "negativamente" nell'altro in modo che una manovra od un mezzo d'opera che impegna il pedale e poi rientra nella stessa stazione, provoca la liberazione del blocco.

Per tale sistema sono in attivazione anche tratte banalizzate su linea a doppio binario e sistemi di ripetizione discontinua in macchina dello stato del percorso (S.C.M.T.).

## Blocco radio

Sulle linee ad Alta Velocità / Alta Capacità (AV/AC) viene istallato un particolare sistema di blocco detto "Blocco Radio" (BRA) costituito da circuiti di binario di linea e autorizzazioni al movimento dei treni trasmesse da una centrale radio (RBC) ai treni mediante sistema GSM-R. Tali linee sono prive di segnali convenzionali per i treni.

Il Blocco Radio (BRA) è un nuovo sistema di distanziamento "dinamico". Sul terreno esistono delle sezioni delimitate da Segnali Imperativi costituiti da tavole speciali ad alta rifrangenza applicate su propri pali o sui pali della linea TE (linea elettrica).

![Immagine](/Documentazione/Immagini/BloccoRadio.png)

Queste sezioni sono costituite da circuiti di binario. Il movimento dei treni può avvenire fra una o più sezioni limitrofe. A seconda delle sezioni disponibili per il movimento, il segnale di termine della sezione diventa il punto di arresto imperativo per il treno.

Le informazioni sulla libertà della sezione, sullo spazio disponibile e sulla velocità da mantenere vengono trasmessi a bordo via radio utilizzando il sistema GSM-R da parte di un posto centrale denominato RBC (Radio Block Center).

Il treno pertanto è autorizzato a muoversi in base a dei messaggi (elettronici) di Autorizzazione al Movimento (MA).

Queste MA possono essere:
- MA in Supervisione Completa
- MA con marcia a vista
- MA con Apposita Prescrizione

Nel primo caso il movimento del treno avviene sotto la completa supervisione del BRA e le indicazioni di marcia sono date dal cruscotto della cabina di guida.

Nel secondo caso il movimento del treno può avvenire con marcia a vista, non superando la velocità di 30 km/h, fino al successivo segnale imperativo.

Nel terzo caso il movimento del treno può avvenire, a seconda delle situazioni e delle prescrizioni ricevute:

- Con marcia a vista (m.a.v.), non superando la velocità di 30 km/h, fino al successivo segnale imperativo;
- Alla velocità massima di 60 km/h fino al successivo segnale imperativo di protezione o fino al successivo segnale di confine senza tener conto dei segnali imperativi di fine sezione incontrati.

In caso di mancanza del segnale radio mentre il treno viaggia nelle modalità "Supervisione Completa" o "Marcia a Vista", il treno viene arrestato d'urgenza (salvo ripristino del segnale). In questo caso il DCO, una volta venuto a conoscenza del guasto dal macchinista, ordinerà al treno di proseguire con m.a.v. e 30 km/h fino al successivo segnale imperativo di fine sezione o di protezione di PdS dove dovrà comunque arrestarsi.

Le linee con BRA sono banalizzate ed ammettono la marcia parallela. Il senso di marcia del BRA viene stabilito negli apparati ACC di PdS e trasmesso al RBC.

# Forme di interlocking

## Blocco elettrico

"La combinazione di uno o più lock elettrici o circuiti di controllo per mezzo dei quali le leve di una macchina di interblocco, o gli interruttori o altri dispositivi azionati in relazione alla segnalazione e all'interblocco, sono assicurati contro l'azionamento in determinate condizioni

## Blocco di sezione

Blocco elettrico efficace mentre un treno occupa una determinata sezione di un percorso e adatto per impedire la manipolazione di leve che metterebbero in pericolo il treno mentre si trova all'interno di tale sezione.

## Blocco dell'itinerario

Blocco elettrico che entra in vigore quando un treno supera un segnale e che è adatto per impedire la manipolazione di leve che metterebbero in pericolo il treno mentre si trova entro i limiti dell'itinerario inserito.

## Blocco sezionale dell'itinerario

Blocco dell'itinerario disposto in modo tale che un treno, nel liberare ogni sezione dell'itinerario, rilasci il blocco relativo a quella sezione.

## Blocco in avvicinamento

Blocco elettrico efficace mentre un treno si avvicina a un segnale che è stato impostato per farlo procedere e adatto a impedire la manipolazione di leve o dispositivi che metterebbero in pericolo il treno stesso.

## Blocco a bastone

Blocco elettrico che entra in vigore all'impostazione di un segnale di marcia di un treno, rilasciato da un treno in transito, e adatto a impedire la manipolazione di leve che metterebbero in pericolo un treno in arrivo.

## Blocco delle indicazioni

Blocco elettrico adatto per impedire qualsiasi manipolazione delle leve che porterebbe a una condizione di non sicurezza nel caso in cui un segnale, un interruttore o un altro dispositivo azionato non compia un movimento corrispondente a quello della leva di azionamento; o adatto direttamente per impedire l'azionamento di un dispositivo nel caso in cui un altro dispositivo, da azionare per primo, non compia il movimento richiesto.

## Blocco di controllo o blocco del traffico

Blocco elettrico che impone la cooperazione tra gli operatori di due impianti adiacenti in modo tale da impedire che segnali opposti che governano lo stesso binario siano impostati per procedere contemporaneamente. Inoltre, dopo che un segnale è stato abilitato e accettato da un treno, il blocco del controllo impedisce l'abilitazione di un segnale opposto nell'impianto di interconnessione adiacente fino a quando il treno non ha attraversato l'impianto stesso.

# Fonti

- [Interlocking](https://en.wikipedia.org/wiki/Interlocking)
- [I sistemi di distanziamento](https://www.segnalifs.it/sfi/it/bl/N_bl_intro.htm)
- [Il blocco elettrico manuale tipo F.S](https://www.segnalifs.it/sfi/it/bl/N_bem.htm)
- [Il blocco conta-assi](https://www.segnalifs.it/sfi/it/bl/N_bca.htm)
- [I dispositivi delle linee banalizzate con BA](https://www.segnalifs.it/sfi/it/bl/N_baban2.htm)
- [I sistemi di distanziamento](https://www.segnalifs.it/sfi/it/bl/N_bl_intro.htm)