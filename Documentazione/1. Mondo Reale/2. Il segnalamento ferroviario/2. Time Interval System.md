Fin da quando è stata inventata la ferrovia, si è reso necessario trovare un modo per organizzare più di un treno sulla stessa rete ferroviaria.

Per questo motivo le reti ferroviarie venivano divise in sezioni, conosciuti come blocchi. 

Originariamente, le persone erano impiegate a rimanere a fianco sul tracciato a certa distanza, formando una specie di blocchi umani.
Dotati di un orologio e utilizzando bandiere per informare il macchinista che il treno precedente è passato prima di un certo ammontare di tempo. 

![Immagine](/Documentazione/Immagini/TimeIntervalSystem1.png)

Bandiera rossa veniva mostrata quando il treno precedente era passato meno di 5 minuti.
Bandiera gialla tra 5 e 10 minuti.
Bandiera verde se più di 10 minuti erano passati.

Questo sistema viene chiamato Time Interval System.
Crea problemi molto seri.

- Le persone non avevano possibilità di sapere se il treno precedente aveva libera o meno la sezione.

Nel caso in cui il treno precedente si sia stoppato per qualsiasi ragione, il macchinista del treno in movimento non avrebbe avuto tale informazione fino a quando sarebbe diventato visibile dalla loro posizione. Di conseguenza, incidenti erano comuni perché macchinisti immaginavano di avere 10 minuti di scarto quando in realtà no.
Ricordiamo che i treni richiedono tanto tempo per poter frenare.

![Immagine](/Documentazione/Immagini/TimeIntervalSystem2.png)

- Un altro problema è bassa capacità: l'intervallo di 10 minuti restringeva in maniera significativa il numero di treni che potevano circolare su una determinata rete ferroviaria.
Gli ingegneri hanno iniziato a ridurre il tempo tra i due treni ma come ci si aspettava, il numero dei treni per ora è incrementato, ma allo stesso tempo anche il numero di incidenti.