- [Segnali di testa](#segnali-di-testa)
- [Segnali di coda](#segnali-di-coda)
- [Tabella speciale per treni a composizione bloccata](#tabella-speciale-per-treni-a-composizione-bloccata)
- [Circolazione sul binario illegale](#circolazione-sul-binario-illegale)
- [Fonte](#fonte)

# Segnali di testa

Normalmente i treni di giorno non portano alcuna segnalazione di testa.

La segnalazione di testa di notte e invece costituita dai due fanali bianchi accesi della locomotiva o della carrozza pilota.
Il segnalamento notturno va portato anche nei tratti di linea con gallerie di lunghezza considerevole.

# Segnali di coda

Il segnalamento di coda serve per individuare con certezza la completezza del treno ovvero se questo è giunto completo nella stazione successiva senza che si sia spezzato lasciando una parte dei veicoli in linea. 

Questo tipo di accertamento è fondamentale per la sicurezza della circolazione nelle linee gestite con il Blocco elettrico manuale o con il Blocco telefonico e anche nelle altre linee in caso di guasto del sistema di blocco elettrico.

Tanto di giorno che di notte, la coda può essere individuata con due fanali speciali con schermo a righe oblique bianche e rosse che proiettano luce rossa lampeggiante.

![Immagine](/Documentazione/Immagini/SegnaleDiCoda1.png)

Oppure con due fanali a luce rossa fissa muniti di base a strisce inclinate bianche e rosse (segnalazione in uso per alcuni treni merci provenienti da rete estera su particolari relazioni di inoltro di norma limitate all'Italia settentrionale - la segnalazione è comunque valida su tutte le linee di RFI).

![Immagine](/Documentazione/Immagini/SegnaleDiCoda2.png)

# Tabella speciale per treni a composizione bloccata

Tabella speciale posta in testa ed in coda ai treni con composizione bloccata (treni navetta, materiale leggero, treni comunque reversibili) e posizionata in basso al veicolo estremo del treno a destra o in posizione centrale.

![Immagine](/Documentazione/Immagini/ComposizioneBloccata.png)

I treni portanti tali tabelle viaggiano, sia di giorno che di notte, con la segnalazione di testa e di coda luminosa accesa e pertanto non occorre la tabella a righe oblique bianche e rosse o il fanale a luce rossa lampeggiante con schermo a righe oblique.

# Circolazione sul binario illegale

Abbiamo già detto che sulle linee a doppio binario i treni normalmente circolano sul binario di "sinistra". Qualora, per interruzione di tale binario per lavori, ostacoli o altro, un treno dovesse essere indirizzato sul binario di destra, si dice che percorre il binario "illegale". Tutti i treni pertanto percorrono l'"unico" binario rimasto in esercizio (circolazione a binario unico).
Questo vale sulle linee non banalizzate ovvero non munite di Blocco Automatico o conta-assi atto a distanziare i treni circolanti sia sul binario di sinistra che su quello di destra.
Nelle linee non banalizzate, i treni percorrono il binario di destra (illegale) senza utilizzare dei sistemi di blocco elettrico e senza segnali e pertanto devono essere distanziati con il blocco telefonico e fatti partire dalle stazioni (licenziati) con segnali a via impedita.

Durante una circolazione "a binario unico", il primo treno che percorre il binario in senso illegale deve portare delle speciali segnalazioni di testa costituite, di giorno, da una bandiera rossa a destra e, di notte, da un fanale a luce rossa a destra nel senso di marcia del treno. 

![Immagine](/Documentazione/Immagini/SegnalamentoBinarioIllegale.png)

Questa segnalazione deve essere mantenuta nella tratta di circolazione a destra. Dopo l'arrivo del treno nella stazione ove termina la circolazione a destra devono essere ripristinati i normali segnali di testa.

# Fonte

- [I segnali di testa e di coda dei treni](https://www.segnalifs.it/sfi/it/sa/N_segntreni.htm)
