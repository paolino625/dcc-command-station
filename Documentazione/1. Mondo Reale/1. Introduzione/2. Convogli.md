- [Introduzione](#introduzione)
- [Trazione multipla](#trazione-multipla)
- [Carrozze semipilota](#carrozze-semipilota)
- [Convoglio bloccato](#convoglio-bloccato)
- [Frenatura treno](#frenatura-treno)
  - [Funzionamento](#funzionamento)
- [Fonti](#fonti)

# Introduzione

Un convoglio è un complesso di uno o più veicoli, con almeno una unità di trazione (mezzo di trazione), che abbia una cabina di guida ed un "sistema di frenatura" che serve per svolgere un determinato servizio ferroviario (trasporto di persone o merci, manutenzione dell’infrastruttura, soccorso ad altri convogli, movimentazione di veicoli).

# Trazione multipla

La trazione multipla viene utilizzata in vari contesti ferroviari quando è necessario avere più potenza per le seguenti ragioni:
- Convogli molto lunghi o pesanti
- Pendenze impegnative

Ad esempio, treni merci in Trentino Alto Adige presentano doppia trazione, una locomotiva in testa e un'altra in spinta.

In alcuni casi è possibile avere anche tripla trazione: una locomotiva in testa, una in spinta e l'altra in mezzo oppure appena dietro la locomotiva di testa. Questa tipologia di composizione è più difficile da trovare.

![Immagine](/Documentazione/Immagini/DoppiaTrazione.jpg)

# Carrozze semipilota

Le carrozze semipilota sono particolari carrozze ferroviarie per il trasporto di passeggeri, parzialmente diversificate da quelle normali tramite l'aggiunta ad un'estremità di una cabina di guida, la quale permette di comandare a distanza la locomotiva, realizzando così la possibilità del regresso dei treni giunti a destinazione, senza dover eseguire alcuna manovra per riportare la locomotiva in testa al convoglio.

Le carrozze pilota, invece, hanno due cabine di guida e sono utilizzabili senza necessità di giratura in piattaforma quando vengono messe in composizione ai treni. In genere hanno tale configurazione (due cabine di guida) i rimorchi delle automotrici e solo raramente le carrozze ordinarie.

Un convoglio completo risulta quindi composto da una carrozza pilota/semipilota, da carrozze passeggeri e da un locomotore.

Sulle semipilota/pilota non è presente un motore: i comandi vengono reindirizzati al locomotore attraverso una condotta elettrica, che si estende lungo tutto il treno, o cavi seriali per il telecomando.

![Immagine](/Documentazione/Immagini/CarrozzaSemipilota.jpg)

L'introduzione delle semipilota, avvenuta in Italia con le carrozze vicinali a piano ribassato, ha permesso di aumentare notevolmente la frequenza dei viaggi effettuati da uno stesso convoglio. Per effettuare un viaggio in senso opposto, con una semipilota in composizione non è più necessario, infatti, instradare il treno su un binario di manovra o spostarne il locomotore: basta reindirizzare i comandi, tramite una procedura detta "cambio banco".

Spesso l'aspetto estetico della semipilota viene pensato in funzione del locomotore tipicamente usato sul convoglio a cui sarà assegnata, per dare una simmetria estetica al treno: un esempio sono le nuove semipilota delle carrozze Vicinali Piano Ribassato e le semipilota Vivalto, progettate per apparire somiglianti ai locomotori E.464, usati in composizione con le carrozze in questione.

# Convoglio bloccato

Un convoglio bloccato o a composizione bloccata è un treno con una particolare configurazione non modificabile in situazioni normali; mentre con una composizione non bloccata è possibile staccare ed attaccare carrozze o cambiare il locomotore con poche operazioni e in tempi relativamente brevi in adeguata stazione, un treno a composizione bloccata può richiedere il ricovero in un'officina attrezzata, dove il cambio di configurazione può arrivare ad interventi di grossa portata, della durata di diverse ore o giorni (a seconda della tipologia del materiale rotabile e della consistenza della variazione).

![Immagine](/Documentazione/Immagini/ConvoglioBloccato.jpg)

I treni a composizione bloccata sono solitamente quelli pensati per linee e servizi specifici, come relazioni alta velocità, treni suburbani e metropolitani. Un esempio è l'ETR.500, che viene usato in diverse configurazioni, tutte modificabili unicamente tramite intervento in deposito locomotive.

Caratteristica generalmente tipica di un convoglio a composizione bloccata è quella di non necessitare di manovre per l'inversione del senso di marcia (convoglio reversibile), permettendo quindi un notevole guadagno in termini di tempo nelle stazioni di termine corsa (ed evitando la caduta di potenzialità degli impianti di stazione di testa), dato che elimina la necessità delle manovre tradizionali di inversione.
 
# Frenatura treno

Come fa un macchinista a sapere quando rallentare per entrare in stazione?

I convogli ferroviari in Italia hanno una massa di centinaia di tonnellate e possono viaggiare fino a 300 km/h. Inoltre, avendo le ruote in acciaio che poggiano su rotaie in acciaio, il coefficiente di attrito è molto basso e quindi bisogna accelerare e decelerare lentamente per evitare slittamenti o pattinamenti.
Ne consegue che lo spazio di frenatura di un treno è notevolmente lungo.
Supponiamo di trovarci in cabina di un treno regionale che sta andando a 160 km/h (44,4 m/s).
Supponendo una decelerazione media di 0,5 m/s^2 sono necessari 2 km per fermarsi completamente, concedendosi di allentare i freni ogni tanto per modulare il rallentamento ed arrestarsi con precisione.

![Immagine](/Documentazione/Immagini/FrenaturaTreno1.png)

D'accordo, un macchinista potrebbe essere molto abile e frenare costantemente al massimo iniziando in un momento ben preciso e finire esattamente in stazione, questo porterebbe ad avere una decelerazione media maggiore ma anche in questo caso si riuscirebbe a fermarsi in un 1 km. Insomma stiamo parlando di un semplice regionale, non certo di un pesante merci o di un veloce freccia rossa, eppure quando si comincia a rallentare la stazione è ancora ben lontana da poter essere persino vista. Come fanno i macchinisi?

Il primo strumento su cui possono contare è la scheda treno: documento nel quale vengono elencate tutte le località dove viene passato un determinato treno con tutta una serie di informazioni su pendenze, limiti di velocità, segnalamento e soprattutto vengono indicate le progressive chilometriche di ogni località e punto particolare della linea. 

Le progressive chilometriche vengono riportate in linea da appositi segnali: i cippi chilometrici, oggi in metallo. Hanno strisce orizzontali bianche e blu ed hanno un numero scritto in verticale che indica il chilomeetro. Un macchinista potrà utilizzarli per orientars ie capire quando cominciare a rallentare e affidandosi alla sua esperienza e valutando l'eventuale ritardo del treno: potrebbe rallentare il più tardi possibile e guadagnare un minuto.

Utilizzare le progressive chilometriche non è molto pratico, i segnali non sono sempre facilmente leggibili, complici la velocità del treno e la poca visibilità dovuta a buio, piante, nebbia.
Per questo motivo esistono altre due segnalazioni.

Occorre precisare che le località dove scendono e salgono i passeggeri si dividono in stazioni e fermate. Le stazioni sono dotate di scambi che permettono ai treni di effettuare precedenze o incroci. Le fermate, al contrario, sono soltanto dei marciapiedi posati ai lati dei binari.

Per le fermate esistono appositi segnali, 2 di avviso. l'altro indica il punto in cui termina il marciapiede ed entro il quale si è obbligati ad arrestarsi.

![Immagine](/Documentazione/Immagini/FrenaturaTreno2.png)

La questione sulle stazioni è ad oggi meno semplice, anche qui occorre fare una premessa, i treni in arrivo ad una stazione incontrano 3 semafori, avviso, protezione e partenza.

![Immagine](/Documentazione/Immagini/FrenaturaTreno3.png)

Anni fa una stazione che si preparava ad accogliere un treno per la fermata, metteva l'avviso a via libera, la protezione a via libera con preavviso di via impedita e la partenza a via impedita.

![Immagine](/Documentazione/Immagini/FrenaturaTreno4.png)

Così facendo i treni venivano praticamente forzati a fermarsi in stazione. Oggi tale pratica è rimasta solo sulle linee a singolo binario, negli altri casi deve essere il macchinista ad accorgersi della presenza della stazione. In questo, la presenza stessa dei segnali può essere un aiuto, anche se non sostituisce la consapevolezza di dove ci si trova lungo la linea data dalle progressive chilometriche. Un ultimo importante aiuto viene dato dalla conoscenza della linea che è fondamentale per un macchinista per sapersi orientare e per poter dare meglio, rallentando solo quando necessario e recuperando minuti preziosi, anche grazie alla sua esperienza ai comandi.

## Funzionamento

Il sistema frenante è costituito da una condotta pneumatica (detta "condotta generale") che attraversa tutto il treno e che, quando il treno è "sfrenato" presenta una pressione di 5 bar.

Creando una depressione, più o meno accentuata, in questa condotta si ottiene la frenatura graduale del treno. Per sfrenare i veicoli occorre riportare la pressione in condotta a 5 bar.

La condotta viene alimentata da serbatoi di aria compressa e da compressori presenti nella locomotiva mentre la depressione o la rialimentazione della condotta avviene mediante un apposito rubinetto presente nella cabina di guida delle locomotive o delle vetture semi-pilota.

Tutte le volte che un treno viene composto, manovrato o che comunque viene manomessa e poi ripristinata la continuità della condotta generale del freno, occorre eseguire la prova freno secondo quanto di seguito specificato.

Tale prova viene effettuata anche quando viene cambiato il rubinetto di comando del freno (inversione di marcia di treno navetta), quando il treno è stato abbandonato da oltre 60 minuti o comunque ogni 24 ore per i treni navetta.

La prova del freno consiste nel verificare la frenatura dei veicoli e la successiva sfrenatura.

Gli ordini al macchinista per l'esecuzione della prova possono essere dati dal verificatore anche mediante specifici segnali luminosi.

# Fonti

- [Come fa un MACCHINISTA a sapere QUANDO FRENARE?](https://www.youtube.com/watch?v=j74ik5XiIF4&t=6s)
- [La prova del freno continuo](https://www.segnalifs.it/sfi/it/rc/N_provafreno.htm)

