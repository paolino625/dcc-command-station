[Voce](https://ttsdemo.com)

# Lettura numeri

## Treno

La citazione del numero del treno può essere omessa (nonostante si tratti di un testo non opzionale) per i treni
categoria Regionali.

Per la lettura del numero del treno devono essere adottate le seguenti specifiche:

- A 2 cifre: lettura integrale (es: 84 si leggeStato “ottantaquattro”)
- A 3 cifre: lettura per singola cifra (es: 753 si leggeStato “sette/cinque/tre”)
- A 4 cifre: lettura a gruppi di due cifre (es: 9448 si leggeStato “novantaquattro/quarantotto”)
- A 5 cifre: lettura del gruppo di due cifre iniziali poi per singola cifra (es: 12156 si leggeStato
  “dodici/uno/cinque/sei”)

## Orari

Se il minuto è a singola cifra, viene letto con lo 0 davanti.
Ad esempio un treno delle 16:05: "Sedici e zero cinque".

# Annunci

## Partenze

### Parte in comune: infoTrenoPartenza()

IL TRENO
*CATEGORIA*
*NUMERO*
DELLE ORE
*ORE PARTENZA*
PER
*LOCALITA' DI ARRIVO*

### Partenza treno

infoTrenoPartenza()

È IN PARTENZA
[IN RITARDO] (se parte con ritardo maggiore di 15 minuti)
DAL BINARIO
*NUMERO DEL BINARIO*

### Preavviso di partenza treno in ritardo

ANNUNCIO RITARDO!

infoTrenoPartenza()

PARTIRA'
CON UN RITARDO PREVISTO DI
*TEMPO RITARDO*
MINUTI

### Soppressione treno in partenza

ANNUNCIO CANCELLAZIONE TRENO!

infoTrenoPartenza();

OGGI NON SARA' EFFETTUATO

CI SCUSIAMO PER IL DISAGIO

## Arrivi

## Parte in comune: infoTrenoArrivo()

IL TRENO
*CATEGORIA*
*NUMERO*
DELLE ORE
*ORA ARRIVO*
PROVENIENTE DA
*LOCALITA' DI PROVENIENZA*

### Arrivo treno

infoTrenoArrivo()
È IN ARRIVO AL BINARIO
*NUMERO DEL BINARIO*

ATTENZIONE!
ALLONTANARSI DALLA LINEA GIALLA

### Preavviso di arrivo treno in ritardo

ANNUNCIO RITARDO!

infoTrenoArrivo()

ARRIVERA' CON UN RITARDO PREVISTO DI
*TEMPO RITARDO*
MINUTI

### Soppressione treno in arrivo

ANNUNCIO CANCELLAZIONE TRENO!

infoTrenoArrivo()

OGGI NON È STATO EFFETTUATO

## Arrivi/Partenza

### Parte in comune: infoTrenoArrivoPattenza()

IL TRENO
*CATEGORIA*
*NUMERO*
PROVENIENTE DA
*LOCALITA' DI PROVENIENZA*
E DIRETTO A
*LOCALITA' DI ARRIVO*
DELLE ORE
*ORA PARTENZA*

### Arrivo/partenza treno

infoTrenoArrivoPattenza()

È IN ARRIVO
[IN RITARDO]
AL BINARIO
*NUMERO DEL BINARIO*

ATTENZIONE!
ALLONTANARSI DALLA LINEA GIALLA

### Preavviso di arrivo / partenza treno in ritardo

ANNUNCIO RITARDO!

infoTrenoArrivoPartenza()

ARRIVERA' CON UN RITARDO PREVISTO DI
*TEMPO RITARDO*
MINUTI

### Soppressione treno in arrivo/partenza

ANNUNCIO CANCELLAZIONE TRENO!

infoTrenoArrivoPartenza()

OGGI NON È STATO EFFETTUATO

## Anomalie

### Sciopero

ATTENZIONE!

AVVISIAMO CHE
OGGI DALLE ORE
*ORE*
ALLE ORE
ORE*

I TRENI
POTRANNO SUBIRE VARIAZIONI
PER UNO SCIOPERO DEL PERSONALE

CI SCUSIAMO PER IL DISAGIO

### Incendi

ATTENZIONE!

AVVISIAMO CHE I TRENI DA E PER
*LOCALITA'*
SUBIRANO RITARDI

PER UN INTERVENTO DEI VIGILI DEL FUOCO NEI PRESSI DELLA LINEA

### Investimenti di persone

ATTENZIONE!

AVVISIAMO CHE I TRENI DA E PER
*LOCALITA'*
SUBIRANNO RITARDI
PER ACCERTAMENTE DELL'AUTORITA' GIUDIZIARIA A SEGUITO DELL'INVESTIMENTO DI UNA PERSONA

### Rimozione di residuati bellici

ATTEZIONE!

AVVISIAMO CHE
I TRENI
[DA E PER *LOCALITA*]
SUBIRANNO RITARDI
PER LA RIMOZIONE DI RESIDUATI BELLICI

CI SCUSIAMO PER IL DISAGIO

### Furto di cavi di rame sulla linea

ATTENZIONE!

AVVISIAMO CHE I TRENI DA E PER
*LOCALITA*
SUBIRANNO RITARDI
PER UN FURTO DI CAVI DI RAME SULLA LINEA TRA LE STAZIONI DI
*NOME STAZIONE*
E DI
*NOME STAZIONE*

## Richiami di attenzione

### Treno che non fa servizio viaggiatori

ATTENZIONE!

AVVISIAMO I VIAGGIATORI CHE IL TRENO
IN ARRIVO AL BINARIO
*NUMERO DEL BINARIO*
NON FA SERVIZIO VIAGGIATORI

### Divieto di attraversare i binari

ATTENZIONE!

È VIETATO ATTRAVERSARE I BINARI.
SERVIRSI DEL SOTTOPASSAGGIO.

### Allontanarsi dalla linea gialla per treno in arrivo

ATTENZIONE!

TRENO IN ARRIVO AL BINARIO
*NUMERO DEL BINARIO*

ALLONTANARSI DALLA LINEA GIALLA

### Allontanarsi dalla linea gialla per treno in transito

ATTENZIONE!

TRENO IN TRANSITO AL BINARIO
*NUMERO DEL BINARIO*

ALLONTANARSI DALLA LINEA GIALLA

### Divieto di salita o discesa dai treni in movimento

ATTENZIONE!

SI RAMMENTA CHE È VIETATO APRIRE LE PORTE ESTERNE DEI TRENI
E SALIRE O SCENDERE QUANDO NON SONO COMPLETAMENTE FERMI

## Annunci a richiesta

### Richiesta di intervento di un medico

ATTENZIONE!

SE IN STAZIONE È PRESENTE UN MEDICO
È INVITATO A RECARSI AL BINARIO
*NUMERO BINARIO*

## Composizione del treno e servizi a bordo

### Classi delle carrozze del treno

LA PRIMA CLASSE È
(IN TESTA AL, IN CODA AL, AL CENTRO DEL)
TRENO
