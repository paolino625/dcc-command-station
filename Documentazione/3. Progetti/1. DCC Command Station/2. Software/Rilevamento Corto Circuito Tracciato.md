- [Corto circuito](#corto-circuito)
  - [Cosa succede](#cosa-succede)
  - [Soluzione](#soluzione)

# Corto circuito

Un corto circuito può avvenire quando viene posato un oggetto metallico tra i due binari oppure quando un rotabile deraglia e le ruote di uno stesso lato finiscono contemporaneamente sui due binari.

## Cosa succede

Nell'eventualità di un corto circuito, l'alimentatore non interviene ed eroga il massimo erogabile (nel nostro caso 10 A) fino a quando il corto circuito non viene interrotto.

I binari e i cavi usati sono in grado di dissipare 10 A, quindi non dovrebbero surriscaldarsi al punto da prendere fuoco. Non si può dire lo stesso dei ponticelli di giunzione usati per gli scambi.

## Soluzione

In ogni caso, per ragioni di sicurezza, è bene evitare ad ogni costo corto circuiti, specie per quando il plastico funzionerà in maniera non supervisionata.

INA219 viene utilizzato per rilevare quando l'assorbimento del tracciato è molto alto, ad esempio sopra i 9 A. In questo caso si assume che un corto circuito sia in atto, viene dunque tolta l'alimentazione al tracciato e viene richiesto un intervento manuale per ripristinare l'alimentazione sul tracciato.