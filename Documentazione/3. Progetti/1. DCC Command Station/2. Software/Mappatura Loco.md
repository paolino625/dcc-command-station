# Mappatura Loco

## Problema

Ogni locomotiva è dotata di un decoder DCC.
La regolazione della velocità avviene inviando un pacchetto DCC al decoder, specificando una velocità compresa tra 1 e
128.
Il decoder, in base al numero ricevuto, regola la tensioneMinimaCorrettoFunzionamento fornita al motore, determinando
così la velocità della locomotiva.

Tuttavia, sorgono alcune sfide:

- Ogni decoder può avere un mapping step -> tensioneMinimaCorrettoFunzionamento diverso.
- Ogni locomotiva è unica: il motore può variare, ma anche se fosse lo stesso, potrebbero esserci differenze nei
  componenti come gli ingranaggi, influenzando così la velocità della locomotiva.

Questa situazione può generare l'effetto seguente: consideriamo due locomotive con decoder entrambi impostati allo step
20. Una locomotiva potrebbe muoversi a 5 cm/s, mentre un'altra potrebbe raggiungere i 15 cm/s.

Questa discrepanza rappresenta una problematica, poiché gli utenti desiderano regolare la velocità delle locomotive in
base a un riferimento assoluto, senza dover memorizzare la corrispondenza id tra uno specifico step e la velocità per
ogni singola locomotiva.

## Soluzione

È imprescindibile implementare una fase di mappatura che associ ad ogni step del decoder la corrispondente velocità
effettiva della locomotiva.

È sufficiente disporre la locomotiva sui binari, configurando il tracciato in modo tale che la locomotiva esegua un
percorso a circuito chiuso.

In seguito, è possibile avviare la funzione di mappatura su Arduino, indicando l'indirizzo della locomotiva e
specificando il sensori IR da utilizzare.

Arduino, per ognuno dei 128 step, avvia la locomotiva al corrispondente step e misura il tempo trascorso tra il primo e
il secondo passaggio della locomotiva sul sensore IR. Essendo a conoscenza della lunghezza del percorso, è in grado di
calcolare la velocità effettiva della locomotiva.

In questo modo viene stabilita una mappatura accurata tra gli step del decoder e le rispettive velocità reali della
locomotiva.

Di fronte a step molto piccoli, potrebbe essere necessario un tempo eccessivo per consentire alla locomotiva di
attraversare due volte lo stesso sensore IR. Pertanto, è possibile configurare Arduino in modo che utilizzi due sensori
IR vicini per accelerare il processo di mappatura.

È importante eseguire la mappatura in entrambe le direzioni, poiché, da esperimenti condotti, è emerso che i valori
possono variare.

## Applicazioni

### Scala standard

L'utente ha la possibilità di definire la velocità delle locomotive utilizzando un valore in scala assoluta, uniforme
per tutte le locomotive. Abbiamo scelto come riferimento la velocità del modello in scala reale (ovvero la velocità
effettiva del modello moltiplicata per un fattore di scala, come ad esempio 87). Se l'utente imposta una velocità di 10
km/h, corrispondente a 3.20 cm/s per il modello in scala, Arduino individuerà lo step del decoder più prossimo a questa
velocità per ciascuna locomotiva e lo imposterà di conseguenza. Ad esempio, potrebbe essere lo step 10 per la locomotiva
numero 1 e lo step 20 per la locomotiva numero 2.

### Accelerazione e decelerazione graduali

Supponiamo di voler integrare in Arduino una funzionalità che consenta alle locomotive di accelerare e decelerare
gradualmente.
Ad esempio, se una locomotiva sta procedendo a velocità step 50 e l'utente imposta lo step 0, Arduino non trasmetterà
immediatamente al decoder la velocità 0, ma organizzerà una decelerazione graduale da 50 a 0 per garantire una
transizione fluida.

Dato che il mapping del decoder in velocità spesso non segue una relazione lineare, l'accelerazione e la decelerazione
potrebbero risultare non completamente precise. Per esempio, se Arduino diminuisse lo step di 1 ogni secondo, potremmo
avere una variazione della velocità da 9 cm/s a 8.5, 8, 6, fino a 3. In questo contesto, la mappatura si rivela
essenziale: grazie ad essa, l'accelerazione e la decelerazione saranno uniformi e precise, garantendo una transizione
fluida tra le diverse velocità (da 9 cm/s a 8, 7, 6, e così via).

### Doppia trazione

In un convoglio a doppia trazione, una locomotiva è in trazione e l'altra è in spinta, richiedendo una regolazione
estremamente precisa delle loro velocità. Anche una leggera differenza di velocità può provocare problemi significativi.

Se la locomotiva in trazione va leggermente più veloce rispetto a quella in spinta, il convoglio può deragliare in curva
a causa dell'eccessiva tensione su uno dei vagoniReali intermedi, che si inclina verso l'interno.

Al contrario, se la locomotiva in spinta esercita una spinta maggiore, un vagone potrebbe essere schiacciato sia da
davanti e da dietro, potrebbe sollevarsi e deragliare.

Risulta dunque essenziale un mapping accurato per evitare tali problematiche.

#### Note

Nonostante la precisione del mapping, ci saranno sempre piccole differenze tra la velocità della locomotiva che spinge e
quella che trae.

Utilizzando le variabili appropriate nella struct Convoglio, è possibile stabilire una differenza di velocità tra le due
locomotive.

È essenziale condurre test su ogni convoglio per determinare la tolleranza dei vagoniReali.
Se i vagoniReali mostrano una buona flessibilità al deragliamento, potrebbe essere vantaggioso impostare la locomotiva
che spinge a una velocità leggermente superiore, in modo da garantire che i vagoniReali rimangano uniti durante la
corsa, migliorando l'aspetto visivo del convoglio.

## Affidabilità nel tempo

La lubrificazione degli ingranaggi di una locomotiva potrebbe variare nel tempo, influenzando così l'affidabilità della
mappatura effettuata della locomotiva.
Nonostante le verifiche effettuate fino a oggi non evidenzino cambiamenti rilevanti a quattro mesi dalla mappatura, è
fondamentale continuare con ulteriori test per confermare queste constatazioni e assicurare l'affidabilità della
mappatura nel tempo.