# Interfaccia grafica

Ho pensato a due modalità di interazione tra utente e sistema:

- KeypadAbstract e display 20x4
- Sito Web

La prima modalità è un po' limitata ed è pensata da essere utilizzata per operazioni di manutenzione o comunque quando
si hanno dei problemi con il web Server.
Il sito Web, invece, è l'interfaccia che viene utilizzata quotidianamente, in quanto più completa.
Permette infatti di visualizzare il sinottico di tutto il plastico ed avere tutta la situazione sotto controllo.

# Utenti

- Io: utente con la possibilità di effettuare operazioni avanzate, ovvero tutte quelle a disposizione.
- Una persona della mia famiglia che vuole far vedere il plastico a degli ospiti senza la mia supervisione.

## Autenticazione

- Se persona presente vicino al plastico, impronta digitale
- Da remoto, inserimento di nome utente e password.

# Funzionalità

## Leggenda

- AMMINISTRATORE = richiesto su interfaccia SOLO amministratore
- TUTTI = richiesto interfaccia utente e amministratore
- NO = non richiesto in nessuna interfaccia

## Convogli

- Aggiungo
    - Aggiungo un nuovo convoglio già con una locomotiva mappata (NO)
- Rimuovo
    - Elimino convoglio (NO)
- Gestisco
    - Gestisco loco
        - Aggiungi (NO)
        - Elimina (NO)
        - Accendi/Spegni luci (TUTTI)
    - Gestisco vagoniReali
        - Aggiungi (NO)
        - Elimina (NO)
    - Gestisco presenza
        - Metti sul plastico
            - Scelta sezione (TUTTI)
        - Togli dal plastico (TUTTI)
        - Mostro lista di convogli su tracciato e quelli non (TUTTI)
- Reset (NO)

## Locomotive

- Mapping velocità (NO)
- Tempo uso (NO)
- Mapping velocità (NO)
    - Tutti step
    - Intervallo step
    - Step singolo
- Cambio indirizzo (NO)

## Accessori

- LED
    - Pre-set striscie LED. (TUTTI)
    - Scelta manuale colore (TUTTI)
- Stop emergenza (TUTTI)
- Orario locale del plastico (TUTTI)
- Stato dei vari moduli. Tensione e assorbimento. Stato anche della Motor Shield.
- Audio
    - Lista comandi
      audio ?? [In autopilot partono in automatico annunci: "treno 2492 provieniente da ... a ... è in arrivo al binario di...". In modalità manuale ha senso permettere all'utente di far partire alcuni annunci preimpostati?]
    - Settare volume (TUTTI)

## Comando treni

### Modalità manuale (AMMINISTRATORE)

- Possibilità di modificare manualmente gli scambi.
- Scelta velocità delle locomotive e direzione. Luci.

### Modalità autopilot

AutopilotConvoglio disponibile sia in modalità amministratore che utente: magari modalità utente semplificare
ulteriormente?

- Colori delle sezioni che cambiano in base a se la sezione è occupata o meno.
- Stato di ogni singola locomotiva.
- Scelta modalità autopilot.
    - Evidenzio sul sinottico le stazioni che possono accogliere un convoglio di tale lunghezza (che conosco)
      Evito di evidenziare la stazione di partenza.
      L'utente sceglie tra due modalità:
        1) Modalità circolare. Opzione inverti ordine stazioni. Opzione: Loop.
        2) Modalità tratta. Opzione: andata e ritorno. Opzione: Loop (solo quando scegli andata e ritorno).
           L'utente, attraverso il sinottico, clicca sulle stazioni dove vuole far fermare il treno.
           L'utente inoltre id:

        - Tempo di attesa convoglio stazione intermedia.
        - Tempo di attesa tra una corsa e l'altra.

## Monitoraggio

- Tensioni 6 alimentatori che alimentato tutto.
- Assorbimento in mA del tracciato.

# Dubbi principali

- Come rappresentare il multi-layer del tracciato?
- Input: sito web deve essere usabile sia da monitor 16" touchscreen che non. Consentire modalità amministratore solo da
  computer dove si ha il mouse per puntare in maniera precisa o consentirlo anche su touchscreen fisso in sala giochi?
- Pagina web molto semplificata per telefono, sia amministratore che utente?
- Durante modalità in manuale, mostrare uno slider per ogni convoglio sul tracciato (per consentire controllo velocità)
  oppure solo 2-3, assegnabili ai vari convogli? Vorrei evitare che l'interfaccia grafica in base a quanti convogli sono
  presenti sul plastico. Anche perché, il plastico potrebbe arrivare ad ospitare contemporaneamente più di 10 convogli:
  difficilmente in modalità manuale ne si riescono a controllare più di 2-3 in contemporanea...

# Link Utili

- Repository: https://gitlab.com/paolino625/dcc-command-station
- Tracciato
  Plastico: https://gitlab.com/paolino625/dcc-command-station/-/tree/master/Documentazione/3.%20Progetti/2.%20Tracciato/Mappe%20Plastico/Immagini?ref_type=heads