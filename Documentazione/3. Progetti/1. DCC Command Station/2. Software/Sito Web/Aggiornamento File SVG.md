# Aggiornare

## Numero Scambi

- Aggiornare codice Arduino Master
- Aggiornare codice ESP8266

## Numero Locomotive

- ArduinoMaster.ino
- ESP32.ino
- controlli.html

## SVG

### Titolo

Prima degli stili aggiungere "#Titolo".
Esempio: #Titolo .st2{font-size:70px;}

### Tracciato

- Aprire Tracciato.svg e attivare tutti i livelli

- Fare un copia del file Tracciato.svg

- Togliere le righe relative alla mappa del plastico

- Assicurarsi che l'ID dell'SVG sia "Tracciato"

- Prima degli stili aggiungere "#Tracciato".
  Esempio: #Tracciato .st2{font-size:70px;}

- Aggiornare indici stili (esempio: st0) sul file javascript a seconda dei "Campioni Stile"

- Eliminare oggetto "Campioni Stile"

- Fare upload del file aggiornato su ESP8266.

### Scambi

- Fare un copia del file tracciato.svg e rinominarlo in scambi.svg.

- Sostituire " con \"

- Modificare stile st scambi 2° piano

- Aggiornare il codice in ESP8266.ino

- Controllare variabili "stileNormaleSVG" e "stileTrasparente"