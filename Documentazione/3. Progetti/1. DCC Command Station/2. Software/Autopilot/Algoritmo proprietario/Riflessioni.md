# Introduzione

Inizialmente si era pensato tra due modalità con cui permettere all'utente la configurazione dell'autopilot.

## Scelta stazioni

L'utente sceglie le stazioni del percorso. Sarà poi l'autopilot a decidere su quale binario della stazione far fermare
il convoglio, a seconda della disponibilità

### Vantaggi

- Il sistema può scegliere il binario della stazione in base alla disponibilità in quel momento.
- Il sistema può scegliere il binario disponibile più piccolo, in modo da non "sprecare" binari più lunghi.

### Svantaggi

- Nella definizione del percorso scelto dall'utente, tra due stazioni consecutive potrebbe non esserci un percorso
  diretto disponibile.
  In questo caso il sistema dovrebbe portare il convoglio a una stazione simmetrica e invertire il convoglio oppure
  portare il convoglio al cappio di ritorno.
  Questo potrebbe essere fastidioso per l'utente che vede il convoglio non rispettare alla lettera il percorso scelto.
- I convogli verranno parcheggiati sempre nello stesso binario: convogli molto corti su binari corti e convogli lunghi
  su binari lunghi.
- Se volessi pianificare un convoglio navetta dal binario 1.1.1 al binario 1.5.4 come potrei fare? Se l'utente sceglie
  le stazioni, potrebbe succedere che il binario 1.5.4 sia occupato, e in quella stazione sia libero solo 1.5.1. Il
  problema è che per entrare in 1.5.1 il convoglio non fa più navetta passando da sotto ma deve fare tutto il giro
  passando dall'altra parte.

## Scelta binari

L'utente sceglie i binari del percorso.

### Vantaggi

- L'utente vede il convoglio percorrere esattamente il percorso da lui designato (senza stop in stazioni intermedie)

## Svantaggi

- Se un altro convoglio si ferma su un binario e il convoglio in questione è in attesa che venga sbloccato il lock
  proprio su quel binario, si
  crea una situazione di stallo.

# Problema Deadlock

Il problema principale di questi due approcci è la possibilità che si vengano a creare situazioni di deadlock.
Avere molti più binari rispetto al numero di convogli potrebbe ridurre la possibilità del deadlock ma non è una
garanzia: ogni convoglio potrebbe rimanere in attesa di effettuare il lock su un binario che è attualmente occupato da
un altro convoglio (una sezione libera ci sarebbe ma nessun convoglio è programmato per andarci).

## Soluzioni

### Approccio casuale

I convogli si comportano in maniera totalmente casuale. Ogni convoglio, dopo un certo tempo di attesa in stazione,
decide di spostarsi su un binario libero di un'altra stazione. Questo metodo di selezione casuale del binario tra quelli
disponibili elimina la possibilità di creare una situazione di deadlock.

Potrebbe avere senso prediligere la casualit all'efficienza: ovvero i convogli corti possono fermarsi su binari corti (
valutare limite di tempo in questo caso?).

#### Non scelta

L'utente avvia l'autopilot e non sceglie niente.

#### Scelta dei convogli

L'utente seleziona i convogli che vuole vedere girare.

### Stazione successiva

L'utente, per ogni convoglio fermo, può scegliere di volta in volta il binario della stazione di arrivo tra quelli
disponibili e
abbastanza capienti: non gli è consentito selezionare un binario occupato in quanto non è dato sapere se si libererà in
futuro.

Ad esempio, l'utente può selezionare un convoglio che è fermo alla stazione A, e impostare un binario della stazione B
come destinazione successiva.
Una volta che il convoglio sarà arrivato alla stazione B, l'utente potrà scegliere la stazione successiva per quel
convoglio.

### Timetable

I timetable possono essere studiati a tavolino in cui si organizza il movimento dei vari convogli in modo tale che non
si presentino situazioni di deadlock.
Dopo la scrittura del timetable per una giornata di tempo nel modellismo ferroviario, sarà necessaria una fase di test
per verificare che non si presentino situazioni di deadlock.

### Scelta percorso

Se un individuo desidera impostare nel percorso del convoglio più di una stazione contemporaneamente, dovrebbe essere
permesso a tale individuo di scegliere solo binari attualmente liberi. Il convoglio dovrebbe bloccare tali binari
immediatamente, in questo modo non possono verificarsi situazioni di deadlock.
Nel caso in cui l'utente voglia che il convoglio esegua il percorso in maniera ciclica, il convoglio non rilascerà mai i
lock dei binari selezionati.

Questa modalità, sebbene garantisca l'assenza di deadlock presenta i seguenti problemi:

- Risulta monotona in quanto ogni binario è sempre occupato dallo stesso convoglio.
- Il numero di convogli che possono circolare contemporaneamente è molto limitato.