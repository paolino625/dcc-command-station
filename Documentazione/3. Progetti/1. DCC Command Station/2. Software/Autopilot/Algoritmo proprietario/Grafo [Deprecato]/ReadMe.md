Inizialmente l'idea era quella di creare un grafo che rappresentasse tutte le sezioni del tracciato, con due direzioni:
una oraria e una antioraria.
In questo modo sarebbe stato possibile trovare lo shortest path tra due stazioni, ovvero il percorso che il convoglio
avrebbe dovuto seguire.

Prima complicanza era evitare che il grafo, nel calcolo del percorso più breve, guidasse il convoglio in contromano
quando la via regolare era disponibile.
Per risolvere questo problema, si è pensato di rendere il grafo pesato, assegnando un peso maggiore (ad esempio, un
fattore penale di 10) ai blocchi percorsi in contromano.

Il percorso del grafo sarebbe stato calcolato in base a due fattori:

- La lunghezza del blocco
- La penalità per il percorso in contromano: se il blocco è percorso in contromano, il peso del blocco sarebbe stato
  moltiplicato per 10.

L'idea fin qui risulta complessa da progettare e implementare (basti pensare a come assegnare il peso ai blocchi),
tuttavia ancora fattibile.
Questo approccio però presenta un problema più grande: quando si cerca il percorso tra due stazioni, non sempre si è
interessati allo shortest path.
Ad esempio, se si considerano le stazioni EST e OVEST del 1° piano, il convoglio non farebbe in tempo ad uscire dalla
stazione EST che si ritroverebbe già dentro la stazione OVEST.
In questo caso, vorremmo che il convoglio faccia un giro completo prima di arrivare alla destinazione.

Già inserendo questo vincolo nel grafo, il problema diventa più complesso, in quanto non si tratterebbe più di trovare
lo
shortest path, ma di trovare un percorso nel grafo il cui peso sia sopra una certa soglia.

Anche volendo riuscirci, non sarebbe possibile personalizzare i percorsi: sarebbe impossibile ad esempio far sì
che il convoglio, nel percorso da stazione A a stazione B, effettui 5 giri.

Per questo motivo, si è deciso di abbandonare l'idea del grafo e di passare a un approccio più semplice, basato sulla
pre-configurazione dei percorsi tra le varie stazioni.