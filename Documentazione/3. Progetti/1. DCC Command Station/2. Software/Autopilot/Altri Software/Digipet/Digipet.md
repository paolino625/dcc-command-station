# Digipet

È possibile trovare il Manuale Win-Digipet in questa cartella.

Disclaimer: non ho utilizzato tale programma ma leggendo il manuale ho notato delle carenza nella progettazione del sistema e dei punti che non mi sono chiari.

## Dubbi

### Decelerazione locomotiva non ottimizzata

Dal capitolo 8.2.2 - Etichette intelligenti

![Immagine](/Documentazione/Immagini/Digipet1.png)

La logica del software non mi sembra ottimale.
Si assume che 2 metri siano sufficienti per una frenata dolce; si assume quindi il caso peggiore, ovvero che la locomotiva vada al massimo della velocità.

In questo caso, la locomotiva entra al massimo della velocità in stazione inizia la frenatura dopo 80 cm e gradualmente rallenta, fino a fermarsi esattamente dove si deve fermare, ovvero a 21 cm dalla fine del contatto 111.

Tuttavia, nel caso in cui la locomotiva vada ad velocità ridotta, inizierà la frenatura comunque dopo 80 cm in modo tale da fermarsi sempre a 21 cm dalla fine del contatto 111.

Tuttavia in questo caso la locomotiva inizierebbe a decelerare troppo presto e impiegherebbe molto tempo per fermarsi allo stop.
Il caso ottimo sarebbe far procedere la locomotiva alla velocità di crociera per un altro po' prima di iniziare la frenatura in maniera graduale, considerando che lo spazio di frenata richiesto è minore.

### Definizione manuale dei percorsi parziali

Dal manuale capitolo 8.7.1
>Fino a quando un treno viaggia lungo un itineario, tutti i dispositivi elettromagnetici in quella route sono bloccati. Non è possibile incrociare altri treni. I dispositivi elettromagnetici possono essere resi disponibili per il movimento di ulteriori treni , quando il treno ha raggiunto la sua destinazione all'interno del suo itinerario.

Non mi sembra una buona logica.
Prendiamo il seguente esempio.

![Immagine](/Documentazione/Immagini/Digipet5.png)

Impostiamo l’itinerario per la locomotiva n° 1 dal blocco 1 al blocco 5.
Diciamo che c’è una locomotiva n° 1 sul blocco 1 e una locomotiva n° 2 sul blocco 6, in attesa di proseguire sul blocco 1.
Seguendo questa logica la locomotiva n°1 occuperà lo scambio presente e non lo rilascerà fin tanto che non arriverà al blocco di destinazione dell’itinerario, ovvero il blocco 5.
Solo allora la locomotiva n° 2 dal blocco 6 potrà utilizzare lo scambio e andare al blocco 1.

Digipet è al corrente di questo problema, infatti dal manuale (stesso capitolo):
>Probabilmente il sistema potrebbe rallentare, se ci sono routes molto lunghe che contengono molti dispositivi elettromagnetici. Per evitare questo e anche per rendere le operazioni dei treni sul tracciato più interessanti, possono essere definiti due percorsi parziali all'interno del route e rilasciati prima che il treno abbia raggiunto il suo contatto di destinazione del suo percorso.

#### Solo due percorsi parziali?

Inoltre, il manuale prevede solo DUE percorsi parziali, non è dunque possibile inserirne degli altri?

#### Configurazione manuale percorsi parziali?

Dato che non ho capito se si possono impostare più di due percorsi parziali, assumiamo che si possano creare infiniti percorsi parziali per rendere il nostro sistema più efficiente.

Prendiamo ora l’esempio seguente:
![Immagine](/Documentazione/Immagini/Digipet6.png)

In questo caso tra ogni blocco e il suo successivo c’è uno scambio.
Per avere il sistema più efficiente possibile, la locomotiva n°1 che va dal blocco 1 al blocco 5, dovrebbe rilasciare lo scambio 1 quando arriva al blocco 2, lo scambio 2 quando arriva al blocco 3 e così via.

Ma se è l’utente a dover fare tutte queste configurazioni, ne uscirebbe pazzo: sarebbe un enorme quantità di lavoro con un'altissima possibilità di sbagliare qualcosa nella configurazione.

## Domande

### Configurazione manuale per sensori precedenti ad un binario di stazione

Tratto dal capitolo 8.2.3:
>In questo caso (distanza di frenata 110 cm) sarebbe una buona idea migliorare la nostra configurazione.
A questo scopo aggiungiamo 3 contatti addizionali prima del contatto 130, questi contatti son oil 129, lo 000 e il 132.

In altre parole, se il binario in stazione non è lungo abbastanza da permettere ad una locomotiva di frenare con una decelerazione graduale, Digipet va a considerare i sensori del “blocco precedente".

Quello che non ho capito è, nella pratica, deve essere l’utente a impostare i sensori del blocco precedente per ogni binario di stazione?

#### Esempio 1

Prendiamo come esempio la figura qui sotto: abbiamo due blocchi, quello a sinistra (blocco di transito) e quello a destra che è un binario di stazione.

![Immagine](/Documentazione/Immagini/Digipet2.png)

Deve essere l'utente a dire che per i treni in movimento dal blocco 1 al blocco 2, Digipet può utilizzare sensori addizionali, come il sensore n° 1 disegnato in figura?

Oppure è una cosa che Digipet fa in maniera automatica?
Ovvero, a seconda della provenienza del treno considera i sensori precedenti?

#### Esempio 2

Prendiamo un altro esempio, in cui ci sono due blocchi precedenti al binario stazione:

![Immagine](/Documentazione/Immagini/Digipet3.png)

È’ l’utente a dover programmare la seguente logica?
- Se il treno arriva dal blocco 1, digipet può usare il sensore n° 1.
- Se il treno arriva dal blocco 2 può usare il sensore n° 2.

Oppure questa operazione è automatica e Digipet capisce quale sensore leggere (dato che è già al corrente del blocco di provenienza del treno)?

## Fermata di una locomotiva su un blocco di transito

Digipet riesce a far fermare i treni alla banchina delle stazioni in maniera precisa e con una fermata graduale.
Ma sul manuale non c’è scritto nulla di cosa succede se una locomotiva deve fermarsi su un blocco che non è di stazione.

Prendiamo l’esempio di seguito, ci sono due blocchi che precedono un blocco di stazione.

![Immagine](/Documentazione/Immagini/Digipet4.png)

Una locomotiva n°2 che arriva dal blocco 2 avrà il semaforo verde e quindi potrà fermarsi al blocco della stazione con una fermata graduale e proprio alla distanza dal semaforo configurata.

Ma cosa succede se un’altra locomotiva, n°1, sta arrivando dal blocco 1?
Troverà un semaforo rosso e quindi deve fermarsi, in attesa che il blocco 3 si liberi.

Come gestisce questo caso Digipet?
Riesce a far fermare la locomotiva n° 1 in maniera graduale proprio prima del semaforo?
Se sì, è possibile impostare la distanza dal semaforo a cui vogliamo che il treno si fermi se il semaforo è rosso?