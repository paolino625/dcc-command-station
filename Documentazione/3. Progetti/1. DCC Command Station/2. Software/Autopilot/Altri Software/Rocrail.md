# Rocrail

Disclaimer: non ho utilizzato tale programma.

In Rocrail l'utente imposta tre velocità differenti per quando il treno entra in stazione:
- Vmin = velocità minima
- Vmid = velocità media
- Vmax = velocità massima

Il treno entra in stazione a Vmax, appena Rocrail rileva il suo passaggio sul sensore iniziale del binario della stazione, imposta la velocità a Vmid, senza gradualità.
Al passaggio sul sensore finale, Rocrail imposta la velocità a Vmin.
Dunque in questo caso la decelerazione è tutto tranne che graduale.

Rocrail consente però di avere una decelerazione più graduale del convoglio tramite una fase di apprendimento dall'utente: è quest'ultimo a decelerare la locomotiva durante l'entrata in stazione azionando la manopola della velocità. L'utente potrebbe essere attento a far fermare il convoglio esattamente al centro della banchina, per un risultato più realistico.

Rocrail registra i valori impostati dall'utente per poi ripeterli in maniera automatica quando un convoglio entra in stazione. 
All'utente viene chiesto di effettuare questa decelerazione per 3 volte, questo per avere dei risultati più precisi. 

Questo approccio, tuttavia, presenta numerosi problemi:
- Viene inserito un importante errore: l'utente, per quanto preciso, non riuscirà mai a decelerare la locomotiva in maniera graduale. Il fatto che Rocrail effettui questa misurazione 3 volte attenua il problema ma non lo risolve affatto.
- Se il convoglio viene modificato (ad esempio vengono aggiunti due vagoniReali), questo porterebbe il convoglio a non fermarsi più al centro della banchina. Per un risultato ottimale, l'utente dovrebbe reconfigurare la decelerazione di Rocrail ad ogni modifica del convoglio.
- La decelerazione graduale prevede che il convoglio in entrata alla stazione stia andando ad una velocità ben prefissata (Vmax). Nel caso in cui si voglia mantenere la decelerazione graduale, non sarebbe possibile far entrare il convoglio in stazione ad una velocità differente da quella usata nella fase di apprendimento.
- L'apprendimento viene effettuato su un solo binario di stazione: nel caso in cui il convoglio dovesse fermarsi su un altro binario della stazione, la mappatura non sarebbe più valida, ammenoché la lunghezza della sezione e la distanza tra i sensori sia sempre uguale.
