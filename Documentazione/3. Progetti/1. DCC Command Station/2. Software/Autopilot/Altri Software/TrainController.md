# TrainController

Sviluppato da Freiwald Software. (https://www.freiwald.com/pages/traincontroller.htm)

Effettua la mappatura automatica della velocità delle locomotive.

Anche in autopilot, la velocità delle locomotive viene gestita dall'utente e non dal software.

Durante l'autopilot il software, quando una locomotiva entra in un blocco, verifica che n blocchi avanti siano liberi (dove n viene impostato dall'utente).
Nel caso in cui il blocco n non sia libero, la velocità della locomotiva viene impostata a 0.
La locomotiva decelererà dunque con la curva di decelerazione impostata dal decoder.

Questo presenta numerosi problemi:
- La locomotiva può fermarsi molto prima del punto di stop.
- La locomotiva può andare talmente veloce da non riuscire a rallentare entro il blocco corrente ma potrebbe finire su quello successivo.
Impostando n alto, non avremmo problemi se la locomotiva non riuscisse a fermarsi entro il blocco corrente. Magari riesce a fermarsi nel blocco n+1, ma il blocco veramente occupato è n+3. In questo modo, però, perché la locomotiva sta occupando diversi blocchi inutilmente.