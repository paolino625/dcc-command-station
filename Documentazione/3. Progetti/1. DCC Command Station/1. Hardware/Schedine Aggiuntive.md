# Schedine aggiuntive

- [Schedine aggiuntive](#schedine-aggiuntive)
    - [Motor Shield](#motor-shield)
    - [Scheda per inversione polarità](#scheda-per-inversione-polarità)
    - [Amplificatore audio](#amplificatore-audio)

## Motor Shield

La Motor Shield svolge la funzione di convertire il segnale logico DCC 5 V generato da Arduino in un segnale 15 V.

Nello specifico viene utilizzata la scheda [MDD10A](https://futuranet.it/prodotto/driver-per-2-motori-dc-da-10-a/),
scheda più professionale e potente della più comune L298N, pensata appunto per il controllo di motori. Questa shield è
in grado di gestire una corrente massima di 10 A.

![Immagine](/Documentazione/Immagini/MDD10A.jpg)

## Scheda per inversione polarità

Per ulteriori informazioni sul cappio di ritorno, visitare
il [capitolo relativo](../../2.-Modellismo-Ferroviario/4.-Cappio-di-ritorno).

Uno dei dispositivi più conosciuti e usati per gestire i cappi di ritorno è il modulo Lenz LK200.
Si è scelto di utilizzare un dispositivo simile realizzato da dcc-decoders.com in quanto svolge le medesime funzioni ma
ad un prezzo più basso.

![Immagine](/Documentazione/Immagini/DCCAutoReverseModule.jpg)

Ci sono alcune soluzioni per chi ama il fai-da-te ma in questo caso si è preferito optare per una soluzione già pronta.

## Amplificatore audio

L'amplificatore [Adafruit MAX9744](https://www.adafruit.com/product/1752) è collegato all'uscita analogica di Arduino
Giga e consente la riproduzione di file audio, nel nostro caso annunci sonori della stazione ferroviaria.

![Immagine](/Documentazione/Immagini/AmplificatoreAdafruitMAX9744.jpg)

Ad esso sono collegati due altoparlanti 3 W 8 Ohm.

![Immagine](/Documentazione/Immagini/Altoparlanti.jpg)

### Problema ronzio Arduino Giga

Dopo il collegamento dell'amplificatore audio ad Arduino Giga, durante la riproduzione di file audio si sente in
sottofondo un ronzio molto fastidioso.

Per risolvere il problema è necessario inserire un condensatore elettrolitico da 100 µF tra il pin AREF (+ del
condensatore) e GND (- del condensatore) di Arduino Giga.

Per ulteriori informazioni è possibile visitare il thread aperto da me sul forum di
Arduino [qui](https://forum.arduino.cc/t/hum-noise-on-the-dac-of-arduino-giga/1188854)