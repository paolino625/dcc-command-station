# Scheda sensori

Questa scheda ospita tutte le schedine demultiplexer [SparkFun BOB-09056](https://www.mouser.it/ProductDetail/SparkFun/BOB-09056?qs=WyAARYrbSnY3lmN50Mm/iQ%3D%3D) alle quali sono collegati i vari sensori di posizione dislocati sul tracciato.

Come illustrato qui [capitolo](/Documentazione/3.%20Progetti/1.%20DCC%20Command%20Station/1.%20Hardware/Sensori/Sensori.md), si è scelto di usare i sensori IR in modalità analogica: pertando è necessario impiegare le schedine demultiplexer poiché Arduino Mega dispone solo di un numero limitato di ingressi analogici, insufficienti per le nostre esigenze.

![Immagine](/Documentazione/Immagini/SchedaSensori.png)

La scheda è dotata di 6 schedine demultiplexer, precisamente:
- 5 demultiplexer collegate agli input analogici di Arduino: dedicate ai sensori IR
- 1 demultiplexer collegata a un input digitale di Arduino: dedicata ai sensori Block Detector.

I cavi Ethernet in uso seguono la codifica T-568B:
![Immagine](/Documentazione/Immagini/CodificaCaviEthernetSensori.png)

Si è deciso di configurare gli ingressi analogici di Arduino, relativi ai canali dei multiplexer, come INPUT_PULLUP. In questo modo, se un sensore non è collegato, Arduino leggerà 5V, corrispondenti a un valore molto vicino a 1024. Questa impostazione facilita l’individuazione di guasti nei sensori o collegamenti errati. Al momento dell’avvio di ArduinoSlaveSensori, potrebbe essere generato un avviso se uno dei valori letti è 1024, indicando un problema di connessione o un sensore mancante.