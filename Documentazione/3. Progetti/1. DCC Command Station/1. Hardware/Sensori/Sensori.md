# Sensori

## Sensori IR

I sensori IR sono in grado di rilevare la presenza di un oggetto al di sopra di essi.

Si è scelto di utilizzare questi sensori IR che presentano sia l'uscita digitale che analogica.

![Immagine](/Documentazione/Immagini/SensoreIR.jpg)

### Modalità analogica

È possibile usare queste schede in due modalità:
- Modalità analogica: lettura del pin A0, valori da 0 a 1024.
- Modalità digitale: lettura del pin D0, HIGH quando il valore del sensore supera la soglia impostata con il trimmer.

Inizialmente, la scelta è ricaduta sulla modalità digitale per via del vasto numero di pin digitali disponibili nativamente sulle schede Arduino. Tuttavia, è emerso che questa soluzione presentava inconvenienti pratici:
- La regolazione del trimmer per ciascun sensore, una volta posizionato sul tracciato, risultava un'operazione complessa.
- La granularità del trimmer era approssimativa, rendendo difficile la precisa impostazione del valore soglia. In caso di necessità di piccoli aggiustamenti, la procedura risultava particolarmente complicata.

Di fronte a tali problematiche, è emersa la convenienza di passare all'utilizzo delle uscite analogiche. Per ovviare alla mancanza di input analogici sulle schede Arduino, vengono usate delle schede demultiplexer CD74HC4067.

Questa configurazione consente la lettura precisa dei valori di ciascun sensore (compresi tra 0 e 1024) e facilita l'impostazione delle soglie di attivazione/disattivazione dei sensori con dati precisi. Inoltre, consente di tarare i valori soglia per ciascun sensore senza la necessità di intervenire fisicamente sul trimmer presente su ciascun sensore.

### Modifica lunghezza sensori

Sono stati condotti test al fine di ottimizzare il posizionamento dei sensori IR. Dall'esito di tali prove, è emersa la decisione di posizionare i sensori IR a filo delle rotaie.

Purtroppo, i piedini dei sensori IR sulle schedine non erano sufficientemente lunghi per le nostre esigenze. Di conseguenza, è stato necessario eseguire l'operazione di dissaldatura dei singoli sensori, prolungarli e successivamente risaldarli sulle schedine interessate.

Per evitare di montarli al contrario, si consiglia di applicare una goccia di sbianchetto sul lato esterno del LED.

La dissaldatura di questi sensori IR si è rivelata una procedura particolarmente delicata, poiché il saldatore e la treccia dissaldante non hanno prodotto risultati soddisfacenti. È emerso che l'approccio più efficace consiste nell'impiego di un dissaldatore dotato di una punta da 1 mm. Si raccomanda di fissare la schedina su una tavola di legno, utilizzando il dissaldatore dalla parte superiore e piegare delicatamente il sensore da sotto per agevolarne l'estrazione.

Nonostante ciò, in diverse occasioni il sensore si è staccato durante il processo, lasciando però il buco riempito di stagno.

In tali situazioni, è stata adottata la soluzione di liberare il buco con uno spillo e l'uso del saldatore. Una volta parzialmente aperto il buco, è stato impiegato il Dremel con una punta da 0.9 mm per completare la pulizia dei fori dallo stagno. È essenziale prestare attenzione per evitare di danneggiare le piste durante questa operazione, poiché ciò potrebbe compromettere la possibilità di saldare un nuovo sensore IR.

Nel tentativo di risaldare nuovi sensori, si è utilizzata della fascetta termorestringente di spessore 1.2 mm della lunghezza della tavola + 2 mm (ad esempio, per una tavola di 15 mm, si prepara una fascetta termorestringente da 17 mm) al fine di mantenere la posizione a livello dei binari.

Nonostante la massima attenzione, è possibile che alcune piste vengano danneggiate durante l'uso del Dremel. Se si riscontra l'incapacità dello stagno di aderire al sensore IR o se questo non mantiene una posizione stabile, la scheda sensore IR deve essere scartata.

Per prolungare i sensori IR, è stata scelta l'opzione di utilizzare un filo rigido che potesse passare attraverso i fori della scheda. A tal scopo, è stato scelto il conduttore interno del cavo coassiale da 0.8 mm

![Immagine](/Documentazione/Immagini/CavoCoassiale.png)

Dopo aver preparato il sensore, è necessario effettuare un check per verificarne il corretto funzionamento. 
A tale scopo è possibile uploadare lo sketch LetturaSensoreIr.cpp presente in ProofOfConcept/sensoriIR (è sufficiente cambiare build_src_filter in platformio.ini, è tutto già pronto.)
Collegare il sensore sull'uscita A1 e verificare il corretto funzionamento controllando l'output del monitor seriale.

### Taratura

Il valore registrato dai sensori IR (varia da 0 a 1024) durante il transito di una locomotiva non è uniforme tra i diversi sensori, così come non è omogeneo il valore quando non è presente alcun oggetto al di sopra. Questa disparità è attribuibile a minime differenze tra i singoli sensori, come l'orientamento o la lunghezza delle gambe di ciascun sensore.

Dopo aver installato i sensori IR sul tracciato, è consigliabile condurre test sul campo e personalizzare la soglia che deve essere considerata come la condizione di attivazione (ON) per ciascun sensore.

## Sensori Block Detection

I sensori IR rappresentano una soluzione efficiente ed economica, garantendo precisione nella rilevazione. Tuttavia, in alcune circostanze, la loro applicazione risulta impraticabile. Nel nostro contesto, ad esempio, non è possibile impiegarli su alcune rampe a vista e su un ponte trasparente realizzato in plexiglass.

In queste situazioni, diventa essenziale isolare tali tratti e adottare sensori block detector in grado di identificare la presenza di rotabili su queste specifiche sezioni.

Esistono diverse soluzioni commerciali disponibili, che è possibile esaminare [qui](https://dccwiki.com/Occupancy_Detectors). Tuttavia, è importante notare che alcune di queste opzioni presentano costi elevati. A titolo di esempio, il Lenz LB101, capace di monitorare due sezioni, ha un prezzo che oscilla tra i 30-40 €.

Tuttavia, sono prodotti estremamente costosi: ad esempio il costo del  Lenz LB101, in grado di monitorare due sezioni, è di 30-40 €.

Di conseguenza, abbiamo scelto di adottare schede compatibili, in particolare la scheda nBlock Detector prodotta da MegaPoints Controllers. Questa scheda offre una soluzione affidabile a meno di 10 €, disponibile a questo [indirizzo](https://megapointscontrollers.co.uk/product/block-detector/?v=79cba1185463).

![Immagine](/Documentazione/Immagini/BlockDetector.png)