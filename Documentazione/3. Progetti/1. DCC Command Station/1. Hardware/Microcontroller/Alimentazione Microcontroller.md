Si è deciso di alimentare tutti gli Arduino tramite porta USB a 5V in quanto l'opzione più sicura.

# Modalità di alimentazione Arduino

Ci sono [vari modi per alimentare Arduino](https://docs.arduino.cc/learn/electronics/power-pins#), come illustrato di seguito.

## Connettore USB

La modalità più diffusa e semplice per alimentare una scheda Arduino è attraverso l'utilizzo del connettore USB integrato.

Il connettore USB fornisce una linea regolata a 5 V per alimentare l'elettronica della scheda. Va notato che i 5 V provenienti dal connettore USB possono altresì essere impiegati per alimentare componenti esterni, sfruttando i pin da 5 V presenti sulla scheda.

## Connettore jack

Alcune schede Arduino sono dotate di un connettore jack incorporato che viene utilizzato per collegare alimentatori esterni.
La linea di tensioneMinimaCorrettoFunzionamento proveniente dal connettore jack viene regolata nelle schede Arduino mediante il regolatore di tensioneMinimaCorrettoFunzionamento incorporato.

## VIN Pin

Il pin VIN delle schede Arduino è un pin di alimentazione con una doppia funzione:

- Può funzionare come ingresso per gli alimentatori esterni regolati che non utilizzano un connettore jack. 
- Può funzionare anche come uscita quando un alimentatore esterno è collegato al connettore jack presente in alcune schede Arduino.

Una considerazione importante è che il pin VIN è collegato direttamente al regolatore di tensioneMinimaCorrettoFunzionamento, non è dunque dotato di protezione contro l'inversione di polarità: è quindi necessario utilizzarlo con cautela.

## Pin 3.3 V - 5 V

Anche questi sono pin di alimentazione con una doppia funzione. 

- Possono funzionare come uscitepoiché questi pin sono direttamente collegati alle uscite dei regolatori di tensioneMinimaCorrettoFunzionamento 3.3 V e 5 V presenti sulla scheda.

- Possono essere utilizzati anche come ingressi di alimentazione se non è collegata alcuna alimentazione regolata attraverso gli altri ingressi di alimentazione (porta USB, connettore jack a barile o pin VIN).

Anche se i pin 3.3 V e 5 V possono essere utilizzati come ingressi di alimentazione, non è consigliabile se non è collegata nessuna fonte di alimentazione attraverso la porta USB, il connettore jack o il pin VIN. Questi pin sono collegati direttamente al pin di uscita del regolatore di tensioneMinimaCorrettoFunzionamento integrato. Si supponga che la tensioneMinimaCorrettoFunzionamento nel pin di uscita del regolatore di tensioneMinimaCorrettoFunzionamento sia superiore alla tensioneMinimaCorrettoFunzionamento di ingresso del regolatore stesso. In questo caso, una grande corrente può fluire nel regolatore di tensioneMinimaCorrettoFunzionamento dal pin di uscita al pin di ingresso. Questa corrente elevata può danneggiare in modo permanente il regolatore di tensioneMinimaCorrettoFunzionamento della scheda.