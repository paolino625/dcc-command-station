# Arduino Giga

## Introduzione

La scheda Arduino MKR Giga R1 è un modello a 32 bit con processore Arm® Cortex®-M7, che svela il potenziale dell'MCU
STM32H7 mantenendo il già noto fattore di forma Mega.

Il modello GIGA R1 Wi-Fi dispone di un processore dual-core (480MHz/240MHz), 76 GPIO, caratteristiche avanzate ADC/DAC e
connettori per fotocamera/display. Una ricca interfaccia USB supporta HID tramite USB-C e USBHost (tastiera, memoria di
massa) tramite un connettore USB-A dedicato.
Un modulo radio Murata 1DX offre comunicazioni Wi-Fi e BLUETOOTH.

![Immagine](/Documentazione/Immagini/ArduinoGiga.jpg)

## Cosa fa nel nostro progetto

Arduino Giga rappresenta il nucleo essenziale del nostro progetto, infatti da esso dipendono molte funzioni. Ecco alcune
di esse:

- Crea il segnale DCC che viene trasmesso alla Motor Shield, in modo tale da creare un segnale bipolare DCC valido.
- Gestisce l'interazione con l'utente:
    - Controlla tastierino numerico, encoder e display.
    - Fa girare un web server, grazie al quale l'utente può interagire con il sistema tramite un sito web.
- Gestisce il salvataggio di variabili non volatili (stato di locomotiva, convogli, vagoniReali e scambi) su una USB (memoria
  non volatile).
- Il Real Time Clock integrato permette al sistema di mantenere la sincronia con il tempo reale, offrendo la possibilità
  di eseguire azioni programmate in modo preciso o di simulare variazioni di luciAccese durante il giorno all'interno
  del plastico.

## Perché Arduino Giga

I motivi per i quali è stato scelto un Arduino Giga piuttosto che un Arduino Mega sono le seguenti:

- Velocità di elaborazione superiore: sebbene tale velocità non sia apprezzabile durante operazioni basilari, si
  apprezza nella gestione di un web server, operazione esosa di risorse.

- Connettività Wi-Fi integrata: nel caso di Arduino Mega, se si volesse implementare un web server, sarebbe necessario
  integrare un altro microcontroller con capacità Wi-Fi, come ad esempio un ESP32. Questo comporterebbe una
  comunicazione costante tra i due microcontroller per mantenere aggiornate le variabili di sistema, aumentando la
  complessità del codice e la probabilità di errori.

- Modulo DAC integrato: Arduino Giga include un convertitore digitale-analogico (DAC), utile per inviare segnali audio a
  un altoparlante tramite un amplificatore esterno.

- Memoria flash e RAM più ampie: Con una memoria flash di 2 MB e una RAM di 1 MB, Arduino Giga supera notevolmente i 256
  KB di memoria flash e gli 8 KB di RAM di Arduino Mega, garantendo spazio sufficiente anche per software più complessi,
  come il nostro.

## Tensione di lavoro

Una caratteristica significativa che distingue Arduino Giga da Arduino Mega è la tensioneMinimaCorrettoFunzionamento di lavoro di 3.3 V anziché la
comune tensioneMinimaCorrettoFunzionamento di 5 V. Pertanto, è essenziale l'impiego di convertitori di livello logico (Logic Level Converter) dove
necessario per garantire una corretta interfacciabilità e proteggere i componenti sensibili alle differenze di tensioneMinimaCorrettoFunzionamento
