# Arduino Mega

## Introduzione

L'Arduino Mega 2560 è una scheda microcontroller basata sull'ATmega2560.

Dispone di 54 pin di input/output digitali (di cui 14 possono essere utilizzati come uscite PWM), 16 ingressi analogici, 4 UART (porte seriali hardware), un oscillatore al quarzo da 16 MHz, una connessione USB, un connettore di alimentazione, un'intestazione ICSP e un pulsante di reset.

![Immagine](/Documentazione/Immagini/ArduinoMega.jpg)

## Cosa fa nel nostro progetto

Grazie ai suoi numerosi pin e al fatto che lavora ad una tensioneMinimaCorrettoFunzionamento di 5 V è adatto per eseguire numerose funzioni:
- Pilotare i relay 5 V della scheda multiplexer.
- Leggere lo stato dei vari sensori di posizione.
- Pilotare i LED 5 V indirizzabili.

## Tensione di lavoro

La tensioneMinimaCorrettoFunzionamento di lavoro è di 5 V.