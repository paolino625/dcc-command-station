## Striscia LED

Per illuminare il plastico vengono utilizzate più striscie LED WS2812B.

![Immagine](/Documentazione/Immagini/WS2812B1.jpg)

![Immagine](/Documentazione/Immagini/WS2812B2.jpg)

Una [guida](https://www.linkedin.com/pulse/choosing-best-addressable-led-strip-ultimate-guide-maisie-ou/) utile per la scelta della tipologia della striscia LED.

