- [Connettori circolari per scambi](#connettori-circolari-per-scambi)
  - [Pezzi necessari](#pezzi-necessari)
- [Accoppiatori Ethernet RJ45](#accoppiatori-ethernet-rj45)
  - [Pezzi necessari](#pezzi-necessari-1)

Alcune tavole del plastico devono poter essere smontabili. Di conseguenza si è pensato ad un metodo intelligente per poter scollegare i fili degli scambi e dei sensori in facilità.

## Connettori circolari per scambi

Per gli scambi si era pensato di utilizzare dei connettori circolari: tuttavia i contatti di questi connettori sono piccoli e riescono a portare una corrente di 1 - 2 A, non sufficiente per l'impulso che deve arrivare agli scambi.

Si è deciso dunque di utilizzare dei connettori maschio/femmina di potenza come [questo](https://www.te.com/usa-en/product-1-770178-1.html): economici e funzionali.

Ognuno di questi ha 6 contatti, utilizzati per il comando di 3 scambi (il ground va a parte).

### Pezzi necessari

- StazioniAbstract panoramica: 3 connettori.
- Stazione Estrema EST 1° piano: 2 connettore
- Stazione Estrema EST 2° piano: 3 connettori
- 2° Piano: 1 connettore
- Tavola triangolare 2° piano: 2 connettori
- Tavola sopraelevata: 4 connettori

TOTALE: 15 connettori

## Accoppiatori Ethernet RJ45

Per i sensori di posizione, dato che questi viaggiano su cavo ethernet, è sufficiente utilizzare degli accoppiatori ethernet RJ45.

### Pezzi necessari

- StazioniAbstract panoramica: 2 connettore
- Stazione Estrema EST 1° piano: 1 connettore
- Stazione Estrema EST 2° piano: 1 connettore
- 2° Piano: 1 connettore
- Tavola triangolare 2° piano: 1 connettore
- Tavola sopraelevata: 1 connettore