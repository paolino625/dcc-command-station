- [Cavo citofonico](#cavo-citofonico)
- [Cavo ethernet](#cavo-ethernet)
- [Cavo schermato telefonico](#cavo-schermato-telefonico)
- [Cavo alimentazione tracciato](#cavo-alimentazione-tracciato)
- [Cavo flatcable](#cavo-flatcable)

## Cavo citofonico

Diametro: 5 mm
Numero cavi: 6
Sezione cavi: 0.5 mmq

In uso per gli scambi in quanto questi ultimi assorbono una corrente elevata (almeno 3 A).
Con un solo cavo alimentiamo 3 scambi (2 cavi che portano il + per scambio, il ground viene portato in maniera separata)

## Cavo ethernet

Diametro: 4 mm
Numero cavi: 8
Sezione cavi: 0.2 mmq

Non è possibile utilizzarlo per gli scambi in quanto i fili presentano una sezione troppo piccola.
Lo utilizziamo per i sensori.

Gestiamo 6 sensori con un singolo cavo:
- 2 fili per alimentazione sensori.
- 6 fili per trasmissione dati sensori.

Come cavo è consigliabile usare cavo CAT5 che risulta essere più flessibile rispetto al cavo CAT6, oltre che decisamente meno costoso. 

## Cavo schermato telefonico

Diametro: 3 mm
Numero cavi: 3
Sezione cavi: 0.2 mmq

In uso per l'ultimo miglio di scambi e sensori (da scambio/sensore a morsetto più vicino)

## Cavo alimentazione tracciato

Diametro: 2.7 mm per cavo
Numero cavi: 2
Sezione cavi: 1 mm circa

Sezione così grande serve per evitare cadute di tensione su lunghe distanze.

## Cavo flatcable

[Flatcable](https://www.mouser.it/ProductDetail/3M-Electronic-Solutions-Division/3302-20-CUT-LENGTH?qs=fXuaQIb4rWZldCNlXLnEhw%3D%3D)

