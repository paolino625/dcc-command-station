# Scheda logica converter

Questa scheda ospita tutti i logic level converter necessari per il nostro schema elettrico.

![Immagine](/Documentazione/Immagini/SchedaLogicLevelConverter.jpg)

I logic level converter sono utili per collegare componenti che lavorano a tensioni differenti senza rischiare di danneggiarli.
Ad esempio utile nel caso in cui si voglia far comunicare Arduino Giga che lavora a 3.3 V con Arduino Mega che lavora a 5 V.

### Spark Fun BOB-12009 - Connessioni I2C

Ho avuto molti problemi con le schede
[Adafruit 757](https://www.mouser.it/ProductDetail/Adafruit/757?qs=GURawfaeGuCFbCY6Rr8Yew%3D%3D)
Nonostante le saldature fatte bene, tutte le schede (5 e più) dopo un breve periodo di funzionamento corretto non hanno più funzionato correttamente.

Ho dunque provato le schede [Spark Fun BOB-12009](https://www.sparkfun.com/products/12009) e non ho mai avuto problemi fino ad ora.

Questi Logic Level Converter sono particolarmente adatti per connessioni I2C.

### Texas Instruments SN74LVC245AN - Connessioni porte seriali

Per connessioni seriali tra i vari Arduino vengono utilizzati moduli SN74LVC245AN.

In precedenza, ho provato a utilizzare le schede [Adafruit 395](https://www.mouser.it/ProductDetail/Adafruit/395?qs=GURawfaeGuC1aDMKJdj9pQ%3D%3D) ma hanno dato numerosi problemi (connessione non stabile).
Da notare che queste schede consentono una comunicazione bidirezionale, il che aggiunge una complessità non necessaria nel nostro caso: infatti, la comunicazione seriale non è bidirezionale dato che viene utilizzato un pin per l'invio e un pin per la ricezione.