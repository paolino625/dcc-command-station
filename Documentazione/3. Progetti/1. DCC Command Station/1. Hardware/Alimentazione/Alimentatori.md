- [Alimentatore 24 V 4 A - CDU](#alimentatore-24-v-4-a---cdu)
- [Alimentatore 15 V 10 A - Tracciato](#alimentatore-15-v-10-a---tracciato)
- [Alimentatore 12 V 1.3 A - Relay ed interruttori](#alimentatore-12-v-13-a---relay-ed-interruttori)
- [Alimentatore 5 V 14 A - Microcontroller e componenti ausiliari](#alimentatore-5-v-14-a---microcontroller-e-componenti-ausiliari)
- [Alimentatore 5 V - 25 A - Strisce LED](#alimentatore-5-v---25-a---strisce-led)

## Alimentatore 24 V 4 A - CDU

Alimenta la CDU la quale, ricordiamo, fornisce un impulso per azionare gli scambi.

## Alimentatore 15 V 10 A - Tracciato

Alimenta la Motor Shield, alla quale sono collegati diversi binari del tracciato dislocati in vari punti. Questa distribuzione è necessaria poiché alimentare un tracciato esteso da un solo punto potrebbe non essere sufficiente.

È stata effettuata la scelta di un alimentatore da 10 A in quanto è la massima corrente supportata dalla Motor Shield in uso. Crediamo che questa corrente sia più che sufficiente tenendo conto che una locomotiva ad una velocità moderata consuma approssimativamente 200 mA.

## Alimentatore 12 V 1.3 A - Relay ed interruttori

Alimenta i LED degli interruttori push push luminosi posti sul quadro di controllo e i relay di controllo posti sulla scheda di alimentazione.

## Alimentatore 5 V 14 A - Microcontroller e componenti ausiliari

Alimenta i microcontroller, i sensori di posizione sul tracciato (sensori IR e Block Detector), i componenti ausiliari a 5 V (come ad esempio il display e gli altorparlanti) e i componenti ausiliari a 3 V (come ad esempio gli encoder e il keypad).

## Alimentatore 5 V - 25 A - Strisce LED

Alimenta le strisce LED.
