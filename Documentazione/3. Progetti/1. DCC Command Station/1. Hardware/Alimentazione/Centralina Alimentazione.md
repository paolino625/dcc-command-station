
- [Centralina Sinistra Alimentazioni - 8 Moduli](#centralina-sinistra-alimentazioni---8-moduli)
- [Centralina Destra LED - 8 Moduli](#centralina-destra-led---8-moduli)

# Centralina Sinistra Alimentazioni - 8 Moduli

- Interruttore magnetotermico 6 A - Alimentatori 5 V Arduino, 12 V, 15 V, 24 V - 4 Cavi da 0.75 mmq
- Interruttore magnetotermico 6 A - Alimentatori 5 V LED - 3 Cavi da 0.75 mmq
- Interruttore magnetotermico 6 A - Alimentatore Switch per Telecamere - 1 Cavo da 0.75 mmq
- Interruttore magnetotermico 6 A - MiniPC e Schermo - 2 Cavi da 0.75 mmq
- 4 posti liberi per eventuali espansioni

Fori di alta tensioneMinimaCorrettoFunzionamento:
- 1 AlimentazioneAbstract input
- 1 Alimentatore 5 V
- 1 Alimentatore 12 V
- 1 Alimentatore 15 V
- 1 Alimentatore 24 V
- 3 AlimentazioneAbstract 5 V LED 
- 1 Alimentatore switch
- 2 Alimentatori Mini PC e schermo

14 fori (11 utilizzati)

# Centralina Destra LED - 8 Moduli

- Shelly PM PRO 4 (3 moduli)
  - Arduino ecc. (24 V, 15 V, 12 V e 5 V)
  - LED
  - Switch POE (Per Telecamere)
  - Schermo e mini PC
- Porta Fusibile Striscia LED 1
- Porta Fusibile Striscia LED 2
- Porta Fusibile Striscia LED 3
- Porta Fusibile Striscia LED 4
- Porta Fusibile Striscia LED 5
- Porta Fusibile Striscia LED 6
- Porta Fusibile Mini PC e schermo
- 2 Modulo libero

In questa scheda fori solo di bassa tensione:
- 9 per strisce LED (cavi da 1.5 mmq)
- 1 per cavo citofonico per segnali di pilotaggio strisce LED
- 1 per cavo citofonico per segnale per sensori di tensioneMinimaCorrettoFunzionamento
- 1 per cavo ethernet per Shelly
- 2 extra

Totale:
- 10 grandi (9 utilizzati)
- 4 piccoli (3 utilizzati)