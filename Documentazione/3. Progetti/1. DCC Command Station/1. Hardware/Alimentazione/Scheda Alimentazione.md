- [Introduzione](#introduzione)
- [Componenti](#componenti)
    - [Regolatore di tensioneMinimaCorrettoFunzionamento](#regolatore-di-tensioneMinimaCorrettoFunzionamento)
    - [Sensore di tensioneMinimaCorrettoFunzionamento](#sensore-di-tensioneMinimaCorrettoFunzionamento)
    - [Portafusibili e fusibili](#portafusibili-e-fusibili)
    - [Relay](#relay)
        - [Mapping relay fritzing](#mapping-relay-fritzing)
    - [Step up](#step-up)
    - [Sensori di corrente](#sensori-di-corrente)
        - [INA 219](#ina-219)
        - [INA 260](#ina-260)

# Introduzione

![Immagine](/Documentazione/Immagini/SchedaAlimentazione.jpg)

Questa scheda funge da punto di ricezione per la corrente proveniente da vari alimentatori, distribuendola attraverso
morsetti di uscita destinati ai differenti componenti del sistema.

Di seguito una breve descrizione della scheda, per approfondimenti si consiglia di consultare lo schema del circuito.

Ad ogni fascia corrisponde una determinata tensioneMinimaCorrettoFunzionamento e ruolo, a partire da sinistra:

- 1° fascia: 12 V per relay controllo (presenti su questa stessa scheda) e LED interruttori push-push
- 2° Fascia: 5 V per strisce LED
- 3° Fascia: 24 V per CDU
- 4° Fascia: 15 V per Motor Shield
- 5° Fascia: 5 V sensori di posizione
- 6° Fascia: 5 V microcontroller
- 7° Fascia: Ingresso 5 V
- 8° Fascia: Componenti ausiliari a 5 V
- 9° Fascia: Componenti ausiliari a 3.3 V

# Componenti

## Regolatore di tensioneMinimaCorrettoFunzionamento

Il regolatore di tensioneMinimaCorrettoFunzionamento è responsabile di convertire l'ingresso di tensioneMinimaCorrettoFunzionamento a 5 V sulla scheda in una tensioneMinimaCorrettoFunzionamento di 3.3
V, tensioneMinimaCorrettoFunzionamento richiesta da alcuni componenti ausiliari.

## Sensore di tensioneMinimaCorrettoFunzionamento

Sensori di tensioneMinimaCorrettoFunzionamento sono utilizzati per fornire a Arduino un feedback dagli alimentatori, permettendogli di monitorare
la tensioneMinimaCorrettoFunzionamento erogata. Questo sistema consente al sistema di identificare tempestivamente eventuali anomalie e di
segnalarle.

## Portafusibili e fusibili

Sono presenti dei portafusibili e relativi fusibili per interrompere il flusso di corrente in caso di corto circuiti.
La dimensione di ciascun fusibile è calibrata in relazione al consumo stimato dei componenti che esso protegge.

## Relay

I relay di controllo sono collegati agli interruttori push-push, che consentono di interrompere il flusso dei vari
alimentatori utilizzati dal sistema. Questa configurazione evita che la corrente di carico attraversi direttamente gli
interruttori, i quali possono gestire al massimo 3 A, una capacità spesso insufficiente nella maggior parte dei casi.

### Mapping relay fritzing

Nell'immagine di seguito è possibile visionare:

- A sinistra lo schema della componente relay utilizzata su Fritzing.
- A destra lo schema del relay reale.

![Immagine](/Documentazione/Immagini/Mapping%20tra%20Relay%20Fritizing%20e%20Relay%20reale.jpg)

## Step up

La settima fascia ospita un convertitore dedicato a trasformare i 5 V in 12 V. Questa conversione è fondamentale per
alimentare il LED associato all'interruttore push push 5 V, garantendo l'luciAccese solamente quando l'alimentatore è
effettivamente in funzione.

## Sensori di corrente

### INA 219

Viene utilizzato l'INA219 insieme ad alcune resistenze per misurare l'assorbimento dei rotabili presenti sul tracciato.
Questo approccio risulta molto utile, non solo per ottenere un'idea dell'assorbimento delle locomotive, ma anche per
rilevare eventuali cortocircuiti sul tracciato. Per ulteriori dettagli su come viene rilevato un corto circuito
visionare questo file [RilevamentoCortoCircuitoTracciato](./RilevamentoCortoCircuitoTracciato.md)

Per consentire a INA219 di dissipare calore fino a 10 A anziché limitarsi a 3.2 A, è necessario aggiungere due
resistenze da 100 mOhm ciascuna alla resistenza (o chiamato anche shunt resistor) R100 già presente sulla scheda.

### INA 260

L'INA260 è stato adottato per i 5 V in quanto supporta correnti fino a 15 A senza richiedere modifiche alla resistenza
shunt.
Diversamente dall'INA219, l'INA260 combina il ground misurato con il ground della scheda.
Per questo motivo non viene utilizzato anche per misurare la corrente del tracciato a 15 V: per evitare di combinare
ground con tensioni differenti.