- [Arduino Mega](#arduino-mega)
- [Motor Shield](#motor-shield)
- [Relay](#relay)
- [Sensore IR](#sensore-ir)
- [Striscia LED WS2812B](#striscia-led-ws2812b)
- [Striscia LED vagone](#striscia-led-vagone)

## Altoparlanti

Un altoparlante è 3 Watt.
Corrente (A) = 3 W / 5 V = 0.6 A = 600 mA

## Arduino Mega

Solo scheda senza nulla collegato: 50 mA circa

## Display

50 mA circa

## Motor Shield

A vuoto: 100 mA

## Relay

- Relay 5 V 30 A: 280 mA
- Relay 12 V Finder: 40 mA
- Relay 12 V Omron: 44 mA

## Sensore IR

20 mA circa (15-25 mA)

## Striscia LED WS2812B

- 1 LED: 0.3 Watt massimi. Ovvero 60 mA.

## Striscia LED vagone

- Striscia LED vagone: 40 mA

