Lo Shelly 4 PRO PM è utilizzato per il controllo remoto delle alimentazioni del plastico, consentendo di accendere e spegnere le diverse uscite:
- 1ª Uscita: fornisce corrente alla scheda di alimentazione principale. In caso di problemi con Arduino, è così possibile riavviare l'intero sistema da remoto.
- 2ª Uscita: fornisce corrente alle strisce LED.
- 3ª Uscita: fornisce corrente allo switch POE, al quale sono collegate tutte le telecamere del plastico.
- 4° Uscita: attualmente non in uso.

Dai test effettuati, è emerso che lo Shelly non riesce a stabilire una connessione con il router quando è collegato tramite cavo Ethernet. Per ovviare a questo problema, è stato deciso di utilizzare la connessione Wi-Fi, con la speranza che un futuro aggiornamento del firmware risolva questa limitazione. Per il collegamento Wi-Fi, si consiglia di configurare un IP statico nell’interfaccia dello Shelly, in quanto l’utilizzo di un IP dinamico potrebbe causare problematiche di connessione.