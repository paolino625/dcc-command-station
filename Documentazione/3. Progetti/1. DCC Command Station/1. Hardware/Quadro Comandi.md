# Quadro comandi

Questi oggetti offrono all'utente un'interazione basilare con il sistema. La modalità preferenziale per interagire con
il sistema è attraverso il sito web, che fornisce tutte le funzionalità.
L'utilizzo diretto di questi oggetti è riservato principalmente a scopi di manutenzione.

![Immagine](/Documentazione/Immagini/QuadroComandi.jpg)

## Input

### Keypad

Il keypad viene utilizzato dall'utente per interagire con il sistema.

![Immagine](/Documentazione/Immagini/Keypad.jpg)

### Encoder

Gli encoder vengono usati nella seguente maniera:

- Ruotando l'encoder a destra o sinsitra è possibile modificare la velocità del convoglio selezionato.
- Premendo l'encoder per meno di 1 secondo è possibile cambiare tra le varie modalità:
    - Passo breve: uno step del decoder corrisponde ad aumentare/diminuire 1 step di velocità.
    - Passo lungo: uno step del coder corrisponde ad aumentare/diminuire 10 step di velocità.
- Premendo l'encoder per più di 1 secondo, viene cambiata la direzione del convoglio selezionato.

![Immagine](/Documentazione/Immagini/Encoder.jpg)

### Pulsanti Emergenza

Se viene premuto un pulsante di emergenza, un segnale di STOP di emergenza viene inviato sul tracciato: i decoder che lo
ricevono fermano immediatamente le locomotive senza seguire l'accelerazione e decelerazione impostate.

I pulsanti scelti presentano un diametro di 22 mm.

![Immagine](/Documentazione/Immagini/PulsanteEmergenza.jpg)

## Output

### Display

Il display 20x4 è impiegato per visualizzare in modo chiaro lo stato dei convogli e consentire all'utebnte l'interazione
con tutte le funzionalità offerte dal sistema.

![Immagine](/Documentazione/Immagini/Display.jpg)