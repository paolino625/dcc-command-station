# Scelta

Si è deciso di evitare di comprare convogli a composizione bloccata, questo per vari motivi
- Costo elevato
- Solo una delle due locomotive è funzionante, l'altra è dummy. Questo è un problema in quanto per superare le pendenze del plastico si è deciso di utilizzare la doppia trazione.
- Saremmo vincolati all'uso dei connettori tra vagoniReali forniti dal produttore, che spesso lasciano molto desiderare (si rovinano/staccano facilmente).