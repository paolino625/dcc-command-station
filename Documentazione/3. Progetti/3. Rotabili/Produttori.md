# Produttori

Ci sono diversi produttori di locomotive e vagoniReali.

## Locomotive

## Vitrains

- Buona qualità.
- Occhio al numero di carrelli motorizzati.

### Rivarossi

- Alta qualità.
- Facile trovare componenti di ricambio.

## Vagoni

### Rivarossi

- Produce un numero limitato di ogni articolo.
Non tiene conto del mercato: ad esempio produce lo stesso numero di carrozze di 1a classe rispetto a quelle di 2a classe (il che ha poco senso).

### Vitrains

- Buona qualità
- Spesso con strisce LED integrate.

### ACME

- Alta qualità.
- Molto costoso. 
- Difficile trovare pezzi di ricambio.

### Lima Expert

- Qualità medio-bassa. 
- Considerare che vagoniReali non vengono con striscia LED (mentre ad esempio molti di Vitrains sì).

## Piko

Media qualità.

# Note

Per carri merci si consigliano produttori Joeuf, Piko e Trix. Dovrebbero essere un buon rapporto qualità/prezzo.