# Ganci

I ganci ad occhiello standard non sono considerati affidabili, poiché causano frequentemente il deragliamento dei treni.  
Ho condotto una serie di test su convogli di lunghezza considerevole (2 metri) in doppia trazione, mettendo a confronto i ganci universali Roco 40395 con i ganci magnetici Almrose.

Ho osservato che, quando l'intero convoglio è equipaggiato con ganci universali, i vagoniReali tendono a deragliare in presenza di una leggera differenza di velocità tra le due locomotive. 
Al contrario, l'uso di ganci magnetici Almrose tra i vagoniReali ha eliminato i problemi di deragliamento, rendendoli la scelta vincente.

L'unico problema è che non è possibile usare questi ganci magnetici per collegare le locomotive ai vagoniReali. Questi ganci richiedono che entrambi i rotabili siano dotati di un timone di allontanamento, elemento che spesso manca nelle locomotive.

Riassumendo, la soluzione perfetta consiste nel collegare i vagoniReali con i ganci magnetici, mentre per il collegamento tra le locomotive e i vagoniReali si opta per l'uso di ganci universali. 
Nel caso in cui sia necessario illuminare il convoglio, si utilizza un gancio standard per una locomotiva e un gancio con conduzione elettrica per l'altra.