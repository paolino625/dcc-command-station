# Pulizia Binari

- È possibile utilizzare le gomme apposite clear binario.
Queste gomme sono fatte per avere il giusto grado di abrasione per eliminare solo l'ossido e senza creare graffi alla superficie di rotolamento.

- In alternativa, è possibile far girare periodicamente vagoniReali clear binari che hanno queste gomme al di sotto.

- È possibile utilizzare anche benzina Avio con cottofionc.

# Pulizia Locomotive

## Ruote e lamelle prendicorrente

Utilizzare avio con cottofionc o spazzolino.

## Lubrificazione

È necessario lubrificare le fessure delle viti senza fine e gli ingranaggi sugli assi delle locomotive ogni 40-100 ore di utilizzo o comunque ogni anno.

Utilizzare dell'olio per macchina da cucire (come ad esempio olio Singer).

L'olio eccedente sarà rigettato al momento del processo, è raccomandata l'utilizzazione per quest’operazione di un banco di prova a rulli, per evitare di vedere l'olio che si sparge su tutto il plastico. Questo film lubrificante è una delle cause più frequentate di disfunzioni sui plastici.

Nota bene: inizialmente ho provato il grasso Roco 10905, specifico per il modellismo.
Tuttavia le locomotive E444 della Rivarossi hanno iniziato ad avere problemi di circolazione: si verificano degli scatti notevoli. Una volta sostituito questo grasso con olio da cucire Singer i problemi non si sono più presentati.

## Cerchiature di aderenza

Le cerchiature d'aderenza non possono essere montate che su delle ruote speciali munite di un solco fresato. 
Quando queste ruote sono utilizzate senza cerchiature d'aderenza, hanno in generale una capacità di circolazione molto cattiva. 
Al contrario, è molto difficile montare una cerchiatura d'aderenza su una ruota che non è prevista per ciò.

Come per i pneumatici, le piccole corone di plastica si usano e con un funzionamento normale si assottigliano, fino alla rottura.

Messa da parte l’usura di sfruttamento, le cerchiature d’aderenza sono ugualmente soggette ad un invecchiamento normale. 
Con il passare del tempo, esse perdono la loro elasticità e diventano fragili. 
Un cambiamento necessario può essere riconosciuto prima della rottura propriamente detta. La forza di trazione diminuisce allora sensibilmente.

Allorchè un gruppo le privilegia come nell’esempio delle ruote puramente metalliche, altri modellisti non hanno mai abbastanza cerchiature d'aderenza. Queste aumentano la forza di trazione del modello. Le ruote così equipaggiate non possono stabilire che difficilmente un contatto elettrico con le rotaie, Marklin cerca di trovare per i rispettivi modelli un compromesso ottimale per queste differenti esigenze di sfruttamento.