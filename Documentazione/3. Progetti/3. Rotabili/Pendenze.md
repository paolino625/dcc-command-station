# Pendenze

## Convoglio 2 Metri con Locomotive Rivarossi

Due anelli di aderenza per locomotiva.

- Fino a 14 % esito positivo.
- 15 %: slittano le ruote

## Minuetto

- Senza anelli di aderenza: neanche 5 % davanti
- Con 2 anelli di aderenza su un solo asse: 5% avanti si, retromarcia no
- Con 2 anelli di aderenza su assi differenti: 7 % sì avanti. Retromarcia con difficoltà.