# Il problema

- Locomotive che non viaggiano in maniera fluida a basse velocità
- Locomotive potrebbero interrompersi completamente
- Luci delle carrozze potrebbero fare sfarfallio.
- Problemi di sound decoder

Ciò comporta in una pulizia del tracciato e ruote che non finisce mai.

## Cause

Capiamo le ragioni perché questo accade:

- Tracciato sporco
- Ruote sporche
- Tracciato irregolare
- Scambi e incroci
- Pochi punti di corrente della locomotiva:

![Immagine](/Documentazione/Immagini/KeepAlive1.png)

Se osserviamo una ruota in dettaglio, diventa più ovvio il motivo per cui c'è un problema. Ogni ruota ha un contatto
minimo con il tracciato e non ogni ruota ha le lamelle prendicorrente.
Se c'è qualsiasi problema con il tracciato, perdiamo la connettività.

La soluzione? Una sorta di accumulatore di energia o "stayAlive".
Questo consente di immagazzinare un po' di energia dentro la locomotiva o vagone.

Durante "track glitches" questa energia consente la locomotiva di continuare il suo movimento e previene il flickering
delle luci dei vagoniReali, prevenie che qualsiasi suono di decoder si stoppi.

# Soluzioni

Ci sono sia soluzioni commerciali che fai-da-te.

Per risolvere il problema dobbiamo:

- Fornire corrente
- Regolare la tensioneMinimaCorrettoFunzionamento
- Mantenere la tensioneMinimaCorrettoFunzionamento

Con circuiti stayAlive intendiamo circuiti che forniscono una soluzione a questi problemi. Le soluzioni fai-da-te
potrebbero invalidare la garanzia di decoder DCC.

Qui abbiamo un circuito basilare stay alive circuit.

![Immagine](/Documentazione/Immagini/KeepAlive2.png)

Abbiamo il condensatore che funziona come batteria ricaricabile.
La resistenza limita il flusso di corrente che potrebbe essere vista come un corto circuito dal controller DCC.

Il diodo funziona come low resistance discharge pathUsb.

# Note

I circuiti stay alive non sono da intendere come un sostituto di manutenzione del tracciato e delle locomotive.

Cerca di scegliere un condensatore che possa garantire un secondo circa di storage. Una ragione di ciò è che un tempo
esagerato potrebbe comportare il passaggio della locomotiva su segnali rossi, dato che non può ricevere segnali da un
tracciato difettoso.

Il tempo stay alive dipende dal motore. Motori a bassa corrente riusciranno a funzionare per più tempo.

# Valori condensatore

I controller DCC forniscono una tensioneMinimaCorrettoFunzionamento DC di circa 12-15 V (dopo la rettificazione).

La tensioneMinimaCorrettoFunzionamento del condensatore deve essere maggiore della tensioneMinimaCorrettoFunzionamento
attesa DC.

# Connettere Stay Alive

![Immagine](/Documentazione/Immagini/KeepAlive3.png)

Abbiamo lo schema di un decoder semplice DCC non sound.
I colori sono standard:

- nero per binario sinistra
- rosso per binario destra
- arancio e grigio per il motore
- blu è il positivo comune

Quindi dove lo connettiamo?

Se vediamo un generico decoder DCC, vediamo che tutti hanno un blu positivo comune e ground, dobbiamo identificare
questi e attaccare lo stay alive conformemente.

![Immagine](/Documentazione/Immagini/KeepAlive4.png)

Qui un esempio du un decoder Zimo, fornisce due pin specifici marcati con blu e grigio, per attaccare un condensatore
stay alive.
Non tutti i decoder hanno i componenti addizionali (diodo e resistenza) e quindi un circuito separato è necessario.

# Super condensatori

I supercodensatori possono mantenere una carica 10-100 volte rispetto a condensatori simili elettrolitici, ma ad una
tensioneMinimaCorrettoFunzionamento minore, il che li rende perfetti per applicazioni del modellismo ferroviario.
Non sono economici ma fanno un buon lavoro.

# Summary

- Stay alive migliorano la maggior parte di decoder DCC.
- Controlla il manuale del decoder per la compatibilità stay alive.
- Stay alive è fortemente dipendente dalla tipologia del motore
- I super condensatori forniscono la carica più grande nel pacchetto più piccolo
- Attento alle condizioni di garanzia del decoder

# DIY

![Immagine](/Documentazione/Immagini/KeepAlive5.png)
![Immagine](/Documentazione/Immagini/KeepAlive6.png)

Questo è uno schema che schermataPrincipale i vari componenti che sono al centro di un keepalive.

Il ground a sinistra è connesso al ground del decoder, in alcuni decoder questo pin è fornito (Filo nero).

Il cavo + a destra per completare il circuito è il cavo blu, per completare le connessioni sul decoder.

5 super condensatori in serie, ognuno di 3 V, quindi sono 15 V in totale. I super condensatori sono un tipo speciale di
condensatori che è in grado di conservare tanti elettroni e rilasciarli velocemente.

Poi il flusso procede tramite la resistenza, perché?
C'è qualcosa chiamato inverse current, quando l'alimentazione viene data ai binari, i condensatori cercano di caricarsi
al massimo della velocità. Il controller DCC potrebbe interpretare questo grande flusso di corrente come un corto
circuito e potrebbero spegnersi.
Per evitare ciò, la resistenza serve per rallentare questo processo inizialmente. In questo modo i dispositivi si
caricano ma senza sembrare un corto circuito al controller DCC.

Il diodo IN4001 è 1 A, va bene per scala H0.
Diodo zener lavora in maniera simile ad un normale diodo: gli elettroni che provengono da sinsitra non riescono ad
attraversarlo, perché appunto vengono bloccati.
Se passano invece per i condensatori vengono bloccati dall'altro diodo IN4001. Dunque gli elettroni sono forzati a
passare attraverso la resistenza e non possono circumnavigarla.

Se c'è un'interruzione di corrente allora il pathUsb può essere da destra verso i supercondensatori senza passare dalla
resistenza, in modo tale da avere tensioneMinimaCorrettoFunzionamento massima.
A questo serve il diodo IN4001, per forzare la corrente a passare attraverso la resistenza durante la carica e ad
evitarla durante la discarica.

A cosa serve il diodo Zener allora? Evita che elettroni possano venire da sinistra, e li forza a passare attraverso i
condensatori.
Quando la tensioneMinimaCorrettoFunzionamento supera i 15 V, che è di più di quello che i super condensatori accettano,
questo diodo zener permette
agli elettroni di passare attraverso di esso e vanno a finire nella resistenza, quindi elettricità viene dissipata come
calore.
Per quello avere una resistenza da 1 W è molto importante: per poter dissipare il calore senza scoppiare. Se la
tensioneMinimaCorrettoFunzionamento
si alza, ad esempio fino a 18 V, potrebbe aver senso aumentare questo valore della resistenza.
È importante perché funzione come da regolatore di tensioneMinimaCorrettoFunzionamento: è molto importante per evitare
che i condensatori scoppino
al passaggio di una corrente superiore ai 15 V.

Non dovrebbe essere di preoccupazione il fatto che il diodo zener siano a 15 V, stessa cosa dei supercondensatori,
perché la maggior parte dei controller DCC erogano sul tracciato una tensioneMinimaCorrettoFunzionamento minore di 15
V (14 V o meno)
Inoltre, una volta che gli elettroni attraversano il decoder e arrivano ai condensatori, la
tensioneMinimaCorrettoFunzionamento si abbassa
ulteriormente perché una certa quantità di corrente viene usata dai decoder stessi, per convertire da DCC a DC.
Quindi in realtà, la tensioneMinimaCorrettoFunzionamento potrebbe essere di circa 13.5 V, quindi il diodo Zener non deve
lavorare nella maggior
parte dei casi.

Questi diodi hanno una tolleranza di 5-10 %, quindi 1.5 V.
Potrebbe iniziare a funzionare a partire da 13.5 V, ma nella maggior parte dei casi non fa nulla appunto.

Se i condensatori sono 15 V, il mio consiglio è quello di prendere un diodo zener di 13 V, per prendere in
considerazione quel + o - 10 %, altrimenti se mettiamo un diodo a 15 V e questo entra in funzione a 16.5 i condensatori
potrebbero scoppiare appunto.

# Decoder

È necessario scegliere dei decoder che siano già predisposti per keep-alive, in questo lavoro è tutto molto più
semplice. Un esempio è il [LokPilot DCC 5](https://www.esu.eu/en/products/lokpilot/lokpilot-5-dcc/)

# Connessione

![Immagine](/Documentazione/Immagini/ConnessioneKeepAliveDecoderEsu.png)

Collegamenti in questa maniera.
Non ho potuto mettere fascetta termorestringente in quanto non si chiudeva il coperchio.

![Immagine](/Documentazione/Immagini/LocomotivaKeepAlive1.png)
![Immagine](/Documentazione/Immagini/LocomotivaKeepAlive2.png)

# Fonti

- [Stayalives](https://www.youtube.com/watch?v=o3DcJMmzU3I)
- [DCC Keep Alives, how they work and how to select components for your own](https://www.youtube.com/watch?v=bhK4HuJb6Ws)
- [New Update Easy DIY Keep Alive](https://www.youtube.com/watch?v=nm2b7MfQrFs&t=280s)
