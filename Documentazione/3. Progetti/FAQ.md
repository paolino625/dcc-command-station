# Ho appena aggiunto un convoglio dal keypad/display ma non lo vedo tra i convogli selezionabili nel menù principale

Dopo aver aggiunto un convoglio è necessario impostarlo come "presente" sul tracciato. 
Il menù principale permette la selezione dei soli convogli presenti attualmente sul tracciato.

# La mia locomotiva va a scatti

- Nel caso in cui non sia presente un KeepAlive a bordo, la locomotiva potrebbe ricevere un'alimentazione irregolare dai binari. Questo può essere dovuto a ruote o binari sporchi.
- Nel caso in cui si sta utilizzando un keep alive, dovrebbe essere sufficiente la lubrificazione degli ingranaggi con dell'olio. Seguire i consigli nel capitolo relativo.

# Arduino Giga si blocca e non risponde ai comandi

Probabilmente Arduino Giga è collegato al Mac.
Infatti quando ciò avviene e il Mac va in standby, c'è qualche oscura ragione per cui Arduino non risponde più ai comandi, nonostante il protocollo DCC continui a funzionare.
È sufficiente dunque riattivare il Mac per risvegliare Arduino.

# Il mapping presenta dei valori sballati

Il sensore IR utilizzato per il mapping non viene azionato sempre.

# Il convoglio deraglia in curva

Controllare se i ganci dei vagoniReali si muovono liberamente.
Certe volte la levetta del gancio si incastra, oppure i fili tirano troppo e non consentono un movimento fluido.

C'è una sostanziale differenza di velocità tra locomotiva in spinta e locomotiva in trazione.
Potrebbe essere utile rieseguire il mapping e controllare se i valori sono cambiati di molto.

# Perdita di connessione tra Arduino Master e Arduino Slave

È successo con board MEGA2560 compatibili ma da quando uso le board originali non ho più avuto problemi.

# Arduino Slave Sensori impazzisce

Sintomi: Le stampe sulla porta seriale presentano velocità intermittenti e i valori letti dai sensori risultano errati.
Causa: Probabile interferenza dovuta al fatto che Arduino Slave Sensori è collegato al Mac mentre quest’ultimo è in carica.
Soluzione: Rimuovere il Mac dalla carica.