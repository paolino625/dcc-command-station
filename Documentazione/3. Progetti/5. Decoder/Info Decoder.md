# Sound

Grossa diatriba: sound o non sound?
Scelta decoder non sound: troppo costosi ed è difficile reperire i suoni reali delle varie locomotive.

# Interfaccia Decoder

## Interfaccia 8 pin

Oramai non più in uso.

![Immagine](/Documentazione/Immagini/Decoder8Pin.png)

![Immagine](/Documentazione/Immagini/Decoder8Pin2.png)

## Interfaccia 21 MTC

L'interfaccia del connettore 21MTC è uno standard adottato sia dall'NMRA che dal NEM 660. Il nome deriva dal connettore a 21 pin Marklin/Trix, sviluppato da Marklin e ESU. Il nome deriva dal connettore a 21 pin Marklin/Trix, sviluppato da Marklin ed ESU. Si trova spesso nelle locomotive con decoder OEM di ESU.

Questa interfaccia è sempre del tipo a collegamento diretto, in quanto non è presente alcun cablaggio. Il decoder si collega direttamente al connettore maschio montato sul telaio della locomotiva.

Lo standard NMRA ha sconsigliato l'uso di questo connettore nei nuovi progetti di locomotive a partire da gennaio 2010. È previsto che venga sostituito dall'interfaccia PluX.

La principale differenza fisica tra i decoder 21MTC e PluX è la disposizione dei pin. Il 21MTC è dotato di una presa femmina che si accoppia con i pin dell'interfaccia della locomotiva. Il PluX è l'opposto: i pin sono sul decoder e si accoppiano con un connettore femmina sulla locomotiva. Lo standard PluX definisce la lunghezza dei pin, che passano attraverso la scheda di interfaccia della locomotiva per un'installazione a basso profilo. Verificare l'allineamento con la documentazione fornita con il decoder, poiché la connessione potrebbe non essere dotata di chiave per evitare un allineamento errato.

Se il produttore dichiara che la locomotiva è dotata di un decoder a 21 pin, ciò indica un'interfaccia 21MTC.

![Immagine](/Documentazione/Immagini/21MTC.JPG)

## Plux 22

L'NMRA e il MOROP hanno adottato un'interfaccia standard più recente, denominata PluX22, e i suoi sottoinsiemi, PluX16 e PluX8 (il NEM 658 del MOROP definisce anche il PluX12, ma lo standard NMRA S-9.1.1 non lo definisce).

Scopo
Lo scopo è quello di fornire un'interfaccia uniforme per l'installazione sicura e rapida di decoder conformi allo standard PluX.

![Immagine](/Documentazione/Immagini/Plux22.png)

## Scelta progettuale

Abbiamo optato per lo standard 21MTC e non Plux22 semplicemente perché la Rivarossi forniva con alcune locomotive decoder con attacco 21MTC. Volendo dunque unificare il più possibile abbiamo optato per quelli.

# Keep-Alive

Una delle caratteristiche che abbiamo considerato molto importante durante la fase di scelta del decoder era la predisposizione o meno del keep-alive, per noi molto importante.

# CV

Nei decoder Digital Command Control (DCC) utilizzati nei modelli ferroviari in scala, il termine "CV" sta per "Configuration Variable" (variabile di configurazione). I CV sono parametri programmabili che consentono agli utenti di personalizzare e configurare il comportamento del decoder in modo specifico.

Alcuni esempi di cosa possono essere configurati attraverso i CV includono:

- Indirizzo DCC: Il CV 1 di solito è utilizzato per impostare l'indirizzo DCC della locomotiva. Questo è l'indirizzo con cui la locomotiva risponderà ai comandi dalla centrale.
- Inerzia: Alcuni CV controllano l'inerzia della locomotiva, determinando quanto tempo impiegherà per accelerare o decelerare.
- Frenatura: Alcuni decoder consentono la programmazione di CV per regolare la forza di frenatura della locomotiva.
- Luci e suoni: I CV possono anche essere utilizzati per controllare le funzioni delle luci e dei suoni sulla locomotiva.
- Configurazione avanzata: Alcuni decoder offrono una vasta gamma di CV che possono essere utilizzati per personalizzare il comportamento della locomotiva in modo più avanzato, come la regolazione della velocità massima, l'accelerazione e altro ancora.

# Marca Decoder

Abbiamo scelto decoder ESU: https://www.esu.eu/en/products/lokpilot/

Nello specifico il decoder DCC: https://www.esu.eu/en/products/lokpilot/lokpilot-5-dcc/

# Fonti

- [Locomotive Interface](https://dccwiki.com/Locomotive_Interface)