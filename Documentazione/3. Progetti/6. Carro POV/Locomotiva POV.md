# Introduzione

Sarebbe bello avere una telecamera su una locomotiva in modo tale da poter esplorare il plastico dal punto di vista del macchinista (vista POV).

Ecco alcuni video di esempio:
 
- [Cab ride plastico ferroviario stazione di Savona](https://youtu.be/cPZVXdn-ikQ?si=Poea8ub8Q9eD5hua)
- [Plastico della direttissima - Prato](https://youtu.be/h_k4cwOrg_Y?si=82Zk-fftlrj_ozY2)
- [Museo di ferrovia in Germania](https://youtu.be/NjOpfCr70AY?si=cXwCA5JwOMoMNrhF)
- [Miniature Wunderland](https://youtu.be/Kfa8gfNy-Ps?si=7JZg_r9Wn4c9BDM1)

Dopo una breve ricerca online ho potuto constatare i metodi più utilizzati.

## Raspberry Pico W + Telecamera Raspberry

https://www.raspberrypi.com/news/ride-a-model-railway-from-the-train-drivers-pov/

![Immagine](/Documentazione/Immagini/POV1.jpeg)

La soluzione proposta qui fa uso di un Raspberry Pico W, che è abbastanza piccolo da poter entrare su una carrozza.

Ad esso è stata collegato un modulo telecamera per Raspberry.

Lui ha poi inserito un powerbank per poter alimentare il raspberry Pi.

Il raspberry si collega al WiFi e riesce a mettere a disposizione lo streaming della telecamera.

È possibile vedere [qui](https://youtu.be/5_c7Cn7BgbM?si=EeOMgIORFgM7J23r) il risultato.

### Vantaggi

- Soluzione economica
- Soluzione che consente la trasmissione in streaming.

### Svantaggi

- La qualità della telecamera è scarsa, sia come risoluzione che in condizioni di lowlight.
- La telecamera non è stabilizzata. Come è possibile vedere dal video, le numerose vibrazioni e oscillazioni della locomotiva in movimento porteranno ad avere un video molto instabile e non piacevole alla visione. Ci sono moduli telecamere stabilizzati tipo [questo](https://it.aliexpress.com/item/1005004802730787.html) ma rimangono di bassa qualità e la stabilizzazione offerta non può essere paragonata ad una offerta da una telecamera più grande come una GoPro, ad esempio.

## Solo telecamera

Alcune soluzioni prevedono soltanto l'utilizzo di una sola telecamera.

Chiaramente bisogna stare attenti alle dimensioni per evitare che la telecamera vada ad occupare binari adiacenti.

### Vantaggi

- Telecamera di maggiore qualità rispetto a quella del raspberry.

### Svantaggi

- Non è possibile real-time view.
È possibile soltanto registrare su SD e poi visionare il girato. Magari è possibile soluzioni proprietarie, cioè scaricando l'app dedicata ma non considero queste soluzioni in quanto vorrei poter avere la scelta di dove mandare il flusso dati (magari ad un NVR in modo tale che possa vedere il video da qualsiasi posto)
- Telecamera non alimentata. È necessario ricaricarla manualmente ogni volta.

### GoPro

GoPro: https://nwsisu.com/3d-printed-ho-model-train-camera-car/

### Polaroid Camera

Fonte: https://www.pilentum-television.com/it/carrozza-con-telecamera-per-treni-in-transito-sui-plastici-ferroviari.htm

Viene consigliata la seguente telecamera: https://shop.runcam.com/runcam-5-orange/

Soluzione sicuramente molto interessante e ben ingegnerizzata.

### Vantaggi

- Possibilità di girare la telecamera per avere vari angoli di visione.

### Svantaggi

- Avere una carrozza così lunga non è una buona cosa. Durante una curva, specie se il raggio di curvatura è piccolo, si ha un disallineamente della telecamera con il tracciato. Di fatto, più il vagone è piccolo, meglio è, in maniera tale che la telecamera curvi esattamente come il binario curvo.

### DJI Pocket Osmo

C'è la possibilità di far trasmettere in real-time il video del DJI Pocjet Osmo, ma non con protocollo ONVIF.
Più informazioni qui: https://cosmostreamer.com/products/pocketosmo/diy/

#### Vantaggi

- Qualità della lente eccelsa
- Stabilizzazione eccezionale

## Mio approccio

Prendo spunto dalla [guida](https://nwsisu.com/3d-printed-ho-model-train-camera-car/) già precedentemente segnalata per la costruzione di una carrozza apposita per ospitare la GoPro in verticale.

Volendo aggiungere la possibilità di streaming, colleghiamo la goPro con un raspberry Pico W 2.

Utilizzando questo [software](https://www.toolsforgopro.com/live.php ) siamo in grado di usare il flusso in input dalla goPro e renderlo disponibile in rete tramite protocollo ONVIF.

VideoGuida: https://www.youtube.com/watch?v=AoK1pkq2lFA

Ci manca la parte dell'alimentazione, nei precedenti esempi sempre lasciati ad un powerbank oppure alla ricarica manuale.

Utilizziamo un decoder per funzioni per convertire segnale DCC in corrente continua e utilizziamo le uscite funzioni del decoder per alimentare il raspberry che a sua volta alimenta la goPro.

Non dovrebbe essere un problema considerando. Il consumo di Raspberry Pi 2 è di 350 mA, il resto dato dall'alimentazione viene lasciato per USB e periferiche.

Per abiltiare la ricarica, sarà necessario abilitare l'uscita funzione di quel determinato decoder.

