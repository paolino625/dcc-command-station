## Stato avanzamento lavori

Leggenda:

- ✅ = Fatto
- ❌ = Non fatto
- ❗️ = Da testare in maniera più approfondita
- 🤓 = Working in progress
- ⏰ = In attesa di qualcosa bloccante


| Livello | Preparazione tavola | Fissaggio tavola | Validazione tracciato | Montaggio scambi | Montaggio sensori | Collegamenti dorsale | Validazione autopilot |
|---------|---------------------|------------------|-----------------------|------------------|-------------------|----------------------|-----------------------|
| 1°      | ✅                   | ✅                | ✅                     | ✅                | ❌                 | ❌                    | ❌                     |
| 1° - 2° | ✅                   | ❌                | ❌                     | ❌                | ❌                 | ❌                    | ❌                     |
| 2°      | ✅                   | ❌                | ❌                     | ❌                | ❌                 | ❌                    | ❌                     |
| 2° - 3° | ✅                   | ❌                | ❌                     | ❌                | ❌                 | ❌                    | ❌                     |
| 3°      | ✅                   | ❌                | ❌                     | ❌                | ❌                 | ❌                    | ❌                     |
