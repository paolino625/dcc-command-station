# Mancanza di realismo

L'obiettivo di questo plastico non è la ricerca del realismo. Ad esempio, abbiamo optato per l'utilizzo di scambi Hornby, nonostante il loro raggio di curvatura estremamente non realistico, poiché ci permettono di sfruttare al meglio lo spazio disponibile.

# Raggio di curvatura

Si è cercato di evitare curve con raggio di curvatura troppo strette (ad esempio 37.1 cm).
Questo perché potrebbero esserci determinate locomotive o convoglio bloccati che potrebbero avere problemi a girare.
Come raggio di curvatura minimo si è scelto dunque 43.8 cm, cercando comunque di far rimanere questi raggi di curvatura nascosti il più possibile.

Le curve in bella vista hanno un raggio di curvatura di circa 91 cm, in modo tale da sembrare realistici.

# Controcurva

Da evitare controcurve, ovvero una curva seguita da una curva nel senso opposto.
Se il raggio di curvatura non è estremamente ampio, l'effetto che ne consegue è che i due vagoniReali viaggiano in "parallelo", molto irrealistico e fastidioso.

Per mitigare questo effetto è possibile aggiungere un binario dritto dopo la prima curva, prima della seconda curva.
Una lunghezza accettabile è 30 cm circa, questo consente di annullare l'effetto.

È più difficile risolvere questo problema quando si vuole usare una coppia di scambi formando una S. Qui è presente una controcurva e non è possibile inserire un pezzetto di binario dritto.

Si hanno due soluzioni: o posizionamento di scambi con un raggio di curva ampia (scambi hornby chiamati ad alta velocità) che però occupano più spazio e solitamente hanno maggiore costo, oppure nascondere questi scambi in parti non visibili del plastico. Nel mio caso ho utilizzato entrambe le soluzioni: la copertura delle coppie di scambi a S e dove ciò non è stato possibile, il piazzamento di scambi con raggio di curvatura ampia.

# Pendenze elevate

Il tracciato presenta pendenze notevolmente elevate, che raggiungono il 13%. 
Nel modellismo ferroviario, è fortemente sconsigliato superare il 3-3.5% di pendenza, poiché le locomotive potrebbero avere difficoltà a salire (anche senza vagoniReali) a causa dello slittamento delle ruote.

Abbiamo risolto tale problema grazie alla doppia trazione: ovvero una locomotiva in trazione e una in spinta. Grazie al mapping preciso delle velocità delle locomotive (argomento trattato in un capitolo precedente), siamo in grado di farle marciare alla stessa identica velocità. È importante che il mapping sia preciso, se così non fosse, la locomotiva in spinta potrebbe esercitare una spinta eccessiva, facendo contrarre il gancio e causando il deragliamento del treno alla prima occasione.
