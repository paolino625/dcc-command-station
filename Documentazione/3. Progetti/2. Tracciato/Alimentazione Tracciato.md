Per l'alimentazione del tracciato è consigliabile utilizzare dei ponticelli R8232 per gli scambi Hornby (che altrimenti sarebbero isolati).
Essendo un plastico in DCC e utilizzando prevalentemente sensori IR per localizzazione delle locomotive (e non Block Detector) abbiamo ritenuto superfluo e complicato procedere al sezionamento del tracciato.
Il tracciato viene alimentato sì da vari punti, ma tutti i binari sono interconnessi, eccetto per quelle sezioni dove non è possibile utilizzare un sensore IR ed è necessario utilizzare un Block Detector.

Si è pensato di saldare i fili dell'alimentazione sulle scarpette di giunzione dei binari in modo tale che, realizzando dei buchi sulla tavola di legno in prossimità, i cavi non sono visibili.
Tuttavia, tali giunzioni rifiutano lo stagno (anche dopo averle grattate).
Si è dunque optati per saldare i fili direttamente sul lato esterno dei binari in modo tale da non dar fastidio alla circolazione dei treni. Chiaramente si è cercato di nascondere tali punti di alimentazione per non renderli facilmente visibili.