# Scelta del posizionamento dei sensori

Nella maggior parte dei casi, il legno agisce come un bloccante naturale tra i due sensori. 
Se i sensori sporgessero dai buchi, non ci sarebbe così tanta differenza tra le parti della locomotiva più basse e la fascetta riflettente. 
Mettendo i sensori un po' in profondità, si evitano queste false letture: il nastro argentato riflette abbastanza da arrivare fino in fondo, mentre tutto il resto no.
Questo previene anche false letture come le dita di un visitatore che passa sui buchi: infatti, in questo caso, il dito non viene rilevato.

Dopo numerosi test, è emerso che, per ottenere un risultato ottimale, è fondamentale inserire un tubetto termorestringente nero con un diametro di circa 2,4 mm e una lunghezza di circa 0,5 cm all’interno del foro del diodo ricevitore (quello nero). Questo accorgimento elimina completamente i falsi positivi.
Assicurarsi di inserire il tubetto in posizione verticale per garantire un funzionamento ottimale. 
Si è inoltre testato l’inserimento di un tubetto termorestringente nero o bianco nel foro del trasmettitore LED, ma tale soluzione non ha prodotto risultati significativi.

# Come posizionare il sensore

Durante le prove, è consigliato abilitare il buzzer su ArduinoSlaveSensori in modo tale da avere un feedback quando i sensori vengono azionati.
Verificare/abilitare la presenza del sensore nell'array sensoriIrPresenti[].

Posizionare il sensore in modo tale che i LED siano a circa metà del buco.
Inserire la fascetta termorestringente di circa 2.4 mm nel buco del diodo ricevitore, in modo tale da far sporgere la fascetta a livello della tavola di legno.
Trovare il numero variabile di distanziali in modo tale che queste condizioni si verifichino:
- Al passaggio di una locomotiva il sensore venga azionato una sola volta.
  - Provare una locomotiva dove la fascetta riflettente è posizionata in alto e un'altra locomotiva dove è più in basso. Provare a velocità ridotta e a velocità massima.
- Al passaggio di vagoni il sensore non viene azionato.
- Al passaggio del dito, il sensore non viene azionato.

Nel caso in cui il sensore non dovesse funzionare come previsto, prima di cambiare la soglia di attivazione/disattivazione del sensore, provare a cambiare il sensore con un altro.

Nel caso in cui si volesse controllare il valore del sensore, si può andare su EnvironmentConfigSvil e abilitare la stampa.