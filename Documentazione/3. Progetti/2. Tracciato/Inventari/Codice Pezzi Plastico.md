# Codici Pezzi Plastico

- Tappeto verde: Heki 30903
- Giunzioni Hornby: R910
- Giunzioni Piko: Piko 55290
- Ponticelli scambi: Hornby R8232
- Microviti per legno (fissaggio motori scambio): M2* 10mm  https://www.ebay.it/itm/114593970520?var=414771589045
- Viti per fissaggio binari: Fleischmann 6410 https://www.tecnomodel.it/i/fleischmann-6410-viti-per-il-fissaggio-di-binari-cremagliere-segnali-etc-144-pz/17289