Misure: Larghezza x Altezza x Profondità

# StazioniAbstract

## Stazione 1

![Immagine](/Documentazione/Immagini/Edificio3.jpg)

23 x 14 x 11.6

## Stazione 2

![Immagine](/Documentazione/Immagini/Edificio4.jpg)

17.2 x 11 x 13.5

## Stazione 3

![Immagine](/Documentazione/Immagini/Edificio1.jpg)

13.9 x 9.5 x 6.3

## Stazione 4

![Immagine](/Documentazione/Immagini/Edificio2.jpg)

10.5 x 8.8 x 10.5

## Stazione 5

![Immagine](/Documentazione/Immagini/Edificio5.jpg)

3.6 x 4.5 x 4.7

## Stazione 6

Da comprare

25.6 x 10 x 10

![Immagine](/Documentazione/Immagini/Edificio10.jpg)

# Officina

![Immagine](/Documentazione/Immagini/Edificio8.jpg)

28.5 x 12 x 16.5

----

![Immagine](/Documentazione/Immagini/Edificio9.jpg)

24 x 7.7 x 6.1

# Merci

![Immagine](/Documentazione/Immagini/Edificio7.jpg)

17.5 x 10.9 x 12.5

# Altro

![Immagine](/Documentazione/Immagini/Edificio6.jpg)

6 x 14.5 x 6
