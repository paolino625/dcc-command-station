- [Spessore](#spessore)
- [Misure Tavole](#misure-tavole)
  - [1° Livello](#1-livello)
  - [2° Livello](#2-livello)
    - [GIA COMPRATE](#gia-comprate)
  - [3° Livello](#3-livello)
  - [Rampe IN ATTESA](#rampe-in-attesa)
- [Prezzo Tavole](#prezzo-tavole)
- [Disponibilità negozi](#disponibilità-negozi)
  - [Leroy Merlin](#leroy-merlin)
  - [Obi](#obi)
- [Lista Spesa](#lista-spesa)
  - [OBI](#obi-1)
  - [Leroy Merlin](#leroy-merlin-1)

# Spessore

- Tavola Livello 1: 20 mm
- Tavola Livello 2: 15 mm
- Sopraelevata 16 mm

# Misure Tavole

## 1° Livello

- Tavola rettangolare 1° Livello Espansione tavole esistenti (staffe piatte)
  - Sinistra
    - Truciolare 150 x 22 cm (spessore 20 mm): piastre giunzione
    - Truciolare 150 x 22 cm (spessore 20 mm): piastre giunzione
  - Destra
    - Truciolare 181 x 20.5 cm (spessore 20 mm): piastre giunzione
    - Truciolare 181 x 20.5 cm (spessore 20 mm): piastre giunzione

- Tavola rettangolare 1° Livello Estrema Destra
  - Truciolare 145 x 30 cm (spessore 20 mm): 4 staffe angolari
  -  Truciolare 145 x 30 cm (spessore 20 mm): 4 staffe angolari

- Tavola rettangolare 1° Livello Estrema Sinistra
  - Truciolare 180 x 15 cm (spessore 20 mm): 4 tasselli a scomparsa
  - Truciolare 180 x 15 cm (spessore 20 mm): 4 tasselli a scomparsa

## 2° Livello

- Tavola rettangolare 2° Livello Estrema Destra
  - Truciolare 180 x 30 cm (spessore 15 mm): 4 staffe angolari
  - Truciolare 180 x 30 cm (spessore 15 mm): 4 staffe angolari

- Tavola rettangolare 2° Livello sopraelevata
  - 280 x 13 truciolare (spessore 15 mm): 6 staffe angolari
  - 280 x 13 truciolare (spessore 15 mm): 6 staffe angolari

- Tavola rettangolare 2° livello estensione tavola quadrata
  -  Tavola Truciolare 115 x 22 cm (spessore 15 mm): piastre giunzione

- Tavola triangolare 2° livello
  - Tavola Truciolare 230 x 95 cm (spessore 15 mm)

### GIA COMPRATE

- Tavola quadrata 100x113 (circa) 2° Livello sinistra: 3 staffe angolari avvitate con bussola filettata + 1 fermaporta + 1 pilastrino
- Tavola trapezoidale: piastra giunzione

## 3° Livello

- Tavola rettangolare 3° Livello
  - Tavola Truciolare 160 x 42 cm (spessore 15 mm)
  - Tavola Truciolare 160 x 42 cm (spessore 15 mm)

## Rampe IN ATTESA

- Tavola x rampa 1°- 2° Piano destra
  - 100 x 6 cm (spessore 4 mm)
  - 100 x 6 cm (spessore 4 mm)

- Tavola x rampa 1°- 2° Piano sinistra
  - 85 x 6 cm (spessore 4 mm)
  - 85 x 6 cm (spessore 4 mm)

IN TOTALE: un foglio 80 x 100 cm (spessore 4 mm)

# Prezzo Tavole

Truciolare 20 mm: 35 € Mq
Truciolare 15 mm: 26 € Mq

Totale 20 mm: 35 x 0.74 = 25.9 €
Totale 15 mm: 26 € x 4,6 = 120 €

# Disponibilità negozi

## Leroy Merlin

Truciolare a 15 e 18 mm
Lunghezza massima: 250 cm

## Obi

Lunghezza massima: 280 cm
Truciolare: 15 e 20 mm

# Lista Spesa

## OBI

- Tavola rettangolare 1° Livello Espansione tavole esistenti
  - Truciolare 150 x 22 cm (spessore 20 mm)
  - Truciolare 150 x 22 cm (spessore 20 mm)
  - Truciolare 181 x 20.5 cm (spessore 20 mm)
  - Truciolare 181 x 20.5 cm (spessore 20 mm)

- Tavola rettangolare 1° Livello Estrema Destra
  - Truciolare 145 x 30 cm (spessore 20 mm).
  -  Truciolare 145 x 30 cm (spessore 20 mm).

- Tavola rettangolare 1° Livello Estrema Sinistra
  - Truciolare 180 x 15 cm (spessore 20 mm)
  - Truciolare 180 x 15 cm (spessore 20 mm)

- Foglio MDF 80 x 100 cm o 80 x 120 cm (spessore 4 mm) per RAMPE

## Leroy Merlin

- Tavola rettangolare 2° Livello Estrema Destra
  - Truciolare 180 x 30 cm (spessore 19 mm)
  - Truciolare 180 x 30 cm (spessore 19 mm)

- Tavola rettangolare 2° Livello continuo sopraelevata
  - Truciolare 90 x 13 cm (spessore 19 mm)

- Tavola trapezoidale 2° Livello fusione tra sopraelevata - estrema destra
  - Truciolare 42 x 80 cm (spessore 19 mm)

- Tavola rettangolare 2° livello estensione tavola quadrata
  -  Tavola MDF 115 x 22 cm (spessore 15 mm)

- Tavola triangolare 2° livello
  - Tavola MDF 230 x 95 cm (spessore 15 mm)

- Tavola rettangolare 3° Livello
  - Tavola Truciolare 160 x 42 cm (spessore 15 mm)
  - Tavola Truciolare 160 x 42 cm (spessore 15 mm)