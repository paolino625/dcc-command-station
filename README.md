- [Introduzione](#introduzione)
- [Struttura progetto](#struttura-progetto)
- [Working in progress](#working-in-progress)

# Introduzione

Fin dalla mia più tenera infanzia, i treni hanno esercitato un fascino irresistibile su di me.

Mio padre, con dedizione, si premurava di memorizzare gli orari dei treni solo per portarmi dinanzi ai passaggi a
livello nel preciso istante in cui si chiudevano, regalandomi la visione travolgente delle locomotive ad alta velocità.

![Immagine](/Documentazione/Immagini/PaoloTreno.png)

Nel lontano 2001, a soli quattro anni, mi è stata regalata la mia prima locomotiva e un modesto set di binari Lima. In
breve tempo, un semplice anello ferroviario non riusciva più a placare la mia crescente passione: il desiderio di creare
un plastico di grandi dimensioni si faceva sempre più impellente, e un'infinità di idee balenavano nella mia mente in
continuazione.

Tuttavia, come spesso accade, i bambini devono confrontarsi con la realtà: per questo tipo di hobby, e in particolare
per la complessità del progetto che desideravo realizzare, erano richieste risorse finanziarie, tempo e, soprattutto,
competenze. Competenze che, purtroppo, non avevo durante l'infanzia e l'adolescenza, determinando così che questo sogno
restasse sempre confinato nel cassetto dei desideri irrealizzabili.

Un punto di svolta è arrivato molti anni più tardi, quando il mio percorso universitario in Informatica mi ha dotato
delle competenze chiave per trasformare in realtà il sogno che coltivavo: progettare un plastico ferroviario
automatizzato partendo da zero, senza fare affidamento a soluzioni commerciali preesistenti, molto costose e poco
personalizzabili.

Ho iniziato il mio percorso studiando il protocollo DCC, lo standard nel modellismo ferroviario che consente di
controllare locomotive e accessori ferroviari. Con successo, ho implementato questo protocollo su un microcontroller
Arduino, dimostrando la fattibilità del mio progetto. Questo ha aperto la porta a una soluzione di controllo ferroviario
completamente personalizzabile a 360°.

Tra le varie funzionalità in sviluppo del sistema, c'è anche la possibilità di far circolare i treni in maniera
autonoma, rispettando segnali, precedenze e altro, tutto senza l'intervento umano.

# Struttura progetto

- BackEnd: questa sezione raggruppa tutti i componenti back-end
    - Arduino: tutti i file relativi ai vari Arduino
        - src
          - main
              - progetto: codice sorgente dei vari Arduino
              - proofOfConcept: alcuni esempi di codice di funzionalità particolari
          - resources
              - arduinoMaster
                  - fileUsb: questi file costituiscono il database del sistema e devono essere caricati nella memoria di massa esterna di
                    Arduino per garantirne il corretto funzionamento.
          - test
            - main: test di unità per il codice sorgente
            - resources: collection per testare endpoint HTTP.
    - ArduinoMasterHelper: codice sorgente Java del microservizio.
- Container: questa sezione contiene i file necessari per la creazione dei container Docker.
- Documentazione: questa sezione contiene la documentazione completa del progetto, comprendente dettagli su ogni
  aspetto.
    - Ambiente di sviluppo: info utili per impostare l'ambiente di sviluppo. 
    - Mondo Reale: questa sezione comprende appunti dettagliati sul funzionamento delle ferrovie nel mondo reale,
      offrendo una panoramica approfondita su temi quali il segnalamento ferroviario, la circolazione ferroviaria, il
      sistema di protezione della marcia dei treni e l'orario ferroviario. Una visione generale di come funzionano le
      cose nel mondo reale è non solo interessante ma anche estremamente utile per una comprensione completa del
      contesto ferroviario.
    - Modellismo ferroviario. Questa sezione fornisce una chiara spiegazione del protocollo standard DCC e approfondisce
      ulteriori tematiche del modellismo ferroviario. Vengono affrontati argomenti come la localizzazione delle
      locomotive in un plastico, il funzionamento degli scambi e la definizione di un cappio di ritorno. L'obiettivo è
      fornire una visione dettagliata e comprensibile di aspetti fondamentali per gli appassionati di modellismo
      ferroviario
    - Progetti: qui è possibile trovare documentazione id sui diversi moduli, quasi a sé stanti, che compongono questo
      progetto.
        - DCC Command Station: indubbiamente il fulcro centrale del progetto, questa sezione introduce l'apparato
          software e hardware sviluppato per assicurare il corretto funzionamento del sistema, illustrando al contempo
          tutte le funzionalità implementate.
        - Tracciato: questa sezione fornisce dettagli sul layout del plastico ferroviario, comprendente la mappa del
          circuito, l'inventario dei binari e le scelte progettuali effettuate.
        - Rotabili: questa sezione fornisce informazioni esaustive sulla pulizia e manutenzione dei rotabili, offrendo
          anche dettagli approfonditi sulle locomotive e sui vagoniReali attualmente in mio possesso.
        - Keep Alive: questa sezione offre una dettagliata spiegazione su cosa sono i keep alive, evidenziando come
          notevolmente migliorino il funzionamento delle locomotive e il motivo per cui risultano fondamentali per il
          nostro progetto. Incluso è anche il circuito di progettazione associato,
        - Decoder: questa sezione fornisce informazioni dettagliate per guidare la scelta dei decoder DCC, accompagnate
          dalla spiegazione delle diverse interfacce disponibili.
        - Carro POV: informazioni sulla progettazione di un carro destinato all'inserimento di una telecamera portatile,
          consentendo una visione Point of View (POV) del plastico ferroviario.
        - Luci Carrozze: progettazione di luci LED per carrozze.
    - Extra: questa sezione offre ulteriori informazioni sul vasto mondo ferroviario, includendo dettagli su fiere di
      modellismo, giochi, plastici ferroviari, e altri eventi correlati.
- FrontEnd: questa sezione raggruppa tutti i componenti relativi al Front-End.
  - File SVG: file SVG del tracciato.
  - ReactWebApp: codice sorgente della web app.

# Working in progress

Attualmente, mi sto concentrando sui seguenti aspetti:

- Progettazione e realizzazione del tracciato ferroviario.
- Sviluppo di un sistema di autopilota avanzato, che non solo consentirà ai treni di muoversi autonomamente ma sarà in
  grado di gestire itinerari complessi e rispondere a situazioni dinamiche sulla rete ferroviaria.
- Creazione di un'interfaccia utente intuitiva.