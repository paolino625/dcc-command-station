 # DCC COMMAND STATION

## NMRA

L'attività più conosciuta di NMRA è la creazione di standard e documenti conosciuti come Reccomended Practices (RP) per il modellismo ferroviario.

Molti standard definiti da NMRA sono ampiamente adottati dall'industria (come ad esempio le dimensioni delle ruote).

La principale ragione della creazione di NMRA era quella di incoraggiare l'interoperabilità tra i produttori: un problema molto sentito tempo fa.

## DCC

E' uno standard definito dalla National Model Railroad Association (NMRA).

### INTRODUZIONE

Il Digital Command Control (DCC) è un protocollo per il controllo di locomotive su percorsi di modelli ferroviari che permette a una o più locomotive di muoversi indipendentemente sulla stessa sezione di binario.

Questo approccio supera dunque il limite del vecchio approccio dove la tensione della traccia veniva utilizzata per controllare la velocità e direzione di tutti i treni presenti sul tracciato.

### FUNZIONAMENTO

Ogni locomotiva contiene un decoder DCC e ad esso viene associato dall'utente un indirizzo univoco.
In questo modo soltanto le istruzioni che corrisponderanno all'indirizzo verranno decodificate dal decoder.

#### Segnale bipolare

I segnali DCC non sono né corrente continua né alternata: sono bipolari.

##### Corrente continua

Con un motore a corrente continua l'incremento della tensione comporta l'aumento di velocità.
Cambiare la polarità fa sì che il motore vada nella direzione opposta.

Questa è la modalità nella quale i plastici ferroviari hanno funzionato per anni prima dell'arrivo del DCC.

##### Corrente alternata

Con la corrente alternata (AC) la tensione applicata non è costante ma prende forma di onda sinusoidale.

0 V -> + 12 V -> 0 V -> -12 V.

##### Corrente bipolare

La corrente bipolare è quasi come la corrente alternata: mantiene infatti una tensione assoluta costante ma cambia la polarità molto rapidamente.
A differenza della corrente alternata non si muove in maniera graduale dal suo massimo valore a 0 e poi al suo valore minimo.

Invece salta molto rapidamente dal suo valore massimo positivo al suo valore massimo negativo.

Questi impulsi assicurano che i decoder DCC e i treni ricevono sempre una tensione costante anche se metà del tempo la polarità è in un verso e l'altra metà è nell'altro verso.

#### Non soltanto energia

Trasmettere l'energia ai decoder DCC non è l'unico scopo dei segnali DCC: devono essere capaci anche di trasmettere informazioni
in modo tale che l'utente sia in grado di controllare i decoder (e dunque le locomotive).

Nell'era digitale l'informazione viene sempre memorizzata ed inviata come bit binari che non sono altro che una serie di 0 e 1: i segnali DCC non fanno eccezione.
Gli standard specificano precisamente come utilizzare i segnali bipolari per codificare bit binari.

##### Codifica Bit

Un impulso è lungo 116 microsecondi con 2 metà uguali di 58 microsecondi ognuno e rappresenta un bit binario "1".

Un impulso che è lungo 200 microsecondi con 2 metà di 100 microsecondi ognuna rappresenta un bit binario 0.

Per un bit "0" la durata dell'impulso deve essere di 100 microsecondi o più, dunque abbiamo un grosso margine.

Al contrario, per un bit "1" la durata dell'impulso deve essere compresa tra 55 e 61 microsecondi.
Dunque, se la durata dell'impulso è più veloce di 55 microsecondi o più lento di 61 microsecondi l'impulso non viene considerato valido.

### CONCORRENZA DEL DCC

Ci sono molti sistemi di controllo digitale in competizione, i più noti sono lo standard europeo Selectrix e lo standard proprietario Märklin-Motorola 
che è utilizzato esclusivamente dai modelli prodotti da Märklin. 

Il DCC rimane comunque lo standard più diffuso. 
Una delle ragioni è che il DCC è adottato sia in Nord America che in Europa.

Il DCC è stato sviluppato originariamente da una compagnia tedesca, la Lenz, che decise di farlo diventare uno standard aperto. 
Come risultato il DCC fu adottato come standard dalla NMRA e anche dalla MOROP.

### COMPATIBILITA' CON LOCOMOTIVE SENZA DECODER

Una centrale DCC può normalmente alimentare una locomotiva in corrente continua su una sezione di binario da sola o insieme a locomotive DCC 
per mezzo del metodo chiamato allungamento dello zero (streched zero).

In questa modalità i bit con valore 0 sul binario possono essere allungati notevolmente per creare un effetto dove la corrente appare al mezzo analogico 
fluire in un'unica direzione.

Questo sistema è comunque un ibrido, in quanto è presente una parte di corrente alternata che fluisce continuamente nei motori, 
quindi a lungo andare può surriscaldarli e in alcuni casi, in particolare motori elettrici "coreless" possono essere danneggiati.

### STRUTTURA PACCHETTI DCC

#### Struttura Generale Pacchetto

##### Preambolo

Il preambolo di un pacchetto consiste in una sequenza di bit 1.
Un digital decoder non accetta come valido un preambolo che abbia meno di 10 bit oppure deve richiedere la ricezione di un pacchetto di più di 12 bit "1".
Una command station deve mandare come minimo 14 bit di preambolo.

##### Packet Start Bit

Il pacchetto start bit è il primo bit con il valore "0" che segue un preambolo valido.
Il packet start bit termina il preambolo ed indica che i bit successivi rappresentano un indirizzo.

##### Address Data Byte

Il primo data byte del pacchetto contiene normalmente 8 bit di informazioni riguardanti l'indirizzo della locomotiva destinataria.
Gli indirizzi 00000000, 11111110 e 11111111 sono riservati per operazioni speciali e non devono essere utilizzati.

##### Data Byte Start Bit

Bit che procede il data byte e ha valore "0".

##### Data Byte

Ogni byte contiene 8 bit di informazioni utilizazti per l'indirizzo, istruzione, dati e per error detection.

N.B. Data Byte Start Bit e Data Byte da ripetere 1 o più volte a seconda della tipologia di istruzione).

##### Packet End Bit

Questo bit contrassegna la fine del pacchetto e ha valore "1".

#### Pacchetto Velocità e Direzione

- Preambolo

- Packet Start Bit 0

- Primo byte: indirizzo locomotiva

- Packet Start Bit 0

- Secondo byte: velocità

- Packet Start Bit 0

- Terzo byte: Error Detection

- Packet End Bit 1

##### Secondo byte

- Bit 1-2: contengono la sequenza di bit: "01" che indicare che il data byte istruzione è per la velocità e la direzione.

- Bit 3: Bit per la direzione. Se il bit è 1 la locomotiva si muove avanti. Se il bit è 0 si muoverà all'indietro.

- Bit 4: per impostazione predefinita deve contenere un bit di velocità aggiuntivo, che è il bit di velocità meno significativo.

- Bit 5-8: indicano la velocità dove 0 è la velocità meno significativa.

###### Step di velocità (ultimi 5 bit):

00000: Stop
10000: Stop

00001: E-Stop (I decoder devono interrompere immediatamente l'energia ai motori)

10001: E-Stop (Segnale Broadcast)

00010: Step 1

10010: Step 2

00011: Step 3

10011: Step 4

00100: Step 5

10100: Step 6

00101: Step 7

10101: Step 8

00110: Step 9

10110: Step 10

00111: Step 11

10111: Step 12

01000: Step 13

11000: Step 14

01001: Step 15

11001: Step 16

01010: Step 17

11010: Step 18

01011: Step 19

11011: Step 20

01100: Step 21

11100: Step 22

01101: Step 23

11101: Step 24

01110: Step 25

11110: Step 26

01111: Step 27

11111: Step 28

#### Pacchetto Reset

11111111111111 0 00000000 0 00000000 0 00000000 1

Un pacchetto di 3 byte dove tutti i bit dei 3 byte contengono il valore 0.

Quando un digital decoder riceve tale pacchetto, dovrebbe eliminare tutta la sua memoria volative (inclusi dati di velocità e direzione).
Se il decoder sta fornendo potenza ad una locomotiva con una velocità diversa dallo 0, dovrebbe fermare immediatamente la locomotiva.

#### Pacchetto Idle

11111111111111 0 11111111 0 00000000 0 11111111 1

Un pacchetto di tre byte, il primo byte contiene otto "1", il secondo contiene otto "0" e il terzo byte contiene otto "1".

Dopo aver ricevuto questo pacchetto, i digital Decoder non devono far nulla, ma devono comportarsi 
come se fosse un pacchetto normale indirizzato a qualche altro decoder.

I pacchetti IDLE servono per continuare ad alimentare i Decoder senza trasmettere informazioni aggiuntive.

#### Paccheto Broadcast Stop

111111111111 0 00000000 0 01DC000S 0 EEEEEEEE

## ARDUINO COME STAZIONE DCC

### PROBLEMA

Dunque per poter utilizzare Arduino come stazione DCC dobbiamo essere in grado di produrre un segnale bipolare 
che inverta la polarità da valori positivi a valori negativi ogni 58 microsecondi per codificare 1 bit oppure ogni 100 microsecondi per codificare un bit 0.
L'accuratezza non può superare una manciata di microsecondi.  

Inoltre la tensione e la capacità di corrente devono essere abbastanza grandi per garantire sufficiente potenza per un motore di una locomotiva.

Sfortunatamente Arduino non riesce a fare nulla di queste cose:

- I microcontrollori, per loro natura, tendono a produrre segnali digitali tra 0 e qualche numero positivo.
Queste onde quadrate sembrano dei segnali bipolari DCC rimpiccioliti ma noi abbiamo bisogno di segnali che invertano completamente la polarità da positiva a negativa:
non soltanto da una tensione positiva a 0: altrimenti i treni non avrebbero potenza durante il periodo di tensione pari a 0.

- I segnali digitali tendono ad essere a bassa tensione: tipicamente 3-5 volt o anche meno: 
per riuscire a dare potenza ai motori di una locomotiva elettrica abbiamo bisogno di 15 volt o anche di più.

- Le locomotive richiedono una grande quantità di corrente per operare: tipicamente più di 500 mA.
Tuttavia il massimo flusso di corrente che è possibile far passare attraverso i pin dei microcontroller è molto basso (per un'uscita di Arduino 40 mA).

Dunque non possiamo collegare direttamente Arduino ai nostri binari.

### SOLUZIONE

Bisogna separare la produzione del segnale DCC in due parti:

1. Produrre un segnale logico a bassa tensione a bassa corrente che corrisponde a tutte le specifiche imposte dallo standard DCC per codificare i bit 0 e 1.

2. Trasformare i segnali logici DCC a bassa corrente e basso voltaggio che contengono tutti i 0 e gli 1 in un segnale DCC bipolare a massimo voltaggio e massima corrente: questo segnale può essere collegato direttamente ai binari per alimentare e controllare i nostri treni.

#### 1° Parte

Per creare impulsi a livello logico potremmo scrivere un programma per Arduino che configura uno dei suoi pin come digital output.

Questo programma potrebbe contenere una logica a loop che processa ogni bit: setta il voltaggio del pin a 5 volt, poi aspetta 58 microsecondi 
per codificare il bit 1 o 100 microsecondi per il bit 0, poi rimette il pin a 0 volt e aspetta altri 58 microsecondi o 100 microsecondi per completare l'impulso.
Chiaramente tutto questo per codificare un solo bit DCC: dunque dobbiamo istruire il microcontroller di ripetere questo processo in continuazione per ogni bit che abbiamo bisogno di codificare.

Il problema di questo approccio è che è molto CPU-intensive.

Il microcontroller non avrebbe altro tempo per eseguire altre istruzioni, come capire che bit deve essere mandato e sicuramente non avrebbe tempo per eseguire 
istruzioni dell'utente su come controllare i treni.

Esempio codice:

If Bit type = 1-Bit
    Set Pin 10 to +5V
    Let X = Current time
    If Current Time - X < 58 usec
        Check Time Again
    Set Pin 10 to 0V
    Let X = current time
    If current time - X < 58 usec
        Check time again
    End 1-bit

If Bit Type = 0-Bit
    Set Pin 10 to +5V
    Let X = Current Time
    If current Time - X < 100 usec
        Check time again
    Set Pin 10 to 0V
    Let X = Curreent Time
    If current time - X < 100 usec
        Check time again
    End 0 Bit

Quello di cui abbiamo bisogno è una modalità per produrre 116 e 200 impulsi digitali per codificare bit 0 e 1 senza consumare tutta la potenza del microcontroller.

Lo si può sfare andando a modificare i counter timer di Arduino (circuito di basso livello) in modo tale che ogni tot microsecondi 
venga sollevato un interrupt e la funzione ad esso associato venga eseguita.

#### 2° parte

Per poter convertire i segnali logici DCC prodotti da Arduino in segnali bipolari DCC dobbiamo far uso di una tipologia di circuito 
noto come Full Bridge Driver.

Nel nostro caso utilizziamo il chip L298N.

Ha un input per collegare una fonte di energia: è importante dato che Arduino può produrre soltanto segnali logici a bassa corrente.

E' molto interessante della scheda L298N la possibilità di decidere le direzioni dei motori attraverso due pin: IN-1 e IN-2.

Questa funzionalità permetterebbe all'utente di cambiare la direzione del motore collegato: noi possiamo utilizzare questa funzionalità per qualcosa di completamente differente: ovvero per trasformare il segnale logico di Arduino 0-5 V ad un segnale bipolare DCC 12 V.

#### Dettagli implementativi

I pacchetti mandati ai Digital Decoder dovrebbero essere ripetuti più frequentemente possibile dato che un pacchetto potrebbe essersi perso 
(a causa della poca conduttività tra rotaie e locomotive ad esempio).

Un digital decoder deve essere in grado di ricevere ed eseguire i vari pacchetti indirizzati a lui, forniti in maniera tale che 
il tempo tra il packet end bit del primo pacchetto e il packet start bit del secondo pacchetto siano separati da almeno 5 millisecondi.
Se un decoder riceve una sequenza di bit non valida deve essere in grado di riconoscere il preambolo successivo valido come inizio di un nuovo pacchetto.

## STRUTTURA PROGETTO

### ARDUINO MASTER - STAZIONE DCC

#### Obiettivi

- Invia il pacchetto richiesto tramite il protocollo DCC con l'ausilio della scheda L298N.

- Salvataggio dello stato degli scambi nella memoria non volatile EEPROM prima che venga a mancare la corrente ad Arduino.

- Quando il Web Server non è disponibile gestisce display LCD e keypad, utili per impartire comandi.

- Quando il Web Server è disponibile riceve delle stringhe da ESP8266 ed esegue le relative istruzioni.

- Supporta degli scenari "Autopilot": gestisce autonomamente DELLE locomotive in scenari preimpostati riuscendo a dare le giuste precedenze ed evitare che le locomotive si scontrino grazie all'utilizzo di sensori IR posizionati sul tracciato in punti strategici.

#### Collegamenti

- Collegato con porta seriale all'Arduino Slave al quale impartisce i comandi tramite Firmata.

- Collegato con porta seriale all'ESP8266 da cui riceve i comandi.

### ARDUINO SLAVE - GESTIONE SCAMBI

#### Obiettivo

- Aziona i relay che a loro volta azionano i motori degli scambi.

#### Collegamenti

- Collegato con porta seriale ad Arduino Master da cui riceve i comandi da eseguire.

### ESP8266 - WEB SERVER

Utilizzo del protocollo REST.

#### Obiettivi

##### Visualizzare

- Sinottico del plastico con lo stato attuale degli scambi.
- Velocità, direzione e luci di tutte le locomotive.

##### Comandi

- Cambiare lo stato di ogni singolo scambio cliccandoci su.

- Cambiare velocità, direzione e luci di ogni locomotiva.

- STOP di emergenza

- Preset di scambi

- Spegnimento sicuro

#### Collegamenti

- Collegato con porta seriale ad Arduino (tramite un Logic Level Converter) al quale manda i comandi da eseguire tramite Firmata.

## MOTORI SCAMBI

Un motore per scambio è composto da due solenoidi.

A seconda del solenoide eccitato viene spostata una levetta che decide la direzione dello scambio.

Il relay serve per interfacciare il circuito di comando di Arduino con i circuiti di potenza dei solenoidi. 

### UTILIZZO DEI RELAY

I relay vengono utilizzati per tre motivi:

- Isolare i circuiti di comando dai circuiti di potenza. Separare circuiti di 5 V e 12 V. 
A noi servono relay con bobina da 5 V DC e che non assorbino più di 40 mA.

- Amplificare un segnale: Arduino chiude un contatto ma possono passare solo 40 mA (per motivi di corrente). 
Il contatto del relay deve supportare la corrente del solenoide: quindi 3 A.
3A è nominale ma potrebbe assorbire anche il doppio nei momenti critici perché il solenoide è un carico fortemente induttivo. Per questo abbiamo scelto i relay da 5 A.

- Logica: con i relè si possono realizzare porte logiche. 
Logica multiplexing cablata.

Nel nostro caso utilizziamo i relè per tutti e 3 i motivi.

### MULTIPLEXING RELAY

Ufficialmente avremo bisogno di 128 digital output di arduino.
Li possiamo raggruppare con delle “griglie”: ad esempio 64 x 2, meglio ancora 32 x 4. 
Il vantaggio che otterei sarebbe sempre più ridicolo a continuare a scendere.

Logica piano cartesiano: disponiamo i 128 relay su 4 righe da 32 colonne.

Ogni colonna verrà collegata ad un digital output di arduino, stessa cosa per ogni riga.
Per le righe sono necessari 4 relay ausiliari per generare un segnale di ground invece che di 5 V.

Proprio come un piano cartesiano, i singoli relay alimentati con un solo filo (o riga o colonna) non verrano eccitati perché manca il differenza di potenziale. 
Invece, andando a generare il segnale sia su una colonna che su una riga, solo il relay all’intersezione verrà eccitato perché ai suoi capi sarà presente una differenza di potenziale di 5 V.

## LOCALIZZAZIONE LOCOMOTIVE

Con i dispositivi in commercio non è possibile conoscere la posizione di ciascuna locomotiva all'interno del tracciato.

Tuttavia si può ottenere questo risultato sfruttando varie tipologie di sensori. 

### TIPOLOGIE DI SENSORI

#### Rilevazione ottica

##### Fotoresistenze

###### Vantaggi

- Economiche

###### Svantaggi

- Rilevano il passaggio di un treno senza identificarlo.

##### Sensori IR

###### Vantaggi

- Economici

- Rilevano il passaggio di un treno e lo identificano. 

#### Rilevamento magnetica

##### Sensori di Hall

###### Vantaggi

- Economici

- Rilevano il passaggio di un treno e riescono ad identificarlo grazie al numero di magneti presenti al di sotto della locomotiva.

###### Svantaggi

- Il sensore potrebbe avere difficoltà a rilevare il numero di magneti installati a causa della distorsione dei campi magnetici.

- Il campo magnetico creato dal motore della locomotiva potrebbe interferire con quelli creati dai magneti.

#### RFID

##### Vantaggi

- Rilevano il passaggio di un treno e lo identificano. 

##### Svantaggi

- Estremamente costosi. 20 € per ogni lettore.

### UN’APPLICAZIONE: L’AUTOPILOT

Nei casi in cui due o più locomotive percorrono circuiti separati ma aventi un tratto in comune, è necessaria una supervisione per evitare scontri.

Potendo localizzare ogni singola locomotiva è possibile delegare al sistema tale supervisione.

#### Funzionamento

Vengono posizionati dei sensori intorno alla sezione critica.
Uno pochi cm dall’incrocio (oppure dallo scambio dove inizia la sezione critica), l’altro sensore 50 - 70 cm prima.

Il primo treno che arriva al penultimo sensore prima dell’incrocio acquisisce la priorità fino a quando con il passaggio su un altro sensore dopo l’incrocio non conferma di averlo liberato.
Per tutto il tempo in cui un treno ha la priorità, leggo in continuazione l’ultima posizione dell’altro treno: se non è la penultima va avanti, se è la penultima rallento, se è l’ultimo lo fermo.
Quando l’incrocio è stato liberato riparte con la stessa velocità e direzione di prima.

## ISTRUZIONI PER L'USO

### LOGIN

Durante l'avvio, Arduino Master richiede di autenticarsi.

#### Tipologie di utenti

##### Amministratore

Ha accesso a tutte le funzionalità.

Ha accesso all'interfaccia grafica.

##### Ospite

Non ha accesso all'interfaccia grafica.

Velocità massima delle locomotive limitata. 

Numero massimo di locomotive in movimento contemporaneamente: 2.

##### Bambino

Non ha accesso all'interfaccia grafica.

Velocità massima delle locomotive limitata notevolmente. 

Numero massimo di locomotive in movimento contemporaneamente: 1.

### MENU TASTIERINO 

- TASTO A: Cambia Velocità Locomotiva

- TASTO B: Cambia Direzione Locomotiva

- TASTO C: Cambia Luci Locomotiva

- TASTO D: Cambia Locomotiva

- TASTO #: Cambia Scambio

- TASTO *: Stop di Emergenza

- TASTO 0: Spegnimento Sicuro

- TASTO 1: cambio utente

- TASTO 2: reset scambi (a destra)

### PRESET SCAMBI

Dall'interfaccia grafica presente sul sito è possibile azionare dei preset di scambi.

Arduino, rispettando i tempi di ricarica della CDU, azionerà tutti gli scambi (uno per volta) affinché rispettino il tracciato voluto.

## AMBIENTE DI SVILUPPO

- Importare le librerie aggiuntive presenti nella repository nella cartella "Librerie" nella cartella Arduino/Libraries (su Mac OS /Documenti/Arduino/Libraries)

- Non è garantita la compatibilità con versioni successivi di tali librerie.

## COMPONENTI UTILIZZATI

### SOFTWARE

#### Visual Studio Code

Editor di testo utilizzato, nettamente migliore rispetto l'IDE ufficiale di Arduino.

Seguendo le istruzioni presenti nella cartella Documentazione è possibile attivare l'Intellisense e la possibilità di compilare/uploadare il codice direttamente da Visual Studio Code.

#### Arduino IDE

Necessario per leggere dalle porte seriali dei microcontroller.

#### Illustrator

Utilizzato per creare l'interfaccia grafica.

Il file viene poi esportato in SVG e copiato inline nella pagina HTML.

#### Any Rail

Software con il quale ho disegnato in scala le tracce dei binari.

Ho poi importato tale immagine su Illustrator e ho ricalcato le tracce dei binari, trasformando di fatto un jpeg in svg. 

Compatibilità solamente con Windows.

### Git

Software di versioning utilizzato.

### Bit Bucket

Repository utilizzata.

### Librerie

#### Wire 

Librerie per la gestione del display LCD.

#### LiquidCrystal_I2C

Librerie per la gestione del display LCD.

#### EEPROM

Libreria per la gestione dela memoria non-volatile EEPROM.

#### Timer One

Libreria per la gestione degli interrupt.

https://playground.arduino.cc/Code/Timer1/

#### Keypad

Libreria per la gestione del tastierino numerico.

Credit: @author Mark Stanley, Alexander Brevig
@contact mstanley@technologist.com, alexanderbrevig@gmail.com

#### Firmata Marshaller

Libreria per la gestione della communicazione tra microcontroller.

### HARDWARE

#### Arduino Mega

##### Vantaggi rispetto ad Arduino Uno

- 10 Extra Analog Input Pin

- 9 Extra Serial I/O Pin che possono essere utilizzati per comunicazione seriale

- 32 Extra Digital I/O Pins

- 256 KB Flash Memory per memorizzare programma (mentre Arduino Uno soltanto 32 KB)

- 8 Kilobyte of SRAM (Arduino solo 2 KB): memorizzare variabili

- 4 Kilobyte di EEPROM (Arduino uno: 1 KB): mantenere informazioni anche quando non c'è corrente

- 5 counter timer (soltanto 3 per Arduino uno)

#### ESP8266 (ESP-12E Module)

##### Caratteristiche

- Modulo Wi-Fi integrato

- Comandi compatibili con Arduino

- 11 GPIO disponibili

- 1 Porta Seriale Hardware (eventualmente raggirabile tramite Software Serial)

##### Vantaggi rispetto ad Arduino + Wi-Fi Shield

- Più potente

- Più economico

- Memoria interna senza necessità di una SHIELD

#### Tastierino Numerico

Utile per fornire input ad Arduino quando il Web Server non è disponibile.

#### Display LCD I2C

Il modulo I2C riduce il numero di PIN da collegare ad Arduino.

##### Web Server Disponibile

Mostra l'indirizzo IP a cui l'ESP8266 si è connesso.

##### Web Server Non Disponibile

Mostra la situazione attuale delle locomotive e degli scambi.

#### Logic Level Converter

Converte i segnali da 3.3 V a 5 V e viceversa.

Svolge la funzione da intermediario nella comunicazione seriale tra Arduino Mega Master ed ESP8266.

#### L298N

Necessario per trasformare il segnale logico di Arduino 0-5 V ad un segnale bipolare DCC 12 V.

#### Sensori Infrarossi - TCRT5000

Vengono utilizzati per l'identificazione delle locomotive durante il passaggio in alcuni punti strategici, necessario per gestire le configurazioni autopilot.

#### Trasduttori di corrente - ACS712 5A

Vnegono utilizzati per misurare la corrente senza interruzione del circuito.

Ne utilizziamo 3:

- Alimentatore 19 volt che alimenta il tracciato delle locomotive e gli scambi.

- Alimentatore 5 volt che alimenta gli Arduino Mega ed ESP8266.

- Alimentatore 12 volt per luci

#### Relay - OMRON G6DN 5V

Gestiscono i motori degli scambi.

Sono pilotabili da Arduino perché 5 V 40 mA.

Il contatto di chiusura è di 3 A, sufficienti per azionare il motore dello scambio.

#### CDU

E' composto da due condensatori in grado di accumulare tutta l'energia necessaria per azionare uno scambio e rilasciarla in un unico impulso.

## PERCHE' QUESTO PROGETTO

### PASSIONE

Sin da quando ero molto piccolo i treni mi hanno sempre affascinato.

Mio padre si era studiato gli orari dei treni per accompagnarmi davanti ai passaggi a livello nel momento esatto in cui si stavano chiudendo, per poter vedere il treno sfrecciare a tutta velocità.

Nel 2001, a soli 4 anni di vita, ho ricevuto come regalo la mia prima locomotiva e i primi binari.

Ben presto, un unico anello nel quale poter far sfrecciare la locomotiva avanti e indietro non era abbastanza.
Ma si sa, per questo tipo di hobby, servono soldi, tempo ma sopratutto competenze.
Da piccolo avevo solo il tempo e mi era sempre stato detto "quando sarai un po' più grande lo potrai costruire da solo".

Beh, nel 2015 mi sono ricordato di questa frase e senza dubbio ero cresciuto abbastanza nel frattempo!
In poche settimane ho steso una quantità di binari notevole occupando sempre più spazio fino ad arrivare a 6 m x 1 m.

Tutto molto bello: decine e decine metri di binari, decine di scambi che davano possibilità ad altrettante combinazioni e varie locomotive che era possibile far viaggiare indipendentemente grazie alla centralina digitale appena comprata.

Rimaneva soltanto un "piccolo" problema: azionare gli scambi ed essere a conoscenza del loro stato a distanza...

Una soluzione c'era ma troppo complicata da realizzata: il progetto è dunque rimasto quindi incompiuto, ad un passo dalla fine...

Ci sono voluti 3 anni a studiare Informatica presso l'Università per avere le competenze necessarie per sbloccare la situazione.

### VANTAGGI

#### Problema

Come riuscire ad azionare i motori degli scambi a distanza e vederne il loro stato?

#### Soluzioni

##### Decoder Hornby

Comprare un decoder Hornby, a cui è possibile collegare 4 scambi, che si connette alla centralina Hornby principale attraverso la quale, con il tastierino, è possibile cambiare anche lo stato degli scambi.

###### Svantaggi

- Estremamente costoso: un decoder costa sugli 80 €.
Va bene per 4 scambi, avendo 64 scambi dovrei spendere 1280 €.

- Non hai un'interfaccia grafica con la quale visionare lo stato effettivo degli scambi.

- E' necessario inserire il numero dello scambio tramite tastierino numerico, questo obbliga a ricordare l'abbinamento numero scambio -> motore scambio ed è un operazione che richiede del tempo.

##### Deviatori Unipolari

E' possibile collegare i 2 solenoidi di ogni scambio ad un deviatore.

###### Svantaggi

- Un deviatore potrebbe memorizzare implicitamente la posizione dello scambio. Tuttavia i solenoidi degli scambi devono essere alimentati per un breve periodo di tempo, altrimenti si surriscaldano e si bruciano. 

Questo di fatto ci obbliga all'utilizzo dei commutatori al posto dei deviatori, non potendo più memorizzare la posizione dello scambio.

##### Commutatori Bipolari

Il commutatore bipolare aziona il solenoide dello scambio e contemporaneamente invia un segnale ad Arduino.
Quest'ultimo a quel punto può accendere un LED o l'altro per mostrare lo stato dello scambio.

Il tracciato viene disegnato su una tavoletta di legno, sulla quale verrano predisposti led e commutatori. 

###### Svantaggi

- E' necessario posizionare i commutatori e i LED sulla tavola di legno in corrispondenza del disegno.

- Cablaggio molto complesso e prono ad errori.

- Rimane difficile rappresentare un plastico di 6 m x 1 m su una tavoletta di legno di 60 cm (Scala 1:10) dovendo considerare anche lo spazio per i commutatori e LED.
Potrebbe peggiorare l'esperienza d'uso invece che migliorarla.

- Disegnare il tracciato su una tavoletta di legno rende il tutto estremamente poco flessibile per eventuali cambiamenti.

- Approccio, seppur migliore tra quelli presentati fino ad ora, rimane di realizzazione complessa: per questo motivo il progetto non è andato avanti.

##### ARDUINO E ESP8266 COME DCC COMMAND STATION

###### Caratteristiche

- Gestione del protocollo DCC. 

- Gestione motori scambi tramite relay.

- Gestione INPUT/OUTPUT tramite tastierino numerico e display LCD.

- Interfaccia grafica.

###### Vantaggi

- Non più dipendente dal decoder Hornby per la gestione delle locomotive: apre tanti scenari possibili (ad esempio autopilot)

- E' possibile impartire i comandi da qualsiasi dispositivo, anche mobile attraverso il sito web

- Interfaccia grafica pulita, aggiornabile facilmente e con la possibilità di effettuare zoom.

- Possibilità di impostare configurazioni prestabilite di tutti gli scambi.

##### Decoder

## BIBLIOGRAFIA

### PROGETTI DCC

- [ESP32 Command Station](https://github.com/atanisoft/ESP32CommandStation/tree/development)

- [NMRA DCC](https://www.arduinolibraries.info/libraries/nmra-dcc)

#### DCC++ Base Station

- [DCC++ Base Station](https://github.com/DccPlusPlus/BaseStation)

- [An Arduino DCC++ Base Station: The Hardware - Part 1 of 4](https://youtu.be/-nsVdpMhiTU)

- [An Arduino DCC++ Base Station: The Hardware - Part 2 of 4](https://youtu.be/rX05axpgROk)

- [An Arduino DCC++ Base Station: The Hardware - Part 3 of 4:](https://youtu.be/6BRvv45db9o)

- [An Arduino DCC++ Base Station: The Hardware - Part 4 of 4:](https://youtu.be/8cBQoI1NfHI)

### PROTOCOLLO DCC

- [Simple Coding for DCC Model Trains with Arduino:](https://youtu.be/VR0WDaPbDPk)

- [NMRA Standards and Recommended Practices](https://www.nmra.org/index-nmra-standards-and-recommended-practices)

- [Communications Standards For Digital Command Control](https://www.nmra.org/sites/default/files/s-92-2004-07.pdf)

- [Digital Command Control](https://it.wikipedia.org/wiki/Digital_Command_Control)

- [DCC Protocol Decoding](https://www.picotech.com/library/oscilloscopes/digital-command-control-dcc-protocol-decoding)

- [NMRA](https://dccwiki.com/NMRA)

### EEPROM

- [Come gestire la EEPROM](https://www.tomshw.it/altro/corso-base-di-arduino-come-gestire-la-eeprom/)

### FIRMATA

- [Firmata GitHub](https://github.com/firmata/arduino/blob/master/Firmata.h)

- [Arduino as client - Issue](https://github.com/firmata/arduino/issues/434#issuecomment-550277559)

- [Protocollo Firmata](https://github.com/firmata/protocol/blob/master/protocol.md)

- [Esempio Echo String](https://github.com/firmata/arduino/blob/master/examples/EchoString/EchoString.ino)

### GIT

- [Git in pochi passi](https://www.html.it/articoli/git-in-pochi-passi/)

- [Git Flow Cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/index.it_IT.html)

### MARKDOWN

- [Guida MarkDown](https://informaticabrutta.it/markdown-guida/)

- [Anteprima MarkDown](https://markdownlivepreview.com)

### MISURARE CORRENTE

- [Misurare una corrente DC o AC con Arduino](https://www.esperimentanda.com/come-misurare-una-corrente-dc-ac-con-arduino-sensore-di-hall-resistenza-di-shunt-continua-alternata/)

### MODULO L298N

- [Arduino DC Motor Control Tutorial – L298N](https://howtomechatronics.com/tutorials/arduino/arduino-dc-motor-control-tutorial-l298n-pwm-h-bridge/)

- [Interface L298N Using NodeMCU](https://www.instructables.com/id/Interface-L298N-Using-NodeMCU/)

### SPIFFS - File System

- [File System](https://arduino-esp8266.readthedocs.io/en/latest/filesystem.html)

- [Esempio Utilizzo SPIFFS](https://blog.squix.org/2015/08/esp8266arduino-playing-around-with.html)

- [Problema Lentezza SPIFFS](https://github.com/bblanchon/ArduinoJson/issues/939)

- [Arduino Stream Utils](https://github.com/bblanchon/ArduinoStreamUtils)

### TIMER E INTERRUPT ARDUINO

- [Timer and Interrupts](https://www.robotshop.com/community/forum/t/arduino-101-timers-and-interrupts/13072)

### TRAIN DETECTION

- [Train Detection](https://dccwiki.com/Train_Detection)

- [Guida Sensori IR](https://etechnophiles.com/beginners-guide-to-ir-sensor-and-how-to-use-it-with-arduino/)

### WEB SERVER

- [ESP8266 Web Server](https://randomnerdtutorials.com/esp8266-web-server/)

### VARIE

- [Converte Testo HTML in Stringa](https://tomeko.net/online_tools/cpp_text_escape.php?lang=en)