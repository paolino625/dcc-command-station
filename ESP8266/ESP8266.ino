


/*

WEB SERVER - ESP8266

by Paolo Calcagni

*/



// ***INCLUDE*** //



// Libreria per la gestione del WiFi

#include <ESP8266WiFi.h>

// Libreria per la gestione della communicazione utilizzando protocollo Firmata tra ESP8266 e Arduino Master

#include <FirmataMarshaller.h>

firmata::FirmataMarshaller marshaller;

// Libreria per la gestione di SPIFFS

#include <FS.h>

// Libreria per incrementare le prestazioni di SPIFFS. 
//Permette la lettura dei file presenti nel file system a blocchi da 64 byte e non byte per byte, incrementando notevolmente le prestazioni (circa 3 volte più veloce).

#include <StreamUtils.h> // https://github.com/bblanchon/ArduinoStreamUtils



// ***DEFINE*** //



// Numero di scambi nel plastico
// Numero scambi + 1 perché non partiamo da 0 ma da 1

#define NUMERO_SCAMBI 65

// Numero di locomotive

#define NUMERO_LOCOMOTIVE 10

// Se ad 1 stampa sulla porta seriale le informazioni relative alla connessione Wi-Fi.
// ATTENZIONE: se ad 1 disabilita il collegamento tra ESP8266 ed Arduino Master (è presente una sola porta seriale hardware)

#define DEBUGGING_ENABLE_PRINT_PORTA_SERIALE 0

// Se ad 1 stampa sulla pagina HTML lo stato degli scambi

#define DEBUGGING_HTML_SCAMBI 0

// Variabili utili per l'utilizzo di SPIFFS 

File binariFissi;
File elementiFissi;

ReadBufferingStream binariFissiBufferizzato{binariFissi, 64};
ReadBufferingStream elementiFissiBufferizzato{elementiFissi, 64};

int binariFissiFileAperto = 1;
int elementiFissiFileAperto = 1;

// Fin tanto che ESP8266 non riceve il comando di spegnimento sicuro, il Web Server rimane attivo.

int webServerAcceso = 1;

// Array di interi per memorizzare la posizione degli scambi.

int scambi[NUMERO_SCAMBI];

/* Creiamo una struttura che ci consenta di memorizzare le informazioni relative ad una locomotiva.
L'indirizzo della locomotiva non lo salviamo esplicitamente come variabile perché corrisponde alla posizione occupata nell'array treni.
Esempio: il treno alla posizione treni[0] avrà indirizzo 0. */

typedef struct{

    int velocita = 0; // velocità 128 step
    bool direzioneAvanti = true;
    bool luci = false;

 } Treno;

Treno treni[NUMERO_LOCOMOTIVE];

// Variabile per memorizzare il numero della locomotiva selezionata nell'interfaccia grafica

int numeroLocomotivaSelezionata = 1;

// Buffer per lettura della stringa in arrivo da Arduino Master

char stringaPosizioneScambi[200];

// Credenziali di accesso

const char* ssid     = "Paolo";
const char* password = "f4gg7h58";

// Setto la porta del web server a 80

WiFiServer server(80);

// Variabile per memorizzare la richiesta HTTP

String header;

// Tempo corrente
unsigned long currentTime = millis();

// Tempo precedente
unsigned long previousTime = 0; 

// Definisco il timeout in millisecondi
const long timeoutTime = 2000;

String realSize = String(ESP.getFlashChipRealSize());
String ideSize = String(ESP.getFlashChipSize());
bool flashCorrectlyConfigured = realSize.equals(ideSize);

// Funzione per far lampeggiare il LED interno di ESP8266
// N.B. LOW accende il led, con HIGH lo si spegne.

void blinkLedBuiltIn(){

  digitalWrite(2, LOW);
  delay(2000);
  digitalWrite(2, HIGH);
  delay(2000);

}

// Ricevo stringa da Arduino Master contenente lo stato degli scambi e li memorizzo nella struttura dati di ESP8266.

void ricevoPosizioneScambi(){

  Serial.begin(9600);

  while (Serial.available() <= 0){

    blinkLedBuiltIn();

  }

  Serial.readBytes(stringaPosizioneScambi, 200);

  char * token = strtok(stringaPosizioneScambi, " ");

  int i = 0;

  while(token != NULL){

    i++;

    int statoScambioStringa = atoi (token);

    if(i <= 66){

        scambi[i] = statoScambioStringa; //

    }

    token = strtok(NULL, " ");

  }

  Serial.end();

}



// ***SETUP*** //



void setup() {

    pinMode(2, OUTPUT);
    digitalWrite(2, HIGH);

    if(!DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

        ricevoPosizioneScambi();
        
    }
  
    if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

        Serial.begin(115200);
        Serial.print("In connessione...");
        Serial.println(ssid);

    }

    else{

        Serial.begin(57600);

        marshaller.begin(Serial);

    }

    // Connessione al Wi-Fi

    
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {

        delay(500);

        if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

        Serial.print(".");

        }
        
    }

    // Stampo indirizzo IP locale ed avvio un web serverPrint local IP address and start web server

    if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

        Serial.println("");
        Serial.println("WiFi Connesso.");
        Serial.println("Indirizzo IP: ");
        Serial.println(WiFi.localIP());

    }

    char stringaDaInviare[30] = "ip ";
    char indirizzoIP[20] = "";

    String stringaIP = WiFi.localIP().toString();
    stringaIP.toCharArray(indirizzoIP, 15);
    strcat(stringaDaInviare, indirizzoIP);

    marshaller.sendString(stringaDaInviare);

    server.begin();

    bool result = SPIFFS.begin();

    if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE && !result){

        Serial.println("SPIFFS non riuscito.");

    }

}



// ***LOOP*** //



void loop(){

    if(webServerAcceso){

        WiFiClient client = server.available();   // In ascolto di nuovi clienti

        if (client){     // Se un cliente si connette

            if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

                Serial.println("Nuovo Client.");

            }

            String currentLine = "";
            currentTime = millis();
            previousTime = currentTime;

            while (client.connected() && currentTime - previousTime <= timeoutTime) { // Loop mentre il cliente è connesso

                currentTime = millis();

                if (client.available()) {

                    char c = client.read();

                    if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

                        Serial.write(c);

                    }

                    header += c;

                    if (c == '\n') {

                        if (currentLine.length() == 0) {

                            // HTTP headers iniziano sempre con un codice di risposta (e.g. HTTP/1.1 200 OK)
                            // indicando il tipo di contenuto in modo tale che il cliente sa cosa sta per arrivare, poi una riga vuota:

                            client.println("HTTP/1.1 200 OK\n"
                            "Content-type:text/html\n"
                            "Connection: close\n"
                            "\n");


                            // ***VERIFICO SE E' RICHIESTA UN AZIONE DA PARTE DEL CLIENT*** //


                            if (header.indexOf("GET /scambio") >= 0) {

                                int scambioAzionato;

                                for(int i = NUMERO_SCAMBI + 1; i >= 0; i--){

                                char buffer[20] = "";
                                char numeroScambioStringa[5];

                                strcpy(buffer, "GET /scambio/");
                                itoa(i, numeroScambioStringa, 10);
                                strcat(buffer, numeroScambioStringa);

                                    if(header.indexOf(buffer) >= 0){

                                        scambioAzionato = i;

                                        scambi[scambioAzionato] = -scambi[scambioAzionato]; 
                                    
                                        char stringaDaInviare[20] = "scambio ";
                                        strcat(stringaDaInviare, numeroScambioStringa);
                                        marshaller.sendString(stringaDaInviare);

                                        break;

                                    }

                                }

                            }

                            if(header.indexOf("GET /stop") >= 0){

                                marshaller.sendString("stop");

                                for(int i = 1; i < NUMERO_LOCOMOTIVE; i++){

                                    treni[i].velocita = 0;

                                }

                            }

                            if(header.indexOf("GET /locomotiva") >= 0){

                                int locomotivaAzionata;

                                for(int i = 1; i <= NUMERO_LOCOMOTIVE ; i++){

                                    char buffer[20] = "";
                                    char numeroLocomotivaStringa[5] = "";

                                    strcpy(buffer, "GET /locomotiva/");
                                    itoa(i, numeroLocomotivaStringa, 10);
                                    strcat(buffer, numeroLocomotivaStringa);

                                        if(header.indexOf(buffer) >= 0){

                                            numeroLocomotivaSelezionata = i;

                                            break;

                                        }

                                }

                            }

                            if(header.indexOf("GET /velocita") >= 0){

                                int velocitaLocomotiva;

                                for(int i = 64; i > 0 ; i--){

                                    char buffer[20] = "";
                                    char velocitaLocomotivaStringa[5];

                                    strcpy(buffer, "GET /velocita/");
                                    itoa(i, velocitaLocomotivaStringa, 10);
                                    strcat(buffer, velocitaLocomotivaStringa);

                                    if(header.indexOf(buffer) >= 0){

                                        velocitaLocomotiva = i * 2;
                                        treni[numeroLocomotivaSelezionata].velocita = velocitaLocomotiva;

                                        char numeroLocomotivaStringa[5] = "";
                                        itoa(numeroLocomotivaSelezionata, numeroLocomotivaStringa, 10);

                                        char stringaDaInviare[30] = "velocita ";
                                        itoa(velocitaLocomotiva, velocitaLocomotivaStringa, 10);
                                        strcat(stringaDaInviare, numeroLocomotivaStringa);
                                        strcat(stringaDaInviare, " ");
                                        strcat(stringaDaInviare, velocitaLocomotivaStringa);
                                        marshaller.sendString(stringaDaInviare);

                                        break;

                                    }

                                }

                            }

                            if(header.indexOf("GET /luciLocomotiva") >= 0){

                                treni[numeroLocomotivaSelezionata].luci = !treni[numeroLocomotivaSelezionata].luci;

                                char stringaDaInviare[20] = "luciLocomotiva ";

                                char numeroLocomotivaSelezionataStringa[2];
                                itoa(numeroLocomotivaSelezionata, numeroLocomotivaSelezionataStringa, 10);
                                strcat(stringaDaInviare, numeroLocomotivaSelezionataStringa);
                                marshaller.sendString(stringaDaInviare);

                            }

                            if(header.indexOf("GET /direzioneLocomotiva") >= 0){

                                treni[numeroLocomotivaSelezionata].direzioneAvanti = !treni[numeroLocomotivaSelezionata].direzioneAvanti;

                                char stringaDaInviare[30] = "direzioneLocomotiva ";

                                char numeroLocomotivaSelezionataStringa[2];
                                itoa(numeroLocomotivaSelezionata, numeroLocomotivaSelezionataStringa, 10);
                                strcat(stringaDaInviare, numeroLocomotivaSelezionataStringa);
                                marshaller.sendString(stringaDaInviare);

                            }

                            if(header.indexOf("GET /setScambi") >= 0){

                                int numeroSetScambi;

                                for(int i = 11; i > 0 ; i--){

                                    char buffer[20] = "";
                                    char numeroSetScambiStringa[5];

                                    strcpy(buffer, "GET /setScambi/");
                                    itoa(i, numeroSetScambiStringa, 10);
                                    strcat(buffer, numeroSetScambiStringa);

                                    if(header.indexOf(buffer) >= 0){

                                        char stringaDaInviare[20] = "setScambi ";
                                        strcat(stringaDaInviare, numeroSetScambiStringa);
                                        marshaller.sendString(stringaDaInviare);

                                        switch(i){
                                        
                                        case 1:

                                        setAnelloEsterno();

                                        break;

                                        case 2:

                                        setAnelloInterno();

                                        break;

                                        case 3:

                                        setAnelloEsternoEsteso();

                                        break;

                                        case 4:

                                        setAnelloEsternoEstesissimoPrimoPiano();

                                        break;

                                        case 5:

                                        setAnelloEsternoEstesissimoSecondoPiano();

                                        break;

                                        case 6:

                                        setStazione1Binario3();

                                        break;

                                        case 7:

                                        setStazione2Binario0();

                                        break;

                                        case 8:

                                        setStazione2Binario1();

                                        break;

                                        case 9:

                                        setStazione2Binario2();

                                        break;

                                        case 10:

                                        setStazione2Binario3();

                                        break;

                                        }

                                        break;

                                    }

                                }

                            }

                            if(header.indexOf("GET /autopilot") >= 0){

                                int numeroAutopilot;

                                for(int i = 0; i < 6 ; i++){

                                    char buffer[20] = "";
                                    char numeroAutopilotStringa[5];

                                    strcpy(buffer, "GET /autopilot/");
                                    itoa(i, numeroAutopilotStringa, 10);
                                    strcat(buffer, numeroAutopilotStringa);

                                    if(header.indexOf(buffer) >= 0){

                                        char stringaDaInviare[20] = "autopilot ";
                                        strcat(stringaDaInviare, numeroAutopilotStringa);
                                        marshaller.sendString(stringaDaInviare);

                                        break;

                                    }

                                }

                            }

                            if(header.indexOf("GET /spegni") >= 0){
                                
                                marshaller.sendString("spegni");

                                webServerAcceso = 0;

                            }


                            // ***STAMPO PAGINA HTML*** //

                        
                            stampoHTMLheader(client);

                            stampoHTMLstyle(client);

                            stampoHTMLtitolo(client);

                            stampoHTMLscambi(client);

                            stampoHTMLbinariFissi(client);

                            stampoHTMLelementiFissi(client);
                        
                            stampoHTMLlocomotivaSelezionata(client);

                            stampoHTMLluciLocomotivaSelezionata(client);

                            stampoHTMLdirezioneLocomotivaSelezionata(client);

                            stampoHTMLvelocitaLocomotive(client);
                        
                            // Chiudo il file SVG
                            
                            client.print("</svg>\n""");

                            client.print("</body>");

                            if(DEBUGGING_HTML_SCAMBI){

                                for(int i = 1; i < NUMERO_SCAMBI; i++){

                                    char stringaProva[3]; 

                                    itoa(i, stringaProva, 10);
                                    client.print(stringaProva);

                                    client.print(" ");
                                    
                                    itoa(scambi[i], stringaProva, 10);
                                    client.print(stringaProva);

                                    client.print("\n");

                                }

                            }

                        // La risposta HTTP finisce con una linea bianca

                        client.println();

                        break;

                        } else {

                        currentLine = "";

                        }

                    } else if (c != '\r') {

                        currentLine += c;

                    }

                }

            }

            header = "";
            
            // Chiudo la connessione

            client.stop();

            if(DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){

                Serial.println("Client disconnesso.");
                Serial.println("");

            }

        }

    }

}

void setAnelloEsterno(){

    scambi[49] = 1;
    scambi[21] = -1;
    scambi[20] = -1;
    scambi[25] = 1;
    scambi[26] = 1;
    scambi[45] = -1;
    scambi[55] = -1;
    scambi[56] = -1;

}

void setAnelloInterno(){

    scambi[48] = 1;
    scambi[47] = 1;
    scambi[23] = -1;
    scambi[22] = -1;
    scambi[27] = 1;
    scambi[28] = 1;
    scambi[44] = -1;

}

void setAnelloEsternoEsteso(){

    scambi[49] = 1;
    scambi[21] = -1;
    scambi[20] = 1;
    scambi[16] = 1;
    scambi[33] = 1;
    scambi[55] = 1;
    scambi[56] = -1;

}

void setAnelloEsternoEstesissimoPrimoPiano(){

    scambi[49] = 1;
    scambi[21] = -1;
    scambi[20] = 1;
    scambi[19] = 1;
    scambi[17] = -1;
    scambi[4] = -1;
    scambi[3] = 1;
    scambi[1] = 1;
    scambi[9] = 1;
    scambi[24] = -1;
    scambi[25] = -1;
    scambi[26] = 1;
    scambi[45] = -1;
    scambi[55] = -1;
    scambi[56] = -1;

}

void setAnelloEsternoEstesissimoSecondoPiano(){

    scambi[24] = 1;
    scambi[25] = -1;
    scambi[26] = 1;
    scambi[45] = -1;
    scambi[55] = -1;
    scambi[56] = 1;
    scambi[57] = 1;
    scambi[62] = 1;
    scambi[59] = -1;

}

void setStazione1Binario3(){

    scambi[47] = -1;
    scambi[32] = 1;
    scambi[23] = 1;

}

void setStazione2Binario0(){

    scambi[19] = 1;
    scambi[17] = 1;
    scambi[2] = -1;
    scambi[1] = -1;

}

void setStazione2Binario1(){

    scambi[19] = 1;
    scambi[17] = -1;
    scambi[4] = -1;
    scambi[3] = -1;

}

void setStazione2Binario2(){

    scambi[19] = -1;
    scambi[18] = 1;
    scambi[13] = -1;
    scambi[12] = 1; 
    scambi[4] = 1;
    scambi[3] = -1;

}

void setStazione2Binario3(){

    scambi[19] = -1;
    scambi[18] = -1;
    scambi[15] = 1;
    scambi[13] = 1;
    scambi[12] = 1;
    scambi[4] = 1;
    scambi[3] = -1;

}

void stampoHTMLheader(WiFiClient client){
    
    // Opacità della pagina a 0 fin tanto che si carica tutta.

    client.print("<!DOCTYPE html!>\n"
        "<head>\n"
        "</head>\n"
        "<script>\n"
        "window.onload = function() {setTimeout(function(){document.body.style.opacity=\"100\";},500);};\n"
        "</script>\n"              
        "<body >\n"
        "<style> body  {opacity:0;}</style>");     

        /*
    
            client.print("<!DOCTYPE html!>\n"
                "<head>\n"
                "</head>\n"             
                "<body >\n");

        */            

}

void stampoHTMLstyle(WiFiClient client){

    client.print("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
        "<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n"
        "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n"
        "\t viewBox=\"0 0 2559 1342.2\" style=\"enable-background:new 0 0 2559 1342.2;\" xml:space=\"preserve\">\n"
        "<style type=\"text/css\">\n"
        "\t\t.st0{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st1{opacity:0.71;fill:none;stroke:#AFA5A5;stroke-width:4;stroke-miterlimit:10;enable-background:new    ;}\n"
        "\t.st2{fill:none;stroke:#ED1C24;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st3{fill:none;stroke:#39B54A;stroke-width:9;stroke-miterlimit:10;}\n"
        "\t.st4{fill:none;stroke:#FBB03B;stroke-width:9;stroke-miterlimit:10;}\n"
        "\t.st5{fill:none;stroke:#ED1C24;stroke-width:9;stroke-miterlimit:10;}\n"
        "\t.st6{fill:#0071BC;}\n"
        "\t.st7{font-family:'Helvetica-Bold';}\n"
        "\t.st8{font-size:50px;}\n"
        "\t.st9{fill:#FFE000;stroke:#ED1C24;stroke-miterlimit:10;}"
        "\t.st10{fill:#5E6363;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st11{fill:none;stroke:#ED1C24;stroke-width:20;stroke-miterlimit:10;}\n"
        "\t.st12{display:none;}\n"
        "\t.st13{display:inline;fill:none;stroke:#ED1C24;stroke-width:20;stroke-miterlimit:10;}\n"
        "\t.st14{fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st15{fill-rule:evenodd;clip-rule:evenodd;fill:#F70B0B;stroke:#F7B000;stroke-width:6;stroke-miterlimit:10;}\n"
        "\t.st16{fill:#FFFFFF;}\n"
        "\t.st17{font-family:'Helvetica';}\n"
        "\t.st18{font-size:95.4429px;}\n"
        "\t.st19{fill-rule:evenodd;clip-rule:evenodd;fill:#FF0000;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st20{font-size:27.4474px;}\n"
        "\t.st21{fill-rule:evenodd;clip-rule:evenodd;fill:#5E6363;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st22{display:none;fill-rule:evenodd;clip-rule:evenodd;fill:#FFDB08;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st23{font-size:27.45px;}\n"
        "\t.st24{fill:#F46719;}\n"
        "\t.st25{font-size:40px;}\n"
        "\t.st26{fill:#44B226;}\n"
        "\t.st27{fill:#6FC13A;stroke:#FFCE00;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st28{font-size:31.7368px;}\n"
        "\t.st29{fill:#3CB6BF;stroke:#FFCE00;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st30{fill:#BF973C;stroke:#FFCE00;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st31{fill:#F2690C;stroke:#FFCE00;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st32{fill:#116BED;stroke:#FFCE00;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st33{fill:#EFCA0F;stroke:#FF0000;stroke-width:4;stroke-miterlimit:10;}\n"
        "\t.st34{font-size:38px;}\n"
        "\t.st35{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:12;}\n"
        "\t.st36{fill:#FFFFFF;stroke:#000000;stroke-miterlimit:10;}\n"
        "\t.st37{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:11.7209,11.7209;}\n"
        "\t.st38{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:12.2326,12.2326;}\n"
        "\t.st39{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;stroke-dasharray:11.8947,11.8947;}\n"
        "\t.st40{fill:#F76E08;}\n"
        "\t.st41{font-size:70px;}\n"
        "</style>");

}

void stampoHTMLtitolo(WiFiClient client){

    client.print("\t<g id=\"Titolo\">\n"
        "\t\t<text transform=\"matrix(1 0 0 1 858.2977 88.147)\" class=\"st40 st7 st41\">DCC COMMAND STATION</text>\n"
        "\t</g>");

}

void stampoHTMLscambi(WiFiClient client){

    if(scambi[1] == 1){
                
        client.print("<a href = \"/scambio/1\"><path id=\"S1Sinistra_2_\" class=\"st1\" d=\"M197,153.9c19.6,0.3,39-3.3,58-12\"/></a>\n");
        client.print("<line id=\"S1Destra_2_\" class=\"st0\" x1=\"197\" y1=\"153.9\" x2=\"255\" y2=\"153.9\"/>\n");

    }

    else{

        client.print("<a href = \"/scambio/1\"><line id=\"S1Destra_2_\" class=\"st1\" x1=\"197\" y1=\"153.9\" x2=\"255\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S1Sinistra_2_\" class=\"st0\" d=\"M197,153.9c19.6,0.3,39-3.3,58-12\"/>\n");

    }

    if(scambi[2] == 1){
    
        client.print("<a href = \"/scambio/2\"><path id=\"S2Sinistra_2_\" class=\"st1\" d=\"M313,130.9c-21,0.4-40.2,4.3-58,11\"/></a>\n");
        client.print("<line id=\"S2Destra_2_\" class=\"st0\" x1=\"313\" y1=\"130.9\" x2=\"256\" y2=\"130.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/2\"><line id=\"S2Destra_2_\" class=\"st1\" x1=\"313\" y1=\"130.9\" x2=\"256\" y2=\"130.9\"/></a>\n");
        client.print("<path id=\"S2Sinistra_2_\" class=\"st0\" d=\"M313,130.9c-21,0.4-40.2,4.3-58,11\"/>\n");

    }

    if(scambi[3] == 1){
    
        client.print("<a href = \"/scambio/3\"><line id=\"S3Sinistra_2_\" class=\"st0\" x1=\"255\" y1=\"153.9\" x2=\"313\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S3Destra_2_\" class=\"st0\" d=\"M255,164.9c19-7.5,38.3-11.5,58-11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/3\"><path id=\"S3Destra_2_\" class=\"st1\" d=\"M255,164.9c19-7.5,38.3-11.5,58-11\"/></a>\n");
        client.print("<line id=\"S3Sinistra_2_\" class=\"st0\" x1=\"255\" y1=\"153.9\" x2=\"313\" y2=\"153.9\"/>\n");

    }

    if(scambi[4] == 1){
    
        client.print("<a href = \"/scambio/4\"><line id=\"S4Sinistra_2_\" class=\"st1\" x1=\"313\" y1=\"153.9\" x2=\"370\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S4Destra_2_\" class=\"st0\" d=\"M313,153.9c20.7-0.5,39.1,3.8,56,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/4\"><path id=\"S4Destra_2_\" class=\"st1\" d=\"M313,153.9c20.7-0.5,39.1,3.8,56,11\"/></a>\n");
        client.print("<line id=\"S4Sinistra_2_\" class=\"st0\" x1=\"313\" y1=\"153.9\" x2=\"370\" y2=\"153.9\"/>\n");

    }

    if(scambi[5] == 1){
    
        client.print("<a href = \"/scambio/5\"><path id=\"S5Sinistra_2_\" class=\"st1\" d=\"M291,230.9c25.2-1.7,49-6.4,72-10\"/></a>\n");
        client.print("<path id=\"S5Destra_2_\" class=\"st0\" d=\"M290,209.9c17.2,0.3,43.2,5,73,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/5\"><path id=\"S5Destra_2_\" class=\"st1\" d=\"M290,209.9c17.2,0.3,43.2,5,73,11\"/></a>\n");
        client.print("<path id=\"S5Sinistra_2_\" class=\"st0\" d=\"M291,230.9c25.2-1.7,49-6.4,72-10\"/>\n");

    }

    if(scambi[6] == 1){
    
        client.print("<a href = \"/scambio/6\"><path id=\"S6Sinistra_2_\" class=\"st1\" d=\"M362,231.9c19.3-8.1,38.6-11.4,58-11\"/></a>\n");
        client.print("<line id=\"S6Destra_2_\" class=\"st0\" x1=\"363\" y1=\"220.9\" x2=\"423.5\" y2=\"220.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/6\"><line id=\"S6Destra_2_\" class=\"st1\" x1=\"363\" y1=\"220.9\" x2=\"423.5\" y2=\"220.9\"/></a>\n");
        client.print("<path id=\"S6Sinistra_2_\" class=\"st0\" d=\"M362,231.9c19.3-8.1,38.6-11.4,58-11\"/>\n");

    }

    if(scambi[7] == 1){
    
        client.print("<a href = \"/scambio/7\"><line id=\"S7Sinistra_2_\" class=\"st1\" x1=\"303\" y1=\"263.9\" x2=\"250\" y2=\"285.9\"/></a>\n");
        client.print("<path id=\"S7Destra_2_\" class=\"st0\" d=\"M246,275.9c23.4-0.7,42.7-4.8,57-12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/7\"><path id=\"S7Destra_2_\" class=\"st1\" d=\"M246,275.9c23.4-0.7,42.7-4.8,57-12\"/></a>\n");
        client.print("<line id=\"S7Sinistra_2_\" class=\"st0\" x1=\"303\" y1=\"263.9\" x2=\"250\" y2=\"285.9\"/>\n");

    }

    if(scambi[8] == 1){
    
        client.print("<a href = \"/scambio/8\"><path id=\"S8Sinistra_2_\" class=\"st1\" d=\"M300,278.9c10-12.7,22-24.5,37-34\"/></a>\n");
        client.print("<path id=\"S8Destra_2_\" class=\"st0\" d=\"M303,263.9c11.8-5.8,23.1-12.2,34-19\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/8\"><path id=\"S8Destra_2_\" class=\"st1\" d=\"M303,263.9c11.8-5.8,23.1-12.2,34-19\"/></a>\n");
        client.print("<path id=\"S8Sinistra_2_\" class=\"st0\" d=\"M300,278.9c10-12.7,22-24.5,37-34\"/>\n");

    }

    if(scambi[9] == 1){
    
        client.print("<a href = \"/scambio/9\"><path id=\"S9Sinistra_2_\" class=\"st1\" d=\"M198,499.9c29,0,51.2-4.2,68-12\"/></a>\n");
        client.print("<line id=\"S9Destra_2_\" class=\"st0\" x1=\"198\" y1=\"499.9\" x2=\"284\" y2=\"499.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/9\"><line id=\"S9Destra_2_\" class=\"st1\" x1=\"198\" y1=\"499.9\" x2=\"284\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S9Sinistra_2_\" class=\"st0\" d=\"M198,499.9c29,0,51.2-4.2,68-12\"/>\n");

    }

    if(scambi[10] == 1){
    
        client.print("<a href = \"/scambio/10\"><path id=\"S10Sinistra_2_\" class=\"st1\" d=\"M266,487.9c26.6-13.3,46.8-29,61-47\"/></a>\n");
        client.print("<line id=\"S10Destra_2_\" class=\"st0\" x1=\"266\" y1=\"487.9\" x2=\"328\" y2=\"455.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/10\"><line id=\"S10Destra_2_\" class=\"st1\" x1=\"266\" y1=\"487.9\" x2=\"328\" y2=\"455.9\"/></a>\n");
        client.print("<path id=\"S10Sinistra_2_\" class=\"st0\" d=\"M266,487.9c26.6-13.3,46.8-29,61-47\"/>\n");

    }

    if(scambi[11] == 1){
    
        client.print("<a href = \"/scambio/11\"><line id=\"S11Sinistra_2_\" class=\"st1\" x1=\"327\" y1=\"440.9\" x2=\"362\" y2=\"394.9\"/></a>\n");
        client.print("<path id=\"S11Destra_2_\" class=\"st0\" d=\"M327,440.9c14.4-16.8,29.3-30.4,45-39\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/11\"><path id=\"S11Destra_2_\" class=\"st1\" d=\"M327,440.9c14.4-16.8,29.3-30.4,45-39\"/></a>\n");
        client.print("<line id=\"S11Sinistra_2_\" class=\"st0\" x1=\"327\" y1=\"440.9\" x2=\"362\" y2=\"394.9\"/>\n");

    }

    if(scambi[12] == 1){
    
        client.print("<a href = \"/scambio/12\"><line id=\"S12Sinistra_2_\" class=\"st1\" x1=\"477\" y1=\"220.9\" x2=\"530\" y2=\"220.9\"/></a>\n");
        client.print("<path id=\"S12Destra_2_\" class=\"st0\" d=\"M530,220.9c-17.3-1-34.7-4.7-52-11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/12\"><path id=\"S12Destra_2_\" class=\"st1\" d=\"M530,220.9c-17.3-1-34.7-4.7-52-11\"/></a>\n");
        client.print("<line id=\"S12Sinistra_2_\" class=\"st0\" x1=\"477\" y1=\"220.9\" x2=\"530\" y2=\"220.9\"/>\n");

    }

    if(scambi[13] == 1){
    
        client.print("<a href = \"/scambio/13\"><line id=\"S13Sinistra_2_\" class=\"st1\" x1=\"530\" y1=\"220.9\" x2=\"586\" y2=\"220.9\"/></a>\n");
        client.print("<path id=\"S13Destra_2_\" class=\"st0\" d=\"M530,220.9c18.1,0.1,36.4,3.9,55,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/13\"><path id=\"S13Destra_2_\" class=\"st1\" d=\"M530,220.9c18.1,0.1,36.4,3.9,55,12\"/></a>\n");
        client.print("<line id=\"S13Sinistra_2_\" class=\"st0\" x1=\"530\" y1=\"220.9\" x2=\"586\" y2=\"220.9\"/>\n");

    }

    if(scambi[14] == 1){
    
        client.print("<a href = \"/scambio/14\"><path id=\"S14Sinistra_2_\" class=\"st1\" d=\"M478,254.9c15.3-7,33.6-10.3,54-11\"/></a>\n");
        client.print("<line id=\"S14Destra_2_\" class=\"st0\" x1=\"532\" y1=\"243.9\" x2=\"477\" y2=\"243.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/14\"><line id=\"S14Destra_2_\" class=\"st1\" x1=\"532\" y1=\"243.9\" x2=\"477\" y2=\"243.9\"/></a>\n");
        client.print("<path id=\"S14Sinistra_2_\" class=\"st0\" d=\"M478,254.9c15.3-7,33.6-10.3,54-11\"/>\n");

    }

    if(scambi[15] == 1){
    
        client.print("<a href = \"/scambio/15\"><line id=\"S15Sinistra_2_\" class=\"st1\" x1=\"584\" y1=\"243.9\" x2=\"643\" y2=\"243.9\"/></a>\n");
        client.print("<path id=\"S15Destra_2_\" class=\"st0\" d=\"M643,243.9c-21.9-0.7-41.2-4.3-58-11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/15\"><path id=\"S15Destra_2_\" class=\"st1\" d=\"M643,243.9c-21.9-0.7-41.2-4.3-58-11\"/></a>\n");
        client.print("<line id=\"S15Sinistra_2_\" class=\"st0\" x1=\"584\" y1=\"243.9\" x2=\"643\" y2=\"243.9\"/>\n");

    }

    if(scambi[16] == 1){
    
        client.print("<a href = \"/scambio/16\"><path id=\"S16Sinistra_2_\" class=\"st1\" d=\"M476,294.9c25.1-2,43.9-6,57-12\"/></a>\n");
        client.print("<line id=\"S16Destra_2_\" class=\"st0\" x1=\"476\" y1=\"294.9\" x2=\"532\" y2=\"294.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/16\"><line id=\"S16Destra_2_\" class=\"st1\" x1=\"476\" y1=\"294.9\" x2=\"532\" y2=\"294.9\"/></a>\n");
        client.print("<path id=\"S16Sinistra_2_\" class=\"st0\" d=\"M476,294.9c25.1-2,43.9-6,57-12\"/>\n");

    }

    if(scambi[17] == 1){
    
        client.print("<a href = \"/scambio/17\"><line id=\"S17Sinistra_2_\" class=\"st1\" x1=\"1233\" y1=\"153.9\" x2=\"1175\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S17Destra_2_\" class=\"st0\" d=\"M1175,141.9c13.6,7.1,34,10.5,58,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/17\"><path id=\"S17Destra_2_\" class=\"st1\" d=\"M1175,141.9c13.6,7.1,34,10.5,58,12\"/></a>\n");
        client.print("<line id=\"S17Sinistra_2_\" class=\"st0\" x1=\"1233\" y1=\"153.9\" x2=\"1175\" y2=\"153.9\"/>\n");

    }

    if(scambi[18] == 1){
    
        client.print("<a href = \"/scambio/18\"><path id=\"S18Sinistra_2_\" class=\"st1\" d=\"M1233,164.9c-17.4,7.4-33.7,18.4-49,33\"/></a>\n");
        client.print("<line id=\"S18Destra_2_\" class=\"st0\" x1=\"1179\" y1=\"186.9\" x2=\"1233\" y2=\"164.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/18\"><line id=\"S18Destra_2_\" class=\"st1\" x1=\"1179\" y1=\"186.9\" x2=\"1233\" y2=\"164.9\"/></a>\n");
        client.print("<path id=\"S18Sinistra_2_\" class=\"st0\" d=\"M1233,164.9c-17.4,7.4-33.7,18.4-49,33\"/>\n");

    }

    if(scambi[19] == 1){
    
        client.print("<a href = \"/scambio/19\"><path id=\"S19Sinistra_2_\" class=\"st1\" d=\"M1233,164.9c22.6-8.4,39.8-11.2,57-11\"/></a>\n");
        client.print("<line id=\"S19Destra_2_\" class=\"st0\" x1=\"1233\" y1=\"153.9\" x2=\"1290\" y2=\"153.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/19\"><line id=\"S19Destra_2_\" class=\"st1\" x1=\"1233\" y1=\"153.9\" x2=\"1290\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S19Sinistra_2_\" class=\"st0\" d=\"M1233,164.9c22.6-8.4,39.8-11.2,57-11\"/>\n");

    }

    if(scambi[20] == 1){
    
        client.print("<a href = \"/scambio/20\"><path id=\"S20Sinistra_2_\" class=\"st1\" d=\"M1347,153.9c-18.5,2.3-36.8,6.7-55,14\"/></a>\n");
        client.print("<line id=\"S20Destra_2_\" class=\"st0\" x1=\"1290\" y1=\"153.9\" x2=\"1347\" y2=\"153.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/20\"><line id=\"S20Destra_2_\" class=\"st1\" x1=\"1290\" y1=\"153.9\" x2=\"1347\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S20Sinistra_2_\" class=\"st0\" d=\"M1347,153.9c-18.5,2.3-36.8,6.7-55,14\"/>\n");

    }

    if(scambi[21] == 1){
    
        client.print("<a href = \"/scambio/21\"><line id=\"S21Sinistra_2_\" class=\"st1\" x1=\"1373\" y1=\"153.9\" x2=\"1432\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S21Destra_2_\" class=\"st0\" d=\"M1373,153.9c24-0.1,41.7,4.3,59,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/21\"><path id=\"S21Destra_2_\" class=\"st1\" d=\"M1373,153.9c24-0.1,41.7,4.3,59,12\"/></a>\n");
        client.print("<line id=\"S21Sinistra_2_\" class=\"st0\" x1=\"1373\" y1=\"153.9\" x2=\"1432\" y2=\"153.9\"/>\n");

    }

    if(scambi[22] == 1){
    
        client.print("<a href = \"/scambio/22\"><line id=\"S22Sinistra_2_\" class=\"st1\" x1=\"1431\" y1=\"176.9\" x2=\"1489\" y2=\"176.9\"/></a>\n");
        client.print("<path id=\"S22Destra_2_\" class=\"st0\" d=\"M1432,165.9c20.7,7.8,39.5,11,57,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/22\"><path id=\"S22Destra_2_\" class=\"st1\" d=\"M1432,165.9c20.7,7.8,39.5,11,57,11\"/></a>\n");
        client.print("<line id=\"S22Sinistra_2_\" class=\"st0\" x1=\"1431\" y1=\"176.9\" x2=\"1489\" y2=\"176.9\"/>\n");

    }

    if(scambi[23] == 1){
    
        client.print("<a href = \"/scambio/23\"><line id=\"S23Sinistra_2_\" class=\"st1\" x1=\"1489\" y1=\"176.9\" x2=\"1546\" y2=\"176.9\"/></a>\n");
        client.print("<path id=\"S23Destra_2_\" class=\"st0\" d=\"M1489,176.9c20.4-0.2,39.5,3.4,57,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/23\"><path id=\"S23Destra_2_\" class=\"st1\" d=\"M1489,176.9c20.4-0.2,39.5,3.4,57,11\"/></a>\n");
        client.print("<line id=\"S23Sinistra_2_\" class=\"st0\" x1=\"1489\" y1=\"176.9\" x2=\"1546\" y2=\"176.9\"/>\n");

    }

    if(scambi[24] == 1){
    
        client.print("<a href = \"/scambio/24\"><line id=\"S24Sinistra_2_\" class=\"st1\" x1=\"1232\" y1=\"499.9\" x2=\"1318\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S24Destra_2_\" class=\"st0\" d=\"M1230,488.9c19.8,9.3,50.2,12.3,88,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/24\"><path id=\"S24Destra_2_\" class=\"st1\" d=\"M1230,488.9c19.8,9.3,50.2,12.3,88,11\"/></a>\n");
        client.print("<line id=\"S24Sinistra_2_\" class=\"st0\" x1=\"1232\" y1=\"499.9\" x2=\"1318\" y2=\"499.9\"/>\n");

    }

    if(scambi[25] == 1){
    
        client.print("<a href = \"/scambio/25\"><line id=\"S25Sinistra_2_\" class=\"st1\" x1=\"1318\" y1=\"499.9\" x2=\"1362\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S25Destra_2_\" class=\"st0\" d=\"M1318,493.9c13.2,2.4,28.1,4.4,44,6\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/25\"><path id=\"S25Destra_2_\" class=\"st1\" d=\"M1318,493.9c13.2,2.4,28.1,4.4,44,6\"/></a>\n");
        client.print("<line id=\"S25Sinistra_2_\" class=\"st0\" x1=\"1318\" y1=\"499.9\" x2=\"1362\" y2=\"499.9\"/>\n");

    }

    if(scambi[26] == 1){
    
        client.print("<a href = \"/scambio/26\"><path id=\"S26Sinistra_2_\" class=\"st1\" d=\"M1362,499.9c19.8-0.1,38.4-4.3,56-12\"/></a>\n");
        client.print("<line id=\"S26Destra_2_\" class=\"st0\" x1=\"1362\" y1=\"499.9\" x2=\"1418\" y2=\"499.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/26\"><line id=\"S26Destra_2_\" class=\"st1\" x1=\"1362\" y1=\"499.9\" x2=\"1418\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S26Sinistra_2_\" class=\"st0\" d=\"M1362,499.9c19.8-0.1,38.4-4.3,56-12\"/>\n");

    }

    if(scambi[27] == 1){
    
        client.print("<a href = \"/scambio/27\"><path id=\"S27Sinistra_2_\" class=\"st1\" d=\"M1418,487.9c21.9-9.1,40.9-12.1,58-11\"/></a>\n");
        client.print("<line id=\"S27Destra_2_\" class=\"st0\" x1=\"1418\" y1=\"476.9\" x2=\"1476\" y2=\"476.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/27\"><line id=\"S27Destra_2_\" class=\"st1\" x1=\"1418\" y1=\"476.9\" x2=\"1476\" y2=\"476.9\"/></a>\n");
        client.print("<path id=\"S27Sinistra_2_\" class=\"st0\" d=\"M1418,487.9c21.9-9.1,40.9-12.1,58-11\"/>\n");

    }

    if(scambi[28] == 1){
    
        client.print("<a href = \"/scambio/28\"><path id=\"S28Sinistra_2_\" class=\"st1\" d=\"M1476,476.9c26.8-1.7,45.2-5.9,57-12\"/></a>\n");
        client.print("<line id=\"S28Destra_2_\" class=\"st0\" x1=\"1476\" y1=\"476.9\" x2=\"1533\" y2=\"476.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/28\"><line id=\"S28Destra_2_\" class=\"st1\" x1=\"1476\" y1=\"476.9\" x2=\"1533\" y2=\"476.9\"/></a>\n");
        client.print("<path id=\"S28Sinistra_2_\" class=\"st0\" d=\"M1476,476.9c26.8-1.7,45.2-5.9,57-12\"/>\n");

    }

    if(scambi[29] == 1){
    
        client.print("<a href = \"/scambio/29\"><path id=\"S29Sinistra_2_\" class=\"st1\" d=\"M1533,464.9c20.5-8,39.7-11.4,58-11\"/></a>\n");
        client.print("<line id=\"S29Destra_2_\" class=\"st0\" x1=\"1533\" y1=\"453.9\" x2=\"1591\" y2=\"453.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/29\"><line id=\"S29Destra_2_\" class=\"st1\" x1=\"1533\" y1=\"453.9\" x2=\"1591\" y2=\"453.9\"/></a>\n");
        client.print("<path id=\"S29Sinistra_2_\" class=\"st0\" d=\"M1533,464.9c20.5-8,39.7-11.4,58-11\"/>\n");

    }

    if(scambi[30] == 1){
    
        client.print("<a href = \"/scambio/30\"><path id=\"S30Sinistra_2_\" class=\"st1\" d=\"M1591,453.9c27-1.4,45.3-5.3,57-11\"/></a>\n");
        client.print("<line id=\"S30Destra_2_\" class=\"st0\" x1=\"1591\" y1=\"453.9\" x2=\"1648\" y2=\"453.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/30\"><line id=\"S30Destra_2_\" class=\"st1\" x1=\"1591\" y1=\"453.9\" x2=\"1648\" y2=\"453.9\"/></a>\n");
        client.print("<path id=\"S30Sinistra_2_\" class=\"st0\" d=\"M1591,453.9c27-1.4,45.3-5.3,57-11\"/>\n");

    }

    if(scambi[31] == 1){
    
        client.print("<a href = \"/scambio/31\"><line id=\"S31Sinistra_2_\" class=\"st1\" x1=\"1553\" y1=\"226.9\" x2=\"1611\" y2=\"226.9\"/></a>\n");
        client.print("<path id=\"S31Destra_2_\" class=\"st0\" d=\"M1554,214.9c20,8.6,38.7,11.6,57,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/31\"><path id=\"S31Destra_2_\" class=\"st1\" d=\"M1554,214.9c20,8.6,38.7,11.6,57,12\"/></a>\n");
        client.print("<line id=\"S31Sinistra_2_\" class=\"st0\" x1=\"1553\" y1=\"226.9\" x2=\"1611\" y2=\"226.9\"/>\n");

    }

    if(scambi[32] == 1){
    
        client.print("<a href = \"/scambio/32\"><line id=\"S32Sinistra_2_\" class=\"st1\" x1=\"1610\" y1=\"226.9\" x2=\"1668\" y2=\"226.9\"/></a>\n");
        client.print("<path id=\"S32Destra_2_\" class=\"st0\" d=\"M1613,215.9c18.1,6.6,36.7,10.4,56,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/32\"><path id=\"S32Destra_2_\" class=\"st1\" d=\"M1613,215.9c18.1,6.6,36.7,10.4,56,11\"/></a>\n");
        client.print("<line id=\"S32Sinistra_2_\" class=\"st0\" x1=\"1610\" y1=\"226.9\" x2=\"1668\" y2=\"226.9\"/>\n");

    }

    if(scambi[33] == 1){
    
        client.print("<a href = \"/scambio/33\"><line id=\"S33Sinistra_2_\" class=\"st1\" x1=\"1586\" y1=\"294.9\" x2=\"1645\" y2=\"294.9\"/></a>\n");
        client.print("<path id=\"S33Destra_2_\" class=\"st0\" d=\"M1586,294.9c27.3,3.3,43.8,8.1,55,14\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/33\"><path id=\"S33Destra_2_\" class=\"st1\" d=\"M1586,294.9c27.3,3.3,43.8,8.1,55,14\"/></a>\n");
        client.print("<line id=\"S33Sinistra_2_\" class=\"st0\" x1=\"1586\" y1=\"294.9\" x2=\"1645\" y2=\"294.9\"/>\n");

    }

    if(scambi[34] == 1){
    
        client.print("<a href = \"/scambio/34\"><path id=\"S34Sinistra_2_\" class=\"st1\" d=\"M1732,283.9c20.9-8.7,40.2-12.8,58-12\"/></a>\n");
        client.print("<line id=\"S34Destra_2_\" class=\"st0\" x1=\"1732\" y1=\"271.9\" x2=\"1790\" y2=\"271.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/34\"><line id=\"S34Destra_2_\" class=\"st1\" x1=\"1732\" y1=\"271.9\" x2=\"1790\" y2=\"271.9\"/></a>\n");
        client.print("<path id=\"S34Sinistra_2_\" class=\"st0\" d=\"M1732,283.9c20.9-8.7,40.2-12.8,58-12\"/>\n");

    }

    if(scambi[35] == 1){
    
        client.print("<a href = \"/scambio/35\"><path id=\"S35Sinistra_2_\" class=\"st1\" d=\"M1789,271.9c28.1-1.7,46.8-5.9,58-11\"/></a>\n");
        client.print("<line id=\"S35Destra_2_\" class=\"st0\" x1=\"1790\" y1=\"271.9\" x2=\"1847\" y2=\"271.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/35\"><line id=\"S35Destra_2_\" class=\"st1\" x1=\"1790\" y1=\"271.9\" x2=\"1847\" y2=\"271.9\"/></a>\n");
        client.print("<path id=\"S35Sinistra_2_\" class=\"st0\" d=\"M1789,271.9c28.1-1.7,46.8-5.9,58-11\"/>\n");

    }

    if(scambi[36] == 1){
    
        client.print("<a href = \"/scambio/36\"><path id=\"S36Sinistra_2_\" class=\"st1\" d=\"M1674,294.9c31-2.5,50.4-7.7,58-11\"/></a>\n");
        client.print("<line id=\"S36Destra_2_\" class=\"st0\" x1=\"1674\" y1=\"294.9\" x2=\"1732\" y2=\"294.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/36\"><line id=\"S36Destra_2_\" class=\"st1\" x1=\"1674\" y1=\"294.9\" x2=\"1732\" y2=\"294.9\"/></a>\n");
        client.print("<path id=\"S36Sinistra_2_\" class=\"st0\" d=\"M1674,294.9c31-2.5,50.4-7.7,58-11\"/>\n");

    }

    if(scambi[37] == 1){
    
        client.print("<a href = \"/scambio/37\"><line id=\"S37Sinistra_2_\" class=\"st1\" x1=\"1732\" y1=\"294.9\" x2=\"1790\" y2=\"294.9\"/></a>\n");
        client.print("<path id=\"S37Destra_2_\" class=\"st0\" d=\"M1732,294.9c22.7,0.5,41.9,4.3,58,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/37\"><path id=\"S37Destra_2_\" class=\"st1\" d=\"M1732,294.9c22.7,0.5,41.9,4.3,58,11\"/></a>\n");
        client.print("<line id=\"S37Sinistra_2_\" class=\"st0\" x1=\"1732\" y1=\"294.9\" x2=\"1790\" y2=\"294.9\"/>\n");

    }

    if(scambi[38] == 1){
    
        client.print("<a href = \"/scambio/38\"><line id=\"S38Sinistra_2_\" class=\"st1\" x1=\"1645\" y1=\"368.9\" x2=\"1703\" y2=\"368.9\"/></a>\n");
        client.print("<path id=\"S38Destra_2_\" class=\"st0\" d=\"M1645,357.9c16.4,7.2,35.4,10.9,58,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/38\"><path id=\"S38Destra_2_\" class=\"st1\" d=\"M1645,357.9c16.4,7.2,35.4,10.9,58,11\"/></a>\n");
        client.print("<line id=\"S38Sinistra_2_\" class=\"st0\" x1=\"1645\" y1=\"368.9\" x2=\"1703\" y2=\"368.9\"/>\n");

    }

    if(scambi[39] == 1){
    
        client.print("<a href = \"/scambio/39\"><path id=\"S39Sinistra_2_\" class=\"st1\" d=\"M1702,380.9c20.1-7.6,39.5-11.6,58-12\"/></a>\n");
        client.print("<line id=\"S39Destra_2_\" class=\"st0\" x1=\"1703\" y1=\"368.9\" x2=\"1759\" y2=\"368.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/39\"><line id=\"S39Destra_2_\" class=\"st1\" x1=\"1703\" y1=\"368.9\" x2=\"1759\" y2=\"368.9\"/></a>\n");
        client.print("<path id=\"S39Sinistra_2_\" class=\"st0\" d=\"M1702,380.9c20.1-7.6,39.5-11.6,58-12\"/>\n");

    }

    if(scambi[40] == 1){
    
        client.print("<a href = \"/scambio/40\"><path id=\"S40Sinistra_2_\" class=\"st1\" d=\"M1645,391.9c23-1,42.3-4.6,57-11\"/></a>\n");
        client.print("<line id=\"S40Destra_2_\" class=\"st0\" x1=\"1645\" y1=\"391.9\" x2=\"1702\" y2=\"391.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/40\"><line id=\"S40Destra_2_\" class=\"st1\" x1=\"1645\" y1=\"391.9\" x2=\"1702\" y2=\"391.9\"/></a>\n");
        client.print("<path id=\"S40Sinistra_2_\" class=\"st0\" d=\"M1645,391.9c23-1,42.3-4.6,57-11\"/>\n");

    }

    if(scambi[41] == 1){
    
        client.print("<a href = \"/scambio/41\"><path id=\"S41Sinistra_2_\" class=\"st1\" d=\"M1629,426.9c18.4-6.9,37.8-10.9,58-12\"/></a>\n");
        client.print("<line id=\"S41Destra_2_\" class=\"st0\" x1=\"1629\" y1=\"414.9\" x2=\"1687\" y2=\"414.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/41\"><line id=\"S41Destra_2_\" class=\"st1\" x1=\"1629\" y1=\"414.9\" x2=\"1687\" y2=\"414.9\"/></a>\n");
        client.print("<path id=\"S41Sinistra_2_\" class=\"st0\" d=\"M1629,426.9c18.4-6.9,37.8-10.9,58-12\"/>\n");

    }

    if(scambi[42] == 1){
    
        client.print("<a href = \"/scambio/42\"><path id=\"S42Sinistra_2_\" class=\"st1\" d=\"M1688,425.9c17.4-7.2,36.1-10.9,56-11\"/></a>\n");
        client.print("<line id=\"S42Destra_2_\" class=\"st0\" x1=\"1687\" y1=\"414.9\" x2=\"1745\" y2=\"414.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/42\"><line id=\"S42Destra_2_\" class=\"st1\" x1=\"1687\" y1=\"414.9\" x2=\"1745\" y2=\"414.9\"/></a>\n");
        client.print("<path id=\"S42Sinistra_2_\" class=\"st0\" d=\"M1688,425.9c17.4-7.2,36.1-10.9,56-11\"/>\n");

    }

    if(scambi[43] == 1){
    
        client.print("<a href = \"/scambio/43\"><line id=\"S43Sinistra_2_\" class=\"st1\" x1=\"1801\" y1=\"414.9\" x2=\"1859\" y2=\"414.9\"/></a>\n");
        client.print("<path id=\"S43Destra_2_\" class=\"st0\" d=\"M1801,403.9c15.7,6.9,35,10.5,58,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/43\"><path id=\"S43Destra_2_\" class=\"st1\" d=\"M1801,403.9c15.7,6.9,35,10.5,58,11\"/></a>\n");
        client.print("<line id=\"S43Sinistra_2_\" class=\"st0\" x1=\"1801\" y1=\"414.9\" x2=\"1859\" y2=\"414.9\"/>\n");

    }

    if(scambi[44] == 1){
    
        client.print("<a href = \"/scambio/44\"><line id=\"S44Sinistra_2_\" class=\"st1\" x1=\"1762\" y1=\"476.9\" x2=\"1820\" y2=\"476.9\"/></a>\n");
        client.print("<path id=\"S44Destra_2_\" class=\"st0\" d=\"M1762,476.9c22.2,0.5,41.7,4.1,58,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/44\"><path id=\"S44Destra_2_\" class=\"st1\" d=\"M1762,476.9c22.2,0.5,41.7,4.1,58,11\"/></a>\n");
        client.print("<line id=\"S44Sinistra_2_\" class=\"st0\" x1=\"1762\" y1=\"476.9\" x2=\"1820\" y2=\"476.9\"/>\n");

    }

    if(scambi[45] == 1){
    
        client.print("<a href = \"/scambio/45\"><line id=\"S45Sinistra_2_\" class=\"st1\" x1=\"1820\" y1=\"499.9\" x2=\"1879\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S45Destra_2_\" class=\"st0\" d=\"M1820,487.9c18.6,7.5,38,11.4,58,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/45\"><path id=\"S45Destra_2_\" class=\"st1\" d=\"M1820,487.9c18.6,7.5,38,11.4,58,12\"/></a>\n");
        client.print("<line id=\"S45Sinistra_2_\" class=\"st0\" x1=\"1820\" y1=\"499.9\" x2=\"1879\" y2=\"499.9\"/>\n");

    }

    if(scambi[46] == 1){
    
        client.print("<a href = \"/scambio/46\"><line id=\"S46Sinistra_2_\" class=\"st1\" x1=\"1874\" y1=\"248.9\" x2=\"1927\" y2=\"226.9\"/></a>\n");
        client.print("<path id=\"S46Destra_2_\" class=\"st0\" d=\"M1874,248.9c16.9-7,35.9-10.7,57-11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/46\"><path id=\"S46Destra_2_\" class=\"st1\" d=\"M1874,248.9c16.9-7,35.9-10.7,57-11\"/></a>\n");
        client.print("<line id=\"S46Sinistra_2_\" class=\"st0\" x1=\"1874\" y1=\"248.9\" x2=\"1927\" y2=\"226.9\"/>\n");

    }

    if(scambi[47] == 1){
    
        client.print("<a href = \"/scambio/47\"><path id=\"S47Sinistra_2_\" class=\"st1\" d=\"M1964,187.9c20.4-7.9,39.3-11.3,57-11\"/></a>\n");
        client.print("<line id=\"S47Destra_2_\" class=\"st0\" x1=\"1964\" y1=\"176.9\" x2=\"2021\" y2=\"176.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/47\"><line id=\"S47Destra_2_\" class=\"st1\" x1=\"1964\" y1=\"176.9\" x2=\"2021\" y2=\"176.9\"/></a>\n");
        client.print("<path id=\"S47Sinistra_2_\" class=\"st0\" d=\"M1964,187.9c20.4-7.9,39.3-11.3,57-11\"/>\n");

    }

    if(scambi[48] == 1){
    
        client.print("<a href = \"/scambio/48\"><path id=\"S48Sinistra_2_\" class=\"st1\" d=\"M2021,176.9c28.1-2.7,48.3-7.5,58-12\"/></a>\n");
        client.print("<line id=\"S48Destra_2_\" class=\"st0\" x1=\"2021\" y1=\"176.9\" x2=\"2079\" y2=\"176.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/48\"><line id=\"S48Destra_2_\" class=\"st1\" x1=\"2021\" y1=\"176.9\" x2=\"2079\" y2=\"176.9\"/></a>\n");
        client.print("<path id=\"S48Sinistra_2_\" class=\"st0\" d=\"M2021,176.9c28.1-2.7,48.3-7.5,58-12\"/>\n");

    }

    if(scambi[49] == 1){
    
        client.print("<a href = \"/scambio/49\"><path id=\"S49Sinistra_2_\" class=\"st1\" d=\"M2079,164.9c17-7.4,36.9-11.3,58-11\"/></a>\n");
        client.print("<line id=\"S49Destra_2_\" class=\"st0\" x1=\"2080\" y1=\"153.9\" x2=\"2137\" y2=\"153.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/49\"><line id=\"S49Destra_2_\" class=\"st1\" x1=\"2080\" y1=\"153.9\" x2=\"2137\" y2=\"153.9\"/></a>\n");
        client.print("<path id=\"S49Sinistra_2_\" class=\"st0\" d=\"M2079,164.9c17-7.4,36.9-11.3,58-11\"/>\n");

    }

    if(scambi[50] == 1){
    
        client.print("<a href = \"/scambio/50\"><line id=\"S50OFF_2_\" class=\"st0\" x1=\"2005\" y1=\"294.9\" x2=\"2062\" y2=\"294.9\"/></a>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/50\"><line id=\"S50ON_2_\" class=\"st2\" x1=\"2005\" y1=\"294.9\" x2=\"2062\" y2=\"294.9\"/></a>\n");

    }

    if(scambi[51] == 1){
    
        client.print("<a href = \"/scambio/51\"><line id=\"S51Sinistra_2_\" class=\"st1\" x1=\"1817\" y1=\"368.9\" x2=\"1875\" y2=\"368.9\"/></a>\n");
        client.print("<path id=\"S51Destra_2_\" class=\"st0\" d=\"M1816,368.9c21.9,0.5,41.6,4.5,59,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/51\"><path id=\"S51Destra_2_\" class=\"st1\" d=\"M1816,368.9c21.9,0.5,41.6,4.5,59,12\"/></a>\n");
        client.print("<line id=\"S51Sinistra_2_\" class=\"st0\" x1=\"1817\" y1=\"368.9\" x2=\"1875\" y2=\"368.9\"/>\n");

    }

    if(scambi[52] == 1){
    
        client.print("<a href = \"/scambio/52\"><line id=\"S52OFF_2_\" class=\"st0\" x1=\"1990\" y1=\"368.9\" x2=\"2048\" y2=\"368.9\"/></a>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/52\"><line id=\"S52ON_2_\" class=\"st2\" x1=\"1990\" y1=\"368.9\" x2=\"2048\" y2=\"368.9\"/></a>\n");

    }

    if(scambi[53] == 1){
    
        client.print("<a href = \"/scambio/53\"><line id=\"S53Sinistra_2_\" class=\"st1\" x1=\"1929\" y1=\"414.9\" x2=\"1987\" y2=\"414.9\"/></a>\n");
        client.print("<path id=\"S53Destra_2_\" class=\"st0\" d=\"M1929,414.9c21,0.1,40,3.7,57,11\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/53\"><path id=\"S53Destra_2_\" class=\"st1\" d=\"M1929,414.9c21,0.1,40,3.7,57,11\"/></a>\n");
        client.print("<line id=\"S53Sinistra_2_\" class=\"st0\" x1=\"1929\" y1=\"414.9\" x2=\"1987\" y2=\"414.9\"/>\n");

    }

    if(scambi[54] == 1){
    
        client.print("<a href = \"/scambio/54\"><path id=\"S54Sinistra_2_\" class=\"st1\" d=\"M1987,414.9c24.4-1.6,42.9-5.4,57-11\"/></a>\n");
        client.print("<line id=\"S54Destra_2_\" class=\"st0\" x1=\"1987\" y1=\"414.9\" x2=\"2044\" y2=\"414.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/54\"><line id=\"S54Destra_2_\" class=\"st1\" x1=\"1987\" y1=\"414.9\" x2=\"2044\" y2=\"414.9\"/></a>\n");
        client.print("<path id=\"S54Sinistra_2_\" class=\"st0\" d=\"M1987,414.9c24.4-1.6,42.9-5.4,57-11\"/>\n");

    }

    if(scambi[55] == 1){
    
        client.print("<a href = \"/scambio/55\"><line id=\"S55Sinistra_2_\" class=\"st1\" x1=\"2080\" y1=\"499.9\" x2=\"2136\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S55Destra_2_\" class=\"st0\" d=\"M2080,487.9c20.6,9.6,39,12.8,56,12\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/55\"><path id=\"S55Destra_2_\" class=\"st1\" d=\"M2080,487.9c20.6,9.6,39,12.8,56,12\"/></a>\n");
        client.print("<line id=\"S55Sinistra_2_\" class=\"st0\" x1=\"2080\" y1=\"499.9\" x2=\"2136\" y2=\"499.9\"/>\n");

    }

    if(scambi[56] == 1){
    
        client.print("<a href = \"/scambio/56\"><path id=\"S56Sinistra_2_\" class=\"st1\" d=\"M2136,499.9c25.8-1.9,47.2-5.7,61-11\"/></a>\n");
        client.print("<line id=\"S56Destra_2_\" class=\"st0\" x1=\"2136\" y1=\"499.9\" x2=\"2197\" y2=\"499.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/56\"><line id=\"S56Destra_2_\" class=\"st1\" x1=\"2136\" y1=\"499.9\" x2=\"2197\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S56Sinistra_2_\" class=\"st0\" d=\"M2136,499.9c25.8-1.9,47.2-5.7,61-11\"/>\n");

    }

    if(scambi[57] == 1){
    
        client.print("<a href = \"/scambio/57\"><path id=\"S57Sinistra_2_\" class=\"st1\" d=\"M2197,499.9c29.3-4.2,48.5-8.1,58-12\"/></a>\n");
        client.print("<line id=\"S57Destra_2_\" class=\"st0\" x1=\"2197\" y1=\"499.9\" x2=\"2255\" y2=\"499.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/57\"><line id=\"S57Destra_2_\" class=\"st1\" x1=\"2197\" y1=\"499.9\" x2=\"2255\" y2=\"499.9\"/></a>\n");
        client.print("<path id=\"S57Sinistra_2_\" class=\"st0\" d=\"M2197,499.9c29.3-4.2,48.5-8.1,58-12\"/>\n");

    }

    if(scambi[58] == 1){
    
        client.print("<a href = \"/scambio/58\"><path id=\"S58Sinistra_2_\" class=\"st1\" d=\"M2345,388.9c5-19.2,18-55,18-55\"/></a>\n");
        client.print("<path id=\"S58Destra_2_\" class=\"st0\" d=\"M2369,337.9c-7.1,18-24,51-24,51\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/58\"><path id=\"S58Destra_2_\" class=\"st1\" d=\"M2369,337.9c-7.1,18-24,51-24,51\"/></a>\n");
        client.print("<path id=\"S58Sinistra_2_\" class=\"st0\" d=\"M2345,388.9c5-19.2,18-55,18-55\"/>\n");

    }

    if(scambi[59] == 1){
    
        client.print("<a href = \"/scambio/59\"><line id=\"S59Sinistra_2_\" class=\"st1\" x1=\"199.5\" y1=\"543.9\" x2=\"255.5\" y2=\"544.9\"/></a>\n");
        client.print("<path id=\"S59Destra_2_\" class=\"st0\" d=\"M199.5,544.1c18.1-0.1,38.6,5.3,61,12.8\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/59\"><path id=\"S59Destra_2_\" class=\"st1\" d=\"M199.5,544.1c18.1-0.1,38.6,5.3,61,12.8\"/></a>\n");
        client.print("<line id=\"S59Sinistra_2_\" class=\"st0\" x1=\"199.5\" y1=\"543.9\" x2=\"255.5\" y2=\"544.9\"/>\n");

    }

    if(scambi[60] == 1){
    
        client.print("<a href = \"/scambio/60\"><path id=\"S60Sinistra_2_\" class=\"st1\" d=\"M305.5,647.9c21.8-20.1,39.3-38.9,55-55\"/></a>\n");
        client.print("<path id=\"S60Destra_2_\" class=\"st0\" d=\"M296.5,638.9c21-17.3,42.3-32.9,64-46\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/60\"><path id=\"S60Destra_2_\" class=\"st1\" d=\"M296.5,638.9c21-17.3,42.3-32.9,64-46\"/></a>\n");
        client.print("<path id=\"S60Sinistra_2_\" class=\"st0\" d=\"M305.5,647.9c21.8-20.1,39.3-38.9,55-55\"/>\n");

    }

    if(scambi[61] == 1){
    
        client.print("<a href = \"/scambio/61\"><path id=\"S61Sinistra_2_\" class=\"st1\" d=\"M469.5,561.9c-30.5-1.6-54.9,2.7-75,11\"/></a>\n");
        client.print("<line id=\"S61Destra_2_\" class=\"st0\" x1=\"382.5\" y1=\"561.9\" x2=\"469.5\" y2=\"561.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/61\"><line id=\"S61Destra_2_\" class=\"st1\" x1=\"382.5\" y1=\"561.9\" x2=\"469.5\" y2=\"561.9\"/></a>\n");
        client.print("<path id=\"S61Sinistra_2_\" class=\"st0\" d=\"M469.5,561.9c-30.5-1.6-54.9,2.7-75,11\"/>\n");

    }

    if(scambi[62] == 1){
    
        client.print("<a href = \"/scambio/62\"><path id=\"S62Sinistra_2_\" class=\"st1\" d=\"M2067.5,554.9c18.2-6.1,36.8-9.8,56-10\"/></a>\n");
        client.print("<line id=\"S62Destra_2_\" class=\"st0\" x1=\"2067.5\" y1=\"544.9\" x2=\"2123.5\" y2=\"544.9\"/>\n");
    
    }

    else{

        client.print("<a href = \"/scambio/62\"><line id=\"S62Destra_2_\" class=\"st1\" x1=\"2067.5\" y1=\"544.9\" x2=\"2123.5\" y2=\"544.9\"/></a>\n");
        client.print("<path id=\"S62Sinistra_2_\" class=\"st0\" d=\"M2067.5,554.9c18.2-6.1,36.8-9.8,56-10\"/>\n");

    }

}

void stampoHTMLbinariFissi(WiFiClient client){

    if(binariFissiFileAperto){

        binariFissi = SPIFFS.open("/binariFissi.txt", "r");

        if(!elementiFissi && DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){
        
        Serial.println("C'è stato un errore nell'aprire il file!");

        }

        binariFissiFileAperto = 0;

    }

    else{

        binariFissi.seek(0, SeekSet);

    }
    
    while(binariFissiBufferizzato.available()){

        char c = binariFissiBufferizzato.read();

        client.print(c);

    }

    /*

    while(binariFissi.available()){                

        char lettera = binariFissi.read();

        client.print(lettera);
        
    */

}  

void stampoHTMLelementiFissi(WiFiClient client){

    if(elementiFissiFileAperto){

        elementiFissi = SPIFFS.open("/elementiFissi.txt", "r");

        if(!elementiFissi && DEBUGGING_ENABLE_PRINT_PORTA_SERIALE){
            
            Serial.println("C'è stato un errore nell'aprire il file!");

        }

        elementiFissiFileAperto = 0;

    }

    else{

    elementiFissi.seek(0, SeekSet);

    }

    while(elementiFissiBufferizzato.available()){

        char c = elementiFissiBufferizzato.read();

        client.print(c);

    }

    /*
        
    while(elementiFissi.available()){
    
    char lettera = elementiFissi.read();

    client.print(lettera);

    }

    */

}

void stampoHTMLlocomotivaSelezionata(WiFiClient client){

    char numeroLocomotivaSelezionataStringa[2];
    itoa(numeroLocomotivaSelezionata, numeroLocomotivaSelezionataStringa, 10);

    client.print("\t\t<text id=\"LocomotivaSelezionata\" transform=\"matrix(1 0 0 1 900.9866 638.9609)\" class=\"st6 st7 st8\">");
    client.print(numeroLocomotivaSelezionata);
    client.print("</text>\n");

}

void stampoHTMLluciLocomotivaSelezionata(WiFiClient client){

    if(treni[numeroLocomotivaSelezionata].luci == true){

        client.print("<a href = \"/luciLocomotiva\"><circle id=\"LUCE_LOCOMOTIVA_ON\" class=\"st9\" cx=\"914.5\" cy=\"1153.2\" r=\"36.9\"/></a>");

    }

    else{

        client.print("<a href = \"/luciLocomotiva\"><circle id=\"LUCE_LOCOMOTIVA_OFF\" class=\"st10\" cx=\"914.5\" cy=\"1153.2\" r=\"37.4\"/></a>");

    }

}

void stampoHTMLdirezioneLocomotivaSelezionata(WiFiClient client){

    if(treni[numeroLocomotivaSelezionata].direzioneAvanti == true){

    client.print("\t\t\t<a href = \"/direzioneLocomotiva\"><line class=\"st13\" x1=\"936.3\" y1=\"1226\" x2=\"969.9\" y2=\"1259.6\"/></a>\n"
        "\t\t\t<a href = \"/direzioneLocomotiva\"><line class=\"st13\" x1=\"936.3\" y1=\"1279.1\" x2=\"962.8\" y2=\"1252.6\"/></a>\n"
        "\t\t\t<a href = \"/direzioneLocomotiva\"><line class=\"st13\" x1=\"869.9\" y1=\"1252.6\" x2=\"962.8\" y2=\"1252.6\"/></a>\n");

    }

    else{

    client.print("\t\t\t<a href = \"/direzioneLocomotiva\"><line class=\"st11\" x1=\"896.5\" y1=\"1278.6\" x2=\"869.9\" y2=\"1252.1\"/></a>\n"
        "\t\t\t<a href = \"/direzioneLocomotiva\"><line class=\"st11\" x1=\"896.5\" y1=\"1225.5\" x2=\"862.9\" y2=\"1259.1\"/></a>\n"
        "\t\t\t<a href = \"/direzioneLocomotiva\"><line class=\"st11\" x1=\"962.8\" y1=\"1252.1\" x2=\"869.9\" y2=\"1252.1\"/></a>\n");

    }

}

void stampoHTMLvelocitaLocomotive(WiFiClient client){

    char velocitaTrenoStringa[4];

    itoa(treni[1].velocita, velocitaTrenoStringa, 10);
    client.print("\t\t<text id=\"Velocità_Locomotive_1_\" transform=\"matrix(1 0 0 1 709.5033 667.1324)\"><tspan x=\"0\" y=\"0\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    itoa(treni[2].velocita, velocitaTrenoStringa, 10);
    client.print("</tspan><tspan x=\"0\" y=\"95\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    itoa(treni[3].velocita, velocitaTrenoStringa, 10);
    client.print("</tspan><tspan x=\"0\" y=\"190\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    itoa(treni[4].velocita, velocitaTrenoStringa, 10);
    client.print("</tspan><tspan x=\"0\" y=\"285\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    itoa(treni[5].velocita, velocitaTrenoStringa, 10);
    client.print("</tspan><tspan x=\"0\" y=\"380\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    itoa(treni[6].velocita, velocitaTrenoStringa, 10);
    client.print("</tspan><tspan x=\"0\" y=\"475\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    itoa(treni[7].velocita, velocitaTrenoStringa, 10);
    client.print("</tspan><tspan x=\"0\" y=\"570\" class=\"st26 st7 st25\">");
    client.print(velocitaTrenoStringa);

    client.print("</tspan></text>\n");

}