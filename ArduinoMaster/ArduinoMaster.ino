


/*

DCC-COMMAND-STATION

by Paolo Calcagni

*/



// ***INCLUDE*** //



// Librerie per la gestione del display LCD I2C

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Libreria per la gestione dela memoria non-volatile EEPROM

#include <EEPROM.h>

// Libreria per la gestione degli interrupt
// Credit: https://playground.arduino.cc/Code/Timer1/

#include <TimerOne.h>

// Libreria per la gestione del tastierino numerico
// Credit: @author Mark Stanley, Alexander Brevig --- @contact mstanley@technologist.com, alexanderbrevig@gmail.com

#include <Keypad.h>

// Libreria per la gestione della communicazione utilizzando protocollo Firmata tra Arduino Master ed Arduino Slave

#include <FirmataMarshaller.h>

firmata::FirmataMarshaller marshaller;



// ***DEFINE*** //



// Definisco i pin di Arduino a cui sono collegati i pin della scheda L298N

#define pinENA 2
#define pinIN1 3
#define pinIN2 4

// Definisco i pin di Arduino a cui sono collegati i pin del Keypad

#define pinKeypad1 22 // Riga 1
#define pinKeypad2 24 // Riga 2
#define pinKeypad3 26 // Riga 3
#define pinKeypad4 28 // Riga 4
#define pinKeypad5 30 // Colonna 1
#define pinKeypad6 32 // Colonna 2
#define pinKeypad7 34 // Colonna 3
#define pinKeypad8 36 // Colonna 4

// Password dell'utente amministratore
// E' insicuro memorizzarla in chiaro ma per quello che mi serve va benissimo

#define PASSWORD_AMMINISTRATORE 123

// Imposto limiti dei vari utenti non amministratori

#define LIMITE_VELOCITA_STEP_OSPITE 80
#define LIMITE_NUMERO_TRENI_ATTIVI_OSPITE 2

#define LIMITE_VELOCITA_STEP_BAMBINO 50
#define LIMITE_NUMERO_TRENI_ATTIVI_BAMBINO 1

// Numero di BIT presenti in un pacchetto DCC
// Un pacchetto standard è di 42 bit ma quelli extendend da noi utilizzati raggiungono al massimo 51 bit (8 bit + Packet Start Bit).

#define NUMERO_BIT_PACCHETTO 51 

/* Numero di volte che bisogna ripetere l'invio del pacchetto: utile nel caso in cui vi sia mancato collegamento tra le ruote e i binari nel lasso di tempo di invio del pacchetto.
Un pacchetto nel caso peggiore è formato da tutti 0 bit (sappiamo che non c'è nessun pacchetto formato in questo modo), dunque ogni bit per essere codificato necessita di 250 microsecondi circa. 
42 bit x 250 sono 10.500 microsecondi. Approssimiamo a 10 millisecondi.
Dunque ripetendo il pacchetto per 10 volte, ogni pacchetto impiegherà 0,1 secondi per essere inviato. E' un ritardo trascurabile considerando la sua utilità e la frequenza con la quale dobbiamo inviare i pacchetti. */

#define NUMERO_RIPETIZIONI_PACCHETTO 10

// Abilita o disattiva la scrittura su EEPROM. 
// Considerando che la EEPROM ha cicli di vita limitati meglio disabilitare la scrittura durante lo sviluppo e debugging.

#define EEPROM_ATTIVA 1

// Se 1 il timer per l'interrupt viene inizializzato.

#define INTERRUPT_ENABLE 1

// Se 1, durante la stampa del pacchetto, viene stampato ogni bit del pacchetto indicando la sua posizione all'interno del pacchetto

#define PRINT_PACCHETTO_CON_INDICI 0

/* Il timer dell'interrupt verrà inizializzato a seconda del valore inserito qui (microsecondi)

Nota Tecnica: affinché il decoder legga in maniera corretta i bit codificati il minimo valore inseribile è 34, 70 è il massimo.
Questo discosta dal documento di NMRA nel quale vengono indicate le specifiche protocollo DCC: qui si dice che affinché il decoder riconosca il bit 1, l'impulso debba durare 58 microsecondi con un errore di 3 microsecondi (dunque in un periodo compreso tra 55 e 61 microsecondi)
*/

#define TEMPO_IMPULSO_BIT_1 58

// Se il bit 1 per essere codificato ha bisogno di due impulsi da 58 microsecondi, un bit 0 ha bisogno di due impulsi da più di 100 microsecondi ciascuno.
// Dunque ogni bit 0 avrà bisogno di 6 impulsi totali, a metà dei quali dovrà invertire polarità.

#define NUMERO_IMPULSI_0 6

// Definisco in millisecondi il tempo massimo in cui Arduino rimarrà in ascolto di eventuali INPUT dal keypad per qualsiasi sottomenù.
// Alla fine di tale tempo Arduino uscirà dal sottomenù e tornerà al menù principale.

#define TEMPO_LIMITE_INPUT_KEYPAD 3000

// Numero di scambi nel plastico.
// Numero scambi + 1 perché non partiamo da 0 ma da 1

#define NUMERO_SCAMBI 65

// Numero di locomotive.

#define NUMERO_LOCOMOTIVE 10

/* Definisco i pin di Arduino a cui sono collegati i relay per la gestione degli scambi

Abbiamo utilizzato il multiplexing per ridurre il numero totale di relay necessari.
Dunque per attivare un singolo relay abbiamo bisogno di attivare 2 digital output: 
1 digital output a scelta tra i pin delle ascisse e 1 digital output tra i pin per l'ordinata.

Codifichiamo lo stato di ogni scambio con un numero di 2 cifre: se il numero è negativo lo scambio è a sinistra, se positivo a destra. */

#define pinRelayOrdinata1 2
#define pinRelayOrdinata2 3
#define pinRelayOrdinata3 4
#define pinRelayOrdinata4 5

#define pinRelayAscissa1 22
#define pinRelayAscissa2 23
#define pinRelayAscissa3 24
#define pinRelayAscissa4 25
#define pinRelayAscissa5 26
#define pinRelayAscissa6 27
#define pinRelayAscissa7 28
#define pinRelayAscissa8 29
#define pinRelayAscissa9 30
#define pinRelayAscissa10 31
#define pinRelayAscissa11 32
#define pinRelayAscissa12 33
#define pinRelayAscissa13 34
#define pinRelayAscissa14 35
#define pinRelayAscissa15 36
#define pinRelayAscissa16 37
#define pinRelayAscissa17 38
#define pinRelayAscissa18 39
#define pinRelayAscissa19 40
#define pinRelayAscissa20 41
#define pinRelayAscissa21 42
#define pinRelayAscissa22 43
#define pinRelayAscissa23 44
#define pinRelayAscissa24 45
#define pinRelayAscissa25 46
#define pinRelayAscissa26 47
#define pinRelayAscissa27 48
#define pinRelayAscissa28 49
#define pinRelayAscissa29 50
#define pinRelayAscissa30 51
#define pinRelayAscissa31 52
#define pinRelayAscissa32 53

// Millisecondi  per i quali il relay viene eccitato

#define TEMPO_RELAY_ON 500

// Millisecondi per ricaricare la CDU (condensatori che alimentano i solenoidi dello scambio)

#define TEMPO_RICARICA_CDU 500 



// ***STRUTTURE*** //



// Inizializzo Display

LiquidCrystal_I2C display(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

// Inizializzo keypad

const byte RIGHE = 4;
const byte COLONNE = 4;

char TASTI[RIGHE][COLONNE] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[RIGHE] = {5, 6, 7, 8}; // Pin righe del keypad
byte colPins[COLONNE] = {9, 10, 11, 12}; // Pin colonne del keypad

Keypad keypad = Keypad( makeKeymap(TASTI), rowPins, colPins, RIGHE, COLONNE);


/* Andiamo a creare una nuova struttura dati per il pacchetto DCC.
Un pacchetto DCC standard contiene 42 bit, un pacchetto DCC extended può contenerne molti di più.
Nel nel nostro caso utilizziamo esclusivamente pacchetti DCC extended di dimensione massima di 51 bit.

Un bit può assumere esclusivamente due valori: 0 oppure 1.
Possiamo definire un pacchetto come un array di 51 elementi di tipo bool.

N.B: Alcuni progetti visti su Internet definiscono un pacchetto DCC come un array di 6 byte utilizzando poi le maschere e le funzioni bitWrite e bitRead per la gestione.
Inizialmente ho provato questo approccio ma con pessimi risultati: l'utilizzo di maschere e delle funzioni bitWrite e bitRead è molto macchinoso:
se è vero che messa alla mano la cosa si fa semplice è anche vero che in questo modo la probabilità di fare errori di distrazione rimane comunque molto alta.
Successivamente non notando un effettiva utilità nel gestire i pacchetti in forma di 6 byte ho deciso di optare per un array di bool. */

typedef struct{

    bool bitPacchetto[NUMERO_BIT_PACCHETTO]; // Per semplificazione ad ogni pacchetto verrà riservato un array bool di 51 elementi, indipendentemente se Standard o Extendend. Nel caso di pacchetti Standard si andranno ad utilizzare soltanto 42 di questi.
    int numeroBitPacchetto = 42; // I Pacchetti standard sono da 42 bit. Nel caso di utilizzo di Pacchetti Extendend bisogna modificare tale valore.

 } Pacchetto;

// Inizializzo pacchetti globali

 Pacchetto pacchettoIdle; // Pacchetto Idle da inviare nel caso in cui non ci siano pacchetti da inviare per continuare ad alimentare il tracciato.
 Pacchetto pacchettoStopEmergenza; // Una volta ricevuto tale pacchetto, tutti i decoder interrompono immediatamente l'energia ai motori.
 Pacchetto pacchettoDaInviare; // Buffer pacchetto formato Standard
 Pacchetto pacchettoDaInviareEsteso; // Buffer pacchetto formato Extended

// Creiamo una struttura che ci consenta di memorizzare variabili di cui avremo spesso bisogno.

typedef struct{

    Pacchetto pacchetto;
    Pacchetto pacchettoSuccessivo;
    bool pacchettoSuccessivoAttivo = false;
    int indiceBitCorrente = 0;
    int numeroRipetizioniPacchetto = 0;             // Numero di ripetizioni del pacchetto già fatte
    int bitCorrente = 0;                            // Se è 0 o 1
    int numeroPulsazioniRimanenti = 0;

} ListaPacchettiCoda;

// E' necessario rendere questa variabile volatile perché deve essere possibile aggiornarla durante una routine interrupt.

volatile ListaPacchettiCoda listaPacchettiCoda;

/* Creiamo una struttura che ci consenta di memorizzare le informazioni relative ad una locomotiva.
L'indirizzo della locomotiva non lo salviamo esplicitamente come variabile perché corrisponde alla posizione occupata nell'array treni.
Esempio: il treno alla posizione treni[0] avrà indirizzo 0. */

typedef struct{

    int velocita = 0;
    bool direzioneAvanti = true;
    bool luci = false;

 } Treno;

Treno treni[NUMERO_LOCOMOTIVE];

// Indirizzo della locomotiva inizialmente selezionata

int trenoCorrente = 1;

/* Inizializzo array di interi per memorizzare la posizione degli scambi.

0: Non sono a conoscenza della posizione dello scambio
-1: sinistra
+1: destra */

int scambi[NUMERO_SCAMBI] = {0};

/* Intero che indica la tipologia dell'utente

0) Utente non selezionato.
1) Amministratore: ha accesso a tutte le funzionalità.
2) Ospite: ha accesso a tutte le funzionalità: velocità massima delle locomotive limitata. Numero massimo di locomotive in movimento contemporaneamente: 2.
3) Bambino: velocità massima delle locomotive limitata notevolmente. Numero massimo di locomotive in movimento contemporaneamente: 1. */

int utente = 0;

// Quando Arduno Master si accorge che il Web Server è attivo (ESP8266 acceso), smette di ricevere input dal keypad e mostra l'indirizzo IP sul display.

bool webServerAttivo = false;

// Salvo l'indirizzo IP

char indirizzoIP[30];

// Variabile utile

bool indirizzoIPdisplay = false;



// ***FUNZIONI CREAZIONE E GESTIONE PACCHETTI*** //



// Crea il byte che nell'istruzione indica la tipologia d'istruzione (in questo caso istruzione velocità) in base agli input forniti.

void creaIstruzioneVelocita(bool * byteVelocita, int velocita, bool fermataEmergenza, bool direzioneAvanti){

    // Bit 0-1: contengono la sequenza di bit "01" che indica che il data byte istruzione è per la velocità e la direzione.

    byteVelocita[0] = 0;
    byteVelocita[1] = 1;
    
    // Bit 2: bit per la direzione. Se il bit è 1 la locomotiva si muoveròà in avanti. Se il bit è 0 si muoverà all'indietro.

    byteVelocita[2] = direzioneAvanti;

    /*
    
    Bit 4: per impostazione predefinita deve contenere un bit di velocità aggiuntivo, che è il bit di velocità meno significativo.

    Bit 5-8: indicano la velocità dove 0 è la velocità meno significativa.

    Step di velocità (ultimi 5 bit):

    00000: Stop
    10000: Stop
    00001: E-Stop (I decoder devono interrompere immediatamente l'energia ai motori)
    10001: E-Stop
    00010: Step 1
    10010: Step 2
    00011: Step 3
    10011: Step 4
    00100: Step 5
    10100: Step 6
    00101: Step 7
    10101: Step 8
    00110: Step 9
    10110: Step 10
    00111: Step 11
    10111: Step 12
    01000: Step 13
    11000: Step 14
    01001: Step 15
    11001: Step 16
    01010: Step 17
    11010: Step 18
    01011: Step 19
    11011: Step 20
    01100: Step 21
    11100: Step 22
    01101: Step 23
    11101: Step 24
    01110: Step 25
    11110: Step 26
    01111: Step 27
    11111: Step 28

    */

    switch(velocita){

        case 0:

        if(fermataEmergenza){
            
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5]= 0;
            byteVelocita[6]= 0;
            byteVelocita[7]= 1;

        }

        else{

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5]= 0;
            byteVelocita[6]= 0;
            byteVelocita[7]= 0;

        }

        break;

        case 1:

        byteVelocita[3] = 0;
        byteVelocita[4] = 0;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 2:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 3:

        byteVelocita[3] = 0;
        byteVelocita[4] = 0;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 4:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 5:

        byteVelocita[3] = 0;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 0;

        break;

        case 6:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 0;

        break;

        case 7:

        byteVelocita[3] = 0;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 1;

        break;

        case 8:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 1;

        break;

        case 9:

        byteVelocita[3] = 0;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 10:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 11:

        byteVelocita[3] = 0;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 12:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 13:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 0;
        byteVelocita[7]= 0;

        break;

        case 14:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 0;
        byteVelocita[7]= 0;

        break;

        case 15:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 0;
        byteVelocita[7]= 1;

        break;

        case 16:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 0;
        byteVelocita[7]= 1;

        break;

        case 17: 

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 18:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 19:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 20:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 21:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 0;

        break;

        case 22:
        
        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 0;

        break;

        case 23:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 1;

        break;

        case 24:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 0;
        byteVelocita[7]= 1;

        break;

        case 25:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 26:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

        case 27:

        byteVelocita[3] = 0;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        case 28:

        byteVelocita[3] = 1;
        byteVelocita[4] = 1;
        byteVelocita[5]= 1;
        byteVelocita[6]= 1;
        byteVelocita[7]= 1;

        break;

        default:

        byteVelocita[3] = 1;
        byteVelocita[4] = 0;
        byteVelocita[5]= 0;
        byteVelocita[6]= 1;
        byteVelocita[7]= 0;

        break;

  }

}

void creaIstruzioneVelocitaEstesa1Byte(bool * byteVelocita){

    byteVelocita[0] = 0;
    byteVelocita[1] = 0;
    byteVelocita[2] = 1;
    byteVelocita[3] = 1;
    byteVelocita[4] = 1;
    byteVelocita[5] = 1;
    byteVelocita[6] = 1;
    byteVelocita[7] = 1;

}

void creaIstruzioneVelocitaEstesa2Byte(bool * byteVelocita, int velocita, bool fermataEmergenza, bool direzioneAvanti){

    // FORMATO BYTE: EDDDDDDD
    // E = direzione: 1 avanti, 0 indietro

    if(direzioneAvanti){

        byteVelocita[0] = 1;

    }

    else{

        byteVelocita[0] = 0;

    }


    switch(velocita){

        case 0:

        if(fermataEmergenza){

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        }

        else{

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        }

        break;

        case 1:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 2:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 3:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 4:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 5:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 6:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 7:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 8:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 9:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 10:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 11:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 12:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        

        case 13:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 14:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 15:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 16:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 17:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 18:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 19:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 20:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 21:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 22:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 23:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 24:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        


        case 25:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 26:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 27:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 28:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 29:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 30:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        


        case 31:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 32:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 33:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 34:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 35:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 36:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        


        case 37:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 38:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 39:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 40:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 41:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 42:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        


        case 43:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 44:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 45:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 46:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 47:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 48:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        

        case 49:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 50:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 51:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 52:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 53:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 54:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 55:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 56:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 57:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 58:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 59:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 60:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        

        case 61:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 62:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 63:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 64:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 65:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 66:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 67:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 68:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 69:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 70:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 71:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 72:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        

        case 73:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 74:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 75:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 76:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 77:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 78:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 79:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 80:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 81:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 82:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 83:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 84:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        


        case 85:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 86:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 87:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 88:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 89:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 90:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 91:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 92:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 93:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 94:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 95:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 96:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;   

        case 97:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 98:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 99:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 100:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 101:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 102:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 103:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 104:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        

        case 105:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 106:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 107:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 108:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 109:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 110:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 111:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 112:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 113:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 114:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 115:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 116:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;        

        case 117:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 118:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        case 119:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 120:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 121:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 122:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;        

        case 123:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

        break;

        case 124:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

        break;

        case 125:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

        case 126:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

        break;

        default:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

        break;

    }

}

// Crea il byte che nell'istruzione indica la tipologia d'istruzione (in questo caso indica l'istruzione per accendere/spegnere le luci).

void creaIstruzioneLuci(bool * byteLuci, bool luciAccese){

    // FORMATO BYTE: 100D0000
    // D = 1 se vogliamo accendere le luci
    // D = 0 se vogliamo spegnere le luci

    byteLuci[0] = 1;
    byteLuci[1] = 0;
    byteLuci[2] = 0;
    byteLuci[4] = 0;
    byteLuci[5] = 0;
    byteLuci[6] = 0;
    byteLuci[7] = 0;

    if(luciAccese){

        byteLuci[3] = 1;

    }

    else{

        byteLuci[3] = 0;

    }

}

// Crea il byte che nell'istruzione svolge funzionalità di Error Detection.

void creaErrorDetection(bool * errorDetection, bool * indirizzoLocomotiva, bool * istruzioneVelocita){

  for(byte k= 0; k < 8; k++){

    errorDetection[k] = indirizzoLocomotiva[k] ^ istruzioneVelocita[k]; // ^ = OR

  }

}

void creaErrorDetectionEstesa(bool * errorDetection, bool * indirizzoLocomotiva, bool * istruzioneVelocitaEstesa1Byte, bool * istruzioneVelocitaEstesa2Byte){

  for(int k = 0; k < 8; k++){

    errorDetection[k] = indirizzoLocomotiva[k] ^ istruzioneVelocitaEstesa1Byte[k] ^ istruzioneVelocitaEstesa2Byte[k]; // OR

  }

}

// Crea il pacchetto velocità in base agli input forniti.
// Accetta come parametro velocità un numero da 0 a 28.

void componiPacchettoVelocita(Pacchetto * pacchetto, int indirizzoLocomotivaInt, int velocita, bool direzioneAvanti, bool fermataEmergenza){

    /*

    - Preambolo: 14 volte 1
    - Packet Start Bit 0
    - Primo byte: indirizzo locomotiva A
    - Packet Start Bit 0
    - Secondo byte: velocità B
    - Packet Start Bit 0
    - Terzo byte: error detection C
    - Packet End Bit 1

    FORMATO PACCHETTO: 11111111 1111110A AAAAAAA0 BBBBBBBB 0CCCCCCC C1
    Indice Decine                 111111 11112222 22222233 33333333 44           
    Indice Unità       01234567 89012345 67890123 45678901 23456789 01

    */  


    // Preambolo

    for(int i = 0; i < 14; i++){ 

        pacchetto -> bitPacchetto[i] = 1;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[14] = 0;

    // Primo Byte: indirizzo locomotiva A

    byte indirizzoLocomotivaByte = indirizzoLocomotivaInt;

    bool indirizzoLocomotiva[8];

    int j = 7;

    for(int i = 0; i < 8; i++){

        indirizzoLocomotiva[i] = bitRead(indirizzoLocomotivaByte, j);
        j--;

    }

    j = 0;

    for(int i = 15; i < 23; i++){
        
        pacchetto -> bitPacchetto[i] = indirizzoLocomotiva[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[23] = 0;

    // Secondo Byte: tipologia di istruzione e dettagli

    bool istruzioneVelocita[8];
    creaIstruzioneVelocita(istruzioneVelocita, velocita, fermataEmergenza, direzioneAvanti);

    j = 0;

    for(int i = 24; i < 32; i++){

        pacchetto -> bitPacchetto[i] = istruzioneVelocita[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[32] = 0;

    // Terzo Byte: Error Detection

    bool errorDetection[8];
    creaErrorDetection(errorDetection, indirizzoLocomotiva, istruzioneVelocita);

    j= 0;

    for(int i = 33; i < 41; i++){

        pacchetto -> bitPacchetto[i] = errorDetection[j];
        j++;

    }

    // Packet End Bit 1

    pacchetto -> bitPacchetto[41] = 1;

}

// Crea il pacchetto velocità in base agli input forniti.
// Accetta come parametro velocità un numero da 0 a 126.

void componiPacchettoVelocitaEsteso(Pacchetto * pacchetto, int indirizzoLocomotivaInt, int velocita, bool direzioneAvanti, bool fermataEmergenza){

    /*

    - Preambolo: 14 volte 1
    - Packet Start Bit 0
    - Primo byte: indirizzo locomotiva A
    - Packet Start Bit 0
    - Secondo byte: 1° byte velocità B
    - Packet Start Bit 0
    - Terzo Byte: 2° byte velocità C
    - Packet Start Bit 0
    - Terzo byte: error detection D
    - Packet End Bit 1

    FORMATO PACCHETTO: 11111111 1111110A AAAAAAA0 BBBBBBBB 0CCCCCCC C0DDDDDD DD1
    Indice Decine                 111111 11112222 22222233 33333333 44444444 445          
    Indice Unità       01234567 89012345 67890123 45678901 23456789 01234567 890

    In particolare il secondo ed il terzo byte:

    001CCCCC 0 EDDDDDDD

    CCCCC = 11111 -> Indica che l'istruzione è di tipo 128 Speed Step Control
    E = indica la direzione: 1 avanti, 0 indietro

    Dunque:

    00111111 0 EDDDDDDD

    */  


    // Preambolo

    for(int i = 0; i < 14; i++){ 

        pacchetto -> bitPacchetto[i] = 1;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[14] = 0;

    // Primo Byte: indirizzo locomotiva A

    byte indirizzoLocomotivaByte = indirizzoLocomotivaInt;

    bool indirizzoLocomotiva[8];

    int j = 7;

    for(int i = 0; i < 8; i++){

        indirizzoLocomotiva[i] = bitRead(indirizzoLocomotivaByte, j);
        j--;

    }

    j = 0;

    for(int i = 15; i < 23; i++){
        
        pacchetto -> bitPacchetto[i] = indirizzoLocomotiva[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[23] = 0;

    // Secondo Byte: tipologia di istruzione ecc.

    // Formato Byte: 00111111

    bool istruzioneVelocitaEstesa1Byte[8];
    creaIstruzioneVelocitaEstesa1Byte(istruzioneVelocitaEstesa1Byte);

    j = 0;

    for(int i = 24; i < 32; i++){

        pacchetto -> bitPacchetto[i] = istruzioneVelocitaEstesa1Byte[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[32] = 0;

    // Terzo Byte: dettagli sulla velocità

    bool istruzioneVelocitaEstesa2Byte[8];

    creaIstruzioneVelocitaEstesa2Byte(istruzioneVelocitaEstesa2Byte, velocita, fermataEmergenza, direzioneAvanti);

    j = 0;

    for(int i = 33; i < 41; i++){

        pacchetto -> bitPacchetto[i] = istruzioneVelocitaEstesa2Byte[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[41] = 0;

    // Terzo Byte: Error Detection

    bool errorDetection[8];
    creaErrorDetectionEstesa(errorDetection, indirizzoLocomotiva, istruzioneVelocitaEstesa1Byte, istruzioneVelocitaEstesa2Byte);

    j= 0;

    for(int i = 42; i < 50; i++){

        pacchetto -> bitPacchetto[i] = errorDetection[j];
        j++;

    }

    // Packet End Bit 1

    pacchetto -> bitPacchetto[50] = 1;

}

// Crea il pacchetto luci in base agli input forniti.

void componiPacchettoLuci(Pacchetto * pacchetto, int indirizzoLocomotivaInt, bool luciAccese){

    /*

    - Preambolo: 14 volte 1
    - Packet Start Bit 0
    - Primo byte: indirizzo locomotiva A
    - Packet Start Bit 0
    - Secondo byte: velocità B
    - Packet Start Bit 0
    - Terzo byte: error detection C
    - Packet End Bit 1

    FORMATO PACCHETTO: 11111111 1111110A AAAAAAA0 BBBBBBBB 0CCCCCCC C1
    Indice Decine                 111111 11112222 22222233 33333333 44           
    Indice Unità       01234567 89012345 67890123 45678901 23456789 01

    */  

    // Preambolo

    for(int i = 0; i < 14; i++){ 

        pacchetto -> bitPacchetto[i] = 1;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[14] = 0;

    // Primo Byte: indirizzo locomotiva A

    byte indirizzoLocomotivaByte = indirizzoLocomotivaInt;

    bool indirizzoLocomotiva[8];

    int j = 7;

    for(int i = 0; i < 8; i++){

        indirizzoLocomotiva[i] = bitRead(indirizzoLocomotivaByte, j);
        j--;

    }

    j = 0;

    for(int i = 15; i < 23; i++){
        
        pacchetto -> bitPacchetto[i] = indirizzoLocomotiva[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[23] = 0;

    // Secondo Byte: tipologia di istruzione e dettagli

    bool istruzioneLuci[8];
    creaIstruzioneLuci(istruzioneLuci, luciAccese);

    j = 0;

    for(int i = 24; i < 32; i++){

        pacchetto -> bitPacchetto[i] = istruzioneLuci[j];
        j++;

    }

    // Packet Start Bit

    pacchetto -> bitPacchetto[32] = 0;

    // Terzo Byte: Error Detection

    bool errorDetection[8];
    creaErrorDetection(errorDetection, indirizzoLocomotiva, istruzioneLuci);

    j= 0;

    for(int i = 33; i < 41; i++){

        pacchetto -> bitPacchetto[i] = errorDetection[j];
        j++;

    }

    // Packet End Bit 1

    pacchetto -> bitPacchetto[41] = 1;

}

 void inviaPacchetto(Pacchetto pacchetto){

    // Se il pacchetto inviato precedentemente non è stato ancora inviato aspetto.

    while(listaPacchettiCoda.pacchettoSuccessivoAttivo == true){

        // busy-waiting

    }

    copiaPacchetto(&pacchetto, &listaPacchettiCoda.pacchettoSuccessivo);
    listaPacchettiCoda.pacchettoSuccessivoAttivo = true;

 }
 
 // Stampo il pacchetto

 void stampaPacchetto(Pacchetto * pacchetto){

    if(!PRINT_PACCHETTO_CON_INDICI){

        Serial.print("INIZIO: ");

    }

    for(int i = 0; i < pacchetto -> numeroBitPacchetto; i++){
        
        if(PRINT_PACCHETTO_CON_INDICI){

            Serial.print("BIT N° ");
            Serial.print(i);
            Serial.print(": ");

        }

        Serial.print(pacchetto -> bitPacchetto[i]);

    }

    if(!PRINT_PACCHETTO_CON_INDICI){

        Serial.println(" FINE");

    }

 }

// Copia il pacchetto1 nel pacchetto2

 void copiaPacchetto(Pacchetto * pacchetto1, Pacchetto * pacchetto2){

    for(int i = 0; i < pacchetto1 -> numeroBitPacchetto; i++){

        pacchetto2 -> bitPacchetto[i] = pacchetto1 -> bitPacchetto[i];

    }
    
    pacchetto2 -> numeroBitPacchetto = pacchetto1 -> numeroBitPacchetto;

}

// Stampo il pacchetto corrente ed il pacchetto successivo

void stampaPacchettiCoda(){

    Serial.print("Pacchetto Corrente: ");
     stampaPacchetto(&listaPacchettiCoda.pacchetto);

     Serial.print("Pacchetto Successivo: ");
     stampaPacchetto(&listaPacchettiCoda.pacchettoSuccessivo);

}

// Inizializzo il pacchetto Idle

void inizializzaPacchettoIdle(){

    /*
    Indice Bit            0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    Bit Pacchetto Idle = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1};

    */

    pacchettoIdle.bitPacchetto[0] = 1;
    pacchettoIdle.bitPacchetto[1] = 1;
    pacchettoIdle.bitPacchetto[2] = 1;
    pacchettoIdle.bitPacchetto[3] = 1;
    pacchettoIdle.bitPacchetto[4] = 1;
    pacchettoIdle.bitPacchetto[5] = 1;
    pacchettoIdle.bitPacchetto[6] = 1;
    pacchettoIdle.bitPacchetto[7] = 1;
    pacchettoIdle.bitPacchetto[8] = 1;
    pacchettoIdle.bitPacchetto[9] = 1;
    pacchettoIdle.bitPacchetto[10] = 1;
    pacchettoIdle.bitPacchetto[11] = 1;
    pacchettoIdle.bitPacchetto[12] = 1;
    pacchettoIdle.bitPacchetto[13] = 1;
    pacchettoIdle.bitPacchetto[14] = 0;
    pacchettoIdle.bitPacchetto[15] = 1;
    pacchettoIdle.bitPacchetto[16] = 1;
    pacchettoIdle.bitPacchetto[17] = 1;
    pacchettoIdle.bitPacchetto[18] = 1;
    pacchettoIdle.bitPacchetto[19] = 1;
    pacchettoIdle.bitPacchetto[20] = 1;
    pacchettoIdle.bitPacchetto[21] = 1;
    pacchettoIdle.bitPacchetto[22] = 1;
    pacchettoIdle.bitPacchetto[23] = 0;
    pacchettoIdle.bitPacchetto[24] = 0;
    pacchettoIdle.bitPacchetto[25] = 0;
    pacchettoIdle.bitPacchetto[26] = 0;
    pacchettoIdle.bitPacchetto[27] = 0;
    pacchettoIdle.bitPacchetto[28] = 0;
    pacchettoIdle.bitPacchetto[29] = 0;
    pacchettoIdle.bitPacchetto[30] = 0;
    pacchettoIdle.bitPacchetto[31] = 0;
    pacchettoIdle.bitPacchetto[32] = 0;
    pacchettoIdle.bitPacchetto[33] = 1;
    pacchettoIdle.bitPacchetto[34] = 1;
    pacchettoIdle.bitPacchetto[35] = 1;
    pacchettoIdle.bitPacchetto[36] = 1;
    pacchettoIdle.bitPacchetto[37] = 1;
    pacchettoIdle.bitPacchetto[38] = 1;
    pacchettoIdle.bitPacchetto[39] = 1;
    pacchettoIdle.bitPacchetto[40] = 1;
    pacchettoIdle.bitPacchetto[41] = 1;

    listaPacchettiCoda.pacchetto = pacchettoIdle;

}



//***FUNZIONI DISPLAY***//



void displayCredits(){

    display.setCursor(0,0);
    display.print("DCC  COMMAND STATION");

    display.setCursor(9,1);
    display.print("by");

    display.setCursor(7,2);
    display.print("Paolo Calcagni");

    display.setCursor(7,3);
    display.print("Aldo  Picciani");

}

void displayMenuPrincipaleAmministratore(){

    display.setCursor(0,0);
    display.print("A Velocita' C Luci");

    display.setCursor(19, 0);

    if(treni[trenoCorrente].luci){

        display.print("*");

    }

    else{

        display.print(" ");

    }
    
    display.setCursor(0, 1);
    display.print("B Direzione D Loco");

    display.setCursor(4, 2);
    display.print("# Scambio   * STOP");

    display.setCursor(4, 3);
    display.print("LOCO: ");

    display.setCursor(10,3);
    display.print(trenoCorrente);

    display.setCursor(16, 3);
    display.print("VEL:");

    display.setCursor(21, 3);
    display.print(treni[trenoCorrente].velocita);

    display.setCursor(12, 3);

    if(treni[trenoCorrente].direzioneAvanti){

        display.print("-->");

    }

    else{

        display.print("<--");

    }

}

void displayMenuPrincipaleOspite(){

    display.setCursor(0,0);
    display.print("A Velocita' C Luci");

    display.setCursor(19, 0);

    if(treni[trenoCorrente].luci){

        display.print("*");

    }

    else{

        display.print(" ");

    }
    
    display.setCursor(0, 1);
    display.print("B Direzione D Loco");

    display.setCursor(4, 2);
    display.print("# Scambio   * STOP");

    display.setCursor(4, 3);
    display.print("LOCO: ");

    display.setCursor(10,3);
    display.print(trenoCorrente);

    display.setCursor(16, 3);
    display.print("VEL:");

    display.setCursor(21, 3);
    display.print(treni[trenoCorrente].velocita);

    display.setCursor(12, 3);

    if(treni[trenoCorrente].direzioneAvanti){

        display.print("-->");

    }

    else{

        display.print("<--");

    }

}

void displayMenuPrincipaleBambino(){
    
    display.setCursor(0,0);
    display.print("A Velocita' C Luci");

    display.setCursor(19, 0);

    if(treni[trenoCorrente].luci){

        display.print("*");

    }

    else{

        display.print(" ");

    }
    
    display.setCursor(0, 1);
    display.print("B Direzione D Cambia");

    display.setCursor(4, 2);
    display.print("* STOP");

    display.setCursor(4, 3);
    display.print("LOCO: ");

    display.setCursor(10,3);
    display.print(trenoCorrente);

    display.setCursor(16, 3);
    display.print("VEL:");

    display.setCursor(21, 3);
    display.print(treni[trenoCorrente].velocita);

    display.setCursor(12, 3);

    if(treni[trenoCorrente].direzioneAvanti){

        display.print("-->");

    }

    else{

        display.print("<--");

    }

}

void pulisciDisplay(){

    display.clear();

}

void displayFormatoInputNonValido(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("INPUT NON VALIDO");

    delay(2000);

}

void displayStopEmergenza(){

    for(int i = 0; i < 3; i++){
                    
        display.setCursor(0,0);
        display.print("********************");

        display.setCursor(0,1);
        display.print("********************");

        display.setCursor(4,2);
        display.print("********************");

        display.setCursor(4,3);
        display.print("********************");

        delay(500);

        display.clear();                  

        delay(500);

    }

}

void displayLimiteTreniRaggiunto(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Hai raggiunto il");

    display.setCursor(0,1);
    display.print("numero massimo di");

    display.setCursor(4,2);
    display.print("locomotive attive!!");

    delay(4000);

}

void displayPasswordNonValida(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Password non valida!");

    delay(2000);

    pulisciDisplay();

}

void displayScambioAzionato(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Scambio azionato");

    display.setCursor(0,1);
    display.print("correttamente.");

    delay(2000);

}

void displayMenuCambiaLocomotiva(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Inserisci il numero");
    
    display.setCursor(0,1);
    display.print("della locomotiva:");

}

void displayMenuCambiaVelocita(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Inserisci un numero");

    display.setCursor(0,1);
    display.print("da 0 a 126:");

}

void displayMenuCambiaVelocitaOspite(){
    
    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Inserisci un numero");

    display.setCursor(0,1);
    display.print("da 0 a ");

    display.setCursor(7, 1);
    display.print(LIMITE_VELOCITA_STEP_OSPITE);

    display.setCursor(9,1);
    display.print(":");

}

void displayMenuCambiaVelocitaBambino(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Inserisci un numero");

    display.setCursor(0,1);
    display.print("da 0 a ");

    display.setCursor(7, 1);
    display.print(LIMITE_VELOCITA_STEP_BAMBINO);

    display.setCursor(9,1);
    display.print(":");

}

void displayMenuCambiaScambio(){

    pulisciDisplay();

    display.setCursor(0, 0);
    display.print("Inserisci il numero");

    display.setCursor(0,1);
    display.print("dello scambio: ");

}

void displayMenuLogin(){
    
    while(utente == 0){ 

        display.setCursor(0,0);
        display.print("LOGIN: ");

        display.setCursor(0,1);
        display.print("A: Amministratore");

        display.setCursor(4,2);
        display.print("B: Ospite");

        display.setCursor(4,3);
        display.print("C: Bambino");

        char tasto = keypad.getKey();

        switch(tasto){

            case 'A':
            
            {
                pulisciDisplay();

                display.setCursor(0,0);
                display.print("Inserisci la");

                display.setCursor(0,1);
                display.print("password: ");

                char primaCifra, secondaCifra, terzaCifra;
                primaCifra = NULL;
                secondaCifra = NULL;
                terzaCifra = NULL;

                int primaCifraInt, secondaCifraInt, terzaCifraInt, password;

                primaCifra = leggiCarattere();

                if(primaCifra){

                    secondaCifra = leggiCarattere();

                    if(secondaCifra){

                        terzaCifra = leggiCarattere();

                    }

                }

                primaCifraInt = (int) primaCifra - 48;
                secondaCifraInt = (int) secondaCifra - 48;
                terzaCifraInt = (int) terzaCifra - 48;

                password = primaCifraInt * 100 + secondaCifraInt * 10 + terzaCifraInt;

                if(password == PASSWORD_AMMINISTRATORE){

                    utente = 1;

                }

                else{

                    displayPasswordNonValida();

                }

            }

            break;

            case 'B':

            {
                utente = 2;

                pulisciDisplay();

            }

            break;

            case 'C':

            {

                utente = 3;

                pulisciDisplay();
                
            }

            break;

        } 

    }

}

void displayResetScambi(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Reset Scambi");

    display.setCursor(0,1);
    display.print("in corso...");

}

void displaySpegniSistema(){

    pulisciDisplay();

    display.setCursor(0,0);
    display.print("Salvataggio EEPROM");

    display.setCursor(0,1);
    display.print("completato.");

    display.setCursor(4,2);
    display.print("Ora puoi spegnere");

    display.setCursor(4,4);
    display.print("Arduino.");

}

void displayIP(){
    
    display.setCursor(4,1);
    display.print("INDIRIZZO IP");

    display.setCursor(8,2);
    display.print(indirizzoIP);

}



// ***FUNZIONI SCAMBI*** //



// Aziono uno scambio

int azionaScambio(int indiceCodificato){

    bool sinistra;
    int numeroScambio;

    numeroScambio = abs(indiceCodificato);

    if(indiceCodificato < -NUMERO_SCAMBI || indiceCodificato > NUMERO_SCAMBI){

        return 1;

    }

    if(indiceCodificato < 0){

        sinistra = true;

        scambi[numeroScambio] = -1;

    }

    else{

        sinistra = false;

        scambi[numeroScambio] = 1;

    }

    if(numeroScambio <= 32){

        char pinRelayAscissa[30];
        char numeroScambioStringa[10];
        itoa(numeroScambio, numeroScambioStringa, 10);
        strcpy(pinRelayAscissa, "pinRelayAscissa");
        strcat(pinRelayAscissa, numeroScambioStringa);
        
        if(!sinistra){
            
            marshaller.sendDigital(pinRelayAscissa, HIGH);
            marshaller.sendDigital(pinRelayOrdinata1, HIGH);
            marshaller.sendDigital(13, HIGH);

            delay(TEMPO_RELAY_ON);

            marshaller.sendDigital(pinRelayAscissa, LOW);
            marshaller.sendDigital(pinRelayOrdinata1, LOW);
            marshaller.sendDigital(13, LOW);
            
        }

        else{

            marshaller.sendDigital(pinRelayAscissa, HIGH);
            marshaller.sendDigital(pinRelayOrdinata2, HIGH);
            marshaller.sendDigital(13, HIGH);

            delay(TEMPO_RELAY_ON);

            marshaller.sendDigital(pinRelayAscissa, LOW);
            marshaller.sendDigital(pinRelayOrdinata2, LOW);
            marshaller.sendDigital(13, LOW);

        }

    }

    else{

        char pinRelayAscissa[30];
        char numeroScambioStringa[10];
        itoa(numeroScambio - 32, numeroScambioStringa, 10);
        strcpy(pinRelayAscissa, "pinRelayAscissa");
        strcat(pinRelayAscissa, numeroScambioStringa);

        if(!sinistra){
            
            marshaller.sendDigital(pinRelayAscissa, HIGH);
            marshaller.sendDigital(pinRelayOrdinata3, HIGH);
            marshaller.sendDigital(13, HIGH);

            delay(TEMPO_RELAY_ON);

            marshaller.sendDigital(pinRelayAscissa, LOW);
            marshaller.sendDigital(pinRelayOrdinata3, LOW);
            marshaller.sendDigital(13, LOW);

        }

        else{

            marshaller.sendDigital(pinRelayAscissa, HIGH);
            marshaller.sendDigital(pinRelayOrdinata4, HIGH);
            marshaller.sendDigital(13, HIGH);

            delay(TEMPO_RELAY_ON);

            marshaller.sendDigital(pinRelayAscissa, LOW);
            marshaller.sendDigital(pinRelayOrdinata4, LOW);
            marshaller.sendDigital(13, LOW);

        }

    }

    return 0;

}

void inizializzaScambiDestra(){

    for(int i = 1; i < NUMERO_SCAMBI; i++){

        scambi[i] = 1;
        
        delay(TEMPO_RICARICA_CDU);
        
        Serial.print(i);
        azionaScambio(i);

    }

}

/*  

Quando si verifica un interruzione dell'alimentazione, Arduino perde tutta la memoria volatile.

Perdere lo stato dei treni non è un problema considerando che anche i decoder delle locomotive perdono tali informazioni (velocità e direzione). 
Tuttavia sarebbe un problema perdere lo stato degli scambi: una volta che Arduino torna ad essere alimentato, non sapremmo più la posizione corretta degli scambi.

Le soluzioni sono due:

- Ad ogni avvio di Arduino, azionare tutti gli scambi secondo una configurazione iniziale.
In questo modo, dopo l'inizializzazione, saremmo sicuri che lo stato degli scambi salvati in memoria corrisponde a quello reale.
Questo approccio tuttavia richiede un po' di tempo: è possibile alimentare un solo scambio per volta. 
Inoltre sarebbe necessario aspettare che la CDU si ricarichi ogni volta.

- Fare utilizzo di una memoria non volatile che memorizzi lo stato degli scambi anche senza elettricità.
Questo è l'approccio da me seguito in quanto porta ad un notevole risparmio di tempo in fase di inizializzazione della Command Station.

L'EEPROM (Electrically Erasable Programmable Read Only Memory) è un tipo di memoria non volatile (presente su Arduino), destinata a memorizzare dati (ad esempio per la configurazione di un dispositivo) che devono essere mantenuti quando viene tolta l’alimentazione elettrica.

In particolare, la capacità EEPROM di Arduino Mega è di 4kb.
Necessitiamo di memorizzare un array di interi di poche decine di elementi, dunque nel nostro caso va più che bene.

Tutta la memoria EEPROM non ha una vita illimitata, ma è garantita fino a circa 100000 cicli di scrittura.
E' necessario dunque prestare attenzione e procedere alla scrittura di tale memoria solo se necessario.

Per questo motivo non andremo a salvare in EEPROM l'array degli interi ad ogni sua modifica ma soltanto nella procedura di "spegnimento sicuro",
eseguita soltanto quando l'utente dichiara la sua volontà di togliere l'alimentazione ad Arduino.

Funzioni:

- EEPROM.read(indirizzo); → restituisce il valore intero memorizzato nella cella avente l’indirizzo indicato.

- EEPROM.write(indirizzo, valore); → scrive il valore nella cella avente l’indirizzo indicato.

- EEPROM.update(indirizzo, valore); → effettua la scrittura solo se il valore indicato è diverso dal valore precedentemente memorizzato. 

La differenza tra write e update è radicale in quanto permette di risparmiare un ciclo di scrittura se il valore da memorizzare non è cambiato rispetto al precedente. 
Inoltre permette di risparmiare anche tempo in quanto la scrittura/modifica di un valore memorizzato richiede un tempo di 3.3 ms circa.

*/

void inizializzaScambi(){

    for(int i = 1; i < NUMERO_SCAMBI; i++){

        scambi[i] = 0;

    }

}

void scrivoStatoScambi(){

    if(EEPROM_ATTIVA){

        for(int i = 1; i < NUMERO_SCAMBI; i++){

            EEPROM.update(i, scambi[i]);

        }

    }

}

void leggoStatoScambi(){

    if(EEPROM_ATTIVA){

        for(int i = 1; i < NUMERO_SCAMBI; i++){

            scambi[i] = EEPROM.read(i);

        }

    }

    for(int i = 1; i < NUMERO_SCAMBI; i++){

        if(scambi[i] == 255){

            scambi[i] = -1;
    
        }

    }

}

void stampaStatoScambi(){

    for(int i = 1; i < NUMERO_SCAMBI; i++){

        Serial.print("Stato cambio ");
        Serial.print(i);
        Serial.print(": ");
        Serial.print(scambi[i]);

    }

    Serial.println();
    
}

void spegnimentoSicuro(){

    digitalWrite(pinENA, LOW);

    scrivoStatoScambi();

    displaySpegniSistema();

    while(true){

        // Ciclo while infinito: aspetto che la corrente sia tolta

    }

}



// ***FUNZIONI KEYPAD*** //



char leggiCarattere(){

    unsigned long time1, time2;

    char carattere = NULL;

    time1 = millis();

    while(!carattere){

        time2 = millis();

        carattere = keypad.getKey();

        if(time2 - time1 > TEMPO_LIMITE_INPUT_KEYPAD){

            break;

        }

    }

    return carattere;

}

void keyPadMenuCambiaScambio(){

    displayMenuCambiaScambio();

    char primaCifra, secondaCifra;
    primaCifra = NULL;
    secondaCifra = NULL;

    int primaCifraInt, secondaCifraInt, numeroScambioInt;

    primaCifra = leggiCarattere();

    if(primaCifra){

        secondaCifra = leggiCarattere();

    }

    primaCifraInt = int (primaCifra) - 48;
    secondaCifraInt = int (secondaCifra) - 48;

    if(primaCifra && secondaCifra){
        
        numeroScambioInt = primaCifraInt * 10 + secondaCifraInt;

        if(numeroScambioInt < NUMERO_SCAMBI && numeroScambioInt >= 0){
            
            if(scambi[numeroScambioInt] == 1){

                azionaScambio(-numeroScambioInt);

            }

            else{

                azionaScambio(numeroScambioInt);

            }

            displayScambioAzionato();

        }

        else{
            
            displayFormatoInputNonValido();

        }

    }

    else if(primaCifra){

        numeroScambioInt = primaCifraInt;

        if(numeroScambioInt <= NUMERO_SCAMBI && numeroScambioInt >= 0){

            if(scambi[numeroScambioInt] == 1){

                azionaScambio(-numeroScambioInt);

            }

            else{

                azionaScambio(numeroScambioInt);

            }

            displayScambioAzionato();

        }

        else{
            
            displayFormatoInputNonValido();

        }

    }

}

void keypadMenuCambiaVelocita(){

    char primaCifra, secondaCifra, terzaCifra;
    primaCifra = NULL;
    secondaCifra = NULL;
    terzaCifra = NULL;

    int limiteVelocitaStep, limiteNumeroTreniAttivi;

    bool velocitaValida = true, tempoScaduto = false;
    
    if(utente == 1){
        
        limiteVelocitaStep = 126;
        limiteNumeroTreniAttivi = 100;

    }

    else if(utente == 2){

        limiteVelocitaStep = LIMITE_VELOCITA_STEP_OSPITE;
        limiteNumeroTreniAttivi = LIMITE_NUMERO_TRENI_ATTIVI_OSPITE;

    }

    else if(utente == 3){

        limiteVelocitaStep = LIMITE_VELOCITA_STEP_BAMBINO;
        limiteNumeroTreniAttivi = LIMITE_NUMERO_TRENI_ATTIVI_BAMBINO;

    }

    int primaCifraInt, secondaCifraInt, terzaCifraInt, velocita;
    
    primaCifra = leggiCarattere();

    if(primaCifra){

        secondaCifra = leggiCarattere();

        if(secondaCifra && utente == 1){

            terzaCifra = leggiCarattere();

        }

    }

    primaCifraInt = (int) primaCifra - 48;
    secondaCifraInt = (int) secondaCifra - 48;
    terzaCifraInt = (int) terzaCifra - 48;

    if(primaCifra && secondaCifra && terzaCifra){

        velocita = primaCifraInt * 100 + secondaCifraInt * 10 + terzaCifraInt;

        if(velocita < 0 || velocita > 126){
            
            velocitaValida = false;

        }

    }

    else if(primaCifra && secondaCifra){

        velocita = primaCifraInt * 10 + secondaCifraInt;

        if(velocita < 0 || velocita > limiteVelocitaStep){
            
            velocitaValida = false;

        }

    }

    else if(primaCifra){

        velocita = primaCifraInt;

        if(velocita < 0 || velocita > 9){

            velocitaValida = false;

        }

    }

    else{

        tempoScaduto  = true;

    }

    int contatoreTreniAttivi = 0;

    for(int i = 0; i < NUMERO_LOCOMOTIVE; i++){
        
        if(treni[i].velocita > 0){

            contatoreTreniAttivi += 1;

        }

    }
    
    if(!tempoScaduto){

        if(velocitaValida){

            if(contatoreTreniAttivi < limiteNumeroTreniAttivi || treni[trenoCorrente].velocita > 0){

                treni[trenoCorrente].velocita = velocita;
                
                componiPacchettoVelocitaEsteso(&pacchettoDaInviareEsteso, trenoCorrente, treni[trenoCorrente].velocita, treni[trenoCorrente].direzioneAvanti, false);
                inviaPacchetto(pacchettoDaInviareEsteso);

            }

            else{

                displayLimiteTreniRaggiunto();

            }

        }

        else{

            displayFormatoInputNonValido();
            
        }

    }

}

void keypadMenuCambiaLocomotiva(){

    char indirizzoLocomotiva;
    indirizzoLocomotiva = NULL;

    int primaCifraInt, indirizzoLocomotivaInt;

    indirizzoLocomotiva = leggiCarattere();
            
    if(indirizzoLocomotiva){  
        
        indirizzoLocomotivaInt = (int) indirizzoLocomotiva - 48;

        if(indirizzoLocomotivaInt < NUMERO_LOCOMOTIVE && indirizzoLocomotivaInt >= 0){
            
            trenoCorrente = indirizzoLocomotivaInt;

        }

        else{

            displayFormatoInputNonValido();

        }

    }

}



// ***ALTRE FUNZIONI*** //



/*  

Routine chiamata nella funzione loop che gestisce il display ed il keypad.

TASTO A: Cambia Velocità Locomotiva
TASTO B: Cambia Direzione Locomotiva
TASTO C: Cambia Luci Locomotiva
TASTO D: Cambia Locomotiva
TASTO #: Cambia Scambio
TASTO *: Stop di Emergenza

TASTO 0: Spegnimento Sicuro
TASTO 1: cambio utente
TASTO 2: reset scambi (a destra)

*/


void gestisciDisplayKeypad(){

    switch(utente){

        case 1: {

            displayMenuPrincipaleAmministratore();

            char tasto = keypad.getKey();

            switch(tasto){
                
                case 'A': {

                    // Cambio velocità

                    displayMenuCambiaVelocita();

                    keypadMenuCambiaVelocita();

                    pulisciDisplay();
                    
                }

                break;

                case 'B': {
                            
                    // Cambio direzione
                
                    treni[trenoCorrente].direzioneAvanti = !(treni[trenoCorrente].direzioneAvanti);
                    Serial.print(treni[trenoCorrente].direzioneAvanti);

                    componiPacchettoVelocitaEsteso(&pacchettoDaInviareEsteso, trenoCorrente, treni[trenoCorrente].velocita, treni[trenoCorrente].direzioneAvanti, false);
                    inviaPacchetto(pacchettoDaInviareEsteso);

                }

                break;

                case 'C': {

                    // Accendo/spengo luci

                    treni[trenoCorrente].luci = !treni[trenoCorrente].luci;

                    componiPacchettoLuci(&pacchettoDaInviare, trenoCorrente, treni[trenoCorrente].luci);
                    inviaPacchetto(pacchettoDaInviare);

                }

                break;

                case 'D': {
                    
                    displayMenuCambiaLocomotiva();

                    keypadMenuCambiaLocomotiva();

                    pulisciDisplay();

                }

                break;

                case '#': {

                    displayMenuCambiaScambio();

                    keyPadMenuCambiaScambio();

                    pulisciDisplay();

                }

                break;

                case '*': {
                    
                    invioPacchettoEmergenza();

                }

                break;

                case '0': {

                    spegnimentoSicuro();

                }

                break;

                case '1': {

                    utente = 0;

                    pulisciDisplay();
                    displayMenuLogin();

                }

                break;
                
                case '2': {

                    displayResetScambi();
                    inizializzaScambiDestra();
                    pulisciDisplay();
                    
                }

                break;
                
            }

        }

        break;

        case 2: {
            
            displayMenuPrincipaleOspite();

            char tasto = keypad.getKey();

            switch(tasto){
                    
                case 'A': {

                    // Cambio velocità
                    
                    displayMenuCambiaVelocitaOspite();

                    keypadMenuCambiaVelocita();

                    pulisciDisplay();

                }

                break;

                case 'B': {
                            
                    // Cambio direzione
                
                    treni[trenoCorrente].direzioneAvanti = !(treni[trenoCorrente].direzioneAvanti);
                    Serial.print(treni[trenoCorrente].direzioneAvanti);

                    componiPacchettoVelocitaEsteso(&pacchettoDaInviareEsteso, trenoCorrente, treni[trenoCorrente].velocita, treni[trenoCorrente].direzioneAvanti, false);
                    inviaPacchetto(pacchettoDaInviareEsteso); 

                }

                break;

                case 'C': {

                    // Accendo/spengo luci

                    treni[trenoCorrente].luci = !treni[trenoCorrente].luci;

                    componiPacchettoLuci(&pacchettoDaInviare, trenoCorrente, treni[trenoCorrente].luci);
                    inviaPacchetto(pacchettoDaInviare);

                }

                break;

                case 'D': {
                    
                    displayMenuCambiaLocomotiva();
                    
                    keypadMenuCambiaLocomotiva();

                    pulisciDisplay();

                }

                break;

                case '#': {

                    displayMenuCambiaScambio();

                    keyPadMenuCambiaScambio();

                    pulisciDisplay();

                }

                break;

                case '*': {

                    invioPacchettoEmergenza();

                }

                case '0': {

                    spegnimentoSicuro();

                }

                break;

                case '1': {

                    utente = 0;

                    pulisciDisplay();
                    displayMenuLogin();

                }

                break;

                case '2': {

                    displayResetScambi();
                    inizializzaScambiDestra();
                    pulisciDisplay();
                    
                }

                break;

                break;

            }
            
        }

        break;

        case 3: {
            
            displayMenuPrincipaleBambino();

            char tasto = keypad.getKey();

            switch(tasto){
                    
                case 'A': {

                    // Cambio velocità

                    displayMenuCambiaVelocitaBambino();

                    keypadMenuCambiaVelocita();

                    pulisciDisplay();

                }

                break;

                case 'B': {
                            
                    // Cambio direzione
                
                    treni[trenoCorrente].direzioneAvanti = !(treni[trenoCorrente].direzioneAvanti);
                    Serial.print(treni[trenoCorrente].direzioneAvanti);

                    componiPacchettoVelocitaEsteso(&pacchettoDaInviareEsteso, trenoCorrente, treni[trenoCorrente].velocita, treni[trenoCorrente].direzioneAvanti, false);
                    inviaPacchetto(pacchettoDaInviareEsteso);

                }

                break;

                case 'C': {

                    // Accendo/spengo luci

                    treni[trenoCorrente].luci = !treni[trenoCorrente].luci;

                    componiPacchettoLuci(&pacchettoDaInviare, trenoCorrente, treni[trenoCorrente].luci);
                    inviaPacchetto(pacchettoDaInviare);

                }

                break;

                case 'D': {
                    
                    displayMenuCambiaLocomotiva();

                    keypadMenuCambiaLocomotiva();

                    pulisciDisplay();

                }

                break;

                case '*': {

                    invioPacchettoEmergenza();

                }

                case '0': {

                    spegnimentoSicuro();

                }

                break;

                case '1': {

                    utente = 0;

                    pulisciDisplay();
                    displayMenuLogin();

                }

                break;

                case '2': {

                    displayResetScambi();
                    inizializzaScambiDestra();
                    pulisciDisplay();
                    
                }

                break;

            }

        }

        break;

    }
    
}

void invioPacchettoEmergenza(){

    inviaPacchetto(pacchettoStopEmergenza);

    pulisciDisplay();

    if(utente >= 2 || !webServerAttivo){

        displayStopEmergenza();

    }
    

    pulisciDisplay();

    for(int i = 0; i < NUMERO_LOCOMOTIVE; i++){

        treni[i].velocita = 0;

    }

}

void invioPosizioneScambi(){

    char stringaScambi[200] = " ";

    char spazio[2] = " ";
    
    for(int i = 1; i < NUMERO_SCAMBI; i++){
        
        char posizioneScambio[4];
        itoa(scambi[i], posizioneScambio, 10);        

        strcat(stringaScambi, posizioneScambio);
        strcat(stringaScambi, spazio);

    }

    Serial2.begin(9600);
    Serial2.write(stringaScambi, 200); // TODO: serializzo i dati

    Serial2.end();

}



// ***FUNZIONI SET SCAMBI*** //



void setAnelloEsterno(){

    azionaScambio(49);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-21);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-20);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(25);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(26);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-45);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-55);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-56);

}

void setAnelloInterno(){

    azionaScambio(48);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(47);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-23);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-22);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(27);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(28);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-44);

}

void setAnelloEsternoEsteso(){

    azionaScambio(49);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-21);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(20);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(16);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(33);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(55);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-56);

}

void setAnelloEsternoEstesissimoPrimoPiano(){

    azionaScambio(49);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-21);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(20);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(19);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-17);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-4);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(3);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(1);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(9);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-24);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-25);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(26);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-45);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-55);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-56);

}

void setAnelloEsternoEstesissimoSecondoPiano(){

    azionaScambio(24);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-25);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(26);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-45);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-55);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(56);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(57);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(62);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-59);

}

void setStazione1Binario3(){

    azionaScambio(-47);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(32);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(23);

}

void setStazione2Binario0(){

    azionaScambio(19);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(17);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-2);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-1);

}

void setStazione2Binario1(){

    azionaScambio(19);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-17);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-4);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-3);

}

void setStazione2Binario2(){

    azionaScambio(-19);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(18);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-13);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(12);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(4);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-3);

}

void setStazione2Binario3(){

    azionaScambio(-19);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-18);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(15);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(13);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(12);
    
    delay(TEMPO_RICARICA_CDU);

    azionaScambio(4);

    delay(TEMPO_RICARICA_CDU);

    azionaScambio(-3);

}



// ***FIRMATA*** //



/*

  Firmata is a generic protocol for communicating with microcontrollers
  from software on a host computer. It is intended to work with
  any host computer software package.

  To download a host software package, please click on the following link
  to open the list of Firmata client libraries in your default browser.

  https://github.com/firmata/arduino#firmata-client-libraries

  Copyright (C) 2006-2008 Hans-Christoph Steiner.  All rights reserved.
  Copyright (C) 2010-2011 Paul Stoffregen.  All rights reserved.
  Copyright (C) 2009 Shigeru Kobayashi.  All rights reserved.
  Copyright (C) 2009-2016 Jeff Hoefs.  All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  See file LICENSE.txt for further informations on licensing terms.

  Last updated August 17th, 2017

*/

#include <Servo.h>
#include <Wire.h>
#include <Firmata.h>

#define I2C_WRITE                   B00000000
#define I2C_READ                    B00001000
#define I2C_READ_CONTINUOUSLY       B00010000
#define I2C_STOP_READING            B00011000
#define I2C_READ_WRITE_MODE_MASK    B00011000
#define I2C_10BIT_ADDRESS_MODE_MASK B00100000
#define I2C_END_TX_MASK             B01000000
#define I2C_STOP_TX                 1
#define I2C_RESTART_TX              0
#define I2C_MAX_QUERIES             8
#define I2C_REGISTER_NOT_SPECIFIED  -1

// the minimum interval for sampling analog input
#define MINIMUM_SAMPLING_INTERVAL   1


/*==============================================================================
 * GLOBAL VARIABLES
 *============================================================================*/

#ifdef FIRMATA_SERIAL_FEATURE
SerialFirmata serialFeature;
#endif

/* analog inputs */
int analogInputsToReport = 0; // bitwise array to store pin reporting

/* digital input ports */
byte reportPINs[TOTAL_PORTS];       // 1 = report this port, 0 = silence
byte previousPINs[TOTAL_PORTS];     // previous 8 bits sent

/* pins configuration */
byte portConfigInputs[TOTAL_PORTS]; // each bit: 1 = pin in INPUT, 0 = anything else

/* timer variables */
unsigned long currentMillis;        // store the current value from millis()
unsigned long previousMillis;       // for comparison with currentMillis
unsigned int samplingInterval = 19; // how often to run the main loop (in ms)

/* i2c data */
struct i2c_device_info {
  byte addr;
  int reg;
  byte bytes;
  byte stopTX;
};

/* for i2c read continuous more */
i2c_device_info query[I2C_MAX_QUERIES];

byte i2cRxData[64];
boolean isI2CEnabled = false;
signed char queryIndex = -1;
// default delay time between i2c read request and Wire.requestFrom()
unsigned int i2cReadDelayTime = 0;

Servo servos[MAX_SERVOS];
byte servoPinMap[TOTAL_PINS];
byte detachedServos[MAX_SERVOS];
byte detachedServoCount = 0;
byte servoCount = 0;

boolean isResetting = false;

// Forward declare a few functions to avoid compiler errors with older versions
// of the Arduino IDE.
void setPinModeCallback(byte, int);
void reportAnalogCallback(byte analogPin, int value);
void sysexCallback(byte, byte, byte*);

/* utility functions */
void wireWrite(byte data)
{
#if ARDUINO >= 100
  Wire.write((byte)data);
#else
  Wire.send(data);
#endif
}

byte wireRead(void)
{
#if ARDUINO >= 100
  return Wire.read();
#else
  return Wire.receive();
#endif
}

/*==============================================================================
 * FUNCTIONS
 *============================================================================*/

void attachServo(byte pin, int minPulse, int maxPulse)
{
  if (servoCount < MAX_SERVOS) {
    // reuse indexes of detached servos until all have been reallocated
    if (detachedServoCount > 0) {
      servoPinMap[pin] = detachedServos[detachedServoCount - 1];
      if (detachedServoCount > 0) detachedServoCount--;
    } else {
      servoPinMap[pin] = servoCount;
      servoCount++;
    }
    if (minPulse > 0 && maxPulse > 0) {
      servos[servoPinMap[pin]].attach(PIN_TO_DIGITAL(pin), minPulse, maxPulse);
    } else {
      servos[servoPinMap[pin]].attach(PIN_TO_DIGITAL(pin));
    }
  } else {
    Firmata.sendString("Max servos attached");
  }
}

void detachServo(byte pin)
{
  servos[servoPinMap[pin]].detach();
  // if we're detaching the last servo, decrement the count
  // otherwise store the index of the detached servo
  if (servoPinMap[pin] == servoCount && servoCount > 0) {
    servoCount--;
  } else if (servoCount > 0) {
    // keep track of detached servos because we want to reuse their indexes
    // before incrementing the count of attached servos
    detachedServoCount++;
    detachedServos[detachedServoCount - 1] = servoPinMap[pin];
  }

  servoPinMap[pin] = 255;
}

void enableI2CPins()
{
  byte i;
  // is there a faster way to do this? would probaby require importing
  // Arduino.h to get SCL and SDA pins
  for (i = 0; i < TOTAL_PINS; i++) {
    if (IS_PIN_I2C(i)) {
      // mark pins as i2c so they are ignore in non i2c data requests
      setPinModeCallback(i, PIN_MODE_I2C);
    }
  }

  isI2CEnabled = true;

  Wire.begin();
}

/* disable the i2c pins so they can be used for other functions */
void disableI2CPins() {
  isI2CEnabled = false;
  // disable read continuous mode for all devices
  queryIndex = -1;
}

void readAndReportData(byte address, int theRegister, byte numBytes, byte stopTX) {
  // allow I2C requests that don't require a register read
  // for example, some devices using an interrupt pin to signify new data available
  // do not always require the register read so upon interrupt you call Wire.requestFrom()
  if (theRegister != I2C_REGISTER_NOT_SPECIFIED) {
    Wire.beginTransmission(address);
    wireWrite((byte)theRegister);
    Wire.endTransmission(stopTX); // default = true
    // do not set a value of 0
    if (i2cReadDelayTime > 0) {
      // delay is necessary for some devices such as WiiNunchuck
      delayMicroseconds(i2cReadDelayTime);
    }
  } else {
    theRegister = 0;  // fill the register with a dummy value
  }

  Wire.requestFrom(address, numBytes);  // all bytes are returned in requestFrom

  // check to be sure correct number of bytes were returned by slave
  if (numBytes < Wire.available()) {
    Firmata.sendString("I2C: Too many bytes received");
  } else if (numBytes > Wire.available()) {
    Firmata.sendString("I2C: Too few bytes received");
  }

  i2cRxData[0] = address;
  i2cRxData[1] = theRegister;

  for (int i = 0; i < numBytes && Wire.available(); i++) {
    i2cRxData[2 + i] = wireRead();
  }

  // send slave address, register and received bytes
  Firmata.sendSysex(SYSEX_I2C_REPLY, numBytes + 2, i2cRxData);
}

void outputPort(byte portNumber, byte portValue, byte forceSend)
{
  // pins not configured as INPUT are cleared to zeros
  portValue = portValue & portConfigInputs[portNumber];
  // only send if the value is different than previously sent
  if (forceSend || previousPINs[portNumber] != portValue) {
    Firmata.sendDigitalPort(portNumber, portValue);
    previousPINs[portNumber] = portValue;
  }
}

/* -----------------------------------------------------------------------------
 * check all the active digital inputs for change of state, then add any events
 * to the Serial output queue using Serial.print() */
void checkDigitalInputs(void)
{
  /* Using non-looping code allows constants to be given to readPort().
   * The compiler will apply substantial optimizations if the inputs
   * to readPort() are compile-time constants. */
  if (TOTAL_PORTS > 0 && reportPINs[0]) outputPort(0, readPort(0, portConfigInputs[0]), false);
  if (TOTAL_PORTS > 1 && reportPINs[1]) outputPort(1, readPort(1, portConfigInputs[1]), false);
  if (TOTAL_PORTS > 2 && reportPINs[2]) outputPort(2, readPort(2, portConfigInputs[2]), false);
  if (TOTAL_PORTS > 3 && reportPINs[3]) outputPort(3, readPort(3, portConfigInputs[3]), false);
  if (TOTAL_PORTS > 4 && reportPINs[4]) outputPort(4, readPort(4, portConfigInputs[4]), false);
  if (TOTAL_PORTS > 5 && reportPINs[5]) outputPort(5, readPort(5, portConfigInputs[5]), false);
  if (TOTAL_PORTS > 6 && reportPINs[6]) outputPort(6, readPort(6, portConfigInputs[6]), false);
  if (TOTAL_PORTS > 7 && reportPINs[7]) outputPort(7, readPort(7, portConfigInputs[7]), false);
  if (TOTAL_PORTS > 8 && reportPINs[8]) outputPort(8, readPort(8, portConfigInputs[8]), false);
  if (TOTAL_PORTS > 9 && reportPINs[9]) outputPort(9, readPort(9, portConfigInputs[9]), false);
  if (TOTAL_PORTS > 10 && reportPINs[10]) outputPort(10, readPort(10, portConfigInputs[10]), false);
  if (TOTAL_PORTS > 11 && reportPINs[11]) outputPort(11, readPort(11, portConfigInputs[11]), false);
  if (TOTAL_PORTS > 12 && reportPINs[12]) outputPort(12, readPort(12, portConfigInputs[12]), false);
  if (TOTAL_PORTS > 13 && reportPINs[13]) outputPort(13, readPort(13, portConfigInputs[13]), false);
  if (TOTAL_PORTS > 14 && reportPINs[14]) outputPort(14, readPort(14, portConfigInputs[14]), false);
  if (TOTAL_PORTS > 15 && reportPINs[15]) outputPort(15, readPort(15, portConfigInputs[15]), false);
}

// -----------------------------------------------------------------------------
/* sets the pin mode to the correct state and sets the relevant bits in the
 * two bit-arrays that track Digital I/O and PWM status
 */
void setPinModeCallback(byte pin, int mode)
{
  if (Firmata.getPinMode(pin) == PIN_MODE_IGNORE)
    return;

  if (Firmata.getPinMode(pin) == PIN_MODE_I2C && isI2CEnabled && mode != PIN_MODE_I2C) {
    // disable i2c so pins can be used for other functions
    // the following if statements should reconfigure the pins properly
    disableI2CPins();
  }
  if (IS_PIN_DIGITAL(pin) && mode != PIN_MODE_SERVO) {
    if (servoPinMap[pin] < MAX_SERVOS && servos[servoPinMap[pin]].attached()) {
      detachServo(pin);
    }
  }
  if (IS_PIN_ANALOG(pin)) {
    reportAnalogCallback(PIN_TO_ANALOG(pin), mode == PIN_MODE_ANALOG ? 1 : 0); // turn on/off reporting
  }
  if (IS_PIN_DIGITAL(pin)) {
    if (mode == INPUT || mode == PIN_MODE_PULLUP) {
      portConfigInputs[pin / 8] |= (1 << (pin & 7));
    } else {
      portConfigInputs[pin / 8] &= ~(1 << (pin & 7));
    }
  }
  Firmata.setPinState(pin, 0);
  switch (mode) {
    case PIN_MODE_ANALOG:
      if (IS_PIN_ANALOG(pin)) {
        if (IS_PIN_DIGITAL(pin)) {
          pinMode(PIN_TO_DIGITAL(pin), INPUT);    // disable output driver
#if ARDUINO <= 100
          // deprecated since Arduino 1.0.1 - TODO: drop support in Firmata 2.6
          digitalWrite(PIN_TO_DIGITAL(pin), LOW); // disable internal pull-ups
#endif
        }
        Firmata.setPinMode(pin, PIN_MODE_ANALOG);
      }
      break;
    case INPUT:
      if (IS_PIN_DIGITAL(pin)) {
        pinMode(PIN_TO_DIGITAL(pin), INPUT);    // disable output driver
#if ARDUINO <= 100
        // deprecated since Arduino 1.0.1 - TODO: drop support in Firmata 2.6
        digitalWrite(PIN_TO_DIGITAL(pin), LOW); // disable internal pull-ups
#endif
        Firmata.setPinMode(pin, INPUT);
      }
      break;
    case PIN_MODE_PULLUP:
      if (IS_PIN_DIGITAL(pin)) {
        pinMode(PIN_TO_DIGITAL(pin), INPUT_PULLUP);
        Firmata.setPinMode(pin, PIN_MODE_PULLUP);
        Firmata.setPinState(pin, 1);
      }
      break;
    case OUTPUT:
      if (IS_PIN_DIGITAL(pin)) {
        if (Firmata.getPinMode(pin) == PIN_MODE_PWM) {
          // Disable PWM if pin mode was previously set to PWM.
          digitalWrite(PIN_TO_DIGITAL(pin), LOW);
        }
        pinMode(PIN_TO_DIGITAL(pin), OUTPUT);
        Firmata.setPinMode(pin, OUTPUT);
      }
      break;
    case PIN_MODE_PWM:
      if (IS_PIN_PWM(pin)) {
        pinMode(PIN_TO_PWM(pin), OUTPUT);
        analogWrite(PIN_TO_PWM(pin), 0);
        Firmata.setPinMode(pin, PIN_MODE_PWM);
      }
      break;
    case PIN_MODE_SERVO:
      if (IS_PIN_DIGITAL(pin)) {
        Firmata.setPinMode(pin, PIN_MODE_SERVO);
        if (servoPinMap[pin] == 255 || !servos[servoPinMap[pin]].attached()) {
          // pass -1 for min and max pulse values to use default values set
          // by Servo library
          attachServo(pin, -1, -1);
        }
      }
      break;
    case PIN_MODE_I2C:
      if (IS_PIN_I2C(pin)) {
        // mark the pin as i2c
        // the user must call I2C_CONFIG to enable I2C for a device
        Firmata.setPinMode(pin, PIN_MODE_I2C);
      }
      break;
    case PIN_MODE_SERIAL:
#ifdef FIRMATA_SERIAL_FEATURE
      serialFeature.handlePinMode(pin, PIN_MODE_SERIAL);
#endif
      break;
    default:
      Firmata.sendString("Unknown pin mode"); // TODO: put error msgs in EEPROM
  }
  // TODO: save status to EEPROM here, if changed
}

/*
 * Sets the value of an individual pin. Useful if you want to set a pin value but
 * are not tracking the digital port state.
 * Can only be used on pins configured as OUTPUT.
 * Cannot be used to enable pull-ups on Digital INPUT pins.
 */
void setPinValueCallback(byte pin, int value)
{
  if (pin < TOTAL_PINS && IS_PIN_DIGITAL(pin)) {
    if (Firmata.getPinMode(pin) == OUTPUT) {
      Firmata.setPinState(pin, value);
      digitalWrite(PIN_TO_DIGITAL(pin), value);
    }
  }
}

void analogWriteCallback(byte pin, int value)
{
  if (pin < TOTAL_PINS) {
    switch (Firmata.getPinMode(pin)) {
      case PIN_MODE_SERVO:
        if (IS_PIN_DIGITAL(pin))
          servos[servoPinMap[pin]].write(value);
        Firmata.setPinState(pin, value);
        break;
      case PIN_MODE_PWM:
        if (IS_PIN_PWM(pin))
          analogWrite(PIN_TO_PWM(pin), value);
        Firmata.setPinState(pin, value);
        break;
    }
  }
}

void digitalWriteCallback(byte port, int value)
{
  byte pin, lastPin, pinValue, mask = 1, pinWriteMask = 0;

  if (port < TOTAL_PORTS) {
    // create a mask of the pins on this port that are writable.
    lastPin = port * 8 + 8;
    if (lastPin > TOTAL_PINS) lastPin = TOTAL_PINS;
    for (pin = port * 8; pin < lastPin; pin++) {
      // do not disturb non-digital pins (eg, Rx & Tx)
      if (IS_PIN_DIGITAL(pin)) {
        // do not touch pins in PWM, ANALOG, SERVO or other modes
        if (Firmata.getPinMode(pin) == OUTPUT || Firmata.getPinMode(pin) == INPUT) {
          pinValue = ((byte)value & mask) ? 1 : 0;
          if (Firmata.getPinMode(pin) == OUTPUT) {
            pinWriteMask |= mask;
          } else if (Firmata.getPinMode(pin) == INPUT && pinValue == 1 && Firmata.getPinState(pin) != 1) {
            // only handle INPUT here for backwards compatibility
#if ARDUINO > 100
            pinMode(pin, INPUT_PULLUP);
#else
            // only write to the INPUT pin to enable pullups if Arduino v1.0.0 or earlier
            pinWriteMask |= mask;
#endif
          }
          Firmata.setPinState(pin, pinValue);
        }
      }
      mask = mask << 1;
    }
    writePort(port, (byte)value, pinWriteMask);
  }
}


// -----------------------------------------------------------------------------
/* sets bits in a bit array (int) to toggle the reporting of the analogIns
 */
//void FirmataClass::setAnalogPinReporting(byte pin, byte state) {
//}
void reportAnalogCallback(byte analogPin, int value)
{
  if (analogPin < TOTAL_ANALOG_PINS) {
    if (value == 0) {
      analogInputsToReport = analogInputsToReport & ~ (1 << analogPin);
    } else {
      analogInputsToReport = analogInputsToReport | (1 << analogPin);
      // prevent during system reset or all analog pin values will be reported
      // which may report noise for unconnected analog pins
      if (!isResetting) {
        // Send pin value immediately. This is helpful when connected via
        // ethernet, wi-fi or bluetooth so pin states can be known upon
        // reconnecting.
        Firmata.sendAnalog(analogPin, analogRead(analogPin));
      }
    }
  }
  // TODO: save status to EEPROM here, if changed
}

void reportDigitalCallback(byte port, int value)
{
  if (port < TOTAL_PORTS) {
    reportPINs[port] = (byte)value;
    // Send port value immediately. This is helpful when connected via
    // ethernet, wi-fi or bluetooth so pin states can be known upon
    // reconnecting.
    if (value) outputPort(port, readPort(port, portConfigInputs[port]), true);
  }
  // do not disable analog reporting on these 8 pins, to allow some
  // pins used for digital, others analog.  Instead, allow both types
  // of reporting to be enabled, but check if the pin is configured
  // as analog when sampling the analog inputs.  Likewise, while
  // scanning digital pins, portConfigInputs will mask off values from any
  // pins configured as analog
}

/*==============================================================================
 * SYSEX-BASED commands
 *============================================================================*/

void sysexCallback(byte command, byte argc, byte *argv)
{
  byte mode;
  byte stopTX;
  byte slaveAddress;
  byte data;
  int slaveRegister;
  unsigned int delayTime;

  switch (command) {
    case I2C_REQUEST:
      mode = argv[1] & I2C_READ_WRITE_MODE_MASK;
      if (argv[1] & I2C_10BIT_ADDRESS_MODE_MASK) {
        Firmata.sendString("10-bit addressing not supported");
        return;
      }
      else {
        slaveAddress = argv[0];
      }

      // need to invert the logic here since 0 will be default for client
      // libraries that have not updated to add support for restart tx
      if (argv[1] & I2C_END_TX_MASK) {
        stopTX = I2C_RESTART_TX;
      }
      else {
        stopTX = I2C_STOP_TX; // default
      }

      switch (mode) {
        case I2C_WRITE:
          Wire.beginTransmission(slaveAddress);
          for (byte i = 2; i < argc; i += 2) {
            data = argv[i] + (argv[i + 1] << 7);
            wireWrite(data);
          }
          Wire.endTransmission();
          delayMicroseconds(70);
          break;
        case I2C_READ:
          if (argc == 6) {
            // a slave register is specified
            slaveRegister = argv[2] + (argv[3] << 7);
            data = argv[4] + (argv[5] << 7);  // bytes to read
          }
          else {
            // a slave register is NOT specified
            slaveRegister = I2C_REGISTER_NOT_SPECIFIED;
            data = argv[2] + (argv[3] << 7);  // bytes to read
          }
          readAndReportData(slaveAddress, (int)slaveRegister, data, stopTX);
          break;
        case I2C_READ_CONTINUOUSLY:
          if ((queryIndex + 1) >= I2C_MAX_QUERIES) {
            // too many queries, just ignore
            Firmata.sendString("too many queries");
            break;
          }
          if (argc == 6) {
            // a slave register is specified
            slaveRegister = argv[2] + (argv[3] << 7);
            data = argv[4] + (argv[5] << 7);  // bytes to read
          }
          else {
            // a slave register is NOT specified
            slaveRegister = (int)I2C_REGISTER_NOT_SPECIFIED;
            data = argv[2] + (argv[3] << 7);  // bytes to read
          }
          queryIndex++;
          query[queryIndex].addr = slaveAddress;
          query[queryIndex].reg = slaveRegister;
          query[queryIndex].bytes = data;
          query[queryIndex].stopTX = stopTX;
          break;
        case I2C_STOP_READING:
          byte queryIndexToSkip;
          // if read continuous mode is enabled for only 1 i2c device, disable
          // read continuous reporting for that device
          if (queryIndex <= 0) {
            queryIndex = -1;
          } else {
            queryIndexToSkip = 0;
            // if read continuous mode is enabled for multiple devices,
            // determine which device to stop reading and remove it's data from
            // the array, shifiting other array data to fill the space
            for (byte i = 0; i < queryIndex + 1; i++) {
              if (query[i].addr == slaveAddress) {
                queryIndexToSkip = i;
                break;
              }
            }

            for (byte i = queryIndexToSkip; i < queryIndex + 1; i++) {
              if (i < I2C_MAX_QUERIES) {
                query[i].addr = query[i + 1].addr;
                query[i].reg = query[i + 1].reg;
                query[i].bytes = query[i + 1].bytes;
                query[i].stopTX = query[i + 1].stopTX;
              }
            }
            queryIndex--;
          }
          break;
        default:
          break;
      }
      break;
    case I2C_CONFIG:
      delayTime = (argv[0] + (argv[1] << 7));

      if (argc > 1 && delayTime > 0) {
        i2cReadDelayTime = delayTime;
      }

      if (!isI2CEnabled) {
        enableI2CPins();
      }

      break;
    case SERVO_CONFIG:
      if (argc > 4) {
        // these vars are here for clarity, they'll optimized away by the compiler
        byte pin = argv[0];
        int minPulse = argv[1] + (argv[2] << 7);
        int maxPulse = argv[3] + (argv[4] << 7);

        if (IS_PIN_DIGITAL(pin)) {
          if (servoPinMap[pin] < MAX_SERVOS && servos[servoPinMap[pin]].attached()) {
            detachServo(pin);
          }
          attachServo(pin, minPulse, maxPulse);
          setPinModeCallback(pin, PIN_MODE_SERVO);
        }
      }
      break;
    case SAMPLING_INTERVAL:
      if (argc > 1) {
        samplingInterval = argv[0] + (argv[1] << 7);
        if (samplingInterval < MINIMUM_SAMPLING_INTERVAL) {
          samplingInterval = MINIMUM_SAMPLING_INTERVAL;
        }
      } else {
        //Firmata.sendString("Not enough data");
      }
      break;
    case EXTENDED_ANALOG:
      if (argc > 1) {
        int val = argv[1];
        if (argc > 2) val |= (argv[2] << 7);
        if (argc > 3) val |= (argv[3] << 14);
        analogWriteCallback(argv[0], val);
      }
      break;
    case CAPABILITY_QUERY:
      Firmata.write(START_SYSEX);
      Firmata.write(CAPABILITY_RESPONSE);
      for (byte pin = 0; pin < TOTAL_PINS; pin++) {
        if (IS_PIN_DIGITAL(pin)) {
          Firmata.write((byte)INPUT);
          Firmata.write(1);
          Firmata.write((byte)PIN_MODE_PULLUP);
          Firmata.write(1);
          Firmata.write((byte)OUTPUT);
          Firmata.write(1);
        }
        if (IS_PIN_ANALOG(pin)) {
          Firmata.write(PIN_MODE_ANALOG);
          Firmata.write(10); // 10 = 10-bit resolution
        }
        if (IS_PIN_PWM(pin)) {
          Firmata.write(PIN_MODE_PWM);
          Firmata.write(DEFAULT_PWM_RESOLUTION);
        }
        if (IS_PIN_DIGITAL(pin)) {
          Firmata.write(PIN_MODE_SERVO);
          Firmata.write(14);
        }
        if (IS_PIN_I2C(pin)) {
          Firmata.write(PIN_MODE_I2C);
          Firmata.write(1);  // TODO: could assign a number to map to SCL or SDA
        }
#ifdef FIRMATA_SERIAL_FEATURE
        serialFeature.handleCapability(pin);
#endif
        Firmata.write(127);
      }
      Firmata.write(END_SYSEX);
      break;
    case PIN_STATE_QUERY:
      if (argc > 0) {
        byte pin = argv[0];
        Firmata.write(START_SYSEX);
        Firmata.write(PIN_STATE_RESPONSE);
        Firmata.write(pin);
        if (pin < TOTAL_PINS) {
          Firmata.write(Firmata.getPinMode(pin));
          Firmata.write((byte)Firmata.getPinState(pin) & 0x7F);
          if (Firmata.getPinState(pin) & 0xFF80) Firmata.write((byte)(Firmata.getPinState(pin) >> 7) & 0x7F);
          if (Firmata.getPinState(pin) & 0xC000) Firmata.write((byte)(Firmata.getPinState(pin) >> 14) & 0x7F);
        }
        Firmata.write(END_SYSEX);
      }
      break;
    case ANALOG_MAPPING_QUERY:
      Firmata.write(START_SYSEX);
      Firmata.write(ANALOG_MAPPING_RESPONSE);
      for (byte pin = 0; pin < TOTAL_PINS; pin++) {
        Firmata.write(IS_PIN_ANALOG(pin) ? PIN_TO_ANALOG(pin) : 127);
      }
      Firmata.write(END_SYSEX);
      break;

    case SERIAL_MESSAGE:
#ifdef FIRMATA_SERIAL_FEATURE
      serialFeature.handleSysex(command, argc, argv);
#endif
      break;
  }
}

/*==============================================================================
 * SETUP()
 *============================================================================*/

void systemResetCallback()
{
  isResetting = true;

  // initialize a defalt state
  // TODO: option to load config from EEPROM instead of default

#ifdef FIRMATA_SERIAL_FEATURE
  serialFeature.reset();
#endif

  if (isI2CEnabled) {
    disableI2CPins();
  }

  for (byte i = 0; i < TOTAL_PORTS; i++) {
    reportPINs[i] = false;    // by default, reporting off
    portConfigInputs[i] = 0;  // until activated
    previousPINs[i] = 0;
  }

  for (byte i = 0; i < TOTAL_PINS; i++) {
    // pins with analog capability default to analog input
    // otherwise, pins default to digital output
    if (IS_PIN_ANALOG(i)) {
      // turns off pullup, configures everything
      setPinModeCallback(i, PIN_MODE_ANALOG);
    } else if (IS_PIN_DIGITAL(i)) {
      // sets the output to 0, configures portConfigInputs
      setPinModeCallback(i, OUTPUT);
    }

    servoPinMap[i] = 255;
  }
  // by default, do not report any analog inputs
  analogInputsToReport = 0;

  detachedServoCount = 0;
  servoCount = 0;

  /* send digital inputs to set the initial state on the host computer,
   * since once in the loop(), this firmware will only send on change */
  /*
  TODO: this can never execute, since no pins default to digital input
        but it will be needed when/if we support EEPROM stored config
  for (byte i=0; i < TOTAL_PORTS; i++) {
    outputPort(i, readPort(i, portConfigInputs[i]), true);
  }
  */
  isResetting = false;
}

// Modifico la funzione stringCallback in base alle mie esigenze.

void stringCallback(char *myString){   // Leggo stringa in arrivo da ESP8266

    Serial.print("Stringa Arrivata da ESP8266: ");
    Serial.println(myString);

    char * token = strtok(myString, " ");

    if(utente == 1){

        if(token != NULL){

            if(strcmp(token, "scambio") == 0){

                token = strtok(NULL, " ");
                
                int numeroScambioSelezionato = atoi(token);

                if(scambi[numeroScambioSelezionato] == 1){

                    azionaScambio(-numeroScambioSelezionato);

                }

                else{

                    azionaScambio(numeroScambioSelezionato);

                }
    
            }

        }

        if(strcmp(token, "stop") == 0){

            invioPacchettoEmergenza();

        }

        if(strcmp(token, "velocita") == 0){

            token = strtok(NULL, " ");
            int numeroLocomotivaSelezionata = atoi(token);

            token = strtok(NULL, " ");
            int velocitaLocomotiva = atoi(token);

            treni[numeroLocomotivaSelezionata].velocita = velocitaLocomotiva;

            componiPacchettoVelocitaEsteso(&pacchettoDaInviareEsteso, numeroLocomotivaSelezionata, velocitaLocomotiva, treni[numeroLocomotivaSelezionata].direzioneAvanti, false);
            inviaPacchetto(pacchettoDaInviareEsteso);

        }

        if(strcmp(token, "luciLocomotiva") == 0){

            token = strtok(NULL, " ");
            int numeroLocomotivaSelezionata = atoi(token);

            treni[numeroLocomotivaSelezionata].luci = !treni[numeroLocomotivaSelezionata].luci;

            componiPacchettoLuci(&pacchettoDaInviare, numeroLocomotivaSelezionata, treni[numeroLocomotivaSelezionata].luci);
            inviaPacchetto(pacchettoDaInviare);

        }

        if(strcmp(token, "direzioneLocomotiva") == 0){

            token = strtok(NULL, " ");
            int numeroLocomotivaSelezionata = atoi(token);

            treni[numeroLocomotivaSelezionata].direzioneAvanti = !treni[numeroLocomotivaSelezionata].direzioneAvanti;

            componiPacchettoVelocitaEsteso(&pacchettoDaInviareEsteso, numeroLocomotivaSelezionata, treni[numeroLocomotivaSelezionata].velocita, treni[numeroLocomotivaSelezionata].direzioneAvanti, false);

        }

        if(strcmp(token, "setScambi") == 0){

            token = strtok(NULL, " ");

            if(strcmp(token, "1") == 0){

                setAnelloEsterno();
                
            }

            if(strcmp(token, "2") == 0){

                setAnelloInterno();

            }

            if(strcmp(token, "3") == 0){

                setAnelloEsternoEsteso();
                
            }

            if(strcmp(token, "4") == 0){

                setAnelloEsternoEstesissimoPrimoPiano();
                
            }

            if(strcmp(token, "5") == 0){

                setAnelloEsternoEstesissimoSecondoPiano();
                
            }

            if(strcmp(token, "6") == 0){

                setStazione1Binario3();
                
            }

            if(strcmp(token, "7") == 0){

                setStazione2Binario0();
                
            }

            if(strcmp(token, "8") == 0){

                setStazione2Binario1();
                
            }

            if(strcmp(token, "9") == 0){

                setStazione2Binario2();
                
            }

            if(strcmp(token, "10") == 0){

                setStazione2Binario3();
                
            }

        }       
        
        if(strcmp(token, "autopilot") == 0){

            token = strtok(NULL, " ");

            // TODO AUTOPILOT
            
            if(strcmp(token, "1") == 0){

            }

            if(strcmp(token, "2") == 0){

            }

            if(strcmp(token, "3") == 0){
                
            }

            if(strcmp(token, "4") == 0){

            }

            if(strcmp(token, "5") == 0){
                
            }

        }

        if(strcmp(token, "spegni") == 0){

            spegnimentoSicuro();

        }

    }

    if(strcmp(token, "ip") == 0){

        token = strtok(NULL, " ");

        webServerAttivo = true;

        strcpy(indirizzoIP, token);

        displayIP();

    }

}

void setupFirmata(){

    Firmata.setFirmwareVersion(FIRMATA_FIRMWARE_MAJOR_VERSION, FIRMATA_FIRMWARE_MINOR_VERSION);

    Firmata.attach(ANALOG_MESSAGE, analogWriteCallback);
    Firmata.attach(DIGITAL_MESSAGE, digitalWriteCallback);
    Firmata.attach(REPORT_ANALOG, reportAnalogCallback);
    Firmata.attach(REPORT_DIGITAL, reportDigitalCallback);
    Firmata.attach(SET_PIN_MODE, setPinModeCallback);
    Firmata.attach(SET_DIGITAL_PIN_VALUE, setPinValueCallback);
    Firmata.attach(START_SYSEX, sysexCallback);
    Firmata.attach(SYSTEM_RESET, systemResetCallback);

    Firmata.attach(STRING_DATA, stringCallback); // Importante per lettura delle stringhe.

    // to use a port other than Serial, such as Serial1 on an Arduino Leonardo or Mega,
    // Call begin(baud) on the alternate serial port and pass it to Firmata to begin like this:
    // Serial1.begin(57600);
    // Firmata.begin(Serial1);
    // However do not do this if you are using SERIAL_MESSAGE
    
    Serial2.begin(57600);
    Firmata.begin(Serial2);

    while (!Serial2) {
            ; // wait for serial port to connect. Needed for ATmega32u4-based boards and Arduino 101
        }

    systemResetCallback();  // reset to default config

}

void istruzioniFirmata(){

    byte pin, analogPin;

    /* DIGITALREAD - as fast as possible, check for changes and output them to the
    * FTDI buffer using Serial.print()  */
    checkDigitalInputs();

    /* STREAMREAD - processing incoming messagse as soon as possible, while still
    * checking digital inputs.  */
    while (Firmata.available())
        Firmata.processInput();

    // TODO - ensure that Stream buffer doesn't go over 60 bytes

    currentMillis = millis();
    if (currentMillis - previousMillis > samplingInterval) {
        previousMillis += samplingInterval;
        /* ANALOGREAD - do all analogReads() at the configured sampling interval */
        for (pin = 0; pin < TOTAL_PINS; pin++) {
        if (IS_PIN_ANALOG(pin) && Firmata.getPinMode(pin) == PIN_MODE_ANALOG) {
            analogPin = PIN_TO_ANALOG(pin);
            if (analogInputsToReport & (1 << analogPin)) {
            Firmata.sendAnalog(analogPin, analogRead(analogPin));
            }
        }
        }
        // report i2c data for all device with read continuous mode enabled
        if (queryIndex > -1) {
        for (byte i = 0; i < queryIndex + 1; i++) {
            readAndReportData(query[i].addr, query[i].reg, query[i].bytes, query[i].stopTX);
        }
        }
    }

    #ifdef FIRMATA_SERIAL_FEATURE
    serialFeature.update();
    #endif

}



// ***FINE FIRMATA*** //



// ***SETUP*** //



 void setup(){

    // Inizializzo porta seriale

    Serial.begin(9600);
    
    // Inizializzo pin della scheda L298N

    pinMode(pinENA, OUTPUT);
    pinMode(pinIN1, OUTPUT);
    pinMode(pinIN2, OUTPUT);

    // Inizializzo Display

    display.begin(16,4); 
    display.backlight();

    // Visualizzo credits sul display per 2 secondi

    displayCredits();
    delay(2000);
    pulisciDisplay();

    // Login

    displayMenuLogin();
    pulisciDisplay();

    // Leggo lo stato degli scambi (al momento dell'ultimo spegnimento sicuro)

    leggoStatoScambi();

    delay(2000);

    // Invio ad ESP8266 lo stato degli scambi

    invioPosizioneScambi();

    // Setup Firmata

    setupFirmata();

    // Inizializzo pacchetto Idle

    inizializzaPacchettoIdle(); 

    // Inizializzo pacchetto Stop Emergenza

    componiPacchettoVelocita(&pacchettoStopEmergenza, 1, 0, false, true);
    
    // Inizializzo dimensione pacchetto extended

    pacchettoDaInviareEsteso.numeroBitPacchetto = 51;

    // Inizializzo Porta Seriale 1 per comunicazione con Arduino Slave

    Serial1.begin(57600);

    marshaller.begin(Serial1);

    // Inizializzo pin Arduino Slave

    for(int i = pinRelayAscissa1; i < pinRelayAscissa32 + 1; i++){

        marshaller.sendPinMode(i, OUTPUT);

    }

    for(int i = pinRelayOrdinata1; i < pinRelayOrdinata4 + 1; i++){

        marshaller.sendPinMode(i, OUTPUT);

    }

    // Fornisco corrente al tracciato

    digitalWrite(pinENA, HIGH); 

    // Inizializzo interrupt

    if(INTERRUPT_ENABLE){

        Timer1.initialize(TEMPO_IMPULSO_BIT_1); // Inizializziamo il timer dell'interrupt
        Timer1.attachInterrupt(timerIsr);       // Ancoriamo la funzione che vogliamo venga chiamata quando l'interrupt si solleva

    }

 }



// ***LOOP*** //



 void loop(){

     // Se il Web Server è attivo e l'utente è abilitato ad usarlo non gestisco keypad e display LCD

    if(utente >= 2 || !webServerAttivo){

        gestisciDisplayKeypad();

        indirizzoIPdisplay = false;

    }

    else{

        // Stampo sul display l'indirizzo IP soltanto se ho cambiato utente

        if(!indirizzoIPdisplay){

            pulisciDisplay();

            displayIP();

        }
        
        indirizzoIPdisplay = true;

    }

    istruzioniFirmata();

 }


// Funzione invocata in timerIsr (funzione invocata ad ogni interrupt)

 void Segnale_DCC(){

    int impulsoInviato = 0;

    // Se c'è un bit già in codifica che è in attesa di un altro impulso

    if(listaPacchettiCoda.numeroPulsazioniRimanenti != 0){ 
        
        impulsoInviato = 1;

        if(listaPacchettiCoda.bitCorrente == 1){
            
            digitalWrite(3, HIGH);
            digitalWrite(4, LOW);

            listaPacchettiCoda.indiceBitCorrente += 1;
            listaPacchettiCoda.numeroPulsazioniRimanenti -= 1;

        }

        else{

            if(listaPacchettiCoda.numeroPulsazioniRimanenti != NUMERO_IMPULSI_0 / 2){

                listaPacchettiCoda.numeroPulsazioniRimanenti -= 1;

                if(listaPacchettiCoda.numeroPulsazioniRimanenti == 1){

                    listaPacchettiCoda.indiceBitCorrente +=1;

                }

            }

            else{

                digitalWrite(3, HIGH);
                digitalWrite(4, LOW);
                
                listaPacchettiCoda.numeroPulsazioniRimanenti -= 1;

            }

        }

     }

    // Il pacchetto è stato totalmente inviato. Passiamo al successivo.

    else if(listaPacchettiCoda.indiceBitCorrente == listaPacchettiCoda.pacchetto.numeroBitPacchetto){ 
        
        listaPacchettiCoda.numeroRipetizioniPacchetto += 1;
        listaPacchettiCoda.indiceBitCorrente = 0;

        if(listaPacchettiCoda.numeroRipetizioniPacchetto == NUMERO_RIPETIZIONI_PACCHETTO){

            listaPacchettiCoda.numeroRipetizioniPacchetto = 0;

            if(listaPacchettiCoda.pacchettoSuccessivoAttivo){
                
                copiaPacchetto(&listaPacchettiCoda.pacchettoSuccessivo, &listaPacchettiCoda.pacchetto);

                listaPacchettiCoda.pacchettoSuccessivoAttivo = false;

            }

            else{

                listaPacchettiCoda.pacchetto = pacchettoIdle;

            }

        }

    }

    // Se non c'è nessun bit in codifica in attesa capiamo il valore del prossimo bit da inviare

    if(!impulsoInviato){

        if(listaPacchettiCoda.pacchetto.bitPacchetto[listaPacchettiCoda.indiceBitCorrente] == 1){ // Il bit è 1

            digitalWrite(3, LOW);
            digitalWrite(4, HIGH);

            listaPacchettiCoda.bitCorrente = 1;
            listaPacchettiCoda.numeroPulsazioniRimanenti = 1;

        }  

        // Il bit è 0

        else{ 

            digitalWrite(3, LOW);
            digitalWrite(4, HIGH);

            listaPacchettiCoda.bitCorrente = 0;
            listaPacchettiCoda.numeroPulsazioniRimanenti = NUMERO_IMPULSI_0 - 1;

        }

    }

 }

// Funzione invocata ad ogni interrupt

 void timerIsr(){
     
     Segnale_DCC();

 }
 
