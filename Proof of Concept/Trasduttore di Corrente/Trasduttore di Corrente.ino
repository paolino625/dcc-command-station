/*

Prova Sensore Lettura Corrente

*/

void setup(){

    Serial.begin(9600);

}

void loop(){

    unsigned int x = 0;
    float AcsValue = 0.0, Samples = 0.0, AvgAcs = 0.0, AcsValueF = 0.0;

    for(int x = 0; i < 150; x++){

        AcsValue = analogRead(A0);
        Samples = Samples + AcsValue;
        delay(3);

    }

    AvgAcs = Samples/150.0;

    AcsValueF = (2.5 - (AvgAcs * (5.0 / 1024.0)))/0.185;

    Serial.println(AcsValueF);

}