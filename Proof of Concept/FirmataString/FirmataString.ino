// L'altro Arduino fa marshaller.sendString("stringa")

#include <Firmata.h>

void stringCallback(char *myString){

  digitalWrite(2, HIGH); // Il led built_in dell'ESP8266 funziona al contrario
  delay(2000);
  digitalWrite(2, LOW); // Il led built_in dell'ESP8266 funziona al contrario
  delay(2000);

}

void sysexCallback(byte command, byte argc, byte *argv){

  Firmata.sendSysex(command, argc, argv);

}

void setup() {

  Serial.begin(57600);

  Firmata.setFirmwareVersion(FIRMATA_FIRMWARE_MAJOR_VERSION, FIRMATA_FIRMWARE_MINOR_VERSION);
  Firmata.attach(STRING_DATA, stringCallback);
  Firmata.attach(START_SYSEX, sysexCallback);
  Firmata.begin(57600);

}

void loop () {

  while (Firmata.available()) {

    Firmata.processInput();

  }

}