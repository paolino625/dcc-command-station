#include <FirmataMarshaller.h>

#define LED 13

firmata::FirmataMarshaller marshaller;

void setup() {

  Serial.begin(57600);

  marshaller.begin(Serial);
  marshaller.sendPinMode(LED, OUTPUT);

}

void loop () {

  marshaller.sendDigital(LED, HIGH);
  delay(1000);
  marshaller.sendDigital(LED, LOW);
  delay(1000);

}