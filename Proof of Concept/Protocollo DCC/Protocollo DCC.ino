/*

PROOF OF CONCEPT CON L298N

*/

int tempo1, tempo2;

void impulsoLogico1(){

  // Per codificare il bit 1 
  
  digitalWrite(3, LOW); // 3 è collegato al pin IN1 della scheda L298N
  digitalWrite(4, HIGH); // 4 è collegato al pin IN2 della scheda L298N
  delayMicroseconds(53); // Microsecondi necessari per codificare il bit 1 // 

  // Valori: max = 53 min = 22

  digitalWrite(3, HIGH); // 3 è collegato al pin IN1 della scheda L298N
  digitalWrite(4, LOW); // 4 è collegato al pin IN2 della scheda L298N
  delayMicroseconds(53); // Microsecondi necessari per codificare il bit 1

  // Se IN1 è LOW e IN2 è HIGH allora il motore si muoverà avanti
  // Se IN1 è HIGH e IN2 è LOW allora il motore si muoverà all'indietro
  // Nel caso in cui sia IN1 che IN2 sono uguali il motore si spegne
  
}

void impulsoLogico0(){

  digitalWrite(3, LOW); // 3 è collegato al pin IN1 della scheda L298N
  digitalWrite(4, HIGH); // 4 è collegato al pin IN2 della scheda L298N
  delayMicroseconds(97); // Microsecondi necessari per codificare il bit 0

  digitalWrite(3, HIGH); // 3 è collegato al pin IN1 della scheda L298N
  digitalWrite(4, LOW); // 4 è collegato al pin IN2 della scheda L298N
  delayMicroseconds(97); // Microsecondi necessari per codificare il bit 0

  // Se IN1 è LOW e IN2 è HIGH allora il motore si muoverà avanti
  // Se IN1 è HIGH e IN2 è LOW allora il motore si muoverà all'indietro
  // Nel caso in cui sia IN1 che IN2 sono uguali il motore si spegne

}

void invioPacketStartBit(){ // Necessario nel pacchetto per separare i vari data byte

    impulsoLogico0(); 

}

void invioPacketEndBit(){ // Necessario nel pacchetto per contraddistinguere la fine del pacchetto

  impulsoLogico1();

}

void bitAdImpulsi(boolean numero){ // Trasformo un numero nel corrispondente impulso

  if(numero == 1){

    impulsoLogico1(); 

  }

  else{

    impulsoLogico0();

  }

}

/*

PREAMBOLO

La prima sequenza di impulsi in un pacchetto viene chiamato preambolo.
Un messaggio digitale DCC o pacchetto inizia con 14 impulsi logici "1".

*/

void invioPreambolo(){

  for(byte k = 0; k < 14; k++){

    impulsoLogico1();

  }

}

/*

1° DATA BYTE

Tale sequenza di bit identifica la locomotiva a cui è indirizzato il messaggio.
L'indirizzo della locomotiva è di 8 bit (l'indirizzo 3 è l'indirizzo standard).
[Nel nostro esempio utilizziamo l'indirizzo 1]

*/

boolean indirizzoLocomotiva3[] = {0,0,0,0,0,0,0,1};

void invioIndirizzoLocomotiva(){

  for(byte k = 0; k < 8; k++){

    bitAdImpulsi(indirizzoLocomotiva3[k]);

  }

}

/*

2° DATA BYTE

Tale sequenza di bit indica la tipologia dell'istruzione ed eventuali informazioni aggiuntive.

Nel nostro caso utilizziamo l'istruzione velocità.
Sequenza: 12345678
Bit 1-2: contengono la sequenza di bit: "01" che indicare che il data byte istruzione è per la velocità e la direzione.
Bit 3: Bit per la direzione. Se il bit è 1 la locomotiva si muove avanti. Se il bit è 0 si muoverà all'indietro.
Bit 4: per impostazione predefinita deve contenere un bit di velocità aggiuntivo, che è il bit di velocità meno significativo
Bit 5-8: indicano la velocità dove 0 è la velocità meno significativa

*/

const boolean codiceIstruzioneVelocita[] = {0,1,0,0,0,0,1,0};

void invioCodiceIstruzioneVelocita(){

  for(byte k = 0; k < 8; k++){

    bitAdImpulsi(codiceIstruzioneVelocita[k]);

  }

}

/*

3° DATA BYTE - Error Detection

Ora il decoder necessita di verificare che ha un ricevuto un messaggio non corrotto.
XOR sui byte precedenti

*/

boolean errorDetection[8];

void calcoloEOR(){

  for(byte k= 0; k < 8; k++){

    errorDetection[k] = indirizzoLocomotiva3[k] ^ codiceIstruzioneVelocita[k];

  }

}

void invioErrorDetection(){

  for(byte k = 0; k < 8; k++){

    bitAdImpulsi(errorDetection[k]);

  }

}

void idle1() { // Utile nella costruzione del pacchetto idle

  for (byte k = 0; k < 8; ++k) {

    impulsoLogico1();

  }

}
void idle0() { // Utile nella costruzione del pacchetto idle

  for (byte  k = 0;  k < 8;  ++k) {

    impulsoLogico0();

  }

}

/*

Componiamo il pacchetto completo e mandiamolo.

*/

void invioPacchetto(){

  invioPreambolo();
  invioPacketStartBit();
  invioIndirizzoLocomotiva();
  invioPacketStartBit();
  invioCodiceIstruzioneVelocita();
  invioPacketStartBit();
  invioErrorDetection();
  invioPacketEndBit();

}

/*

Qualora non avessimo pacchetti di istruzioni da mandare, dobbiamo comunque continuare a mandare degli impulsi idle per continuare ad alimentare le locomotive.

*/

void pacchettiIdle(byte numero){

  for(byte j = 0; j < numero; j++){

    invioPreambolo();
    impulsoLogico0(); // start bit
    idle1();
    impulsoLogico0(); // start bit
    idle0();
    impulsoLogico0(); // end bit
    idle1();
    impulsoLogico1();

  }

}

/*

PROGRAMMA SETUP

*/

void setup(){

 Serial.begin(9600); //Utile in fase di debugging

 pinMode(2, OUTPUT); // 2 è collegato al pin ENA della scheda L298N
 pinMode(3, OUTPUT); // 3 è collegato al pin IN1 della scheda L298N
 pinMode(4, OUTPUT); // 4 è collegato al pin IN2 della scheda L298N
 calcoloEOR(); // Calcolo Error Detection del pacchetto
  
 digitalWrite(2, HIGH); // Forniamo corrente al tracciato

}

void loop(){

  invioPacchetto();
  pacchettiIdle(3); // Devono passare almeno 5 msecs dalla fine dell'invio di un pacchetto per poterne inviarne un altro

}

/*

// La funzione micros ci mette 4 microsecondi per essere completata SU ARDUINO 16 MHZ

int tempo1, tempo2;
 int tempo1 = micros();
int tempo2 = micros();
Serial.println("TEMPO: ");
Serial.println(tempo1);
Serial.println(tempo2);

*/