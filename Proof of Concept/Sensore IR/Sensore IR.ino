// PROVA CON SENSORE IR

// SE L'INTERRUPT ARRIVA PROPRIO DURANTE LA LETTURA?

#define pinSensoreIR 13
#define TEMPO_LETTURA_IR 5000


void setup() {

    pinMode(pinSensoreIR,INPUT);

    Serial.begin(9600);

}

void loop() {

    bool bitPrecedente0 = false;
    int indirizzoLocomotiva = 0, valoreSensore = 1;
    long timer1, timer2, differenzaTimer = 0;

    while(valoreSensore != 0){

        valoreSensore = digitalRead(pinSensoreIR);

    }

    indirizzoLocomotiva += 1;
    bitPrecedente0 = true;

    timer1 = millis();
    
    while(differenzaTimer < TEMPO_LETTURA_IR){

        delayMicroseconds(5); // Necessario per stabilizzazione del segnale
          
        valoreSensore = digitalRead(pinSensoreIR);
        
        if(valoreSensore == 0){

            if(!bitPrecedente0){

             indirizzoLocomotiva += 1;
             bitPrecedente0 = true;

            }

        }

        else{

            bitPrecedente0 = false;

        }

        timer2 = millis();
        differenzaTimer = timer2 - timer1;

    }    

    Serial.print("Indirizzo Locomotiva:  ");
    Serial.println(indirizzoLocomotiva);

}