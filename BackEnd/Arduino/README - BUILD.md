# Build

Ci sono due sistemi di build differenti utilizzati:

Platformio: utilizzato per gestire progetti Arduino.
CMake: impiegato per eseguire i Google Test in modo nativo con supporto al debugger.

Ogni volta che si cambia tra i due sistemi, è necessario seguire questi passaggi:
- Cancellare la cartella .idea nella directory del progetto
- Aprire il file platformio.ini/CMakeLists.txt come progetto in CLion.

# Problemi

In caso di problemi strani, cancellare la cartella .pio.