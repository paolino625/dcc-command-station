È possibile effettuare l'upload del codice di ArduinoGiga anche a distanza.

# Computer host (collegato ad Arduino)

## Installazione

- Installare arduino-cli (https://arduino.github.io/arduino-cli/0.25/installation/)
  - brew update
  - brew install arduino-cli
- Installare platformIO (https://docs.platformio.org/en/latest/core/installation/methods/installer-script.html#super-quick-macos-linux)
  - Installare comandi platformIO su shell (https://docs.platformio.org/en/latest/core/installation/shell-commands.html#id3)
    - mkdir -p /usr/local/bin
    - ln -s ~/.platformio/penv/bin/platformio /usr/local/bin/platformio
    - ln -s ~/.platformio/penv/bin/pio /usr/local/bin/pio
    - ln -s ~/.platformio/penv/bin/piodebuggdb /usr/local/bin/piodebuggdb
- Installare driver JLink Segger (https://www.segger.com/downloads/jlink/)
- Effettuare log in account PlatformIO
    - pio account login

## Esecuzione

- Eseguire il comando:
    - pio remote agent start

# Computer remoto

## Installazione

- Assicurarsi che la modalità di upload nel file platformio.ini sia impostata su jlink

## Esecuzione

- Aprire terminale da IntelliJ (in modo tale che sia presente platformio)
- Eseguire il comando:
    - pio remote run --environment ArduinoMaster --target upload
- Sul computer host accettare i termini e condizioni del debugger
- Dopo upload riavviare Arduino (il debugger non lo riavvia in automatico dopo l'upload)