#include <Arduino.h>

#define pinRelay1 13
#define pinRelay2 12
#define tempoEccitazioneRelay \
    20  // Senza prolunga scambio: 10 ms funziona, 5 ms no. Con 10 metri: 15 funziona, 10 no
#define tempoRicaricaCDU 150

void setup() {
    Serial.begin(9600);

    pinMode(pinRelay1, OUTPUT);
    pinMode(pinRelay2, OUTPUT);
}

void loop() {
    digitalWrite(pinRelay1, HIGH);
    delay(tempoEccitazioneRelay);
    digitalWrite(pinRelay1, LOW);

    delay(tempoRicaricaCDU);

    digitalWrite(pinRelay2, HIGH);
    delay(tempoEccitazioneRelay);
    digitalWrite(pinRelay2, LOW);

    delay(tempoRicaricaCDU);
}
