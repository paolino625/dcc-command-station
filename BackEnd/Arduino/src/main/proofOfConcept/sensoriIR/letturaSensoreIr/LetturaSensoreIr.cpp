// PROVA CON SENSORE IR

#include <Arduino.h>

#define pinSensoreIR A1
#define TEMPO_LETTURA_IR 500

// Se il sensore IR rileva più segnalazioni di quelle che sembrerebbe guardando il LED sul sensore, alzare questo parametro
#define STABILIZZAZIONE_SEGNALE 500

int numeroRilevamento = 1;

void setup() {
    pinMode(pinSensoreIR, INPUT);

    Serial.begin(115200);
    Serial.println("Porta seriale avviata.\n");
}

void loop() {
    static unsigned long lastMillis = 0;
    unsigned long currentMillis = millis();

    if (currentMillis - lastMillis >= TEMPO_LETTURA_IR) {
        lastMillis = currentMillis;

        int valoreSensore = analogRead(pinSensoreIR);
        Serial.print("Valore sensore: ");
        Serial.println(valoreSensore);
    }
}
