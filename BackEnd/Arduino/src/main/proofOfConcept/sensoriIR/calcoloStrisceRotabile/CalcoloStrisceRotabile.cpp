// DEPRECATO

// Questa funzione mi era utile per capire la lunghezza del convoglio quando erano previste delle
// etichette sotto ogni locomotiva/vagone Ritorna lunghezza del convoglio che passa su un
// determinato sensore
/*
void aggiornoLetturaConvoglioSensore(byte sensoreInLettura, byte numeroConvoglio) {
    unsigned long timer2Rotabile, timer2Convoglio, differenzaTempoMillisecondi;

    if (misuraInCorsoRotabile[numeroConvoglio]) {
        timer2Rotabile = millis();
        differenzaTempoMillisecondi = timer2Rotabile - timer1Rotabile[numeroConvoglio];

        byte locomotivaInTrazioneConvoglio = trovoLocomotivaInTrazioneConvoglio(numeroConvoglio);

        float velocitaConvoglio =
convertiVelocitaRealeModellino(locomotive[locomotivaInTrazioneConvoglio].velocitaAttuale); float
tempoLetturaMassimoStrisceRotabileSecondi; float tempoLetturaMassimoStrisceRotabileMillisecondi;

        if (velocitaConvoglio != 0) {
            tempoLetturaMassimoStrisceRotabileSecondi = DISTANZA_MASSIMA_STRISCE_ROTABILE_CM /
velocitaConvoglio; tempoLetturaMassimoStrisceRotabileMillisecondi =
tempoLetturaMassimoStrisceRotabileSecondi * 1000; } else {
            // Imposto un valore molto basso in modo tale che mi fermi ad aspettare un'altra
striscia del rotabile tempoLetturaMassimoStrisceRotabileMillisecondi = 10;
        }

        if (differenzaTempoMillisecondi > tempoLetturaMassimoStrisceRotabileMillisecondi) {
            // Resetto timer per misurare tempo massimo di attesa per prossimo rotabile
            timer1Convoglio[numeroConvoglio] = millis();

            Serial.print(F("Numero totale strisce rotabile: "));
            Serial.println(numeroStrisceRotabile[numeroConvoglio]);
            Serial.print(F("Lunghezza convoglio parziale: "));
            lunghezzaConvoglio[numeroConvoglio] += 8 + (numeroStrisceRotabile[numeroConvoglio] * 2);
            Serial.print(lunghezzaConvoglio[numeroConvoglio]);
            Serial.println(F(" cm\n"));

            Serial.print(F("Distanza massima strisce rotabile: "));
            Serial.print(DISTANZA_MASSIMA_STRISCE_ROTABILE_CM);
            Serial.print(F(" cm\n"));

            Serial.print(F("Velocità convoglio: "));
            Serial.print(velocitaConvoglio);
            Serial.println(" cm/s");

            Serial.print(F("Tempo lettura massimo strisce rotabile: "));
            Serial.print(tempoLetturaMassimoStrisceRotabileSecondi);
            Serial.println(" s\n");

            Serial.println(F("Fine rotabile\n"));

            numeroStrisceRotabile[numeroConvoglio] = 0;
            misuraInCorsoRotabile[numeroConvoglio] = false;
        }

        if (sensoreAzionato(sensoreInLettura)) {
            numeroStrisceRotabile[numeroConvoglio] += 1;
            timer1Rotabile[numeroConvoglio] = millis();

            Serial.print(F("Numero parziale strisce rotabile: "));
            Serial.println(numeroStrisceRotabile[numeroConvoglio]);
            Serial.println();
        }

    } else {
        // Il convoglio non è ancora arrivato sul sensore. Aspetto che il primo rotabile del
convoglio passi sul sensore per far partire il timer. if (!convoglioArrivato[numeroConvoglio]) { if
(sensoreAzionato(sensoreInLettura)) { Serial.println(F("INIZIO CONVOGLIO\n"));
                Serial.println(F("Inizio rotabile\n"));
                convoglioArrivato[numeroConvoglio] = true;
                misuraInCorsoRotabile[numeroConvoglio] = true;
                numeroStrisceRotabile[numeroConvoglio] += 1;

                Serial.print(F("Numero parziale strisce rotabile: "));
                Serial.println(numeroStrisceRotabile[numeroConvoglio]);

                // Avvio timer per misurare tempo massimo di lettura tra strisce di uno stesso
rotabile timer1Rotabile[numeroConvoglio] = millis();
            }

        } else {
            // Non si tratta più del primo rotabile del convoglio. Se sono qui non c'è in corso una
lettura di un rotabile

            timer2Convoglio = millis();
            differenzaTempoMillisecondi = timer2Convoglio - timer1Convoglio[numeroConvoglio];

            byte locomotivaInTrazioneConvoglio =
trovoLocomotivaInTrazioneConvoglio(numeroConvoglio); float velocitaConvoglio =
convertiVelocitaRealeModellino(locomotive[locomotivaInTrazioneConvoglio].velocitaAttuale);

            float tempoLetturaMassimoStrisceConvoglioSecondi;
            float tempoLetturaMassimoStrisceConvoglioMillisecondi;

            if (velocitaConvoglio != 0) {
                tempoLetturaMassimoStrisceConvoglioSecondi = DISTANZA_MASSIMA_ROTABILI_CM /
velocitaConvoglio; tempoLetturaMassimoStrisceConvoglioMillisecondi =
tempoLetturaMassimoStrisceConvoglioSecondi * 1000; } else {
                // Imposto un valore molto basso in modo tale che mi fermi ad aspettare un altro
rotabile tempoLetturaMassimoStrisceConvoglioMillisecondi = 10;
            }

            // Se è passato abbastanza tempo, il convoglio è finito. Posso dunque ritornare la
lunghezza. if (differenzaTempoMillisecondi > tempoLetturaMassimoStrisceConvoglioMillisecondi) {
                Serial.print(F("Lunghezza convoglio totale: "));
                Serial.print(lunghezzaConvoglio[numeroConvoglio]);
                Serial.println(F(" cm\n"));

                Serial.print(F("Distanza strisce tra due rotabili: "));
                Serial.print(DISTANZA_MASSIMA_ROTABILI_CM);
                Serial.println(F(" cm"));
                Serial.print(F("Velocita' convoglio: "));
                Serial.print(velocitaConvoglio);
                Serial.println(" cm/s");

                Serial.print(F("Tempo lettura massimo strisce tra due rotabili: "));
                Serial.print(tempoLetturaMassimoStrisceConvoglioSecondi);
                Serial.println(F(" s\n"));

                Serial.println(F("FINE CONVOGLIO\n"));

                // Resetto variabili prima di uscire
                numeroStrisceRotabile[numeroConvoglio] = 0;
                numeroRotabiliConvoglio[numeroConvoglio] = 1;
                lunghezzaConvoglio[numeroConvoglio] = 0;
                convoglioArrivato[numeroConvoglio] = false;

                return;
            }

            if (sensoreAzionato(sensoreInLettura)) {
                Serial.println(F("Inizio rotabile\n"));
                numeroStrisceRotabile[numeroConvoglio] += 1;
                numeroRotabiliConvoglio[numeroConvoglio] += 1;
                misuraInCorsoRotabile[numeroConvoglio] = true;

                Serial.print(F("Numero strisce rotabile: "));
                Serial.println(numeroStrisceRotabile[numeroConvoglio]);
            }
        }
    }
}
*/