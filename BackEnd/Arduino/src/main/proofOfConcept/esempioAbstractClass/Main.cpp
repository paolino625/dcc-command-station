/*
Nel codice, viene definita una classe astratta chiamata "animale". Una classe astratta è una classe
che contiene almeno una funzione virtuale pura, indicata dal = 0 alla fine della dichiarazione della
funzione. Nel tuo caso, la funzione virtuale pura è emetteSuono(). Una classe astratta non può
essere istanziata direttamente; viene utilizzata come base per altre classi derivate che devono
implementare tutte le funzioni virtuali pure definite nella classe base. In parole semplici,
qualsiasi classe che eredita dalla classe animale deve fornire una propria implementazione per la
funzione emetteSuono(). La presenza di una funzione virtuale pura nella classe base implica che le
classi derivate devono implementare quella funzione, altrimenti saranno anch'esse classificate come
astratte e non potranno essere istanziate direttamente.
 */

#include <Arduino.h>

#include "Cane.cpp"
#include "Gatto.cpp"

void setup() {
    //  animale a;  // Sarebbe un errore: non possiamo avere un'istanza di una classe astratta
    cane b;
    gatto c;
    b.emetteSuono();
    c.emetteSuono();
}

void loop() {}