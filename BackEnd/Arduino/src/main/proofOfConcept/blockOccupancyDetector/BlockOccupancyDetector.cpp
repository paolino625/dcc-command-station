#include <Arduino.h>

#define BLOCK_OCCUPANCY_DETECTOR_PIN 8

bool sezioneLibera() {
    int letturaSensore = digitalRead(BLOCK_OCCUPANCY_DETECTOR_PIN);
    if (letturaSensore == 1) {
        return true;
    } else {
        return false;
    }
}

void setup() {
    Serial.begin(115200);
    pinMode(BLOCK_OCCUPANCY_DETECTOR_PIN, INPUT_PULLUP);
}

void loop() {
    if (sezioneLibera()) {
        Serial.println("Sezione libera");
    } else {
        Serial.println("Sezione occupata");
    }
}
