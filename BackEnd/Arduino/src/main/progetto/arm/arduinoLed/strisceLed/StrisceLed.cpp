// *** INCLUDE *** //

#include "StrisceLed.h"

// *** DEFINIZIONE METODI *** //

void StrisceLed::inizializza() {
    strisciaLed1PianoSinistra = new StrisciaLed(NUMERO_LED_STRISCIA_1_PIANO_SINISTRA, LUMINOSITA_MASSIMA);
    FastLED.addLeds<WS2812B, PIN_LED_STRISCIA_1_PIANO_SINISTRA, GRB>(strisciaLed1PianoSinistra->getLedArray(),
                                                                     NUMERO_LED_STRISCIA_1_PIANO_SINISTRA);

    strisciaLed1PianoDestra = new StrisciaLed(NUMERO_LED_STRISCIA_1_PIANO_DESTRA, LUMINOSITA_MASSIMA);
    FastLED.addLeds<WS2812B, PIN_LED_STRISCIA_1_PIANO_DESTRA, GRB>(strisciaLed1PianoDestra->getLedArray(),
                                                                   NUMERO_LED_STRISCIA_1_PIANO_DESTRA);
    strisciaLed1PianoEstremaSinistra =
        new StrisciaLed(NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_SINISTRA, LUMINOSITA_MASSIMA);
    FastLED.addLeds<WS2812B, PIN_LED_STRISCIA_1_PIANO_ESTREMA_SINISTRA, GRB>(
        strisciaLed1PianoEstremaSinistra->getLedArray(), NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_SINISTRA);

    strisciaLed1PianoEstremaDestra = new StrisciaLed(NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA, LUMINOSITA_MASSIMA);
    FastLED.addLeds<WS2812B, PIN_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA, GRB>(
        strisciaLed1PianoEstremaDestra->getLedArray(), NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA);

    strisciaLed3Piano = new StrisciaLed(NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA, LUMINOSITA_MASSIMA);
    FastLED.addLeds<WS2812B, PIN_LED_STRISCIA_3_PIANO, GRB>(strisciaLed3Piano->getLedArray(),
                                                            NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA);
}

// *** DEFINIZIONE VARIABILI *** //

StrisceLed strisceLed;