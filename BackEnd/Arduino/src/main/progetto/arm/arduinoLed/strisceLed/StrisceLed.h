#ifndef ARDUINO_STRISCELED_H
#define ARDUINO_STRISCELED_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoLed/strisceLed/strisciaLed/StrisciaLed.h"

// *** DEFINE *** //

#define PIN_LED_STRISCIA_1_PIANO_SINISTRA 5
#define PIN_LED_STRISCIA_1_PIANO_DESTRA 6
#define PIN_LED_STRISCIA_1_PIANO_ESTREMA_SINISTRA 7
#define PIN_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA 8
#define PIN_LED_STRISCIA_3_PIANO 9

// TODO Controllare numero led
#define NUMERO_LED_STRISCIA_1_PIANO_SINISTRA 150
#define NUMERO_LED_STRISCIA_1_PIANO_DESTRA 150
#define NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_SINISTRA 150
#define NUMERO_LED_STRISCIA_1_PIANO_ESTREMA_DESTRA 150

// *** CLASSE *** //

class StrisceLed {
    // *** DICHIARAZIONE VARIABILI *** //

    StrisciaLed* strisciaLed1PianoSinistra;
    StrisciaLed* strisciaLed1PianoDestra;
    StrisciaLed* strisciaLed1PianoEstremaSinistra;
    StrisciaLed* strisciaLed1PianoEstremaDestra;
    StrisciaLed* strisciaLed3Piano;

    // *** DICHIARAZIONE METODI *** //

   public:
    void inizializza();
};

extern StrisceLed strisceLed;

#endif
