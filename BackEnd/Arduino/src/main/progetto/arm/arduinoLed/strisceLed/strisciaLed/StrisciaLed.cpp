// *** INCLUDE *** //

#include "StrisciaLed.h"

// *** DEFINIZIONE METODI *** //

int StrisciaLed::getLuminosita() { return this->luminosita; }

void StrisciaLed::setLuminosita(int luminosita) { this->luminosita = luminosita; }

void StrisciaLed::impostaColore(ColoreLed coloreLed) {
    // Imposto il colore scelto
    switch (coloreLed) {
        case ROSSO:
            fill_solid(ledArray, numeroLed, CRGB::Red);
            break;
        case VERDE:
            fill_solid(ledArray, numeroLed, CRGB::Green);
            break;
        case BLU:
            fill_solid(ledArray, numeroLed, CRGB::Blue);
            break;
        case BIANCO:
            fill_solid(ledArray, numeroLed, CRGB::White);
            break;
    }

    // Gestisco la dissolvenza
    for (int i = 0; i < LUMINOSITA_MASSIMA; i++) {
        FastLED.setBrightness(i);
        FastLED.show();
        delay(INTERVALLO_DISSOLVENZA);
    }
}