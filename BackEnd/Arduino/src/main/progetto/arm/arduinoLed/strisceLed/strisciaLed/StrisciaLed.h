#ifndef DCC_COMMAND_STATION_STRISCIALED_H
#define DCC_COMMAND_STATION_STRISCIALED_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "FastLED.h"

// *** DEFINE *** //

#define LUMINOSITA_MASSIMA 255
#define INTERVALLO_DISSOLVENZA 15

// *** ENUMERAZIONI *** //

enum ColoreLed { ROSSO, VERDE, BLU, BIANCO };

// *** DICHIARAZIONE FUNZIONI *** //

class StrisciaLed {
    // *** VARIABILI *** //

    CRGB *ledArray;
    int numeroLed;
    int luminosita;  // Da 0 a 255

    // *** COSTRUTTORE *** //

   public:
    StrisciaLed(int numeroLed, int luminosita) {
        ledArray = new CRGB[numeroLed];
        this->numeroLed = numeroLed;
        this->luminosita = luminosita;
    }

    // *** DICHIARAZIONE METODI *** //

   public:
    CRGB *getLedArray() { return ledArray; }

    int getLuminosita();
    void setLuminosita(int luminosita);

    void impostaColore(ColoreLed coloreLed);
};

#endif