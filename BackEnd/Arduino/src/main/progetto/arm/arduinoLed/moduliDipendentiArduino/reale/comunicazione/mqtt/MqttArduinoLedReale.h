#ifndef ARDUINO_MQTTARDUINOLEDREALE_H
#define ARDUINO_MQTTARDUINOLEDREALE_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class MqttArduinoLedReale {
    // *** VARIABILI *** //
   public:
    MqttCoreArduinoArmAbstract &mqttCore;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

    MqttArduinoLedReale(MqttCoreArduinoArmAbstract &mqttCore, LoggerAbstract& logger) : mqttCore(mqttCore), logger(logger) {}

    // *** METODI *** //

    void inviaMessaggioLog(char* payload);
};

// *** DICHIARAZIONE VARIABILI *** //

extern MqttArduinoLedReale mqttArduinoLedReale;

#endif
