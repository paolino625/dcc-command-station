#ifndef ARDUINO_PROGRAMMAREALE_H
#define ARDUINO_PROGRAMMAREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"

// *** CLASSE *** //

class ProgrammaReale : public ProgrammaAbstract {
    // *** DICHIARAZIONE METODI *** //
   public:
    [[noreturn]] void ferma() override;
};

#endif

// *** DICHIARAZIONE VARIABILI *** //

extern ProgrammaReale programmaReale;
