// *** INCLUDE *** //

#include "MqttArduinoLedReale.h"

#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/comunicazione/mqtt/topic/TopicMqtt.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/reale/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

void MqttArduinoLedReale::inviaMessaggioLog(char* payload) {
    // Imposto loggerAttivo a false, altrimenti si creerebbe una dipendenza circolare.
    // Voglio scrivere un log "ARDUINO MASTER", entro in questa funzione ma poi mi accorgo che devo prima pubblicare un
    // messaggio di log, quindi viene richiamata questa funzione e così via, fino all'infinito.
    mqttCore.inviaMessaggio(TopicMqtt::LOG, payload, false);
}

// *** DEFINIZIONE VARIABILI *** //

MqttArduinoLedReale mqttArduinoLedReale(mqttCoreArduinoArmReale, loggerReale);