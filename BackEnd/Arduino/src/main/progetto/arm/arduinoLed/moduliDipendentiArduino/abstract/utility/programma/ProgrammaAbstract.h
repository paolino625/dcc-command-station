#ifndef ARDUINO_PROGRAMMAABSTRACT_H
#define ARDUINO_PROGRAMMAABSTRACT_H

// *** CLASSE *** //

class ProgrammaAbstract {
   public:
    virtual ~ProgrammaAbstract() {}
    virtual void ferma() = 0;
};

#endif
