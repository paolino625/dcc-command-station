#ifndef ARDUINO_INIZIALIZZAZIONEPORTASERIALE_H
#define ARDUINO_INIZIALIZZAZIONEPORTASERIALE_H

// *** DEFINE *** //

#define NUMERO_TENTATIVI_INIZIALIZZAZIONE_PORTA_SERIALE 1

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DICHIARAZIONE FUNZIONI *** //

boolean inizializzazionePortaSeriale(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                     bool portaSerialeDaVerificare);

#endif
