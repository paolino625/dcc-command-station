#ifndef ARDUINO_BUZZERREALEARDUINOLED_H
#define ARDUINO_BUZZERREALEARDUINOLED_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"

// *** DICHIARAZIONE VARIABILI *** //

extern BuzzerAbstractReale buzzerRealeArduinoLed;

#endif
