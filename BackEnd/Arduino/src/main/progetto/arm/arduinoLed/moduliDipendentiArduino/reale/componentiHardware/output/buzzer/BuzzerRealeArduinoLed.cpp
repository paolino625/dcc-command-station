// *** INCLUDE *** //

#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/AnalogPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AnalogPinReale pinBuzzerRealeArduinoLed(PIN_BUZZER_ARDUINO_LED);
BuzzerAbstractReale buzzerRealeArduinoLed(pinBuzzerRealeArduinoLed, delayReale, loggerReale);