// *** INCLUDE *** //

#include "InizializzazionePortaSeriale.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/PorteSeriali.h"

// *** DEFINIZIONE FUNZIONI *** //

boolean inizializzazionePortaSeriale(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                     bool portaSerialeDaVerificare) {
    for (int i = 0; i < NUMERO_TENTATIVI_INIZIALIZZAZIONE_PORTA_SERIALE; i++) {
        bool portaSerialeInizializzata =
            inizializzoPortaSerialeArduinoGigaArduinoMega(numeroPortaSeriale, velocitaPorta, portaSerialeDaVerificare);
        if (portaSerialeInizializzata) {
            return true;
        }
    }

    return false;
}