#ifndef ARDUINO_CLIENTETHERNET_H
#define ARDUINO_CLIENTETHERNET_H

// *** INCLUDE *** //

#include "EthernetClient.h"

// *** DICHIARAZIONE VARIABILI *** //

extern EthernetClient ethernetClientWebServer;
extern EthernetClient ethernetClientMqtt;

#endif
