// *** INCLUDE *** //

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"

// *** VARIABILI *** //

#ifdef PROD

// *** DEBUGGING *** //

#define DEBUGGING_MQTT 0

#define DEBUGGING_CHECK_CONNESSIONE_MQTT 0

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

// *** MQTT *** //

#define MQTT_ATTIVO 1

#endif