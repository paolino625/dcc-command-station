// *** INCLUDE *** //

#include <Arduino.h>

#include "FastLED.h"
#include "main/progetto/arm/arduinoLed/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/comunicazione/mqtt/MqttArduinoLedReale.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoLed/strisceLed/StrisceLed.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/ethernet/core/EthernetCore.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/PorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINE *** //

#define ATTESA_INIZIALIZZAZIONE_PORTA_SERIALE 500

// *** SETUP *** //

#define NUM_LEDS_ON 210

void setup() {
    bool successo = inizializzoPortaSerialeArduinoMinima(VELOCITA_PORTA_SERIALE_COMPUTER);
    if (!successo) {
        programmaReale.ferma();
    }

    // Devo aspettare affinché la porta seriale sia pronta, altrimenti non vengono stampati i primi messaggi
    delay(ATTESA_INIZIALIZZAZIONE_PORTA_SERIALE);

    // Inizializzo porta seriale computer
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Porta Seriale Computer inizializzata.",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "ARDUINO LED",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    ethernetCore.inizializza(true);

    // Mqtt è una delle prime cose che attivo, in questa maniera posso iniziare a inviare i log al broker immediatamente
    if (MQTT_ATTIVO) {
        mqttArduinoLedReale.mqttCore.inizializza();
    }

    strisceLed.inizializza();
}

// *** LOOP *** //

void loop() {
    if (MQTT_ATTIVO) {
        mqttArduinoLedReale.mqttCore.checkConnessione();
        mqttArduinoLedReale.mqttCore.inviaKeepAlive();
    }

    ethernetCore.mantieneConnessione();

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Ciao", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                          true);
    delay(1000);
}