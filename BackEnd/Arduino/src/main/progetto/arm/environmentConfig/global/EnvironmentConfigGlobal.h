// *** VARIABILI GLOBALI *** //

#ifdef ARDUINO_MASTER
#include "main/progetto/arm/arduinoMaster/environmentConfig/global/EnvironmentConfigGlobal.h"
#endif

#ifdef ARDUINO_LED
#include "main/progetto/arm/arduinoLed/environmentConfig/global/EnvironmentConfigGlobal.h"
#endif

#define NOME_TOPIC_MQTT_LOCOMOTIVA "DccCommandStation/Locomotiva"
#define NOME_TOPIC_MQTT_CONVOGLIO "DccCommandStation/Convoglio"
#define NOME_TOPIC_MQTT_SCAMBIO "DccCommandStation/Scambio"
#define NOME_TOPIC_MQTT_SEZIONE "DccCommandStation/Sezione"
#define NOME_TOPIC_MQTT_SENSORE_POSIZIONE "DccCommandStation/SensorePosizione"
#define NOME_TOPIC_MQTT_LOG "DccCommandStation/Log"
#define NOME_TOPIC_MQTT_INFO_AUTOPILOT "DccCommandStation/InfoAutopilot"
#define NOME_TOPIC_MQTT_ARDUINO_READY "DccCommandStation/ArduinoReady"