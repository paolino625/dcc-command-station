#ifndef BACKEND_MQTTCORE_H
#define BACKEND_MQTTCORE_H

// *** INCLUDE *** //

#include "MqttClient.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/abstract/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmAbstract.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/reale/comunicazione/mqtt/mqttCore/topic/TopicMqtt.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"

// *** DEFINE *** //

#define INTERVALLO_CHECK_CONNESSIONE_MQTT 5000

#define NUMERO_MASSIMO_TENTATIVI_RICONNESSIONE_MQTT 12
#define INTERVALLO_TENTATIVO_RICONNESSIONE_MQTT_MILLISECONDI 5000

// *** DICHIARAZIONE VARIABILI *** //

extern unsigned long timestampUltimoCheckConnessioneMqtt;

// *** CLASSE *** //

class MqttCoreArduinoArmReale : public MqttCoreArduinoArmAbstract {
    // *** DICHIARAZIONE VARIABILI *** //

    // Non posso usare reale perché può essere sia il buzzer di Arduino che quello di Arduino LED.
    BuzzerAbstractReale& buzzer;

    MqttClient* mqttClient;

    bool mqttInizializzato;

    // *** COSTRUTTORE *** //

   public:
    MqttCoreArduinoArmReale(BuzzerAbstractReale& buzzer) : buzzer{buzzer} {}

    // *** DICHIARAZIONE FUNZIONI *** //

   public:
    void inizializza() override;
    void inviaKeepAlive() override;
    void inviaMessaggio(TopicMqtt topicMqtt, char* payload, bool loggerAttivo) override;
    void checkConnessione() override;
    void connetteBroker(bool fermoProgrammaInCasoDiFallimento) override;
    bool isMqttInizializzato() const override;

   private:
    const char* calcoloNomeTopic(TopicMqtt topicMqtt, bool loggerAttivo) override;
};

extern MqttCoreArduinoArmReale mqttCoreArduinoArmReale;

#endif
