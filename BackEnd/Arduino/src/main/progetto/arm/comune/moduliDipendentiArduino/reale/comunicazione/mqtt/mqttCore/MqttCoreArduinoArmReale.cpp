// *** INCLUDE *** //

#include "MqttCoreArduinoArmReale.h"

#include "EthernetClient.h"
#include "main/progetto/arm/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/arm/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

#ifdef ARDUINO_MASTER
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/connessioneInternet/connessioneEthernet/client/ClientEthernet.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"

#endif

#ifdef ARDUINO_LED
#include "main/progetto/arm/arduinoLed/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoLed.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/comunicazione/connessioneInternet/connessioneEthernet/client/ClientEthernet.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#endif

/*
0: Al massimo una volta
1: Almeno una volta
2: Esattamente una volta
*/
#define QOS_MQTT 2

// *** DEFINIZIONE VARIABILI *** //

const char broker[] = INDIRIZZO_MQTT_BROKER;

unsigned long timestampUltimoCheckConnessioneMqtt;

// *** DEFINIZIONE METODI *** //

void MqttCoreArduinoArmReale::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo MQTT...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    loggerReale.cambiaTabulazione(1);

    mqttClient = new MqttClient(ethernetClientMqtt);

    connetteBroker(true);
    mqttInizializzato = true;
    loggerReale.cambiaTabulazione(-1);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo MQTT... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MqttCoreArduinoArmReale::connetteBroker(bool fermoProgrammaInCasoDiFallimento) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Connessione al broker MQTT ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, broker, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    // Mi autentico
    mqttClient->setUsernamePassword(MQTT_BROKER_USERNAME, MQTT_BROKER_PASSWORD);

    // Tento la connessione al broker
    if (!mqttClient->connect(broker, PORTA_MQTT_BROKER)) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Connessione broker MQTT fallita! Codice errore = ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, std::to_string(mqttClient->connectError()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
#ifdef ARDUINO_MASTER
        if (fermoProgrammaInCasoDiFallimento) {
            displayReale.errori.mqtt.connessioneNonRiuscita();
            programmaReale.ferma(false);
        }
#endif

#ifdef ARDUINO_UNO_R4_MINIMA
        if (fermoProgrammaInCasoDiFallimento) {
            programmaReale.ferma();
        }
#endif
    }

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Connessione al broker MQTT ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, broker, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... OK",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
}

bool MqttCoreArduinoArmReale::isMqttInizializzato() const { return mqttInizializzato; }

void MqttCoreArduinoArmReale::inviaKeepAlive() {
    // È necessario chiamare poll() in maniera regolare per mandare MQTT keep alives che evitano di essere disconnessi
    // dal broker
    mqttClient->poll();
}

void MqttCoreArduinoArmReale::inviaMessaggio(TopicMqtt topicMqtt, char* payload, bool loggerAttivo) {
    if (MQTT_ATTIVO) {
        const char* nomeTopic = calcoloNomeTopic(topicMqtt, false);

        if (loggerAttivo) {
            if (DEBUGGING_MQTT) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio messaggio MQTT a topic ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, nomeTopic,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "... ",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                loggerReale.cambiaTabulazione(1);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                      "Payload: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, payload,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                loggerReale.cambiaTabulazione(-1);
            }
        }

        // Pubblico il messaggio al topic specifico
        mqttClient->beginMessage(nomeTopic, false, QOS_MQTT, false);
        mqttClient->print(payload);
        mqttClient->endMessage();

        if (loggerAttivo) {
            if (DEBUGGING_MQTT) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio messaggio MQTT a topic ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, nomeTopic,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "... OK",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        }
    }
}

const char* MqttCoreArduinoArmReale::calcoloNomeTopic(TopicMqtt topicMqtt, bool loggerAttivo) {
    switch (topicMqtt) {
        case TopicMqtt::LOCOMOTIVA: {
            return NOME_TOPIC_MQTT_LOCOMOTIVA;
        }
        case TopicMqtt::CONVOGLIO: {
            return NOME_TOPIC_MQTT_CONVOGLIO;
        }
        case TopicMqtt::SCAMBIO: {
            return NOME_TOPIC_MQTT_SCAMBIO;
        }
        case TopicMqtt::SEZIONE: {
            return NOME_TOPIC_MQTT_SEZIONE;
        }
        case TopicMqtt::SENSORE_POSIZIONE: {
            return NOME_TOPIC_MQTT_SENSORE_POSIZIONE;
        }
        case TopicMqtt::LOG: {
            return NOME_TOPIC_MQTT_LOG;
        }
        case TopicMqtt::INFO_AUTOPILOT: {
            return NOME_TOPIC_MQTT_INFO_AUTOPILOT;
        }

        case TopicMqtt::ARDUINO_READY: {
            return NOME_TOPIC_MQTT_ARDUINO_READY;
        }

        default: {
            if (loggerAttivo) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Enumerazione non gestita.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
#ifdef ARDUINO_MASTER
            programmaReale.ferma(true);
#endif

#ifdef ARDUINO_UNO_R4_MINIMA
            programmaReale.ferma();
#endif
        }
    }
}

void MqttCoreArduinoArmReale::checkConnessione() {
    unsigned timestampAttuale = millis();
    if (timestampAttuale - timestampUltimoCheckConnessioneMqtt >= INTERVALLO_CHECK_CONNESSIONE_MQTT) {
        if (DEBUGGING_CHECK_CONNESSIONE_MQTT) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Check connessione MQTT...",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            loggerReale.cambiaTabulazione(1);
        }

        timestampUltimoCheckConnessioneMqtt = timestampAttuale;
        if (!mqttClient->connected()) {
            buzzer.beepAvviso();
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Connessione persa con il broker MQTT.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

            bool riconnessioneMqttRiuscita = false;
            for (int i = 0; i < NUMERO_MASSIMO_TENTATIVI_RICONNESSIONE_MQTT; i++) {
                delayReale.milliseconds(INTERVALLO_TENTATIVO_RICONNESSIONE_MQTT_MILLISECONDI);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING,
                                      "Tento riconnessione al broker MQTT tentativo n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, std::to_string(i + 1).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "...",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                loggerReale.cambiaTabulazione(1);

                connetteBroker(false);

                loggerReale.cambiaTabulazione(-1);

                if (mqttClient->connected()) {
                    buzzer.beepOk();
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING,
                                          "Tento riconnessione al broker MQTT tentativo n° ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, std::to_string(i + 1).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "... OK",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                    riconnessioneMqttRiuscita = true;
                    // Se sono riuscito a connettermi allora esco
                    break;
                } else {
                    buzzer.beepAvviso();
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING,
                                          "Tento riconnessione al broker MQTT tentativo n° ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, std::to_string(i + 1).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "... KO",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                    riconnessioneMqttRiuscita = false;
                }
            }

            // Se dopo tutti questi tentativi non sono riuscito a riconnettermi, allora fermo il programma
            if (!riconnessioneMqttRiuscita) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Connessione con broker MQTT persa.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

#ifdef ARDUINO_MASTER
                displayReale.errori.mqtt.connessionePersa();
                programmaReale.ferma(false);
#elif ARDUINO_LED
                programmaReale.ferma();
#endif
            }
        }

        if (DEBUGGING_CHECK_CONNESSIONE_MQTT) {
            loggerReale.cambiaTabulazione(-1);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Check connessione MQTT... OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
}

#ifdef ARDUINO_MASTER
MqttCoreArduinoArmReale mqttCoreArduinoArmReale(buzzerRealeArduinoMaster);
#elif ARDUINO_LED
MqttCoreArduinoArmReale mqttCoreArduinoArmReale(buzzerRealeArduinoLed);
#endif