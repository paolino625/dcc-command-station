#ifndef MQTT_CORE_ARDUINO_ARM_ABSTRACT
#define MQTT_CORE_ARDUINO_ARM_ABSTRACT

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliDipendentiArduino/reale/comunicazione/mqtt/mqttCore/topic/TopicMqtt.h"

// *** CLASSE *** //

class MqttCoreArduinoArmAbstract{
   public:
    virtual void inizializza() = 0;
    virtual void inviaKeepAlive() = 0;
    virtual void inviaMessaggio(TopicMqtt topicMqtt, char* payload, bool loggerAttivo) = 0;
    virtual void checkConnessione() = 0;
    virtual void connetteBroker(bool fermoProgrammaInCasoDiFallimento) = 0;
    virtual bool isMqttInizializzato() const = 0;

   private:
    virtual const char* calcoloNomeTopic(TopicMqtt topicMqtt, bool loggerAttivo) = 0;
};

#endif