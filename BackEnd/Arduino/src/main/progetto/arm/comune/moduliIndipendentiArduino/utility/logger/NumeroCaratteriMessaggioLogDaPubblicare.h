#ifndef ARDUINO_NUMEROCARATTERIMESSAGGIOLOGDAPUBBLICARE_H
#define ARDUINO_NUMEROCARATTERIMESSAGGIOLOGDAPUBBLICARE_H

// *** DEFINE *** //

// 10000 è anche il limite del payload MQTT impostato sulla libreria MqttArduino di cui è stato effettuato il fork.
#define NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE 10000

#endif