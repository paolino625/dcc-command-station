#ifndef DCC_COMMAND_STATION_TIPIPRIMITIVIARDUINO_H
#define DCC_COMMAND_STATION_TIPIPRIMITIVIARDUINO_H

// *** INCLUDE *** //

#include <cstdint>

// Serve per non importare Arduino.h in classi che hanno bisogno del simbolo byte ma non di chiamate Arduino.
// Utile in modo tale che tali classi senza dipendenza da libreria Arduino possano essere usate nei test.

// *** TYPEDEF *** //

typedef uint8_t byte;
typedef bool boolean;

#endif