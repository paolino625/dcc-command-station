#ifndef DCC_COMMAND_STATION_USCITAMENUREALE_H
#define DCC_COMMAND_STATION_USCITAMENUREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/menuDisplayKeypad/uscitaMenu/UscitaMenuAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern UscitaMenuAbstract uscitaMenuReale;

#endif
