#ifndef BACKEND_CLIENTETHERNET_H
#define BACKEND_CLIENTETHERNET_H

// *** INCLUDE *** //

#include "EthernetClient.h"

// *** DICHIARAZIONE VARIABILI *** //

extern EthernetClient ethernetClientWebServer;
extern EthernetClient ethernetClientMqtt;
extern EthernetClient ethernetClientRestClient;

#endif