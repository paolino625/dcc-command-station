// *** INCLUDE *** //

#include "ControlloCortoCircuitoTracciatoAbstract.h"

// *** DEFINIZIONE METODI *** //

/*
Potrebbe succedere che un vagone deragli o per qualsiasi motivo ci sia un contatto tra le due
rotaie. In questo caso l'alimentatore non interviene e continua a funzione: peraltro il
cortocircuito assorbe tutta la corrente a disposizione, ovvero 10 A. Questo può essere un grave
problema di sicurezza, specie quando il plastico è in funzione senza diretta supervisione.
Sfruttiamo dunque il sensore di corrente per verificare l'assorbimento del tracciato, nel caso in
cui sia superiore di una certa soglia da noi scelta, ipotizziamo che si stia verificando un
cortocircuito e spegniamo la motor shield.
*/

void ControlloCortoCircuitoTracciatoAbstract::effettuaControllo(
    MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypad) {
    // Leggo sensore corrente INA219 senza campionamento altrimenti sono troppo lento a rilevare il
    // corto circuito. Bisogna intervenire subito. Arduino Giga potrebbe metterci un po' a
    // intervenire alcune volte: questo probabilmente a causa della lettura del sensore INA219
    // tramite I2C.
    if (sensoreCorrenteIna219.leggeCorrente() > SOGLIA_MILLIAMPERE_CORTOCIRCUITO && motorShield.isAccesa()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "CORTO CIRCUITO RILEVATO!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        motorShield.spegne();
        buzzer.beepAvviso();

        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Attendo...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        delay.milliseconds(INTERVALLO_MILLISECONDI_TENTATIVO_DOPO_CORTO_CIRCUITO);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Attendo... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        // Riaccendo la Motor Shield
        motorShield.accende();
        delay.milliseconds(TEMPO_ACCENSIONE_MOTOR_SHIELD_MILLISECONDI);

        // Se il corto circuito è ancora presente allora richiedo intervento manuale
        if (sensoreCorrenteIna219.leggeCorrente() > SOGLIA_MILLIAMPERE_CORTOCIRCUITO) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING,
                                  "Corto circuito ancora presente! Richiesto intervento manuale!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            motorShield.spegne();
            // Fermo l'autopilot se attivo
            autopilot.ferma(false, false);
            // Fermo tutte le locomotive, altrimenti al ritorno della corrente tornano a girare
            stopEmergenza.aziona(true);
            display.avvisi.cortoCircuito();

            buzzer.beepErrore();
            int carattereLetto = keypad.leggoSceltaMenuUtente();
            while (carattereLetto != 1) {
                carattereLetto = keypad.leggoSceltaMenuUtente();
            }
            buzzer.beepOk();
            motorShield.accende();
            display.homepage.displayDaAggiornare = true;

        } else {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Corto circuito non più presente. Ripristino corrente.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            buzzer.beepOk();
        }
    }
}