// *** INCLUDE *** //

#include "AutopilotConvoglioAbstract.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/ModalitaAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/utility/ConversioneVelocita.h"

// *** DEFINIZIONE METODI *** //

bool AutopilotConvoglioAbstract::setPosizioneConvoglio(IdSezioneTipo idSezioneCorrenteOccupata,
                                                       bool direzionePrimaLocomotivaDestraPuntoDiVistaUtente) {
    char *idSezioneCorrenteOccupataStringa = idSezioneCorrenteOccupata.toStringShort();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    logger.cambiaTabulazione(1);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Imposto la posizione del convoglio...",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

    logger.cambiaTabulazione(1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Info:", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                          false);
    logger.cambiaTabulazione(1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          "Id Sezione Corrente Occupata: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                          false);
    LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneCorrenteOccupataStringa,
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          "Direzione Prima Locomotiva Destra: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                          false);
    std::string direzione = direzionePrimaLocomotivaDestraPuntoDiVistaUtente ? "true" : "false";
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, direzione.c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(-1);

    // *** CONTROLLI *** //

    // Controllo che l'autopilot non sia già stato attivato
    if (statoInizializzazioneAutopilotConvoglio !=
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "L'autopilot è già stato attivato. "
                              "Non posso modificare le informazioni "
                              "del convoglio.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // Controllo che la sezione esista
    if (!sezioni.esisteSezione(idSezioneCorrenteOccupata)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La sezione inserita non esiste.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // Controllo che la sezione sia di stazione
    if (!idSezioneCorrenteOccupata.isBinarioStazione()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "È necessario che la sezione iniziale "
                              "di un convoglio sia di "
                              "stazione.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // Controllo che la sezione sia libera
    SezioneAbstract *sezione = sezioni.getSezione(idSezioneCorrenteOccupata);
    if (sezione->isLocked()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La sezione iniziale del convoglio è già bloccata.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // *** FINE CONTROLLI *** //

    // Blocco la sezione
    sezioni.lock(sezione->getId(), convoglio.getId());

    this->posizioneConvoglio.idSezioneCorrenteOccupata = idSezioneCorrenteOccupata;

    /*
    N.B. La variabile direzionePrimaLocomotivaDestra contiene la direzione della prima locomotiva del convoglio secondo
    il punto di vista dell'utente, non in senso assoluto. Esempio: se il convoglio è stato inserito su uno dei binari
    della stazione EST e la variabile direzionePrimaLocomotivaDestra è a true, in realtà il valore corretto sarà false
    in quanto per le direzioni si prende sempre la stazione centrale come punto di riferimento. Per sapere la direzione
    corretta, devo capire a quale stazione appartiene la sezione iniziale del convoglio, per poi invertire la direzione
    nel caso in cui fosse necessario.
    */
    bool direzionePrimaLocomotivaDestraPuntoDiVistaStazioneCentrale;
    StazioneAbstract *stazione = stazioni.getStazioneFromSezione(sezione->getId());
    if (stazione->isStessaProspettivaStazioneCentrale()) {
        LOG_MESSAGGIO_STATICO(
            logger, LivelloLog::DEBUG_,
            "La stazione inserita è nella stessa prospettiva della stazione centrale. Non inverto la direzione.",
            InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        // In questo caso non c'è da invertire la direzione
        direzionePrimaLocomotivaDestraPuntoDiVistaStazioneCentrale = direzionePrimaLocomotivaDestraPuntoDiVistaUtente;
    } else {
        LOG_MESSAGGIO_STATICO(
            logger, LivelloLog::DEBUG_,
            "La stazione inserita non è nella stessa prospettiva della stazione centrale. Inverto la direzione.",
            InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        // In questo caso c'è da invertire la direzione
        direzionePrimaLocomotivaDestraPuntoDiVistaStazioneCentrale = !direzionePrimaLocomotivaDestraPuntoDiVistaUtente;
    }
    this->posizioneConvoglio.direzionePrimaLocomotivaDestra =
        direzionePrimaLocomotivaDestraPuntoDiVistaStazioneCentrale;

    this->posizioneConvoglio.validita = ValiditaPosizioneConvoglio::POSIZIONE_VALIDA;

    this->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;

    logger.cambiaTabulazione(-1);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Imposto la posizione del convoglio... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    logger.cambiaTabulazione(-1);

    return true;
}

bool AutopilotConvoglioAbstract::inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(
    IdSezioneTipo idBinarioStazione, int delayAttesaPartenzaStazioneLocale) {
    setStatoAutopilotModalitaStazioneSuccessiva(
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA);

    char *idBinarioStazioneStringa = idBinarioStazione.toStringShort();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    logger.cambiaTabulazione(1);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo Autopilot Modalità stazione successiva... ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

    logger.cambiaTabulazione(1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          "Id binario stazione successiva:", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                          false);
    LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idBinarioStazioneStringa,
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true, true);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Delay Attesa Partenza Stazione Locale: ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(delayAttesaPartenzaStazioneLocale).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    // *** CONTROLLI *** //

    // Controllo che l'autopilot sia stato inizializzato
    if (statoInizializzazioneAutopilotConvoglio !=
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Stato Inizializzazione Autopilot non è PRONTO_PER_CONFIGURAZIONE_AUTOPILOT. È "
                              "necessario chiamare prima la funzione setPosizioneConvoglio()",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    // Controllo che la sezione inserita esista
    if (!sezioni.esisteSezione(idBinarioStazione)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La sezione inserita non esiste.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // Controllo che la sezione inserita sia di una stazione
    if (!idBinarioStazione.isBinarioStazione()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "La sezione non "
                              "appartiene a nessuna stazione!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    SezioneAbstract *sezione = sezioni.getSezione(idBinarioStazione);

    // Controllo che la sezione sia libera
    if (sezione->isLocked()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La sezione iniziale del convoglio è già bloccata.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // Controllo che esista un percorso dalla sezione di partenza del convoglio alla stazione impostata
    if (!percorsi.esistePercorso(posizioneConvoglio.idSezioneCorrenteOccupata, idBinarioStazione)) {
        char *idSezioneCorrenteOccupataStringa = posizioneConvoglio.idSezioneCorrenteOccupata.toStringShort();

        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Percorso non valido!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Sezione stazione partenza: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::BUG, idSezioneCorrenteOccupataStringa,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Sezione stazione arrivo: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::BUG, idBinarioStazione.toStringShort(),
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
        logger.cambiaTabulazione(-2);

        return false;
    }

    // Controllo che la stazione successiva impostata possa ospitare il convoglio.
    if (!isSezioneLungaASufficienzaPerConvoglio(sezione)) {
        // Se la sezione non è lunga a sufficienza per ospitare il convoglio, allora errore
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Il binario di stazione successiva "
                              "impostato non è lungo a sufficienza per ospitare il "
                              "convoglio.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Lunghezza binario stazione successiva: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(sezione->getLunghezzaCm()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Lunghezza convoglio: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(convoglio.getLunghezzaCm()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Distanza cm stop locomotiva da fine sezione: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              std::to_string(DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        logger.cambiaTabulazione(-2);
        return false;
    }

    // *** FINE CONTROLLI *** //

    sezioni.lock(sezione->getId(), convoglio.getId());

    // Controllo se percorsoConvoglioBinariStazioni è già stato allocato in precedenza e in caso lo dealloco
    if (percorsoConvoglioBinariStazioni != nullptr) {
        delete[] percorsoConvoglioBinariStazioni;
    }

    percorsoConvoglioBinariStazioni = new IdSezioneTipo[2];
    // Come primo binario di stazione metto il binario di partenza del convoglio, come secondo il binario della
    // stazione di destinazione inserito dall'utente
    percorsoConvoglioBinariStazioni[0] = posizioneConvoglio.idSezioneCorrenteOccupata;
    percorsoConvoglioBinariStazioni[1] = idBinarioStazione;

    // Imposto il delayReale tra le stazioni del convoglio
    delayAttesaPartenzaStazione = delayAttesaPartenzaStazioneLocale;

    // Inizializzo timer, utile per FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA
    millisInizioAttesaPartenzaStazione = tempo.milliseconds();

    // La configurazione è completata, ora cambio lo stato autopilot in modo
    // tale che Arduino possa gestire in autonomia.
    cambioStatoInizializzazioneAutopilotConvoglio(
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA);

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo Autopilot Modalità stazione successiva... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(-1);

    return true;
}

void AutopilotConvoglioAbstract::inizializzoAutopilotModalitaCasuale(
    bool ottimizzazioneAssegnazioneConvogliAiBinariModalitaAutopilotCasualeLocale) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo Autopilot Modalità Casuale...",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

    percorsoConvoglioBinariStazioni = new IdSezioneTipo[2];
    percorsoConvoglioBinariStazioni[0] = posizioneConvoglio.idSezioneCorrenteOccupata;

    ottimizzazioneAssegnazioneConvogliAiBinariModalitaAutopilotCasuale =
        ottimizzazioneAssegnazioneConvogliAiBinariModalitaAutopilotCasualeLocale;

    // La configurazione è completata, ora cambio lo stato autopilot in modo
    // tale che Arduino possa gestire in autonomia.
    cambioStatoInizializzazioneAutopilotConvoglio(
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo Autopilot Modalità Casuale... OK",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
}

void AutopilotConvoglioAbstract::processo(bool autopilotInSpegnimento) {
    switch (statoInizializzazioneAutopilotConvoglio) {
        case StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO: {
            attesaInizializzazioneConvoglio();
        }

        break;

        case StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT: {
            prontoPerConfigurazioneAutopilot();
        }

        break;

        case StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA: {
            processoModalitaAutopilotSelezionata(autopilotInSpegnimento);
        }

        break;
    }
}

void AutopilotConvoglioAbstract::processoModalitaAutopilotSelezionata(bool autopilotInSpegnimento) {
    // In base alla modalità autopilot scelta dall'utente.
    switch (modalitaAutopilot) {
        case ModalitaAutopilot::STAZIONE_SUCCESSIVA: {
            switch (statoAutopilotModalitaStazioneSuccessiva) {
                case StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA: {
                    bool momentoPartenzaArrivato = fermoStazioneAttesaMomentoAttesaPartenza();
                    if (momentoPartenzaArrivato) {
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                              "Stato: FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA. ",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                              "Tempo di attesa partenza stazione terminato.",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                        cambioStatoAutopilotModalitaStazioneSuccessiva(
                            StatoAutopilotModalitaStazioneSuccessiva::
                                FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA);
                    }
                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::
                    FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA: {
                    fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva();
                    cambioStatoAutopilotModalitaStazioneSuccessiva(
                        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO);

                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO: {
                    fermoStazioneCalcoloDirezioneConvoglio();
                    cambioStatoAutopilotModalitaStazioneSuccessiva(
                        StatoAutopilotModalitaStazioneSuccessiva::
                            FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE);
                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::
                    FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE: {
                    bool lockRiuscito = fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente();
                    if (lockRiuscito) {
                        // Invalido la posizione del convoglio da quando l'autopilot fa partire il convoglio a quanto
                        // arriva alla stazione successiva: in questo modo se dovesse esserci un problema per cui
                        // Arduino dovesse spegnersi nel frattempo, al suo riavvio la posizione dei convogli che erano
                        // in movimento risulta invalidata e viene richiesto un intervento manuale.
                        convoglio.invalidaPosizioneConvoglio(false);

                        // Se sono riuscito a prendere i lock sia delle sezioni che
                        // degli scambi, allora posso passare alla fase successiva.
                        cambioStatoAutopilotModalitaStazioneSuccessiva(
                            StatoAutopilotModalitaStazioneSuccessiva::IN_PARTENZA_STAZIONE);
                    }
                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::IN_PARTENZA_STAZIONE: {
                    inPartenzaStazione();
                    cambioStatoAutopilotModalitaStazioneSuccessiva(
                        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE);

                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE: {
                    bool primoSensoreAzionato = inMovimentoInAttesaPrimoSensore();
                    if (primoSensoreAzionato) {
                        eseguoOperazioniPrimoSensoreAzionato();

                        cambioStatoAutopilotModalitaStazioneSuccessiva(
                            StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO);
                    }
                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO: {
                    RisultatoInMovimento risultatoInMovimento = inMovimento();
                    switch (risultatoInMovimento) {
                        case CONVOGLIO_FERMATO_BINARIO_STAZIONE: {
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                                  "Stato: CONVOGLIO_FERMATO_BINARIO_STAZIONE",
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

                            cambioStatoAutopilotModalitaStazioneSuccessiva(
                                StatoAutopilotModalitaStazioneSuccessiva::ARRIVATO_STAZIONE_DESTINAZIONE);

                            break;
                        }

                        case CONVOGLIO_FERMATO_SEZIONE_INTERMEDIA: {
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                                  "Stato: CONVOGLIO_FERMATO_SEZIONE_INTERMEDIA",
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

                            // Se l'ultima sezione bloccata è una sezione intermedia che non sia l'ultima allora il
                            // convoglio si è fermato prima della stazione di destinazione. Passiamo al relativo
                            // stato.
                            cambioStatoAutopilotModalitaStazioneSuccessiva(
                                StatoAutopilotModalitaStazioneSuccessiva::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI);
                            break;
                        }

                        case CONVOGLIO_ANCORA_IN_MOVIMENTO: {
                            // Non devo fare nulla
                            break;
                        }
                    }
                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI: {
                    bool lockPreso = fermoSezioneAttesaLockSezioni();
                    if (lockPreso) {
                        cambioStatoAutopilotModalitaStazioneSuccessiva(
                            StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO);
                    }
                    break;
                }

                case StatoAutopilotModalitaStazioneSuccessiva::ARRIVATO_STAZIONE_DESTINAZIONE: {
                    cambioStatoInizializzazioneAutopilotConvoglio(
                        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT);

                    arrivatoStazioneDestinazione();

                    // Resetto lo stato per eventuali iterazioni successive
                    cambioStatoAutopilotModalitaStazioneSuccessiva(
                        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA);

                    break;
                }
            }

            break;
        }

        case ModalitaAutopilot::APPROCCIO_CASUALE_SCELTA_CONVOGLI:
            // È voluto non inserire il break, in modo tale che per entrambi i casi casuali, il codice eseguito è lo
            // stesso.
        case ModalitaAutopilot::APPROCCIO_CASUALE_TUTTI_CONVOGLI:
            switch (statoAutopilotModalitaApproccioCasuale) {
                case StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA: {
                    bool momentoPartenzaArrivato = fermoStazioneAttesaMomentoAttesaPartenza();
                    if (momentoPartenzaArrivato) {
                        cambioStatoAutopilotModalitaApproccioCasuale(
                            StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK);
                    }
                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK: {
                    scelgoBinarioStazioneSuccessivaELock();

                    cambioStatoAutopilotModalitaApproccioCasuale(
                        StatoAutopilotModalitaApproccioCasuale::
                            FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA);

                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::
                    FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA: {
                    fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva();
                    cambioStatoAutopilotModalitaApproccioCasuale(
                        StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO);

                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO: {
                    fermoStazioneCalcoloDirezioneConvoglio();
                    cambioStatoAutopilotModalitaApproccioCasuale(
                        StatoAutopilotModalitaApproccioCasuale::
                            FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE);

                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::
                    FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE: {
                    bool lockRiuscito = fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente();
                    if (lockRiuscito) {
                        // Invalido la posizione del convoglio da quando l'autopilot fa partire il convoglio a quanto
                        // arriva alla stazione successiva: in questo modo se dovesse esserci un problema per cui
                        // Arduino dovesse spegnersi nel frattempo, al suo riavvio la posizione dei convogli che erano
                        // in movimento risulta invalidata e viene richiesto un intervento manuale.
                        convoglio.invalidaPosizioneConvoglio(false);

                        // Se sono riuscito a prendere i lock sia delle sezioni che
                        // degli scambi, allora posso passare alla fase successiva.
                        cambioStatoAutopilotModalitaApproccioCasuale(
                            StatoAutopilotModalitaApproccioCasuale::IN_PARTENZA_STAZIONE);
                    }
                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::IN_PARTENZA_STAZIONE: {
                    inPartenzaStazione();
                    cambioStatoAutopilotModalitaApproccioCasuale(
                        StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE);

                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE: {
                    bool primoSensoreAzionato = inMovimentoInAttesaPrimoSensore();
                    if (primoSensoreAzionato) {
                        eseguoOperazioniPrimoSensoreAzionato();

                        cambioStatoAutopilotModalitaApproccioCasuale(
                            StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO);
                    }
                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO: {
                    RisultatoInMovimento risultatoInMovimento = inMovimento();
                    switch (risultatoInMovimento) {
                        case CONVOGLIO_FERMATO_BINARIO_STAZIONE: {
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                                  "Stato: CONVOGLIO_FERMATO_BINARIO_STAZIONE",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                            cambioStatoAutopilotModalitaApproccioCasuale(
                                StatoAutopilotModalitaApproccioCasuale::ARRIVATO_STAZIONE_DESTINAZIONE);

                            break;
                        }

                        case CONVOGLIO_FERMATO_SEZIONE_INTERMEDIA: {
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                                  "Stato: CONVOGLIO_FERMATO_SEZIONE_INTERMEDIA",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                            // Se l'ultima sezione bloccata è una sezione intermedia che non sia l'ultima allora il
                            // convoglio si è fermato prima della stazione di destinazione. Passiamo al relativo
                            // stato.
                            cambioStatoAutopilotModalitaApproccioCasuale(
                                StatoAutopilotModalitaApproccioCasuale::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI);
                            break;
                        }

                        case CONVOGLIO_ANCORA_IN_MOVIMENTO: {
                            // Non devo fare nulla
                            break;
                        }
                    }
                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI: {
                    bool lockPreso = fermoSezioneAttesaLockSezioni();
                    if (lockPreso) {
                        cambioStatoAutopilotModalitaApproccioCasuale(
                            StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO);
                    }
                    break;
                }

                case StatoAutopilotModalitaApproccioCasuale::ARRIVATO_STAZIONE_DESTINAZIONE: {
                    cambioStatoInizializzazioneAutopilotConvoglio(
                        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT);

                    arrivatoStazioneDestinazione();

                    // Se l'autopilot è in fase di spegnimento, devo passare allo stato
                    // PRONTO_INIZIALIZZAZIONE_CONVOGLIO per poter far fermare l'autopilot
                    if (autopilotInSpegnimento) {
                        cambioStatoInizializzazioneAutopilotConvoglio(
                            StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);
                    } else {
                        // Se l'autopilot non è in fase di spegnimento, allora resetto lo stato in modo tale che
                        // l'autopilot possa ripartire per una nuova destinazione in maniera automatica
                        cambioStatoAutopilotModalitaApproccioCasuale(
                            StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA);
                    }
                    break;
                }
            }
    }
}

int AutopilotConvoglioAbstract::
    trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva() {
    /*
    Non tutte le sezioni sono idonee a ospitare un convoglio:
    - La sezione potrebbe non essere lunga a sufficienza per ospitare il convoglio + la distanza di sicurezza.
    - La sezione potrebbe essere una sezione critica, dunque non può essere considerata come sezione di arrivo.

    Se il convoglio prova a bloccare di volta in volta solo sezioni non critiche e di lunghezza sufficienza, vorrà
    dire che le sezioni critiche verranno bloccate tutte assieme.

    Scansiono tutte le sezioni intermedie dalla stazione attuale a quella successiva.
    La funzione ritorna la prima sezione del percorso idonea
    Se non trovo nessuna sezione abbastanza lunga restituisco -1.
    */

    // L'array parte da 0
    for (int i = indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva + 1;
         i < sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray; i++) {
        IdSezioneTipo idSezioneCorrente = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
        SezioneAbstract *sezione = sezioni.getSezione(idSezioneCorrente);

        // Non solo la sezione deve poter contenere il convoglio, ma bisogna considerare anche che il convoglio deve
        // fermarsi indicativamente un po' prima della fine della sezione.
        bool sezionePuoOspitareConvoglio = isSezioneLungaASufficienzaPerConvoglio(sezione);

        bool isSezioneCritica = sezione->isCritica(posizioneConvoglio.direzionePrimaLocomotivaDestra);

        // Se la sezione è capiente a sufficiente e non è critica per il senso di marcia del convoglio, allora si
        // tratta di una sezione idonea e possiamo restituirla.
        if (sezionePuoOspitareConvoglio && !isSezioneCritica) {
            return i;
        }
    }
    return -1;
}

LunghezzaTracciatoTipo AutopilotConvoglioAbstract::calcoloLunghezzaSezioniBloccate() {
    LunghezzaTracciatoTipo lunghezzaTotaleSezioniBloccate = 0;
    int indicePrimaSezioneBloccata = 0;
    // Se c'è qualche sezione sbloccata allora devo partire dalla sezione successiva a quella bloccata, altrimenti
    // parto da 0.
    if (indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva >= 0) {
        indicePrimaSezioneBloccata =
            indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva + 1;
    }
    for (int i = indicePrimaSezioneBloccata;
         i < indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva + 1; i++) {
        IdSezioneTipo idSezioneCorrente = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
        SezioneAbstract *sezione = sezioni.getSezione(idSezioneCorrente);
        lunghezzaTotaleSezioniBloccate += sezione->getLunghezzaCm();
    }
    return lunghezzaTotaleSezioniBloccate;
}

void AutopilotConvoglioAbstract::aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop() {
    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm =
        calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // Per calcolare la distanza dal convoglio al prossimo stop, devo considerare la distanza del convoglio
    // dall'ultimo sensore azionato e sottrarci la distanza che il convoglio ha percorso da esso
    LunghezzaTracciatoTipo distanzaDaConvoglioAProssimoStopCm =
        distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm - distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm;

    if (DEBUGGING_AUTOPILOT_CALCOLO_DISTANZA_PROSSIMO_STOP) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Distanza da ultimo sensore azionato a prossimo stop: ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              std::to_string(distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Distanza percorsa dal convoglio dall'ultimo sensore: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              std::to_string(distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              false);

        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Distanza da convoglio a prossimo stop: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(distanzaDaConvoglioAProssimoStopCm).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              false);
    }

    // Non posso semplicemente cambiare la velocità in +1 e -1 confrontando continuamente la distanza che ho per frenare
    // rispetto alla distanza che necessito alla velocità attuale. Questo perché, nel caso in cui da un sensore
    // all'altro venga accumulato dell'errore, questo errore non viene azzerato immediatamente.

    LocomotivaAbstract *locomotivaPiuVeloceConvoglio =
        locomotive.getLocomotiva(convoglio.getLocomotivaPiuVeloce(), true);

    LunghezzaTracciatoTipo distanzaFrenataStepAttuale =
        distanzaFrenataLocomotiva.getDistanzaFrenataPerVelocitaKmH(locomotivaPiuVeloceConvoglio->getVelocitaAttuale());
    VelocitaRotabileKmHTipo nuovaVelocita;

    if (DEBUGGING_AUTOPILOT_CALCOLO_DISTANZA_PROSSIMO_STOP) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Distanza frenata step attuale corrente: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(distanzaFrenataStepAttuale).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
    }

    // Se dal convoglio al prossimo stop ho più spazio di quanto necessito per frenare allo step attuale, allora posso
    // aumentare la velocità.
    if (distanzaDaConvoglioAProssimoStopCm > distanzaFrenataStepAttuale) {
        // Se la velocità della locomotiva è minore della velocità massima impostata per l'autopilot, allora posso
        // aumentarla.
        if (locomotivaPiuVeloceConvoglio->getVelocitaAttuale() < VELOCITA_MASSIMA_AUTOPILOT_KM_H) {
            // Se la distanza che ho a disposizione adesso per frenare è maggiore della distanza necessaria per frenare
            // allo step attuale, posso aumentare la velocità. In questo caso possiamo modificare la velocità in maniera
            // graduale, dunque + 1.
            nuovaVelocita = locomotivaPiuVeloceConvoglio->getVelocitaAttuale() + 1;
            if (DEBUGGING_AUTOPILOT_CALCOLO_DISTANZA_PROSSIMO_STOP) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Accelero",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                      "Nuova velocità: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(nuovaVelocita).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        } else {
            // Lascio la velocità precedente
            nuovaVelocita = locomotivaPiuVeloceConvoglio->getVelocitaAttuale();
        }
    } else {
        // Nel caso in cui la distanza che ho a disposizione adesso per frenare è minore della distanza necessaria per
        // frenare allo step attuale, significa che sto andando troppo veloce e devo iniziare a rallentare.
        // Tuttavia, in questo caso non possiamo semplicemente modificare la velocità sottraendo 1.
        // Potremmo farlo nel caso in cui avessimo tantissimi sensori distanti pochissimo l'uno dall'altro, ma in questo
        // caso no. Questo perché bisogna tenere conto che tra le lettura di due sensori differenti potrebbe essersi
        // accumulato dell'errore: quando un convoglio passa su un sensore potrebbe accorgersi improvvisamente di avere
        // a disposizione molto meno spazio di quello che pensava. Dobbiamo quindi essere celeri nel riportare la
        // velocità del convoglio a una velocità che gli consenta di frenare prima dello stop. In questo caso non ci
        // interessa rendere la decelerazione gradevole.

        LunghezzaRotabileTipo differenzaDistanzaFrenataTraStepEGoalMigliore = 1000000;

        LunghezzaRotabileTipo differenzaDistanzaFrenataTraStepAttualeEGoal;
        LunghezzaTracciatoTipo distanzaFrenataTarget = distanzaDaConvoglioAProssimoStopCm;

        // Scansioniamo le distanze di frenata per tutte le velocità possibili e scegliamo quella più adeguata, ovvero
        // la distanza di frenata più vicina allo spazio disponibile per fermarsi, superandolo (successivamente viene
        // spiegato il perché di questa scelta).
        for (int i = 0; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
            LunghezzaTracciatoTipo distanzaFrenataStepAttualeCorrente =
                distanzaFrenataLocomotiva.getDistanzaFrenataPerVelocitaKmH(i);

            if (distanzaFrenataTarget < distanzaFrenataStepAttualeCorrente) {
                // Solo se la distanza di frenata target è minore della distanza di frenata dello step attuale
                // considero questo step. In precedenza avevo provato a considerare entrambe le casistiche, sia quando
                // distanzaFrenataTarget fosse maggiore di distanzaFrenataStepAttualeCorrente che viceversa: tuttavia si
                // aveva il problema per cui la decelerazione avveniva a scatti di 2 invece che di 1. Si lascia al
                // lettore il ragionamento sul perché.

                differenzaDistanzaFrenataTraStepAttualeEGoal =
                    distanzaFrenataStepAttualeCorrente - distanzaFrenataTarget;

                // Se la differenza è minore di quella che avevo trovato precedentemente, allora questo è lo step
                // migliore per il momento.
                if (differenzaDistanzaFrenataTraStepAttualeEGoal < differenzaDistanzaFrenataTraStepEGoalMigliore) {
                    nuovaVelocita = i;
                    differenzaDistanzaFrenataTraStepEGoalMigliore = differenzaDistanzaFrenataTraStepAttualeEGoal;
                }
            } else {
                // Se la distanza di frenata target è maggiore della distanza di frenata dello step attuale, allora
                // non considero questo step.
            }
        }

        if (DEBUGGING_AUTOPILOT_CALCOLO_DISTANZA_PROSSIMO_STOP) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Decelero",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Distanza frenata target: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(distanzaFrenataTarget).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Distanza frenata convoglio scelta: ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(
                logger, LivelloLog::DEBUG_,
                std::to_string(distanzaFrenataLocomotiva.getDistanzaFrenataPerVelocitaKmH(nuovaVelocita)).c_str(),
                InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Nuova velocità: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(nuovaVelocita).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
    // Gioco sempre con velocità attuale, velocità impostata e decelerazione e
    // accelerazione automatica non ci serve qui. Vogliamo avere il controllo
    // totale e preciso.
    convoglio.cambiaVelocita(
        nuovaVelocita, false,
        StatoAutopilot::ATTIVO);  // Se chiamo cambiaVelocita da qui sicuramente l'autopilot è ATTIVO.
}

LunghezzaTracciatoTipo AutopilotConvoglioAbstract::calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop() {
    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStop;
    // Devo separare i due casi: se l'ultimo sensore azionato è quello della stazione di destinazione oppure uno su una
    // delle sezioni intermedie.
    if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA) {
        // Devo misurare la distanza che c'è tra l'ultimo sensore su cui il convoglio passato fino al punto di stop
        // dell'ultima sezione bloccata.

        LunghezzaTracciatoTipo sommaLunghezzaSezioni = 0;

        // Durante il calcolo della lunghezza della prima sezione (dove c'è il sensore), bisogna considerare solo la
        // lunghezza dal sensore in poi.
        bool primaSezioneAnalizzata = false;

        for (int i = indiceSezioniIntermedie_UltimaSezioneSensoreAzionato;
             i < indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva + 1; i++) {
            // Siamo nello stato IN_MOVIMENTO. Ciò significa che un sensore è stato già azionato dal convoglio.
            if (i == -1) {
                SezioneAbstract *sezione =
                    sezioni.getSezione(percorsoConvoglioBinariStazioni[getIndiceStazionePartenzaCorrente()]);
                sommaLunghezzaSezioni +=
                    sezione->getDistanzaDaSensoreAFineSezioneCm(posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
                primaSezioneAnalizzata = true;
            } else {
                IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
                SezioneAbstract *sezione = sezioni.getSezione(idSezione);
                // Se è la prima sezione, devo considerare la distanza dal sensore in poi
                if (!primaSezioneAnalizzata) {
                    sommaLunghezzaSezioni += sezione->getDistanzaDaSensoreAFineSezioneCm(
                        posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
                    primaSezioneAnalizzata = true;
                } else if (i == indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva) {
                    LunghezzaTracciatoTipo lunghezzaSezione = sezione->getLunghezzaCm();

                    // Se l'ultima sezione bloccata è l'ultima delle sezioni intermedie, significa che devo considerare
                    // tutta questa sezione perché il convoglio si fermerà sul binario di stazione
                    if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva ==
                        sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
                        sommaLunghezzaSezioni += lunghezzaSezione;
                    } else {
                        // Se invece non si tratta dell'ultima sezione delle sezioni intermedie, allora significa che il
                        // convoglio si fermerà su questa sezione intermedia. Non posso considerare tutta la lunghezza
                        // della sezione. Altrimenti il convoglio si fermerebbe proprio a fine della sezione, con il
                        // rischio che vada a intralciare la sezione successiva.

                        sommaLunghezzaSezioni +=
                            lunghezzaSezione - getDistanzaCmStopConvoglioDaFineSezioneIntermedia(sezione);
                    }

                } else {
                    // La sezione non è né la prima, né l'ultima. C'è da considerare la lunghezza totale.
                    sommaLunghezzaSezioni += sezione->getLunghezzaCm();
                }
            }
        }

        // Se l'ultima sezione bloccata è l'ultima delle sezioni intermedie, significa che tutte le sezioni intermedie
        // sono bloccate, quindi devo considerare anche la lunghezza del binario di stazione
        if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva ==
            sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
            // Recupero l'ID della sezione della stazione di destinazione
            IdSezioneTipo idSezione = percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];

            // Recupero la sezione della stazione di destinazione
            SezioneAbstract *sezione = sezioni.getSezione(idSezione);

            LunghezzaTracciatoTipo distanzaStopConvoglioBinarioStazione =
                calcoloDistanzaDaInizioSezioneAStopConvoglioBinarioStazione(sezione);
            sommaLunghezzaSezioni += distanzaStopConvoglioBinarioStazione;
        }

        // Questa è la distanza
        distanzaDaUltimoSensoreAzionatoFinoAProssimoStop = sommaLunghezzaSezioni;
    } else {
        // Recupero l'ID della sezione della stazione di destinazione
        IdSezioneTipo idSezione = percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];

        // Recupero la sezione della stazione di destinazione
        SezioneAbstract *sezione = sezioni.getSezione(idSezione);

        StazioneAbstract *stazione = stazioni.getStazioneFromSezione(sezione->getId());

        if (stazione->isStazioneDiTesta()) {
            LunghezzaTracciatoTipo distanzaDaSensoreAFineSezioneStazione =
                sezione->getDistanzaDaSensoreAFineSezioneCm(posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);

            distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
                distanzaDaSensoreAFineSezioneStazione - DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA;
        } else {
            LunghezzaTracciatoTipo distanzaDaInizioSezioneAStopConvoglioBinarioStazione =
                calcoloDistanzaDaInizioSezioneAStopConvoglioBinarioStazione(sezione);
            if (tipologiaUltimoSensoreAzionato == STAZIONE_SENSORE_1) {
                distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
                    distanzaDaInizioSezioneAStopConvoglioBinarioStazione -
                    sezione->getDistanzaDaSensoreAInizioSezioneCm(posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
            } else if (tipologiaUltimoSensoreAzionato == STAZIONE_SENSORE_2) {
                distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
                    distanzaDaInizioSezioneAStopConvoglioBinarioStazione -
                    sezione->getDistanzaDaSensoreAInizioSezioneCm(posizioneConvoglio.direzionePrimaLocomotivaDestra, 2);
            }
        }
    }

    return distanzaDaUltimoSensoreAzionatoFinoAProssimoStop;
}

LunghezzaTracciatoTipo AutopilotConvoglioAbstract::calcoloDistanzaDaInizioSezioneAStopConvoglioBinarioStazione(
    SezioneAbstract *sezione) {
    StazioneAbstract *stazione = stazioni.getStazioneFromSezione(sezione->getId());

    if (stazione->isStazioneDiTesta()) {
        // Faccio fermare il convoglio appena prima del paraurti del binario
        return sezione->getLunghezzaCm() - DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA;
    } else {
        // Calcolo quanto dista la metà del binario, dopodiché aggiungo la metà
        // della lunghezza del convoglio
        return sezione->getLunghezzaCm() / 2 + (convoglio.getLunghezzaCm() / 2);
    }
}

byte AutopilotConvoglioAbstract::getIndiceStazionePartenzaCorrente() const {
    return indiceBinarioStazionePercorsoConvoglio;
}

byte AutopilotConvoglioAbstract::getIndiceStazioneArrivoCorrente() const {
    return indiceBinarioStazionePercorsoConvoglio + 1;
}

bool AutopilotConvoglioAbstract::lockSezioni(IdSezioneTipo *arraySezioni, int numeroElementiArray) {
    for (int i = 0; i < numeroElementiArray; i++) {
        SezioneAbstract *sezione = sezioni.getSezione(arraySezioni[i]);
        bool successoLock = sezioni.lock(sezione->getId(), convoglio.getId());

        if (!successoLock) {
            // Se non sono riuscito a fare lock, allora libero le sezioni precedentemente occupate
            for (int r = 0; r < i; r++) {
                sezioni.unlock(arraySezioni[r]);
            }
            return false;
        }
    }
    // Se sono riuscito a bloccare tutte le sezioni, ritorno true, false altrimenti
    return true;
}

void AutopilotConvoglioAbstract::lockSezioniFuturo() {
    // Non posso limitarmi a bloccare una sezione per volta e aspettare
    // che il convoglio raggiunga quella sezione prima di provare a bloccare il
    // set di sezioni successivo. Questo perché lo spazio a disposizione
    // per un'eventuale frenata rimarrebbe sempre minimo e questo non
    // consentirebbe al convoglio di andare a una velocità moderata.
    // Tuttavia, dobbiamo evitare anche di bloccare tutte le sezioni successive,
    // altrimenti un solo convoglio potrebbe bloccare buona parte delle sezioni
    // del plastico, la maggior parte delle quali troppo in anticipo e limitare
    // la circolazione dei treni. Introduciamo dunque una soglia in cm che
    // indica la lunghezza massima del totale delle sezioni bloccabili.

    LunghezzaTracciatoTipo lunghezzaSezioniBloccate = calcoloLunghezzaSezioniBloccate();

    // Provo a bloccare le sezioni fino alla prossima sezione capiente per il convoglio fin quando non sono riuscito
    // a prendere il lock oppure ho raggiunto la lunghezza massima bloccabile
    bool lockPreso;
    do {
        lockPreso =
            lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();
        if (lockPreso) {
            IdSezioneTipo idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
                    .arraySezioni[indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva];
            char *idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessivaStringa =
                idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva.toStringShort();

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Lock sezioni fino alla prossima sezione capiente preso con successo.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ultima sezione bloccata sezione presa: ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_,

                                   idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessivaStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
        }

    } while (lockPreso && lunghezzaSezioniBloccate < lunghezzaTotaleSezioniBloccabili);
    // Se il totale della lunghezza delle sezioni già bloccate è inferiore a quella ammissibile, provo a bloccare
    // sezioni successive fino a una sezione abbastanza capiente per il convoglio.
}

bool AutopilotConvoglioAbstract::
    lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva() {
    // Devo individuare la prossima sezione che può ospitare il convoglio e
    // bloccare le sezioni per poterla raggiungere.
    // Se non c'è una sezione abbastanza capiente, blocco tutte le sezioni intermedie.

    int indiceProssimaSezioneCapientePerConvoglioPercorsoConvoglioArraySezioni =
        trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    if (indiceProssimaSezioneCapientePerConvoglioPercorsoConvoglioArraySezioni == -1) {
        // Se ritorna -1, significa che non c'è una sezione intermedia abbastanza capiente per il convoglio, bisogna
        // dunque cercare di bloccare tutte le sezioni intermedie
        indiceProssimaSezioneCapientePerConvoglioPercorsoConvoglioArraySezioni =
            sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1;
    }

    int indiceSezionePartenza;
    // Se l'indice dell'ultima sezione bloccata è uguale all'ultima sezione intermedia, significa che tutte le
    // sezioni intermedie sono bloccate. Restituiamo false perché non possiamo fare il lock di nessun'altra sezione.
    if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva ==
        sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
        return false;
    }
    // Se è -1 significa che non è stata bloccata ancora nessuna delle sezioni intermedie, dunque iniziamo da 0
    else if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva == -1) {
        indiceSezionePartenza = 0;
    } else {
        indiceSezionePartenza = indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva + 1;
    }

    int indiceSezioneArrivo = indiceProssimaSezioneCapientePerConvoglioPercorsoConvoglioArraySezioni;

    bool risultatoLock = lockSezioniDaSezioneASezione_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva(
        indiceSezionePartenza, indiceSezioneArrivo);

    if (risultatoLock) {
        // Dato che sono riuscito a prendere i lock, memorizzo gli indici della prima e ultima sezione bloccate

        indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
            indiceProssimaSezioneCapientePerConvoglioPercorsoConvoglioArraySezioni;
    }

    return risultatoLock;
}

// indiceSezionePartenza e indiceSezioneArrivo inclusi
bool AutopilotConvoglioAbstract::lockSezioniDaSezioneASezione_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva(
    int indiceSezionePartenza, int indiceSezioneArrivo) {
    // Creo un array buffer dove salvo tutti gli ID delle sezioni
    // Se la sezione n° 3 è stata già bloccata e la sezione capiente è la n° 6,
    // sono interessato alle posizioni dalla 4 alla 6
    int numeroSezioni = indiceSezioneArrivo - indiceSezionePartenza + 1;

    IdSezioneTipo *idSezioniBuffer = new IdSezioneTipo[numeroSezioni];
    byte indiceBuffer = 0;

    for (int i = indiceSezionePartenza; i < indiceSezioneArrivo + 1; i++) {
        idSezioniBuffer[indiceBuffer] = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
        indiceBuffer += 1;
    }

    bool risultatoLock = lockSezioni(idSezioniBuffer, numeroSezioni);

    // Se riesco a prendere i lock, memorizzo l'indice
    // della sezione fino alla quale sono riuscito a bloccare.
    if (risultatoLock) {
        indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
            trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();
    }

    // Libero la memoria allocata per l'array buffer
    delete[] idSezioniBuffer;

    return risultatoLock;
}

// ** UNLOCK ** //

void AutopilotConvoglioAbstract::unlockSezioni(IdSezioneTipo *arraySezioni, int numeroElementiArray) {
    for (int i = 0; i < numeroElementiArray; i++) {
        sezioni.unlock(arraySezioni[i]);
    }
}

/*
void unlockSezioniDaSezioneASezione(int indiceSezionePartenza, int indiceSezioneArrivo) {
    for (int i = indiceSezionePartenza; i < indiceSezioneArrivo + 1; i++) {
        // unlockSezione(percorsoConvoglioArraySezioni[i]);
    }
}
 */

void AutopilotConvoglioAbstract::unlockSezioniPassaggioConvoglioAvvenuto() {
    // Se la stazione di partenza non è stata ancora sbloccata
    if (indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva == -2) {
        // Devo calcolare la lunghezza dalla fine della sezione (iniziando dunque dalle sezioni intermedie) di stazione
        // fino al sensore
        LunghezzaTracciatoTipo distanzaTraSensoreEFineSezioneCorrente = 0;

        // +1 perché devo considerare anche la sezione contenente il sensore
        for (int i = 0; i < indiceSezioniIntermedie_UltimaSezioneSensoreAzionato + 1; i++) {
            IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
            SezioneAbstract *sezione = sezioni.getSezione(idSezione);

            // Se è l'ultima sezione devo considerare la distanza dall'inizio della sezione al sensore
            if (i == indiceSezioniIntermedie_UltimaSezioneSensoreAzionato) {
                distanzaTraSensoreEFineSezioneCorrente +=
                    sezione->getDistanzaDaSensoreAFineSezioneCm(!posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
            }
            // Se non si tratta dell'ultima sezione devo considerare tutta la lunghezza della sezione
            else {
                distanzaTraSensoreEFineSezioneCorrente += sezione->getLunghezzaCm();
            }
        }

        // Se la distanza percorsa dal convoglio è maggiore della lunghezza del convoglio, allora il binario di stazione
        // è da considerarsi libero.
        if (distanzaTraSensoreEFineSezioneCorrente - convoglio.getLunghezzaCm() > 0) {
            IdSezioneTipo idBinarioStazione = percorsoConvoglioBinariStazioni[getIndiceStazionePartenzaCorrente()];
            char *idSezioneStringa = idBinarioStazione.toStringShort();
            SezioneAbstract *sezione = sezioni.getSezione(idBinarioStazione);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sblocco la stazione di partenza: ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

            sezioni.unlock(sezione->getId());

            // Segnalo che la stazione di partenza è stata sbloccata
            indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = -1;
        }
    }

    // Se la sezione di stazione è stata liberata procedo con le sezioni intermedie
    if (indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva >= -1) {
        if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA) {
            /*
            Ci sono tre casi:
            - Il sensore è su una sezione più avanti rispetto alla sezione presa in esame
            - Il sensore è su una sezione precedente rispetto alla sezione presa in esame
            - Il sensore è sulla stessa sezione presa in esame
            */

            // Devo capire fino a che sezione posso sbloccare, per poter capire come impostare il ciclo for
            int indiceUltimaSezioneDaCapireSeSbloccabileSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva;
            // Se è stata bloccata l'ultima sezione delle sezioni intermedie, allora è possibile considerare di
            // sbloccare anche questa sezione.
            if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva ==
                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
                // Voglio che nel ciclo for venga analizzata anche l'ultima sezione intermedia perché è possibile
                // sbloccarla considerando che il convoglio ha già bloccato la sezione di stazione.
                indiceUltimaSezioneDaCapireSeSbloccabileSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
                    indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva + 1;
            } else {
                // Se non si tratta dell'ultima sezione intermedia, allora non posso considerare questa sezione come
                // sbloccabile.
                indiceUltimaSezioneDaCapireSeSbloccabileSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
                    indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva;
            }

            // Parto dalla sezione successiva all'ultima sezione sbloccata e arrivo all'ultima sezione bloccata esclusa
            // (l'ultima sezione bloccata è quella su cui è posizionata il convoglio).
            for (int i = indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva + 1;
                 i < indiceUltimaSezioneDaCapireSeSbloccabileSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva;
                 i++) {
                // Se la tipologia dell'ultimo sensore azionato è su una sezione intermedia, allora effettuo la logica
                // seguente che assume che l'ultimo sensore azionato sia su una sezione intermedia
                // sezione 1 (sensore) -> sezione 2 -> sezione 3 (corrente)
                // Il sensore è su una sezione precedente rispetto alla sezione presa in esame
                if (indiceSezioniIntermedie_UltimaSezioneSensoreAzionato <
                    i) {
                    // Devo considerare la distanza tra la fine della sezione corrente e il sensore.
                    // Questa distanza va sottratta alla distanza percorsa dal convoglio dall'ultimo sensore.
                    LunghezzaTracciatoTipo distanzaDaFineSezioneASensoreCorrente = 0;
                    for (int r = indiceSezioniIntermedie_UltimaSezioneSensoreAzionato; r < i + 1; r++) {
                        SezioneAbstract *sezione;
                        // Se r == -1 significa che il sensore è sul binario di stazione
                        if (r == -1) {
                            IdSezioneTipo idSezionePartenza =
                                percorsoConvoglioBinariStazioni[getIndiceStazionePartenzaCorrente()];
                            sezione = sezioni.getSezione(idSezionePartenza);
                        } else {
                            sezione = sezioni.getSezione(
                                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[r]);
                        }
                        // Se è la prima sezione bisogna considerare la distanza dal sensore in poi
                        if (r == indiceSezioniIntermedie_UltimaSezioneSensoreAzionato) {
                            distanzaDaFineSezioneASensoreCorrente += sezione->getDistanzaDaSensoreAFineSezioneCm(
                                posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
                        } else {
                            distanzaDaFineSezioneASensoreCorrente += sezione->getLunghezzaCm();
                        }
                    }
                    // Sottraggo la distanza tra la fine della sezione corrente e il sensore dalla distanza percorsa dal
                    // convoglio dall'ultimo sensore
                    LunghezzaTracciatoTipo distanzaPercorsaConvoglioDaFineSezioneCorrente =
                        distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm - distanzaDaFineSezioneASensoreCorrente;

                    // Se la distanza percorsa dal convoglio è maggiore della lunghezza del convoglio, allora la sezione
                    // corrente è da considerarsi libera.
                    if (distanzaPercorsaConvoglioDaFineSezioneCorrente - convoglio.getLunghezzaCm() > 0) {
                        IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
                        char *idSezioneStringa = idSezione.toStringShort();

                        SezioneAbstract *sezione = sezioni.getSezione(idSezione);

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO. ",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sblocco la seguente sezione: ",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

                        sezioni.unlock(sezione->getId());
                        indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = i;
                    }
                }
                // Il sensore è su una sezione successiva rispetto alla sezione presa in esame
                // sezione 1 (corrente) -> sezione 2 -> sezione 3 (sensore)
                else if(indiceSezioniIntermedie_UltimaSezioneSensoreAzionato >
                         i) {
                    LunghezzaTracciatoTipo distanzaTraSensoreEFineSezioneCorrente = 0;

                    // Devo considerare la distanza tra il sensore e la fine della sezione corrente e aggiungerla alla
                    // distanza percorsa dal convoglio dall'ultimo sensore.
                    // Parto da +2 perché devo partire dalla fine della sezione corrente, dunque non va considerata la
                    // sua lunghezza
                    for (int r = indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva + 2;
                         r < indiceSezioniIntermedie_UltimaSezioneSensoreAzionato + 1; r++) {
                        SezioneAbstract *sezione =
                            sezioni.getSezione(sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[r]);

                        // Se è l'ultima sezione devo considerare la distanza dall'inizio della sezione al sensore
                        if (r == indiceSezioniIntermedie_UltimaSezioneSensoreAzionato) {
                            distanzaTraSensoreEFineSezioneCorrente += sezione->getDistanzaDaSensoreAFineSezioneCm(
                                !posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
                        }
                        // Se non si tratta dell'ultima sezione devo considerare tutta la lunghezza della sezione
                        else {
                            distanzaTraSensoreEFineSezioneCorrente += sezione->getLunghezzaCm();
                        }
                    }

                    LunghezzaTracciatoTipo distanzaDaFineSezioneCorrenteAConvoglio =
                        distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm + distanzaTraSensoreEFineSezioneCorrente;

                    if (distanzaDaFineSezioneCorrenteAConvoglio - convoglio.getLunghezzaCm() > 0) {
                        IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
                        char *idSezioneStringa = idSezione.toStringShort();
                        SezioneAbstract *sezione = sezioni.getSezione(idSezione);

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sblocco la seguente sezione: ",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

                        sezioni.unlock(sezione->getId());
                        indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = i;
                    }
                } else{
                    // Il sensore è sulla stessa sezione presa in esame

                    // Calcolo la lunghezza che c'è tra il sensore e la fine di questa sezione
                    IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
                    SezioneAbstract * sezione = sezioni.getSezione(idSezione);
                    LunghezzaTracciatoTipo distanzaDaSensoreAFineSezioneCm = sezione->getDistanzaDaSensoreAFineSezioneCm(
                        posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);

                    // Calcolo la distanza tra il sensore e la coda del convoglio
                    LunghezzaTracciatoTipo distanzaTraCodaConvoglioASensoreCm = distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm - convoglio.getLunghezzaCm();
                    if(distanzaTraCodaConvoglioASensoreCm > distanzaDaSensoreAFineSezioneCm){
                        char *idSezioneStringa = idSezione.toStringShort();

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO. ",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sblocco la seguente sezione: ",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

                        sezioni.unlock(sezione->getId());
                        indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = i;
                    }

                }
            }

        }  // Altrimenti devo seguire la seguente logica, che tiene in considerazione che l'ultimo sensore azionato non
        // è su una sezione intermedia
        else {
            // Parto dalla sezione successiva all'ultima sezione sbloccata e arrivo all'ultima sezione intermedia.
            for (int i = indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva + 1;
                 i < sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray; i++) {
                // Devo considerare la somma di:
                // - Distanza dall'ultima sezione sbloccata inclusa alla fine di tutte le sezioni intermedie
                // - Distanza dall'inizio del binario di stazione al sensore

                // Inizio a sommare la lunghezza di tutte le sezioni intermedie a partire dalla sezione presa in esame
                LunghezzaTracciatoTipo distanzaDaSezioneAttualeAFineSezioniIntermedie = 0;
                for (int r = i; r < sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray; r++) {
                    SezioneAbstract *sezione =
                        sezioni.getSezione(sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[r]);
                    distanzaDaSezioneAttualeAFineSezioniIntermedie += sezione->getLunghezzaCm();
                }

                IdSezioneTipo idBinarioStazioneArrivo =
                    percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];
                SezioneAbstract *sezioneStazioneArrivo = sezioni.getSezione(idBinarioStazioneArrivo);

                // Aggiungo la distanza dal sensore all'inizio del binario di stazione
                LunghezzaTracciatoTipo distanzaDaInizioBinarioDiStazioneASensore;
                if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1) {
                    distanzaDaInizioBinarioDiStazioneASensore =
                        sezioneStazioneArrivo->getDistanzaDaSensoreAInizioSezioneCm(
                            posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
                } else if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_2) {
                    distanzaDaInizioBinarioDiStazioneASensore =
                        sezioneStazioneArrivo->getDistanzaDaSensoreAInizioSezioneCm(
                            posizioneConvoglio.direzionePrimaLocomotivaDestra, 2);
                }

                // Ora sommo le due distanze
                LunghezzaTracciatoTipo distanzaDaUltimaSezioneSbloccataCorrenteASensore =
                    distanzaDaSezioneAttualeAFineSezioniIntermedie + distanzaDaInizioBinarioDiStazioneASensore;

                if (distanzaDaUltimaSezioneSbloccataCorrenteASensore - convoglio.getLunghezzaCm() > 0) {
                    IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
                    char *idSezioneStringa = idSezione.toStringShort();

                    SezioneAbstract *sezione = sezioni.getSezione(idSezione);

                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sblocco la seguente sezione: ",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

                    sezioni.unlock(sezione->getId());
                    indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = i;
                }
            }
        }
    }
}

// ** FUNZIONI AUSILIARIE ** //

int AutopilotConvoglioAbstract::trovoIndiceSezioniIntermedie_PrimaSezioneConSensore() {
    // Scansiono tutte le sezioni intermedie e trovo il primo sensore

    for (int i = 0; i < sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray; i++) {
        IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
        SezioneAbstract *sezione = sezioni.getSezione(idSezione);

        if (sezione->hasSensore()) {
            return i;
        }
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                          "Non ho trovato nessun sensore nelle "
                          "sezioni intermedie",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    programma.ferma(false);
}

int AutopilotConvoglioAbstract::trovoIndiceSezioniIntermedie_ProssimaSezioneConSensore() {
    // Scansiono le sezioni a partire da quella dopo l'ultima sezione con il sensore azionato fino all'ultima sezione
    // bloccata. Non vogliamo aspettare sensori di sezioni che non abbiamo ancora bloccato.
    for (int i = indiceSezioniIntermedie_UltimaSezioneSensoreAzionato + 1;
         i < indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva + 1; i++) {
        IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i];
        SezioneAbstract *sezione = sezioni.getSezione(idSezione);

        if (sezione->hasSensore()) {
            return i;
        }
    }

    return -1;
}

void AutopilotConvoglioAbstract::leggoPassaggioSensoreSuccessivoSeEsiste() {
    // Quando entro qui significa che il convoglio ha appena superato il
    // primo sensore (può essere quello di stazione se presente oppure il primo utile).

    // Mi metto in attesa del passaggio sul sensore successivo del percorso, se questo esiste.
    int indiceSezioniIntermedie_ProssimaSezioneConSensore = trovoIndiceSezioniIntermedie_ProssimaSezioneConSensore();

    // Se esiste una sezione con sensore prima della sezione del binario di stazione, mi metto in attesa.
    if (indiceSezioniIntermedie_ProssimaSezioneConSensore != -1) {
        IdSezioneTipo idSezioneProssimaConSensore =
            sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
                .arraySezioni[indiceSezioniIntermedie_ProssimaSezioneConSensore];

        IdSensorePosizioneTipo idSensore =
            sezioni.getSezione(idSezioneProssimaConSensore)
                ->getSensorePassaggioLocomotiva(posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);

        SensorePosizioneAbstract *sensore = sensoriPosizione.getSensorePosizione(idSensore);
        // Segnalo che sul sensore in questione sto aspettando questo convoglio.
        sensore->setIdConvoglioInAttesa(convoglio.getId());

        bool passaggioSensore = sensore->fetchStato();

        if (passaggioSensore) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO. ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sensore di una sezione intermedia azionato!",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "ID sensore: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idSensore).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoPrimaDelPassaggioSulSensore =
                calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();
            LunghezzaTracciatoTipo distanzaDaConvoglioAProssimoStopCmPrimaDelPassaggioSulSensore =
                distanzaDaUltimoSensoreAzionatoPrimaDelPassaggioSulSensore -
                distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm;

            indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = indiceSezioniIntermedie_ProssimaSezioneConSensore;
            tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA;
            // Ora che il convoglio è passato su un altro sensore resettiamo la distanza percorso dall'ultimo
            // sensore
            distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

            LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoDopoIlPassaggioSulSensore =
                calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();
            LunghezzaTracciatoTipo distanzaDaConvoglioAProssimoStopCmDopoIlPassaggioSulSensore =
                distanzaDaUltimoSensoreAzionatoDopoIlPassaggioSulSensore -
                distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm;

            LunghezzaTracciatoTipo erroreAutopilotCorretto =
                distanzaDaConvoglioAProssimoStopCmDopoIlPassaggioSulSensore -
                distanzaDaConvoglioAProssimoStopCmPrimaDelPassaggioSulSensore;

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Errore autopilot corretto con il passaggio sul sensore: ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(erroreAutopilotCorretto).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
    // Potremmo essere in due casi
    // - Il convoglio ha bloccato tutte le sezioni intermedie ma nessuna di queste possiede un sensore.
    // - Ci sono sezioni intermedie con sensore successive ma ancora non bloccate.
    else if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva ==
             sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
        // Se il convoglio ha bloccato tutte le sezioni intermedie non esiste una sezione intermedia con sensore,
        // verifichiamo se ci sia un sensore sul binario di stazione
        IdSezioneTipo idSezioneBinarioStazione = percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];
        // Verifico che il binario di stazione possegga un sensore
        SezioneAbstract *sezioneBinarioStazione = sezioni.getSezione(idSezioneBinarioStazione);

        // Teoricamente tutti i binari delle stazioni posseggono un sensore ma l'algoritmo non vuole imporre questa
        // assunzione. Quindi controlliamo se il binario di stazione possiede un sensore.
        if (sezioneBinarioStazione->hasSensore()) {
            IdSensorePosizioneTipo idSensore;
            // Ricordiamo che i binari delle stazioni di testa posseggono un solo sensore mentre i binari delle stazioni
            // simmetriche posseggono due sensori. Nel caso di stazioni simmetriche è necessario capire quale dei due
            // sensori sia stato azionato. Se l'ultimo sensore azionato è quello di una sezione intermedia, allora è il
            // primo sensore sul binario di stazione che incontro.
            if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA) {
                idSensore = sezioneBinarioStazione->getSensorePassaggioLocomotiva(
                    posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
            } else if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1) {
                // Se il binario di stazione possiede due sensori e ne è stato attivato uno solo, allora devo aspettare
                // il passaggio sul secondo sensore, altrimenti non mi aspetto di passare su altri sensori.
                if (sezioneBinarioStazione->hasDueSensori()) {
                    idSensore = sezioneBinarioStazione->getSensorePassaggioLocomotiva(
                        posizioneConvoglio.direzionePrimaLocomotivaDestra, 2);
                } else {
                    // Non mi aspetto di passare su altri sensori
                    return;
                }

            } else if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_2) {
                // Se è stato azionato anche il secondo sensore del binario (a questo punto si tratta di una stazione
                // simmetrica) non devo aspettare il passaggio su nessun altro sensore.
                return;
            } else {
                // Necessario per azzittire warning su variabile null
                return;
            }

            SensorePosizioneAbstract *sensore = sensoriPosizione.getSensorePosizione(idSensore);

            // Segnalo che sul sensore in questione sto aspettando questo convoglio.
            sensore->setIdConvoglioInAttesa(convoglio.getId());
            bool passaggioSensore = sensore->fetchStato();

            if (passaggioSensore) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_MOVIMENTO. ",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sensore della stazione di destinazione azionato!",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                      "ID sensore: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idSensore).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoPrimaDelPassaggioSulSensore =
                    calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();
                LunghezzaTracciatoTipo distanzaDaConvoglioAProssimoStopCmPrimaDelPassaggioSulSensore =
                    distanzaDaUltimoSensoreAzionatoPrimaDelPassaggioSulSensore -
                    distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm;

                distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

                // Quando il sensore di stazione è azionato, devo modificare questa variabile in modo tale che la
                // funzione calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop() sappia che l'ultimo sensore
                // azionato non è delle sezioni intermedie ma della stazione di arrivo.
                if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA) {
                    // Se l'ultimo sensore azionato è quello di una sezione intermedia, allora è stato azionato il primo
                    // sensore del binario di stazione
                    tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1;
                } else if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1) {
                    // Se è stato azionato il primo sensore del binario di stazione, allora aspetto il passaggio sul
                    // secondo sensore
                    tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_2;
                }

                LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoDopoIlPassaggioSulSensore =
                    calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();
                LunghezzaTracciatoTipo distanzaDaConvoglioAProssimoStopCmDopoIlPassaggioSulSensore =
                    distanzaDaUltimoSensoreAzionatoDopoIlPassaggioSulSensore -
                    distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm;

                LunghezzaTracciatoTipo erroreAutopilotCorretto =
                    distanzaDaConvoglioAProssimoStopCmDopoIlPassaggioSulSensore -
                    distanzaDaConvoglioAProssimoStopCmPrimaDelPassaggioSulSensore;

                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                      "Errore autopilot corretto con il passaggio sul sensore: ",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(erroreAutopilotCorretto).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " cm",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        }

        // Se il binario di stazione non possiede un sensore non faccio nulla, la funzione
        // leggoPassaggioSensoreSuccessivoSeEsiste() continuerà ad essere richiamata ma senza alcun effetto.
    } else {
        // Ci sono sezioni intermedie con sensore successive ma ancora non bloccate.
        // Non facciamo nulla
    }
}

void AutopilotConvoglioAbstract::attesaInizializzazioneConvoglio() {
    // Il convoglio non è stato ancora registrato, non faccio
    // nulla. Rimango in attesa che l'utente chiami la funzione setPosizioneConvoglio().
}

void AutopilotConvoglioAbstract::prontoPerConfigurazioneAutopilot() {
    // Il convoglio è stato registrato ma nessun percorso è stato impostato.
    // Rimango in attesa che l'utente configuri l'autopilot tramite una delle chiamate inizializzo()
}

// Ritorna true se è arrivato il momento di partenza
bool AutopilotConvoglioAbstract::fermoStazioneAttesaMomentoAttesaPartenza() {
    // Aspetto il momento di partenza: ovvero l'intervallo di attesa
    // impostato dall'utente dal momento in cui la locomotiva arriva in
    // stazione al momento in cui deve partire.
    // Aspetto anche prima di bloccare la stazione di arrivo perché se il convoglio deve aspettare per
    // un'ora inutile bloccare per tutto questo tempo il binario della stazione di arrivo.

    if (tempo.milliseconds() - millisInizioAttesaPartenzaStazione >= delayAttesaPartenzaStazione) {
        return true;
    }
    return false;
}

void AutopilotConvoglioAbstract::fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          "Stato: SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

    int indiceStazionePartenzaCorrente = getIndiceStazionePartenzaCorrente();
    IdSezioneTipo idSezionePartenza = percorsoConvoglioBinariStazioni[indiceStazionePartenzaCorrente];
    int indiceStazioneArrivoCorrente = getIndiceStazioneArrivoCorrente();
    IdSezioneTipo idSezioneArrivo = percorsoConvoglioBinariStazioni[indiceStazioneArrivoCorrente];

    // Salvo l'array delle sezioni intermedie da stazione di partenza a stazione di arrivo
    sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni =
        percorsi.getPercorso(idSezionePartenza, idSezioneArrivo)->idSezioniIntermedie;
    sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray =
        percorsi.getPercorso(idSezionePartenza, idSezioneArrivo)
            ->numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva;

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          "Sezioni intermedie da stazione attuale a stazione successiva salvate: ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    for (int i = 0; i < sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray; i++) {
        if (i == sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
            char *idSezioneStringa =
                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i].toStringShort();
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true, true);
        } else {
            char *idSezioneStringa =
                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i].toStringShort();
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true, true);
        }
    }
}

void AutopilotConvoglioAbstract::fermoStazioneCalcoloDirezioneConvoglio() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: CALCOLO_DIREZIONE_CONVOGLIO",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);

    // Abbiamo già verificato che la sequenza di binari sia corretta.
    // Dobbiamo solo capire se è necessario cambiare la direzione del convoglio.

    // Se il percorso esiste con la direzione opposta, allora cambio la direzione del convoglio
    if (!percorsi.esistePercorso(percorsoConvoglioBinariStazioni[getIndiceStazionePartenzaCorrente()],
                                 percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()],
                                 posizioneConvoglio.direzionePrimaLocomotivaDestra)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Il percorso esiste con la direzione opposta: cambio direzione al convoglio.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        convoglio.cambiaDirezione(StatoAutopilot::ATTIVO);
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Il percorso esiste con la direzione attuale del convoglio: non c'è bisogno di cambiare "
                              "direzione del convoglio.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

// Restituisce true se riesce a prendere il lock
bool AutopilotConvoglioAbstract::fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente() {
    // Inutile provare a prendere il lock in continuazione, lo faccio
    // ogni tot di tempo.
    if (tempo.milliseconds() - ultimoTentativoLock > INTERVALLO_TENTATIVO_LOCK_SEZIONI) {
        ultimoTentativoLock = tempo.milliseconds();

        bool risultatoLock =
            lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();
        if (risultatoLock) {
            IdSezioneTipo idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
                    .arraySezioni[indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva];
            char *idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessivaStringa =
                idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva.toStringShort();

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Stato: FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ultima sezione bloccata sezione presa: ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_,
                                   idUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessivaStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

            return true;
        }
    }

    return false;
}

void AutopilotConvoglioAbstract::inPartenzaStazione() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_PARTENZA_STAZIONE.",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Cambio velocità convoglio in ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          std::to_string(VELOCITA_USCITA_LOCOMOTIVA_STAZIONE_FINO_A_PRIMO_SENSORE).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " km/h", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    // Iniziamo a far muovere il convoglio a una velocità costante fino
    // a quando non passa sul sensore del binario
    convoglio.cambiaVelocita(VELOCITA_USCITA_LOCOMOTIVA_STAZIONE_FINO_A_PRIMO_SENSORE, true, StatoAutopilot::ATTIVO);
}

// Restituisce true se il primo sensore è stato azionato
bool AutopilotConvoglioAbstract::inMovimentoInAttesaPrimoSensore() {
    // Il sensore di nostro interesse è il primo sensore delle sezioni intermedie.
    // Non considero il sensore di stazione: questo perché i sensori di stazioni vengono usati solo per l'arrivo del
    // convoglio; un convoglio di 2 metri arrivato in stazione ha già superato l'ultimo sensore della stazione.

    int indiceSezioniIntermedie_PrimaSezioneConSensore = trovoIndiceSezioniIntermedie_PrimaSezioneConSensore();

    SezioneAbstract *sezione = sezioni.getSezione(sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
                                                      .arraySezioni[indiceSezioniIntermedie_PrimaSezioneConSensore]);

    // Devo tenere traccia di qual è l'ultima sezione in cui il sensore è già stato azionato per il
    // passaggio del convoglio. Impostiamo indiceSezioniIntermedie_UltimaSezioneSensoreAzionato all'indice della sezione
    // intermedia a cui abbiamo trovato il sensore.
    indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = indiceSezioniIntermedie_PrimaSezioneConSensore;

    IdSensorePosizioneTipo idSensore =
        sezione->getSensorePassaggioLocomotiva(posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
    SensorePosizioneAbstract *sensore = sensoriPosizione.getSensorePosizione(idSensore);

    // Segnalo che sul sensore in questione sto aspettando questo convoglio.
    sensore->setIdConvoglioInAttesa(convoglio.getId());

    bool passaggioSensore = sensore->fetchStato();

    // Appena il convoglio passa sul sensore, passo allo stato IN_MOVIMENTO e inizio a tenere traccia della
    // posizione.
    if (passaggioSensore) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: IN_ATTESA_PRIMO_SENSORE. ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Sensore azionato. ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "ID Sensore: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idSensore).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

        return true;
    }

    return false;
}

RisultatoInMovimento AutopilotConvoglioAbstract::inMovimento() {
    // Se esiste un sensore successivo, verifico il passaggio del convoglio su quest'ultimo ed eventualmente
    // aggiorno indiceSezioniIntermedie_UltimaSezioneSensoreAzionato e
    // distanzaPercorsaDaSensoreUltimaSezioneCm
    leggoPassaggioSensoreSuccessivoSeEsiste();

    TimestampTipo timestampCorrente = tempo.milliseconds();

    // Aggiungo la distanza percorsa dal convoglio nell'ultimo intervallo di tempo.
    // Potrei aggiornare la distanza percorsa insieme alla velocità del convoglio, tuttavia c'è un ma.
    // Quando viene azionato un sensore, stampo qual è la distanza che l'autopilot pensa che il convoglio abbia percorso
    // dall'ultimo sensore. In questo modo è possibile capire quanto è stato l'errore dell'autopilot prima del reset
    // dell'errore con il passaggio sul sensore. Tuttavia, se aggiorniamo la distanza percorsa insieme alla velocità,
    // significa che nella stampa delle info c'è un ritardo di INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT.
    if (timestampCorrente - timestampUltimoAggiornamentoDistanzaPercorsaConvoglioAutopilot >=
        INTERVALLO_AGGIORNAMENTO_DISTANZA_PERCORSA_CONVOGLIO_AUTOPILOT) {
        // Non necessariamente l'ultimo intervallo di tempo è INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT,
        // Arduino esegue le istruzioni velocemente ma non c'è nessuna garanzia della precisione al millisecondo. Anche
        // un millisecondo di ritardo potrebbe essere sufficiente per creare un errore significativo se
        // INTERVALLO_AGGIORNAMENTO_DISTANZA_PERCORSA_CONVOGLIO_AUTOPILOT è piccolo (qualche decina di millisecondi).
        TimestampTipo ultimoIntervalloTempo =
            timestampCorrente - timestampUltimoAggiornamentoDistanzaPercorsaConvoglioAutopilot;

        distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm +=
            calcoloDistanzaPercorsaDalConvoglioUltimoIntervalloTempo(ultimoIntervalloTempo);

        aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop();

        timestampUltimoAggiornamentoDistanzaPercorsaConvoglioAutopilot = timestampCorrente;
    }

    // Aggiorno la velocità del convoglio in base all'intervallo inserito.
    // N.B. È importante che la velocità venga aggiornata dopo che viene aggiornata la distanza percorsa.
    // Questo perché calcoloDistanzaPercorsaDalConvoglioUltimoIntervalloTempo considera che il convoglio sia andato alla
    // velocità attuale nell'ultimo intervallo di tempo. Se la velocità viene aggiornata subito prima dell'aggiornamento
    // della distanza percorsa, l'intervallo di tempo in cui il convoglio è andato alla nuova velocità non è ancora
    // passato.
    if (timestampCorrente - timestampUltimoAggiornamentoAccelerazioneDecelerazioneConvoglioAutopilot >=
        INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT) {
        timestampUltimoAggiornamentoAccelerazioneDecelerazioneConvoglioAutopilot = timestampCorrente;
    }

    // Il convoglio è in movimento e le sezioni/scambi fino alla sezione
    // utile successiva sono stati bloccati. Tuttavia, prima di arrivare
    // lì, bisogna cercare di bloccare le sezioni successive in modo
    // tale da consentire al convoglio di viaggiare a una buona
    // velocità perché lo spazio di frenata a disposizione aumenta.
    lockSezioniFuturo();

    // Mano a mano che il convoglio si sposta, devo sbloccare le sezioni ormai superate dal convoglio.
    // Non posso aspettare che il convoglio arrivi alla stazione di destinazione o che passi su un sensore.
    unlockSezioniPassaggioConvoglioAvvenuto();

    /*
    Se il convoglio raggiunge velocità 0 significa che si è fermato nella stazione di arrivo o in una
    sezione intermedia perché non è riuscito a prendere lock di sezioni successive.
    */
    if (convoglio.getVelocitaAttuale() == 0) {
        // Se l'ultima sezione intermedia è bloccata, significa che il convoglio si è fermato in un binario
        // di stazione. Questo perché il convoglio non si fermerebbe mai nell'ultima sezione intermedia: il
        // binario di stazione è bloccato dall'inizio.
        if (indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva ==
            sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray - 1) {
            return CONVOGLIO_FERMATO_BINARIO_STAZIONE;

        } else {
            return CONVOGLIO_FERMATO_SEZIONE_INTERMEDIA;
        }
    }
    return CONVOGLIO_ANCORA_IN_MOVIMENTO;
}

// Restituisce il risultato del lock delle sezioni
bool AutopilotConvoglioAbstract::fermoSezioneAttesaLockSezioni() {
    // Inutile provare a prendere il lock in continuazione, lo faccio
    // ogni tot di tempo.
    if (tempo.milliseconds() - ultimoTentativoLock > INTERVALLO_TENTATIVO_LOCK_SEZIONI) {
        ultimoTentativoLock = tempo.milliseconds();

        bool risultatoLock =
            lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();
        if (risultatoLock) {
            IdSezioneTipo idSezioneUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva =
                sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
                    .arraySezioni[indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva];

            char *idSezioneUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessivaStringa =
                idSezioneUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva.toStringShort();

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stato: FERMO_SEZIONE_ATTESA_LOCK_SEZIONI.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Lock sezioni fino alla prossima sezione capiente preso con successo.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ultima sezione bloccata sezione presa: ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(
                logger, LivelloLog::DEBUG_,
                idSezioneUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessivaStringa,
                InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

            return true;
        }
    }
    return false;
}

void AutopilotConvoglioAbstract::scelgoBinarioStazioneSuccessivaELock() {
    bool binarioLockPreso = false;

    // Scansiono tutte le stazioni fin tanto che non trovo un binario libero.
    SezioneAbstract *binarioScelto;

    do {
        // Scelgo casualmente la stazione successiva.

        // Recupero la stazione di partenza
        StazioneAbstract *stazionePartenza =
            stazioni.getStazioneFromSezione(posizioneConvoglio.idSezioneCorrenteOccupata);
        IdStazioneTipo idStazionePartenza = stazionePartenza->getId();

        IdStazioneTipo idStazioneSceltoCasualmente;

        const IdStazioneTipo *idStazioniPresenti = stazioni.getIdStazioniPresenti();
        stazioni.getIdStazioniPresenti();
        do {
            // Non utilizzo funzione random in modo tale da essere compatibile sia con la suite Google
            // Test che viene eseguita nativamente sia con Arduino.
            unsigned long timestamp = tempo.milliseconds();
            // Ottengo un numero casuale tra 1 e il numero di stazioni presenti.
            int numeroSceltoCasualmente = (IdStazioneTipo)(timestamp % stazioni.getNumeroStazioni()) + 1;
            // Ottengo l'id della stazione scelto casualmente
            idStazioneSceltoCasualmente = idStazioniPresenti[numeroSceltoCasualmente];
        }
        // Continuo a scegliere casualmente la stazione fin tanto che scelgo una diversa da quella di partenza.
        while (idStazioneSceltoCasualmente == idStazionePartenza);

        StazioneAbstract *stazioneScelta = stazioni.getStazione(idStazioneSceltoCasualmente);

        // Se l'utente ha richiesto l'ottimizzazione durante l'assegnazione dei convogli ai binari,
        // allora cerco di bloccare il binario più corto che possa ospitare il convoglio
        if (ottimizzazioneAssegnazioneConvogliAiBinariModalitaAutopilotCasuale) {
            // I binari della stazione sono già ordinati in base alla lunghezza.
            // Scansiono i binari della stazione dal più corto al più lungo.

            for (int i = 1; i < stazioneScelta->numeroBinari + 1; i++) {
                binarioScelto = stazioneScelta->getBinario(i);
                // Se il binario è lungo a sufficienza per ospitare il convoglio provo a prendere il lock
                if (isSezioneLungaASufficienzaPerConvoglio(binarioScelto)) {
                    binarioLockPreso = sezioni.lock(binarioScelto->getId(), convoglio.getId());
                    // Se sono riuscito a prendere il lock del binario, allora esco dal ciclo
                    if (binarioLockPreso) {
                        break;
                    }
                }
            }

        } else {
            int numeroTentativi = 0;
            do {
                // Se non devo fare l'ottimizzazione, allora scelgo casualmente il binario fin tanto
                // che ne trovo uno libero
                int indiceBinarioStazioneSceltoCasualmente =
                    (int)tempo.milliseconds() % stazioneScelta->numeroBinari + 1;
                binarioScelto = stazioneScelta->getBinario(indiceBinarioStazioneSceltoCasualmente);

                // Se il binario è lungo a sufficienza per ospitare il convoglio provo a prendere il lock
                if (isSezioneLungaASufficienzaPerConvoglio(binarioScelto)) {
                    binarioLockPreso = sezioni.lock(binarioScelto->getId(), convoglio.getId());
                }

                // Se tutti i binari della stazione presa in considerazione sono bloccati, rimango
                // in questo do-while finché non trovo un binario libero, il che potrebbe richiedere
                // molto tempo. Invece introduco un contatore del numero di tentativi: se dopo un
                // certo numero di tentativi non sono riuscito a prendere nessun binario di questa stazione,
                // allora esco dal while e passo alla stazione successiva, anch'essa scelta casualmente.
                numeroTentativi += 1;
            } while (!binarioLockPreso && numeroTentativi < NUMERO_TENTATIVI_LOCK_BINARIO_SCELTO_CASUALMENTE);
        }
    } while (!binarioLockPreso);

    // Una volta che ho preso il lock del binario, salvo l'id del binario scelto
    percorsoConvoglioBinariStazioni[1] = binarioScelto->getId();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "AUTOPILOT - Convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio.getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ho scelto e preso il lock del seguente binario: ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, percorsoConvoglioBinariStazioni[1].toStringShort(),
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
}

void AutopilotConvoglioAbstract::arrivatoStazioneDestinazione() {
    // Salvo la nuova posizione del convoglio
    posizioneConvoglio.idSezioneCorrenteOccupata = percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];

    // Durante il tragitto dell'autopilot era stata invalidata la posizione del convoglio, ora è
    // possibile rivalidarla.
    posizioneConvoglio.validita = ValiditaPosizioneConvoglio::POSIZIONE_VALIDA;
    fileConvogliDaAggiornare = true;

    /*
    Dobbiamo controllare se sono presenti sezioni di inversione di polarità nel percorso appena effettuato.
    Se presenti, dobbiamo cambiare la direzione del convoglio per ogni sezione di inversione di polarità presente.
    Questo perché se un convoglio dovesse passare su una sezione di inversione di polarità senza che noi cambiamo la
    direzione, il sistema utilizzerebbe la direzione sbagliata durante il calcolo della direzione per raggiungere la
    stazione successiva.

    Prendiamo come esempio un convoglio con direzionePrimaLocomotivaDestra = true che va dalla sezione 1.3.1 alla
    sezione 1.1.1 passando per la sezione 2.3 (senza inversione di polarità). Il percorso inverso è stato salvato con
    direzionePrimaLocomotivaDestra = false, quindi verrà invertita la direzione del convoglio prima della partenza per
    la sezione 1.3.1.

    Prendiamo ora come esempio un convoglio con direzionePrimaLocomotivaDestra = false che va dalla sezione a 1.3.1
    alla sezione 1.1.1 passando per la sezione 2.7 (con inversione di polarità). Il percorso inverso è sempre stato
    salvato con direzionePrimaLocomotiva = false, quindi non viene cambiata la direzione al convoglio prima della
    partenza: questo causa il convoglio andare a sbattere contro il paraurti.
    */

    for (int i = 0; i < sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray; i++) {
        SezioneAbstract *sezione =
            sezioni.getSezione(sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni[i]);
        // Se la sezione presenta l'inversione di polarità, allora cambio la direzione del convoglio
        if (sezione->hasInversioneDiPolarita()) {
            posizioneConvoglio.direzionePrimaLocomotivaDestra = !posizioneConvoglio.direzionePrimaLocomotivaDestra;
        }
    }

    /*
    Supponiamo che ci sia un guasto su un sensore: vogliamo che i log ci aiutino a identificarlo.
    Se il guasto si verifica sul primo sensore dopo la stazione di partenza, noteremmo che lo stato
    dell'autopilot rimarrebbe in IN_ATTESA_PRIMO_SENSORE anziché passare a IN_MOVIMENTO, il che è facile
    da rilevare. Tuttavia, se il guasto si verifica su un sensore intermedio, potremmo non accorgercene
    subito, poiché il convoglio si ferma comunque alla stazione di destinazione basandosi sulla
    posizione approssimata calcolata in precedenza. Questo accumulerebbe un errore significativo, ma
    l'autopilot continuerebbe a funzionare. Pertanto, per facilitare le operazioni di debugging, quando
    il convoglio arriva alla stazione di destinazione, vogliamo stampare un log di ERROR se il convoglio
    non è passato su tutti i sensori intermedi previsti.
    */

    int indiceSezioniIntermedie_ProssimaSezioneConSensore = trovoIndiceSezioniIntermedie_ProssimaSezioneConSensore();
    // Ci aspettiamo che non ci siano sezioni intermedie con un sensore ancora da azionare
    if (indiceSezioniIntermedie_ProssimaSezioneConSensore != -1) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "AUTOPILOT - Convoglio n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(convoglio.getId()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Il convoglio è arrivato in stazione ma non tutti i sensori previsti "
                              "sul percorso sono stati azionati!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

        IdSezioneTipo idSezione = sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
                                      .arraySezioni[indiceSezioniIntermedie_ProssimaSezioneConSensore];
        char *idSezioneStringa = idSezione.toString();

        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Mi aspettavo che il sensore presente sulla sezione con ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, idSezioneStringa,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, " venisse azionato.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Verificare il corretto funzionamento del sensore n° ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

        SezioneAbstract *sezione = sezioni.getSezione(idSezione);
        IdSensorePosizioneTipo idSensore =
            sezione->getSensorePassaggioLocomotiva(posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(idSensore).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

        eseguoOperazioniSensoreAttesoNonAzionato(idSensore);
    } else {
        /* Solo se non ci sono sezioni intermedie con sensori da azionare, controlliamo il sensore di
        stazione. È chiaro che se un sensore in una sezione intermedia non è stato azionato, anche il
        sensore di stazione non sarà stato azionato in quanto l'algoritmo di autopilot li legge in
        maniera sequenziale.
        */
        // Se la tipologia dell'ultimo sensore azionato è ancora una SEZIONE_INTERMEDIA significa che il
        // sensore sulla stazione non è mai stato azionato
        if (tipologiaUltimoSensoreAzionato == TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "AUTOPILOT - Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(convoglio.getId()).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                                  "Il convoglio è arrivato in stazione ma il sensore sul binario di stazione "
                                  "non è stato azionato!",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

            IdSezioneTipo idSezioneBinarioStazione = percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];
            SezioneAbstract *sezioneBinarioStazione = sezioni.getSezione(idSezioneBinarioStazione);

            char *idSezioneBinarioStazioneStringa = idSezioneBinarioStazione.toString();

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Mi aspettavo che il sensore presente sulla sezione con ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, idSezioneBinarioStazioneStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, " venisse azionato.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            IdSensorePosizioneTipo idSensore = sezioneBinarioStazione->getSensorePassaggioLocomotiva(
                posizioneConvoglio.direzionePrimaLocomotivaDestra, 1);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Verificare il corretto funzionamento del sensore: n° ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(idSensore).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

            eseguoOperazioniSensoreAttesoNonAzionato(idSensore);
        }
    }

    // Se il binario di stazione presenta due sensori, nel caso in cui il convoglio sia molto corto il secondo sensore non viene azionato.
    // Dobbiamo quindi resettare il suo stato.
    IdSezioneTipo idSezioneBinarioStazione = percorsoConvoglioBinariStazioni[getIndiceStazioneArrivoCorrente()];
    SezioneAbstract *sezioneBinarioStazione = sezioni.getSezione(idSezioneBinarioStazione);
    if(sezioneBinarioStazione->hasDueSensori()){
        IdSensorePosizioneTipo idSensore2 = sezioneBinarioStazione->getSensorePassaggioLocomotiva(posizioneConvoglio.direzionePrimaLocomotivaDestra, 2);
        SensorePosizioneAbstract *sensore2 = sensoriPosizione.getSensorePosizione(idSensore2);
        sensore2->reset(true);
    }

    resetParziale();
}

// Questa funzione deve essere chiamata quando si vuole sapere se una sezione è sufficientemente lunga per ospitare il
// convoglio nella modalità autopilot (viene consierato anche il potenziale errore dell'autopilot). Se si vuole sapere
// se una sezione è sufficientemente lunga per ospitare il convoglio in situazione normale usare la funzione presente in
// SezioneAbstract.
bool AutopilotConvoglioAbstract::isSezioneLungaASufficienzaPerConvoglio(SezioneAbstract *sezione) const {
    LunghezzaRotabileTipo lunghezzaConvoglio = convoglio.getLunghezzaCm();
    LunghezzaTracciatoTipo lunghezzaSezione = sezione->getLunghezzaCm();
    /*
    Il ragionamento seguente si applicazione sia per sezioni di stazione che sezioni intermedie ma prendiamo come
    esempio una sezione di stazione. Devo tenere conto dell'errore di posizionamento: se il convoglio si dovesse fermare
    a 5 cm più dello stop non sarebbe un grave problema: questo perché la
    DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA già include il possibile errore. Tuttavia, nel caso in
    cui il convoglio si dovesse fermare a 5 cm prima dello stop, la sezione precedente alla sezione di stazione potrebbe
    risultare ancora occupata dal convoglio (ammesso che il convoglio sia molto lungo). Uno schemino riepilogativo:
    <---- DISTANZA MINIMA DA FINE BINARIO STAZIONE DI TESTA ----> <---- ERRORE MASSIMO POSIZIONAMENTO CONVOGLIO ---->
    <---- LUNGHEZZA CONVOGLIO ----> <---- ERRORE MASSIMO POSIZIONAMENTO CONVOGLIO ----> <---- DISTANZA MINIMA DA FINE
    BINARIO STAZIONE DI TESTA ----> Nel nostro caso DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA
    comprende già DISTANZA MINIMA DA FINE BINARIO STAZIONE DI TESTA ed ERRORE MASSIMO POSIZIONAMENTO CONVOGLIO in quanto
    è un valore maggiore della somma di queste due variabili. D'altra parte dobbiamo considerare ERRORE MASSIMO
    POSIZIONAMENTO CONVOGLIO e DISTANZA MINIMA DA FINE BINARIO STAZIONE DI TESTA.
    */

    if (sezione->getId().isBinarioStazione()) {
        return lunghezzaSezione > lunghezzaConvoglio + DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA +
                                      ERRORE_DISTANZA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_STAZIONE +
                                      DISTANZA_CM_MINIMA_DA_FINE_SEZIONE_INTERMEDIA;
    } else {
        if (lunghezzaSezione <= SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE) {
            // Se la sezione è lunga meno di SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE, si
            // ritiene
            //        affidabile l'errore di posizionamento base per una sezione intermedia.
            return lunghezzaSezione >
                   DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA + lunghezzaConvoglio +
                       ERRORE_DISTANZA_MINIMA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_INTERMEDIA +
                       DISTANZA_CM_MINIMA_DA_FINE_SEZIONE_INTERMEDIA;
        } else {
            /* Bisogna tenere conto della lunghezza della sezione.
            Se però la sezione è più lunga, allora per ogni 10 cm in più si considera
            DISTANZA_CM_ERRORE_POSIZIONAMENTO_AGGIUNTIVO_PER_OGNI_10_CM_SEZIONE_INTERMEDIA cm in più di errore di
            posizionamento rispetto all'errore di base.
            */

            LunghezzaTracciatoTipo distanzaCmSezioneInEccesso =
                lunghezzaSezione - SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE;

            double erroreDiPosizionamentoInEccesso =
                distanzaCmSezioneInEccesso / 10 *
                DISTANZA_CM_ERRORE_POSIZIONAMENTO_AGGIUNTIVO_PER_OGNI_10_CM_SEZIONE_INTERMEDIA;

            double erroreDiPosizionamentoTotale =
                ERRORE_DISTANZA_MINIMA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_INTERMEDIA +
                erroreDiPosizionamentoInEccesso;

            return lunghezzaSezione > getDistanzaCmStopConvoglioDaFineSezioneIntermedia(sezione) + lunghezzaConvoglio +
                                          erroreDiPosizionamentoTotale + DISTANZA_CM_MINIMA_DA_FINE_SEZIONE_INTERMEDIA;
        }
    }
}

LunghezzaTracciatoTipo AutopilotConvoglioAbstract::getDistanzaCmStopConvoglioDaFineSezioneIntermedia(
    SezioneAbstract *sezione) {
    LunghezzaTracciatoTipo lunghezzaSezione = sezione->getLunghezzaCm();

    // Se la sezione è lunga meno di SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE, si ritiene
    // affidabile l'errore di posizionamento base per una sezione intermedia.
    if (lunghezzaSezione <= SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE) {
        return DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA;
    } else {
        LunghezzaTracciatoTipo distanzaCmSezioneInEccesso =
            lunghezzaSezione - SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE;

        double erroreDiPosizionamentoInEccesso =
            distanzaCmSezioneInEccesso / 10 *
            DISTANZA_CM_ERRORE_POSIZIONAMENTO_AGGIUNTIVO_PER_OGNI_10_CM_SEZIONE_INTERMEDIA;

        // Calcolo qual è la nuova distanza minima da fine sezione intermedia: la distanza minima già teneva in
        // considerazione l'errore basilare, devo aggiungere solo l'errore in eccesso.
        return DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA + erroreDiPosizionamentoInEccesso;
    }
}

void AutopilotConvoglioAbstract::cambioStatoInizializzazioneAutopilotConvoglio(
    StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglioNuovo) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Cambio stato inizializzazione autopilot: ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(
        logger, LivelloLog::DEBUG_,
        getStatoInizializzazioneAutopilotConvoglioAsAString(statoInizializzazioneAutopilotConvoglio, logger),
        InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " -> ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(
        logger, LivelloLog::DEBUG_,
        getStatoInizializzazioneAutopilotConvoglioAsAString(statoInizializzazioneAutopilotConvoglioNuovo, logger),
        InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    statoInizializzazioneAutopilotConvoglio = statoInizializzazioneAutopilotConvoglioNuovo;

    // Invio al Front-End un messaggio MQTT con le info del convoglio aggiornate.
    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;
}

void AutopilotConvoglioAbstract::cambioStatoAutopilotModalitaStazioneSuccessiva(
    StatoAutopilotModalitaStazioneSuccessiva statoNuovo) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Cambio stato autopilot modalità stazione successiva: ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(
        logger, LivelloLog::DEBUG_,
        getStatoAutopilotModalitaStazioneSuccessivaAsAString(statoAutopilotModalitaStazioneSuccessiva, logger),
        InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " -> ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          getStatoAutopilotModalitaStazioneSuccessivaAsAString(statoNuovo, logger),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    setStatoAutopilotModalitaStazioneSuccessiva(statoNuovo);

    // Invio al Front-End un messaggio MQTT con le info del convoglio aggiornate.
    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;
}

void AutopilotConvoglioAbstract::cambioStatoAutopilotModalitaApproccioCasuale(
    StatoAutopilotModalitaApproccioCasuale statoNuovo) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Cambio stato autopilot modalità approccio casuale: ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(
        logger, LivelloLog::DEBUG_,
        getStatoAutopilotModalitaApproccioCasualeAsAString(statoAutopilotModalitaApproccioCasuale, logger),
        InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " -> ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          getStatoAutopilotModalitaApproccioCasualeAsAString(statoNuovo, logger),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    setStatoAutopilotModalitaApproccioCasuale(statoNuovo);

    // Invio al Front-End un messaggio MQTT con le info del convoglio aggiornate.
    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;
}

LunghezzaTracciatoTipo AutopilotConvoglioAbstract::calcoloDistanzaPercorsaDalConvoglioUltimoIntervalloTempo(
    TimestampTipo intervalloTempo) const {
    VelocitaRotabileKmHTipo velocitaConvoglio = convoglio.getVelocitaAttuale();

    double velocitaConvoglioCmS = convertoVelocitaRealeModellino(velocitaConvoglio);
    double intervalloAggiornamentoDistanzaPercorsaLocomotiveSecondi = (double)intervalloTempo / 1000;
    LunghezzaTracciatoTipo distanzaPercorsaCmUltimoIntervallo =
        velocitaConvoglioCmS * intervalloAggiornamentoDistanzaPercorsaLocomotiveSecondi;

    return distanzaPercorsaCmUltimoIntervallo;
}

void AutopilotConvoglioAbstract::resetTotale() {
    resetParziale();
    setStatoInizializzazioneAutopilotConvoglio(
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);
    setStatoAutopilotModalitaStazioneSuccessiva(
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA);
    setStatoAutopilotModalitaApproccioCasuale(
        StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA);
}

void AutopilotConvoglioAbstract::resetParziale() {
    indiceBinarioStazionePercorsoConvoglio = 0;
    indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva = -1;
    timestampUltimoAggiornamentoAccelerazioneDecelerazioneConvoglioAutopilot = 0;
    indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = -2;
    indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = -1;
    tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA;
}

void AutopilotConvoglioAbstract::setStatoInizializzazioneAutopilotConvoglio(
    StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglioLocale) {
    statoInizializzazioneAutopilotConvoglio = statoInizializzazioneAutopilotConvoglioLocale;
    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;
}

void AutopilotConvoglioAbstract::setStatoAutopilotModalitaStazioneSuccessiva(
    StatoAutopilotModalitaStazioneSuccessiva statoAutopilotModalitaStazioneSuccessivaLocale) {
    statoAutopilotModalitaStazioneSuccessiva = statoAutopilotModalitaStazioneSuccessivaLocale;
    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;
}

void AutopilotConvoglioAbstract::setStatoAutopilotModalitaApproccioCasuale(
    StatoAutopilotModalitaApproccioCasuale statoAutopilotModalitaApproccioCasualeLocale) {
    statoAutopilotModalitaApproccioCasuale = statoAutopilotModalitaApproccioCasualeLocale;
    convogliAggiornamentoMqttRichiesto[convoglio.getId()] = true;
}

void AutopilotConvoglioAbstract::eseguoOperazioniPrimoSensoreAzionato() {
    // Iniziamo a tenere traccia dei cm percorsi dall'ultimo sensore.
    // Soltanto la prima volta che si entra nello stato IN_MOVIMENTO questo contatore va
    // resettato.
    distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;
    // Necessario affinché anche il primo intervallo di tempo calcolato sia corretto.
    timestampUltimoAggiornamentoDistanzaPercorsaConvoglioAutopilot = tempo.milliseconds();
    // Devo aggiornare anche questo affinché l'operazione di aggiornamento della distanza percorsa e
    // l'operazione di aggiornamento della velocità siano sincronizzate.
    timestampUltimoAggiornamentoAccelerazioneDecelerazioneConvoglioAutopilot = tempo.milliseconds();
}

void AutopilotConvoglioAbstract::eseguoOperazioniSensoreAttesoNonAzionato(IdSensorePosizioneTipo idSensore){
    if (AUTOPILOT_SENSORE_ATTESO_MA_NON_AZIONATO_FERMA_PROGRAMMA) {
        // Un po' complicato mostrare un messaggio di errore sul display personalizzato per via delle dipendenze
        // circolari
        programma.ferma(false);
    } else {
// Non vogliamo eseguire il codice dai google test
#ifdef ARDUINO
        // Resetto lo stato del sensore in modo tale che in fase di sviluppo, sul front end lo stato del sensore
        // venga resettato, altrimenti rimarrebbe in attesa del convoglio.
        SensorePosizioneAbstract *sensorePosizione = sensoriPosizione.getSensorePosizione(idSensore);
        sensorePosizione->reset(true);
#endif
    }
}