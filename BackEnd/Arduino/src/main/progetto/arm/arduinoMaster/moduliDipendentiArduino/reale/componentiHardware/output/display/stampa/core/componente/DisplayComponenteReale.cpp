// *** INCLUDE *** //

#include "DisplayComponenteReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/DisplayCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINIZIONE METODI *** //

void DisplayComponenteReale::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo display... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    if (displayCore.begin(COLONNE_DISPLAY, RIGHE_DISPLAY)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non sono riuscito ad inizializzare il display.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(false);
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo display... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void DisplayComponenteReale::pulisce() { displayCore.clear(); }

void DisplayComponenteReale::posizionaCursore(int colonna, int riga) { displayCore.setCursor(colonna, riga); }

void DisplayComponenteReale::print(const __FlashStringHelper* stringa) { displayCore.print(stringa); }

void DisplayComponenteReale::print(const char* stringa) { displayCore.print(stringa); }

void DisplayComponenteReale::print(int numero) { displayCore.print(numero); }

void DisplayComponenteReale::print(char carattere) { displayCore.print(carattere); }

void DisplayComponenteReale::print(float numero) { displayCore.print(numero); }

void DisplayComponenteReale::print(String stringa) { displayCore.print(stringa); }

void DisplayComponenteReale::print(unsigned long numero) { displayCore.print(numero); }

// *** DEFINIZIONE VARIABILI *** //

DisplayComponenteReale displayComponenteReale(tempoReale, loggerReale, programmaReale, INDIRIZZO_I2C_DISPLAY);