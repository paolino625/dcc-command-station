// *** INCLUDE *** //

#include "I2CReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/stampa/core/DisplayCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/wire/WireReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZINOE VARIABILI *** //

I2cAbstract I2CReale(wireReale, displayReale, displayCoreReale, buzzerRealeArduinoMaster, programmaReale, delayReale,
                     loggerReale);