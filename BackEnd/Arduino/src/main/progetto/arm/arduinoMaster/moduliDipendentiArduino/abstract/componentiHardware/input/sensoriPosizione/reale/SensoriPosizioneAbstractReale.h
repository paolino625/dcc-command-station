#ifndef DCC_COMMAND_STATION_SENSORIPOSIZIONEABSTRACTREALE_H
#define DCC_COMMAND_STATION_SENSORIPOSIZIONEABSTRACTREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensorePosizione/SensorePosizioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriPosizione/SensoriPosizioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"

// *** CLASSE *** //

class SensoriPosizioneAbstractReale : public SensoriPosizioneAbstract {
   private:
    // *** VARIABILI *** //

    SensorePosizioneAbstract* sensoriPosizione[NUMERO_SENSORI_POSIZIONE_TOTALI];
    ProgrammaAbstract& programma;
    MqttArduinoMasterAbstract& mqttArduinoMaster;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    SensoriPosizioneAbstractReale(ProgrammaAbstract& programma, MqttArduinoMasterAbstract& mqttArduinoMaster, LoggerAbstract& logger)
        : programma(programma), mqttArduinoMaster(mqttArduinoMaster), logger(logger) {}

    // *** METODI PUBBLICI *** //

    void inizializza() override;
    void reset(boolean stampaLog) override;
    void resetInAttesaPassaggioPrimaLocomotivaConvoglio() override;
    bool esisteIdSensore(byte idSensore) override;

    // *** GETTER *** //

    SensorePosizioneAbstract* getSensorePosizione(int indiceSensore) override;
};

#endif