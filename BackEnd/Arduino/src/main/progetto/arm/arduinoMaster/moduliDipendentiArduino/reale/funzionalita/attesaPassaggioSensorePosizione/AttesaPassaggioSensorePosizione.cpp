// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/controlloCortoCircuitoTracciato/ControlloCortoCircuitoTracciatoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/arduinoSlaveRelay/ArduinoSlaveRelay.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/arduinoSlaveSensori/ArduinoSlaveSensori.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// Funzione chiamata soltanto dalla funzione mapping, utile per aspettare il passaggio sul sensore
// ma nel frattempo controllare eventuali corto circuiti
void aspettoPassaggioSensoreControlloCortoCircuito(
    byte sensoreInLettura, ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuitoTracciato,
    MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypadOperazioniMultiple) {
    // Prima di aspettare passaggio sul sensore resetto le precedenti letture dei sensori perché
    // potrebbero esserci stata una doppia lettura in precedenza
    sensoriPosizioneReali.getSensorePosizione(sensoreInLettura)->reset(false);

    // Da una chiamata all'altra di questa funzione, i buffer delle porte seriali si sono riempiti.
    // Resetto le porte seriali per questo motivo.
    // Impostiamo il beep a off in quanto si tratta un reset della porta seriale in maniera
    // controllata.
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Aspetto passaggio sul sensore n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(sensoreInLettura).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    /*
    stampo("Resetto porte seriali... \n\n");
    resettoPortaSerialeArduinoMaster(PORTA_SERIALE_ARDUINO_SLAVE_RELAY,
    VELOCITA_PORTA_SERIALE_ARDUINO, true, false);
    resettoPortaSerialeArduinoMaster(PORTA_SERIALE_ARDUINO_SLAVE_SENSORI,
    VELOCITA_PORTA_SERIALE_ARDUINO, true, false);
    */

    while (!sensoriPosizioneReali.getSensorePosizione(sensoreInLettura)->fetchStato()) {
        arduinoSlaveRelay.ricezione.riceve();
        arduinoSlaveSensori.riceve();
        arduinoSlaveRelay.controlloPresenza.controllaPresenza();
        controlloCortoCircuitoTracciato.effettuaControllo(menuDisplayKeypadOperazioniMultiple);
        displayReale.homepage.displayDaAggiornare = true;

        // Rinvio pacchetti nel caso in cui la locomotiva si sia fermata per mancanza di contatto
        // con il tracciato invioPacchetto(pacchettoExtendedDCC);  // Rinvio pacchetto velocità
        // invioPacchetto(pacchettoStandardDcc);        // Rinvio pacchetto luci
    }

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Aspetto passaggio sul sensore n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(sensoreInLettura).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... OK",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
}