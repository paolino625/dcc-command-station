#ifndef DCC_COMMAND_STATION_INIZIALIZZAZIONEARDUINOMASTER_H
#define DCC_COMMAND_STATION_INIZIALIZZAZIONEARDUINOMASTER_H

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

#define NUMERO_TENTATIVI_INIZIALIZZAZIONE_PORTA_SERIALE 1

// *** DICHIARAZIONE FUNZIONI *** //

boolean inizializzoPortaSerialeArduinoMaster(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                             bool portaSerialeDaVerificare, bool beepAttivo);

#endif
