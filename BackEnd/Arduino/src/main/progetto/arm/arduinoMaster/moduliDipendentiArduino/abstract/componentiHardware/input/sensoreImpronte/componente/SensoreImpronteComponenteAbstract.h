#ifndef DCC_COMMAND_STATION_SENSOREIMPRONTECOMPONENTE_H
#define DCC_COMMAND_STATION_SENSOREIMPRONTECOMPONENTE_H

// *** INCLUDE *** //

#include <cstdint>

// *** CLASSE *** //

class SensoreImpronteComponenteAbstract {
   public:
    virtual void begin() = 0;
    virtual bool verifyPassword() = 0;
    virtual uint8_t getImage() = 0;
    virtual uint8_t image2Tz() = 0;
    virtual uint8_t image2Tz(int numero) = 0;
    virtual uint8_t fingerSearch() = 0;
    virtual uint8_t createModel() = 0;
    virtual uint8_t storeModel(uint16_t id) = 0;
    virtual uint8_t emptyDatabase() = 0;
    virtual uint8_t getTemplateCount() = 0;
    virtual uint8_t getFingerprintIDVariabile() = 0;
    virtual uint8_t getTemplateCountVariabile() = 0;
    virtual uint8_t getConfidenceVariabile() = 0;
    virtual uint8_t LEDcontrol(uint8_t control, uint8_t speed, uint8_t coloridx,
                               uint8_t count = 0) = 0;
};

#endif
