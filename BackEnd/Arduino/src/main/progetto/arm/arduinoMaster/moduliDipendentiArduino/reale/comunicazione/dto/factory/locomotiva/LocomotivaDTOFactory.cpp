// *** INCLUDE *** //

#include "LocomotivaDTOFactory.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotiva/LocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
LocomotivaResponseDTO* LocomotivaDTOFactory::creaLocomotivaResponseDTO(IdLocomotivaTipo idLocomotiva) {
    if (DEBUGGING_FACTORY) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Creo LocomotivaDTO con ID: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idLocomotiva).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    LocomotivaAbstract* locomotivaAbstract = locomotiveReali.getLocomotiva(idLocomotiva, false);

    LocomotivaResponseDTO* locomotivaResponseDTO = new LocomotivaResponseDTO();
    locomotivaResponseDTO->id = locomotivaAbstract->getId();
    locomotivaResponseDTO->nome = locomotivaAbstract->getNome();
    locomotivaResponseDTO->codice = locomotivaAbstract->getCodice();

    return locomotivaResponseDTO;
};

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
char* LocomotivaDTOFactory::converteLocomotivaResponseDTOToStringJson(LocomotivaResponseDTO* locomotivaResponseDTO) {
    // Converto un oggetto LocomotivaDTO in una stringa JSON
    JsonDocument bufferJson;
    bufferJson["id"] = locomotivaResponseDTO->id;
    bufferJson["nome"] = locomotivaResponseDTO->nome;
    bufferJson["codice"] = locomotivaResponseDTO->codice;

    serializzoJson(bufferJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_LOCOMOTIVA_STRINGA_JSON, true);
    return stringaJsonUsbBuffer;
}

// *** DEFINIZIONE VARIABILI *** //

LocomotivaDTOFactory locomotivaDTOFactory = LocomotivaDTOFactory();