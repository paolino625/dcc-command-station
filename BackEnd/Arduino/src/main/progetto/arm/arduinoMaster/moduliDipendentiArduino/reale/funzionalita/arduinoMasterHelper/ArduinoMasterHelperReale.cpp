// *** INCLUDE *** //

#include "ArduinoMasterHelperReale.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/restClient/RestClientReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

bool ArduinoMasterHelperReale::esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo) {
    // Preparo il request body

    JsonDocument requestBodyJson;

    JsonObject idSezionePartenzaJson = requestBodyJson["idSezionePartenza"].to<JsonObject>();
    idSezionePartenzaJson["tipologia"] = idSezionePartenza.tipologia;
    idSezionePartenzaJson["id"] = idSezionePartenza.id;
    idSezionePartenzaJson["specifica"] = idSezionePartenza.specifica;

    JsonObject idSezioneArrivoJson = requestBodyJson["idSezioneArrivo"].to<JsonObject>();
    idSezioneArrivoJson["tipologia"] = idSezioneArrivo.tipologia;
    idSezioneArrivoJson["id"] = idSezioneArrivo.id;
    idSezioneArrivoJson["specifica"] = idSezioneArrivo.specifica;

    char requestBodyString[NUMERO_CARATTERI_MASSIMI_REQUEST_BODY];
    serializzoJson(requestBodyJson, requestBodyString, NUMERO_CARATTERI_MASSIMI_REQUEST_BODY, true);

    // Passiamo noi Response Header e Response Body, in questa maniera è il chiamante ad avere il controllo della
    // memoria di tale variabili
    char responseHeader[NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER];
    char responseBody[NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY];

    // Inizializzo le stringhe a stringa vuota
    memset(responseHeader, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER);
    memset(responseBody, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);

    // Inviamo la richiesta
    restClientReale.inviaRichiestaHTTP(REST_CLIENT_POST, INDIRIZZO_ARDUINO_MASTER_HELPER, PORTA_ARDUINO_MASTER_HELPER,
                                       "/percorso/esiste/entrambeDirezioni", requestBodyString, responseHeader,
                                       responseBody);

    // Se la chiamata è fallita il primo carattere sarà ancora '\0'
    if (responseBody[0] == '\0') {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Chiamata REST esistePercorso fallita",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        return false;
    } else {
        // Se la chiamata non è fallita, deserializziamo il JSON e ritorno il valore
        JsonDocument responseBodyJson;
        deserializzoJson(responseBodyJson, responseBody, true);
        return responseBodyJson["percorsoEsiste"];
    }
}

bool ArduinoMasterHelperReale::esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo,
                                              bool direzionePrimaLocomotivaDestra) {
    // Preparo il request body

    JsonDocument requestBodyJson;

    JsonObject idSezionePartenzaJson = requestBodyJson["idSezionePartenza"].to<JsonObject>();
    idSezionePartenzaJson["tipologia"] = idSezionePartenza.tipologia;
    idSezionePartenzaJson["id"] = idSezionePartenza.id;
    idSezionePartenzaJson["specifica"] = idSezionePartenza.specifica;

    JsonObject idSezioneArrivoJson = requestBodyJson["idSezioneArrivo"].to<JsonObject>();
    idSezioneArrivoJson["tipologia"] = idSezioneArrivo.tipologia;
    idSezioneArrivoJson["id"] = idSezioneArrivo.id;
    idSezioneArrivoJson["specifica"] = idSezioneArrivo.specifica;

    requestBodyJson["direzionePrimaLocomotivaDestra"] = direzionePrimaLocomotivaDestra;

    char requestBodyString[NUMERO_CARATTERI_MASSIMI_REQUEST_BODY];
    serializzoJson(requestBodyJson, requestBodyString, NUMERO_CARATTERI_MASSIMI_REQUEST_BODY, true);

    // Passiamo noi Response Header e Response Body, in questa maniera è il chiamante ad avere il controllo della
    // memoria di tale variabili
    char responseHeader[NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER];
    char responseBody[NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY];

    // Inizializzo le stringhe a stringa vuota
    memset(responseHeader, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER);
    memset(responseBody, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);

    // Inviamo la richiesta
    restClientReale.inviaRichiestaHTTP(REST_CLIENT_POST, INDIRIZZO_ARDUINO_MASTER_HELPER, PORTA_ARDUINO_MASTER_HELPER,
                                       "/percorso/esiste/unaSolaDirezione", requestBodyString, responseHeader,
                                       responseBody);

    JsonDocument responseBodyJson;
    deserializzoJson(responseBodyJson, responseBody, true);

    return responseBodyJson["percorsoEsiste"];
}

PercorsoAbstract* ArduinoMasterHelperReale::getPercorso(IdSezioneTipo idSezionePartenzaRequest,
                                                        IdSezioneTipo idSezioneArrivoRequest) {
    // Preparo il request body

    JsonDocument requestBodyJson;

    JsonObject idSezionePartenzaJson = requestBodyJson["idSezionePartenza"].to<JsonObject>();
    idSezionePartenzaJson["tipologia"] = idSezionePartenzaRequest.tipologia;
    idSezionePartenzaJson["id"] = idSezionePartenzaRequest.id;
    idSezionePartenzaJson["specifica"] = idSezionePartenzaRequest.specifica;

    JsonObject idSezioneArrivoJson = requestBodyJson["idSezioneArrivo"].to<JsonObject>();
    idSezioneArrivoJson["tipologia"] = idSezioneArrivoRequest.tipologia;
    idSezioneArrivoJson["id"] = idSezioneArrivoRequest.id;
    idSezioneArrivoJson["specifica"] = idSezioneArrivoRequest.specifica;

    char requestBodyString[NUMERO_CARATTERI_MASSIMI_REQUEST_BODY];
    serializzoJson(requestBodyJson, requestBodyString, NUMERO_CARATTERI_MASSIMI_REQUEST_BODY, true);

    // Passiamo noi Response Header e Response Body, in questa maniera è il chiamante ad avere il controllo della
    // memoria di tale variabili
    char responseHeader[NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER];
    char responseBody[NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY];

    // Inizializzo le stringhe a stringa vuota
    memset(responseHeader, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER);
    memset(responseBody, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);

    // Inviamo la richiesta
    restClientReale.inviaRichiestaHTTP(REST_CLIENT_POST, INDIRIZZO_ARDUINO_MASTER_HELPER, PORTA_ARDUINO_MASTER_HELPER,
                                       "/percorso/get", requestBodyString, responseHeader, responseBody);

    JsonDocument responseBodyJson;
    bool risultatoDeserializzazione = deserializzoJson(responseBodyJson, responseBody, false);
    if (!risultatoDeserializzazione) {
        return nullptr;
    }

    JsonObject bodyJson = responseBodyJson.as<JsonObject>();

    // Ottengo i vari oggetti Json dal body
    JsonObject idSezionePartenzaJsonObject = responseBodyJson["idSezionePartenza"].as<JsonObject>();
    JsonObject idSezioneArrivoJsonObject = responseBodyJson["idSezioneArrivo"].as<JsonObject>();
    JsonArray idSezioniIntermedieJsonArray = responseBodyJson["idSezioniIntermedie"].as<JsonArray>();

    // Salvo idSezionePartenza
    IdSezioneTipo idSezionePartenzaResponse;
    idSezionePartenzaResponse.tipologia = idSezionePartenzaJsonObject["tipologia"];
    idSezionePartenzaResponse.id = idSezionePartenzaJsonObject["id"];
    idSezionePartenzaResponse.specifica = idSezionePartenzaJsonObject["specifica"];

    // Salvo idSezioneArrivo
    IdSezioneTipo idSezioneArrivoResponse;
    idSezioneArrivoResponse.tipologia = idSezioneArrivoJsonObject["tipologia"];
    idSezioneArrivoResponse.id = idSezioneArrivoJsonObject["id"];
    idSezioneArrivoResponse.specifica = idSezioneArrivoJsonObject["specifica"];

    // Salvo l'array delle sezioni intermedie
    IdSezioneTipo* idSezioniIntermedie = new IdSezioneTipo[idSezioniIntermedieJsonArray.size()];
    for (int i = 0; i < idSezioniIntermedieJsonArray.size(); i++) {
        JsonObject idSezioneIntermediaJsonObject = idSezioniIntermedieJsonArray[i].as<JsonObject>();
        idSezioniIntermedie[i].tipologia = idSezioneIntermediaJsonObject["tipologia"];
        idSezioniIntermedie[i].id = idSezioneIntermediaJsonObject["id"];
        idSezioniIntermedie[i].specifica = idSezioneIntermediaJsonObject["specifica"];
    }

    // Salvo le informazioni dal JSON a un oggetto percorso che creo io

    PercorsoAbstract* percorso = new PercorsoAbstract(loggerReale);
    percorso->idSezionePartenza = idSezionePartenzaResponse;
    percorso->idSezioneArrivo = idSezioneArrivoResponse;
    percorso->idSezioniIntermedie = idSezioniIntermedie;
    percorso->numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = idSezioniIntermedieJsonArray.size();

    return percorso;
}

bool ArduinoMasterHelperReale::verificaConnessione() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Verifico connessione ad Arduino Master Helper...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    if (ARDUINO_MASTER_HELPER_PRESENTE) {
        char requestBodyString[NUMERO_CARATTERI_MASSIMI_REQUEST_BODY];

        // Passiamo noi Response Header e Response Body, in questa maniera è il chiamante ad avere il controllo della
        // memoria di tale variabili
        char responseHeader[NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER];
        char responseBody[NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY];

        // Inizializzo le stringhe a stringa vuota
        memset(responseHeader, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER);
        memset(responseBody, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);

        // Inviamo la richiesta
        restClientReale.inviaRichiestaHTTP(REST_CLIENT_GET, INDIRIZZO_ARDUINO_MASTER_HELPER,
                                           PORTA_ARDUINO_MASTER_HELPER, "/utility/healthCheck", requestBodyString,
                                           responseHeader, responseBody);

        // Se la chiamata è fallita il primo carattere sarà ancora '\0'
        if (responseBody[0] == '\0') {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL,
                                  "Chiamata REST verifica connessione ArduinoMaster Helper fallita!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            display.errori.arduinoMasterHelper.connessioneNonRiuscita();
            programma.ferma(false);
        } else {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Verifico connessione ad Arduino Master Helper... OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

            return true;
        }
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Comunicazione con Arduino Master Helper non attiva.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            return false;
    }
}

bool ArduinoMasterHelperReale::reset() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Reset Arduino Master Helper...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    if (ARDUINO_MASTER_HELPER_PRESENTE) {
        char requestBodyString[NUMERO_CARATTERI_MASSIMI_REQUEST_BODY];

        // Passiamo noi Response Header e Response Body, in questa maniera è il chiamante ad avere il controllo della
        // memoria di tale variabili
        char responseHeader[NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER];
        char responseBody[NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY];

        // Inizializzo le stringhe a stringa vuota
        memset(responseHeader, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER);
        memset(responseBody, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);

        // Inviamo la richiesta
        restClientReale.inviaRichiestaHTTP(REST_CLIENT_POST, INDIRIZZO_ARDUINO_MASTER_HELPER,
                                           PORTA_ARDUINO_MASTER_HELPER, "/utility/reset", requestBodyString,
                                           responseHeader, responseBody);

        // Se la chiamata è fallita il primo carattere sarà ancora '\0'
        if (responseBody[0] == '\0') {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, "Reset Arduino Master Helper fallito!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            display.errori.arduinoMasterHelper.connessioneNonRiuscita();
            programma.ferma(false);
        } else {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Reset Arduino Master Helper... OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

            return true;
        }
    }
}
// *** DEFINIZIONE VARIABILI *** //

ArduinoMasterHelperReale arduinoMasterHelperReale(displayReale, programmaReale);