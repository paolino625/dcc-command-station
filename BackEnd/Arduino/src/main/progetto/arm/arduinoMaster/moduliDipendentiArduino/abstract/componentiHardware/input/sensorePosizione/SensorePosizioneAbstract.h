#ifndef DCC_COMMAND_STATION_SENSOREPOSIZIONEABSTRACT_H
#define DCC_COMMAND_STATION_SENSOREPOSIZIONEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensorePosizione/letturaSensore/ModalitaLetturaSensore.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/mqtt/MqttArduinoMasterAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class SensorePosizioneAbstract {
    // *** VARIABILI *** //

   private:
    bool stato;
    IdSensorePosizioneTipo id;
    /*
    Il convoglio può essere composto da:
    - Una sola locomotiva: in questo caso avrà due strisce argentate a debita distanza
    - Due locomotive: entrambe le locomotive avranno una striscia argentata

     Quando il sensore viene azionato e non siamo in modalità di mapping locomotiva, dobbiamo capire se il sensore è
    stato azionato dalla prima locomotiva del convoglio o dalla seconda. All'autopilot interessa solo la prima
    locomotiva, quindi nel caso in cui il sensore venga azionato dalla seconda locomotiva, ignoriamo la lettura.
     */
    bool inAttesaPassaggioPrimaLocomotivaConvoglio = true;

    // Tengo memorizzato il convoglio che sta rimanendo in attesa di questo sensore.
    // 0 se non c'è nessun convoglio in attesa.
    // Mi risulta utile mantenere questa informazione per controllare se vengono azionati dei sensori su cui non mi
    // aspetto il passaggio di nessun convoglio: in questo caso qualcosa è andato storto (magari il sensore precedente a
    // questo sul percorso del convoglio non si è azionato a causa di un guasto).
    IdConvoglioTipo idConvoglioInAttesa = 0;
    ProgrammaAbstract &programma;
    MqttArduinoMasterAbstract &mqttArduinoMaster;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   public:
    SensorePosizioneAbstract(IdSensorePosizioneTipo idLocale, ProgrammaAbstract &programma, MqttArduinoMasterAbstract &mqttArduinoMaster,LoggerAbstract &logger)
        : programma(programma), mqttArduinoMaster(mqttArduinoMaster), logger(logger) {
        id = idLocale;
        stato = false;
    }

    // *** DEFINIZIONE METODI *** //

    void reset(bool resetIdConvoglioInAttesa);
    void resetInAttesaPassaggioPrimaLocomotivaConvoglio();

    IdSensorePosizioneTipo getId() const;
    bool getStato() const;
    IdConvoglioTipo getIdConvoglioInAttesa() const;
    bool fetchStato();
    void setStato(bool nuovoStato);
    void setIdConvoglioInAttesa(IdConvoglioTipo idConvoglio);
};

#endif