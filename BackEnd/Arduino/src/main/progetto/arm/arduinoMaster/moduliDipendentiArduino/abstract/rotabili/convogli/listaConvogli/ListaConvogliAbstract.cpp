// *** INCLUDE *** //

#include "ListaConvogliAbstract.h"

#include <cstdlib>
#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void ListaConvogliAbstract::inizializza() {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        listaConvogli[i] = (char *)malloc(NUMERO_CARATTERI_PER_CONVOGLIO_LISTA);  // +1 per il terminatore
                                                                                  // null
    }
}

int ListaConvogliAbstract::crea(bool mostraConvogliTracciato, bool mostraConvoglioFuoriTracciato,
                                bool mostraStatoConvoglio) {
    byte numeroConvogliTrovati = 0;

    byte indiceLista = 1;
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        std::string stringaConvoglio;
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, false);
        if (convoglio->isInUso()) {
            bool condizione = false;
            if (mostraConvogliTracciato && mostraConvoglioFuoriTracciato) {
                // La condizione è sempre true perché voglio mostrare tutti i
                // convogli
                condizione = true;
            } else if (mostraConvogliTracciato && !mostraConvoglioFuoriTracciato) {
                condizione = convoglio->isPresenteSulTracciato();
            } else if (!mostraConvogliTracciato && mostraConvoglioFuoriTracciato) {
                condizione = !convoglio->isPresenteSulTracciato();
            } else {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Condizioni input non rispettate.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                programma.ferma(true);
            }

            // Stampo in base alla condizione calcolata prima
            if (condizione) {
                numeroConvogliTrovati += 1;

                // Se mi è richiesto di stampare lo stato del convoglio lo
                // faccio
                if (mostraStatoConvoglio) {
                    if (convoglio->isPresenteSulTracciato()) {
                        stringaConvoglio += "* ";
                    } else {
                        stringaConvoglio += "  ";
                    }
                }

                stringaConvoglio += std::to_string(convoglio->getId());
                stringaConvoglio += ". ";
                stringaConvoglio += convoglio->getLocomotiva1()->getCodice();

                // Se stampo anche il codice della seconda locomotiva, non ci
                // entra tutto in una riga. Per il momento lascio perdere.

                if (convoglio->hasDoppiaLocomotiva()) {
                    stringaConvoglio += " + 1 ";
                    // stringaConvoglioLocomotiva1 +=
                    // getLocomotivaPerRiferimento(idLocomotiva2)->codice;
                }

                // Copio array locale String in struttura char passata per
                // riferimento

                strcpy(listaConvogli[indiceLista], stringaConvoglio.c_str());
                indiceLista += 1;
            }
        }
    }

    return numeroConvogliTrovati;
}
