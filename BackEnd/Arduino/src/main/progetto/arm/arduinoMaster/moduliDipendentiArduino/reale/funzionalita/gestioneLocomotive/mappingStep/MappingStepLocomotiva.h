#ifndef DCC_COMMAND_STATION_MAPPINGSTEPLOCOMOTIVA_H
#define DCC_COMMAND_STATION_MAPPINGSTEPLOCOMOTIVA_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/controlloCortoCircuitoTracciato/ControlloCortoCircuitoTracciatoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotiva/LocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINE *** //

// Inserire +1
#define NUMERO_STEP_VELOCITA_DECODER 127

#define VELOCITA_CMS_THRESHOLD_SWITCH_DA2A1SENSORI 5

// Tempo massimo (ovvero locomotiva a velocità 1) che passa da quando il sensore rileva la locomotiva più lunga a quando
// la locomotiva oltrepassa completamente il sensore
#define ATTESA_MODALITA_2SENSORI_RETROMARCIA_LOCOMOTIVA 45000

#define INTERVALLO_TEMPO_MINIMO_STEP_DECODER_MAPPING_1SENSORE 40000

// *** CLASSE *** //

class MappingStepLocomotiva {
    // *** DICHIARAZIONE VARIABILI *** //

    LunghezzaTracciatoTipo lunghezzaTracciatoMappingLocomotivaCm1Sensore;
    LunghezzaTracciatoTipo lunghezzaTracciatoMappingLocomotivaCm2Sensori;

    // *** DICHIARAZIONE METODI *** //

    static bool isMappingCompleto(StepVelocitaDecoderTipo stepIniziale, StepVelocitaDecoderTipo stepFinale);

   public:
    void inizializzaLunghezzeTracciatoMapping();
    void inizializzaModalitaTuttiStep();
    void inizializzaMappingVelocitaSingoloStep();
    void inizializzaMappingVelocitaIntervalloStep();
    void mappoVelocitaLoco(LocomotivaAbstract *locomotiva, byte numeroSensoreIr1, byte numeroSensoreIr2,
                           StepVelocitaDecoderTipo stepIniziale, StepVelocitaDecoderTipo stepFinale,
                           bool modalita2SensoriUtente, InvioStatoTracciatoAbstract invioTracciatoStatoLocomotive,
                           ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuito,
                           ProtocolloDccCoreAbstract &protocolloDccCore) const;
};

// *** DICHIARAZIONE VARIABILI *** //

extern MappingStepLocomotiva mappingStepLocomotiva;

#endif
