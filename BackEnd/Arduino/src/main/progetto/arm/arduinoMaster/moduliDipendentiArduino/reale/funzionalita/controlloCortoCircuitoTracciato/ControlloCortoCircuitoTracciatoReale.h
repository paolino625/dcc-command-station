#ifndef DCC_COMMAND_STATION_CONTROLLOCORTOCIRCUITOTRACCIATOREALE_H
#define DCC_COMMAND_STATION_CONTROLLOCORTOCIRCUITOTRACCIATOREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/controlloCortoCircuitoTracciato/ControlloCortoCircuitoTracciatoAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuitoTracciatoReale;

#endif
