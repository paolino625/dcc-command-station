// *** INCUDE *** //

#include "Usb.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/ledIntegrato/LedIntegratoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/usb/nomeUsb/NomeUsb.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/usb/pathFile/PathFile.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/riproduzioneAudio/core/RiproduzioneAudioCore.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/scambi/ScambiRelayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/vagoni/vagoni/VagoniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/DisplayTempiLimiteScritta.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

void Usb::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo USB... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);

    // Fornisco corrente alla USB
    pinMode(PA_15, OUTPUT);
    digitalWrite(PA_15, HIGH);

    ledIntegratoReale.accendiBlu();

    bool inizializzazioneOk;
    for (int i = 0; i < NUMERO_TENTATIVI_INIZIALIZZAZIONE_USB; i++) {
        // Aspetto un po' prima di riprovare
        delay.milliseconds(10);
        inizializzazioneOk = msd.connect();

        if (inizializzazioneOk) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Tentativo n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(i + 1).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " riuscito",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            break;
        }
        if (i == NUMERO_TENTATIVI_INIZIALIZZAZIONE_USB - 1) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non riesco ad inizializzare USB dopo ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(i + 1).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, " tentativi!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            displayReale.errori.usb.inizializzazione();
            programmaReale.ferma(false);
        }
    }

    int err = usbCore.mount(&msd);
    if (err) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non riesco a montare USB!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(err).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo USB... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Usb::resetVariabiliFlagTriggerAggiornamentoFile() {
    fileScambiDaAggiornare = false;
    fileLocomotiveDaAggiornare = false;
    fileConvogliDaAggiornare = false;
    fileUtentiDaAggiornare = false;
}

void Usb::aggiornaFile() {
    if (fileScambiDaAggiornare) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file scambi... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        scriveInfoScambi();
        fileScambiDaAggiornare = false;
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file scambi... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    if (fileLocomotiveDaAggiornare) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file info locomotive... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        scriveInfoLocomotive();
        // stampaInfo();
        fileLocomotiveDaAggiornare = false;
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file info locomotive... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    if (fileConvogliDaAggiornare) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file info convogli...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        scriveInfoConvogli();
        // stampaInfo();
        fileConvogliDaAggiornare = false;
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file info convogli... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    if (fileUtentiDaAggiornare) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file utenti... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        scriveInfoUtenti(utentiReali);
        fileUtentiDaAggiornare = false;
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno file utenti... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

// *** APERTURA CHIUSURA FILE ***

void Usb::apreFileTestoLettura(char pathFile[]) {
    pathUsbBuffer = "";
    pathUsbBuffer += "/";
    pathUsbBuffer += getNome();
    pathUsbBuffer += "/";
    pathUsbBuffer += pathFile;

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Apro file in lettura: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
    }

    setFile(fopen(pathUsbBuffer.c_str(), "r+"));
    // Apro il file per lettura
    if (getFile() == nullptr) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Errore apertura file: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, pathFile,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        displayReale.errori.usb.fileInesistente(pathFile);
        delay.milliseconds(TEMPO_LIMITE_SCRITTA_DISPLAY_BREVE);
        programmaReale.ferma(true);
    } else {
        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Apro file in lettura: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
}

// Variabile stampaLogger utile per evitare di stampare log quando si tratta del file di log.
void Usb::apreFileTestoScrittura(char pathFile[], bool cancellaContenuto, bool stampaLogger) {
    pathUsbBuffer = "";
    pathUsbBuffer += "/";
    pathUsbBuffer += getNome();
    pathUsbBuffer += "/";
    pathUsbBuffer += pathFile;

    if (stampaLogger) {
        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Apro file in scrittura: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
        logger.cambiaTabulazione(1);
    }

    if (cancellaContenuto) {
        if (stampaLogger) {
            if (DEBUGGING_USB) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Modalità cancellazione contenuto... ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
        }
        setFile(fopen(pathUsbBuffer.c_str(), "wb"));
    } else {
        if (stampaLogger) {
            if (DEBUGGING_USB) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Modalità aggiunta contenuto... ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
        }
        setFile(fopen(pathUsbBuffer.c_str(), "a"));
    }

    if (getFile() == nullptr) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non riesco ad aprire il file ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, pathFile,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        programmaReale.ferma(true);
    }
    if (stampaLogger) {
        logger.cambiaTabulazione(-1);
        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Apro file in scrittura: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
}

void Usb::chiudeFile(bool stampaLogger) {
    if (stampaLogger) {
        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Chiudo file ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
    fflush(stdout);
    int err = fclose(getFile());
    if (err < 0) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Errore: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, strerror(errno),
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        programmaReale.ferma(true);
    } else {
        if (stampaLogger) {
            if (DEBUGGING_USB) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Chiudo file ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        }
    }
}

void Usb::apreLeggeChiudeFileTesto(char pathFile[], char *localBuffer, unsigned long numeroCaratteriBuffer) {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Apro, leggo e chiudo file testo... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
    logger.cambiaTabulazione(1);
    // Apro il file in lettura
    apreFileTestoLettura(pathFile);

    // Copio il contenuto del file in localBuffer
    leggeFileTesto(localBuffer, numeroCaratteriBuffer);

    // Chiudo file
    chiudeFile(true);

    logger.cambiaTabulazione(-1);
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Apro, leggo e chiudo file testo... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void Usb::apreFileAudioLettura(char pathFile[]) {
    pathUsbBuffer = "";
    pathUsbBuffer += "/";
    pathUsbBuffer += getNome();
    pathUsbBuffer += "/";
    pathUsbBuffer += pathFile;

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Apro file ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
    }

    // Usiamo rb perché non si tratta di un file di testo
    setFile(fopen(pathUsbBuffer.c_str(), "rb"));
    // Apro il file per lettura

    if (getFile() == nullptr) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Errore apertura file: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, pathFile,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);

        displayReale.errori.usb.fileInesistente(pathFile);
        delay.milliseconds(TEMPO_LIMITE_SCRITTA_DISPLAY_BREVE);
        programmaReale.ferma(true);
    } else {
        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Apro file ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
}

void Usb::apreFileAudio(char pathFile[NUMERO_CARATTERI_PATH_FILE]) {
    String pathUsbBuffer;
    pathUsbBuffer = "";
    pathUsbBuffer += "/";
    pathUsbBuffer += getNome();
    pathUsbBuffer += "/";
    pathUsbBuffer += pathFile;

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Apro file audio... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
    }

    riproduzioneAudioCoreReale.fileAudio = fopen(pathUsbBuffer.c_str(), "rb");

    // Copio il pathUsb del file nella variabile fileCorrentementeAperto. Utile così da evitare di
    // stampare avviso di fine lettura file quando si tratta del file di silenzio.
    strcpy(riproduzioneAudioCoreReale.fileCorrentementeAperto, pathUsbBuffer.c_str());

    if (riproduzioneAudioCoreReale.fileAudio == nullptr) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Errore apertura file: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, strerror(errno),
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        programmaReale.ferma(true);
    }

#if STAMPO_HEADER_FILE_AUDIO
    stampo("Leggo audio header...\n");
#endif

    struct wav_header_t {
        char chunkID[4];              //"RIFF" = 0x46464952
        unsigned long chunkSize;      // 28 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes] +
                                      // sum(sizeof(chunk.ID) + sizeof(chunk.size) + chunk.size)
        char format[4];               //"WAVE" = 0x45564157
        char subchunk1ID[4];          //"fmt " = 0x20746D66
        unsigned long subchunk1Size;  // 16 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes]
        unsigned short audioFormat;
        unsigned short numChannels;
        unsigned long sampleRate;
        unsigned long byteRate;
        unsigned short blockAlign;
        unsigned short bitsPerSample;
    };

    wav_header_t header;
    fread(&header, sizeof(header), 1, riproduzioneAudioCoreReale.fileAudio);

#if STAMPO_HEADER_FILE_AUDIO
    char msg[64] = {0};
    Serial.println(F("Letto WAV File Header:"));

    snprintf(msg, sizeof(msg), "File Type: %s", header.chunkID);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "File Size: %ld", header.chunkSize);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "WAV Marker: %s", header.format);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Format Name: %s", header.subchunk1ID);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Format Length: %ld", header.subchunk1Size);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Format Type: %hd", header.audioFormat);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Number of Channels: %hd", header.numChannels);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Sample Rate: %ld", header.sampleRate);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Sample Rate * Bits/Sample * Channels / 8: %ld", header.byteRate);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Bits per Sample * Channels / 8: %hd", header.blockAlign);
    Serial.println(msg);
    snprintf(msg, sizeof(msg), "Bits per Sample: %hd", header.bitsPerSample);
    Serial.println(msg);
#endif

    // Find the data section of the WAV file.
    struct chunk_t {
        char ID[4];
        unsigned long size;
    };

    chunk_t chunk;
#if STAMPO_HEADER_FILE_AUDIO
    snprintf(msg, sizeof(msg),
             "ID\t"
             "size");
    Serial.println(msg);
#endif
    // Find data chunk.

    while (true) {
        fread(&chunk, sizeof(chunk), 1, riproduzioneAudioCoreReale.fileAudio);
#if STAMPO_HEADER_FILE_AUDIO
        snprintf(msg, sizeof(msg),
                 "%c%c%c%c\t"
                 "%li",
                 chunk.ID[0], chunk.ID[1], chunk.ID[2], chunk.ID[3], chunk.size);
        stampoESalvoLog(msg);
        stampoESalvoLog("\n");

#endif
        if (*(unsigned int *)&chunk.ID == 0x61746164) break;
        // Skip chunk data bytes.
        fseek(getFile(), chunk.size, SEEK_CUR);
    }

    // Determine number of samples.
    riproduzioneAudioCoreReale.sampleSize = header.bitsPerSample / 8;
    riproduzioneAudioCoreReale.samplesCount = chunk.size * 8 / header.bitsPerSample;
#if STAMPO_HEADER_FILE_AUDIO
    snprintf(msg, sizeof(msg), "Sample size = %i", sampleSize);
    stampo(msg);
    stampo("\n");

    snprintf(msg, sizeof(msg), "Samples count = %i", samplesCount);
    stampo(msg);
    stampo("\n");

    stampo("Frequenza file audio: ");
    bufferConversioneNumeroInStringa = std::to_string(header.sampleRate);
    stampo(bufferConversioneNumeroInStringa.c_str());
    stampo("\n");
#endif

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Apro file audio... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pathUsbBuffer.c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

// *** LETTURA FILE ***

void Usb::leggeFileTesto(char *stringa, unsigned int numeroCaratteriBuffer) {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Leggo file testo... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    inizializzoStringa(stringa, numeroCaratteriBuffer);

    int data;
    unsigned int indice = 0;
    bool outOfBuffer = false;

    while ((data = fgetc(getFile())) != EOF) {
        // Se c'è spazio nel buffer salvo il carattere
        if (!outOfBuffer) {
            stringa[indice] = (char)data;
        }
        // Se scrivo oltre alla zona di memoria del buffer Arduino va in crash.
        // Non voglio che questo succeda, quindi stampo errore e fermo il
        // programma.
        if (indice == numeroCaratteriBuffer) {
            outOfBuffer = true;
        }
        indice++;
    }

    if (outOfBuffer) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Buffer riservato a lettura file non è sufficiente.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Byte da scrivere: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(indice).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Byte allocati: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(numeroCaratteriBuffer).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Allocare più memoria!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    } else {
        stringa[indice] = '\0';
        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Leggo file testo... OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
}

void Usb::leggeInfoScambi() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo stato scambi...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    logger.cambiaTabulazione(1);

    apreLeggeChiudeFileTesto(pathFileScambi, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

    // Deserializza la stringa JSON in un oggetto JSON
    JsonDocument documentoJson;

    // Deserializzo Json
    deserializzoJson(documentoJson, stringaJsonUsbBuffer, true);

    JsonArray scambiArray = documentoJson.as<JsonArray>();
    int numeroScambi = (int)scambiArray.size();

    // Se il numero degli scambi letti non è uguale al numero degli scambi che ho allocato in memoria, lancio un errore.
    if (numeroScambi != NUMERO_SCAMBI - 1) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Numero scambi letti da file diverso da quello impostato nella Macro.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Numero scambi letti: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(numeroScambi).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Numero scambi impostate con Macro: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(NUMERO_SCAMBI - 1).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

        displayReale.errori.usb.numeroLocomotiveNonCombacia();
        programmaReale.ferma(true);
    }

    for (int i = 0; i < numeroScambi; i++) {
        JsonObject scambioJson = scambiArray[i];
        // Noi iniziamo a salvare gli scambi a partire dal primo posto. Quindi
        // i+1.
        byte indiceScambio = i + 1;

        // Copiamo i dati dalla struttura JSON dell'utente alla nostra struct

        ScambioRelayAbstract *scambioRelay = scambiRelay.getScambio(indiceScambio);

        if (strcmp(scambioJson["stato"], "DESTRA") == 0) {
            scambioRelay->setDestra();
        } else if (strcmp(scambioJson["stato"], "SINISTRA") == 0) {
            scambioRelay->setSinistra();
        } else {
            scambioRelay->setPosizioneNonConosciuta();
        }
    }

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo stato scambi... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Usb::leggeInfoLocomotive() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo info locomotive da USB...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    logger.cambiaTabulazione(1);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(NUMERO_LOCOMOTIVE - 1).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " locomotive...",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(1);

    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo locomotiva n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(i).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " ... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
        logger.cambiaTabulazione(1);

        int idLocomotiva = i;

        char pathCompletoFileLocomotiva[NUMERO_CARATTERI_PATH_FILE];
        strcpy(pathCompletoFileLocomotiva, pathParzialeFileLocomotiva);

        // Converto il numero in una stringa
        char numeroLocomotivaStringa[3];
        sprintf(numeroLocomotivaStringa, "%d", idLocomotiva);

        // Concateno il numero al path parziale e aggiungo l'estensione .json
        strcat(pathCompletoFileLocomotiva, numeroLocomotivaStringa);
        strcat(pathCompletoFileLocomotiva, ".json");

        apreLeggeChiudeFileTesto(pathCompletoFileLocomotiva, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

        // Deserializza la stringa JSON in un oggetto JSON
        JsonDocument locomotivaJson;
        deserializzoJson(locomotivaJson, stringaJsonUsbBuffer, true);

        // Copiamo i dati dalla struttura JSON della locomotiva alla nostra
        // struct
        LocomotivaAbstract *locomotiva = locomotiveReali.getLocomotiva(idLocomotiva, true);

        locomotiva->setId(locomotivaJson["id"]);
        locomotiva->setIndirizzoDecoderDcc(locomotivaJson["indirizzoDecoderDCC"]);

        char localBuffer[30];

        strcpy(localBuffer, locomotivaJson["nome"]);
        locomotiva->setNome(localBuffer);

        strcpy(localBuffer, locomotivaJson["codice"]);
        locomotiva->setCodice(localBuffer);

        strcpy(localBuffer, locomotivaJson["tipologia"]);
        locomotiva->setTipologia(getEnumerazioneTipologiaRotabileDaStringa(localBuffer));

        strcpy(localBuffer, locomotivaJson["categoria"]);
        locomotiva->setCategoria(getEnumerazioneCategoriaRotabileDaStringa(localBuffer));

        strcpy(localBuffer, locomotivaJson["livrea"]);
        locomotiva->setLivrea(getEnumerazioneLivreaRotabileDaStringa(localBuffer));

        strcpy(localBuffer, locomotivaJson["produttore"]);
        locomotiva->setProduttore(getEnumerazioneProduttoreRotabileDaStringa(localBuffer));

        strcpy(localBuffer, locomotivaJson["numeroModello"]);
        locomotiva->setNumeroModello(localBuffer);

        locomotiva->setLunghezzaCm(locomotivaJson["lunghezzaCm"]);
        locomotiva->setDirezioneInvertita(locomotivaJson["direzioneInvertita"]);

        locomotiva->setDirezioneAvantiDisplay(locomotivaJson["direzioneAvantiDisplay"]);

        locomotiva->setTempoFunzionamentoUltimaManutenzione(locomotivaJson["tempoFunzionamentoUltimaManutenzione"]);
        locomotiva->setTempoFunzionamentoTotale(locomotivaJson["tempoFunzionamentoTotale"]);
        locomotiva->setVelocitaMassimaKmH(locomotivaJson["velocitaMassimaKmH"]);

        for (int r = 0; r < NUMERO_STEP_VELOCITA_DISPLAY_KMH; r++) {
            locomotiva->setMappingVelocitaStepAvanti(r, locomotivaJson["mappingVelocitaStepAvanti"][r]);
            locomotiva->setMappingVelocitaStepIndietro(r, locomotivaJson["mappingVelocitaStepIndietro"][r]);
        }

        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo locomotiva n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(i).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " ... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(NUMERO_LOCOMOTIVE - 1).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " locomotive...",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(-1);

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo info locomotive da USB... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Usb::leggeInfoVagoni() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo info vagoni da USB...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    logger.cambiaTabulazione(1);

    apreLeggeChiudeFileTesto(pathFileVagoni, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

    // Deserializza la stringa JSON in un oggetto JSON

    JsonDocument documentoJson;

    // Deserializzo Json
    deserializzoJson(documentoJson, stringaJsonUsbBuffer, true);

    JsonArray vagoniArray = documentoJson.as<JsonArray>();
    int numeroVagoni = (int)vagoniArray.size();

    if (numeroVagoni != NUMERO_VAGONI - 1) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Numero vagoni letti da file diverso da quello impostato nella Macro.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }

    for (int i = 0; i < numeroVagoni; i++) {
        JsonObject vagoneJsonObject = vagoniArray[i];
        // Noi iniziamo a salvare i vagoni a partire dal primo posto. Quindi
        // i+1.
        byte indiceVagone = i + 1;

        VagoneAbstract *vagone = vagoniReali.getVagone(indiceVagone);

        // Copiamo i dati dalla struttura JSON del vagone alla nostra struct
        vagone->setId(vagoneJsonObject["id"]);

        char localBuffer[25];

        strcpy(localBuffer, vagoneJsonObject["nome"]);
        vagone->setNome(localBuffer);

        strcpy(localBuffer, vagoneJsonObject["codice"]);
        vagone->setCodice(localBuffer);

        strcpy(localBuffer, vagoneJsonObject["numeroModello"]);
        vagone->setNumeroModello(localBuffer);

        strcpy(localBuffer, vagoneJsonObject["tipologia"]);
        vagone->setTipologia(getEnumerazioneTipologiaRotabileDaStringa(localBuffer));

        // Solo i vagoni passeggeri contengono la sezione categoria
        if (vagoneJsonObject.containsKey("categoria")) {
            strcpy(localBuffer, vagoneJsonObject["categoria"]);
            vagone->setCategoria(getEnumerazioneCategoriaRotabileDaStringa(localBuffer));
        }

        strcpy(localBuffer, vagoneJsonObject["livrea"]);
        vagone->setLivrea(getEnumerazioneLivreaRotabileDaStringa(localBuffer));

        strcpy(localBuffer, vagoneJsonObject["produttore"]);
        vagone->setProduttore(getEnumerazioneProduttoreRotabileDaStringa(localBuffer));

        vagone->setLunghezzaCm(vagoneJsonObject["lunghezzaCm"]);

        vagone->setLuciAccese(vagoneJsonObject["illuminazione"]);
    }
    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo Info vagoni da USB... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Usb::leggeInfoConvogli() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo Info convogli da USB...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    logger.cambiaTabulazione(1);
    apreLeggeChiudeFileTesto(pathFileConvogli, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

    // Deserializza la stringa JSON in un oggetto JSON
    JsonDocument documentoJson;

    // Deserializzo Json
    deserializzoJson(documentoJson, stringaJsonUsbBuffer, true);

    JsonArray convogliArray = documentoJson.as<JsonArray>();
    int numeroConvogli = (int)convogliArray.size();

    if (numeroConvogli > NUMERO_CONVOGLI) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Nel file sono salvati più del numero massimo di convogli.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Numero convogli letti da file: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(numeroConvogli).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Numero convogli massimi (stesso numero delle locomotive): ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(NUMERO_CONVOGLI - 1).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        programmaReale.ferma(true);
    }

    for (int i = 0; i < numeroConvogli; i++) {
        JsonObject convoglioJson = convogliArray[i];
        // Noi iniziamo a salvare i convogli a partire dal primo posto. Quindi
        // i+1.
        byte indiceConvoglio = i + 1;

        // Copiamo i dati
        ConvoglioAbstractReale *convoglio = convogliReali.getConvoglio(indiceConvoglio, false);

        convoglio->setId(convoglioJson["id"]);
        convoglio->setNome(convoglioJson["nome"]);
        convoglio->setIdLocomotiva1(convoglioJson["idLocomotiva1"]);
        convoglio->setIdLocomotiva2(convoglioJson["idLocomotiva2"]);

        convoglio->setPresenteSulTracciato(convoglioJson["convoglioSulTracciato"]);

        char localBuffer[25];
        strcpy(localBuffer, convoglioJson["differenzaVelocitaLocomotivaTrazioneSpinta"]);
        convoglio->setDifferenzaVelocitaLocomotivaTrazioneSpinta(
            getEnumerazioneDifferenzaVelocitaLocomotivaTrazioneSpintaDaStringa(localBuffer));

        convoglio->setDifferenzaStepVelocitaTraLocomotive(
            convoglioJson["differenzaStepVelocitaTraLocomotiveConvoglio"]);

        JsonArray idVagoniArray = convoglioJson["idVagoni"].as<JsonArray>();

        for (int r = 0; r < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO - 1; r++) {
            byte indiceIdVagoneLocale = r + 1;
            convoglio->setVagoneId(indiceIdVagoneLocale, idVagoniArray[r]);
        }

        // *** AUTOPILOT *** //

        JsonObject autopilotJson = convoglioJson["autopilot"].as<JsonObject>();

        // ** POSIZIONE CONVOGLIO ** //

        JsonObject posizioneConvoglioJson = autopilotJson["posizioneConvoglio"].as<JsonObject>();

        // Salvo validità
        char localBuffer2[25];
        strcpy(localBuffer2, posizioneConvoglioJson["validita"]);
        convoglio->autopilotConvoglio->posizioneConvoglio.validita =
            getEnumerazionePosizioneConvoglioDaStringa(localBuffer2);

        // Salvo direzione prima locomotiva destra
        convoglio->autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra =
            posizioneConvoglioJson["direzionePrimaLocomotivaDestra"];

        // Salvo direzione seconda locomotiva destra
        JsonObject idSezioneCorrenteOccupataJson = posizioneConvoglioJson["idSezioneCorrenteOccupata"].as<JsonObject>();
        IdSezioneTipo idSezioneCorrenteOccupata = IdSezioneTipo();
        idSezioneCorrenteOccupata.tipologia = idSezioneCorrenteOccupataJson["tipologia"];
        idSezioneCorrenteOccupata.id = idSezioneCorrenteOccupataJson["id"];
        idSezioneCorrenteOccupata.specifica = idSezioneCorrenteOccupataJson["specifica"];

        convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = idSezioneCorrenteOccupata;
    }
    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo info convogli da USB... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Usb::leggeInfoUtenti() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo info utenti da USB...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    logger.cambiaTabulazione(1);

    apreLeggeChiudeFileTesto(pathFileUtenti, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

    // Deserializza la stringa JSON in un oggetto JSON
    JsonDocument documentoJson;

    // Deserializzo Json
    deserializzoJson(documentoJson, stringaJsonUsbBuffer, true);

    JsonArray utentiArray = documentoJson.as<JsonArray>();
    int numeroUtenti = (int)utentiArray.size();

    // Se il numero degli utenti letti non è uguale al numero di utenti che ho
    // allocato in memoria, lancio un errore.
    if (numeroUtenti != NUMERO_UTENTI - 1) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Numero utenti letti da file diverso da quello impostato nella Macro.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Numero utenti letti: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(numeroUtenti).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Numero utenti impostate con Macro: ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(NUMERO_UTENTI - 1).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

        displayReale.errori.usb.numeroLocomotiveNonCombacia();
        programmaReale.ferma(true);
    }

    for (int i = 0; i < numeroUtenti; i++) {
        JsonObject utenteJson = utentiArray[i];
        // Noi iniziamo a salvare gli utenti a partire dal primo posto. Quindi
        // i+1.
        byte indiceUtente = i + 1;

        // Copiamo i dati dalla struttura JSON dell'utente alla nostra struct

        strlcpy(utentiReali.lista[indiceUtente].nome, utenteJson["nome"], sizeof(utentiReali.lista[indiceUtente].nome));

        strlcpy(utentiReali.lista[indiceUtente].sesso, utenteJson["sesso"],
                sizeof(utentiReali.lista[indiceUtente].sesso));

        strlcpy(utentiReali.lista[indiceUtente].tipologiaUtente, utenteJson["tipologiaUtente"],
                sizeof(utentiReali.lista[indiceUtente].tipologiaUtente));

        utentiReali.lista[indiceUtente].numeroImpronteSalvate = utenteJson["numeroImpronteSalvate"];

        for (int r = 0; r < utenteJson["numeroImpronteSalvate"]; r++) {
            utentiReali.lista[indiceUtente].idImpronte[r] = utenteJson["idImpronte"][r];
        }

        utentiReali.lista[indiceUtente].password = utenteJson["password"];
    }
    logger.cambiaTabulazione(-1);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Leggo info utenti da USB... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Usb::leggeFile() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Leggo file da USB...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);

    leggeInfoLocomotive();
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, locomotiveReali.toString(),
                               InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true, true);
    }

    leggeInfoConvogli();
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, convogliReali.toString(),
                               InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true, true);
    }

    leggeInfoVagoni();
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, vagoniReali.toString(),
                               InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true, true);
    }

    leggeInfoScambi();
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, scambiRelay.toString(),
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
    }

    leggeInfoUtenti();
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, utentiReali.toString(),
                               InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true, true);
    }

    logger.cambiaTabulazione(-1);
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Leggo file da USB... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta
Usb::getEnumerazioneDifferenzaVelocitaLocomotivaTrazioneSpintaDaStringa(char *stringa) {
    if (strcmp(stringa, "NESSUNA_DIFFERENZA") == 0) {
        return NESSUNA_DIFFERENZA;
    } else if (strcmp(stringa, "SPINTA_PIU_VELOCE") == 0) {
        return SPINTA_PIU_VELOCE;
    } else if (strcmp(stringa, "TRAZIONE_PIU_VELOCE") == 0) {
        return TRAZIONE_PIU_VELOCE;
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Stringa non valida.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

ValiditaPosizioneConvoglio Usb::getEnumerazionePosizioneConvoglioDaStringa(char *stringa) {
    if (strcmp(stringa, "POSIZIONE_VALIDA") == 0) {
        return POSIZIONE_VALIDA;
    } else if (strcmp(stringa, "POSIZIONE_NON_VALIDA") == 0) {
        return POSIZIONE_NON_VALIDA;
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Stringa non valida.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

// *** SCRITTURA FILE ***

void Usb::scriveInfoScambi() {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scrivo stato scambi...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    byte dimensioneArray = NUMERO_SCAMBI - 1;

    JsonDocument documentoJson;

    JsonArray scambiJson = documentoJson.to<JsonArray>();
    for (int i = 0; i < dimensioneArray; i++) {
        // Noi partiamo da 1 per le nostre struct
        byte indiceArrayLocale = i + 1;

        JsonObject scambioJson = scambiJson.add<JsonObject>();

        ScambioRelayAbstract *scambioRelay = scambiRelay.getScambio(indiceArrayLocale);

        scambioJson["id"] = indiceArrayLocale;
        if (scambioRelay->isDestra()) {
            scambioJson["stato"] = "DESTRA";
        } else if (scambioRelay->isSinistra()) {
            scambioJson["stato"] = "SINISTRA";
        } else {
            scambioJson["stato"] = "POSIZIONE_NON_CONOSCIUTA";
        }
    }

    apreFileTestoScrittura(pathFileScambi, true, true);
    serializzoJson(documentoJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER, true);
    scrivoFileTesto(stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

    chiudeFile(true);

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scrivo stato scambi... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void Usb::scriveInfoLocomotive() {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo Info Locomotive su USB...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        int idLocomotiva = i;
        JsonDocument locomotivaJson;
        LocomotivaAbstract *locomotiva = locomotiveReali.getLocomotiva(idLocomotiva, true);

        locomotivaJson["id"] = locomotiva->getId();
        locomotivaJson["indirizzoDecoderDCC"] = locomotiva->getIndirizzoDecoderDcc();
        locomotivaJson["nome"] = locomotiva->getNome();
        locomotivaJson["codice"] = locomotiva->getCodice();

        char localBuffer[25];

        setStringaEnumerazioneTipologiaRotabile(localBuffer, locomotiva->getTipologia());
        locomotivaJson["tipologia"] = localBuffer;

        setStringaEnumerazioneCategoriaRotabile(localBuffer, locomotiva->getCategoria());
        locomotivaJson["categoria"] = localBuffer;

        setStringaEnumerazioneLivreaRotabile(localBuffer, locomotiva->getLivrea());
        locomotivaJson["livrea"] = localBuffer;

        setStringaEnumerazioneProduttoreRotabile(localBuffer, locomotiva->getProduttore());
        locomotivaJson["produttore"] = localBuffer;

        locomotivaJson["numeroModello"] = locomotiva->getNumeroModello();
        locomotivaJson["lunghezzaCm"] = locomotiva->getLunghezzaCm();
        locomotivaJson["direzioneInvertita"] = locomotiva->getDirezioneInvertita();
        locomotivaJson["direzioneAvantiDisplay"] = locomotiva->getDirezioneAvantiDisplay();
        locomotivaJson["tempoFunzionamentoUltimaManutenzione"] = locomotiva->getTempoFunzionamentoUltimaManutenzione();
        locomotivaJson["tempoFunzionamentoTotale"] = locomotiva->getTempoFunzionamentoTotale();
        locomotivaJson["velocitaMassimaKmH"] = locomotiva->getVelocitaMassimaKmH();

        for (int r = 0; r < NUMERO_STEP_VELOCITA_DISPLAY_KMH; r++) {
            locomotivaJson["mappingVelocitaStepAvanti"][r] = locomotiva->getMappingVelocitaStepAvanti(r);
            locomotivaJson["mappingVelocitaStepIndietro"][r] = locomotiva->getMappingVelocitaStepIndietro(r);
        }

        char pathCompletoFileLocomotiva[NUMERO_CARATTERI_PATH_FILE];
        strcpy(pathCompletoFileLocomotiva, pathParzialeFileLocomotiva);

        // Converto il numero in una stringa
        char numeroLocomotivaStringa[3];
        sprintf(numeroLocomotivaStringa, "%d", idLocomotiva);

        // Concateno il numero al path parziale e aggiungo l'estensione .json
        strcat(pathCompletoFileLocomotiva, numeroLocomotivaStringa);
        strcat(pathCompletoFileLocomotiva, ".json");

        apreFileTestoScrittura(pathCompletoFileLocomotiva, true, true);

        // Serializzo il JSON in una stringa
        serializzoJson(locomotivaJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER, true);
        scrivoFileTesto(stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

        chiudeFile(true);
    }

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo Info Locomotive su USB... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void Usb::scriveInfoConvogli() {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo Info Convogli su USB...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    byte dimensioneArray = NUMERO_CONVOGLI - 1;

    JsonDocument documentoJson;

    JsonArray convogliJson = documentoJson.to<JsonArray>();

    for (int i = 0; i < dimensioneArray; i++) {
        byte indiceArrayLocale = i + 1;

        if (DEBUGGING_USB) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(indiceArrayLocale).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        }

        // Se il convoglio non è in uso non devo scriverlo sul file
        if (convogliReali.getConvoglio(indiceArrayLocale, false)->isInUso()) {
            // Noi partiamo da 1 per le nostre struct

            JsonObject convoglioJson = convogliJson.add<JsonObject>();

            ConvoglioAbstractReale *convoglio = convogliReali.getConvoglio(indiceArrayLocale, false);

            convoglioJson["id"] = convoglio->getId();
            convoglioJson["nome"] = convoglio->getNome();
            convoglioJson["idLocomotiva1"] = convoglio->getIdLocomotiva1();
            convoglioJson["idLocomotiva2"] = convoglio->getIdLocomotiva2();
            convoglioJson["convoglioSulTracciato"] = convoglio->isPresenteSulTracciato();

            char localBuffer[25];

            setStringaEnumerazioneDifferenzaVelocitaLocomotiva(
                localBuffer, convoglio->getTipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta());
            convoglioJson["differenzaVelocitaLocomotivaTrazioneSpinta"] = localBuffer;

            convoglioJson["differenzaStepVelocitaTraLocomotiveConvoglio"] =
                convoglio->getDifferenzaStepVelocitaTraLocomotive();

            for (int r = 0; r < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO - 1; r++) {
                IdVagoneTipo indiceIdVagoneLocale = r + 1;
                IdVagoneTipo idVagone = convoglio->getIdVagone(indiceIdVagoneLocale);

                convoglioJson["idVagoni"][r] = idVagone;
            }

            // *** AUTOPILOT *** //

            JsonObject autopilotJson = convoglioJson["autopilot"].to<JsonObject>();

            // ** POSIZIONE CONVOGLIO ** //

            JsonObject posizioneConvoglioJson = autopilotJson["posizioneConvoglio"].to<JsonObject>();

            char localBuffer2[25];
            setStringaEnumerazioneValiditaPosizioneConvoglio(
                localBuffer2, convoglio->autopilotConvoglio->posizioneConvoglio.validita);
            posizioneConvoglioJson["validita"] = localBuffer2;

            posizioneConvoglioJson["direzionePrimaLocomotivaDestra"] =
                convoglio->autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra;

            JsonObject idSezioneCorrenteOccupataJsonObject =
                posizioneConvoglioJson["idSezioneCorrenteOccupata"].to<JsonObject>();
            idSezioneCorrenteOccupataJsonObject["tipologia"] =
                convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata.tipologia;
            idSezioneCorrenteOccupataJsonObject["id"] =
                convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata.id;
            idSezioneCorrenteOccupataJsonObject["specifica"] =
                convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata.specifica;
        }
    }

    apreFileTestoScrittura(pathFileConvogli, true, true);
    serializzoJson(documentoJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER, true);
    scrivoFileTesto(stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);
    chiudeFile(true);

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo Info Convogli su USB... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void Usb::scriveInfoUtenti(UtentiAbstract &utenti) {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo info utenti su USB...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    byte dimensioneArray = NUMERO_UTENTI - 1;

    JsonDocument documentoJson;

    JsonArray datiJson = documentoJson.to<JsonArray>();
    for (int i = 0; i < dimensioneArray; i++) {
        // Noi partiamo da 1 per le nostre struct
        byte indiceArrayLocale = i + 1;

        JsonObject utenteJson = datiJson.add<JsonObject>();

        utenteJson["Nome"] = utenti.lista[indiceArrayLocale].nome;
        utenteJson["Sesso"] = utenti.lista[indiceArrayLocale].sesso;
        utenteJson["TipologiaUtente"] = utenti.lista[indiceArrayLocale].tipologiaUtente;
        utenteJson["NumeroImpronteSalvate"] = utenti.lista[indiceArrayLocale].numeroImpronteSalvate;

        for (int r = 0; r < utenti.lista[indiceArrayLocale].numeroImpronteSalvate; r++) {
            utenteJson["idImpronte"][r] = utenti.lista[indiceArrayLocale].idImpronte[r];
        }

        utenteJson["Password"] = utenti.lista[indiceArrayLocale].password;
    }

    apreFileTestoScrittura(pathFileUtenti, true, true);
    serializzoJson(documentoJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER, true);
    scrivoFileTesto(stringaJsonUsbBuffer, NUMERO_CARATTERI_FILE_JSON_BUFFER);

    chiudeFile(true);

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Scrivo info utenti su USB... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

// *** UTILITY *** //

void Usb::setStringaEnumerazioneDifferenzaVelocitaLocomotiva(
    char *stringaSalvataggio, TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta valore) {
    if (valore == NESSUNA_DIFFERENZA) {
        strcpy(stringaSalvataggio, "NESSUNA_DIFFERENZA");
    } else if (valore == TRAZIONE_PIU_VELOCE) {
        strcpy(stringaSalvataggio, "TRAZIONE_PIU_VELOCE");

    } else if (valore == SPINTA_PIU_VELOCE) {
        strcpy(stringaSalvataggio, "SPINTA_PIU_VELOCE");
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Enumerazione non prevista.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

void Usb::setStringaEnumerazioneValiditaPosizioneConvoglio(char *stringaSalvataggio,
                                                           ValiditaPosizioneConvoglio valore) {
    if (valore == POSIZIONE_VALIDA) {
        strcpy(stringaSalvataggio, "POSIZIONE_VALIDA");
    } else if (valore == POSIZIONE_NON_VALIDA) {
        strcpy(stringaSalvataggio, "POSIZIONE_NON_VALIDA");
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Enumerazione non prevista.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

void Usb::scrivoFileTesto(char *stringa, unsigned int numeroCaratteri) {
    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scrivo file testo... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    // Non posso scrivere fino all'ultimo carattere perché altrimenti crasha alla lettura successiva.
    // Probabilmente perché non c'è spazio per aggiungere l'END OF FILE.
    for (unsigned int i = 0; i < numeroCaratteri - 10; i++) {
        int err = fprintf(getFile(), "%c", stringa[i]);
        if (err < 0) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                                  "Errore: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::CRITICAL, strerror(errno),
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false, true);
        }
    }

    if (DEBUGGING_USB) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scrivo file testo... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void Usb::copioDaUsbA(String stringaSalvataggio) {
    int data;

    while ((data = fgetc(getFile())) != EOF) {
        stringaSalvataggio += (char)data;
    }
}

// *** GETTER *** //

FILE *Usb::getFile() { return file; }

const char *Usb::getNome() { return nomeUsb; }

// *** SETTER *** //

void Usb::setFile(FILE *file) { this->file = file; }

mbed::FATFileSystem usbCore(nomeUsb);

Usb usb(scambiRelayReale, delayReale, loggerReale);