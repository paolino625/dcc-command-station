// *** INCLUDE *** //

#include "StopEmergenzaAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/stopAutopilot/StopAutopilot.h"

// *** DEFINIZIONE METODI *** //

void StopEmergenzaAbstract::aziona(bool fermaAutopilot) {
    invioStatoTracciatoAbstract.invioPacchettoStopEmergenza();

    buzzer.beepOk();

    // Invio il pacchetto prima di stampare sulla Seriale dato che quest'ultima è lenta
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "STOP EMERGENZA",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    // Salvo tempo in uso delle locomotive
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        if (locomotive.esisteLocomotiva(i)) {
            LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(i, true);
            // Se la locomotiva è in movimento, allora salvo il tempo trascorso in uso
            if (locomotiva->getVelocitaImpostata() != 0) {
                locomotiva->fermaTimerParziale();
            }
        }
    }

    // Setto a 0 le velocità dei convogli nelle struct
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, false);
        // Il convoglio può esistere ma può essere vuoto, in questo caso andremmo a modificare la velocità di una
        // locomotiva che non esiste.
        if (convoglio->isInUso()) {
            // Passiamo NON_ATTIVO ma in realtà è indifferente
            convoglio->cambiaVelocita(0, false, statoAutopilot);
        }
    }

    // Se il chiamante di questa funzione ha chiesto di disattivare l'autopilot e l'autopilot è attivo, allora procedo a
    // settare la variabile trigger
    if (fermaAutopilot && statoAutopilot != StatoAutopilot::NON_ATTIVO) {
        stopAutopilot = true;
    }
}