// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/amplificatoreAudio/AmplificatoreAudioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/i2c/I2CReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/wire/WireReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AmplificatoreAudioAbstract amplificatoreAudioReale(programmaReale, I2CReale, wireReale,
                                                   INDIRIZZO_I2C_AMPLIFICATORE_AUDIO_MAX_9744, loggerReale);