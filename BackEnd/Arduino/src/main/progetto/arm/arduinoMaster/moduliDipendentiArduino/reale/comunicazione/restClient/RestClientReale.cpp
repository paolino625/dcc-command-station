// *** INCLUDE *** //

#include "RestClientReale.h"

#include "Arduino.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/connessioneInternet/connessioneEthernet/client/ClientEthernet.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/restClient/enum/TipologiaRichiestaHttpRestClient.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DICHIARAZIONE METODI *** //

void RestClientReale::inviaRichiestaHTTP(TipologiaRichiestaHttpRestClient tipologiaRichiestaHttp, char* indirizzoServer,
                                         int porta, char* endPoint, char* requestBody, char* responseHeader,
                                         char* responseBody) {
    if (DEBUGGING_REST_CLIENT) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio richiesta HTTP",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Indirizzo server: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, indirizzoServer,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Porta: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(porta).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "End point: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, endPoint,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Request body: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, requestBody,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
    }

    char requestHeader[NUMERO_CARATTERI_MASSIMI_REQUEST_HEADER];

    // Inizializzo le stringhe a stringa vuota
    memset(requestHeader, 0, NUMERO_CARATTERI_MASSIMI_REQUEST_HEADER);

    // Mi collego al server
    if (ethernetClientRestClient.connect(indirizzoServer, porta)) {
        if (DEBUGGING_REST_CLIENT) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Connessione al server riuscita",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }

        // Devo comporre il request body prima del request header perché è presenta la lunghezza del body
        // nell'header
        compongoRequestHeader(tipologiaRichiestaHttp, indirizzoServer, endPoint, requestHeader, requestBody);

        if (DEBUGGING_REST_CLIENT_REQUEST) {
            stampoRequest(requestHeader, requestBody);
        }

        // Una volta che ho preparato header e body, invio la richiesta al server
        invioAlServer(requestHeader, requestBody);

        // Leggo la risposta del server
        leggoRispostaDelServer(responseHeader, responseBody);

        if (DEBUGGING_REST_CLIENT_RESPONSE) {
            stampoResponse(responseHeader, responseBody);
        }
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, "Connessione al server FALLITA",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

void RestClientReale::compongoRequestHeader(TipologiaRichiestaHttpRestClient tipologiaRichiestaHttp,
                                            char* indirizzoServer, char* endPoint, char* requestHeader,
                                            char* requestBody) {
    switch (tipologiaRichiestaHttp) {
        case REST_CLIENT_GET:
            strcat(requestHeader, "GET");
            break;
        case REST_CLIENT_POST:
            strcat(requestHeader, "POST");
            break;

        case REST_CLIENT_PUT:
            strcat(requestHeader, "PUT");
            break;
    }
    strcat(requestHeader, " ");
    strcat(requestHeader, endPoint);
    strcat(requestHeader, " ");
    strcat(requestHeader, "HTTP/1.1\r\n");
    strcat(requestHeader, "Host: ");
    strcat(requestHeader, indirizzoServer);
    strcat(requestHeader, "\r\n");
    strcat(requestHeader, "Content-Type: application/json\r\n");
    strcat(requestHeader, "Content-Length: ");
    int lunghezzaBodyInteger = strlen(requestBody);
    char lunghezzaBodyStringa[10];
    itoa(lunghezzaBodyInteger, lunghezzaBodyStringa, 10);
    strcat(requestHeader, lunghezzaBodyStringa);
    strcat(requestHeader, "\r\n");
    strcat(requestHeader, "Connection: close\r\n");
}

void RestClientReale::compongoRequestBody(char* requestBody,
                                          char* requestBodyDaInviare) {  // Copio il requestBodyDaInviare in requestBody
    strcat(requestBody, requestBodyDaInviare);
}

void RestClientReale::invioAlServer(char* requestHeader, char* requestBody) {
    ethernetClientRestClient.print(requestHeader);
    // L'header deve essere separato dal body da una riga vuota
    ethernetClientRestClient.print("\r\n");
    ethernetClientRestClient.print(requestBody);
    ethernetClientRestClient.print("\r\n");
}

void RestClientReale::leggoRispostaDelServer(char* responseHeader, char* responseBody) {
    bool headerLetto = false;
    size_t headerIndex = 0;
    size_t bodyIndex = 0;
    while (ethernetClientRestClient.connected() || ethernetClientRestClient.available()) {
        if (ethernetClientRestClient.available()) {
            String line = ethernetClientRestClient.readStringUntil('\n');

            if (!headerLetto) {
                if (line == "\r") {
                    headerLetto = true;
                } else {
                    if (headerIndex + line.length() < NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER) {
                        strcat(responseHeader, line.c_str());
                        strcat(responseHeader, "\n");
                        headerIndex += line.length();
                    } else {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG,
                                              "Metodo leggoRispostaDelServer dal file RestApi. "
                                              "NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER non sufficiente.",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        programmaReale.ferma(true);
                    }
                }
            } else {
                // Springboot restituisce il body in chunk, quindi dobbiamo gestirli
                if (line.length() > 0) {
                    int chunkSize = strtol(line.c_str(), nullptr, 16);
                    if (chunkSize == 0) {
                        break;  // End of chunks
                    }
                    while (chunkSize > 0) {
                        String chunk = ethernetClientRestClient.readStringUntil('\n');
                        chunk.trim();  // Remove any leading/trailing whitespace
                        if (bodyIndex + chunk.length() < NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY) {
                            strcat(responseBody, chunk.c_str());
                            strcat(responseBody, "\n");
                            bodyIndex += chunk.length();
                            chunkSize -= chunk.length();
                        } else {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG,
                                                  "Metodo leggoRispostaDelServer dal file RestApi. "
                                                  "NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY non sufficiente.",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            break;
                        }
                    }
                }
            }
        }
    }
}

void RestClientReale::stampoRequest(char* requestHeader, char* requestBody) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nHEADER REQUEST: \n",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, requestHeader,
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nBODY REQUEST: \n",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, requestBody,
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
}

void RestClientReale::stampoResponse(char* responseHeader, char* responseBody) {
    // RestClient viene chiamato da loggerReale. Dunque non posso chiamarlo altrimenti ci sarebbe una dipendenza
    // circolare. Ci limitiamo a stampare sul loggerReale in locale per questioni di debugging.

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nHEADER RESPONSE: \n",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, responseHeader,
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nBODY RESPONSE: \n",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, responseBody,
                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
}

// *** DEFINIZIONE VARIABILI *** //

RestClientReale restClientReale = RestClientReale();