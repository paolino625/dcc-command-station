#ifndef DCC_COMMAND_STATION_MODIFICACVABSTRACT_H
#define DCC_COMMAND_STATION_MODIFICACVABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreAbstract.h"

// *** CLASSE *** //

class ModificaCvAbstract {
    // *** VARIABILI *** //

    ProtocolloDccCoreAbstract &protocolloDccCore;

    // *** COSTRUTTORE *** //

   public:
    ModificaCvAbstract(ProtocolloDccCoreAbstract &protocolloDccCore) : protocolloDccCore(protocolloDccCore) {}

    // *** METODI *** //

   public:
    void tempoLimiteUsoKeepAlive(byte moltiplicatoreTempoUnita);
    void indirizzoLocomotiva(int nuovoIndirizzoLocomotiva);
    void accelerazioneLocomotiva(byte valore);
    void decelerazioneLocomotiva(byte valore);
    void resetAccelerazioneDecelerazioneLocomotiva();
};

#endif