// *** INCLUDE *** //

#include "SezioniAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/tracciato/LunghezzaBinari.h"

// *** DEFINIZIONE METODI *** //

void SezioniAbstract::resetLock() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Resetto lock delle sezioni... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < numeroSezioni + 1; i++) {
        sezioni[i]->resetLock();
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Resetto lock delle sezioni...  OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void SezioniAbstract::inizializzaInfoBasilari() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo info basilari delle sezioni... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    // *** INIZIALIZZO INFORMAZIONI BASILARI SEZIONI *** //

    // ** 1° PIANO ** //

    aggiungoSezioniStazioniPrimoPiano();
    aggiungoSezioniIntermediePrimoPiano();
    aggiungoSezioniIntermediePrimoPiano();
    aggiungoSezioniScambiPrimoPiano();
    aggiungoSezioniIncrociPrimoPiano();

    // ** 1° PIANO - 2° PIANO ** //

    aggiungoSezioniIntermedieTraPrimoESecondoPiano();

    // ** 2° PIANO ** //

    aggiungoSezioniIntermedieSecondoPiano();
    aggiungoSezioniScambiSecondoPiano();
    aggiungoSezioniIncrociSecondoPiano();

    // ** 2° PIANO - 3° PIANO ** //

    aggiungoSezioniIntermedieTraSecondoETerzoPiano();

    // ** 3° PIANO ** //

    aggiungoSezioniStazioneTerzoPiano();
    aggiungoSezioniIntermedieTerzoPiano();
    aggiungoSezioniScambiTerzoPiano();
    aggiungoSezioniIncrociTerzoPiano();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo info basilari delle sezioni...  OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void SezioniAbstract::inizializzaPosizioneSensori() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo posizione sensori su sezioni... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    aggiungoSensoriASezioniPrimoPiano();
    aggiungoSensoriASezioniTraPrimoESecondoPiano();
    aggiungoSensoriASezioniSecondoPiano();
    aggiungoSensoriASezioniTraSecondoETerzoPiano();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo posizione sensori su sezioni...  OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

SezioneAbstract* SezioniAbstract::getSezione(IdSezioneTipo idSezione) {
    for (int i = 1; i < numeroSezioni + 1; i++) {
        if (sezioni[i]->getId().equals(idSezione)) {
            return sezioni[i];
        }
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "ID sezione non valido!",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
}

SezioneAbstract* SezioniAbstract::getSezioneFromIndex(int indiceSezione) {
    if (indiceSezione <= numeroSezioni && indiceSezione > 0) {
        return sezioni[indiceSezione];
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "ID sezione non valido!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }
}

SezioneAbstract* SezioniAbstract::getSezioneDelSensore(IdSensorePosizioneTipo idSensore) {
    // Scansiono tutte le sezioni in cerca della sezione che contiene il Sensore
    for (int i = 1; i < numeroSezioni + 1; i++) {
        SezioneAbstract* sezione = getSezioneFromIndex(i);
        if (sezione->getSensore1() == idSensore || sezione->getSensore2() == idSensore) {
            return sezione;
        }
    }
}

void SezioniAbstract::aggiungoSezione(IdSezioneTipo idSezione, LunghezzaTracciatoTipo lunghezzaCm,
                                      TipologiaSezioneCritica tipologiaSezioneCritica, bool inversioneDiPolarita) {
    if (numeroSezioni >= NUMERO_SEZIONI_MASSIMO - 1) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Numero sezioni massimo raggiunto!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }

    numeroSezioni += 1;
    int indiceSezioneDiSezioni = numeroSezioni;

    SezioneAbstract* sezione = new SezioneAbstract(scambiRelay, indiceSezioneDiSezioni, idSezione, lunghezzaCm,
                                                   tipologiaSezioneCritica, inversioneDiPolarita, programma, logger);

    sezioni[indiceSezioneDiSezioni] = sezione;
}

bool SezioniAbstract::esisteSezione(IdSezioneTipo idSezione) {
    for (int i = 1; i < numeroSezioni + 1; i++) {
        if (sezioni[i]->getId().equals(idSezione)) {
            return true;
        }
    }
    return false;
}

bool SezioniAbstract::lock(IdSezioneTipo idSezione, IdConvoglioTipo idConvoglio) {
    bool lockPreso = false;
    if (idSezione.isBinarioStazione() || idSezione.isSezioneIntermedia()) {
        lockPreso = lockSezioneNormale(idConvoglio, idSezione);
    } else if (idSezione.isScambio()) {
        lockPreso = lockScambio(idConvoglio, idSezione);
    } else if (idSezione.isIncrocio()) {
        lockPreso = lockIncrocio(idConvoglio, idSezione);
    }

    return lockPreso;
}

void SezioniAbstract::unlock(IdSezioneTipo idSezione) {
    if (idSezione.isBinarioStazione() || idSezione.isSezioneIntermedia()) {
        unlockSezioneNormale(idSezione);
    } else if (idSezione.isScambio()) {
        unlockScambio(idSezione);
    } else if (idSezione.isIncrocio()) {
        unlockIncrocio(idSezione);
    }
}

// Come sezione normali si intendono le sezioni di stazione e le sezioni intermedie
bool SezioniAbstract::lockSezioneNormale(IdConvoglioTipo idConvoglio, IdSezioneTipo idSezione) {
    // Se la sezione è una sezione normale, basta richiamare il metodo della sezione
    SezioneAbstract* sezione = getSezione(idSezione);

    return sezione->lock(idConvoglio);
}

bool SezioniAbstract::lockScambio(IdConvoglioTipo idConvoglio, IdSezioneTipo idSezione) {
    // Quando proviamo a effettuare il lock di uno scambio, dobbiamo assicurarci di effettuare il lock di entrambe le
    // sezioni associate allo scambio

    // Innanzitutto trovo l'ID della sezione complementare dello scambio
    IdSezioneTipo idSezioneComplementare;
    if (idSezione.specifica == 1) {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 2);
    } else {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 1);
    }

    SezioneAbstract* sezioneScambio1 = getSezione(idSezione);
    SezioneAbstract* sezioneScambio2 = getSezione(idSezioneComplementare);

    // Verifico che le sezioni non siano già bloccate. Non mi aspetto che una delle due sezioni possa essere bloccata e
    // l'altra no, ma per sicurezza controllo entrambe
    if (!sezioneScambio1->isLocked() && !sezioneScambio2->isLocked()) {
        sezioneScambio1->lock(idConvoglio);
        sezioneScambio2->lock(idConvoglio);

        // Se è stato preso il lock, bisogna anche azionare il motore dello scambio
        if (idSezione.isScambio()) {
            if (idSezione.isScambioSinistra()) {
                // Se lo scambio è a sinistra, in base alla codifica scelta, devo inviare l'id dello scambio negativo
                scambiRelay.azionoScambioSeNecessario(-idSezione.id);
            } else {
                // Se lo scambio è a sinistra, in base alla codifica scelta, devo inviare l'id dello scambio positivo
                scambiRelay.azionoScambioSeNecessario(idSezione.id);
            }
        }
        return true;
    } else {
        return false;
    }
}

bool SezioniAbstract::lockIncrocio(IdConvoglioTipo idConvoglio, IdSezioneTipo idSezione) {
    // Quando proviamo a effettuare il lock di un incrocio, dobbiamo assicurarci di effettuare il lock di entrambe le
    // sezioni associate all'incrocio

    // Innanzitutto trovo l'ID della sezione complementare dello scambio
    IdSezioneTipo idSezioneComplementare;
    if (idSezione.specifica == 1) {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 2);
    } else {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 1);
    }

    SezioneAbstract* sezioneIncrocio1 = getSezione(idSezione);
    SezioneAbstract* sezioneIncrocio2 = getSezione(idSezioneComplementare);

    // Verifico che le sezioni non siano già bloccate. Non mi aspetto che una delle due sezioni possa essere bloccata e
    // l'altra no, ma per sicurezza controllo entrambe
    if (!sezioneIncrocio1->isLocked() && !sezioneIncrocio2->isLocked()) {
        sezioneIncrocio1->lock(idConvoglio);
        sezioneIncrocio2->lock(idConvoglio);
        return true;
    } else {
        return false;
    }
}

void SezioniAbstract::unlockSezioneNormale(IdSezioneTipo idSezione) {
    SezioneAbstract* sezione = getSezione(idSezione);
    sezione->unlock();
}

void SezioniAbstract::unlockScambio(IdSezioneTipo idSezione) {
    // Quando proviamo a effettuare l'unlock di uno scambio, dobbiamo assicurarci di effettuare l'unlock di entrambe le
    // sezioni associate allo scambio

    // Innanzitutto trovo l'ID della sezione complementare dello scambio
    IdSezioneTipo idSezioneComplementare;
    if (idSezione.specifica == 1) {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 2);
    } else {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 1);
    }

    SezioneAbstract* sezioneScambio1 = getSezione(idSezione);
    SezioneAbstract* sezioneScambio2 = getSezione(idSezioneComplementare);

    sezioneScambio1->unlock();
    sezioneScambio2->unlock();
}

void SezioniAbstract::unlockIncrocio(IdSezioneTipo idSezione) {
    // Quando proviamo a effettuare l'unlock di un incrocio, dobbiamo assicurarci di effettuare l'unlock di entrambe le
    // sezioni associate all'incrocio

    // Innanzitutto trovo l'ID della sezione complementare dello scambio
    IdSezioneTipo idSezioneComplementare;
    if (idSezione.specifica == 1) {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 2);
    } else {
        idSezioneComplementare = IdSezioneTipo(idSezione.tipologia, idSezione.id, 1);
    }

    SezioneAbstract* sezioneIncrocio1 = getSezione(idSezione);
    SezioneAbstract* sezioneIncrocio2 = getSezione(idSezioneComplementare);

    sezioneIncrocio1->unlock();
    sezioneIncrocio2->unlock();
}

void SezioniAbstract::aggiungoSezioniStazioniPrimoPiano() {
    aggiungoSezioniStazioneCentralePrimoPiano();
    aggiungoSezioniStazioneEstPrimoPiano();
    aggiungoSezioniStazioneOvestPrimoPiano();
    aggiungoSezioniStazioneEstremoEstPrimoPiano();
    aggiungoSezioniStazioneEstremoOvestPrimoPiano();
}

// N.B. La lunghezza delle sezioni non considera i paraurti

void SezioniAbstract::aggiungoSezioniStazioneCentralePrimoPiano() {
    aggiungoSezione(IdSezioneTipo(1, 3, 1), 280, MAI, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 3, 2), 249.1, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 3, 3), 204.1, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 3, 4), 173.2, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 3, 5), 128.1, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 3, 6), 97.2, MAI, false);   // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 3, 7),
                    3.9 + LUNGHEZZA_CURVA_HORNBY_R606_CM + 52.2 + LUNGHEZZA_CURVA_HORNBY_R606_CM + 3.9, MAI,
                    false);  // Misura effettuata
}

void SezioniAbstract::aggiungoSezioniStazioneEstPrimoPiano() {
    aggiungoSezione(IdSezioneTipo(1, 2, 1), 233, MAI, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 2, 2), 219.7, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 2, 3), 201, MAI, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 2, 4), 185.5, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 2, 5), 160.8, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 2, 6), 145.2, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 2, 7), 115 + LUNGHEZZA_CURVA_HORNBY_R606_CM + 3.8, MAI,
                    false);  // Misura effettuata
}

void SezioniAbstract::aggiungoSezioniStazioneOvestPrimoPiano() {
    aggiungoSezione(IdSezioneTipo(1, 4, 1), 212.8, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 4, 2), 201, MAI, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 4, 3), 181.4, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 4, 4), 166, MAI, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 4, 5), 140.9, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 4, 6), 125.4, MAI, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(1, 4, 7), 7.7 + LUNGHEZZA_CURVA_HORNBY_R606_CM + 94.1, MAI,
                    false);  // Misura effettuata
}

void SezioniAbstract::aggiungoSezioniStazioneEstremoEstPrimoPiano() {
    aggiungoSezione(IdSezioneTipo(1, 1, 1), 100, MAI, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(1, 1, 2), 100, MAI, false);  // TODO Misura da effettuare
}

void SezioniAbstract::aggiungoSezioniStazioneEstremoOvestPrimoPiano() {
    aggiungoSezione(IdSezioneTipo(1, 5, 1), 100, MAI, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(1, 5, 2), 100, MAI, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(1, 5, 3), 100, MAI, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(1, 5, 4), 100, MAI, false);  // TODO Misura da effettuare
}

void SezioniAbstract::aggiungoSezioniIntermediePrimoPiano() {
    aggiungoSezione(IdSezioneTipo(2, 1), 20, SEMPRE, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(2, 2), 20, SEMPRE, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(2, 3), 20, SEMPRE, false);  // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(2, 4), 2.7 + (LUNGHEZZA_CURVA_HORNBY_R609_CM * 2) + 29.5,
                    SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 5), 12.5 + (LUNGHEZZA_CURVA_HORNBY_R607_CM * 4) + 62.5,
                    SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 6), LUNGHEZZA_CURVA_HORNBY_R608_CM + LUNGHEZZA_CURVA_HORNBY_R609_CM + 165.4,
                    SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 7), 20, SEMPRE, true);
    aggiungoSezione(IdSezioneTipo(2, 8), 134.7, SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 9), 168.1, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 10), 168.6, SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);   // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 11), 39.4, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 12), 23.7, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 13),
                    37.1 + LUNGHEZZA_CURVA_HORNBY_R606_CM + 26.7 + LUNGHEZZA_CURVA_HORNBY_R606_CM + 37.1, SEMPRE,
                    false);                                                                  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 14), 39.8, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 15), 6.4, SEMPRE, false);                               // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 16), 24.5, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 17), 7.7, SEMPRE, false);                               // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 18), 6.4, SEMPRE, false);                               // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 19), 24.4, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 20), 7.7, SEMPRE, false);                               // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 21), 52.8, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 22), 33.5, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 23), 44.8, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);   // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 24), 237, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 25), 45.2, SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);     // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 26), 48.9, SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);     // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 27), 97.6, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 28), 38.4, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);   // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 29), 112.6, SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 30), 146.1, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 31), 33.1 + (2 * LUNGHEZZA_CURVA_HORNBY_R609_CM) + 3.3,
                    SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 32), 16.9 + (LUNGHEZZA_CURVA_HORNBY_R607_CM * 4) + 41.2,
                    SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 33), LUNGHEZZA_CURVA_HORNBY_R608_CM + LUNGHEZZA_CURVA_HORNBY_R609_CM + 144.2,
                    SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);                                // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 34), 39.8, SEMPRE, false);                             // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 35), 6.6, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 36), 24.5, SEMPRE, false);                             // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 37), 3.9, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 38), 6.6, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 39), 24.7, SEMPRE, false);                             // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 40), 7.7, SEMPRE, false);                              // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 41), 52.9, SEMPRE, false);                             // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 42), 33.5, SEMPRE, false);                             // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 43), 45.6, SOLO_DIREZIONE_CONVOGLIO_SINISTRA, false);  // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 44), 45.1, SOLO_DIREZIONE_CONVOGLIO_DESTRA, false);    // Misura effettuata
    aggiungoSezione(IdSezioneTipo(2, 45), 20, SEMPRE, true);                                // TODO Misura da effettuare
    aggiungoSezione(IdSezioneTipo(2, 46), 20, SEMPRE, false);                               // TODO Misura da effettuare
}

void SezioniAbstract::aggiungoSezioniScambiPrimoPiano() {
    // TODO DA MISURARE
    aggiungoSezione(IdSezioneTipo(3, 1, 1), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 1, 2), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 2, 1), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 2, 2), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 3, 1), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 3, 2), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 4, 1), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 4, 2), LUNGHEZZA_SCAMBIO_LUNGO_PIKO_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 5, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 5, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 6, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 6, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 7, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 7, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 8, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 8, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 9, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 9, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 10, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 10, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 11, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 11, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 12, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 12, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 13, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 13, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 14, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 14, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 15, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 15, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 16, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 16, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 17, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 17, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 18, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 18, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 19, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 19, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 20, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 20, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 21, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 21, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 22, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 22, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 23, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 23, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 24, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 24, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 25, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 25, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 26, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 26, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 27, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 27, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 28, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 28, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 29, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 29, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 30, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 30, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 31, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 31, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 32, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 32, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 33, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 33, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 34, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 34, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 35, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 35, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 36, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 36, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 37, 1), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DEVIATA_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 37, 2), LUNGHEZZA_SCAMBIO_LUNGO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 38, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 38, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 39, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 39, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 40, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 40, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 41, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 41, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 42, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 42, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 43, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 43, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 44, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 44, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 45, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 45, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 46, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 46, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 47, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 47, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 48, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 48, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 49, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 49, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 50, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 50, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 51, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 51, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 52, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 52, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 53, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 53, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 54, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 54, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 55, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 55, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 56, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 56, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 57, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 57, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 58, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 58, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 59, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 59, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 60, 1), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 60, 2), LUNGHEZZA_SCAMBIO_CORTO_HORNBY_DEVIATA, SEMPRE, false);
}

void SezioniAbstract::aggiungoSezioniIncrociPrimoPiano() {
    aggiungoSezione(IdSezioneTipo(4, 1, 1), LUNGHEZZA_INCROCIO_PIKO, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 1, 2), LUNGHEZZA_INCROCIO_PIKO, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(4, 2, 1), LUNGHEZZA_INCROCIO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 2, 2), LUNGHEZZA_INCROCIO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(4, 3, 1), LUNGHEZZA_INCROCIO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 3, 2), LUNGHEZZA_INCROCIO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(4, 4, 1), LUNGHEZZA_INCROCIO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 4, 2), LUNGHEZZA_INCROCIO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(4, 5, 1), LUNGHEZZA_INCROCIO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 5, 2), LUNGHEZZA_INCROCIO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(4, 6, 1), LUNGHEZZA_INCROCIO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 6, 2), LUNGHEZZA_INCROCIO_HORNBY_DEVIATA_CM, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(4, 7, 1), LUNGHEZZA_INCROCIO_HORNBY_DRITTO_CM, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 7, 2), LUNGHEZZA_INCROCIO_HORNBY_DEVIATA_CM, SEMPRE, false);
}

void SezioniAbstract::aggiungoSensoriASezioniPrimoPiano() {
    getSezione(IdSezioneTipo(1, 1, 2))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(1, 0);  // TODO Da misurare quando il binario sarà montato
    getSezione(IdSezioneTipo(1, 1, 1))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(2, 0);  // TODO Da misurare quando il binario sarà montato
    getSezione(IdSezioneTipo(2, 3))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(3, 0);  // TODO Da misurare quando il binario sarà montato
    getSezione(IdSezioneTipo(2, 4))
        ->setSensore1DistanzaDaSensoreAFineSezioneDestra(
            4, 50.5);  // Binario curvo: 39.7 cm. Binario curvo parziale: 10.8 cm.
    getSezione(IdSezioneTipo(2, 5))
        ->setSensore1DistanzaDaSensoreAFineSezioneDestra(
            5, 111.4);  // Binario curvo parziale: 9.1 cm. Binario curvo: 39.7 cm. Binario dritto: 62.6 cm.
    getSezione(IdSezioneTipo(2, 6))->setSensore1DistanzaDaSensoreAFineSezioneDestra(6, 106);
    getSezione(IdSezioneTipo(2, 7))->setSensore1DistanzaDaSensoreAFineSezioneDestra(7, 128.5);
    getSezione(IdSezioneTipo(1, 2, 1))->setSensore1DistanzaDaSensoreAFineSezioneDestra(8, 50);
    getSezione(IdSezioneTipo(1, 2, 2))->setSensore1DistanzaDaSensoreAFineSezioneDestra(9, 50.2);
    getSezione(IdSezioneTipo(1, 2, 3))->setSensore1DistanzaDaSensoreAFineSezioneDestra(10, 52.2);
    getSezione(IdSezioneTipo(1, 2, 4))->setSensore1DistanzaDaSensoreAFineSezioneDestra(11, 49.4);
    getSezione(IdSezioneTipo(1, 2, 5))->setSensore1DistanzaDaSensoreAFineSezioneDestra(12, 50.6);
    getSezione(IdSezioneTipo(1, 2, 6))->setSensore1DistanzaDaSensoreAFineSezioneDestra(13, 50.2);
    getSezione(IdSezioneTipo(1, 2, 7))->setSensore1DistanzaDaSensoreAFineSezioneDestra(14, 50.2);
    getSezione(IdSezioneTipo(2, 8))->setSensore1DistanzaDaSensoreAFineSezioneDestra(15, 69.1);
    getSezione(IdSezioneTipo(2, 9))->setSensore1DistanzaDaSensoreAFineSezioneDestra(16, 86.2);
    getSezione(IdSezioneTipo(2, 21))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(17, 27);
    getSezione(IdSezioneTipo(2, 22))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(18, 18.1);
    getSezione(IdSezioneTipo(2, 23))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(19, 26);
    getSezione(IdSezioneTipo(2, 25))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(20, 23);
    getSezione(IdSezioneTipo(2, 10))->setSensore1DistanzaDaSensoreAFineSezioneDestra(21, 85.3);
    getSezione(IdSezioneTipo(2, 12))->setSensore1DistanzaDaSensoreAFineSezioneDestra(22, 11.9);
    getSezione(IdSezioneTipo(2, 13))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(
            23, 67.8);  // Binario dritto parziale: 13.4 cm. Binario curva: 17.2 cm. Binario dritto: 37.2 cm.
    getSezione(IdSezioneTipo(1, 3, 7))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(
            24, 26.9);  // Binario dritto: 7.7 cm. Binario curva: 17.2 cm. Binario dritto parziale: 2 cm.
    getSezione(IdSezioneTipo(1, 3, 6))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(25, 25.2);
    getSezione(IdSezioneTipo(1, 3, 5))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(26, 35.6);
    getSezione(IdSezioneTipo(1, 3, 4))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(27, 38.4);
    getSezione(IdSezioneTipo(1, 3, 3))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(28, 52.3);
    getSezione(IdSezioneTipo(1, 3, 2))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(29, 52.3);
    getSezione(IdSezioneTipo(1, 3, 1))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(30, 52.3);
    getSezione(IdSezioneTipo(2, 24))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(31, 118.8);
    getSezione(IdSezioneTipo(2, 27))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(32, 48.2);
    getSezione(IdSezioneTipo(1, 3, 7))
        ->setSensore2DistanzaDaSensoreAFineSezioneDestra(
            33, 28.9);  // Binario dritto: 7.7 cm. Binario curva: 17.2 cm. Binario dritto parziale: 4 cm.
    getSezione(IdSezioneTipo(1, 3, 6))->setSensore2DistanzaDaSensoreAFineSezioneDestra(34, 25.3);
    getSezione(IdSezioneTipo(1, 3, 5))->setSensore2DistanzaDaSensoreAFineSezioneDestra(35, 35.2);
    getSezione(IdSezioneTipo(1, 3, 4))->setSensore2DistanzaDaSensoreAFineSezioneDestra(36, 42.1);
    getSezione(IdSezioneTipo(1, 3, 3))->setSensore2DistanzaDaSensoreAFineSezioneDestra(37, 51.2);
    getSezione(IdSezioneTipo(1, 3, 2))->setSensore2DistanzaDaSensoreAFineSezioneDestra(38, 50.3);
    getSezione(IdSezioneTipo(1, 3, 1))->setSensore2DistanzaDaSensoreAFineSezioneDestra(39, 50.4);
    getSezione(IdSezioneTipo(2, 44))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(40, 22.4);
    getSezione(IdSezioneTipo(2, 29))->setSensore1DistanzaDaSensoreAFineSezioneDestra(41, 62.4);
    getSezione(IdSezioneTipo(2, 30))->setSensore1DistanzaDaSensoreAFineSezioneDestra(42, 62.5);
    getSezione(IdSezioneTipo(2, 41))->setSensore1DistanzaDaSensoreAFineSezioneDestra(43, 25.6);
    getSezione(IdSezioneTipo(2, 42))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(44, 14.6);
    getSezione(IdSezioneTipo(2, 43))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(45, 22.7);
    getSezione(IdSezioneTipo(1, 4, 1))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(46, 50.9);
    getSezione(IdSezioneTipo(1, 4, 2))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(47, 52.3);
    getSezione(IdSezioneTipo(1, 4, 3))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(48, 49.4);
    getSezione(IdSezioneTipo(1, 4, 4))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(49, 51);
    getSezione(IdSezioneTipo(2, 31))
        ->setSensore1DistanzaDaSensoreAFineSezioneDestra(50, 60.4);  // Binario dritto: 33.2 cm. Binario curva: 27.2 cm.
    getSezione(IdSezioneTipo(2, 32))
        ->setSensore1DistanzaDaSensoreAFineSezioneDestra(
            51, 87.6);  // Binario curva: 68.8 cm. Binario dritto: 16.8 cm. Binario oltre la curva: 2 cm.
    getSezione(IdSezioneTipo(1, 4, 5))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(52, 50.1);
    getSezione(IdSezioneTipo(1, 4, 6))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(53, 50);
    getSezione(IdSezioneTipo(1, 4, 7))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(54, 49.5);
    getSezione(IdSezioneTipo(2, 33))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(55, 95.8);
    getSezione(IdSezioneTipo(2, 45))->setSensore1DistanzaDaSensoreAFineSezioneSinistra(56, 134.2);
    getSezione(IdSezioneTipo(1, 5, 4))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(57, 0);  // TODO Da misurare quando il binario sarà montato
    getSezione(IdSezioneTipo(1, 5, 3))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(58, 0);  // TODO Da misurare quando il binario sarà montato
    getSezione(IdSezioneTipo(1, 5, 2))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(59, 0);  // TODO Da misurare quando il binario sarà montato
    getSezione(IdSezioneTipo(1, 5, 1))
        ->setSensore1DistanzaDaSensoreAFineSezioneSinistra(60, 0);  // TODO Da misurare quando il binario sarà montato
}

void SezioniAbstract::aggiungoSezioniIntermedieTraPrimoESecondoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità
    aggiungoSezione(IdSezioneTipo(2, 47), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 48), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSensoriASezioniTraPrimoESecondoPiano() {
    // TODO Da fare
}

void SezioniAbstract::aggiungoSezioniStazioniSecondoPiano() {
    aggiungoSezioniStazioneOvestSecondoPiano();
    aggiungoSezioniStazioneEstremoEstSecondoPiano();
}

void SezioniAbstract::aggiungoSezioniStazioneOvestSecondoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità

    aggiungoSezione(IdSezioneTipo(1, 6, 1), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 6, 2), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 6, 3), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 6, 4), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 6, 5), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 6, 6), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 6, 7), 20, MAI, false);
}

void SezioniAbstract::aggiungoSezioniStazioneEstremoEstSecondoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità

    aggiungoSezione(IdSezioneTipo(1, 7, 1), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 7, 2), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 7, 3), 20, MAI, false);
    aggiungoSezione(IdSezioneTipo(1, 7, 4), 20, MAI, false);
}

void SezioniAbstract::aggiungoSezioniIntermedieSecondoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità

    aggiungoSezione(IdSezioneTipo(2, 49), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 50), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 51), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 52), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 53), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 54), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 55), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 56), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 57), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 58), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 59), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 60), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 61), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 62), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 63), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 64), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSezioniScambiSecondoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità

    aggiungoSezione(IdSezioneTipo(3, 61, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 61, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 62, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 62, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 63, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 63, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 64, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 64, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 65, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 65, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 66, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 66, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 67, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 67, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 68, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 68, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 69, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 69, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 70, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 70, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 71, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 71, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 72, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 72, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 73, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 73, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 74, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 74, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 75, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 75, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 76, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 76, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 77, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 77, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 78, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 78, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 79, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 79, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 80, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 80, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 81, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 81, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 82, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 82, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 83, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 83, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 84, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 84, 2), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSezioniIncrociSecondoPiano() {
    // Non sono presenti incroci al secondo piano
}

void SezioniAbstract::aggiungoSensoriASezioniSecondoPiano() {
    // TODO Da fare
}

void SezioniAbstract::aggiungoSezioniIntermedieTraSecondoETerzoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità
    aggiungoSezione(IdSezioneTipo(2, 65), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 66), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 67), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 68), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 69), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 70), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSensoriASezioniTraSecondoETerzoPiano() {
    // TODO Da fare
}

void SezioniAbstract::aggiungoSezioniStazioneTerzoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità
    aggiungoSezione(IdSezioneTipo(1, 8, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(1, 8, 2), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(1, 8, 3), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(1, 8, 4), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(1, 8, 5), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(1, 8, 6), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSezioniIntermedieTerzoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità
    aggiungoSezione(IdSezioneTipo(2, 71), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(2, 72), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSezioniScambiTerzoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità

    aggiungoSezione(IdSezioneTipo(3, 85, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 85, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 86, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 86, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 87, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 87, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 88, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 88, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 89, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 89, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 90, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 90, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 91, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 91, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 92, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 92, 2), 20, SEMPRE, false);

    aggiungoSezione(IdSezioneTipo(3, 93, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(3, 93, 2), 20, SEMPRE, false);
}

void SezioniAbstract::aggiungoSezioniIncrociTerzoPiano() {
    // TODO Check lunghezza sezioni
    // TODO Settare enumerazione tipologia sezione  CAPIRE CHE ENUMERAZIONE TIPOLOGIA SEZIONE CRITICA INSERIRE
    // TODO Check inversione di polarità
    aggiungoSezione(IdSezioneTipo(4, 8, 1), 20, SEMPRE, false);
    aggiungoSezione(IdSezioneTipo(4, 8, 2), 20, SEMPRE, false);
}