// *** INCLUDE *** //

#include "WebServerCore.h"

#include "ArduinoJson.h"
#include "EthernetClient.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/connessioneInternet/connessioneEthernet/client/ClientEthernet.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/scambio/ScambioDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/sezione/SezioneDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/autopilot/autopilotReale/AutopilotReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/stopEmergenza/StopEmergenzaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/scambi/ScambiRelayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/sezioni/SezioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/displayVariabiliFlagAggiornamento/DisplayVariabiliFlagAggiornamento.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/convoglio/ConvoglioResponseDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

EthernetServer ethernetServer(80);
WiFiServer wifiServer(80);

// *** DEFINIZIONE METODI *** //

void WebServerCore::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Web Server...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    ethernetServer.begin();

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Web Server... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void WebServerCore::gestisceClient() {
    Client* clientPtr = nullptr;
    ethernetClientWebServer = ethernetServer.available();
    clientPtr = &ethernetClientWebServer;

    if (*clientPtr) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Nuovo client",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }

        // Non le metto come variabili globali così appena si esce fuori da questa funzione vengono distrutte e Arduino
        // ha più memoria disponibile.
        char requestHeader[NUMERO_CARATTERI_MASSIMI_REQUEST_HEADER];
        char requestBody[NUMERO_CARATTERI_MASSIMI_REQUEST_BODY];
        char responseHeader[NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER];
        char responseBody[NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY];

        // Inizializzo le stringhe a stringa vuota

        memset(requestHeader, 0, NUMERO_CARATTERI_MASSIMI_REQUEST_HEADER);
        memset(requestBody, 0, NUMERO_CARATTERI_MASSIMI_REQUEST_BODY);
        memset(responseHeader, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER);
        memset(responseBody, 0, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);

        // an HTTP request ends with a blank line
        boolean currentLineIsBlank = true;
        while (clientPtr->connected()) {
            if (clientPtr->available()) {
                char c = clientPtr->read();
                char stringaTemporanea[2] = {c, '\0'};
                strncat(requestHeader, stringaTemporanea, 1);

                StatoHTTP statoHttp;

                // Se sono arrivato alla fine della linea (ho ricevuto un carattere newline) e la linea è vuota, la
                // richiesta HTTP è finita, quindi possiamo inviare una risposta
                if (c == '\n' && currentLineIsBlank) {
                    TipologiaRichiestaHttpWebServer tipologiaRichiestaHTTP = getTipologiaRichiestaHTTP(requestHeader);

                    // Verifico che il client abbia la giusta autorizzazione, soltanto nel caso in cui la richiesta non
                    // sia OPTIONS. Importante Lazy Evaluation: se la richiesta è OPTIONS, non devo verificare
                    // l'autenticazione.
                    if (tipologiaRichiestaHTTP == WEB_SERVER_OPTIONS || autentico(requestHeader)) {
                        // Se si tratta di una richiesta POST o PUT, devo continuare a leggere dopo la riga vuota, in
                        // quanto il body si trova lì.
                        if (tipologiaRichiestaHTTP == WEB_SERVER_POST || tipologiaRichiestaHTTP == WEB_SERVER_PUT) {
                            leggoBody(clientPtr, requestBody);
                        }

                        if (DEBUGGING_WEB_SERVER_REQUEST) {
                            stampoRequest(tipologiaRichiestaHTTP, requestHeader, requestBody);
                        }

                        // Gestisco la richiesta HTTP in base al tipo di richiesta
                        switch (tipologiaRichiestaHTTP) {
                            case WEB_SERVER_OPTIONS:

                                if (DEBUGGING_WEB_SERVER) {
                                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Richiesta OPTIONS",
                                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                                }

                                statoHttp = OK_204;
                                break;
                            case WEB_SERVER_GET:

                                if (DEBUGGING_WEB_SERVER) {
                                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Richiesta GET",
                                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                                    loggerReale.cambiaTabulazione(1);
                                }

                                statoHttp = gestiscoRichiestaGET(requestHeader, responseBody);
                                if (DEBUGGING_WEB_SERVER) {
                                    loggerReale.cambiaTabulazione(-1);
                                }
                                break;
                            case WEB_SERVER_POST:

                                if (DEBUGGING_WEB_SERVER) {
                                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Richiesta POST",
                                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                                    loggerReale.cambiaTabulazione(1);
                                }

                                statoHttp = gestiscoRichiestaPOST(requestHeader, requestBody, responseBody);
                                if (DEBUGGING_WEB_SERVER) {
                                    loggerReale.cambiaTabulazione(-1);
                                }
                                break;
                            case WEB_SERVER_PUT:

                                if (DEBUGGING_WEB_SERVER) {
                                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Richiesta PUT",
                                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                                    loggerReale.cambiaTabulazione(1);
                                }

                                statoHttp = gestiscoRichiestaPUT(requestHeader, requestBody, responseBody);
                                if (DEBUGGING_WEB_SERVER) {
                                    loggerReale.cambiaTabulazione(-1);
                                }
                                break;
                        }
                    } else {
                        if (DEBUGGING_WEB_SERVER) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Client non autenticato",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        }
                        statoHttp = UNAUTHORIZED_401;
                    }

                    invioResponseAlClient(clientPtr, statoHttp, responseHeader, responseBody);

                    if (DEBUGGING_WEB_SERVER_RESPONSE) {
                        stampoResponse(responseHeader, responseBody);
                    }

                    break;
                }
                if (c == '\n') {
                    // Iniziamo una nuova linea
                    currentLineIsBlank = true;
                } else if (c != '\r') {
                    // Abbiamo un carattere sulla linea corrente
                    currentLineIsBlank = false;
                }
            }
        }
        // Do al browser il tempo di ricevere i dati
        delay(1);

        // Chiudo la connessione
        clientPtr->stop();
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Client disconnesso",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
}

TipologiaRichiestaHttpWebServer WebServerCore::getTipologiaRichiestaHTTP(char* requestHeader) {
    // Alcuni browser inviano una richiesta OPTIONS prima di una richiesta.
    // È necessario che Arduino gestisca questo caso.
    // Se è una richiesta OPTIONS per PUT, entrambe le parole saranno contenute nell'header: è dunque necessario cercare
    // prima OPTIONS delle altre keyword.
    if (strstr(requestHeader, "OPTIONS") != NULL) {
        return WEB_SERVER_OPTIONS;
    } else if (strstr(requestHeader, "GET") != NULL) {
        return WEB_SERVER_GET;
    } else if (strstr(requestHeader, "POST") != NULL) {
        return WEB_SERVER_POST;
    } else if (strstr(requestHeader, "PUT") != NULL) {
        return WEB_SERVER_PUT;
    }
}

StatoHTTP WebServerCore::gestiscoRichiestaGET(char* requestHeader, char* responseBody) {
    if (strstr(requestHeader, "healthCheck") != NULL) {
        creoResponseBodyInfoJson(responseBody, "Health check OK", TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
        return OK_200;
    } else {
        creoResponseBodyInfoJson(responseBody, "Richiesta non valida.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        return NOT_FOUND_404;
    }
}

StatoHTTP WebServerCore::gestiscoRichiestaPOST(char* requestHeader, char* requestBody, char* responseBody) {
    // Controlliamo prima debugging, perché altrimenti per l'endpoint debugging/sensore verrebbe interpretato l'endpoint
    // /sensore e non /debugging/sensore.
    if (strstr(requestHeader, "debugging/") != NULL) {
        if (strstr(requestHeader, "sensore/") != NULL) {
            if (strstr(requestHeader, "aziona") != NULL) {
                JsonDocument documentoJson;
                bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
                if (!risultatoDeserializzazione) {
                    return BAD_REQUEST_400;
                }

                JsonObject bodyJson = documentoJson.as<JsonObject>();
                IdSensorePosizioneTipo idSensore;

                if (bodyJson["idSensore"].is<JsonVariant>()) {
                    idSensore = bodyJson["idSensore"];
                    if (sensoriPosizioneReali.esisteIdSensore(idSensore)) {
                        SensorePosizioneAbstract* sensore = sensoriPosizioneReali.getSensorePosizione(idSensore);
                        sensore->setStato(true);
                        creoResponseBodyInfoJson(responseBody, "Sensore azionato correttamente.",
                                                 TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                        return OK_200;
                    } else {
                        creoResponseBodyInfoJson(responseBody, "ID sensore non valido.",
                                                 TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                        return BAD_REQUEST_400;
                    }
                } else {
                    creoResponseBodyInfoJson(responseBody, "Parametro 'idSensore' non trovato.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                    return BAD_REQUEST_400;
                }
            } else {
                creoResponseBodyInfoJson(responseBody, "Path non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return NOT_FOUND_404;
            }
        } else if (strstr(requestHeader, "sezione/") != NULL) {
            JsonDocument documentoJson;
            bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
            if (!risultatoDeserializzazione) {
                return BAD_REQUEST_400;
            }

            JsonObject bodyJson = documentoJson.as<JsonObject>();

            IdSezioneTipo idSezione = leggoIdSezioneBody("idSezione", bodyJson, responseBody);
            SezioneAbstract* sezione = sezioniReali.getSezione(idSezione);

            // Se c'è stato un errore durante la lettura dell'ID della sezione, ritorno errore HTTP.
            if (idSezione.equals(new IdSezioneTipo(0, 0, 0))) {
                return BAD_REQUEST_400;
            }

            // Posiziono unlock prima perché lock è contenuto nella parola unlock. Altrimenti entrerei sempre in lock.
            if (strstr(requestHeader, "unlock") != NULL) {
                if (DEBUGGING_WEB_SERVER) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "sezione/unlock",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                if (!sezione->isLocked()) {
                    creoResponseBodyInfoJson(responseBody, "La sezione risulta già sbloccata.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                    return BAD_REQUEST_400;
                } else {
                    sezioniReali.unlock(sezione->getId());
                    creoResponseBodyInfoJson(responseBody, "Sezione sbloccata correttamente.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                    return OK_200;
                }
            } else if (strstr(requestHeader, "lock") != NULL) {
                if (DEBUGGING_WEB_SERVER) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "sezione/lock",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                if (sezione->isLocked()) {
                    creoResponseBodyInfoJson(responseBody, "La sezione risulta già bloccata.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                    return BAD_REQUEST_400;
                } else {
                    // Gli endpoint lock e unlock servono soltanto a scopo di debugging, quindi impostiamo il convoglio
                    // che ha bloccato la sezione a 0
                    sezioniReali.lock(sezione->getId(), 0);
                    creoResponseBodyInfoJson(responseBody, "Sezione bloccata correttamente.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                    return OK_200;
                }
            } else {
                creoResponseBodyInfoJson(responseBody, "Path non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return NOT_FOUND_404;
            }
        } else {
            creoResponseBodyInfoJson(responseBody, "Path non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
            return NOT_FOUND_404;
        }
    } else if (strstr(requestHeader, "autopilot/") != NULL) {
        // L'endpoint resetPosizioneConvoglio deve essere prima di setPosizioneConvoglio
        if(strstr(requestHeader, "resetPosizioneConvoglio") != NULL){
            JsonDocument documentoJson;
            bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
            if (!risultatoDeserializzazione) {
                return BAD_REQUEST_400;
            }

            JsonObject bodyJson = documentoJson.as<JsonObject>();
            // Leggo input effettuando controlli

            IdConvoglioTipo idConvoglio;
            if (bodyJson["idConvoglio"].is<JsonVariant>()) {
                idConvoglio = bodyJson["idConvoglio"];
            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'idConvoglio' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            if (!convogliReali.esisteConvoglio(idConvoglio)) {
                creoResponseBodyInfoJson(responseBody, "ID convoglio non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            ConvoglioAbstractReale* convoglio = convogliReali.getConvoglio(idConvoglio, true);
            convoglio->resetPosizione();

            creoResponseBodyInfoJson(responseBody, "Reset posizione del convoglio effettuato correttamente.",
                                     TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
            return OK_200;

        }
        else if (strstr(requestHeader, "setPosizioneConvoglio") != NULL) {
            JsonDocument documentoJson;
            bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
            if (!risultatoDeserializzazione) {
                return BAD_REQUEST_400;
            }

            JsonObject bodyJson = documentoJson.as<JsonObject>();

            // Leggo input effettuando controlli

            IdConvoglioTipo idConvoglio;
            if (bodyJson["idConvoglio"].is<JsonVariant>()) {
                idConvoglio = bodyJson["idConvoglio"];
            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'idConvoglio' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            if (!convogliReali.esisteConvoglio(idConvoglio)) {
                creoResponseBodyInfoJson(responseBody, "ID convoglio non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            IdSezioneTipo idSezioneCorrenteOccupata =
                leggoIdSezioneBody("idSezioneCorrenteOccupata", bodyJson, responseBody);

            // Se c'è stato un errore durante la lettura dell'ID della sezione, ritorno errore HTTP.
            if (idSezioneCorrenteOccupata.equals(new IdSezioneTipo(0, 0, 0))) {
                return BAD_REQUEST_400;
            }

            bool direzionePrimaLocomotivaDestra;
            if (bodyJson["direzionePrimaLocomotivaDestra"].is<JsonVariant>()) {
                direzionePrimaLocomotivaDestra = bodyJson["direzionePrimaLocomotivaDestra"];
            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'direzionePrimaLocomotivaDestra' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            // Controllo che esista l'ID della sezione inserita dall'utente
            if (!sezioniReali.esisteSezione(idSezioneCorrenteOccupata)) {
                creoResponseBodyInfoJson(responseBody, "L'ID della sezione inserita non esiste.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            ConvoglioAbstractReale* convoglio = convogliReali.getConvoglio(idConvoglio, true);
            bool risultatoSetPosizioneConvoglio = convoglio->autopilotConvoglio->setPosizioneConvoglio(
                idSezioneCorrenteOccupata, direzionePrimaLocomotivaDestra);
            if (risultatoSetPosizioneConvoglio) {
                creoResponseBodyInfoJson(responseBody, "Posizione convoglio impostata correttamente.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                return OK_200;
            } else {
                creoResponseBodyInfoJson(responseBody, "Errore durante l'impostazione della posizione del convoglio.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return INTERNAL_SERVER_ERROR_500;
            }

        } else if (strstr(requestHeader, "avvia") != NULL) {
            bool risultatoAvvioAutopilot;
            if (strstr(requestHeader, "modalitaAutopilotStazioneSuccessiva") != NULL) {
                risultatoAvvioAutopilot = autopilotReale.avviaAutopilotModalitaStazioneSuccessiva();
                // Bisogna prima controllare modalitaAutopilotApproccioCasualeSceltaConvogli e poi
                // modalitaAutopilotApproccioCasuale perché il secondo è contenuto nel primo
            } else if (strstr(requestHeader, "modalitaAutopilotApproccioCasualeSceltaConvogli") != NULL) {
                JsonDocument documentoJson;
                bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
                if (!risultatoDeserializzazione) {
                    return BAD_REQUEST_400;
                }
                JsonObject body = documentoJson.as<JsonObject>();

                // Alloco memoria per NUMERO_CONVOGLI, ovvero il numero massimo di convogli che possono essere passati
                IdConvoglioTipo arrayIdConvogli[NUMERO_CONVOGLI];
                int numeroIdConvogli;
                if (body["arrayIdConvogli"].is<JsonVariant>()) {
                    JsonArray arrayIdConvogliJsonObject = body["arrayIdConvogli"].as<JsonArray>();
                    numeroIdConvogli = arrayIdConvogliJsonObject.size();
                    if (numeroIdConvogli > NUMERO_CONVOGLI) {
                        creoResponseBodyInfoJson(
                            responseBody,
                            "Numero massimo di convogli superato. Sono stati inseriti più convogli di "
                            "quelli massimi consentiti dal sistema.",
                            TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                        return BAD_REQUEST_400;
                    } else {
                        // Copio gli ID dell'oggetto JSON nella variabile arrayIdConvogli
                        for (int i = 0; i < numeroIdConvogli; i++) {
                            arrayIdConvogli[i] = arrayIdConvogliJsonObject[i];
                        }
                    }

                } else {
                    creoResponseBodyInfoJson(responseBody, "Parametro 'arrayIdConvogli' non trovato.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                    return BAD_REQUEST_400;
                }

                bool ottimizzazioneAssegnazioneConvogliAiBinari;
                if (body["ottimizzazioneAssegnazioneConvogliAiBinari"].is<JsonVariant>()) {
                    ottimizzazioneAssegnazioneConvogliAiBinari = body["ottimizzazioneAssegnazioneConvogliAiBinari"];
                } else {
                    creoResponseBodyInfoJson(responseBody,
                                             "Parametro 'ottimizzazioneAssegnazioneConvogliAiBinari' non trovato.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                    return BAD_REQUEST_400;
                }

                // Se sono arrivato fino a qui, ho tutti i parametri necessari per chiamare la funzione
                risultatoAvvioAutopilot = autopilotReale.avviaAutopilotModalitaCasualeSceltaConvogli(
                    arrayIdConvogli, numeroIdConvogli, ottimizzazioneAssegnazioneConvogliAiBinari);

            } else if (strstr(requestHeader, "modalitaAutopilotApproccioCasuale") != NULL) {
                JsonDocument documentoJson;
                bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
                if (!risultatoDeserializzazione) {
                    return BAD_REQUEST_400;
                }
                JsonObject body = documentoJson.as<JsonObject>();

                bool ottimizzazioneAssegnazioneConvogliAiBinari;
                if (body["ottimizzazioneAssegnazioneConvogliAiBinari"].is<JsonVariant>()) {
                    ottimizzazioneAssegnazioneConvogliAiBinari = body["ottimizzazioneAssegnazioneConvogliAiBinari"];
                } else {
                    creoResponseBodyInfoJson(responseBody,
                                             "Parametro 'ottimizzazioneAssegnazioneConvogliAiBinari' non trovato.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                    return BAD_REQUEST_400;
                }
                risultatoAvvioAutopilot =
                    autopilotReale.avviaAutopilotModalitaCasuale(ottimizzazioneAssegnazioneConvogliAiBinari);
            } else {
                creoResponseBodyInfoJson(responseBody, "Path non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            // Controllo se l'avvio dell'autopilot è andato a buon fine
            if (risultatoAvvioAutopilot) {
                creoResponseBodyInfoJson(responseBody, "Autopilot avviato correttamente.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                return OK_200;
            } else {
                creoResponseBodyInfoJson(responseBody, "Errore durante l'avvio dell'autopilot. Controlla i log.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return INTERNAL_SERVER_ERROR_500;
            }

        } else if (strstr(requestHeader, "ferma") != NULL) {
            autopilotReale.ferma(true, true);
            creoResponseBodyInfoJson(responseBody, "Autopilot fermato correttamente.",
                                     TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
            return OK_200;

        } else {
            creoResponseBodyInfoJson(responseBody, "Path non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
            return BAD_REQUEST_400;
        }
    } else if (strstr(requestHeader, "autopilotConvoglio/") != NULL) {
        if (strstr(requestHeader, "inizializzaAutopilotConvoglioModalitaStazioneSuccessiva") != NULL) {
            JsonDocument documentoJson;
            bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
            if (!risultatoDeserializzazione) {
                return BAD_REQUEST_400;
            }

            JsonObject body = documentoJson.as<JsonObject>();

            IdConvoglioTipo idConvoglio;

            if (body["idConvoglio"].is<JsonVariant>()) {
                idConvoglio = body["idConvoglio"];
            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'idConvoglio' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            IdSezioneTipo idSezioneArrivo = leggoIdSezioneBody("idSezioneStazioneArrivo", body, responseBody);
            if (idSezioneArrivo.equals(new IdSezioneTipo(0, 0, 0))) {
                creoResponseBodyInfoJson(responseBody, "Parametro 'idSezioneStazioneArrivo' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            int delayAttesaPartenzaStazioneLocale;
            if (body["delayAttesaPartenzaStazioneLocale"].is<JsonVariant>()) {
                delayAttesaPartenzaStazioneLocale = body["delayAttesaPartenzaStazioneLocale"];

            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'delayAttesaPartenzaStazioneLocale' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            // Controllo che esista l'ID della sezione inserita dall'utente
            if (!sezioniReali.esisteSezione(idSezioneArrivo)) {
                creoResponseBodyInfoJson(responseBody, "L'ID della sezione inserita non esiste.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            ConvoglioAbstractReale* convoglio = convogliReali.getConvoglio(idConvoglio, true);
            bool risultatoInizializzazione =
                convoglio->autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(
                    idSezioneArrivo, delayAttesaPartenzaStazioneLocale);

            if (risultatoInizializzazione) {
                creoResponseBodyInfoJson(responseBody, "Autopilot convoglio inizializzato correttamente.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                return OK_200;
            } else {
                creoResponseBodyInfoJson(responseBody,
                                         "Errore durante l'inizializzazione dell'autopilot del convoglio.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return INTERNAL_SERVER_ERROR_500;
            }
        }
    } else if (strstr(requestHeader, "convoglio") != NULL) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Convoglio",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            loggerReale.cambiaTabulazione(1);
        }

        IdConvoglioTipo idConvoglio = estraiIdDaHeader(requestHeader, "convoglio");
        if (idConvoglio == -1) {
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "ID non trovato nel path.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                loggerReale.cambiaTabulazione(-1);
            }

            creoResponseBodyInfoJson(responseBody, "ID non trovato nel path.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
            return BAD_REQUEST_400;
        } else {
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                      "ID Convoglio: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idConvoglio).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }

            if (!convogliReali.esisteConvoglio(idConvoglio)) {
                if (DEBUGGING_WEB_SERVER) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR,
                                          "È stato inserito un ID di un convoglio che non esiste.",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                creoResponseBodyInfoJson(responseBody, "È stato inserito un ID di un convoglio che non esiste.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                if (DEBUGGING_WEB_SERVER) {
                    loggerReale.cambiaTabulazione(-1);
                }
                return BAD_REQUEST_400;
            } else {
                if (strstr(requestHeader, "cambiaDirezione") != NULL) {
                    // Devo cambiare la direzione del convoglio
                    ConvoglioAbstract* convoglio = convogliReali.getConvoglio(idConvoglio, false);
                    convoglio->cambiaDirezione(statoAutopilot);

                    creoResponseBodyInfoJson(responseBody, "Direzione del convoglio cambiata con successo.",
                                             TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
                    if (DEBUGGING_WEB_SERVER) {
                        loggerReale.cambiaTabulazione(-1);
                    }
                    return OK_200;
                } else {
                    return NOT_FOUND_404;
                }
            }
        }
    } else if (strstr(requestHeader, "sezione/") != NULL) {
        JsonDocument documentoJson;
        bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
        if (!risultatoDeserializzazione) {
            return BAD_REQUEST_400;
        }

        JsonObject bodyJson = documentoJson.as<JsonObject>();

        IdSezioneTipo idSezione = leggoIdSezioneBody("idSezione", bodyJson, responseBody);

        // Se c'è stato un errore durante la lettura dell'ID della sezione, ritorno errore HTTP.
        if (idSezione.equals(new IdSezioneTipo(0, 0, 0))) {
            return BAD_REQUEST_400;
        }

        // Sarebbe più efficiente/giusto fare un endpoint che, passato l'ID del convoglio, restituisca tutte le sezioni
        // che possono ospitare il convoglio. Tuttavia, anche in questo caso ci scontreremmo con la memoria limitata di
        // Arduino: il response body potrebbe risultare troppo grande e portare al crash di Arduino.
        else if (strstr(requestHeader, "puoOspitareConvoglioModalitaAutopilot") != NULL) {
            // Leggo input effettuando controlli

            IdConvoglioTipo idConvoglio;
            if (bodyJson["idConvoglio"].is<JsonVariant>()) {
                idConvoglio = bodyJson["idConvoglio"];
            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'idConvoglio' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            if (!convogliReali.esisteConvoglio(idConvoglio)) {
                creoResponseBodyInfoJson(responseBody, "ID convoglio non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            // Se sono arrivato fino a qui, ho tutti i parametri necessari per chiamare la funzione

            AutopilotConvoglioAbstract* autopilotConvoglio =
                convogliReali.getConvoglio(idConvoglio, true)->autopilotConvoglio;

            SezioneAbstract* sezione = sezioniReali.getSezione(idSezione);
            bool sezioneLungaASufficienza = autopilotConvoglio->isSezioneLungaASufficienzaPerConvoglio(sezione);
            creoResponseBodyBooleanResultJson(responseBody, sezioneLungaASufficienza);
            return OK_200;
        } else if(strstr(requestHeader, "puoOspitareConvoglioModalitaManuale") != NULL){
            // Leggo input effettuando controlli

            IdConvoglioTipo idConvoglio;
            if (bodyJson["idConvoglio"].is<JsonVariant>()) {
                idConvoglio = bodyJson["idConvoglio"];
            } else {
                creoResponseBodyInfoJson(responseBody, "Parametro 'idConvoglio' non trovato.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            if (!convogliReali.esisteConvoglio(idConvoglio)) {
                creoResponseBodyInfoJson(responseBody, "ID convoglio non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                return BAD_REQUEST_400;
            }

            // Se sono arrivato fino a qui, ho tutti i parametri necessari per chiamare la funzione

            ConvoglioAbstract *convoglio = convogliReali.getConvoglio(idConvoglio, true);

            SezioneAbstract* sezione = sezioniReali.getSezione(idSezione);
            bool sezioneLungaASufficienza = sezione->isSezioneLungaASufficienzaPerConvoglio(convoglio);
            creoResponseBodyBooleanResultJson(responseBody, sezioneLungaASufficienza);
            return OK_200;
        }

    } else {
        return NOT_FOUND_404;
    }
}

StatoHTTP WebServerCore::gestiscoRichiestaPUT(char* requestHeader, char* requestBody, char* responseBody) {
    if (strstr(requestHeader, "convoglio") != NULL) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Convoglio",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            loggerReale.cambiaTabulazione(1);
        }

        IdConvoglioTipo idConvoglio = estraiIdDaHeader(requestHeader, "convoglio");
        if (idConvoglio == -1) {
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "ID non trovato nel path.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                loggerReale.cambiaTabulazione(-1);
            }

            creoResponseBodyInfoJson(responseBody, "ID non trovato nel path.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
            return BAD_REQUEST_400;
        } else {
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                      "ID Convoglio: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idConvoglio).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }

            if (!convogliReali.esisteConvoglio(idConvoglio)) {
                if (DEBUGGING_WEB_SERVER) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR,
                                          "È stato inserito un ID di un convoglio che non esiste.",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                creoResponseBodyInfoJson(responseBody, "È stato inserito un ID di un convoglio che non esiste.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                if (DEBUGGING_WEB_SERVER) {
                    loggerReale.cambiaTabulazione(-1);
                }
                return BAD_REQUEST_400;
            } else {
                StatoHTTP statoHttp = salvoStatoConvoglio(idConvoglio, requestBody, responseBody);
                if (DEBUGGING_WEB_SERVER) {
                    loggerReale.cambiaTabulazione(-1);
                }
                return statoHttp;
            }
        }
    } else if (strstr(requestHeader, "stop") != NULL) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Stop",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            loggerReale.cambiaTabulazione(1);
        }

        stopEmergenzaReale.aziona(true);
        creoResponseBodyInfoJson(responseBody, "Stop emergenza inviato.", TIPOLOGIA_LOG_RESPONSE_BODY_INFO);
        if (DEBUGGING_WEB_SERVER) {
            loggerReale.cambiaTabulazione(-1);
        }
        return OK_200;
    } else if (strstr(requestHeader, "scambio") != NULL) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Locomotiva",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            loggerReale.cambiaTabulazione(1);
        }

        IdScambioTipo idScambio = estraiIdDaHeader(requestHeader, "scambio");
        if (idScambio == -1) {
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "ID non trovato nel path.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                loggerReale.cambiaTabulazione(-1);
            }

            creoResponseBodyInfoJson(responseBody, "ID non trovato nel path.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
            return BAD_REQUEST_400;
        } else {
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                      "ID scambio: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idScambio).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }

            if (!scambiRelayReale.esisteNumeroScambio(idScambio)) {
                if (DEBUGGING_WEB_SERVER) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR,
                                          "È stato inserito un ID di uno scambio che non esiste.",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                creoResponseBodyInfoJson(responseBody, "È stato inserito un ID di uno scambio che non esiste.",
                                         TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
                if (DEBUGGING_WEB_SERVER) {
                    loggerReale.cambiaTabulazione(-1);
                }
                return BAD_REQUEST_400;
            } else {
                StatoHTTP statoHttp = salvoStatoScambio(idScambio, requestBody, responseBody);
                if (DEBUGGING_WEB_SERVER) {
                    loggerReale.cambiaTabulazione(-1);
                }
                return statoHttp;
            }
        }

    } else {
        creoResponseBodyInfoJson(responseBody, "Path non valido.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        return NOT_FOUND_404;
    }
}

int WebServerCore::estraiIdDaHeader(char* requestHeader, const char* stringaDaCercare) {
    // Imposto il valore di default per idLocomotiva se l'ID della locomotiva non viene trovato
    int id = -1;

    int lunghezzaStringaDaCercare = strlen(stringaDaCercare);

    // Utilizzo strstr per trovare l'inizio della sottostringa stringaDaCercare.
    char* startPtr = strstr(requestHeader, stringaDaCercare);
    if (startPtr != NULL) {
        // Calcolo la posizione iniziale dell'ID spostandomi oltre "pathParziale/"
        // +1 per considerare lo slash che non viene passato in stringaDaCercare
        int posizioneInizio = startPtr - requestHeader + lunghezzaStringaDaCercare + 1;

        // Cerco la fine dell'ID cercando "HTTP" o uno spazio dopo "pathParziale/"
        char* endPtrHTTP = strstr(startPtr + lunghezzaStringaDaCercare + 1, "HTTP");
        char* endPtrSpace = strchr(startPtr + lunghezzaStringaDaCercare + 1, ' ');
        int posizioneFine;

        if (endPtrHTTP != NULL && (endPtrSpace == NULL || endPtrHTTP < endPtrSpace)) {
            posizioneFine = endPtrHTTP - requestHeader;
        } else if (endPtrSpace != NULL) {
            posizioneFine = endPtrSpace - requestHeader;
        } else {
            // Se non trovo né "HTTP" né uno spazio, uso la lunghezza totale della stringa
            posizioneFine = strlen(requestHeader);
        }

        // Estraggo l'ID come sottostringa
        int idLength = posizioneFine - posizioneInizio;
        char idLocomotivaStr[idLength + 1];  // +1 per il terminatore null
        strncpy(idLocomotivaStr, requestHeader + posizioneInizio, idLength);
        idLocomotivaStr[idLength] = '\0';  // Aggiungo il terminatore null

        // Converto la stringa dell'ID in un intero
        id = atoi(idLocomotivaStr);
    }

    // Se la stringa è vuota, atoi restituisce 0, quindi controllo e restituisco -1 in quel caso
    if (id == 0) {
        return -1;
    } else {
        return id;
    }
}

void WebServerCore::leggoBody(Client* client, char* requestBody) {
    // Se si tratta di una richiesta POST, devo continuare a leggere dopo la riga vuota, in quanto
    // il body si trova lì.
    while (client->available()) {
        char c = client->read();
        char stringaTemporanea[2] = {c, '\0'};
        strncat(requestBody, stringaTemporanea, 1);
    }
}

void WebServerCore::compongoResponseHTTP200(char* responseHeader) {
    strcat(responseHeader, "HTTP/1.1 200 OK\r\n");
    strcat(responseHeader, "Content-Type: application/json\r\n");
    strcat(responseHeader, "Access-Control-Allow-Origin: *\r\n");
    strcat(responseHeader, "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS\r\n");
    strcat(responseHeader, "Access-Control-Allow-Headers: *\r\n");
    strcat(responseHeader, "Connection: close\r\n");
}

void WebServerCore::compongoResponseHTTP204(char* responseHeader) {
    strcat(responseHeader, "HTTP/1.1 204 No Content\r\n");
    strcat(responseHeader, "Access-Control-Allow-Origin: *\r\n");
    strcat(responseHeader, "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS\r\n");
    strcat(responseHeader, "Access-Control-Allow-Headers: *\r\n");
    strcat(responseHeader, "Connection: close\r\n");
}

void WebServerCore::compongoResponseHTTP400(char* responseHeader) {
    strcat(responseHeader, "HTTP/1.1 400 Bad Request\r\n");
    strcat(responseHeader, "Content-Type: application/json\r\n");
    strcat(responseHeader, "Access-Control-Allow-Origin: *\r\n");
    strcat(responseHeader, "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS\r\n");
    strcat(responseHeader, "Access-Control-Allow-Headers: *\r\n");
    strcat(responseHeader, "Connection: close\r\n");
}

void WebServerCore::compongoResponseHTTP401(char* responseHeader) {
    strcat(responseHeader, "HTTP/1.1 401 Unauthorized\r\n");
    strcat(responseHeader, "Content-Type: application/json\r\n");
    strcat(responseHeader, "Access-Control-Allow-Origin: *\r\n");
    strcat(responseHeader, "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS\r\n");
    strcat(responseHeader, "Access-Control-Allow-Headers: *\r\n");
    strcat(responseHeader, "Connection: close\r\n");
}

void WebServerCore::compongoResponseHTTP404(char* responseHeader) {
    strcat(responseHeader, "HTTP/1.1 404 Not Found\r\n");
    strcat(responseHeader, "Content-Type: application/json\r\n");
    strcat(responseHeader, "Access-Control-Allow-Origin: *\r\n");
    strcat(responseHeader, "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS\r\n");
    strcat(responseHeader, "Access-Control-Allow-Headers: *\r\n");
    strcat(responseHeader, "Connection: close\r\n");
}

void WebServerCore::compongoResponseHTTP500(char* responseHeader) {
    strcat(responseHeader, "HTTP/1.1 500 Internal Server Error\r\n");
    strcat(responseHeader, "Content-Type: application/json\r\n");
    strcat(responseHeader, "Access-Control-Allow-Origin: *\r\n");
    strcat(responseHeader, "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS\r\n");
    strcat(responseHeader, "Access-Control-Allow-Headers: *\r\n");
    strcat(responseHeader, "Connection: close\r\n");
}

void WebServerCore::invioResponseAlClient(Client* client, StatoHTTP statoHttp, char* responseHeader,
                                          char* responseBody) {
    switch (statoHttp) {
        case OK_204:
            compongoResponseHTTP204(responseHeader);
            break;
        case OK_200:
            compongoResponseHTTP200(responseHeader);
            break;
        case BAD_REQUEST_400:
            compongoResponseHTTP400(responseHeader);
            break;
        case UNAUTHORIZED_401:
            compongoResponseHTTP401(responseHeader);
            break;
        case NOT_FOUND_404:
            compongoResponseHTTP404(responseHeader);
            break;
        case INTERNAL_SERVER_ERROR_500:
            compongoResponseHTTP500(responseHeader);
            break;
    }

    invioAlClient(client, responseHeader);
    // L'header deve essere distanziato dal body con una riga vuota
    invioAlClient(client, "\n");
    invioAlClient(client, responseBody);
}

void WebServerCore::stampoRequest(TipologiaRichiestaHttpWebServer tipologiaRichiestaHTTP, char* requestHeader,
                                  char* requestBody) {
    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nHEADER REQUEST: \n",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, requestHeader,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    // Se la richiesta è di tipo POST, stampo anche il body
    if (tipologiaRichiestaHTTP == WEB_SERVER_POST || tipologiaRichiestaHTTP == WEB_SERVER_PUT) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nBODY REQUEST: \n",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, requestBody,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\n",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
}

void WebServerCore::stampoResponse(char* responseHeader, char* responseBody) {
    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nHEADER RESPONSE: \n",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, responseHeader,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    // Se il response body è stato inizializzato, stampo anche quello
    if (strlen(responseBody) > 0) {
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "\nBODY RESPONSE: \n",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, responseBody,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }
    }
}

StatoHTTP WebServerCore::salvoStatoConvoglio(IdConvoglioTipo idConvoglio, char* requestBody, char* responseBody) {
    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Salvo stato convoglio",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        loggerReale.cambiaTabulazione(1);
    }

    ConvoglioAbstractReale* convoglio = convogliReali.getConvoglio(idConvoglio, false);

    JsonDocument documentoJson;
    bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
    if (!risultatoDeserializzazione) {
        return BAD_REQUEST_400;
    }

    JsonObject convoglioJson = documentoJson.as<JsonObject>();

    // Scansiono tutte le key che mi aspetto, se c'è procedo a salvare nella struct convoglio

    // Mi segno se aggiorno qualcosa, in caso negativo restituisco un messaggio di errore
    bool aggiornamentoEffettuato = false;

    char responseMessage[150];
    memset(responseMessage, 0, 150);

    if (convoglioJson["velocitaImpostata"].is<JsonVariant>()) {
        if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
            const char* messaggioErrore =
                "Impossibile cambiare la velocità del convoglio in quanto l'autopilot è attivo.";

            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, messaggioErrore,
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
            strcat(responseMessage, messaggioErrore);
            return BAD_REQUEST_400;
        }

        VelocitaRotabileKmHTipo velocita = convoglioJson["velocitaImpostata"];
        convoglio->cambiaVelocita(velocita, true, statoAutopilot);

        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                  "Aggiorno velocitaImpostata: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(velocita).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }

        strcat(responseMessage, "Letto parametro 'velocitaImpostata'. ");

        aggiornamentoEffettuato = true;

        // Se l'ID del convoglio aggiornato è uno di quelli mostrati attualmente sul display, devo triggerare
        // l'aggiornamento della velocità
        if (idConvoglio == displayReale.homepage.idConvoglioCorrente1Display ||
            idConvoglio == displayReale.homepage.idConvoglioCorrente2Display) {
            velocitaConvogliDisplayDaAggiornare = true;
        }
    }

    if (convoglioJson["luciAccese"].is<JsonVariant>()) {
        bool luciAccese = convoglioJson["luciAccese"];
        convoglio->setLuciAccese(luciAccese);

        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                  "Aggiorno luci convoglio: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, luciAccese ? "accese" : "spente",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }

        strcat(responseMessage, "Letto parametro 'luciAccese'. ");

        aggiornamentoEffettuato = true;

        // Se l'ID del convoglio aggiornato è uno di quelli mostrati attualmente sul display, devo triggerare
        // l'aggiornamento delle luci
        if (idConvoglio == displayReale.homepage.idConvoglioCorrente1Display ||
            idConvoglio == displayReale.homepage.idConvoglioCorrente2Display) {
            luciConvogliDisplayDaAggiornare = true;
        }
    }

    if (convoglioJson["isPresenteSulTracciato"].is<JsonVariant>()) {
        if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
            const char* messaggioErrore =
                "Impossibile cambiare la presenza sul tracciato del convoglio in quanto l'autopilot è attivo.";

            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, messaggioErrore,
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
            strcat(responseMessage, messaggioErrore);
            return BAD_REQUEST_400;
        }

        bool presenteSulTracciato = convoglioJson["isPresenteSulTracciato"];
        convoglio->setPresenteSulTracciato(presenteSulTracciato);

        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Aggiorno presenteSulTracciato: ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, presenteSulTracciato ? "true" : "false",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }

        strcat(responseMessage, "Letto parametro 'presenteSulTracciato'. ");

        aggiornamentoEffettuato = true;
    }

    if (convoglioJson["idSezioneCorrenteOccupata"].is<JsonVariant>()) {
        if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
            const char* messaggioErrore =
                "Impossibile cambiare l'ID della sezione corrente occupata in quanto l'autopilot è attivo.";

            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, messaggioErrore,
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
            strcat(responseMessage, messaggioErrore);
            return BAD_REQUEST_400;
        }

        IdSezioneTipo idSezioneCorrenteOccupata =
            leggoIdSezioneBody("idSezioneCorrenteOccupata", convoglioJson, responseBody);

        convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = idSezioneCorrenteOccupata;

        if (DEBUGGING_WEB_SERVER) {
            char* idSezioneCorrenteOccupataStringa = idSezioneCorrenteOccupata.toStringShort();
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Aggiorno ID sezione corrente occupata convoglio: ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, idSezioneCorrenteOccupataStringa,
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
        }

        strcat(responseMessage, "Letto parametro 'idSezioneCorrenteOccupata'. ");

        aggiornamentoEffettuato = true;

        // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
        fileConvogliDaAggiornare = true;
    }

    if (aggiornamentoEffettuato) {
        // Se ho trovato almeno un parametro valido restituisco un messaggio di successo
        creoResponseBodyInfoJson(responseBody, responseMessage, TIPOLOGIA_LOG_RESPONSE_BODY_INFO);

        loggerReale.cambiaTabulazione(-1);
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Salvo stato convoglio OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }

        return OK_200;
    } else {
        // Se non trovo nessun parametro valido, restituisco un messaggio di errore

        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Nessun parametro valido trovato.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
        creoResponseBodyInfoJson(responseBody, "Nessun parametro valido trovato.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        loggerReale.cambiaTabulazione(-1);
        return BAD_REQUEST_400;
    }
}

StatoHTTP WebServerCore::salvoStatoScambio(IdScambioTipo idScambio, char* requestBody, char* responseBody) {
    if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
        const char* messaggioErrore = "Impossibile cambiare lo stato dello scambio in quanto l'autopilot è attivo.";

        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, messaggioErrore,
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
        strcat(responseBody, messaggioErrore);
        return BAD_REQUEST_400;
    }

    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Salvo stato scambio",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
    loggerReale.cambiaTabulazione(1);
    // Invio lo stato dello scambio al client
    ScambioRelayAbstract* scambio = scambiRelayReale.getScambio(idScambio);

    JsonDocument documentoJson;
    bool risultatoDeserializzazione = deserializzoJson(documentoJson, requestBody, false);
    if (!risultatoDeserializzazione) {
        return BAD_REQUEST_400;
    }
    JsonObject scambioJson = documentoJson.as<JsonObject>();

    if (scambioJson["stato"].is<JsonVariant>()) {
        if (strcmp(scambioJson["stato"], "DESTRA") == 0) {
            scambio->azionaDestra();
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Aggiornato stato scambio: DESTRA",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }

        } else if (strcmp(scambioJson["stato"], "SINISTRA") == 0) {
            scambio->azionaSinistra();
            if (DEBUGGING_WEB_SERVER) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Aggiornato stato scambio: SINISTRA",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
        }

        creoResponseBodyInfoJson(responseBody, "Letto parametro 'stato'.", TIPOLOGIA_LOG_RESPONSE_BODY_INFO);

        loggerReale.cambiaTabulazione(-1);
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Salvo stato scambio OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
        return OK_200;
    }

    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Nessun parametro valido trovato",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
    creoResponseBodyInfoJson(responseBody, "Nessun parametro valido trovato", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
    loggerReale.cambiaTabulazione(-1);
    return BAD_REQUEST_400;
}

void WebServerCore::creoResponseBodyInfoJson(char* responseBody, const char* responseMessage,
                                             TipologiaLogResponseBody tipologiaLog) {
    JsonDocument doc;

    switch (tipologiaLog) {
        case TIPOLOGIA_LOG_RESPONSE_BODY_INFO:
            doc["Info"] = responseMessage;
            break;
        case TIPOLOGIA_LOG_RESPONSE_BODY_ERROR:
            doc["Error"] = responseMessage;
            break;
        default:
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Enumerazione non gestita.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    serializeJson(doc, responseBody, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);
}

// Uso questa funzione per creare un response body JSON con un risultato booleano.
void WebServerCore::creoResponseBodyBooleanResultJson(char* responseBody, bool result) {
    JsonDocument doc;

    doc["result"] = result;

    serializeJson(doc, responseBody, NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY);
}

// È possibile usare la funzione client->print fino a 2048 byte per volta (probabilmente per un limite
// hardware). Nel caso in cui il response body dovesse essere più lungo, è necessario sezionarlo e inviarlo in
// più parti.
void WebServerCore::invioAlClient(Client* client, char* responseBody) {
    const int dimensioneMassimaChunk = 2000;
    int lunghezzaResponseBody = strlen(responseBody);
    for (int i = 0; i < lunghezzaResponseBody; i += dimensioneMassimaChunk) {
        // Calcolo la lunghezza del chunk corrente. Se la lunghezza residua è minore di dimensioneMassimaChunk,
        // usa quella residua.
        int dimensioneChunkCorrente = ((i + dimensioneMassimaChunk) > lunghezzaResponseBody)
                                          ? (lunghezzaResponseBody - i)
                                          : dimensioneMassimaChunk;
        // Creo un buffer temporaneo per il chunk corrente
        char chunkBuffer[dimensioneChunkCorrente + 1];  // +1 per il carattere di terminazione
        // Copio il segmento corrente di responseBody nel buffer temporaneo
        strncpy(chunkBuffer, responseBody + i, dimensioneChunkCorrente);
        // Aggiungo il carattere di terminazione alla fine del chunk
        chunkBuffer[dimensioneChunkCorrente] = '\0';
        // Invio il chunk corrente al client
        client->print(chunkBuffer);
    }
}

bool WebServerCore::autentico(char* requestHeader) {
    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Autentico richiesta...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    bool successo = false;

    // Inizio cercando l'header "Authorization: Basic " nel requestHeader.
    char* carattereInizio;
    carattereInizio = strstr(requestHeader, "Authorization: Basic ");
    // Se non trovo la stringa, controllo la stessa stringa con la parola authorization in minuscolo.
    if (carattereInizio == NULL) {
        carattereInizio = strstr(requestHeader, "authorization: Basic ");
    }
    if (carattereInizio) {
        // Se trovo l'header, cerco il carattere di nuova linea per determinare la fine delle credenziali.
        char* carattereFine = strchr(carattereInizio, '\n');
        if (carattereFine) {
            // Calcolo la lunghezza delle credenziali escludendo "Authorization: Basic ".
            int lunghezzaCredenziali = carattereFine - (carattereInizio + strlen("Authorization: Basic ")) - 1;

            // Creo un buffer per contenere le credenziali codificate in Base64.
            char credenziali[lunghezzaCredenziali + 1];  // +1 per il terminatore null

            // Copio le credenziali nel buffer.
            strncpy(credenziali, carattereInizio + strlen("Authorization: Basic "), lunghezzaCredenziali);

            // Aggiungo il terminatore null alla fine del buffer.
            credenziali[lunghezzaCredenziali] = '\0';

            // A questo punto, dovrei decodificare le credenziali Base64 per verificarle.
            // Poiché Arduino non ha una funzione built-in per la decodifica Base64, dovrei implementarla o
            // usare una libreria esterna. Per questo esempio, suppongo che le credenziali "admin:password"
            // codificate in Base64 siano "YWRtaW46cGFzc3dvcmQ=".

            if (strcmp(credenziali, CREDENZIALI_AUTENTICAZIONE_BASE_WEBSERVER_BASE64) == 0) {
                // Se le credenziali corrispondono, ritorno true.
                successo = true;
            } else {
                if (DEBUGGING_WEB_SERVER) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Le credenziali non corrispondono!",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                successo = false;
            }
        }

    } else {
        // Se non trovo l'header Authorization o le credenziali non corrispondono, ritorno false.
        if (DEBUGGING_WEB_SERVER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Header Authorization non presente!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
        successo = false;
    }

    if (DEBUGGING_WEB_SERVER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Autentico richiesta... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
    return successo;
}

IdSezioneTipo WebServerCore::leggoIdSezioneBody(char* nomeParametroSezione, JsonObject body, char* responseBody) {
    IdSezioneTipo idSezione;

    // Inizializzo i parametri della sezione a 0, in questo modo quando c'è un errore nel recupero dell'ID della
    // sezione il chiamante se ne accorge.
    IdSezioneTipo idSezioneVuoto;
    idSezioneVuoto.tipologia = 0;
    idSezioneVuoto.id = 0;
    idSezioneVuoto.specifica = 0;

    JsonObject idSezioneCorrenteOccupataJsonObject;

    if (body[nomeParametroSezione].is<JsonVariant>()) {
        idSezioneCorrenteOccupataJsonObject = body[nomeParametroSezione].as<JsonObject>();

    } else {
        creoResponseBodyInfoJson(responseBody, "Parametro idSezione non trovato.", TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        return idSezioneVuoto;
    }

    if (idSezioneCorrenteOccupataJsonObject["tipologia"].is<JsonVariant>()) {
        idSezione.tipologia = idSezioneCorrenteOccupataJsonObject["tipologia"];
    } else {
        creoResponseBodyInfoJson(responseBody,
                                 "Parametro 'tipologia' dell'oggetto idSezioneCorrenteOccupata non trovato.",
                                 TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        return idSezioneVuoto;
    }

    if (idSezioneCorrenteOccupataJsonObject["id"].is<JsonVariant>()) {
        idSezione.id = idSezioneCorrenteOccupataJsonObject["id"];
    } else {
        creoResponseBodyInfoJson(responseBody, "Parametro 'id' dell'oggetto idSezioneCorrenteOccupata non trovato.",
                                 TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        return idSezioneVuoto;
    }

    if (idSezioneCorrenteOccupataJsonObject["specifica"].is<JsonVariant>()) {
        idSezione.specifica = idSezioneCorrenteOccupataJsonObject["specifica"];
    } else {
        creoResponseBodyInfoJson(responseBody,
                                 "Parametro 'specifica' dell'oggetto idSezioneCorrenteOccupata non trovato.",
                                 TIPOLOGIA_LOG_RESPONSE_BODY_ERROR);
        return idSezioneVuoto;
    }

    return idSezione;
}

// *** DEFINIZIONE VARIABILI *** //

WebServerCore webServerCore = WebServerCore();