// *** INCLUDE *** //

#include "SensoreTensioneAbstract.h"

#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void SensoreTensioneAbstract::effettuaLetturaAggiornaUnaEntryCampionamento() {
    int val1 = leggoPinAnalogico();
    TensioneTipo val2 = (TensioneTipo) ( val1 * 16.5 / 1024);

    bufferTensione[indiceAttualeBufferStatoSensoriTensione] = val2;
    indiceAttualeBufferStatoSensoriTensione++;
    if (indiceAttualeBufferStatoSensoriTensione == NUMERO_CAMPIONI_SENSORI_TENSIONE) {
        indiceAttualeBufferStatoSensoriTensione = 0;
    }
}

void SensoreTensioneAbstract::effettuaLetturaAggiornaTutteEntryCampionamento() {
    for (int i = 0; i < NUMERO_CAMPIONI_SENSORI_TENSIONE; i++) {
        effettuaLetturaAggiornaUnaEntryCampionamento();
    }
}

char* SensoreTensioneAbstract::toString() {
    std::string stringa;

    stringa += "Stato sensore tensioneMinimaCorrettoFunzionamento: ";
    stringa += std::to_string(tensione);
    stringa += "V\n";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

TensioneTipo SensoreTensioneAbstract::misuraMediaCampioni() {
    TensioneTipo somma = 0;

    for (int i = 0; i < NUMERO_CAMPIONI_SENSORI_TENSIONE; i++) {
        somma += bufferTensione[i];
    }

    TensioneTipo mediaFinale = somma / NUMERO_CAMPIONI_SENSORI_TENSIONE;

    return mediaFinale;
}

int SensoreTensioneAbstract::leggoPinAnalogico() { return pinSensore.legge(); }

TensioneTipo SensoreTensioneAbstract::getTensione() {
    TensioneTipo tensioneLetta = misuraMediaCampioni();
    return tensioneLetta;
}