// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/pulsanteEmergenza/PulsanteEmergenzaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

DigitalPinReale pinPulsanteEmergenza(PIN_PULSANTE_EMERGENZA);
PulsanteEmergenzaAbstract pulsanteEmergenzaReale{pinPulsanteEmergenza, loggerReale};