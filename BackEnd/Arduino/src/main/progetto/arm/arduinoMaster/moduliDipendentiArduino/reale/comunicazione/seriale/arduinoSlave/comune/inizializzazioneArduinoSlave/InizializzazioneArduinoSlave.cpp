// *** INCLUDE *** //

#include "InizializzazioneArduinoSlave.h"

#include "Arduino.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoMaster/inizializzazioneArduinoMaster/InizializzazioneArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/comune/resetArduinoSlave/ResetArduinoSlave.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/PorteSeriali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/Timestamp.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void inizializzoArduinoSlave() {
    pinMode(PIN_RESET_ARDUINO_SLAVE_RELAY, OUTPUT);
    pinMode(PIN_RESET_ARDUINO_SLAVE_SENSORI, OUTPUT);

    resetArduinoSlaveRelay();
    resetArduinoSlaveSensori();

    // Aspetto il boot dei due Arduino Slave
    delay(ATTESA_BOOT_ARDUINO_SLAVE);

    bool successo;

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Arduino Slave Relay...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    successo = inizializzoPortaSerialeArduinoMaster(PORTA_SERIALE_ARDUINO_SLAVE_RELAY, VELOCITA_PORTA_SERIALE_ARDUINO,
                                                    true, false);
    if (!successo) {
        displayReale.errori.generico.inizializzazionePortaSeriale(PORTA_SERIALE_ARDUINO_SLAVE_RELAY);
        programmaReale.ferma(false);
    }
    timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp = millis();

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Arduino Slave Sensori...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    successo = inizializzoPortaSerialeArduinoMaster(PORTA_SERIALE_ARDUINO_SLAVE_SENSORI, VELOCITA_PORTA_SERIALE_ARDUINO,
                                                    true, false);
    timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp = millis();
    if (!successo) {
        displayReale.errori.generico.inizializzazionePortaSeriale(PORTA_SERIALE_ARDUINO_SLAVE_SENSORI);
        programmaReale.ferma(false);
    }
}