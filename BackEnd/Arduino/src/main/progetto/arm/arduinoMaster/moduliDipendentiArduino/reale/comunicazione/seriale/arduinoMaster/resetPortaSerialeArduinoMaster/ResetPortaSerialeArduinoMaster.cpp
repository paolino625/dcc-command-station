// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoMaster/inizializzazioneArduinoMaster/InizializzazioneArduinoMaster.h"
#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/PorteSeriali.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINIZIONE METODI *** //

void resettoPortaSerialeArduinoMaster(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                      bool portaSerialeDaVerificare, bool beepAttivato) {
    if (beepAttivato) {
        buzzerRealeArduinoMaster.beepAvviso();
    }

    chiudoPortaSerialeArduinoGigaArduinoMaster(numeroPortaSeriale);

    inizializzoPortaSerialeArduinoMaster(numeroPortaSeriale, velocitaPorta, portaSerialeDaVerificare, beepAttivato);
}