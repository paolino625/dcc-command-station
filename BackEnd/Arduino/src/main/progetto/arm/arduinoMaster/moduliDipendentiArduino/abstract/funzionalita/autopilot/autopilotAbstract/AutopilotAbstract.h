#ifndef DCC_COMMAND_STATION_AUTOPILOT_H
#define DCC_COMMAND_STATION_AUTOPILOT_H

// *** INCLUDE *** //

#include "ModalitaAutopilot.h"
#include "StatoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/stopEmergenza/StopEmergenzaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** CLASSE *** //

class AutopilotAbstract {
    // *** VARIABILI *** //

   public:
    ConvogliAbstract &convogli;
    SensoriPosizioneAbstract &sensoriPosizione;
    SezioniAbstract &sezioni;
    InvioStatoTracciatoAbstract &invioStatoTracciato;
    StopEmergenzaAbstract &stopEmergenza;

   private:
    LoggerAbstract &logger;

   public:
    // Variabili relative alla modalità MODALITA_AUTOPILOT_APPROCCIO_CASUALE_SCELTA_CONVOGLI
    bool modalitaAutopilotApproccioCasualeSceltaConvogliInizializzata = false;
    IdConvoglioTipo *convogliSceltiModalitaAutopilotApproccioCasuale = nullptr;
    int numeroConvogliSceltiModalitaAutopilotApproccioCasuale;

    // *** COSTRUTTORE *** //

    AutopilotAbstract(ConvogliAbstract &convogli, SensoriPosizioneAbstract &sensoriPosizione, SezioniAbstract &sezioni,
                      InvioStatoTracciatoAbstract &invioStatoTracciato,StopEmergenzaAbstract &stopEmergenza, LoggerAbstract &logger)
        : convogli(convogli),
          sensoriPosizione(sensoriPosizione),
          sezioni(sezioni),
          invioStatoTracciato(invioStatoTracciato), stopEmergenza(stopEmergenza),
          logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

   public:
    bool avviaAutopilotModalitaStazioneSuccessiva();
    bool avviaAutopilotModalitaCasuale(bool ottimizzazioneAssegnazioneConvogliAiBinari);
    bool avviaAutopilotModalitaCasualeSceltaConvogli(IdConvoglioTipo *arrayIdConvogli, int numeroIdConvogli,
                                                     bool ottimizzazioneAssegnazioneConvogliAiBinari);
    void ferma(bool shutdownForzato, bool inviaPacchettoTracciatoStopNelCasoDiShutdownForzato);
    void processo();

   private:
    void inizializzoStatoSezioni();
    void eseguoAutopilotConvogli();
    bool convogliPresentiSulTracciatoInizializzati();
    bool inizializzaModalitaAutopilotApproccioCasualeSceltaConvogli(IdConvoglioTipo *arrayIdConvogli,
                                                                    int numeroIdConvogli);
    void spengoDefinitivamente();
    void aggiornoStatoInizializzazioneAutopilotConvogli();
    void resetAutopilot();
    void controlloPresenzaTrenoFantasma();
};

#endif
