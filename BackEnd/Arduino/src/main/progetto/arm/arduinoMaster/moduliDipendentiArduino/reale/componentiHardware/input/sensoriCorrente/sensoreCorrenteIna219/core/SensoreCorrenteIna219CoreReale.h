#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA219COREREALE_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA219COREREALE_H

// *** INCLUDE *** //

#include "Adafruit_INA219.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/core/SensoreCorrenteIna219CoreAbstract.h"

// *** CLASSE *** //

class SensoreCorrenteIna219CoreReale : public SensoreCorrenteIna219CoreAbstract {
    // *** VARIABILI *** //

    Adafruit_INA219 ina219;

    // *** METODI *** //

   public:
    bool begin();
    float getCurrent_mA();
    float getBusVoltage_V();
    float getShuntVoltage_mV();
};

// *** DICHIARAZIONE VARIABILI *** //

extern SensoreCorrenteIna219CoreReale sensoreCorrenteIna219CoreReale;

#endif