#ifndef DCC_COMMAND_STATION_MENUDISPLAYKEYPADCORE_H
#define DCC_COMMAND_STATION_MENUDISPLAYKEYPADCORE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/controlloCortoCircuitoTracciato/ControlloCortoCircuitoTracciatoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/ScambiRelayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/menuDisplayKeypad/operazioniMultiple/MenuDisplayKeypadOperazioniMultipleAbstract.h"

// *** CLASSE *** //

class MenuDisplayKeypadCore {
    // *** VARIABILI *** //

    ScambiRelayAbstract &scambiRelay;

   public:
    MenuDisplayKeypadCore(ScambiRelayAbstract &scambiRelay) : scambiRelay{scambiRelay} {}

    IdConvoglioTipo *convoglioCorrenteDisplay;

    // *** DICHIARAZIONE METODI *** //

    void gestiscoMenuPrincipale(char sceltaMenu, MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypad);

   public:
    void assegnoConvogliInUsoDisplay();
    void gestiscoDisplayKeypad(TempoAbstract &tempo, KeypadAbstract &keypad, DisplayAbstract &display,
                               ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuito,
                               MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypadOperazioniMultiple,
                               UtentiAbstract &utenti);
};

// *** DICHIARAZIONE VARIABILI *** //

extern MenuDisplayKeypadCore menuDisplayKeypadCoreReale;

#endif