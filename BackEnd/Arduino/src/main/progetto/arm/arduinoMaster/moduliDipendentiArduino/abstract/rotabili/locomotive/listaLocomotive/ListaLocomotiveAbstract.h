#ifndef DCC_COMMAND_STATION_LISTALOCOMOTIVEABSTRACT_H
#define DCC_COMMAND_STATION_LISTALOCOMOTIVEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_PER_LOCOMOTIVA_LISTA 40

// *** DICHIARAZIONE FUNZIONI *** //

class ListaLocomotiveAbstract {
   public:
    // *** VARIABILI *** //

    char* listaLocomotive[NUMERO_LOCOMOTIVE];
    LocomotiveAbstract& locomotive;
    ConvogliAbstract& convogli;
    ProgrammaAbstract& programma;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

    ListaLocomotiveAbstract(LocomotiveAbstract& locomotive, ConvogliAbstract& convogli, ProgrammaAbstract& programma,
                            LoggerAbstract& logger)
        : locomotive(locomotive), convogli(convogli), programma(programma), logger(logger) {}

    // *** DEFINIZIONE METODI *** //

   public:
    void inizializza();
    int crea(bool mostraLocomotiveInUso, bool mostraLocomotiveNonInUso);
    char** get() { return listaLocomotive; }
};

#endif
