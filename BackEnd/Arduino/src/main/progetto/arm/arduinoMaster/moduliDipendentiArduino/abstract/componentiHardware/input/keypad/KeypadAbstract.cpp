// *** INCLUDE *** //

#include "KeypadAbstract.h"

// *** DEFINIZIONE METODI *** //

int KeypadAbstract::leggoPosizioneConvoglioDisplay(bool stampaNumeroInserito) {
    int numeroConvoglio = leggoNumero1Cifra(stampaNumeroInserito);
    return numeroConvoglio;
}

IdConvoglioTipo KeypadAbstract::leggoIdConvoglioEVerifico(bool convogliInUso, bool convogliNonInUso) {
    int idConvoglio = leggoPosizioneConvoglioDisplay(false);

    // Key Escape
    if (idConvoglio == 0) {
        return 0;
    }

    if (idConvoglio == -1) {
        return -1;
    }

    ConvoglioAbstractReale* convoglio = convogli.getConvoglio(idConvoglio, false);

    // Verifico che l'input dell'utente corrisponde a un convoglio accettabile
    bool condizione = false;

    if (convogliInUso && !convogliNonInUso) {
        condizione = convoglio->isPresenteSulTracciato();
    } else if (!convogliInUso && convogliNonInUso) {
        condizione = !convoglio->isPresenteSulTracciato();
    } else if (convogliInUso && convogliNonInUso) {
        condizione = convoglio->isPresenteSulTracciato() || !convoglio->isPresenteSulTracciato();
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Input non validi.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

        programma.ferma(true);
    }

    // Se è stato inserito un indirizzo non valido esco
    if (!condizione) {
        // displayInputNonValido();
        // fuoriMenu = FUORI_MENU_NO_INDICE;
        return -1;
    }

    return idConvoglio;
}

IdLocomotivaTipo KeypadAbstract::leggoIdLocomotiva(bool stampaNumeroInserito) {
    IdLocomotivaTipo idLocomotiva = leggoNumero1Cifra(stampaNumeroInserito);

    return idLocomotiva;
}

IdLocomotivaTipo KeypadAbstract::leggoIdLocomotivaEVerifico(bool locomotiveInUso, bool locomotiveNonInUso) {
    IdLocomotivaTipo idLocomotiva = leggoIdLocomotiva(false);

    // Key Escape
    if (idLocomotiva == 0) {
        return 0;
    }

    if (idLocomotiva == -1 || !locomotive.esisteLocomotiva(idLocomotiva)) {
        return -1;
    }

    // Verifico che l'input dell'utente corrisponde a una locomotiva accettabile
    bool condizione = false;
    if (locomotiveInUso && !locomotiveNonInUso) {
        condizione = convogli.isLocomotivaInUso(idLocomotiva);
    } else if (!locomotiveInUso && locomotiveNonInUso) {
        condizione = !convogli.isLocomotivaInUso(idLocomotiva);
    } else if (locomotiveInUso && locomotiveNonInUso) {
        condizione = convogli.isLocomotivaInUso(idLocomotiva) || !convogli.isLocomotivaInUso(idLocomotiva);
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Input non validi.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }

    // Se è stato inserito un indirizzo non valido esco, a meno che non sia 0 che è il key escape
    if (!condizione && idLocomotiva != 0) {
        // displayInputNonValido();
        // fuoriMenu = FUORI_MENU_NO_INDICE;
        return -1;
    }

    return idLocomotiva;
}

IdVagoneTipo KeypadAbstract::leggoIdVagone(bool stampaNumeroInserito) {
    int idVagone = leggoNumero1Cifra(stampaNumeroInserito);
    if (idVagone == -1) {
        return -1;
    }
    return idVagone;
}

IdVagoneTipo KeypadAbstract::leggoIdVagoneEVerifico(bool vagoniInUso, bool vagoniNonInUso, VagoniAbstract& vagoni) {
    int idVagone = leggoIdVagone(false);

    // Key Escape
    if (idVagone == 0) {
        return 0;
    }

    if (idVagone == -1) {
        return -1;
    }

    // Verifico che l'input dell'utente corrisponde a un vagone accettabile
    bool condizione = false;

    if (vagoniInUso && !vagoniNonInUso) {
        condizione = convogli.isVagoneInUso(vagoni.getVagone(idVagone));
    } else if (!vagoniInUso && vagoniNonInUso) {
        condizione = !convogli.isVagoneInUso(vagoni.getVagone(idVagone));
    } else if (vagoniInUso && vagoniNonInUso) {
        condizione =
            convogli.isVagoneInUso(vagoni.getVagone(idVagone)) || !convogli.isVagoneInUso(vagoni.getVagone(idVagone));
        ;
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Input non validi.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }

    // Se è stato inserito un indirizzo non valido esco
    if (!condizione) {
        // displayInputNonValido();
        // fuoriMenu = FUORI_MENU_NO_INDICE;
        return -1;
    }

    return idVagone;
}

IndirizzoDecoderTipo KeypadAbstract::leggoIndirizzoLocomotiva() {
    int indirizzoDecoder = leggoNumero2Cifre(true);

    return indirizzoDecoder;
}

StepVelocitaDecoderTipo KeypadAbstract::leggoStepVelocitaEVerifico() {
    StepVelocitaDecoderTipo stepVelocita = leggoStepVelocita();
    if (stepVelocita == 0) {
        return 0;
    }
    if (stepVelocita >= 1 && stepVelocita <= 126) {
        return stepVelocita;
    } else {
        return -1;
    }
}

StepVelocitaDecoderTipo KeypadAbstract::leggoStepVelocita() {
    StepVelocitaDecoderTipo stepVelocita = leggoNumero3Cifre(true);

    return stepVelocita;
}

IdSensorePosizioneTipo KeypadAbstract::leggoNumeroSensore() {
    int numeroSensore = leggoNumero2Cifre(true);
    return numeroSensore;
}

IdScambioTipo KeypadAbstract::leggoNumeroScambio() {
    IdScambioTipo numeroScambio = leggoNumero2Cifre(true);

    return numeroScambio;
}

int KeypadAbstract::leggoLivelloVolume() {
    int volume = leggoNumero1Cifra(true);
    return volume;
}

int KeypadAbstract::leggoMoltiplicatoreKeepAlive() {
    int moltiplicatoreKeepAlive = leggoNumero3Cifre(true);

    if (moltiplicatoreKeepAlive == 0 || moltiplicatoreKeepAlive > 255) {
        return 0;
    }

    else if (moltiplicatoreKeepAlive == -1) {
        return -1;
    }

    return moltiplicatoreKeepAlive;
}

int KeypadAbstract::leggoSceltaMenuUtente() {
    int sceltaMenu = leggoNumero1Cifra(false);
    return sceltaMenu;
}

int KeypadAbstract::keyPadLeggoNumero1CifraTempoLimite(bool stampaNumeroInserito, int tempoLimiteInput) {
    char primaCifra;
    int primaCifraInt;

    primaCifra = keypadCore.leggoCarattereTempoLimite(tempoLimiteInput);

    if (primaCifra == '-') {
        return -1;
    }

    if (stampaNumeroInserito) {
        displayCore.getDisplayComponente().print(primaCifra);
    }

    primaCifraInt = int(primaCifra) - 48;
    return primaCifraInt;
}

int KeypadAbstract::leggoNumero1Cifra(bool stampaNumeroInserito) {
    char primaCifra;
    int primaCifraInt;

    primaCifra = keypadCore.leggoCarattere();

    if (primaCifra == '-') {
        return -1;
    }

    if (stampaNumeroInserito) {
        // Devo trasformare la cifra in stringa altrimenti viene stampato il valore ASCII
        displayCore.getDisplayComponente().print(primaCifra);
    }

    primaCifraInt = int(primaCifra) - 48;
    return primaCifraInt;
}

// Quando leggeStato numeri da 2 o 3 cifre, utilizzo delayReale.
// Sarebbe corretto mantenere la logica di loop continuo anche in questo caso, ma si complicherebbe
// un bel po'
int KeypadAbstract::leggoNumero2Cifre(bool stampaNumeroInserito) {
    int risultato;

    int primaCifra = keyPadLeggoNumero1CifraTempoLimite(stampaNumeroInserito, TEMPO_LIMITE_INPUT_KEYPAD_SOTTOMENU);
    if (primaCifra == -1) {
        return -1;
    }

    int secondaCifra = keyPadLeggoNumero1CifraTempoLimite(stampaNumeroInserito, TEMPO_LIMITE_INPUT_KEYPAD_SOTTOMENU);
    if (secondaCifra == -1) {
        risultato = primaCifra;
        return risultato;
    }

    risultato = primaCifra * 10 + secondaCifra;

    return risultato;
}

int KeypadAbstract::leggoNumero3Cifre(bool stampaNumeroInserito) {
    int risultato;

    int primaCifra = keyPadLeggoNumero1CifraTempoLimite(stampaNumeroInserito, TEMPO_LIMITE_INPUT_KEYPAD_SOTTOMENU);
    if (primaCifra == -1) {
        return -1;
    }

    int secondaCifra = keyPadLeggoNumero1CifraTempoLimite(stampaNumeroInserito, TEMPO_LIMITE_INPUT_KEYPAD_SOTTOMENU);
    if (secondaCifra == -1) {
        risultato = primaCifra;
        return risultato;
    }

    int terzaCifra = keyPadLeggoNumero1CifraTempoLimite(stampaNumeroInserito, TEMPO_LIMITE_INPUT_KEYPAD_SOTTOMENU);
    if (terzaCifra == -1) {
        risultato = primaCifra * 10 + secondaCifra;
        return risultato;
    }

    risultato = primaCifra * 100 + secondaCifra * 10 + terzaCifra;
    return risultato;
}