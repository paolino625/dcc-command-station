#ifndef ARDUINO_RETRIEVEPERCORSOHELPER_H
#define ARDUINO_RETRIEVEPERCORSOHELPER_H

// *** INCLUDE *** //

#include "ArduinoJson.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/arduinoMasterHelper/ArduinoMasterHelperAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/percorso/PercorsoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"

// *** CLASSE *** //

// Classe helper che si occupa di effettuare la chiamata REST e fare deserializzazione del JSON di risposta
class ArduinoMasterHelperReale : public ArduinoMasterHelperAbstract {
    // *** DICHIARAZIONE VARIABILI *** //

    DisplayAbstract& display;
    ProgrammaAbstract& programma;

    // *** COSTRUTTORE *** //

   public:
    ArduinoMasterHelperReale(DisplayAbstract& display, ProgrammaAbstract& programma)
        : display(display), programma(programma) {}

    // *** DICHIARAZIONE METODI *** //

   public:
    bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo) override;
    bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo,
                        bool direzionePrimaLocomotivaDestra) override;
    PercorsoAbstract* getPercorso(IdSezioneTipo idSezionePartenzaRequest,
                                  IdSezioneTipo idSezioneArrivoRequest) override;
    bool verificaConnessione() override;
    bool reset() override;
};

// *** DICHIARAZIONE VARIABILI *** //

extern ArduinoMasterHelperReale arduinoMasterHelperReale;

#endif
