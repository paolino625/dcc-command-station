// *** INCLUDE *** //

#include "StatoAutopilotModalitaStazioneSuccessiva.h"

// *** DEFINIZIONE FUNZIONI *** //

const char *getStatoAutopilotModalitaStazioneSuccessivaAsAString(
    StatoAutopilotModalitaStazioneSuccessiva statoAutopilotModalitaStazioneSuccessivaLocale, LoggerAbstract &logger) {
    switch (statoAutopilotModalitaStazioneSuccessivaLocale) {
        case StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA:
            return "FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA";
        case StatoAutopilotModalitaStazioneSuccessiva::
            FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA:
            return "FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_"
                   "STAZIONE_"
                   "CORRENTE_A_STAZIONE_SUCCESSIVA";
        case StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO:
            return "FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO";
        case StatoAutopilotModalitaStazioneSuccessiva::
            FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE:
            return "FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_"
                   "PROSSIMA_SEZIONE_CAPIENTE";
        case StatoAutopilotModalitaStazioneSuccessiva::IN_PARTENZA_STAZIONE:
            return "IN_PARTENZA_STAZIONE";
        case StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE:
            return "IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE";
        case StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO:
            return "IN_MOVIMENTO";
        case StatoAutopilotModalitaStazioneSuccessiva::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI:
            return "FERMO_SEZIONE_ATTESA_LOCK_SEZIONI";
        case StatoAutopilotModalitaStazioneSuccessiva::ARRIVATO_STAZIONE_DESTINAZIONE:
            return "ARRIVATO_STAZIONE_DESTINAZIONE";
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Enumerazione non mappata nella funzione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
    }
}