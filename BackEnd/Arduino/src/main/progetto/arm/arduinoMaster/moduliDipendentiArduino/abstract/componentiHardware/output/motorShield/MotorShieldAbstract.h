#ifndef DCC_COMMAND_STATION_MOTORSHIELDABSTRACT_H
#define DCC_COMMAND_STATION_MOTORSHIELDABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/DigitalPinAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define TEMPO_ACCENSIONE_MOTOR_SHIELD_MILLISECONDI 10

// *** CLASSE *** //

class MotorShieldAbstract {
   private:
    // *** VARIABILI *** //

    DigitalPinAbstract& pinPwm;
    DigitalPinAbstract& pinDir1;
    bool accesa;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    MotorShieldAbstract(DigitalPinAbstract& pinPwm, DigitalPinAbstract& pinDir1, LoggerAbstract& logger)
        : pinPwm{pinPwm}, pinDir1{pinDir1}, logger{logger} {}

    // *** DEFINIZIONE METODI *** //

    void inizializza();
    void accende();
    void spegne();
    bool isAccesa() const;
};

#endif