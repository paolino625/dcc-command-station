#ifndef DCC_COMMAND_STATION_PATHFILE_H
#define DCC_COMMAND_STATION_PATHFILE_H

// *** DEFINE *** //

#define NUMERO_CARATTERI_FILE_SCAMBI (NUMERO_SCAMBI * 3)

// *** DICHIARAZIONE VARIABILI *** //

extern char pathParzialeFileLocomotiva[];
extern char pathFileConvogli[];
extern char pathFileVagoni[];
extern char pathFileScambi[];
extern char pathFileUtenti[];

extern char pathFileAudioTest1[];
extern char pathFileAudioTest2[];
extern char pathFileAudioTest3[];
extern char pathFileAudioTest4[];
extern char pathFileAudioTest5[];

#endif
