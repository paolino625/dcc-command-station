// *** INCLUDE *** //

#include "ConvoglioDTOFactory.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/reale/ConvoglioAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE FUNZIONI *** //

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
ConvoglioResponseDTO* ConvoglioDTOFactory::creaConvoglioResponseDTO(IdConvoglioTipo idConvoglio) {
    if (DEBUGGING_FACTORY) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Creo ConvoglioResponseDTO con ID: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idConvoglio).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    ConvoglioAbstractReale* convoglio = convogliReali.getConvoglio(idConvoglio, false);
    ConvoglioResponseDTO* convoglioResponseDTO = new ConvoglioResponseDTO();

    convoglioResponseDTO->id = convoglio->getId();
    strcpy(convoglioResponseDTO->nome, convoglio->getNome());
    convoglioResponseDTO->idLocomotiva1 = convoglio->getIdLocomotiva1();
    convoglioResponseDTO->idLocomotiva2 = convoglio->getIdLocomotiva2();
    convoglioResponseDTO->velocitaMassima = convoglio->getVelocitaMassima();
    convoglioResponseDTO->velocitaImpostata = convoglio->getVelocitaImpostata();
    convoglioResponseDTO->velocitaAttuale = convoglio->getVelocitaAttuale();
    convoglioResponseDTO->luciAccese = convoglio->isLuciAccese();
    convoglioResponseDTO->presenteSulTracciato = convoglio->isPresenteSulTracciato();

    // ** AUTOPILOT ** //

    Autopilot autopilot;

    // * POSIZIONE CONVOGLIO * //

    autopilot.posizioneConvoglio.validita = convoglio->autopilotConvoglio->posizioneConvoglio.validita;
    autopilot.posizioneConvoglio.idSezioneCorrenteOccupata =
        convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata;
    autopilot.posizioneConvoglio.direzionePrimaLocomotivaDestra =
        convoglio->autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra;

    // * STATO INIZIALIZZAZIONE AUTOPILOT CONVOGLIO * //

    autopilot.statoInizializzazioneAutopilotConvoglio =
        convoglio->autopilotConvoglio->statoInizializzazioneAutopilotConvoglio;

    // * STATO AUTOPILOT MODALITA STAZIONE SUCCESSIVA * //

    autopilot.statoAutopilotModalitaStazioneSuccessiva =
        convoglio->autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva;

    // * STATO AUTOPILOT MODALITA APPROCCIO CASUALE * //

    autopilot.statoAutopilotModalitaApproccioCasuale =
        convoglio->autopilotConvoglio->statoAutopilotModalitaApproccioCasuale;

    convoglioResponseDTO->autopilot = autopilot;

    return convoglioResponseDTO;
}

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
char* ConvoglioDTOFactory::converteConvoglioResponseDTOToStringJson(ConvoglioResponseDTO* convoglioDTO) {
    JsonDocument bufferJson;
    bufferJson["id"] = convoglioDTO->id;
    bufferJson["nome"] = convoglioDTO->nome;
    bufferJson["idLocomotiva1"] = convoglioDTO->idLocomotiva1;
    bufferJson["idLocomotiva2"] = convoglioDTO->idLocomotiva2;
    bufferJson["velocitaMassima"] = convoglioDTO->velocitaMassima;
    bufferJson["velocitaImpostata"] = convoglioDTO->velocitaImpostata;
    bufferJson["velocitaAttuale"] = convoglioDTO->velocitaAttuale;
    bufferJson["luciAccese"] = convoglioDTO->luciAccese;
    bufferJson["presenteSulTracciato"] = convoglioDTO->presenteSulTracciato;

    // ** AUTOPILOT ** //

    JsonObject autopilotJsonObject = bufferJson["autopilot"].to<JsonObject>();

    // * POSIZIONE CONVOGLIO * //

    JsonObject posizioneConvoglioJsonObject = autopilotJsonObject["posizioneConvoglio"].to<JsonObject>();

    posizioneConvoglioJsonObject["validita"] =
        getValiditaPosizioneConvoglioAsAString(convoglioDTO->autopilot.posizioneConvoglio.validita, loggerReale);

    JsonObject idSezioneCorrenteOccupataJsonObject =
        posizioneConvoglioJsonObject["idSezioneCorrenteOccupata"].to<JsonObject>();

    idSezioneCorrenteOccupataJsonObject["tipologia"] =
        convoglioDTO->autopilot.posizioneConvoglio.idSezioneCorrenteOccupata.tipologia;
    idSezioneCorrenteOccupataJsonObject["id"] = convoglioDTO->autopilot.posizioneConvoglio.idSezioneCorrenteOccupata.id;
    idSezioneCorrenteOccupataJsonObject["specifica"] =
        convoglioDTO->autopilot.posizioneConvoglio.idSezioneCorrenteOccupata.specifica;

    posizioneConvoglioJsonObject["direzionePrimaLocomotivaDestra"] =
        convoglioDTO->autopilot.posizioneConvoglio.direzionePrimaLocomotivaDestra;

    // * STATO INIZIALIZZAZIONE AUTOPILOT CONVOGLIO * //

    autopilotJsonObject["statoInizializzazioneAutopilotConvoglio"] =
        getStatoInizializzazioneAutopilotConvoglioAsAString(
            convoglioDTO->autopilot.statoInizializzazioneAutopilotConvoglio, loggerReale);

    // * STATO AUTOPILOT MODALITA STAZIONE SUCCESSIVA * //

    autopilotJsonObject["statoAutopilotModalitaStazioneSuccessiva"] =
        getStatoAutopilotModalitaStazioneSuccessivaAsAString(
            convoglioDTO->autopilot.statoAutopilotModalitaStazioneSuccessiva, loggerReale);

    // * STATO AUTOPILOT MODALITA APPROCCIO CASUALE * //

    autopilotJsonObject["statoAutopilotModalitaApproccioCasuale"] = getStatoAutopilotModalitaApproccioCasualeAsAString(
        convoglioDTO->autopilot.statoAutopilotModalitaApproccioCasuale, loggerReale);

    serializzoJson(bufferJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_CONVOGLIO_STRINGA_JSON, true);
    return stringaJsonUsbBuffer;
}

// *** DEFINIZIONE VARIABILI *** //

ConvoglioDTOFactory convoglioDTOFactory = ConvoglioDTOFactory();