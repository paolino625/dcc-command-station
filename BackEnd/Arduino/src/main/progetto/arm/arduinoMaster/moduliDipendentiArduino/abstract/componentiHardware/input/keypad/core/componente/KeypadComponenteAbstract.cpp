// *** INCLUDE *** //

#include "KeypadComponenteAbstract.h"

// *** DEFINIZIONE METODI *** //

void KeypadComponenteAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo keypad... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    // Imposto i pin delle colonne del keypad come OUTPUT
    for (int i = 0; i < 4; i++) {
        pinColonneKeypad[i]->setOutputMode();
        pinColonneKeypad[i]->setHigh();
    }

    // Imposto i pin delle righe del keypad come INPUT_PULLUP
    for (int i = 0; i < 4; i++) {
        pinRigheKeypad[i]->setInputPullupMode();
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo keypad... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

char KeypadComponenteAbstract::leggeCarattere() {
    char carattereRisultato = '-';
    for (int c = 0; c < NUMERO_COLONNE; c++) {
        pinColonneKeypad[c]->setLow();
        delay.microseconds(KEYPAD_SETTLING_DELAY);

        for (int r = 0; r < NUMERO_RIGHE; r++) {
            bool tastoPremuto = !pinRigheKeypad[r]->isHigh();
            if (tastoPremuto) {
                carattereRisultato = tastiKeypad[r][c];
            }
        }
        pinColonneKeypad[c]->setHigh();
    }
    return carattereRisultato;
}