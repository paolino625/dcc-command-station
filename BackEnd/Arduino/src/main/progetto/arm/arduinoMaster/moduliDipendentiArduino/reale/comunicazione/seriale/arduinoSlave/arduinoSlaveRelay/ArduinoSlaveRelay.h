#ifndef DCC_COMMAND_STATION_ARDUINOSLAVERELAY_H
#define DCC_COMMAND_STATION_ARDUINOSLAVERELAY_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/Ping.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoMaster/resetPortaSerialeArduinoMaster/ResetPortaSerialeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/PorteSeriali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/Timestamp.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/arduinoSlave/comune/controlloPing/ControlloPing.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

#define NUMERO_ELEMENTI_CODA_ARDUINO_SLAVE_RELAY 10

// *** CLASSE *** //

class ArduinoSlaveRelay {
    // *** CLASSE ANNIDATA *** //

    class CodaRelay {
        // Coda dei relay da inviare ad Arduino Slave Relay

        // *** VARIABILI *** //

        byte codaRelayDaInviare[NUMERO_ELEMENTI_CODA_ARDUINO_SLAVE_RELAY];

        byte indiceCodaRelayDaInviareSalvataggioArduinoSlaveRelay;
        byte indiceCodaRelayDaInviareAttualeArduinoSlaveRelay;

        LoggerAbstract &logger;

       public:
        CodaRelay(LoggerAbstract &logger) : logger{logger} {}

        // *** METODI PUBBLICI *** //

        void aggiungeRelay(int numeroRelay) {
            if (DEBUGGING_CODA_RELAY_ARDUINO_SLAVE_RELAY) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Aggiungo relay n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(numeroRelay).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                      " alla coda dei relay da inviare ad Arduino Slave Relay",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }

            codaRelayDaInviare[indiceCodaRelayDaInviareSalvataggioArduinoSlaveRelay] = numeroRelay;
            indiceCodaRelayDaInviareSalvataggioArduinoSlaveRelay += 1;
            if (indiceCodaRelayDaInviareSalvataggioArduinoSlaveRelay == NUMERO_ELEMENTI_CODA_ARDUINO_SLAVE_RELAY) {
                indiceCodaRelayDaInviareSalvataggioArduinoSlaveRelay = 0;
            }
        }

        bool isVuota() {
            if (indiceCodaRelayDaInviareAttualeArduinoSlaveRelay ==
                indiceCodaRelayDaInviareSalvataggioArduinoSlaveRelay) {
                return true;
            } else {
                return false;
            }
        }

        int getElemento() {
            int elemento = codaRelayDaInviare[indiceCodaRelayDaInviareAttualeArduinoSlaveRelay];
            // Tolgo l'elemento dalla coda
            tolgoElemento();
            return elemento;
        }

       private:
        void tolgoElemento() {
            indiceCodaRelayDaInviareAttualeArduinoSlaveRelay += 1;
            if (indiceCodaRelayDaInviareAttualeArduinoSlaveRelay == NUMERO_ELEMENTI_CODA_ARDUINO_SLAVE_RELAY) {
                indiceCodaRelayDaInviareAttualeArduinoSlaveRelay = 0;
            }
        }
    };

    // *** CLASSE ANNIDATA *** //
   public:
    class Ricezione {
        // *** VARIABILI *** //

        TimestampTipo millisInvioUltimoMessaggioArduinoSlaveRelay;

        LoggerAbstract &logger;

        char stringaInAttesaAckArduinoSlaveRelay[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];

        // *** COSTRUTTORE *** //

       public:
        Ricezione(LoggerAbstract &logger) : logger{logger} {}

        // *** METODI PUBBLICI *** //

        void avviaControlloAck(const char *stringaInviata) {
            // Copio la stringa che ho inviato nell'apposito buffer
            strcpy(stringaInAttesaAckArduinoSlaveRelay, stringaInviata);
            ackInAttesa = true;
            millisInvioUltimoMessaggioArduinoSlaveRelay = millis();
        }

        void controllaAck() {
            while (Serial2.available() > 0) {
                char stringaRicevuta[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
                Serial2.readBytes(stringaRicevuta, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

                if (DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Ricevuto da Arduino Slave Relay: ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, stringaRicevuta,
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                    logger.cambiaTabulazione(1);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Valido stringa ACK...",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                // Uso strstr per vedere se la stringa contiene la parola "ping", non uso strcmp perché il buffer è più
                // grande
                if (strstr(stringaRicevuta, stringaInAttesaAckArduinoSlaveRelay) != nullptr) {
                    if (DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY) {
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Stringa ACK corrisponde!",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    }
                    ackInAttesa = false;
                    timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp = millis();
                } else {
                    if (DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY) {
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Stringa ACK non corrisponde!",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Stringa ACK attesa: ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, stringaInAttesaAckArduinoSlaveRelay,
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                    }
                }
                if (DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Valido stringa ACK... OK",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    logger.cambiaTabulazione(-1);
                }
            }

            // Se non ho ricevuto un ACK nelle righe di prima, rinvio il messaggio se è
            // passato del tempo
            if (ackInAttesa) {
                // Se non ho ricevuto un ACK entro tot tempo, rinvio il messaggio
                if (millis() - millisInvioUltimoMessaggioArduinoSlaveRelay >= ATTESA_ACK) {
                    if (DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY) {
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "ACK Timeout: invio di nuovo il messaggio: ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, stringaInAttesaAckArduinoSlaveRelay,
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                    }
                    Serial2.write(stringaInAttesaAckArduinoSlaveRelay, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
                    // Resetto il timer dato che ho appena rinviato il messaggio
                    millisInvioUltimoMessaggioArduinoSlaveRelay = millis();
                }
            }
        }

        void riceve() {
            if (Serial2.available() > 0) {
                // Ogni volta che ricevo un messaggio resetto il timestamp, utile per
                // accertarmi che Arduino Mega sia controlloPresenzaArduinoSlaveAttivo e il segnale Interrupt sia
                // valido.
                char localBuffer[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
                Serial2.readBytes(localBuffer, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

                if (DEBUGGING_STRINGHE_RICEVUTE_DA_ARDUINO_SLAVE_RELAY) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stringa ricevuta da Arduino Slave Relay: ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, localBuffer,
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                }

                // Uso strstr per vedere se la stringa contiene la parola "ping", non uso strcmp perché il buffer è più
                // grande
                if (strstr(localBuffer, "ping") != nullptr) {
                    if (DEBUGGING_PING_RICEVUTI_DA_ARDUINO_SLAVE_RELAY) {
                        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ping Arduino Slave Relay ricevuto.",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    }
                    timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp = millis();
                }
            }
        }
        bool ackInAttesa = false;
    };

    // *** CLASSE ANNIDATA *** //

    class ControlloPresenza {
        // *** VARIABILI *** //
        LoggerAbstract &logger;

        // *** COSTRUTTORE *** //

       public:
        ControlloPresenza(LoggerAbstract &logger) : logger{logger} {}

        // *** METODI PUBBLICI *** //

        void controllaPresenza() {
            if (controlloPresenzaArduinoSlaveAttivo) {
                TimestampTipo tempoCorrente = millis();

                if (tempoCorrente - timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp >
                    ATTESA_MASSIMA_RICEZIONE_PING_ARDUINO_SLAVE_RELAY) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non ricevo segnale da Arduino Slave Relay!",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                    // Le connessioni seriali sono deboli. Nelle mie prove, dopo un po' di
                    // tempo (che può essere qualche decina di minuti o qualche ora) queste
                    // smettono di funzionare. Andiamo dunque a resettare ogni talvolta che
                    // questo accade.
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Tento riconnessione porta seriale... ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    resettoPortaSerialeArduinoMaster(PORTA_SERIALE_ARDUINO_SLAVE_RELAY, VELOCITA_PORTA_SERIALE_ARDUINO,
                                                     true, true);
                    // Resetto timer dopo aver resettato la porta seriale
                    timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp = millis();
                }

                if (tempoCorrente - timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp >
                    ATTESA_MASSIMA_RICEZIONE_PING_ARDUINO_SLAVE_RELAY) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non ricevo segnale da Arduino Slave Sensori!",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Tento riconnessione porta seriale... ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    resettoPortaSerialeArduinoMaster(PORTA_SERIALE_ARDUINO_SLAVE_SENSORI,
                                                     VELOCITA_PORTA_SERIALE_ARDUINO, true, true);
                    // Resetto timer dopo aver resettato la porta seriale
                    timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp = millis();
                }
            }
        }
    };

    // *** VARIABILI *** //

    LoggerAbstract &logger;

    // Creo un'istanza per ciascuna delle classi annidate definite prima
    CodaRelay codaRelayDaInviare = CodaRelay(logger);
    Ricezione ricezione = Ricezione(logger);
    ControlloPresenza controlloPresenza = ControlloPresenza(logger);

    // *** COSTRUTTORE *** //

   public:
    ArduinoSlaveRelay(LoggerAbstract &logger) : logger{logger} {}

    // *** METODI PUBBLICI *** //

    void inviaInfoRelay() {
        int numeroRelay = codaRelayDaInviare.getElemento();
        if (DEBUGGING_MESSAGGI_INVIATI_AD_ARDUINO_SLAVE_RELAY) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Invio ad Arduino Slave Relay: relay ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(numeroRelay).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }

        char localBuffer[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
        sprintf(localBuffer, "relay %d", numeroRelay);

        Serial2.write(localBuffer, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

        if (DEBUGGING_MESSAGGI_INVIATI_AD_ARDUINO_SLAVE_RELAY) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Invio ad Arduino Slave Relay: relay ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(numeroRelay).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " ... OK",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }

        ricezione.avviaControlloAck(localBuffer);
    }
};

// *** DICHIARAZIONE VARIABILI *** //

extern ArduinoSlaveRelay arduinoSlaveRelay;

#endif
