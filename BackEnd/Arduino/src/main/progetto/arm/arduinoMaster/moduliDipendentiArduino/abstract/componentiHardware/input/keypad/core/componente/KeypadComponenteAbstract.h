#ifndef DCC_COMMAND_STATION_KEYPADCOMPONENTEABSTRACT_H
#define DCC_COMMAND_STATION_KEYPADCOMPONENTEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/DigitalPinAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define NUMERO_RIGHE 4
#define NUMERO_COLONNE 4

#define KEYPAD_SETTLING_DELAY 20

// *** CLASSE *** //

class KeypadComponenteAbstract {
    // *** VARIABILI *** //

   private:
    DelayAbstract& delay;
    LoggerAbstract& logger;

    DigitalPinAbstract* pinRigheKeypad[4];
    DigitalPinAbstract* pinColonneKeypad[4];
    char tastiKeypad[NUMERO_RIGHE][NUMERO_COLONNE] = {
        {'1', '2', '3', 'A'}, {'4', '5', '6', 'B'}, {'7', '8', '9', 'C'}, {'*', '0', '#', 'D'}};

    // *** COSTRUTTORE *** //

   public:
    KeypadComponenteAbstract(DigitalPinAbstract& pinRiga1, DigitalPinAbstract& pinRiga2, DigitalPinAbstract& pinRiga3,
                             DigitalPinAbstract& pinRiga4, DigitalPinAbstract& pinColonna1,
                             DigitalPinAbstract& pinColonna2, DigitalPinAbstract& pinColonna3,
                             DigitalPinAbstract& pinColonna4, DelayAbstract& delay, LoggerAbstract& logger)
        : delay{delay}, logger{logger} {
        // Copio le variabili passate come argomento
        pinRigheKeypad[0] = &pinRiga1;
        pinRigheKeypad[1] = &pinRiga2;
        pinRigheKeypad[2] = &pinRiga3;
        pinRigheKeypad[3] = &pinRiga4;

        pinColonneKeypad[0] = &pinColonna1;
        pinColonneKeypad[1] = &pinColonna2;
        pinColonneKeypad[2] = &pinColonna3;
        pinColonneKeypad[3] = &pinColonna4;
    }

    // *** METODI *** //

    void inizializza();
    char leggeCarattere();
};

#endif
