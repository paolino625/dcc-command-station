// *** INCLUDE *** //

#include "StazioneAbstract.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"

// *** DEFINIZIONE METODI *** //

void StazioneAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo stazione n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
    logger.cambiaTabulazione(1);

    if (DEBUGGING_RICERCA_SEZIONI_APPARTENENTI_STAZIONE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Trovo le sezioni appartenenti alla stazione... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(1);
    }
    for (int i = 1; i < sezioni.numeroSezioni + 1; i++) {
        SezioneAbstract* sezione = sezioni.getSezioneFromIndex(i);
        // Se si tratta di una sezione di stazione, controllo se appartiene alla stazione presa in esame
        if (sezione->getId().isBinarioStazione()) {
            if (sezione->getId().id == id) {
                if (DEBUGGING_RICERCA_SEZIONI_APPARTENENTI_STAZIONE) {
                    IdSezioneTipo idSezione = sezione->getId();
                    char* idSezioneStringa = idSezione.toStringShort();

                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Trovata sezione appartenente alla stazione: ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, idSezioneStringa,
                                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);
                }
                // Se la sezione appartiene alla stazione, la aggiungo all'array dei binari.
                numeroBinari += 1;
                binari[numeroBinari] = sezione;
            }
        }
    }
    if (DEBUGGING_RICERCA_SEZIONI_APPARTENENTI_STAZIONE) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Trovo le sezioni appartenenti alla stazione... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    ordinoBinariInBaseAllaLunghezza();

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo stazione n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
}

NomeStazioneUnivoco StazioneAbstract::getNomeUnivoco() { return nomeUnivoco; }

bool StazioneAbstract::isStazioneDiTesta() const { return stazioneDiTesta; }

bool StazioneAbstract::isStessaProspettivaStazioneCentrale() const { return stessaProspettivaStazioneCentrale; }

void StazioneAbstract::setId(NomeStazioneUnivoco idStazione) { this->nomeUnivoco = idStazione; }

void StazioneAbstract::setStazioneDiTesta(bool stazioneDiTesta) { this->stazioneDiTesta = stazioneDiTesta; }

SezioneAbstract* StazioneAbstract::getBinario(int numeroBinario) {
    if (numeroBinario > 0 && numeroBinario <= numeroBinari) {
        return binari[numeroBinario];
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Numero binario inserito non valido!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }
}

void StazioneAbstract::ordinoBinariInBaseAllaLunghezza() {
    // Uso il Bubble Sort per ordinare i binari in base alla lunghezza, dal più corto al più lungo.

    int i, j;
    SezioneAbstract* temp;

    for (i = 1; i < numeroBinari + 1; i++) {
        for (j = 1; j < numeroBinari - i + 1; j++) {
            if (binari[j]->getLunghezzaCm() > binari[j + 1]->getLunghezzaCm()) {
                temp = binari[j];
                binari[j] = binari[j + 1];
                binari[j + 1] = temp;
            }
        }
    }
}