#ifndef DCC_COMMAND_STATION_SEZIONIABSTRACT_H
#define DCC_COMMAND_STATION_SEZIONIABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/enumerazioni/validitaPosizioneConvoglio/ValiditaPosizioneConvoglio.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezione/SezioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/tracciato/sezioni/NumeroSezioni.h"

// *** CLASSE *** //

class SezioniAbstract {
    // *** VARIABILI *** //

   public:
    int numeroSezioni = 0;

   protected:
    ScambiRelayAbstract& scambiRelay;
    SezioneAbstract* sezioni[NUMERO_SEZIONI_MASSIMO + 1];
    ProgrammaAbstract& programma;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    SezioniAbstract(ScambiRelayAbstract& scambiRelay, ProgrammaAbstract& programma, LoggerAbstract& logger)
        : scambiRelay(scambiRelay), programma(programma), logger(logger) {}

    // *** DEFINIZIONE METODI *** //

    void inizializzaInfoBasilari();
    void inizializzaPosizioneSensori();
    SezioneAbstract* getSezione(IdSezioneTipo idSezione);
    SezioneAbstract* getSezioneFromIndex(int indiceSezione);
    SezioneAbstract* getSezioneDelSensore(IdSensorePosizioneTipo);

    bool esisteSezione(IdSezioneTipo idSezione);

    // Per effettuare il lock o l'unlock di una sezione è necessario chiamare questi metodi sull'oggetto sezioni.
    // Non è possibile chiamare le funzioni direttamente sull'oggetto sezione, questo perché in alcuni casi (vedi
    // sezioni incroci o scambi), per bloccare una sezione è necessario anche bloccare la sezione gemella.
    bool lock(IdSezioneTipo idSezione, IdConvoglioTipo idConvoglio);
    void unlock(IdSezioneTipo idSezione);
    void resetLock();

   private:
    bool lockSezioneNormale(IdConvoglioTipo idConvoglio, IdSezioneTipo idSezione);
    bool lockScambio(IdConvoglioTipo idConvoglio, IdSezioneTipo idSezione);
    bool lockIncrocio(IdConvoglioTipo idConvoglio, IdSezioneTipo idSezione);

    void unlockSezioneNormale(IdSezioneTipo idSezione);
    void unlockScambio(IdSezioneTipo idSezione);
    void unlockIncrocio(IdSezioneTipo idSezione);

    void aggiungoSezione(IdSezioneTipo idSezione, LunghezzaTracciatoTipo lunghezzaCm,
                         TipologiaSezioneCritica tipologiaSezioneCritica, bool inversioneDiPolarita);

    // *** 1° PIANO *** //

    // ** STAZIONI ** //

    void aggiungoSezioniStazioniPrimoPiano();
    void aggiungoSezioniStazioneCentralePrimoPiano();
    void aggiungoSezioniStazioneEstPrimoPiano();
    void aggiungoSezioniStazioneOvestPrimoPiano();
    void aggiungoSezioniStazioneEstremoEstPrimoPiano();
    void aggiungoSezioniStazioneEstremoOvestPrimoPiano();

    // ** ALTRO ** //

    void aggiungoSezioniIntermediePrimoPiano();
    void aggiungoSezioniScambiPrimoPiano();
    void aggiungoSezioniIncrociPrimoPiano();

    // ** SENSORI ** //

    void aggiungoSensoriASezioniPrimoPiano();

    // *** 1° PIANO - 2° PIANO *** //

    // ** ALTRO ** //

    void aggiungoSezioniIntermedieTraPrimoESecondoPiano();

    // ** SENSORI ** //

    void aggiungoSensoriASezioniTraPrimoESecondoPiano();

    // *** 2° PIANO *** //

    // ** STAZIONI ** //

    void aggiungoSezioniStazioniSecondoPiano();
    void aggiungoSezioniStazioneOvestSecondoPiano();
    void aggiungoSezioniStazioneEstremoEstSecondoPiano();

    // ** ALTRO ** //

    void aggiungoSezioniIntermedieSecondoPiano();
    void aggiungoSezioniScambiSecondoPiano();
    void aggiungoSezioniIncrociSecondoPiano();

    // ** SENSORI ** //

    void aggiungoSensoriASezioniSecondoPiano();

    // *** 2° PIANO - 3° PIANO *** //

    // ** ALTRO ** //

    void aggiungoSezioniIntermedieTraSecondoETerzoPiano();

    // ** SENSORI ** //

    void aggiungoSensoriASezioniTraSecondoETerzoPiano();

    // *** 3° PIANO *** //

    // ** STAZIONI ** //

    void aggiungoSezioniStazioneTerzoPiano();

    // ** ALTRO ** //

    void aggiungoSezioniIntermedieTerzoPiano();
    void aggiungoSezioniScambiTerzoPiano();
    void aggiungoSezioniIncrociTerzoPiano();
};

#endif