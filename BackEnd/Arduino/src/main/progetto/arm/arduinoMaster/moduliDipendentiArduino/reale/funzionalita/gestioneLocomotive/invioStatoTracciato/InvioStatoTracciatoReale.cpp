// *** INCLUDE *** //

#include "InvioStatoTracciatoReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

InvioStatoTracciatoAbstract invioStatoTracciatoReale = InvioStatoTracciatoAbstract(
    convogliReali, locomotiveReali, buzzerRealeArduinoMaster, protocolloDccCoreReale, loggerReale);