// *** INCLUDE *** //

#include "OrarioPlasticoAbstract.h"

#include <cstring>
#include <string>

// *** METODI *** //

void OrarioPlasticoAbstract::aggiorna() {
    unsigned long tempoCorrenteRealeMillis = tempo.milliseconds();
    unsigned long tempoTrascorsoDaUltimoAggiornamentoMillis = tempoCorrenteRealeMillis - ultimoAggiornamento;
    ultimoAggiornamento = tempoCorrenteRealeMillis;

    // In base al tempo trascorso dall'ultimo aggiornamento, calcolo il tempo trascorso accelerato.
    // In altre parole calcolo quanto tempo è passato nel mondo immaginario.
    unsigned long tempoTrascorsoAcceleratoMillis = tempoTrascorsoDaUltimoAggiornamentoMillis * fattoreAccelerazione;

    // Aggiorno l'orario del mondo immaginario
    // Divido per 60000 per ottenere i minuti
    int tempoTrascorsoAcceleratoMinuti = (int)tempoTrascorsoAcceleratoMillis / 60000;

    // Aggiungo i minuti trascorsi ai minuti attuali
    int nuoviMinuti = orario.minuti + tempoTrascorsoAcceleratoMinuti;

    // La somma precedente potrebbe aver portato a un numero di minuti maggiore di 60, quindi normalizzo
    int nuoviMinutiNormalizzato = nuoviMinuti % 60;

    // Capisco quante ore devo aggiungere come conseguenza del normalizzare i minuti
    int nuoveOreDaAggiungere = nuoviMinuti / 60;

    // Salvo i minuti
    orario.minuti = nuoviMinutiNormalizzato;

    // Aggiungo le ore
    int nuoveOre = orario.ore + nuoveOreDaAggiungere;
    // Se aggiungendo le ore ho superato le 24, normalizzo
    int nuoveOreNormalizzate = nuoveOre % 24;
    // Capisco quante giornate devo aggiungere come conseguenza del normalizzare le ore
    int nuoveGiornateDaAggiungere = nuoveOre / 24;

    // Salvo le ore
    orario.ore = nuoveOreNormalizzate;

    // Aggiungo le giornate
    orario.giornata += nuoveGiornateDaAggiungere;
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente.
char* OrarioPlasticoAbstract::toString() const {
    std::string stringa;

    stringa += "ORARIO\n";
    stringa += "Giornata: ";
    stringa += std::to_string(orario.giornata);
    stringa += "\n";
    stringa += "Ore: ";
    stringa += std::to_string(orario.ore);
    stringa += "\n";
    stringa += "Minuti: ";
    stringa += std::to_string(orario.minuti);

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

Orario OrarioPlasticoAbstract::getOrario() { return orario; }
int OrarioPlasticoAbstract::getGiornata() const { return orario.giornata; }
int OrarioPlasticoAbstract::getOre() const { return orario.ore; }
int OrarioPlasticoAbstract::getMinuti() const { return orario.minuti; }

int OrarioPlasticoAbstract::calcoloFattoreVelocita(int lunghezzaGiornataInMinutiMondoPlastico) {
    int lunghezzaGiornataRealeInMinuti = 24 * 60;  // 60 minuti in 1 ora per le 24 ore della giornata
    return lunghezzaGiornataRealeInMinuti / lunghezzaGiornataInMinutiMondoPlastico;
}