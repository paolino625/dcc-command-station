// *** INCLUDE *** //

#include "KeypadComponenteReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

DigitalPinReale pinKeypadRiga1(PIN_KEYPAD_RIGA_1);
DigitalPinReale pinKeypadRiga2(PIN_KEYPAD_RIGA_2);
DigitalPinReale pinKeypadRiga3(PIN_KEYPAD_RIGA_3);
DigitalPinReale pinKeypadRiga4(PIN_KEYPAD_RIGA_4);
DigitalPinReale pinKeypadColonna1(PIN_KEYPAD_COLONNA_1);
DigitalPinReale pinKeypadColonna2(PIN_KEYPAD_COLONNA_2);
DigitalPinReale pinKeypadColonna3(PIN_KEYPAD_COLONNA_3);
DigitalPinReale pinKeypadColonna4(PIN_KEYPAD_COLONNA_4);

KeypadComponenteAbstract keypadComponenteReale{
    pinKeypadRiga1,    pinKeypadRiga2,    pinKeypadRiga3,    pinKeypadRiga4, pinKeypadColonna1,
    pinKeypadColonna2, pinKeypadColonna3, pinKeypadColonna4, delayReale,     loggerReale};