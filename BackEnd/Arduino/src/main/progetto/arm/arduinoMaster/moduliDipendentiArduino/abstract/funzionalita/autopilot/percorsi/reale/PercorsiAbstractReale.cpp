// *** INCLUDE *** //

#include "PercorsiAbstractReale.h"

// *** DEFINIZIONE METODI *** //

bool PercorsiAbstractReale::esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo) {
    return retrievePercorsoHelper.esistePercorso(idSezionePartenza, idSezioneArrivo);
}

bool PercorsiAbstractReale::esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo,
                                      bool direzionePrimaLocomotivaDestra) {
        return retrievePercorsoHelper.esistePercorso(idSezionePartenza, idSezioneArrivo, direzionePrimaLocomotivaDestra);

}

PercorsoAbstract* PercorsiAbstractReale::getPercorso(IdSezioneTipo idSezionePartenzaRequest,
                                                IdSezioneTipo idSezioneArrivoRequest) {
        return retrievePercorsoHelper.getPercorso(idSezionePartenzaRequest, idSezioneArrivoRequest);
}