#ifndef DCC_COMMAND_STATION_FERMOPROGRAMMAREALE_H
#define DCC_COMMAND_STATION_FERMOPROGRAMMAREALE_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"

// *** CLASSE *** //

class ProgrammaReale : public ProgrammaAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    [[noreturn]] void ferma(bool stampaMessaggioErroreGenerico) override;
};

#endif

// *** DICHIARAZIONE VARIABILI *** //

extern ProgrammaReale programmaReale;