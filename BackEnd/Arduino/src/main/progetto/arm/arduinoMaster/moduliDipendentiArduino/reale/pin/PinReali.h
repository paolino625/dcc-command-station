#ifndef ARDUINO_MASTER_PIN_REALI_H
#define ARDUINO_MASTER_PIN_REALI_H

// *** DEFINE *** //

// Porte seriali

// Inizializzo Porta Seriale 0 che corrisponde alla porta USB-C
#define PORTA_SERIALE_COMPUTER 0

// Inizializzo Porta Seriale 2 che corrisponde a RX1/TX1 per comunicazione con Arduino Slave Relay
#define PIN_RESET_ARDUINO_SLAVE_RELAY 22

// Inizializzo Porta Seriale 3 che corrisponde a RX2/TX2 per comunicazione con Arduino Slave Sensori
#define PIN_RESET_ARDUINO_SLAVE_SENSORI 23

// Motor driver shield

#define PIN_PWM 2
#define PIN_DIR_1 3

// Indirizzo I2C

#define INDIRIZZO_I2C_DISPLAY 0x27
#define INDIRIZZO_I2C_INA_219 0x40
#define INDIRIZZO_I2C_INA_260 0x41
#define INDIRIZZO_I2C_AMPLIFICATORE_AUDIO_MAX_9744 0x4B

// KeypadAbstract

#define PIN_KEYPAD_COLONNA_1 30
#define PIN_KEYPAD_COLONNA_2 31
#define PIN_KEYPAD_COLONNA_3 32
#define PIN_KEYPAD_COLONNA_4 33
#define PIN_KEYPAD_RIGA_1 34
#define PIN_KEYPAD_RIGA_2 35
#define PIN_KEYPAD_RIGA_3 36
#define PIN_KEYPAD_RIGA_4 37

// Pulsante di emergenza

#define PIN_PULSANTE_EMERGENZA 38

// EncoderAbstract 1

#define CLK1 24
#define DT1 25
#define SW1 26

// EncoderAbstract 2

#define CLK2 27
#define DT2 28
#define SW2 29

#define PIN_BUZZER 11  // Deve essere un'uscita PWM

#define PIN_SENSORE_TENSIONE_LUCI_INTERRUTTORI A0
#define PIN_SENSORE_TENSIONE_LED1 A1
#define PIN_SENSORE_TENSIONE_LED2 A2
#define PIN_SENSORE_TENSIONE_LED3 A3
#define PIN_SENSORE_TENSIONE_CDU A4
#define PIN_SENSORE_TENSIONE_SENSORI_POSIZIONE A5

// Non posso inserire A8/A9 perché altrimenti da problemi in compilazione. Questo perché pin A8-A11 sono di tipo
// PureAnalogPin e non int.
#define PIN_SENSORE_TENSIONE_ARDUINO 118                  // A8
#define PIN_SENSORE_TENSIONE_COMPONENTI_AUSILIARI_5V 119  // A9
#define PIN_SENSORE_TENSIONE_COMPONENTI_AUSILIARI_3V 120  // A10

#endif