#ifndef DCC_COMMAND_STATION_LOCOMOTIVE_H
#define DCC_COMMAND_STATION_LOCOMOTIVE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotiva/LocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"

// *** DEFINE *** //

/*
Definisco il numero di locomotive.
Non sovradimensionare questo numero perché Arduino invierà al tracciato continuamente lo stato delle
locomotive. Più locomotive ci sono, più il loop di invio sarà lungo.
N.B. In caso di modifica e superamento di 10, inserire nella funzione leggoIDLocomotiva
keyPadLeggoNumero2Cifre
Inserire il numero di locomotive + 1, perché l'array parte da 1.
*/
#define NUMERO_LOCOMOTIVE 9

// *** CLASSE *** //

class LocomotiveAbstract {
    // *** VARIABILI *** //

   private:
    ProgrammaAbstract& programma;
    LocomotivaAbstract** locomotive = new LocomotivaAbstract*[NUMERO_LOCOMOTIVE];
    TempoAbstract& tempo;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    LocomotiveAbstract(ProgrammaAbstract& programma, TempoAbstract& tempo, LoggerAbstract& logger)
        : programma(programma), tempo(tempo), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void inizializzaLocomotive();
    bool esisteLocomotiva(IdLocomotivaTipo idLocomotiva);
    LocomotivaAbstract* getLocomotiva(int id, bool fermaProgramma);
    char* toString();
};

#endif