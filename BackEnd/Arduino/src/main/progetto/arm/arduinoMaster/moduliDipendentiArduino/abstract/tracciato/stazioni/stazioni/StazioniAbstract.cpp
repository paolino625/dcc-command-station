// *** INCLUDE *** //

#include "StazioniAbstract.h"

// *** DEFINIZIONE METODI *** //

void StazioniAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo stazioni...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);

    // N.B. Non cambiare gli ID delle stazioni perché sono legati all'IdSezioneTipo.
    // TODO Riabilitare quando saranno presenti le stazioni estreme
    // creaStazioneEAggiunge(1, STAZIONE_ESTREMO_EST_PRIMO_PIANO, true, true);
    // creaStazioneEAggiunge(5, STAZIONE_ESTREMO_OVEST_PRIMO_PIANO, true, true);
    creaStazioneEAggiunge(2, STAZIONE_EST_PRIMO_PIANO, true, false);
    creaStazioneEAggiunge(3, STAZIONE_CENTRALE, false, true);
    creaStazioneEAggiunge(4, STAZIONE_OVEST_PRIMO_PIANO, true, false);

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo stazioni... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void StazioniAbstract::creaStazioneEAggiunge(IdStazioneTipo idStazione, NomeStazioneUnivoco nomeStazione,
                                             bool stazioneDiTesta, bool stessaProspettivaStazioneCentrale) {
    StazioneAbstract* stazione =
        new StazioneAbstract(idStazione, nomeStazione, stazioneDiTesta, stessaProspettivaStazioneCentrale, programma, sezioni, logger);
    stazione->inizializza();
    numeroStazioni += 1;
    // Salviamo l'ID della stazione aggiunta
    idStazioniPresenti[numeroStazioni] = idStazione;
    stazioni[numeroStazioni] = stazione;
}

StazioneAbstract* StazioniAbstract::getStazioneFromArray(int indiceStazione) {
    if (indiceStazione <= numeroStazioni && indiceStazione > 0) {
        return stazioni[indiceStazione];
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Indice stazione inserito non valido!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return nullptr;
    }
}

StazioneAbstract* StazioniAbstract::getStazione(IdStazioneTipo idStazione) {
    for (int i = 1; i <= numeroStazioni; i++) {
        if (stazioni[i]->getId() == idStazione) {
            return stazioni[i];
        }
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "ID stazione n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(idStazione).c_str(),
                            InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO( logger, LivelloLog::BUG, " non esiste.",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    programma.ferma(true);

    return nullptr;
}

StazioneAbstract* StazioniAbstract::getStazioneFromSezione(IdSezioneTipo idSezione) {
    if (idSezione.isBinarioStazione()) {
        IdStazioneTipo idStazione = idSezione.id;
        return getStazione(idStazione);
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "ID sezione non valido.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
    }
}