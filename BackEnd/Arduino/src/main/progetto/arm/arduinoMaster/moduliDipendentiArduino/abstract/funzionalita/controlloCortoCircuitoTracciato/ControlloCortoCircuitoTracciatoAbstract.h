#ifndef DCC_COMMAND_STATION_CONTROLLO_CORTO_CIRCUITO_TRACCIATO_H
#define DCC_COMMAND_STATION_CONTROLLO_CORTO_CIRCUITO_TRACCIATO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/KeypadAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/SensoreCorrenteIna219Abstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/AutopilotAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/stopEmergenza/StopEmergenzaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/menuDisplayKeypad/operazioniMultiple/MenuDisplayKeypadOperazioniMultipleAbstract.h"

// *** DEFINE *** //

#define SOGLIA_MILLIAMPERE_CORTOCIRCUITO 2000

#define INTERVALLO_MILLISECONDI_TENTATIVO_DOPO_CORTO_CIRCUITO 1000

// *** CLASSE *** //

class ControlloCortoCircuitoTracciatoAbstract {
    // *** VARIABILI *** //
   public:
    KeypadAbstract &keypad;
    SensoreCorrenteIna219Abstract &sensoreCorrenteIna219;
    MotorShieldAbstract &motorShield;
    BuzzerAbstractReale &buzzer;
    DisplayAbstract &display;
    StopEmergenzaAbstract &stopEmergenza;
    DelayAbstract &delay;
    AutopilotAbstract &autopilot;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

    ControlloCortoCircuitoTracciatoAbstract(KeypadAbstract &keypad,
                                            SensoreCorrenteIna219Abstract &sensoreCorrenteIna219,
                                            MotorShieldAbstract &motorShield, BuzzerAbstractReale &buzzer,
                                            DisplayAbstract &display, StopEmergenzaAbstract &stopEmergenza,
                                            DelayAbstract &delay, AutopilotAbstract &autopilot, LoggerAbstract &logger)
        : keypad(keypad),
          sensoreCorrenteIna219(sensoreCorrenteIna219),
          motorShield(motorShield),
          buzzer(buzzer),
          display(display),
          stopEmergenza(stopEmergenza),
          delay(delay),
          autopilot(autopilot),
          logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

   public:
    void effettuaControllo(MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypad);
};

#endif