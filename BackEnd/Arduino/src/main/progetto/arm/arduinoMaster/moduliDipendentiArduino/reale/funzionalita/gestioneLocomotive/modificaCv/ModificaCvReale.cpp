// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/modificaCv/ModificaCvAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreReale.h"

// *** DEFINIZIONE VARIABILI *** //

ModificaCvAbstract modificaCvReale = ModificaCvAbstract(protocolloDccCoreReale);