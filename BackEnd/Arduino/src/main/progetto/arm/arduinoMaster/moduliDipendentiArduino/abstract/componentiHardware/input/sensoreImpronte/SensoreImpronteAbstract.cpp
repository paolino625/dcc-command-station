// *** INCLUDE *** //

#include "SensoreImpronteAbstract.h"

#include <string>

#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE METODI *** //

void SensoreImpronteAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensore impronte... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    sensoreImpronteComponente.begin();
    delay.milliseconds(5);
    if (sensoreImpronteComponente.verifyPassword()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensore impronte... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Sensore impronte non trovato.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(false);
    }
}

void SensoreImpronteAbstract::controllaDatabaseImpronte() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Check database impronte... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    if (getNumeroImpronteDatabase() == 0) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Il database delle impronte è vuoto! Aggiungerne alcune!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        buzzer.beepAvviso();
    } else {
        logger.cambiaTabulazione(1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Il database del sensore contiene ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(getNumeroImpronteDatabase()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " impronte.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Check database impronte... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
};

uint8_t SensoreImpronteAbstract::getFingerprintId() {
    uint8_t p = sensoreImpronteComponente.getImage();
    switch (p) {
        case FINGERPRINT_OK:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Immagine acquisita dal sensore di impronte.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            break;
        case FINGERPRINT_NOFINGER:
            // Lascio commentato altrimenti si riempie di log
            // LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Nessun dito rilevato.",
            // InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        case FINGERPRINT_PACKETRECIEVEERR:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        case FINGERPRINT_IMAGEFAIL:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore immagine.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
    }

    // OK success!

    p = sensoreImpronteComponente.image2Tz();
    switch (p) {
        case FINGERPRINT_OK:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Immagine convertita dal sensore di impronte.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            break;
        case FINGERPRINT_IMAGEMESS:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, " Immagine troppo confusa.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        case FINGERPRINT_PACKETRECIEVEERR:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        case FINGERPRINT_FEATUREFAIL:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non riesco a trovare i dettagli dell'impronta.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        case FINGERPRINT_INVALIDIMAGE:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Immagine non valida.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return 0;
    }

    // OK converted!
    p = sensoreImpronteComponente.fingerSearch();
    if (p == FINGERPRINT_OK) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ho trovato un match.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        sensoreImpronteComponente.LEDcontrol(FINGERPRINT_LED_ON, 0, FINGERPRINT_LED_BLUE);
    } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return 0;
    } else if (p == FINGERPRINT_NOTFOUND) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non è stato trovato un match.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        sensoreImpronteComponente.LEDcontrol(FINGERPRINT_LED_ON, 0, FINGERPRINT_LED_RED);
        delay.milliseconds(TEMPO_LED_ROSSO_IMPRONTA_NON_VALIDA_MILLISECONDI);
        sensoreImpronteComponente.LEDcontrol(FINGERPRINT_LED_ON, 0, FINGERPRINT_LED_PURPLE);
        return 0;
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return 0;
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Trovato match con impronta ID ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          std::to_string(sensoreImpronteComponente.getFingerprintIDVariabile()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " con una confidenza del ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                          std::to_string(sensoreImpronteComponente.getConfidenceVariabile()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " %.", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    return sensoreImpronteComponente.getFingerprintIDVariabile();
}

uint8_t SensoreImpronteAbstract::getFingerprintEnroll(uint8_t idNuovaImpronta) {
    int p = -1;
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Appoggia il dito che vuoi registrare",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Salvataggio nuova impronta nello slot n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idNuovaImpronta).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    while (p != FINGERPRINT_OK) {
        p = sensoreImpronteComponente.getImage();
        switch (p) {
            case FINGERPRINT_OK:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Immagine acquisita.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                break;
            case FINGERPRINT_NOFINGER:
                // Non stampo nulla per evitare di avere una sfilza di log
                break;
            case FINGERPRINT_PACKETRECIEVEERR:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                break;
            case FINGERPRINT_IMAGEFAIL:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore immagine.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                break;
            default:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                break;
        }
    }

    // OK success!

    p = sensoreImpronteComponente.image2Tz(1);
    switch (p) {
        case FINGERPRINT_OK:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Immagine convertita.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            break;
        case FINGERPRINT_IMAGEMESS:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Immagine troppo confusa.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

            return p;
        case FINGERPRINT_PACKETRECIEVEERR:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        case FINGERPRINT_FEATUREFAIL:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non riesco a trovare le feature del sensore.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        case FINGERPRINT_INVALIDIMAGE:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Immagine non valida.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Rimuovi dito", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                          true);
    delay.milliseconds(2000);
    p = 0;
    while (p != FINGERPRINT_NOFINGER) {
        p = sensoreImpronteComponente.getImage();
    }
    p = -1;
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Appoggia lo stesso dito un'altra volta.",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    while (p != FINGERPRINT_OK) {
        p = sensoreImpronteComponente.getImage();
        switch (p) {
            case FINGERPRINT_OK:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Immagine registrata.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                break;
            case FINGERPRINT_NOFINGER:

                break;
            case FINGERPRINT_PACKETRECIEVEERR:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                break;
            case FINGERPRINT_IMAGEFAIL:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore immagine.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                break;
            default:
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
                break;
        }
    }

    // OK success!

    p = sensoreImpronteComponente.image2Tz(2);
    switch (p) {
        case FINGERPRINT_OK:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Immagine convertita.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            break;
        case FINGERPRINT_IMAGEMESS:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, " Immagine troppo confusa.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        case FINGERPRINT_PACKETRECIEVEERR:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        case FINGERPRINT_FEATUREFAIL:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non riesco a trovare le feature del sensore.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        case FINGERPRINT_INVALIDIMAGE:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Immagine non valida.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            return p;
    }

    // OK converted!
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Creo un modello per lo slot n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idNuovaImpronta).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    p = sensoreImpronteComponente.createModel();
    if (p == FINGERPRINT_OK) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Match delle immagini!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore di comunicazione.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return p;
    } else if (p == FINGERPRINT_ENROLLMISMATCH) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Le immagini non matchano.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        return p;
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return p;
    }

    p = sensoreImpronteComponente.storeModel(idNuovaImpronta);
    if (p == FINGERPRINT_OK) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Impronta registrata!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore comunicazione.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return p;
    } else if (p == FINGERPRINT_BADLOCATION) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Non posso memorizzare in quello slot.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return p;
    } else if (p == FINGERPRINT_FLASHERR) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore nella scrittura flash.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return p;
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR,
                              "Funzione getFingerprintEnroll() nel file SensoreImpronteAbstract.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return p;
    }

    return true;
}

void SensoreImpronteAbstract::resetDatabase() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Resetto database impronte... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    sensoreImpronteComponente.emptyDatabase();
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Resetto database impronte... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// *** GETTER *** //

SensoreImpronteComponenteAbstract* SensoreImpronteAbstract::getSensoreImpronteComponente() {
    return &sensoreImpronteComponente;
}

int SensoreImpronteAbstract::getNumeroImpronteDatabase() {
    // Controintuitivamente con getTemplateCount non otteniamo il numero di impronte salvate.il sensore salva il
    // numero di impronte nella variabile Questa funzione però è necessaria per trovare nella variabile
    // templateCount il numero di impronte.
    sensoreImpronteComponente.getTemplateCount();
    return sensoreImpronteComponente.getTemplateCountVariabile();
}