// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/ledIntegrato/LedIntegratoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/motorShield/MotorShieldReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoReale.h"

// *** DEFINIZIONE METODI *** //

class ProgrammaReale : public ProgrammaAbstract {
    [[noreturn]] void ferma(bool stampaMessaggioErroreGenerico) {
        // Prima di spegnere la motor shield, inviamo un segnale di stop a tutte le locomotive, altrimenti queste
        // continuerebbero a muoversi grazie al keep-alive.
        if (motorShieldReale.isAccesa()) {
            invioStatoTracciatoReale.stopEmergenza();
            motorShieldReale.spegne();
        }
        ledIntegratoReale.accendiRosso();
        if (stampaMessaggioErroreGenerico) {
            displayReale.errori.generico.fermoProgramma();
        }
        buzzerRealeArduinoMaster.beepErrore();
        while (true) {
            ledIntegratoReale.lampeggiaRosso(1000);
        }
    }
};

// *** DEFINIZIONE VARIABILI *** //

ProgrammaReale programmaReale = ProgrammaReale();