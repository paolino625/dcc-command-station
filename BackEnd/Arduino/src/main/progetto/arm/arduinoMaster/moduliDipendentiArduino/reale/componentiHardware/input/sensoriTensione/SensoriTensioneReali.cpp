// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreTensione/SensoreTensioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/AnalogPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AnalogPinReale analogPinSensoreTensioneLuciInterruttoriReale(PIN_SENSORE_TENSIONE_LUCI_INTERRUTTORI);
AnalogPinReale analogPinSensoreTensioneLED1Reale(PIN_SENSORE_TENSIONE_LED1);
AnalogPinReale analogPinSensoreTensioneLED2Reale(PIN_SENSORE_TENSIONE_LED2);
AnalogPinReale analogPinSensoreTensioneLED3Reale(PIN_SENSORE_TENSIONE_LED3);
AnalogPinReale analogPinSensoreTensioneCDUReale(PIN_SENSORE_TENSIONE_CDU);
AnalogPinReale analogPinSensoreTensioneSensoriPosizioneReale(PIN_SENSORE_TENSIONE_SENSORI_POSIZIONE);
AnalogPinReale analogPinSensoreTensioneArduinoReale(PIN_SENSORE_TENSIONE_ARDUINO);
AnalogPinReale analogPinSensoreTensioneComponentiAusiliari5VReale(PIN_SENSORE_TENSIONE_COMPONENTI_AUSILIARI_5V);
AnalogPinReale analogPinSensoreTensioneComponentiAusiliari3VReale(PIN_SENSORE_TENSIONE_COMPONENTI_AUSILIARI_3V);

SensoreTensioneAbstract sensoreTensioneLuciInterruttoriReale =
    SensoreTensioneAbstract(analogPinSensoreTensioneLuciInterruttoriReale, loggerReale);
SensoreTensioneAbstract sensoreTensioneLED1Reale =
    SensoreTensioneAbstract(analogPinSensoreTensioneLED1Reale, loggerReale);
SensoreTensioneAbstract sensoreTensioneLED2Reale =
    SensoreTensioneAbstract(analogPinSensoreTensioneLED2Reale, loggerReale);
SensoreTensioneAbstract sensoreTensioneLED3Reale =
    SensoreTensioneAbstract(analogPinSensoreTensioneLED3Reale, loggerReale);
SensoreTensioneAbstract sensoreTensioneCDUReale =
    SensoreTensioneAbstract(analogPinSensoreTensioneCDUReale, loggerReale);
SensoreTensioneAbstract sensoreTensioneSensoriPosizioneReale =
    SensoreTensioneAbstract(analogPinSensoreTensioneSensoriPosizioneReale, loggerReale);
SensoreTensioneAbstract sensoreTensioneArduinoReale =
    SensoreTensioneAbstract(analogPinSensoreTensioneArduinoReale, loggerReale);
SensoreTensioneAbstract sensoreTensioneComponentiAusiliari5VReale =
    SensoreTensioneAbstract(analogPinSensoreTensioneComponentiAusiliari5VReale, loggerReale);
SensoreTensioneAbstract sensoreTensioneComponentiAusiliari3VReale =
    SensoreTensioneAbstract(analogPinSensoreTensioneComponentiAusiliari3VReale, loggerReale);