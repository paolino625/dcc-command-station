// *** INCLUDE *** //

#include "InvioStatoTracciatoAbstract.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/codaPacchettiDcc/CodaPacchettiDcc.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/base/extended/PacchettoExtended.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/base/standard/PacchettoStandard.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/funzioniAusiliarie/FunzioniAusiliarie.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/stopEmergenza/PacchettoStopEmergenza.h"

// *** STRUCT *** //

struct StatoLocomotivaRidotto {
    int velocitaAttuale;
    bool direzioneAvantiDisplay;
    bool statoFunzioniAusiliari[NUMERO_FUNZIONI_AUSILIARI_DECODER];
};

// *** DEFINIZIONE VARIABILI *** //

StatoLocomotivaRidotto statoPrecedenteLocomotive[NUMERO_LOCOMOTIVE];

// *** DEFINIZIONE METODI *** //

void InvioStatoTracciatoAbstract::invioPacchettiStatoTutteLocomotive() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Invio pacchetti stato di tutte le locomotive...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    // Inizialmente invio pacchetti per tutte le locomotive.
    // Successivamente, invierò pacchetti solo per le locomotive che hanno cambiato stato.
    // Se cambia solo la velocità di una locomotiva, invierò solo il pacchetto velocità per quella locomotiva.
    // In questo modo, evito di inviare pacchetti inutili e riduco il tempo tra il cambiamento di stato da parte
    // dell'utente o dell'autopilota e la ricezione del pacchetto da parte del decoder.

    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(i, true);

        // *** INVIO PACCHETTO VELOCITA *** //

        invioPacchettoVelocitaLocomotiva(locomotiva);

        // Aggiorno stato precedente locomotiva
        statoPrecedenteLocomotive[i].velocitaAttuale = locomotiva->getVelocitaAttuale();
        statoPrecedenteLocomotive[i].direzioneAvantiDisplay = locomotiva->getDirezioneAvantiDisplay();

        // *** INVIO PACCHETTO FUNZIONI AUSILIARI 1 *** //

        invioPacchettiLuciLocomotiva(locomotiva);

        // Aggiorno stato precedente locomotiva
        for (int r = 0; r < NUMERO_FUNZIONI_AUSILIARI_DECODER; r++) {
            statoPrecedenteLocomotive[i].statoFunzioniAusiliari[r] = locomotiva->getStatoFunzioniAusiliari(r);
        }
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Invio pacchetti stato di tutte le locomotive... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

bool InvioStatoTracciatoAbstract::inviaPacchettoStatoLocomotivaAggiornataSePresente() {
    bool cambiamentoStatoTrovato = false;

    // Scansiono tutte le locomotive in cerca di un cambiamento di stato.
    // Appena ne trovo uno, esco dal ciclo perché questa chiamata è fatta ad ogni loop() e non voglio
    // bloccare il loop() per troppo tempo.
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(i, true);

        // Devo confrontare lo stato attuale con lo stato precedente, in modo tale da capire se devo inviare un
        // pacchetto per aggiornare lo stato della locomotiva.

        // Se la velocità o la direzione è cambiata, allora invio il pacchetto velocità
        if (locomotiva->getVelocitaAttuale() != statoPrecedenteLocomotive[i].velocitaAttuale ||
            locomotiva->getDirezioneAvantiDisplay() != statoPrecedenteLocomotive[i].direzioneAvantiDisplay) {
            cambiamentoStatoTrovato = true;

            invioPacchettoVelocitaLocomotiva(locomotiva);

            // Aggiorno stato precedente locomotiva
            statoPrecedenteLocomotive[i].velocitaAttuale = locomotiva->getVelocitaAttuale();
            statoPrecedenteLocomotive[i].direzioneAvantiDisplay = locomotiva->getDirezioneAvantiDisplay();

            break;
        }

        // Scansiono le funzioni ausiliari

        for (int j = 0; j < NUMERO_FUNZIONI_AUSILIARI_DECODER; j++) {
            if (locomotiva->getStatoFunzioniAusiliari(j) != statoPrecedenteLocomotive[i].statoFunzioniAusiliari[j]) {
                invioPacchettiLuciLocomotiva(locomotiva);

                // Aggiorno stato precedente locomotiva
                for (int r = 0; r < NUMERO_FUNZIONI_AUSILIARI_DECODER; r++) {
                    statoPrecedenteLocomotive[i].statoFunzioniAusiliari[r] = locomotiva->getStatoFunzioniAusiliari(r);
                }

                cambiamentoStatoTrovato = true;
                break;
            }
        }

        // Se ho trovato un cambiamento di stato nel for precedente sono uscito da quel for ma devo uscire anche dal
        // for principale
        if (cambiamentoStatoTrovato) {
            break;
        }
    }

    // Se ho aggiornato lo stato di una locomotiva ritorno true, altrimenti false
    return cambiamentoStatoTrovato;
}

void InvioStatoTracciatoAbstract::processo() {
    // Se non c'è un pacchetto successivo in attesa allora capisco quale pacchetto inviare sul tracciato.
    // Se tolgo l'if, questa funzione diventa una chiamata bloccante,
    // Arduino si blocca ogni volta che entra qui e ad esempio non riesce più a leggere in tempo l'encoder.
    // Questo perché processo verrebbe chiamata la prima volta, aggiungere il pacchetto come successivo, poi verrebbe
    // chiamata 1 microsecondo dopo ma si bloccherebbe perché rimarrebbe in attesa di scrivere il pacchetto successivo.
    if (!codaPacchetti.pacchettoSuccessivoAttivo) {
        bool inviatoPacchettoStatoLocomotivaAggiornata = inviaPacchettoStatoLocomotivaAggiornataSePresente();
        if (inviatoPacchettoStatoLocomotivaAggiornata) {
            // Se ho già inviato un pacchetto di una locomotiva che è stata aggiornata non faccio nient'altro: vogliamo
            // che questa funzione invii al massimo un pacchetto
            return;
        } else {
            // Se nessuna locomotiva è stata aggiornata dall'ultima volta, continuo a inviare pacchetti per tutte le
            // locomotive in maniera ciclica

            switch (stepInvioStatoLocomotiva) {
                case PACCHETTO_VELOCITA:
                    invioPacchettoVelocitaLocomotiva(locomotive.getLocomotiva(idLocomotivaRefreshAttuale, true));
                    stepInvioStatoLocomotiva = PACCHETTO_FUNZIONI_AUSILIARI_1;
                    break;

                case PACCHETTO_FUNZIONI_AUSILIARI_1:
                    invioPacchettoLuciLocomotiva1(locomotive.getLocomotiva(idLocomotivaRefreshAttuale, true));
                    stepInvioStatoLocomotiva = PACCHETTO_FUNZIONI_AUSILIARI_2;
                    break;

                case PACCHETTO_FUNZIONI_AUSILIARI_2:
                    invioPacchettoLuciLocomotiva2(locomotive.getLocomotiva(idLocomotivaRefreshAttuale, true));
                    stepInvioStatoLocomotiva = PACCHETTO_FUNZIONI_AUSILIARI_3;
                    break;

                case PACCHETTO_FUNZIONI_AUSILIARI_3:
                    invioPacchettoLuciLocomotiva3(locomotive.getLocomotiva(idLocomotivaRefreshAttuale, true));

                    // Se ho inviato tutti i pacchetti inerenti ad una locomotiva, paso alla prossima e resetto lo step.
                    idLocomotivaRefreshAttuale += 1;
                    if (idLocomotivaRefreshAttuale == NUMERO_LOCOMOTIVE) {
                        idLocomotivaRefreshAttuale = 1;
                    }

                    stepInvioStatoLocomotiva = PACCHETTO_VELOCITA;

                    break;
            }
        }
    }
}

void InvioStatoTracciatoAbstract::stopEmergenza() {
    // Invio pacchetti di stop sul tracciato
    invioPacchettoStopEmergenza();
}

void InvioStatoTracciatoAbstract::invioPacchettoStopEmergenza() {
    byte ripetizioniPacchetto = 20;

    // Invio pacchetti di stop sul tracciato
    for (int i = 0; i < ripetizioniPacchetto; i++) {
        protocolloDccCore.invioPacchetto(pacchettoStopEmergenza.pacchetto);
    }
}

void InvioStatoTracciatoAbstract::invioPacchettoVelocitaLocomotiva(LocomotivaAbstract *locomotiva) {
    // Aggiorno la direzione avanti del decoder
    locomotiva->aggiornaDirezioneAvantiDecoder();

    int velocitaStepLocomotiva;
    int velocitaAttualeLocomotiva = locomotiva->getVelocitaAttuale();

    // Direzione avanti

    if (locomotiva->getDirezioneAvantiDisplay()) {
        velocitaStepLocomotiva = locomotiva->getMappingVelocitaStepAvanti(velocitaAttualeLocomotiva);

    } else {
        // Direzione indietro
        velocitaStepLocomotiva = locomotiva->getMappingVelocitaStepIndietro(velocitaAttualeLocomotiva);
    }

    if (DEBUGGING_INVIO_TRACCIATO_STATO_LOCOMOTIVE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio Pacchetto Stato Velocita Locomotiva ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(locomotiva->getId()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Velocità attuale: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(locomotiva->getVelocitaAttuale()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Velocità step: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(velocitaStepLocomotiva).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    }

    compongoPacchettoVelocitaEsteso(&pacchettoExtended.pacchetto, locomotiva->getIndirizzoDecoderDcc(),
                                    velocitaStepLocomotiva, locomotiva->getDirezioneAvantiDecoder(), false);
    protocolloDccCore.invioPacchetto(pacchettoExtended.pacchetto);
}

void InvioStatoTracciatoAbstract::invioPacchettiLuciLocomotiva(LocomotivaAbstract *locomotiva) {
    invioPacchettoLuciLocomotiva1(locomotiva);
    invioPacchettoLuciLocomotiva2(locomotiva);
    invioPacchettoLuciLocomotiva3(locomotiva);
}

void InvioStatoTracciatoAbstract::invioPacchettoLuciLocomotiva1(LocomotivaAbstract *locomotiva) {
    if (DEBUGGING_INVIO_TRACCIATO_STATO_LOCOMOTIVE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio Pacchetto Stato Luci 1 Locomotiva ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(locomotiva->getId()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    }

    compongoFunzioniAusiliarie(locomotive, &pacchettoStandard.pacchetto, locomotiva->getId(), 1);
    protocolloDccCore.invioPacchetto(pacchettoStandard.pacchetto);
}

void InvioStatoTracciatoAbstract::invioPacchettoLuciLocomotiva2(LocomotivaAbstract *locomotiva) {
    if (DEBUGGING_INVIO_TRACCIATO_STATO_LOCOMOTIVE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio Pacchetto Stato Luci 2 Locomotiva ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(locomotiva->getId()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    }
    compongoFunzioniAusiliarie(locomotive, &pacchettoStandard.pacchetto, locomotiva->getId(), 2);
    protocolloDccCore.invioPacchetto(pacchettoStandard.pacchetto);
}

void InvioStatoTracciatoAbstract::invioPacchettoLuciLocomotiva3(LocomotivaAbstract *locomotiva) {
    if (DEBUGGING_INVIO_TRACCIATO_STATO_LOCOMOTIVE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio Pacchetto Stato Luci 3 Locomotiva ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(locomotiva->getId()).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    }
    compongoFunzioniAusiliarie(locomotive, &pacchettoStandard.pacchetto, locomotiva->getId(), 3);
    protocolloDccCore.invioPacchetto(pacchettoStandard.pacchetto);
}