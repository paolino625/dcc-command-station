// *** INCLUDE *** //

#include "TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta.h"

// *** DEFINIZIONE FUNZIONI *** //

const char* convertoTipologiaDifferenzaVelocitaLocomotivaTrazioneSpintaInStringa(
    TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta tipologia) {
    switch (tipologia) {
        case NESSUNA_DIFFERENZA:
            return "NESSUNA_DIFFERENZA";
        case SPINTA_PIU_VELOCE:
            return "SPINTA_PIU_VELOCE";
        case TRAZIONE_PIU_VELOCE:
            return "TRAZIONE_PIU_VELOCE";
    }
    return "Enumerazione non contemplata.";
}