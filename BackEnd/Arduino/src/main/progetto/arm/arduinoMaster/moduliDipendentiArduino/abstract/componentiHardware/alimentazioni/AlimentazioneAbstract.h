#ifndef DCC_COMMAND_STATION_ALIMENTAZIONEABSTRACT_H
#define DCC_COMMAND_STATION_ALIMENTAZIONEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreTensione/LetturaTensioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define PERCENTUALE_TOLLERANZA_TENSIONE \
    0.1  // Da 0 a 1. 0.1 corrisponde al 10 %. 5 % è troppo poco perché la tensione dei componenti ausiliari 3.3 V può
         // variare un po'.

// *** CLASSE *** //

class AlimentazioneAbstract {
    // *** VARIABILI *** //

   protected:
    LetturaTensioneAbstract& sensoreTensioneAbstract;
    ProgrammaAbstract& fermoProgramma;
    LoggerAbstract& logger;
    const char* nomeAlimentazione;

    TensioneTipo tensioneMinimaCorrettoFunzionamento;  // Fa anche da ID

    // *** COSTRUTTORE *** //

   public:
    AlimentazioneAbstract(const char* nomeAlimentazione, TensioneTipo tensioneMinima,
                          LetturaTensioneAbstract& sensoreTensioneAbstract, ProgrammaAbstract& fermoProgramma,
                          LoggerAbstract& logger)
        : sensoreTensioneAbstract{sensoreTensioneAbstract}, fermoProgramma{fermoProgramma}, logger{logger} {
        this->nomeAlimentazione = nomeAlimentazione;
        tensioneMinimaCorrettoFunzionamento =
            (TensioneTipo)(tensioneMinima - (tensioneMinima * PERCENTUALE_TOLLERANZA_TENSIONE));
    }

    // *** DEFINIZIONE METODI *** //

    void controllaFunzionamento();
    char* toString();
    float getTensione();
};

#endif