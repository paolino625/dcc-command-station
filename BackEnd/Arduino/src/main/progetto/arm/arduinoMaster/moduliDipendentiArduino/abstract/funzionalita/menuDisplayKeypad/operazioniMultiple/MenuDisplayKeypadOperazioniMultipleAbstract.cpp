// *** INCLUDE *** //

#include "MenuDisplayKeypadOperazioniMultipleAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/modificaValore/ModificaValore.h"

// *** DEFINIZIONE METODI *** //

void MenuDisplayKeypadOperazioniMultipleAbstract::cambioConvoglioCorrenteDisplay(int *idConvoglio) {
    ConvoglioAbstractReale *convoglio = convogli.getConvoglio(*idConvoglio, false);
    modificaValore(idConvoglio, *idConvoglio + 1);

    // Se il convoglio è l'ultimo, allora reinizio daccapo
    if (*idConvoglio == NUMERO_CONVOGLI) {
        modificaValore(idConvoglio, 1);
    }

    // Se il convoglio è presente già sul display oppure nessuna locomotiva vi è assegnata, lo
    // skippo
    while (display.homepage.idConvoglioCorrente1Display == display.homepage.idConvoglioCorrente2Display ||
           convoglio->getLocomotiva1() == nullptr ||
           !(convogli.getConvoglio(*idConvoglio, false))->isPresenteSulTracciato()) {
        modificaValore(idConvoglio, *idConvoglio + 1);
        // Se il convoglio è l'ultimo, allora reinizio daccapo
        if (*idConvoglio == NUMERO_CONVOGLI) {
            modificaValore(idConvoglio, 1);
        }
    }
}

void MenuDisplayKeypadOperazioniMultipleAbstract::cambioConvoglioCorrente1EAggiornoDisplay() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Cambio convoglio corrente...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    cambioConvoglioCorrenteDisplay(&display.homepage.idConvoglioCorrente1Display);

    display.homepage.idLocomotiveConvoglio1(convogli);
    display.homepage.velocitaConvoglio1(convogli);
    display.homepage.direzioneConvoglio1(convogli);
    display.homepage.luciConvoglio1(convogli);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Cambio convoglio corrente... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MenuDisplayKeypadOperazioniMultipleAbstract::cambioConvoglioCorrente2EAggiornoDisplay() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Cambio convoglio corrente...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    cambioConvoglioCorrenteDisplay(&display.homepage.idConvoglioCorrente2Display);

    display.homepage.idLocomotiveConvoglio2(convogli);
    display.homepage.velocitaConvoglio2(convogli);
    display.homepage.direzioneConvoglio2(convogli);
    display.homepage.luciConvoglio2(convogli);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Cambio convoglio corrente... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MenuDisplayKeypadOperazioniMultipleAbstract::cambioLuciConvoglio1EAggiornoDisplay(ConvogliAbstract &convogli) {
    ConvoglioAbstractReale *convoglioCorrente =
        convogli.getConvoglio(display.homepage.idConvoglioCorrente1Display, false);
    // Accendo/spengo luci
    convoglioCorrente->inverteLuci();

    display.homepage.luciConvoglio1(convogli);
}

void MenuDisplayKeypadOperazioniMultipleAbstract::cambioLuciConvoglio2EAggiornoDisplay(ConvogliAbstract &convogli) {
    ConvoglioAbstractReale *convoglioCorrente =
        convogli.getConvoglio(display.homepage.idConvoglioCorrente2Display, false);
    // Accendo/spengo luci
    convoglioCorrente->inverteLuci();

    display.homepage.luciConvoglio2(convogli);
}