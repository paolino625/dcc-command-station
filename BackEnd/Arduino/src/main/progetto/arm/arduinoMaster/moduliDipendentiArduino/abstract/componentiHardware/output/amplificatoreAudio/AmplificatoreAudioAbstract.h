#ifndef DCC_COMMAND_STATION_AMPLIFICATOREAUDIOABSTRACT_H
#define DCC_COMMAND_STATION_AMPLIFICATOREAUDIOABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/i2c/I2cAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/wire/WireAbstract.h"

// *** DEFINE *** //

#define VOLUME_AMPLIFICATORE_DEFAULT 50

// *** CLASSE *** //

class AmplificatoreAudioAbstract {
   private:
    // *** VARIABILI *** //

    ProgrammaAbstract& programma;
    I2cAbstract& i2c;
    WireAbstract& wire;
    int indirizzoI2C;
    LoggerAbstract& logger;

    VolumeTipo volume;

    // *** COSTRUTTORE *** //

   public:
    AmplificatoreAudioAbstract(ProgrammaAbstract& programma, I2cAbstract& i2c, WireAbstract& wire, int indirizzoI2C,
                               LoggerAbstract& logger)
        : programma(programma),
          i2c(i2c),
          wire{wire},
          indirizzoI2C{indirizzoI2C},
          logger{logger},
          volume{VOLUME_AMPLIFICATORE_DEFAULT} {}

    // *** DEFINIZIONE METODI *** //

    void inizializza();
    int getIndirizzoI2C();
    VolumeTipo getVolume() const;
    void setVolume(VolumeTipo volumeAmplificatoreDaImpostare);
};

#endif