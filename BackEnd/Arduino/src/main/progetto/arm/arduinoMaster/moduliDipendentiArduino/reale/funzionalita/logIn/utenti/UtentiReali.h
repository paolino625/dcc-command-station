#ifndef DCC_COMMAND_STATION_UTENTIREALI_H
#define DCC_COMMAND_STATION_UTENTIREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/utenti/UtentiAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern UtentiAbstract utentiReali;

#endif
