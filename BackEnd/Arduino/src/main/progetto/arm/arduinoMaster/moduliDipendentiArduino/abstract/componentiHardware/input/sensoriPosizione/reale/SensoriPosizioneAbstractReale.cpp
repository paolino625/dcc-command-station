// *** INCLUDE *** //

#include "SensoriPosizioneAbstractReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/mqtt/MqttArduinoMasterReale.h"

// *** DEFINIZIONE METODI *** //

void SensoriPosizioneAbstractReale::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensori posizione... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_SENSORI_POSIZIONE_TOTALI; i++) {
        SensorePosizioneAbstract* sensorePosizione = new SensorePosizioneAbstract(i, programma, mqttArduinoMaster, logger);
        sensoriPosizione[i] = sensorePosizione;
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensori posizione... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void SensoriPosizioneAbstractReale::reset(boolean stampaLog) {
    if (stampaLog) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset sensori posizione... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
    for (int i = 1; i < NUMERO_SENSORI_POSIZIONE_TOTALI; i++) {
        sensoriPosizione[i]->reset(true);
    }

    if (stampaLog) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset sensori posizione... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void SensoriPosizioneAbstractReale::resetInAttesaPassaggioPrimaLocomotivaConvoglio() {
    for (int i = 1; i < NUMERO_SENSORI_POSIZIONE_TOTALI; i++) {
        sensoriPosizione[i]->resetInAttesaPassaggioPrimaLocomotivaConvoglio();
    }
}

bool SensoriPosizioneAbstractReale::esisteIdSensore(byte idSensore) {
    if (idSensore >= 1 && idSensore <= NUMERO_SENSORI_POSIZIONE_TOTALI - 1) {
        return true;
    } else {
        return false;
    }
}

SensorePosizioneAbstract* SensoriPosizioneAbstractReale::getSensorePosizione(int indiceSensore) {
    if (esisteIdSensore(indiceSensore)) {
        return sensoriPosizione[indiceSensore];
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Indice sensore non valido.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        return sensoriPosizione[0];
    }
}