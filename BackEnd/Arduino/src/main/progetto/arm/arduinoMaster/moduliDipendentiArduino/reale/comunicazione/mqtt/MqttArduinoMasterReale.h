#ifndef MQTT_H
#define MQTT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/mqtt/MqttArduinoMasterAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/abstract/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class MqttArduinoMasterReale : public MqttArduinoMasterAbstract {
    // *** VARIABILI *** //

   public:
    MqttCoreArduinoArmAbstract& mqttCore;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

    MqttArduinoMasterReale(MqttCoreArduinoArmAbstract& mqttCore, LoggerAbstract& logger) : mqttCore(mqttCore), logger(logger) {}

    // *** METODI *** //

    void inizializzaVariabiliTriggerInvioMqtt() override;

    void inviaStatoOggettiAggiornati() override;
    void inviaStatoLocomotiva(IdLocomotivaTipo idLocomotiva) override;
    void inviaStatoConvoglio(IdConvoglioTipo idConvoglio) override;
    void inviaStatoScambio(IdScambioTipo idScambio) override;
    void inviaStatoSezione(int indiceSezione) override;
    void inviaStatoSensorePosizione(IdSensorePosizioneTipo idSensore) override;
    void inviaInfoAutopilot() override;
    void invioStatoLocomotiveAggiornate() override;
    void invioStatoConvogliAggiornati() override;
    void invioStatoScambiAggiornati() override;
    void invioStatoSensoriPosizioneAggiornati() override;
    void invioStatoSezioniAggiornate() override;
    void invioInfoAutopilotAggiornato() override;
    void invioTriggerArduinoReady() override;
    void inviaMessaggioLog(char* payload) override;

    MqttCoreArduinoArmAbstract& getMqttCore() override;
};

// *** DICHIARAZIONE VARIABILI *** //

extern MqttArduinoMasterReale mqttArduinoMasterReale;

#endif
