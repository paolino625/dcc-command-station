#ifndef DCC_COMMAND_STATION_ARDUINOSLAVESENSORI_H
#define DCC_COMMAND_STATION_ARDUINOSLAVESENSORI_H

// **** INCLUDE **** //

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/Timestamp.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"

// **** CLASSE **** //

class ArduinoSlaveSensori {
    // **** VARIABILI **** //

    LoggerAbstract &logger;

    // **** COSTRUTTORE **** //
   public:
    ArduinoSlaveSensori(LoggerAbstract &logger) : logger{logger} {}

    // **** METODI PUBBLICI **** //

    void inviaAck(const char *stringaRicevuta) {
        // Rinvio stringa che ho ricevuto come ACK
        Serial3.write(stringaRicevuta, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

        if (DEBUGGING_ACK_INVIATI_AD_ARDUINO_SLAVE_RELAY) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio ACK Arduino Slave Sensori: ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, stringaRicevuta,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " ...OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        }
    }

    void riceve() {
        if (Serial3.available() > 0) {
            // Ogni volta che ricevo un messaggio resetto il timestamp, utile per
            // accertarmi che Arduino Mega sia controlloPresenzaArduinoSlaveAttivo e il segnale Interrupt sia
            // valido.
            char stringaRicevuta[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
            Serial3.readBytes(stringaRicevuta, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

            if (DEBUGGING_STRINGHE_RICEVUTE_DA_ARDUINO_SLAVE_SENSORI) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Stringa ricevuta da Arduino Slave Sensori: ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, stringaRicevuta,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }

            if (strcmp(stringaRicevuta, "ping") == 0) {
                inviaAck("ping");

                // stampo("Ping Arduino Slave Sensori ricevuto.\n");
                timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp = millis();
            } else if (strncmp(stringaRicevuta, "sens", strlen("sens")) == 0) {
                // Copio stringa ricevuta adesso per l'Ack, in quanto strtok la
                // cambia.
                char stringaAck[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
                strcpy(stringaAck, stringaRicevuta);

                char *token;
                // Non considero la parola "sensore"
                strtok(stringaRicevuta, " ");

                token = strtok(nullptr, " ");

                byte numeroSensoreAzionato = atoi(token);
                SensorePosizioneAbstract *sensorePosizione =
                    sensoriPosizioneReali.getSensorePosizione(numeroSensoreAzionato);
                sensorePosizione->setStato(true);

                LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Azionato sensore n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(numeroSensoreAzionato).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                inviaAck(stringaAck);

                // Se Arduino Slave Sensori non riesce a inviare il messaggio di un
                // sensore azionato perché proprio in quel momento la connessione
                // viene persa, Arduino Master riceve il messaggio di nuovo e lo
                // considera come un messaggio di ping in quanto mostra la
                // connessione funzionante
                timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp = millis();
            }
        }
    }
};

// **** DICHIARAZIONE VARIABILI **** //

extern ArduinoSlaveSensori arduinoSlaveSensori;

#endif