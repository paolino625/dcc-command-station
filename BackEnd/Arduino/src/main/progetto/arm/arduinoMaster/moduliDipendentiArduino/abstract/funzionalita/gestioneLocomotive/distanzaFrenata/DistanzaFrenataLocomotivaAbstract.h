#ifndef DCC_COMMAND_STATION_DISTANZAFRENATA_H
#define DCC_COMMAND_STATION_DISTANZAFRENATA_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotiva/NumeroStepVelocita.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class DistanzaFrenataLocomotivaAbstract {
    // *** VARIABILI *** //

    LoggerAbstract& logger;

    LunghezzaTracciatoTipo distanzaFrenataLocomotivaCm[NUMERO_STEP_VELOCITA_DISPLAY_KMH];

    // *** COSTRUTTORE *** //

   public:
    DistanzaFrenataLocomotivaAbstract(LoggerAbstract& logger) : logger(logger) {}

    // *** METODI *** //

    void inizializza();
    static LunghezzaTracciatoTipo calcoloDistanzaFrenataLocomotiva(VelocitaRotabileKmHTipo velocitaKmHIniziale);
    LunghezzaTracciatoTipo getDistanzaFrenataPerVelocitaKmH(VelocitaRotabileKmHTipo velocitaKmH);
    char* toString();
};

#endif
