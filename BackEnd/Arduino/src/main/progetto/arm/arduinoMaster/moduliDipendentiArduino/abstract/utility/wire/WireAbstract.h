#ifndef DCC_COMMAND_STATION_WIREABSTRACT_H
#define DCC_COMMAND_STATION_WIREABSTRACT_H

// *** CLASSE *** //

class WireAbstract {
   public:
    virtual auto begin() -> void = 0;
    virtual auto beginTransmission(int address) -> void = 0;
    virtual auto write(int data) -> void = 0;
    virtual auto endTransmission() -> int = 0;
};

#endif