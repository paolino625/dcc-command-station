#ifndef DCC_COMMAND_STATION_SENSORETENSIONEABSTRACT_H
#define DCC_COMMAND_STATION_SENSORETENSIONEABSTRACT_H

// *** INCLUDE *** //

#include "LetturaTensioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/AnalogPinAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define NUMERO_CAMPIONI_SENSORI_TENSIONE 50

// *** CLASSE *** //

class SensoreTensioneAbstract : public LetturaTensioneAbstract {
    // *** VARIABILI *** //
   private:
    AnalogPinAbstract& pinSensore;
    int indiceAttualeBufferStatoSensoriTensione;
    TensioneTipo tensione;
    // N.B. Considerando che si tratta di un array, fare attenzione al consumo di memoria.
    TensioneTipo bufferTensione[NUMERO_CAMPIONI_SENSORI_TENSIONE];
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    SensoreTensioneAbstract(AnalogPinAbstract& pinSensore, LoggerAbstract& logger)
        : pinSensore{pinSensore}, indiceAttualeBufferStatoSensoriTensione{0}, logger{logger} {}

    // *** DEFINIZIONE METODI *** //

    void effettuaLetturaAggiornaUnaEntryCampionamento();
    void effettuaLetturaAggiornaTutteEntryCampionamento();
    void stampaInfo();
    TensioneTipo getTensione();

   private:
    char* toString();
    TensioneTipo misuraMediaCampioni();
    int leggoPinAnalogico();
};

#endif