// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void liberoLista(char *lista[], byte numeroElementiLista) {
    for (int i = 1; i < numeroElementiLista; i++) {
        free(lista[i]);
    }
}

void stampoLista(char *arrayStringhe[], byte numeroElementiLista) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                          "Stampo lista: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    for (int i = 1; i < numeroElementiLista + 1; i++) {
        if (i != numeroElementiLista) {
            LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, arrayStringhe[i],
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false, false);
        } else {
            LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::DEBUG_, arrayStringhe[i],
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        }
    }
}