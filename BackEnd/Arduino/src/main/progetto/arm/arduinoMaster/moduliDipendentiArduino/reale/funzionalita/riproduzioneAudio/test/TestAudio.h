#ifndef DCC_COMMAND_STATION_TESTAUDIO_H
#define DCC_COMMAND_STATION_TESTAUDIO_H

// *** DICHIARAZIONE VARIABILI *** //

extern bool statoBottonePrecedente;
extern bool bottonePremuto;

// *** DICHIARAZIONE FUNZIONI *** //

void controlloPulsante();
void riproducoFileAudioTest();

#endif
