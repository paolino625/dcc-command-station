#ifndef DCC_COMMAND_STATION_KEYPADABSTRACT_H
#define DCC_COMMAND_STATION_KEYPADABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/core/KeypadCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/DisplayCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** DEFINE *** //

// Definisco in millisecondi il tempo massimo in cui Arduino rimarrà in ascolto di eventuali INPUT dal keypad per il
// menù principale e i sottomenù.
#define TEMPO_LIMITE_INPUT_KEYPAD_SOTTOMENU 1

// Millisecondi dopo i quali il keypad torna in ascolto dopo un primo input
#define DELAY_KEYPAD_TRA_DUE_INPUT 500

// *** CLASSE *** //

class KeypadAbstract {
    // *** VARIABILI *** //

   public:
    DisplayCoreAbstract &displayCore;
    KeypadCoreAbstract &keypadCore;
    ProgrammaAbstract &programma;
    LocomotiveAbstract &locomotive;
    ConvogliAbstract &convogli;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

    KeypadAbstract(DisplayCoreAbstract &displayCore, KeypadCoreAbstract &keypadCore, ProgrammaAbstract &programma,
                   LocomotiveAbstract &locomotive, ConvogliAbstract &convogli, LoggerAbstract &logger)
        : displayCore(displayCore),
          keypadCore(keypadCore),
          programma(programma),
          locomotive(locomotive),
          convogli(convogli),
          logger(logger) {}

    // *** DEFINIZIONE METODI *** //

    int keyPadLeggoNumero1CifraTempoLimite(bool stampaNumeroInserito, int tempoLimiteInput);

    int leggoNumero1Cifra(bool stampaNumeroInserito);
    int leggoNumero2Cifre(bool stampaNumeroInserito);
    int leggoNumero3Cifre(bool stampaNumeroInserito);

    int leggoPosizioneConvoglioDisplay(bool stampaNumeroInserito);
    IdConvoglioTipo leggoIdConvoglioEVerifico(bool convogliInUso, bool convogliNonInUso);
    IdLocomotivaTipo leggoIdLocomotiva(bool stampaNumeroInserito);
    IdLocomotivaTipo leggoIdLocomotivaEVerifico(bool locomotiveInUso, bool locomotiveNonInUso);
    IdVagoneTipo leggoIdVagone(bool stampaNumeroInserito);
    IdVagoneTipo leggoIdVagoneEVerifico(bool vagoniInUso, bool vagoniNonInUso, VagoniAbstract &vagoni);
    IndirizzoDecoderTipo leggoIndirizzoLocomotiva();
    StepVelocitaDecoderTipo leggoStepVelocitaEVerifico();
    StepVelocitaDecoderTipo leggoStepVelocita();

    IdSensorePosizioneTipo leggoNumeroSensore();
    IdScambioTipo leggoNumeroScambio();
    int leggoLivelloVolume();
    int leggoMoltiplicatoreKeepAlive();
    int leggoSceltaMenuUtente();
};

#endif
