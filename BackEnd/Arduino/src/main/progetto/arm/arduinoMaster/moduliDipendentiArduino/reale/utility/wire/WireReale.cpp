// *** INCLUDE *** //

#include "WireReale.h"

// *** DEFINIZIONE METODI *** //

// N.B.: è necessario utilizzare Wire1, che corrisponde ai pin SDA1 e SCL1 su Arduino Giga. Questi sono differenti dai
// pin SDA e SCL associati a Wire, che si trovano sui pin 20 e 21.

void WireReale::begin() { Wire1.begin(); }

void WireReale::beginTransmission(int indirizzo) { Wire1.beginTransmission(indirizzo); }

void WireReale::write(int data) { Wire1.write(data); }

int WireReale::endTransmission() {
    int result = Wire1.endTransmission();
    return result;
}

// *** DEFINIZIONE VARIABILI *** //

WireReale wireReale = WireReale();