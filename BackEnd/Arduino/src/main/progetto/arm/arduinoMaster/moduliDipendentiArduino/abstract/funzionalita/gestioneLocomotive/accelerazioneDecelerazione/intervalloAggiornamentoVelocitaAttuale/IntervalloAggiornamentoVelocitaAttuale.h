#ifndef ARDUINO_INTERVALLOAGGIORNAMENTOVELOCITAATTUALE_H
#define ARDUINO_INTERVALLOAGGIORNAMENTOVELOCITAATTUALE_H

// *** DEFINE *** //

/*
Intervallo di aggiornamento della velocità attuale di una locomotiva quando l'autopilot non è attivo.
Si consiglia di non abbassare troppo questo valore. Un intervallo troppo breve rende accelerazione e decelerazione
meno realistiche e può causare problemi di stabilità al convoglio, aumentando il rischio di deragliamento,
specialmente in curva.
Impostare 500 per un'accelerazione/decelerazione graduale.
*/
#define INTERVALLO_AGGIORNAMENTO_VELOCITA_ATTUALE_CONVOGLIO_GUIDA_MANUALE 250

#endif
