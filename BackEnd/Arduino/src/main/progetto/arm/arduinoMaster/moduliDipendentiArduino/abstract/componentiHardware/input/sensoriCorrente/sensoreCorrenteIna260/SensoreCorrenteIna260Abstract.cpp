// *** INCLUDE *** //

#include "SensoreCorrenteIna260Abstract.h"

// *** DEFINIZIONE METODI *** //

void SensoreCorrenteIna260Abstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensore corrente INA260... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    if (!sensoreCorrenteIna260Core.begin(indirizzoI2C)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non trovo il chip INA260.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        display.errori.inizializzazione.ina260();
        programma.ferma(false);
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensore corrente INA260... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void SensoreCorrenteIna260Abstract::aggiornaCampionamentoCorrente() {
    // Un minimo di campionamento viene già fatto dalla libreria INA219

    bufferCorrente[indiceAttualeBufferStatoSensoreCorrenteIna260] =
        (CorrenteTipo)sensoreCorrenteIna260Core.readCurrent();
    indiceAttualeBufferStatoSensoreCorrenteIna260++;
    if (indiceAttualeBufferStatoSensoreCorrenteIna260 == NUMERO_CAMPIONI_SENSORE_CORRENTE_INA260) {
        indiceAttualeBufferStatoSensoreCorrenteIna260 = 0;
    }
    misuraMediaCampioniSensoreCorrenteIna260();
}

char* SensoreCorrenteIna260Abstract::toString() const {
    std::string stringa;
    stringa += "Stato sensore corrente INA260: ";
    stringa += std::to_string(statoSensoreCorrenteIna260Campionato);
    stringa += " mA";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

void SensoreCorrenteIna260Abstract::effettuaLetturaAggiornaUnaEntryCampionamento() {
    // Il campionamento viene già effettuato all'interno di INA260
}

void SensoreCorrenteIna260Abstract::effettuaLetturaAggiornaTutteEntryCampionamento() {
    // Il campionamento viene già effettuato all'interno di INA260
}

void SensoreCorrenteIna260Abstract::misuraMediaCampioniSensoreCorrenteIna260() {
    CorrenteTipo sommaCampioni = 0;

    for (int i = 0; i < NUMERO_CAMPIONI_SENSORE_CORRENTE_INA260; i++) {
        sommaCampioni += bufferCorrente[i];
    }

    CorrenteTipo valorePesato = (CorrenteTipo)(sommaCampioni / NUMERO_CAMPIONI_SENSORE_CORRENTE_INA260);

    statoSensoreCorrenteIna260Campionato = valorePesato;
}

int SensoreCorrenteIna260Abstract::getIndirizzoI2C() const { return indirizzoI2C; }

int SensoreCorrenteIna260Abstract::getCorrente() const { return statoSensoreCorrenteIna260Campionato; }

float SensoreCorrenteIna260Abstract::getTensione() { return sensoreCorrenteIna260Core.readBusVoltage() / 1000; }