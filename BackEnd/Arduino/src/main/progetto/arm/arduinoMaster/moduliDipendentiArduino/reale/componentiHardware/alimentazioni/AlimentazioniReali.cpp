// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/alimentazioni/AlimentazioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/SensoreCorrenteIna219Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/SensoreCorrenteIna260Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriTensione/SensoriTensioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AlimentazioneAbstract alimentazione12VReale("Luci interruttori e relay", 12, sensoreTensioneLuciInterruttoriReale,
                                            programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneLED1Reale("LED 1", 5, sensoreTensioneLED1Reale, programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneLED2Reale("LED 2", 5, sensoreTensioneLED2Reale, programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneLED3Reale("LED 3", 5, sensoreTensioneLED3Reale, programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneCDUReale("CDU", 24, sensoreTensioneCDUReale, programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneTracciatoReale("Tracciato", 15, sensoreCorrenteIna219Reale, programmaReale,
                                                  loggerReale);
AlimentazioneAbstract alimentazione5VTotaleReale("5V Totale", 5, sensoreCorrenteIna260Reale, programmaReale,
                                                 loggerReale);
AlimentazioneAbstract alimentazioneSensoriPosizioneReale("Sensori posizione", 5, sensoreTensioneSensoriPosizioneReale,
                                                         programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneArduinoReale("Arduino", 5, sensoreTensioneArduinoReale, programmaReale, loggerReale);
AlimentazioneAbstract alimentazioneComponentiAusiliari5VReale("Componenti ausiliari 5V", 5,
                                                              sensoreTensioneComponentiAusiliari5VReale, programmaReale,
                                                              loggerReale);
AlimentazioneAbstract alimentazioneComponentiAusiliari3VReale("Componenti ausiliari 3V", 3.3,
                                                              sensoreTensioneComponentiAusiliari3VReale, programmaReale,
                                                              loggerReale);
