// *** INCLUDE *** //

#include "LocomotivaAbstract.h"

#include <cstring>
#include <string>

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/conversioneTempo/ConversioneTempo.h"

// *** DEFINIZIONE METODI *** //

void LocomotivaAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo locomotiva n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    accendeLuciBasilari();
    // Le restanti funzioni le metto tutte a false
    for (int r = 1; r < NUMERO_FUNZIONI_AUSILIARI_DECODER; r++) {
        setStatoFunzioniAusiliari(r, false);
    }
    aggiornaLuciAusiliarie(true);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo locomotiva n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
}

void LocomotivaAbstract::cambiaDirezione() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Cambio direzione locomotiva n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
    direzione.avantiDisplay = !direzione.avantiDisplay;
    aggiornaLuciAusiliarie(true);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Cambio direzione locomotiva n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... OK", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
}

void LocomotivaAbstract::aggiornaDirezioneAvantiDecoder() {
    if (direzione.invertita) {
        direzione.avantiDecoder = !direzione.avantiDisplay;
    } else {
        direzione.avantiDecoder = direzione.avantiDisplay;
    }
}

void LocomotivaAbstract::accendeLuciVagoni() {
    // TODO Controllare Sul decoder le luci sono state collegate a AUX3 che corrisponde a F5.
    setStatoFunzioniAusiliari(5, true);

    // TODO Togliere questi altri
    setStatoFunzioniAusiliari(1, true);
    setStatoFunzioniAusiliari(2, true);
    setStatoFunzioniAusiliari(3, true);
    setStatoFunzioniAusiliari(4, true);
    setStatoFunzioniAusiliari(6, true);
    setStatoFunzioniAusiliari(7, true);
    setStatoFunzioniAusiliari(8, true);
    setStatoFunzioniAusiliari(9, true);
    setStatoFunzioniAusiliari(10, true);
    setStatoFunzioniAusiliari(11, true);
    setStatoFunzioniAusiliari(12, true);
}

void LocomotivaAbstract::aggiornaLuciAusiliarie(bool luciConvoglioAccese) {
    if (strcmp(getCodice(), "E402B.164") == 0) {
        if (luciConvoglioAccese) {
            if (getDirezioneAvantiDisplay()) {
                setStatoFunzioniAusiliari(1, false);  // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, true);   // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, false);  // Faro posteriore
                setStatoFunzioniAusiliari(6, true);   // Faro anteriore
            } else {
                setStatoFunzioniAusiliari(1, true);   // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, false);  // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, true);   // Faro posteriore
                setStatoFunzioniAusiliari(6, false);  // Faro anteriore
            }
        } else {
            if (getDirezioneAvantiDisplay()) {
                setStatoFunzioniAusiliari(1, false);  // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, true);   // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, false);  // Faro posteriore
                setStatoFunzioniAusiliari(6, false);  // Faro anteriore
            } else {
                setStatoFunzioniAusiliari(1, true);   // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, false);  // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, false);  // Faro posteriore
                setStatoFunzioniAusiliari(6, false);  // Faro anteriore
            }
        }
    } else if (strcmp(getCodice(), "E652.019") == 0) {
        if (luciConvoglioAccese) {
            if (getDirezioneAvantiDisplay()) {
                setStatoFunzioniAusiliari(1, false);  // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, true);   // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, false);  // Faro posteriore
                setStatoFunzioniAusiliari(6, true);   // Faro anteriore
            } else {
                setStatoFunzioniAusiliari(1, true);   // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, false);  // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, true);   // Faro posteriore
                setStatoFunzioniAusiliari(6, false);  // Faro anteriore
            }
        } else {
            if (getDirezioneAvantiDisplay()) {
                setStatoFunzioniAusiliari(1, false);  // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, true);   // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, false);  // Faro posteriore
                setStatoFunzioniAusiliari(6, false);  // Faro anteriore
            } else {
                setStatoFunzioniAusiliari(1, true);   // Luce rossa anteriore
                setStatoFunzioniAusiliari(2, false);  // Luce rossa posteriore
                setStatoFunzioniAusiliari(5, false);  // Faro posteriore
                setStatoFunzioniAusiliari(6, false);  // Faro anteriore
            }
        }
    }
}

void LocomotivaAbstract::spegneLuciAusiliarie() {
    // Spengo tutte le luci, anche quelle posteriore che solitamente non vengono spente quando vengono spente le luci
    // basilari
    if (strcmp(getCodice(), "E402B.164") == 0) {
        setStatoFunzioniAusiliari(1, false);
        setStatoFunzioniAusiliari(2, false);
        setStatoFunzioniAusiliari(5, false);
        setStatoFunzioniAusiliari(6, false);
    }
}

void LocomotivaAbstract::accendeLuciBasilari() { statoFunzioniAusiliari[0] = true; }

void LocomotivaAbstract::spegneLuciBasilari() { statoFunzioniAusiliari[0] = false; }

void LocomotivaAbstract::avviaTimerParziale() {
    if (DEBUGGING_TIMER_LOCOMOTIVE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Avvio timer locomotiva ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
    timerInizio = tempo.milliseconds();
}

void LocomotivaAbstract::fermaTimerParziale() {
    TimestampTipo timerFine = tempo.milliseconds();
    TimestampTipo periodoMovimento = timerFine - timerInizio;
    TimestampTipo periodoMovimentoSecondi = convertoMillisecondiASecondi(periodoMovimento);
    tempoFunzionamento.ultimaManutenzione += periodoMovimentoSecondi;
    tempoFunzionamento.totale += periodoMovimentoSecondi;

    if (DEBUGGING_TIMER_LOCOMOTIVE) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Fermo timer locomotiva ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        logger.cambiaTabulazione(1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "RESOCONTO",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Tempo trascorso: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(periodoMovimentoSecondi).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " secondi",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Tempo da ultima manutenzione: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(tempoFunzionamento.ultimaManutenzione).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " secondi",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Tempo totale: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(tempoFunzionamento.totale).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " secondi",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        logger.cambiaTabulazione(-1);
    }

    fileLocomotiveDaAggiornare = true;
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente con delete[].
char* LocomotivaAbstract::toString() {
    std::string stringa;
    stringa += "\nINFO LOCOMOTIVA\n";
    stringa += "\nID: ";
    stringa += std::to_string(id);
    stringa += "\nIndirizzo decoder: ";
    stringa += std::to_string(indirizzoDecoderDCC);
    stringa += "\nNome: ";
    stringa += infoRotabile.nome;
    stringa += "\nCodice: ";
    stringa += infoRotabile.codice;
    stringa += "\nTipologia: ";
    stringa += std::to_string(infoRotabile.tipologia);
    stringa += "\nCategoria: ";
    stringa += std::to_string(infoRotabile.categoria);
    stringa += "\nLivrea: ";
    stringa += std::to_string(infoRotabile.livrea);
    stringa += "\nProduttore: ";
    stringa += std::to_string(infoRotabile.produttore);
    stringa += "\nNumero modello: ";
    stringa += infoRotabile.numeroModello;
    stringa += "\nLunghezza: ";
    stringa += std::to_string(lunghezzaCm);
    stringa += "\nDirezione invertita: ";
    stringa += direzione.invertita ? "true" : "false";
    stringa += "\nDirezione avanti display: ";
    stringa += direzione.avantiDisplay ? "true" : "false";
    stringa += "\nTempo funzionamento ultima manutenzione: ";
    stringa += std::to_string(tempoFunzionamento.ultimaManutenzione);
    stringa += "\nTempo funzionamento totale: ";
    stringa += std::to_string(tempoFunzionamento.totale);
    stringa += "\nVelocità massima Km/h: ";
    stringa += std::to_string(velocita.massimaKmH);
    stringa += "\n";

    /*
     Righe commentate perché sono troppi byte da memorizzare temporaneamente in una stringa, Arduino finirebbe la
    memoria e crasherebbe stringa += "\nMapping velocità step avanti: \n"; for (int i = 0; i <
    NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) { stringa += "Velocità: "; stringa += std::to_string(i); stringa += " - Step:
    "; stringa += std::to_string(getMappingVelocitaStepAvanti(i)); stringa += "\n";
    }

    stringa += "\nMapping velocità step indietro: \n";
    for (int i = 0; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
        stringa += "Velocità: ";
        stringa += std::to_string(i);
        stringa += " - Step: ";
        stringa += std::to_string(getMappingVelocitaStepIndietro(i));
        stringa += "\n";
    }
     */

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

IdLocomotivaTipo LocomotivaAbstract::getId() const { return id; }

IndirizzoDecoderTipo LocomotivaAbstract::getIndirizzoDecoderDcc() const { return indirizzoDecoderDCC; }

char* LocomotivaAbstract::getNome() { return infoRotabile.nome; }

char* LocomotivaAbstract::getCodice() { return infoRotabile.codice; }

TipologiaRotabile LocomotivaAbstract::getTipologia() { return infoRotabile.tipologia; }

CategoriaRotabilePasseggeri LocomotivaAbstract::getCategoria() { return infoRotabile.categoria; }

LivreaRotabile LocomotivaAbstract::getLivrea() { return infoRotabile.livrea; }

ProduttoreRotabile LocomotivaAbstract::getProduttore() { return infoRotabile.produttore; }

char* LocomotivaAbstract::getNumeroModello() { return infoRotabile.numeroModello; }

LunghezzaRotabileTipo LocomotivaAbstract::getLunghezzaCm() const { return lunghezzaCm; }

bool LocomotivaAbstract::getDirezioneInvertita() const { return direzione.invertita; }

bool LocomotivaAbstract::getDirezioneAvantiDisplay() const { return direzione.avantiDisplay; }

TimestampTipo LocomotivaAbstract::getTempoFunzionamentoUltimaManutenzione() const {
    return tempoFunzionamento.ultimaManutenzione;
}

TimestampTipo LocomotivaAbstract::getTempoFunzionamentoTotale() const { return tempoFunzionamento.totale; }

VelocitaRotabileKmHTipo LocomotivaAbstract::getVelocitaMassimaKmH() const { return velocita.massimaKmH; }

StepVelocitaDecoderTipo LocomotivaAbstract::getMappingVelocitaStepAvanti(StepVelocitaKmHTipo step) {
    return mappingVelocitaStep.avanti[step];
}

StepVelocitaDecoderTipo LocomotivaAbstract::getMappingVelocitaStepIndietro(StepVelocitaKmHTipo step) {
    return mappingVelocitaStep.indietro[step];
}

VelocitaRotabileKmHTipo LocomotivaAbstract::getVelocitaMassima() const { return velocita.massimaKmH; }

VelocitaRotabileKmHTipo LocomotivaAbstract::getVelocitaImpostata() const { return velocita.impostata; }

VelocitaRotabileKmHTipo LocomotivaAbstract::getVelocitaAttuale() const { return velocita.attuale; }

bool LocomotivaAbstract::getDirezioneAvantiDecoder() const { return direzione.avantiDecoder; }

TimestampTipo LocomotivaAbstract::getTimerInizio() const { return timerInizio; }

bool LocomotivaAbstract::getStatoFunzioniAusiliari(byte numeroFunzioneAusiliare) {
    return statoFunzioniAusiliari[numeroFunzioneAusiliare];
}

void LocomotivaAbstract::setId(IdLocomotivaTipo idNuovo) { id = idNuovo; }

void LocomotivaAbstract::setIndirizzoDecoderDcc(IndirizzoDecoderTipo indirizzoDecoderDCCNuovo) {
    indirizzoDecoderDCC = indirizzoDecoderDCCNuovo;
}

void LocomotivaAbstract::setNome(char* nomeNuovo) { strncpy(infoRotabile.nome, nomeNuovo, sizeof(infoRotabile.nome)); }

void LocomotivaAbstract::setCodice(char* codiceNuovo) {
    strncpy(infoRotabile.codice, codiceNuovo, sizeof(infoRotabile.codice));
}

void LocomotivaAbstract::setTipologia(TipologiaRotabile tipologiaNuovo) { infoRotabile.tipologia = tipologiaNuovo; }

void LocomotivaAbstract::setCategoria(CategoriaRotabilePasseggeri categoriaNuovo) {
    infoRotabile.categoria = categoriaNuovo;
}

void LocomotivaAbstract::setLivrea(LivreaRotabile livreaNuovo) { infoRotabile.livrea = livreaNuovo; }

void LocomotivaAbstract::setProduttore(ProduttoreRotabile produttoreNuovo) {
    infoRotabile.produttore = produttoreNuovo;
}

void LocomotivaAbstract::setNumeroModello(char* numeroModelloNuovo) {
    strncpy(infoRotabile.numeroModello, numeroModelloNuovo, sizeof(infoRotabile.numeroModello));
}

void LocomotivaAbstract::setLunghezzaCm(LunghezzaRotabileTipo lunghezzaCmNuovo) { lunghezzaCm = lunghezzaCmNuovo; }

void LocomotivaAbstract::setDirezioneInvertita(bool direzioneInvertitaNuovo) {
    direzione.invertita = direzioneInvertitaNuovo;
}

void LocomotivaAbstract::setDirezioneAvantiDisplay(bool direzioneAvantiDisplayNuovo) {
    direzione.avantiDisplay = direzioneAvantiDisplayNuovo;
}

void LocomotivaAbstract::setTempoFunzionamentoUltimaManutenzione(
    TimestampTipo tempoFunzionamentoUltimaManutenzioneNuovo) {
    tempoFunzionamento.ultimaManutenzione = tempoFunzionamentoUltimaManutenzioneNuovo;
}

void LocomotivaAbstract::setTempoFunzionamentoTotale(TimestampTipo tempoFunzionamentoTotaleNuovo) {
    tempoFunzionamento.totale = tempoFunzionamentoTotaleNuovo;
}

void LocomotivaAbstract::setVelocitaMassimaKmH(VelocitaRotabileKmHTipo velocitaMassimaKmHNuovo) {
    velocita.massimaKmH = velocitaMassimaKmHNuovo;
}

void LocomotivaAbstract::setVelocitaAttuale(int velocitaAttualeNuovo) { velocita.attuale = velocitaAttualeNuovo; }

void LocomotivaAbstract::setVelocitaImpostata(int velocitaImpostataNuovo) {
    velocita.impostata = velocitaImpostataNuovo;
}

void LocomotivaAbstract::setDirezioneAvantiDecoder(bool direzioneAvantiDecoderNuovo) {
    direzione.avantiDecoder = direzioneAvantiDecoderNuovo;
}

void LocomotivaAbstract::setTimerInizio(TimestampTipo timerInizioNuovo) { timerInizio = timerInizioNuovo; }

void LocomotivaAbstract::setStatoFunzioniAusiliari(int idFunzioneAusiliare, bool stato) {
    if (idFunzioneAusiliare >= 0 && idFunzioneAusiliare < NUMERO_FUNZIONI_AUSILIARI_DECODER) {
        statoFunzioniAusiliari[idFunzioneAusiliare] = stato;
    }
}

void LocomotivaAbstract::setMappingVelocitaStepAvanti(StepVelocitaKmHTipo stepVelocitaKmH,
                                                      StepVelocitaDecoderTipo stepVelocitaDecoder) {
    if (stepVelocitaKmH >= 0 && stepVelocitaKmH < NUMERO_STEP_VELOCITA_DISPLAY_KMH) {
        mappingVelocitaStep.avanti[stepVelocitaKmH] = stepVelocitaDecoder;
    }
}

void LocomotivaAbstract::setMappingVelocitaStepIndietro(StepVelocitaKmHTipo stepVelocitaKmH,
                                                        StepVelocitaDecoderTipo stepVelocitaDecoder) {
    if (stepVelocitaKmH >= 0 && stepVelocitaKmH < NUMERO_STEP_VELOCITA_DISPLAY_KMH) {
        mappingVelocitaStep.indietro[stepVelocitaKmH] = stepVelocitaDecoder;
    }
}

void LocomotivaAbstract::setIndirizzoDcc(int nuovoIndirizzoLocomotiva) {
    indirizzoDecoderDCC = nuovoIndirizzoLocomotiva;
}