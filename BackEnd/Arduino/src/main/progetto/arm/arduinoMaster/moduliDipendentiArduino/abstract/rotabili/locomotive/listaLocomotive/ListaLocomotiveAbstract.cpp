// *** INCLUDE *** //

#include "ListaLocomotiveAbstract.h"

#include <cstdlib>
#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void ListaLocomotiveAbstract::inizializza() {
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        listaLocomotive[i] = (char *)malloc(NUMERO_CARATTERI_PER_LOCOMOTIVA_LISTA);  // +1 per il terminatore null
    }
}

int ListaLocomotiveAbstract::crea(bool mostraLocomotiveInUso, bool mostraLocomotiveNonInUso) {
    byte numeroLocomotiveTrovate = 0;

    // stampo("\nOttengo lista locomotive... ");
    byte indiceLista = 1;
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(i, true);
        std::string stringaLocomotiva;

        bool condizione;
        if (mostraLocomotiveInUso && mostraLocomotiveNonInUso) {
            // La condizione è sempre true perché voglio mostrare tutte le locomotive
            condizione = true;
        } else if (mostraLocomotiveInUso && !mostraLocomotiveNonInUso) {
            condizione = convogli.isLocomotivaInUso(i);
        } else if (!mostraLocomotiveInUso && mostraLocomotiveNonInUso) {
            condizione = !convogli.isLocomotivaInUso(i);
        } else {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La funzione crea() non lavora così.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            programma.ferma(true);
        }

        if (condizione) {
            numeroLocomotiveTrovate += 1;
            stringaLocomotiva += std::to_string(locomotiva->getId());
            stringaLocomotiva += ". ";
            stringaLocomotiva += locomotiva->getCodice();

            strcpy(listaLocomotive[indiceLista], stringaLocomotiva.c_str());
            indiceLista += 1;
        }
    }

    return numeroLocomotiveTrovate;
}
