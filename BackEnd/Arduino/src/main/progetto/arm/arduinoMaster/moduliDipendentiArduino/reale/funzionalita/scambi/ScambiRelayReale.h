#ifndef DCC_COMMAND_STATION_SCAMBIRELAYREALE_H
#define DCC_COMMAND_STATION_SCAMBIRELAYREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/ScambiRelayAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ScambiRelayAbstract scambiRelayReale;

#endif