#ifndef DCC_COMMAND_STATION_DISPLAYCOREABSTRACT_H
#define DCC_COMMAND_STATION_DISPLAYCOREABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/core/KeypadCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/componente/DisplayComponenteAbstract.h"
#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

#define RIGHE_DISPLAY 4
#define COLONNE_DISPLAY 20

#define MOSTRA_CREDITS 0

#define INTERVALLO_STAMPA_PAGINE_DISPLAY 4000

// *** CLASSE *** //

class DisplayCoreAbstract {
   private:
    // *** VARIABILI *** //

    DisplayComponenteAbstract& displayCore;
    TempoAbstract& tempo;
    LoggerAbstract& logger;

   public:
    bool avvisoDaMostrare = true;
    byte numeroPaginaSuccessivaDisplay = 1;
    bool singolaPaginaDisplayMostrata = false;
    TimestampTipo timestampDisplayPaginaPrecedente;

    // *** COSTRUTTORE *** //

   public:
    DisplayCoreAbstract(DisplayComponenteAbstract& displayCore, TempoAbstract& tempo, LoggerAbstract& logger)
        : displayCore(displayCore), tempo(tempo), logger(logger) {}

    void attendeConEscapeKey(TimestampTipo tempoLimite,
                             KeypadCoreAbstract& keypadCore);  // I metodi statici non possono essere virtuali

    // *** DICHIARAZIONE METODI *** //

    DisplayComponenteAbstract& getDisplayComponente();

    void stampaTesto1Riga(const char* primaRiga);
    void stampaTesto2Righe(const char* primaRiga, const char* secondaRiga);
    void stampaTesto3Righe(const char* primaRiga, const char* secondaRiga, const char* terzaRiga);
    void stampaTesto4Righe(const char* primaRiga, const char* secondaRiga, const char* terzaRiga,
                           const char* quartaRiga);

    void displayLista(TempoAbstract& tempo, char* lista[], byte numeroElementiLista);
    void displayPaginaLista(byte numeroPagina, char* arrayStringhe[], byte numeroElementiLista);
};

#endif