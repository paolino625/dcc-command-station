#ifndef DCC_COMMAND_STATION_DIGITALPINABSTRACT_H
#define DCC_COMMAND_STATION_DIGITALPINABSTRACT_H

// *** CLASSE *** //

// Dichiarazione di una classe astratta per i Digital Pin

// Quando il codice richiede di usare un digital pin, richiede un'implementazione di questa interfaccia.
class DigitalPinAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    virtual auto setOutputMode() -> void = 0;

    virtual auto setInputMode() -> void = 0;

    virtual auto setInputPullupMode() -> void = 0;

    virtual auto setHigh() -> void = 0;

    virtual auto setLow() -> void = 0;

    // L'attributo [[nodiscard]] indica che il compilatore darà un avviso se il valore restituito da questo metodo non
    // viene utilizzato.
    [[nodiscard]] virtual auto isHigh() const -> bool = 0;

    [[nodiscard]] virtual auto isLow() const -> bool = 0;

    [[nodiscard]] virtual auto legge() const -> bool = 0;
};
#endif
