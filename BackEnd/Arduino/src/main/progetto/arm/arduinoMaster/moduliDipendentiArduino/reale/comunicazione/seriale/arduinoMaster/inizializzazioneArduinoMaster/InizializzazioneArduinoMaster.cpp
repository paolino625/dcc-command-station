// *** INCLUDE *** //

#include "InizializzazioneArduinoMaster.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/PorteSeriali.h"

// *** DEFINIZIONE METODI *** //

boolean inizializzoPortaSerialeArduinoMaster(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                             bool portaSerialeDaVerificare, bool beepAttivo) {
    for (int i = 0; i < NUMERO_TENTATIVI_INIZIALIZZAZIONE_PORTA_SERIALE; i++) {
        if (beepAttivo) {
            buzzerRealeArduinoMaster.beepAvviso();
        }
        bool portaSerialeInizializzata =
            inizializzoPortaSerialeArduinoGigaArduinoMega(numeroPortaSeriale, velocitaPorta, portaSerialeDaVerificare);
        if (portaSerialeInizializzata) {
            if (beepAttivo) {
                buzzerRealeArduinoMaster.beepOk();
            }

            return true;
        }
    }

    return false;
}