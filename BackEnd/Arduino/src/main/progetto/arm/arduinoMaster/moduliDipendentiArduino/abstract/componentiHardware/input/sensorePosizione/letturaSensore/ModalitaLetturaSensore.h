#ifndef ARDUINO_MODALITALETTURASENSORE_H
#define ARDUINO_MODALITALETTURASENSORE_H

// *** ENUMERAZIONI *** //

// Nel caso in cui l'azionamento del sensore deve essere considerato per ogni convoglio passato (autopilot) o per ogni
// locomotiva (mapping locomotiva).
enum class ModalitaLetturaSensore { LETTURA_LOCOMOTIVA, LETTURA_CONVOGLIO };

// *** DICHIARAZIONE VARIABILI *** //

extern ModalitaLetturaSensore modalitaLetturaSensore;

#endif
