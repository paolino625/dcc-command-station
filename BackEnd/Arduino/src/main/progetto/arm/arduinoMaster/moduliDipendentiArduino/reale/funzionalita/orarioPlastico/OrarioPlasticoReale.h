#ifndef DCC_COMMAND_STATION_ORARIOPLASTICOREALE_H
#define DCC_COMMAND_STATION_ORARIOPLASTICOREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/orarioPlastico/OrarioPlasticoAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern OrarioPlasticoAbstract orarioPlasticoReale;

#endif