// *** INCLUDE *** //

#include "DisplayCoreAbstract.h"

#include <cmath>
#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

DisplayComponenteAbstract &DisplayCoreAbstract::getDisplayComponente() { return displayCore; }

void DisplayCoreAbstract::stampaTesto1Riga(const char *primaRiga) {
    displayCore.pulisce();

    displayCore.posizionaCursore(0, 0);
    displayCore.print(primaRiga);
}

void DisplayCoreAbstract::stampaTesto2Righe(const char *primaRiga, const char *secondaRiga) {
    displayCore.pulisce();

    displayCore.posizionaCursore(0, 0);
    displayCore.print(primaRiga);

    displayCore.posizionaCursore(0, 1);
    displayCore.print(secondaRiga);
}

void DisplayCoreAbstract::stampaTesto3Righe(const char *primaRiga, const char *secondaRiga, const char *terzaRiga) {
    displayCore.pulisce();

    displayCore.posizionaCursore(0, 0);
    displayCore.print(primaRiga);

    displayCore.posizionaCursore(0, 1);
    displayCore.print(secondaRiga);

    displayCore.posizionaCursore(0, 2);
    displayCore.print(terzaRiga);
}

void DisplayCoreAbstract::stampaTesto4Righe(const char *primaRiga, const char *secondaRiga, const char *terzaRiga,
                                            const char *quartaRiga) {
    displayCore.pulisce();

    displayCore.posizionaCursore(0, 0);
    displayCore.print(primaRiga);

    displayCore.posizionaCursore(0, 1);
    displayCore.print(secondaRiga);

    displayCore.posizionaCursore(0, 2);
    displayCore.print(terzaRiga);

    displayCore.posizionaCursore(0, 3);
    displayCore.print(quartaRiga);
}

void DisplayCoreAbstract::displayLista(TempoAbstract &tempo, char *lista[], byte numeroElementiLista) {
    float numeroPagine = (float)numeroElementiLista / 4;
    // Uso ceil per arrotondare per eccesso.
    float numeroPagineArrotondato = ceil(numeroPagine);
    bool singolaPaginaDisplay;
    if (numeroPagineArrotondato == 1) {
        singolaPaginaDisplay = true;
    } else {
        singolaPaginaDisplay = false;
    }

    if (tempo.milliseconds() - timestampDisplayPaginaPrecedente > INTERVALLO_STAMPA_PAGINE_DISPLAY) {
        if (singolaPaginaDisplay) {
            if (!singolaPaginaDisplayMostrata) {
                // Se c'è una sola pagina da mostrare mostro la pagina e poi non aggiorno
                getDisplayComponente().pulisce();

                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Visualizzo pagina n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "1",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " sul display",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                displayPaginaLista(1, lista, numeroElementiLista);

                // Imposto a true così non eseguo il refresh dello schermo. Questa variabile viene
                // resettata quando esco dal menù.
                singolaPaginaDisplayMostrata = true;
            }
        } else {
            getDisplayComponente().pulisce();

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Visualizzo pagina n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(numeroPaginaSuccessivaDisplay).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " sul display",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

            displayPaginaLista(numeroPaginaSuccessivaDisplay, lista, numeroElementiLista);

            numeroPaginaSuccessivaDisplay += 1;

            // Se c'è un'altra pagina da visualizzare ok, altrimenti riparto dalla prima
            if ((float)numeroPaginaSuccessivaDisplay > numeroPagineArrotondato) {
                numeroPaginaSuccessivaDisplay = 1;
            }
        }

        timestampDisplayPaginaPrecedente = tempo.milliseconds();
    }
}

void DisplayCoreAbstract::displayPaginaLista(byte numeroPagina, char *arrayStringhe[], byte numeroElementiLista) {
    getDisplayComponente().pulisce();

    int numeroElementoInizialeStampare = 4 * (numeroPagina - 1) + 1;

    // Controllo se ci sono elementi nella lista
    if (numeroElementiLista == 0) {
        getDisplayComponente().posizionaCursore(0, 0);
        getDisplayComponente().print("Non ci sono");
        getDisplayComponente().posizionaCursore(0, 1);
        getDisplayComponente().print("elementi");
    } else {
        // C'è almeno un elemento da stampare
        byte rigaDisplayAttuale = 0;

        for (int i = numeroElementoInizialeStampare; i < numeroElementiLista + 1; i++) {
            // Se ho raggiunto una riga fuori dal display, esco
            if (rigaDisplayAttuale == 4) {
                return;
            }

            getDisplayComponente().posizionaCursore(0, rigaDisplayAttuale);
            getDisplayComponente().print(arrayStringhe[i]);

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "RIGA ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(rigaDisplayAttuale).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, arrayStringhe[i],
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);

            rigaDisplayAttuale++;
        }
    }
}

void DisplayCoreAbstract::attendeConEscapeKey(TimestampTipo tempoLimite, KeypadCoreAbstract &keypadCore) {
    TimestampTipo millisPrecedenteDisplayAttesa = tempo.milliseconds();

    while (true) {
        TimestampTipo millisCorrenteDisplayAttesa = tempo.milliseconds();
        if (millisCorrenteDisplayAttesa - millisPrecedenteDisplayAttesa >= tempoLimite) {
            millisPrecedenteDisplayAttesa = millisCorrenteDisplayAttesa;
            return;
        }

        if (keypadCore.leggoCarattere() != '-') {
            return;
        }
    }
}