// *** INCLUDE *** //

#include "Arduino.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void resetArduinoSlaveRelay() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Resetto Arduino Slave Relay... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    digitalWrite(PIN_RESET_ARDUINO_SLAVE_RELAY, LOW);
    delay(10);
    digitalWrite(PIN_RESET_ARDUINO_SLAVE_RELAY, HIGH);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Resetto Arduino Slave Relay... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void resetArduinoSlaveSensori() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Resetto Arduino Slave Sensori... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    digitalWrite(PIN_RESET_ARDUINO_SLAVE_SENSORI, LOW);
    delay(10);
    digitalWrite(PIN_RESET_ARDUINO_SLAVE_SENSORI, HIGH);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Resetto Arduino Slave Sensori... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}