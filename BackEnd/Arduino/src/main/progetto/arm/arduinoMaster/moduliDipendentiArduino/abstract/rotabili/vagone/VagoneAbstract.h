#ifndef DCC_COMMAND_STATION_VAGONE_H
#define DCC_COMMAND_STATION_VAGONE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/Rotabili.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class VagoneAbstract {
    // *** VARIABILI *** //

   private:
    IdVagoneTipo id;
    InfoRotabile infoRotabile;

    LunghezzaRotabileTipo lunghezzaCm;
    bool luciAccese;

    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //
   public:
    VagoneAbstract(IdVagoneTipo id, LoggerAbstract& logger) : logger(logger) { this->id = id; }

    // *** DEFINIZIONE METODI *** //

    void stampoInfo();
    char* toString();

    // *** GETTER *** //

    IdVagoneTipo getId() const;
    char* getNome();
    TipologiaRotabile getTipologia() const;
    LivreaRotabile getLivrea() const;
    ProduttoreRotabile getProduttore() const;
    char* getNumeroModello();
    LunghezzaRotabileTipo getLunghezzaCm() const;
    bool isLuciAccese() const;

    // *** SETTER *** //

    void setId(IdVagoneTipo idNuovo);
    void setNome(char* nomeNuovo);
    void setCodice(char* codiceNuovo);
    void setNumeroModello(char* numeroModelloNuovo);
    void setTipologia(TipologiaRotabile tipologiaNuovo);
    void setCategoria(CategoriaRotabilePasseggeri categoriaNuovo);
    void setLivrea(LivreaRotabile livreaNuovo);
    void setProduttore(ProduttoreRotabile produttoreNuovo);
    void setLunghezzaCm(LunghezzaRotabileTipo lunghezzaCmNuovo);
    void setLuciAccese(bool luciAcceseNuovo);
};

#endif