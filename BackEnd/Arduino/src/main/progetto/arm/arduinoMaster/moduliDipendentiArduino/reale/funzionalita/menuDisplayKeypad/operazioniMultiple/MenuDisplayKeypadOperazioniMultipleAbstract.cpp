// *** INCLUDE *** //

#include "MenuDisplayKeypadOperazioniMultipleAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypadOperazioniMultipleReale =
    MenuDisplayKeypadOperazioniMultipleAbstract(convogliReali, displayReale, loggerReale);