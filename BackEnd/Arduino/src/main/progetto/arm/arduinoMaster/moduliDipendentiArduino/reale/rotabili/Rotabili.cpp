// *** INCLUDE *** //

#include "Rotabili.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

TipologiaRotabile getEnumerazioneTipologiaRotabileDaStringa(char *stringa) {
    if (strcmp(stringa, "MERCI") == 0) {
        return MERCI;
    } else if (strcmp(stringa, "PASSEGGERI") == 0) {
        return PASSEGGERI;
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Enumerazione tipologia rotabile non conosciuta: ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::BUG, stringa,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        programmaReale.ferma(true);
    }
}

void setStringaEnumerazioneTipologiaRotabile(char *stringaSalvataggio, TipologiaRotabile valore) {
    if (valore == MERCI) {
        strcpy(stringaSalvataggio, "MERCI");
    } else if (valore == PASSEGGERI) {
        strcpy(stringaSalvataggio, "PASSEGGERI");
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Valore non mappato nella funzione.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

CategoriaRotabilePasseggeri getEnumerazioneCategoriaRotabileDaStringa(char *stringa) {
    if (strcmp(stringa, "INTERCITY") == 0) {
        return INTERCITY;
    } else if (strcmp(stringa, "FRECCIA") == 0) {
        return FRECCIA;
    } else if (strcmp(stringa, "REGIONALE") == 0) {
        return REGIONALE;
    } else if (strcmp(stringa, "ALTRO") == 0) {
        return ALTRO;
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG,
                              "Enumerazione non conosciuta: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::BUG, stringa,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        programmaReale.ferma(true);
    }
}

void setStringaEnumerazioneCategoriaRotabile(char *stringaSalvataggio, CategoriaRotabilePasseggeri valore) {
    if (valore == INTERCITY) {
        strcpy(stringaSalvataggio, "INTERCITY");
    } else if (valore == FRECCIA) {
        strcpy(stringaSalvataggio, "FRECCIA");
    } else if (valore == REGIONALE) {
        strcpy(stringaSalvataggio, "REGIONALE");
    } else if (valore == ALTRO) {
        strcpy(stringaSalvataggio, "ALTRO");
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Valore non mappato nella funzione!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

LivreaRotabile getEnumerazioneLivreaRotabileDaStringa(char *stringa) {
    if (strcmp(stringa, "XMPR") == 0) {
        return XMPR;
    } else if (strcmp(stringa, "FRECCIA_BIANCA") == 0) {
        return FRECCIA_BIANCA;
    } else if (strcmp(stringa, "GRIGIO") == 0) {
        return GRIGIO;
    } else if (strcmp(stringa, "GRIGIO_ROSSO") == 0) {
        return GRIGIO_ROSSO;
    } else if (strcmp(stringa, "INTERCITY_NOTTE")) {
        return INTERCITY_NOTTE;
    } else if (strcmp(stringa, "AGIP") == 0) {
        return AGIP;
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Valore non mappato nella funzione!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

void setStringaEnumerazioneLivreaRotabile(char *stringaSalvataggio, LivreaRotabile valore) {
    if (valore == XMPR) {
        strcpy(stringaSalvataggio, "XMPR");
    } else if (valore == FRECCIA_BIANCA) {
        strcpy(stringaSalvataggio, "FRECCIA_BIANCA");
    } else if (valore == GRIGIO) {
        strcpy(stringaSalvataggio, "GRIGIO");
    } else if (valore == GRIGIO_ROSSO) {
        strcpy(stringaSalvataggio, "GRIGIO_ROSSO");
    } else if (valore == INTERCITY_NOTTE) {
        strcpy(stringaSalvataggio, "INTERCITY_NOTTE");
    } else if (valore == AGIP) {
        strcpy(stringaSalvataggio, "AGIP");
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Valore non mappato nella funzione!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}

ProduttoreRotabile getEnumerazioneProduttoreRotabileDaStringa(char *stringa) {
    if (strcmp(stringa, "RIVAROSSI") == 0) {
        return RIVAROSSI;
    } else if (strcmp(stringa, "VITRAINS") == 0) {
        return VITRAINS;
    } else if (strcmp(stringa, "JOUEF") == 0) {
        return JOUEF;
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Enumerazione produttore rotabile non conosciuta: ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_DINAMICO(loggerReale, LivelloLog::BUG, stringa,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false, true);
        programmaReale.ferma(true);
    }
}

void setStringaEnumerazioneProduttoreRotabile(char *stringaSalvataggio, ProduttoreRotabile valore) {
    if (valore == RIVAROSSI) {
        strcpy(stringaSalvataggio, "RIVAROSSI");
    } else if (valore == VITRAINS) {
        strcpy(stringaSalvataggio, "VITRAINS");
    } else if (valore == JOUEF) {
        strcpy(stringaSalvataggio, "JOUEF");
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Valore non mappato nella funzione!",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
}