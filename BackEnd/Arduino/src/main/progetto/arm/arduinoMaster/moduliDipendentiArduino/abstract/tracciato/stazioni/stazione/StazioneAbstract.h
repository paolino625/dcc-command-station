#ifndef DCC_COMMAND_STATION_STAZIONEABSTRACT_H
#define DCC_COMMAND_STATION_STAZIONEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezione/SezioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezioni/SezioniAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/stazioni/NomiStazioni.h"

// *** DEFINE *** //

#define NUMERO_MASSIMO_BINARI 15

// *** CLASSE *** //

class StazioneAbstract {
    // *** VARIABILI *** //

   public:
    // Array parte da 1
    // Binari ordinati dal più corto al più lungo
    SezioneAbstract* binari[NUMERO_MASSIMO_BINARI + 1];
    int numeroBinari = 0;

   protected:
    IdStazioneTipo id;  // Serve rintracciare le sezioni appartenenti alla stazione.
    NomeStazioneUnivoco nomeUnivoco;
    bool stazioneDiTesta;

    // Per impostare correttamente la direzione della prima locomotiva dei convogli durante l'inizializzazione dell'autopilot, è necessario determinare se la stazione è in prospettiva con la stazione centrale.
    bool stessaProspettivaStazioneCentrale;

    ProgrammaAbstract& programma;
    SezioniAbstract& sezioni;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    StazioneAbstract(IdStazioneTipo id, NomeStazioneUnivoco nomeStazioneUnivoco, bool stazioneDiTesta, bool stessaProspettivaStazioneCentrale,
                     ProgrammaAbstract& programma, SezioniAbstract& sezioni, LoggerAbstract& logger)
        : id(id),
          nomeUnivoco(nomeStazioneUnivoco),
          stazioneDiTesta(stazioneDiTesta),
          stessaProspettivaStazioneCentrale(stessaProspettivaStazioneCentrale),
          programma(programma),
          sezioni(sezioni),
          logger(logger) {}

    // *** DEFINIZIONE METODI *** //

    void inizializza();
    IdStazioneTipo getId() const { return id; }
    NomeStazioneUnivoco getNomeUnivoco();
    bool isStazioneDiTesta() const;
    bool isStessaProspettivaStazioneCentrale() const;
    void setId(NomeStazioneUnivoco idStazione);
    void setStazioneDiTesta(bool stazioneDiTesta);
    SezioneAbstract* getBinario(int numeroBinario);
    void stampoBinari();

   private:
    void ordinoBinariInBaseAllaLunghezza();
};

#endif