#ifndef DCC_COMMAND_STATION_WIREREALE_H
#define DCC_COMMAND_STATION_WIREREALE_H

// *** INCLUDE *** //

#include "Wire.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/wire/WireAbstract.h"

// *** CLASSE *** //

class WireReale : public WireAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    auto begin() -> void;
    auto beginTransmission(int address) -> void;
    auto write(int data) -> void;
    auto endTransmission() -> int;
};

// *** DICHIARAZIONE VARIABILI *** //

extern WireReale wireReale;

#endif
