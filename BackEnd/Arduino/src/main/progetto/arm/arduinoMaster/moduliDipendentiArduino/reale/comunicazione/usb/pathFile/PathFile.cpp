// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/NumeroCaratteriPathFile.h"

// *** DEFINIZIONE VARIABILI *** //

char pathParzialeFileLocomotiva[NUMERO_CARATTERI_PATH_FILE] = "rotabili/locomotiva";
char pathFileConvogli[NUMERO_CARATTERI_PATH_FILE] = "rotabili/convogli.json";
char pathFileVagoni[NUMERO_CARATTERI_PATH_FILE] = "rotabili/vagoni.json";
char pathFileScambi[NUMERO_CARATTERI_PATH_FILE] = "plastico/scambi.json";
char pathFileUtenti[NUMERO_CARATTERI_PATH_FILE] = "utenti.json";

char pathFileAudioTest1[NUMERO_CARATTERI_PATH_FILE] = "/annunciSonori/1.wav";
char pathFileAudioTest2[NUMERO_CARATTERI_PATH_FILE] = "/annunciSonori/2.wav";
char pathFileAudioTest3[NUMERO_CARATTERI_PATH_FILE] = "/annunciSonori/3.wav";
char pathFileAudioTest4[NUMERO_CARATTERI_PATH_FILE] = "/annunciSonori/4.wav";
char pathFileAudioTest5[NUMERO_CARATTERI_PATH_FILE] = "/annunciSonori/5.wav";