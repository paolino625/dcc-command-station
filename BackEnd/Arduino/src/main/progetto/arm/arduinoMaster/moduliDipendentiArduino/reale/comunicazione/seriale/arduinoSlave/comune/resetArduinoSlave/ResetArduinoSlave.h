#ifndef DCC_COMMAND_STATION_RESETARDUINOSLAVE_H
#define DCC_COMMAND_STATION_RESETARDUINOSLAVE_H

// *** DICHIARAZIONE FUNZIONI *** //

void resetArduinoSlaveRelay();
void resetArduinoSlaveSensori();

#endif
