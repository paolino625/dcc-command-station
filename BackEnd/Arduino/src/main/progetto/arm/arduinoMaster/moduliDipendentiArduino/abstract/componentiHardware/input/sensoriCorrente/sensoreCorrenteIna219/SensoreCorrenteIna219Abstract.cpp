// *** INCLUDE *** //

#include "SensoreCorrenteIna219Abstract.h"

// *** DEFINIZIONE METODI *** //

void SensoreCorrenteIna219Abstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensore corrente INA219... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    if (!sensoreCorrenteIna219Core.begin()) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non trovo il chip INA219.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        display.errori.inizializzazione.ina219();
        programma.ferma(false);
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo sensore corrente INA219... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

float SensoreCorrenteIna219Abstract::leggeCorrente() {
    return sensoreCorrenteIna219Core.getCurrent_mA() *
           3;  // Moltiplico per 3 per via delle resistenze aggiuntive inserite
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente.
char* SensoreCorrenteIna219Abstract::toString() {
    std::string stringa;
    stringa += "Stato sensore corrente INA219: ";
    stringa += std::to_string(statoSensoreCorrenteIna219Campionato);
    stringa += " mA";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

void SensoreCorrenteIna219Abstract::aggiornaCampionamentoCorrente() {
    // Un minimo di campionamento viene già fatto dalla libreria INA219

    bufferCorrente[indiceAttualeBufferStatoSensoreCorrenteIna219] = leggeCorrente();
    indiceAttualeBufferStatoSensoreCorrenteIna219++;
    if (indiceAttualeBufferStatoSensoreCorrenteIna219 == NUMERO_CAMPIONI_SENSORE_CORRENTE_INA219) {
        indiceAttualeBufferStatoSensoreCorrenteIna219 = 0;
    }
    misuraMediaCampioniSensoreCorrenteIna219();
}

void SensoreCorrenteIna219Abstract::effettuaLetturaAggiornaUnaEntryCampionamento() {
    // Il campionamento viene già effettuato all'interno di INA219
}

void SensoreCorrenteIna219Abstract::effettuaLetturaAggiornaTutteEntryCampionamento() {
    // Il campionamento viene già effettuato all'interno di INA219
}

void SensoreCorrenteIna219Abstract::misuraMediaCampioniSensoreCorrenteIna219() {
    CorrenteTipo sommaCampioni = 0;

    for (int i = 0; i < NUMERO_CAMPIONI_SENSORE_CORRENTE_INA219; i++) {
        sommaCampioni += bufferCorrente[i];
    }

    CorrenteTipo valorePesato = (CorrenteTipo)(sommaCampioni / NUMERO_CAMPIONI_SENSORE_CORRENTE_INA219);

    statoSensoreCorrenteIna219Campionato = valorePesato;
}

int SensoreCorrenteIna219Abstract::getIndirizzoI2C() const { return indirizzoI2C; }

CorrenteTipo SensoreCorrenteIna219Abstract::getCorrente() const { return statoSensoreCorrenteIna219Campionato; }

float SensoreCorrenteIna219Abstract::getTensione() { return sensoreCorrenteIna219Core.getBusVoltage_V(); }