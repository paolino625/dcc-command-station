#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA219ABSTRACT_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA219ABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/core/SensoreCorrenteIna219CoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"

// *** DEFINE *** //

// Più il numero di campioni aumenta, più tempo ci mette Arduino Giga per rilevare un eventuale corto circuito e
// interrompere l'alimentazione al tracciato
// Attenzione a incrementare questo numero, potrebbe portare a un consumo eccessivo di memoria
#define NUMERO_CAMPIONI_SENSORE_CORRENTE_INA219 50

// *** CLASSE *** //

class SensoreCorrenteIna219Abstract : public LetturaTensioneAbstract {
    // *** VARIABILI *** //

   private:
    SensoreCorrenteIna219CoreAbstract& sensoreCorrenteIna219Core;
    ProgrammaAbstract& programma;
    int indirizzoI2C;
    DisplayAbstract& display;
    LoggerAbstract& logger;

    CorrenteTipo statoSensoreCorrenteIna219Campionato;
    CorrenteTipo bufferCorrente[NUMERO_CAMPIONI_SENSORE_CORRENTE_INA219];
    int indiceAttualeBufferStatoSensoreCorrenteIna219;

    // *** COSTRUTTORE *** //

   public:
    SensoreCorrenteIna219Abstract(SensoreCorrenteIna219CoreAbstract& sensoreCorrenteIna219Core,
                                  ProgrammaAbstract& programma, int indirizzoI2C, DisplayAbstract& display,
                                  LoggerAbstract& logger)
        : sensoreCorrenteIna219Core{sensoreCorrenteIna219Core},
          programma{programma},
          indirizzoI2C(indirizzoI2C),
          display{display},
          logger{logger} {}

    // *** METODI PUBBLICI *** //

    void inizializza();
    float leggeCorrente();
    char* toString();
    void aggiornaCampionamentoCorrente();
    void effettuaLetturaAggiornaUnaEntryCampionamento();
    void effettuaLetturaAggiornaTutteEntryCampionamento();

    // *** METODI PRIVATI *** //

   private:
    void misuraMediaCampioniSensoreCorrenteIna219();

    // *** GETTER *** //

   public:
    int getIndirizzoI2C() const;
    CorrenteTipo getCorrente() const;
    TensioneTipo getTensione();
};

#endif