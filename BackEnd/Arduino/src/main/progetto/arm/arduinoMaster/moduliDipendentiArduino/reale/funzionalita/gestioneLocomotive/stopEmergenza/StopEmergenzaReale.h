#ifndef ARDUINO_STOPEMERGENZAREALE_H
#define ARDUINO_STOPEMERGENZAREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/stopEmergenza/StopEmergenzaAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern StopEmergenzaAbstract stopEmergenzaReale;

#endif