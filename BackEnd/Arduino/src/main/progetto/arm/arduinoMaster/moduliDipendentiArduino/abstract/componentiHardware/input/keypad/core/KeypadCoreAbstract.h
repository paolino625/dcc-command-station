#ifndef DCC_COMMAND_STATION_KEYPADCOREABSTRACT_H
#define DCC_COMMAND_STATION_KEYPADCOREABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/core/componente/KeypadComponenteAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/tempo/TempoAbstract.h"

// *** CLASSE *** //

class KeypadCoreAbstract {
    KeypadComponenteAbstract &keypadComponente;
    TempoAbstract &tempo;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   public:
    KeypadCoreAbstract(KeypadComponenteAbstract &keypadComponente, TempoAbstract &tempo, LoggerAbstract &logger)
        : keypadComponente(keypadComponente), tempo(tempo), logger(logger) {}

    // *** DEFINIZIONE METODI *** //

    char leggoCarattere();
    char leggoCarattereTempoLimite(double tempoLimiteSecondi);
};

#endif
