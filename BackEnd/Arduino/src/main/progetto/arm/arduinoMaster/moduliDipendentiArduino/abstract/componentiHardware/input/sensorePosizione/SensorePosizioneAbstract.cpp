// *** INCLUDE *** //

#include "SensorePosizioneAbstract.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensorePosizione/letturaSensore/ModalitaLetturaSensore.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"

// *** DEFINIZIONE METODI *** //

void SensorePosizioneAbstract::reset(bool resetIdConvoglioInAttesa) {
    if (stato || idConvoglioInAttesa != 0) {
        stato = false;
        if (resetIdConvoglioInAttesa) {
            idConvoglioInAttesa = 0;
        }
        sensoriPosizioneAggiornamentoMqttRichiesto[id] = true;
    }
}

void SensorePosizioneAbstract::resetInAttesaPassaggioPrimaLocomotivaConvoglio() {
    inAttesaPassaggioPrimaLocomotivaConvoglio = true;
}

IdSensorePosizioneTipo SensorePosizioneAbstract::getId() const { return id; }

// GetStato ritorna lo stato del sensore ma senza resettare il suo stato
bool SensorePosizioneAbstract::getStato() const { return stato; }

IdConvoglioTipo SensorePosizioneAbstract::getIdConvoglioInAttesa() const { return idConvoglioInAttesa; }

// Ho chiamato questa funzione fetchStato e non getStato per rendere più chiaro il fatto che il valore dello stato del
// sensore viene resettato dopo che viene letto
bool SensorePosizioneAbstract::fetchStato() {
    bool statoDaRestituire = stato;

    // Se il sensore è stato azionato, devo resettare anche l'id del convoglio in attesa, altrimenti no.
    if(statoDaRestituire){
        reset(true);
    } else{
        reset(false);
    }
    return statoDaRestituire;
}

void SensorePosizioneAbstract::setStato(bool nuovoStato) {
    // Se il nuovo stato è a false lo setto direttamente
    if (!nuovoStato) {
        this->stato = nuovoStato;
        sensoriPosizioneAggiornamentoMqttRichiesto[id] = true;
    }
    // Se il nuovo stato è a true
    else {
        if (modalitaLetturaSensore == ModalitaLetturaSensore::LETTURA_CONVOGLIO) {
            if (inAttesaPassaggioPrimaLocomotivaConvoglio) {
                inAttesaPassaggioPrimaLocomotivaConvoglio = false;

                if (DEBUGGING_LOGICA_LETTURA_SENSORE) {
                    LOG_MESSAGGIO_STATICO(
                        logger, LivelloLog::DEBUG_,
                        "Il sensore è stato azionato dalla prima locomotiva del convoglio. Considero questa lettura.",
                        InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }

                // Imposto lo stato a true in modo tale che la prossima volta che la prossima volta che viene chiamata
                // la funzione fetchStato, ritorni true
                stato = true;

                // Devo inviare lo stato aggiornato su MQTT
                mqttArduinoMaster.inviaStatoSensorePosizione(id);
            } else {
                if (DEBUGGING_LOGICA_LETTURA_SENSORE) {
                    LOG_MESSAGGIO_STATICO(
                        logger, LivelloLog::DEBUG_,
                        "Il sensore è stato azionato dalla seconda locomotiva del convoglio. Ignoro questa lettura.",
                        InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }

                inAttesaPassaggioPrimaLocomotivaConvoglio = true;

                // Non aspettiamo più nessun convoglio su questo sensore
                idConvoglioInAttesa = 0;

                // Anche se non considero questa lettura sensore come valida, devo inviare lo stato aggiornato su MQTT a
                // true, in modo tale che il Front-End possa far illuminare il sensore
                stato = true;
                mqttArduinoMaster.inviaStatoSensorePosizione(id);

                // Devo ignorare questa lettura, non voglio che venga letta da "fetchStato"
                stato = false;

                // Devo reinviare lo stato aggiornato su MQTT.
                mqttArduinoMaster.inviaStatoSensorePosizione(id);

                // Dobbiamo però comunque inviare il messaggio MQTT per aggiornare lo stato del sensore, prima a true, e
                // poi a false
            }
        } else if (modalitaLetturaSensore == ModalitaLetturaSensore::LETTURA_LOCOMOTIVA) {
            // Non effettuo nessun tipo di logica
            stato = true;
        }
    }
}

void SensorePosizioneAbstract::setIdConvoglioInAttesa(IdConvoglioTipo idConvoglio) {
    if (idConvoglio != idConvoglioInAttesa) {
        this->idConvoglioInAttesa = idConvoglio;
        sensoriPosizioneAggiornamentoMqttRichiesto[id] = true;
    }
}