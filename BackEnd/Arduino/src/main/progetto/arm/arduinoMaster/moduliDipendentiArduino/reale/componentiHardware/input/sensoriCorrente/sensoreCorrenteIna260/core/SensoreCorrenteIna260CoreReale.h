#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA260COREREALE_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA260COREREALE_H

// *** INCLUDE *** //

#include "Adafruit_INA260.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/core/SensoreCorrenteIna260CoreAbstract.h"

// *** CLASSE *** //

class SensoreCorrenteIna260CoreReale : public SensoreCorrenteIna260CoreAbstract {
    // *** VARIABILI *** //

    Adafruit_INA260 ina260;

    // *** DICHIARAZIONE METODI *** //

   public:
    bool begin(int indirizzoI2c);
    float readBusVoltage();
    float readCurrent();
};

// *** DICHIARAZIONE VARIABILI *** //

extern SensoreCorrenteIna260CoreReale sensoreCorrenteIna260CoreReale;

#endif
