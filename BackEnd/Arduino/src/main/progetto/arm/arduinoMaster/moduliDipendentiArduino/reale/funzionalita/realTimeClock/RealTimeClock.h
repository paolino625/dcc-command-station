// *** INCLUDE *** //

#include <WiFi.h>
#include <WiFiUdp.h>

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** DEFINE *** //

// Libreria che converte automaticamente orario UTC in orario tempo locale non compatible con Arduino Giga.
// Inserisco manualmente l'offset fino a quando tale libreria non diventa compatibile con Arduino Giga.

#define OFFSET_UTC_LOCALTIME 1

// *** DICHIARAZIONE VARIABILI *** //

constexpr auto timeServer{"pool.ntp.org"};

extern unsigned int localPort;  // Porta locale per ascoltare pacchetti UDB

extern const int ntpPacketSize;  // Il timestamp NTP sono i primi 48 byte del messaggio

extern byte packetBuffer[];  // Buffer per memorizzare pacchetti in entrata e in uscita

extern WiFiUDP wifiUdp;  // Un'istanza UDP ci permette di inviare e ricevere pacchetti UDP

// *** CLASSE *** //

class RealTimeClock {
    // *** DICHIARAZIONE METODI *** //
   public:
    void inizializza();
    static String getDataEOra();
    static String getData();

   protected:
    void invioPacchettoNtp(const char *address);
    unsigned long leggoPacchettoNtp();
    void leggoTempoNtp();
};

// *** DICHIARAZIONE VARIABILI *** //

extern RealTimeClock realTimeClock;