#ifndef DCC_COMMAND_STATION_SENSOREIMPRONTEREALE_H
#define DCC_COMMAND_STATION_SENSOREIMPRONTEREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreImpronte/SensoreImpronteAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SensoreImpronteAbstract sensoreImpronteReale;

#endif
