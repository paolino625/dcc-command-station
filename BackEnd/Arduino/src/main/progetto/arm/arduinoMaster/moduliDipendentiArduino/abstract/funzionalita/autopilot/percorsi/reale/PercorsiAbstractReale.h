#ifndef DCC_COMMAND_STATION_PERCORSIABSTRACTREALE_H
#define DCC_COMMAND_STATION_PERCORSIABSTRACTREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/arduinoMasterHelper/ArduinoMasterHelperAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/PercorsiAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class PercorsiAbstractReale : public PercorsiAbstract {
    // *** DICHIARAZIONE VARIABILI *** //

    ArduinoMasterHelperAbstract& retrievePercorsoHelper;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    PercorsiAbstractReale(ArduinoMasterHelperAbstract& retrievePercorsoHelper, LoggerAbstract& logger)
        : retrievePercorsoHelper(retrievePercorsoHelper), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo) override;
    bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo,
                        bool direzionePrimaLocomotivaDestra) override;
    PercorsoAbstract* getPercorso(IdSezioneTipo idSezionePartenzaRequest,
                                  IdSezioneTipo idSezioneArrivoRequest) override;
};

#endif
