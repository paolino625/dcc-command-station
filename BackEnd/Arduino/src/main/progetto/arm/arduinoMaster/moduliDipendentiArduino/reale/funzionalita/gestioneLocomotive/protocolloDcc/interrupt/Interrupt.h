/****************************************************************************************************************************
  TimerInterruptTest.ino
  For Portenta_H7 boards
  Written by Khoi Hoang

  Built by Khoi Hoang https://github.com/khoih-prog/Portenta_H7_TimerInterrupt
  Licensed under MIT license

  Now even you use all these new 16 ISR-based timers,with their maximum interval practically unlimited (limited only by
  unsigned long miliseconds), you just consume only one Portenta_H7 STM32 timer and avoid conflicting with other cores'
tasks. The accuracy is nearly perfect compared to software timers. The most important feature is they're ISR-based
timers Therefore, their executions are not blocked by bad-behaving functions / tasks. This important feature is
absolutely necessary for mission-critical tasks.
*****************************************************************************************************************************/
/*
   Notes:
   Special design is necessary to share data between interrupt code and the rest of your program.
   Variables usually need to be "volatile" types. Volatile tells the compiler to avoid optimizations that assume
   variable can not spontaneously change. Because your function may change variables while your program is using them,
   the compiler needs this hint. But volatile alone is often not enough.
   When accessing shared variables, usually interrupts must be disabled. Even with volatile,
   if the interrupt changes a multibyte variable between a sequence of instructions, it can be read incorrectly.
   If your data is multiple variables, such as an array and a count, usually interrupts need to be disabled
   or the entire sequence of your code which accesses the data.
*/

#ifndef DCC_COMMAND_STATION_INTERRUPT_H
#define DCC_COMMAND_STATION_INTERRUPT_H

// *** DEFINE *** //

// These define must be placed at the beginning before #include "Portenta_H7_TimerInterrupt.h"
// _TIMERINTERRUPT_LOGLEVEL_ from 0 to 4
// Don't define _TIMERINTERRUPT_LOGLEVEL_ > 0. Only for special ISR debugging only. Can hang the system.
#define _TIMERINTERRUPT_LOGLEVEL_ 4

// *** INCLUDE *** //

// Can be included as many times as necessary, without `Multiple Definitions` Linker Error
#include "Portenta_H7_TimerInterrupt.h"

// In Portenta_H7, avoid doing something fancy in ISR, for example Serial.print
// Or you can get this run-time error / crash

#define TIMER0_INTERVAL_MS 1000
#define TIMER0_DURATION_MS 10000

#define TIMER1_INTERVAL_MS 3000
#define TIMER1_DURATION_MS 30000

// *** DICHIARAZIONE VARIABILI *** //

// Init timer TIM15
extern Portenta_H7_Timer ITimer0;
// Init  timer TIM16
extern Portenta_H7_Timer ITimer1;

// *** DICHIARAZIONE FUNZIONI *** //

void callbackTimerInterrupt1();
void inizializzoInterruptTimer();

#endif
