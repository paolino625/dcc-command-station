// *** INCLUDE *** //

#include "ConvogliAbstractReali.h"

// *** DEFINIZIONE METODI *** //

void ConvogliAbstractReali::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo convogli... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale* convoglio =
            new ConvoglioAbstractReale(locomotive, vagoni, i, programma, percorsi, tempo, sensoriPosizione, sezioni,
                                       stazioni, distanzaFrenataLocomotiva, logger);
        convogli[i] = convoglio;
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

bool ConvogliAbstractReali::esisteConvoglio(IdConvoglioTipo idConvoglio) {
    if (idConvoglio == 0) {
        return false;
    } else {
        for (int i = 1; i < NUMERO_CONVOGLI; i++) {
            if (convogli[i]->getId() == idConvoglio) {
                return true;
            }
        }
    }

    return false;
}

void ConvogliAbstractReali::reset() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo convogli... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        convogli[i]->reset();
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

bool ConvogliAbstractReali::isLocomotivaInUso(IdLocomotivaTipo idLocomotiva) {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        if (convogli[i]->getIdLocomotiva1() == idLocomotiva || convogli[i]->getIdLocomotiva2() == idLocomotiva) {
            return true;
        }
    }
    return false;
}

bool ConvogliAbstractReali::isVagoneInUso(VagoneAbstract* vagone) {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale* convoglio = getConvoglio(i, false);
        for (int r = 1; r < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; r++) {
            if (convoglio->getIdVagone(r) == vagone->getId()) {
                return true;
            }
        }
    }

    return false;
}

void ConvogliAbstractReali::aggiornaLunghezza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno lunghezza convogli... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        if (convogli[i]->isInUso()) {
            ConvoglioAbstractReale* convoglio = getConvoglio(i, false);
            convoglio->aggiornaLunghezza();
        }
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno lunghezza convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void ConvogliAbstractReali::aggiornaLuci() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno luci convogli...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        if (convogli[i]->isInUso()) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(i).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " in uso: aggiorno luci.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            ConvoglioAbstractReale* convoglio = getConvoglio(i, false);
            convoglio->aggiornaLuci();
        }
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno luci convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void ConvogliAbstractReali::aggiornaDirezioneLocomotive() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno direzione locomotive convogli...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        if (convogli[i]->isInUso()) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Convoglio n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(i).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, " in uso: aggiorno direzione locomotive.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            ConvoglioAbstractReale* convoglio = getConvoglio(i, false);

            if (convoglio->hasDoppiaLocomotiva()) {
                LocomotivaAbstract* locomotiva1 = locomotive.getLocomotiva(convoglio->getIdLocomotiva1(), false);
                LocomotivaAbstract* locomotiva2 = locomotive.getLocomotiva(convoglio->getIdLocomotiva2(), false);

                // Se il convoglio è composto da due locomotive, devo assicurarmi che la direzione della seconda
                // locomotiva sia opposta alla prima.
                locomotiva2->setDirezioneAvantiDisplay(!locomotiva1->getDirezioneAvantiDisplay());
            }
        }
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno direzione locomotive convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
};

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente con delete[].
char* ConvogliAbstractReali::toString() {
    std::string stringa;

    stringa += "\n";
    stringa += "STATO CONVOGLI:\n";
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale* convoglio = getConvoglio(i, false);
        char* stringaConvoglio = convoglio->toString();
        stringa += stringaConvoglio;
        delete[] stringaConvoglio;
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

ConvoglioAbstractReale* ConvogliAbstractReali::getConvoglio(int id, bool fermaProgramma) {
    if (esisteConvoglio(id)) {
        return convogli[id];
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Il convoglio con id ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(id).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, " non esiste",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        if (fermaProgramma) {
            programma.ferma(true);
        }
    }
    return convogli[id];
}

ConvoglioAbstractReale* ConvogliAbstractReali::getConvoglioDellaLocomotiva(IdLocomotivaTipo idLocomotiva) {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale* convoglio = getConvoglio(i, false);
        if (convogli[i]->getLocomotiva1()->getId() == idLocomotiva ||
            convogli[i]->getLocomotiva2()->getId() == idLocomotiva) {
            return convoglio;
        }
    }

    return nullptr;
}
void ConvogliAbstractReali::bloccoSezioniOccupate() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Blocco le sezioni occupate dai convogli...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale* convoglio = getConvoglio(i, true);

        // Se la posizione del convoglio è da ritenersi valida allora la blocco
        if (convoglio->autopilotConvoglio->posizioneConvoglio.validita ==
            ValiditaPosizioneConvoglio::POSIZIONE_VALIDA) {
            IdSezioneTipo idSezioneOccupataDalConvoglio =
                convoglio->autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata;

            sezioni.lock(idSezioneOccupataDalConvoglio, convoglio->getId());
        }
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Blocco le sezioni occupate dai convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}
