#ifndef DCC_COMMAND_STATION_INVIOSTATOTRACCIATOREALE_H
#define DCC_COMMAND_STATION_INVIOSTATOTRACCIATOREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern InvioStatoTracciatoAbstract invioStatoTracciatoReale;

#endif
