#ifndef DCC_COMMAND_STATION_PROTOCOLLODCCCOREREALE_H
#define DCC_COMMAND_STATION_PROTOCOLLODCCCOREREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class ProtocolloDccCoreReale final : public ProtocolloDccCoreAbstract {
    // *** COSTRUTTORE *** //
   public:
    ProtocolloDccCoreReale(LoggerAbstract &logger) : ProtocolloDccCoreAbstract(logger) {}

    // *** DICHIARAZIONE FUNZIONI *** //
   public:
    void invioPacchetto(volatile Pacchetto pacchetto) override;
    void segnaleDcc() override;
    void scrivoIndirizzoCv(int indirizzoCv, byte datoDaScrivere) override;
};

// *** DICHIARAZIONE VARIABILI *** //

extern ProtocolloDccCoreReale protocolloDccCoreReale;

#endif