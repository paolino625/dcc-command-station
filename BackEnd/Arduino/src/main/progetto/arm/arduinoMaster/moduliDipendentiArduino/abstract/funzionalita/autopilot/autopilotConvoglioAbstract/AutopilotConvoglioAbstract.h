/*
L'autopilot è stato progettato come una macchina a stati finiti per ogni convoglio.

L'algoritmo richiede le seguenti informazioni:

- Conoscenza della lunghezza di ogni rotabile (locomotiva o vagone)
- Conoscenza della composizione del convoglio (locomotiva e vagoniReali in uso)
- Conoscenza della velocità reale di ogni locomotiva (vedi capitolo relativo alla mappatura della loco)
- Quando un convoglio viene posizionato sul tracciato, l'utente inserisce la sezione occupata e la direzione del
convoglio
- Sul tracciato è possibile installare un numero variabile di sensori, senza la necessità di averne uno per ogni
sezione. L'accuratezza del sistema aumenta con l'incremento del numero di sensori installati. Questo riduce l'errore tra
la posizione desiderata di arresto del convoglio e la posizione in cui si ferma effettivamente.
*/

/*
Prevedere 3 use case:
- L'utente sceglie la modalità autopilot totalmente casuale in cui non vengono impostati i percorsi dei convogli ma
questi vengono decisi run-time. Creare stato FERMO_STAZIONE_SCELTA_STAZIONE_DESTINAZIONE dopo
FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA, in cui, solo se la modalità autopilot è random, viene scelto a caso un binario
libero di una delle stazioni. Viene creata un'altra funzione inizializzoModalitaRandom() che non accetta in input
stazioni ecc. Nello stato ARRIVATO_STAZIONE_DESTINAZIONE, viene controllata la modalità autopilot, se è random allora
viene riportato nello stato FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA.
- L'utente sceglie solo la stazione successiva. Quindi creare una funzione inizializzoModalitaStazioneSuccessiva() che
accetta solo il binario di una stazione successiva Nello stato ARRIVATO_STAZIONE_DESTINAZIONE, viene controllata la
modalità autopilot, se è questa qui, allora si passa allo stato PRONTO_PER_CONFIGURAZIONE_AUTOPILOT
- L'utente sceglie un percorso a loop in cui i binari di stazione devono essere bloccati all'inizio. Creare uno stato
dopo PRONTO_PER_CONFIGURAZIONE_AUTOPILOT. Non si può aspettare il momento di partenza per bloccare perché così i binari
di stazione non possono essere bloccati da nessun altro convoglio.
- Funzione timetable. Funzione inizializzoModalitaTimetable() che accetta un riferimento ad una lista di binari di
stazione da attraversare e una lista degli orari da rispettare per la partenza, uno per ogni stazione.
 */

#ifndef DCC_COMMAND_STATION_AUTOPILOTCONVOGLIO_H
#define DCC_COMMAND_STATION_AUTOPILOTCONVOGLIO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriPosizione/SensoriPosizioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/enumerazioni/statoAutopilot/statoAutopilotModalitaApproccioCasuale/StatoAutopilotModalitaApproccioCasuale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/enumerazioni/statoAutopilot/statoAutopilotModalitaStazioneSuccessiva/StatoAutopilotModalitaStazioneSuccessiva.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/enumerazioni/statoInizializzazioneAutopilotConvoglio/StatoInizializzazioneAutopilotConvoglio.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/struct/PosizioneConvoglio.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/PercorsiAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/distanzaFrenata/DistanzaFrenataLocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/ConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/stazioni/stazioni/StazioniAbstract.h"

// Devo eseguire le macro FRIEND_TEST solo se il codice viene compilato per i test e non per Arduino. Altrimenti la
// build si spaccherebbe considerando che Google Test non esiste per Arduino.
#if !ARDUINO
#include <gtest/gtest_prod.h>
#endif

// *** DEFINE *** //

#define VELOCITA_USCITA_LOCOMOTIVA_STAZIONE_FINO_A_PRIMO_SENSORE 20

/*
Si noti che, superata una certa velocità, la locomotiva può perdere pacchetti DCC a causa di un contatto non
continuo, rendendo difficile la decodifica. Sebbene lo stato venga sempre aggiornato, la locomotiva potrebbe ricevere
i pacchetti di velocità con ritardo rispetto a quanto previsto dall'Autopilot. Questo rende le misurazioni
dell'autopilot meno precise del solito.
*/

/*
È possibile impostare la velocità massima per un convoglio con modalità autopilot attiva.
Si noti che, superata una certa velocità, il decoder della locomotiva perde i pacchetti DCC in maniera più frequente,
questo a causa di un contatto tra le ruote e le rotaie non sempre continuo. Dunque, sebbene lo stato di tutte le
locomotive venga inviato continuamente sul tracciato, la locomotiva potrebbe ricevere i pacchetti di velocità più tardi
da quanto viene previsto dall'Autopilot. La conseguenza di tutto ciò è che le misurazioni effettuate dall'autopilot
potrebbero essere meno precise del solito.
*/
#define VELOCITA_MASSIMA_AUTOPILOT_KM_H 50

/*
Corrispettivo della variabile INTERVALLO_AGGIORNAMENTO_VELOCITA_ATTUALE_CONVOGLIO_GUIDA_MANUALE ma per l'Autopilot.
Si potrebbe volere un intervallo di aggiornamento differente per l'autopilot rispetto alla modalità di guida manuale.
Magari nella guida manuale si vogliono avere dei convogli un po' più reattivi rispetto all'input forniti.
*/
#define INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT 1000

#define INTERVALLO_AGGIORNAMENTO_DISTANZA_PERCORSA_CONVOGLIO_AUTOPILOT 1000

/*
Ogni quanto aggiorno la distanza percorsa dal convoglio.
È importante che INTERVALLO_AGGIORNAMENTO_DISTANZA_PERCORSA_CONVOGLIO_AUTOPILOT sia un divisore di
INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT. Altrimenti, in questo intervallo di tempo il convoglio
potrebbe essere andato a due differenti velocità, andando a introdurre dell'errore nel calcolo.
*/
static_assert(INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT %
                      INTERVALLO_AGGIORNAMENTO_DISTANZA_PERCORSA_CONVOGLIO_AUTOPILOT ==
                  0,
              "INTERVALLO_AGGIORNAMENTO_DISTANZA_PERCORSA_CONVOGLIO_AUTOPILOT must be a divisor of "
              "INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT");

/*
Cercare di tenere questa distanza più piccola possibile, in modo tale da poter sfruttare la lunghezza delle sezioni
di stazione a pieno.
*/
#define DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA 15

#define DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA 15

/*
Dopo la sezione intermedia è molto probabile che ci sia uno scambio. È necessario che il convoglio si fermi un po'
prima della fine della sezione, altrimenti si potrebbe scontrare con un altro convoglio che sta passando sullo
scambio.
*/
#define DISTANZA_CM_MINIMA_DA_FINE_SEZIONE_INTERMEDIA 5

// Corrisponde alla lunghezza del paraurti.
#define DISTANZA_CM_MINIMA_DA_FINE_BINARIO_STAZIONE_DI_TESTA 5

// Assunzione di quanto possa sballare la posizione finale del convoglio rispetto a quella desiderata in un binario di
// stazione.
#define ERRORE_DISTANZA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_STAZIONE 5

/*
Per le sezioni intermedie il discorso è più complicato. Ci sono alcune sezioni intermedie che sono molto lunghe.
Se una sezione è più lunga di 1.5 m vogliamo che all'errore dell'autopilot base si consideri 1 cm in più per ogni 10
cm in più di lunghezza.
Questa variabile definisce l'errore di base che viene sempre conteggiato, anche per sezioni poco lunghe.
*/
#define ERRORE_DISTANZA_MINIMA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_INTERMEDIA 5

static_assert(DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA -
                      ERRORE_DISTANZA_MINIMA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_INTERMEDIA >=
                  DISTANZA_CM_MINIMA_DA_FINE_SEZIONE_INTERMEDIA,
              "È necessario che nel caso in cui il convoglio abbia accumulato l'errore massimo di posizione, comunque "
              "non si fermi oltre alla distanza minima richiesta dalla sezione intermedia.");

static_assert(DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA -
                      ERRORE_DISTANZA_CM_POSIZIONE_CONVOGLIO_AUTOPILOT_VS_REALE_SEZIONE_STAZIONE >=
                  DISTANZA_CM_MINIMA_DA_FINE_BINARIO_STAZIONE_DI_TESTA,
              "È necessario che nel caso in cui il convoglio abbia accumulato l'errore massimo di posizione, comunque "
              "non si fermi oltre alla distanza minima richiesta dal binario di stazione.");

/* Decido da che lunghezza in poi delle sezioni intermedie è necessario tenere conto di errore di posizionamento
aggiuntivo rispetto a quello basilare. Per le sezioni con una lunghezza superiore a questa, verrà considerata
l'aggiunta di errore di posizionamento in base alla lunghezza della sezione.
*/
#define SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE 150

#define DISTANZA_CM_ERRORE_POSIZIONAMENTO_AGGIUNTIVO_PER_OGNI_10_CM_SEZIONE_INTERMEDIA 1

#define INTERVALLO_TENTATIVO_LOCK_SEZIONI 1000

#define LUNGHEZZA_TOTALE_SEZIONI_BLOCCABILI_CM 300

#define NUMERO_TENTATIVI_LOCK_BINARIO_SCELTO_CASUALMENTE 10

// *** ENUMERAZIONI *** //

enum RisultatoInMovimento {
    CONVOGLIO_ANCORA_IN_MOVIMENTO,
    CONVOGLIO_FERMATO_BINARIO_STAZIONE,
    CONVOGLIO_FERMATO_SEZIONE_INTERMEDIA
};

enum TipologiaUltimoSensoreAzionato { SEZIONE_INTERMEDIA, STAZIONE_SENSORE_1, STAZIONE_SENSORE_2 };

// *** STRUCT *** //

struct ArraySezioni {
    IdSezioneTipo *arraySezioni;
    int numeroElementiArray;

    bool operator==(const ArraySezioni &altro) const {
        if (numeroElementiArray != altro.numeroElementiArray) {
            return false;
        }

        for (int i = 0; i < numeroElementiArray; ++i) {
            if (!(arraySezioni[i].equals(altro.arraySezioni[i]))) {
                return false;
            }
        }

        return true;
    }
};

// *** CLASSE *** //

class AutopilotConvoglioAbstract {
    // Seguente codice da eseguire solo se il codice viene compilato per i test.
#if !ARDUINO
    // Dichiarando la classe AutopilotConvoglioTest come friend, le funzioni al suo interno possono accedere a membri
    // privati di questa classe
    friend class AutopilotConvoglioTest;

    // I test marcati con FRIEND_TEST possono accedere a membri privati di questa classe
    FRIEND_TEST(AutopilotConvoglioTest, esistePercorso);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaEsiste_SezioniPrecedentiAQuellaIdoneaNonLungheAbbastanza);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaEsiste_SezioniPrecedentiAQuellaIdoneaCritiche);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaNonEsiste_SezioniCritiche);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaNonEsiste_SezioniTroppoCorte);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva);
    FRIEND_TEST(AutopilotConvoglioTest, lockSezioni_UnLockOccupato);
    FRIEND_TEST(AutopilotConvoglioTest, lockSezioni_TuttiLockLiberi);
    FRIEND_TEST(AutopilotConvoglioTest, fermoStazioneAttesaMomentoPartenza);
    FRIEND_TEST(AutopilotConvoglioTest, fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva);
    FRIEND_TEST(AutopilotConvoglioTest, fermoStazioneCalcoloDirezioneConvoglio_NoCambioDirezione);
    FRIEND_TEST(AutopilotConvoglioTest, fermoStazioneCalcoloDirezioneConvoglio_CambioDirezione);
    FRIEND_TEST(AutopilotConvoglioTest, fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente);
    FRIEND_TEST(AutopilotConvoglioTest, inPartenzaStazione);
    FRIEND_TEST(AutopilotConvoglioTest, inAttesaPrimoSensore_SensoreAzionato);
    FRIEND_TEST(AutopilotConvoglioTest, inAttesaPrimoSensore_SensoreNonAzionato);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreSezioneIntermedia_SensoreNonAzionato);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreSezioneIntermedia_SensoreAzionato);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneDiTesta_SensoreNonAzionato);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneDiTesta_SensoreAzionato);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneSimmetrica_PrimoSensoreAzionato);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneSimmetrica_SecondoSensoreAzionato);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_aggiornoDistanzaProssimoStop_DaStazionePartenza_LockFinoASezioneIntermediaLunghezzaNormale);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        inMovimento_aggiornoDistanzaProssimoStop_DaStazionePartenza_LockFinoASezioneIntermediaLunghezzaOltreLaSoglia);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_aggiornoDistanzaProssimoStop_DaStazionePartenza_LockTutteSezioniIntermedie);
    FRIEND_TEST(AutopilotConvoglioTest, inMovimento_aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop);
    FRIEND_TEST(AutopilotConvoglioTest, inMovimento_lockSezioniFuturo);
    FRIEND_TEST(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinario1);
    FRIEND_TEST(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinario2);
    FRIEND_TEST(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinario3);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinarioStazioneDestinazione);
    FRIEND_TEST(AutopilotConvoglioTest, fermoSezioneAttesaLockSezioni_SezioniSuccessiveLibere);
    FRIEND_TEST(AutopilotConvoglioTest, fermoSezioneAttesaLockSezioni_SezioniSuccessiveOccupate);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_primaStazioneScelta_BinarioPiuCortoLibero);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_primaStazioneSceltaCasualmente_BinarioPiuLungoUnicoLibero);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_primaStazioneSceltaCasualmente_NessunBinarioLibero);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_ultimaStazioneSceltaCasualmente_BinarioPiuCortoLibero);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_NoOttimizzazioneAssegnazioneConvogli_primaStazioneScelta_BinarioLibero);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_NoOttimizzazioneAssegnazioneConvogli_primaStazioneScelta_UnSoloBinarioLibero);
    FRIEND_TEST(AutopilotConvoglioTest, inizializzazioneAutopilotConvoglio_AttesaInizializzazioneConvoglio);
    FRIEND_TEST(AutopilotConvoglioTest, inizializzazioneAutopilotConvoglio_ProntoPerConfigurazioneAutopilot);
    FRIEND_TEST(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaStazione);
    FRIEND_TEST(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaStazione_sezioneGiaOccupata);
    FRIEND_TEST(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaNonEsiste);
    FRIEND_TEST(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaNonDiStazione);
    FRIEND_TEST(AutopilotConvoglioTest, modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Ok);
    FRIEND_TEST(AutopilotConvoglioTest,
                modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_NonProntoPerConfigurazioneAutopilot);
    FRIEND_TEST(AutopilotConvoglioTest,
                modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_SezioneNonValida);
    FRIEND_TEST(AutopilotConvoglioTest,
                modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_SezioneNonDiStazione);
    FRIEND_TEST(AutopilotConvoglioTest,
                modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_SezioneGiaBloccata);
    FRIEND_TEST(AutopilotConvoglioTest,
                modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_PercorsoNonEsiste);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_BinarioStazioneSuccessivaNonLungoASufficienzaPerConvoglio);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_aggiornoDistanzaProssimoStop_InArrivoStazioneDestinazione_StazioneDiTesta);
    FRIEND_TEST(AutopilotConvoglioTest,
                inMovimento_aggiornoDistanzaProssimoStop_InArrivoStazioneDestinazione_StazioneSimmetrica_PrimoSensore);
    FRIEND_TEST(
        AutopilotConvoglioTest,
        inMovimento_aggiornoDistanzaProssimoStop_InArrivoStazioneDestinazione_StazioneSimmetrica_SecondoSensore);

#endif

    // *** VARIABILI *** //

   public:
    ConvoglioAbstract &convoglio;
    PosizioneConvoglio posizioneConvoglio;
    SensoriPosizioneAbstract &sensoriPosizione;
    SezioniAbstract &sezioni;
    StazioniAbstract &stazioni;
    TempoAbstract &tempo;
    LocomotiveAbstract &locomotive;
    PercorsiAbstract &percorsi;
    ProgrammaAbstract &programma;
    DistanzaFrenataLocomotivaAbstract &distanzaFrenataLocomotiva;
    LoggerAbstract &logger;

    // Variabile che potrebbe sembrare superflua ma utile durante i test per poter settare la lunghezza desiderata
    // durante i vari test.
    LunghezzaTracciatoTipo lunghezzaTotaleSezioniBloccabili = LUNGHEZZA_TOTALE_SEZIONI_BLOCCABILI_CM;

    StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;

    // Devo mantenere lo stato per tutte le modalità autopilot
    StatoAutopilotModalitaStazioneSuccessiva statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA;
    StatoAutopilotModalitaApproccioCasuale statoAutopilotModalitaApproccioCasuale =
        StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA;

    // Devo inizializzare a nullptr perché poi controllo se è diverso da nullptr per deallocare la memoria
    IdSezioneTipo *percorsoConvoglioBinariStazioni = nullptr;
    int numeroStazioniPercorsoConvoglioArray;
    int indiceBinarioStazionePercorsoConvoglio = 0;  // Inizializzato a 0;

    ArraySezioni sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva;
    /*
    Mi serve tenere traccia dell'ultima sezione intermedia che ho bloccato.
    Questo indice si riferisce a sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.
    L'array parte da 0 ma impostiamo il valore di questo indice a -1.
    In questo modo riusciamo a capire se non è mai stato bloccata nessuna sezione intermedia (-1) oppure se è stata
    bloccata la prima (0)
     */
    int indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva = -1;

    TimestampTipo timestampUltimoAggiornamentoAccelerazioneDecelerazioneConvoglioAutopilot = 0;

    TimestampTipo timestampUltimoAggiornamentoDistanzaPercorsaConvoglioAutopilot = 0;

    /*
    Mi serve tenere traccia dell'ultima sezione intermedia che ho sbloccato.
    Questo indice si riferisce a sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.
    È necessario durante l'unlock delle sezioni: devo sapere quale sezioni sono da sbloccare, altrimenti rischio di
    sbloccare una sezione già sbloccata dal convoglio corrente e ribloccata da un altro convoglio.
    Indice parte da -1.
    - -2 significa che il binario di stazione di partenza non è stato sbloccato
    - -1 significa che il binario di stazione di partenza è stato sbloccato
    - 0 significa che la prima sezione delle sezioni intermedie è stata sbloccata ecc.
    */
    int indiceUltimaSezioneSbloccataSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva = -2;

    int indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = -1;
    // Devo tenere traccia se l'ultimo sensore azionato è stato quello di una sezione intermedia o di una stazione
    TipologiaUltimoSensoreAzionato tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA;

    TimestampTipo delayAttesaPartenzaStazione;
    TimestampTipo millisInizioAttesaPartenzaStazione;

    TimestampTipo ultimoTentativoLock;

    // Tengo traccia dei cm percorsi dall'ultimo sensore dell'ultima sezione attraversata dal convoglio.
    // Resetto questa variabile al passaggio di ogni sensore, anche quelle sezioni che non sono lunghe a sufficienza per
    // ospitare il convoglio
    LunghezzaTracciatoTipo distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm;

    // Nel in cui la modalità dell'autopilot sia casuale, memorizziamo se l'utente ha richiesto di essere efficienti
    // nell'assegnazione dei convogli ai binari o meno
    bool ottimizzazioneAssegnazioneConvogliAiBinariModalitaAutopilotCasuale = false;

    // *** COSTRUTTORE *** //

   public:
    AutopilotConvoglioAbstract(ConvoglioAbstract &convoglio, SensoriPosizioneAbstract &sensoriPosizione,
                               SezioniAbstract &sezioni, StazioniAbstract &stazioni, TempoAbstract &tempo,
                               LocomotiveAbstract &locomotive, PercorsiAbstract &percorsi, ProgrammaAbstract &programma,
                               DistanzaFrenataLocomotivaAbstract &distanzaFrenataLocomotiva, LoggerAbstract &logger)
        : convoglio(convoglio),
          sensoriPosizione(sensoriPosizione),
          sezioni(sezioni),
          stazioni(stazioni),
          tempo(tempo),
          locomotive(locomotive),
          percorsi(percorsi),
          programma(programma),
          distanzaFrenataLocomotiva(distanzaFrenataLocomotiva),
          logger(logger) {};

    // *** METODI PUBBLICI *** //

    bool setPosizioneConvoglio(IdSezioneTipo idSezioneCorrenteOccupata,
                               bool direzionePrimaLocomotivaDestraPuntoDiVistaUtente);
    bool inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo idBinarioStazione,
                                                                 int delayAttesaPartenzaStazioneLocale);
    void inizializzoAutopilotModalitaCasuale(
        bool ottimizzazioneAssegnazioneConvogliAiBinariModalitaAutopilotCasualeLocale);
    void processo(bool autopilotInSpegnimento);
    void resetTotale();

    void setStatoInizializzazioneAutopilotConvoglio(
        StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglioLocale);
    void setStatoAutopilotModalitaStazioneSuccessiva(
        StatoAutopilotModalitaStazioneSuccessiva statoAutopilotModalitaStazioneSuccessivaLocale);
    void setStatoAutopilotModalitaApproccioCasuale(
        StatoAutopilotModalitaApproccioCasuale statoAutopilotModalitaApproccioCasualeLocale);

    bool isSezioneLungaASufficienzaPerConvoglio(SezioneAbstract *sezione) const;

   private:
    // ** FUNZIONI AUSILIARIE ** //

    void resetParziale();

    void processoModalitaAutopilotSelezionata(bool autopilotInSpegnimento);

    LunghezzaTracciatoTipo calcoloLunghezzaSezioniBloccate();

    void aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop();
    LunghezzaTracciatoTipo calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();
    LunghezzaTracciatoTipo calcoloDistanzaDaInizioSezioneAStopConvoglioBinarioStazione(SezioneAbstract *sezione);

    byte getIndiceStazionePartenzaCorrente() const;
    byte getIndiceStazioneArrivoCorrente() const;

    bool lockSezioni(IdSezioneTipo *arraySezioni, int numeroElementiArray);
    void lockSezioniFuturo();
    bool lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();
    bool lockSezioniDaSezioneASezione_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva(int indiceSezionePartenza,
                                                                                            int indiceSezioneArrivo);
    int trovoIndiceSezioniIntermedie_PrimaSezioneConSensore();
    int trovoIndiceSezioniIntermedie_ProssimaSezioneConSensore();
    int trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    void unlockSezioni(IdSezioneTipo *arraySezioni, int numeroElementiArray);
    void unlockSezioniPassaggioConvoglioAvvenuto();

    // *** FUNZIONI CORE *** //

    void leggoPassaggioSensoreSuccessivoSeEsiste();
    void attesaInizializzazioneConvoglio();
    void prontoPerConfigurazioneAutopilot();
    bool fermoStazioneAttesaMomentoAttesaPartenza();
    void fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva();
    void fermoStazioneCalcoloDirezioneConvoglio();
    bool fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente();
    void inPartenzaStazione();
    bool inMovimentoInAttesaPrimoSensore();
    RisultatoInMovimento inMovimento();
    bool fermoSezioneAttesaLockSezioni();
    void scelgoBinarioStazioneSuccessivaELock();
    void arrivatoStazioneDestinazione();

    static LunghezzaTracciatoTipo getDistanzaCmStopConvoglioDaFineSezioneIntermedia(SezioneAbstract *sezione);
    void cambioStatoInizializzazioneAutopilotConvoglio(
        StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglioNuovo);
    void cambioStatoAutopilotModalitaStazioneSuccessiva(StatoAutopilotModalitaStazioneSuccessiva statoNuovo);
    void cambioStatoAutopilotModalitaApproccioCasuale(StatoAutopilotModalitaApproccioCasuale statoNuovo);

    LunghezzaTracciatoTipo calcoloDistanzaPercorsaDalConvoglioUltimoIntervalloTempo(
        TimestampTipo intervalloTempo) const;

    void eseguoOperazioniPrimoSensoreAzionato();
    void eseguoOperazioniSensoreAttesoNonAzionato(IdSensorePosizioneTipo idSensore);
};

#endif
