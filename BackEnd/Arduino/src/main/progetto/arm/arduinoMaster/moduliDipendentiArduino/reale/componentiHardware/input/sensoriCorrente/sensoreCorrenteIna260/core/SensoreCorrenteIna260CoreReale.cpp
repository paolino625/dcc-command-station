// *** INCLUDE *** //

#include "SensoreCorrenteIna260CoreReale.h"

// *** DEFINIZIONE METODI *** //

// Di default la libreria usa Wire. Dato che sto usando SDA1 e SCL1 di ArduinoGiga, specifico che voglio usare Wire1.
bool SensoreCorrenteIna260CoreReale::begin(int indirizzoI2c) { return ina260.begin(indirizzoI2c, &Wire1); }

float SensoreCorrenteIna260CoreReale::readBusVoltage() { return ina260.readBusVoltage(); }

float SensoreCorrenteIna260CoreReale::readCurrent() { return ina260.readCurrent(); }

// *** DEFINIZIONE VARIABILI *** //

SensoreCorrenteIna260CoreReale sensoreCorrenteIna260CoreReale = SensoreCorrenteIna260CoreReale();
