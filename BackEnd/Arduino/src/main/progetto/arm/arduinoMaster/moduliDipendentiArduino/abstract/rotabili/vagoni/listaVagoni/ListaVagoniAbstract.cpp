// *** INCLUDE *** //

#include "ListaVagoniAbstract.h"

#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void ListaVagoniAbstract::inizializza() {
    for (int i = 1; i < NUMERO_VAGONI; i++) {
        listaVagoni[i] = (char *)malloc(NUMERO_CARATTERI_PER_VAGONE_LISTA);  // +1 per il terminatore null
    }
}

int ListaVagoniAbstract::creo(bool mostraVagoniInUso, bool mostraVagoniNonInUso, ConvoglioAbstractReale *convoglio) {
    byte numeroVagoniTrovati = 0;
    byte indiceLista = 1;
    for (int i = 1; i < NUMERO_VAGONI; i++) {
        // TODO Potenziale problema con Arduino Giga?
        std::string stringaVagone;

        bool condizione;
        // Se idConvoglioDisplayKeypad == 0 non sto cercando informazioni convogli relativi a un
        // determinato convoglio
        if (convoglio == 0) {
            if (mostraVagoniInUso && mostraVagoniNonInUso) {
                // La condizione è sempre true perché voglio mostrare tutte le locomotive
                condizione = true;
            } else if (mostraVagoniInUso && !mostraVagoniNonInUso) {
                condizione = convogli.isVagoneInUso(vagoni.getVagone(i));
            } else if (!mostraVagoniInUso && mostraVagoniNonInUso) {
                condizione = !convogli.isVagoneInUso(vagoni.getVagone(i));
            } else {
                condizione = false;
            }
            // Se idConvoglioDisplayKeypad è diverso da 0, voglio mostrare i vagoni in uso da quel
            // convoglio
        } else {
            if (!mostraVagoniInUso) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La funzione non lavora così.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                programma.ferma(true);
            }
            condizione = convoglio->esisteVagone(i);
        }

        if (condizione) {
            numeroVagoniTrovati += 1;

            stringaVagone += std::to_string(vagoni.getVagone(i)->getId());
            stringaVagone += ". ";
            stringaVagone += vagoni.getVagone(i)->getNome();

            strcpy(listaVagoni[indiceLista], stringaVagone.c_str());
            indiceLista += 1;
        }
    }

    return numeroVagoniTrovati;
}