// *** INCLUDE *** //

#include "MappingStepLocomotiva.h"

#include <cfloat>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/KeypadReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/usb/usb/Usb.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/attesaPassaggioSensorePosizione/AttesaPassaggioSensorePosizione.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/menuDisplayKeypad/operazioniMultiple/MenuDisplayKeypadOperazioniMultipleAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/menuDisplayKeypad/uscitaMenu/UscitaMenuReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/realTimeClock/RealTimeClock.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/sezioni/SezioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/base/extended/PacchettoExtended.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/velocita/Velocita.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/utility/ConversioneVelocita.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/bufferMenuDisplayKeypad/BufferMenuDisplayKeypad.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/conversioneTempo/ConversioneTempo.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

MappingStepLocomotiva mappingStepLocomotiva;

// *** DEFINIZIONE METODI *** //

void MappingStepLocomotiva::mappoVelocitaLoco(LocomotivaAbstract *locomotiva, byte numeroSensoreIr1,
                                              byte numeroSensoreIr2, StepVelocitaDecoderTipo stepIniziale,
                                              StepVelocitaDecoderTipo stepFinale, bool modalita2SensoriUtente,
                                              InvioStatoTracciatoAbstract invioTracciatoStatoLocomotive,
                                              ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuito,
                                              ProtocolloDccCoreAbstract &protocolloDccCore) const {
    // Durante il mapping di una locomotiva, ogni passaggio della locomotiva sul sensore deve essere considerato. Non dobbiamo ignorare il passaggio della seconda locomotiva, come facciamo normalmente.
    modalitaLetturaSensore = ModalitaLetturaSensore::LETTURA_LOCOMOTIVA;

    TimestampTipo timerInizioMapping = millis();

    // Anche durante il mapping devo considerare il tempo per cui la locomotiva si muove.
    locomotiva->avviaTimerParziale();

    // Fermo tutte le locomotive sul plastico
    invioTracciatoStatoLocomotive.stopEmergenza();

    // Azzero letture dei sensori: altrimenti potrei leggere erroneamente delle letture precedenti
    // se il controllo delle locomotive era manuale
    sensoriPosizioneReali.reset(true);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Avvio mapping velocita locomotiva ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(locomotiva->getId()).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    // Spengo tutte le luci della locomotiva
    locomotiva->spegneLuciBasilari();
    locomotiva->spegneLuciAusiliarie();
    invioTracciatoStatoLocomotive.invioPacchettiLuciLocomotiva(locomotiva);

    // Compongo il nome del file
    char pathFile[60] = "mappingLocomotive/mapping_";

    // Aggiungo numero della locomotiva
    char idLocomotivaStringa[5];
    itoa(locomotiva->getId(), idLocomotivaStringa, 10);
    strcat(pathFile, "locomotiva");
    strcat(pathFile, idLocomotivaStringa);

    strcat(pathFile, "_");

    // Aggiungo il timestamp attuale al nome del file
    String clock = RealTimeClock::getData();
    strcat(pathFile, clock.c_str());

    char estensione[5] = ".txt";
    strcat(pathFile, estensione);

    // Apro il file
    usb.apreFileTestoScrittura(pathFile, true, true);

    // Stampo titolo
    fprintf(usb.getFile(), "%s", "---------------------------------\n");
    fprintf(usb.getFile(), "%s", "RESOCONTO VELOCITA STEP LOCOMOTIVA ");
    fprintf(usb.getFile(), "%d", locomotiva);

    fprintf(usb.getFile(), "%s", "\n\n");

    fprintf(usb.getFile(), "%s", "Step iniziale: ");
    fprintf(usb.getFile(), "%d", stepIniziale);
    fprintf(usb.getFile(), "%s", "\n");

    fprintf(usb.getFile(), "%s", "Step finale: ");
    fprintf(usb.getFile(), "%d", stepFinale - 1);
    fprintf(usb.getFile(), "%s", "\n\n");

    float velocitaMassimaLocomotivaCmSAvanti;
    float velocitaMassimaLocomotivaCmSIndietro;

    // Salvo la direzione della locomotiva prima di iniziare il mapping
    bool direzioneAvantiDisplayLocomotivaPrecedenteAMapping = locomotiva->getDirezioneAvantiDisplay();

    // For per direzione locomotiva: sia avanti che indietro
    for (int r = 0; r < 2; r++) {
        if (r == 0) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "DIREZIONE AVANTI",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        } else if (r == 1) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "DIREZIONE INDIETRO",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
        float velocitaStepLocomotiva[NUMERO_STEP_VELOCITA_DECODER] = {0.00};
        TimestampTipo timer1, timer2;
        TimestampTipo differenzaTimerMillisecondi;

        // Imposto direzione avanti display
        if (r == 0) {
            locomotiva->setDirezioneAvantiDisplay(true);
            locomotiva->aggiornaDirezioneAvantiDecoder();
        } else if (r == 1) {
            locomotiva->setDirezioneAvantiDisplay(false);
            locomotiva->aggiornaDirezioneAvantiDecoder();
        }

        bool sensoriInvertiti = false;

        bool modalita2SensoriFinita = false;
        bool modalita2Sensori;
        // Se si tratta di un mapping completo inizio a impostare modalità 2 sensori e poi passerà
        // automaticamente ad 1. Se la modalità 2 sensori è stata voluta dall'utente la imposto
        // ugualmente.
        if (isMappingCompleto(stepIniziale, stepFinale) || modalita2SensoriUtente) {
            modalita2Sensori = true;
        } else {
            modalita2Sensori = false;
        }

        for (int i = stepIniziale; i < stepFinale; i++) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  "Step velocita: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(i).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            int numeroGiro = 0;
            VelocitaLocomotivaCmSTipo velocitaLocomotiveGiro[10];  // Utile per calcolo scarto quadratico medio
            TimestampTipo tempoTuttiGiri = 0;

            IdSensorePosizioneTipo numeroSensoreIrInizio, numeroSensoreIrFine;

            // Se al giro precedente ho raggiunto la soglia di switch da 2 sensori ad 1 cambio ora
            // la variabile booleana. Non posso farlo prima altrimenti ha ripercussioni sul
            // resoconto giro.
            if (modalita2SensoriFinita) {
                modalita2Sensori = false;
                modalita2SensoriFinita = false;
            }
            if (modalita2Sensori) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Modalita' 2 sensori",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                // Capisco su quale sensore devo aspettare
                if (r == 0) {
                    // Direzione avanti
                    if (!sensoriInvertiti) {
                        numeroSensoreIrInizio = numeroSensoreIr1;
                        numeroSensoreIrFine = numeroSensoreIr2;
                    } else {
                        numeroSensoreIrInizio = numeroSensoreIr2;
                        numeroSensoreIrFine = numeroSensoreIr1;
                    }
                    // Entro nell'else se r == 1
                } else {
                    // Direzione indietro
                    if (!sensoriInvertiti) {
                        numeroSensoreIrInizio = numeroSensoreIr2;
                        numeroSensoreIrFine = numeroSensoreIr1;
                    } else {
                        numeroSensoreIrInizio = numeroSensoreIr1;
                        numeroSensoreIrFine = numeroSensoreIr2;
                    }
                }

                displayReale.locomotiva.mapping.schermataPrincipale.schermataPrincipale(locomotiva, i, numeroGiro - 1);

                // Invio pacchetto velocità
                compongoPacchettoVelocitaEsteso(&pacchettoExtended.pacchetto, locomotiva->getIndirizzoDecoderDcc(), i,
                                                locomotiva->getDirezioneAvantiDecoder(), false);
                protocolloDccCore.invioPacchetto(pacchettoExtended.pacchetto);

                displayReale.locomotiva.mapping.schermataPrincipale.schermataPrincipale(locomotiva, i, numeroGiro);

                // Aspetto che passi sul 1° sensore

                aspettoPassaggioSensoreControlloCortoCircuito(numeroSensoreIrInizio, controlloCortoCircuito,
                                                              menuDisplayKeypadOperazioniMultipleReale);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Rilevato passaggio sul sensore inizio",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);

                timer1 = millis();

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Timer partito\n",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                // Aspetto che passi sul 2° sensore
                aspettoPassaggioSensoreControlloCortoCircuito(numeroSensoreIrFine, controlloCortoCircuito,
                                                              menuDisplayKeypadOperazioniMultipleReale);

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Rilevato passaggio sul sensore fine",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                timer2 = millis();
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Timer fermato",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                differenzaTimerMillisecondi = timer2 - timer1;
                tempoTuttiGiri += differenzaTimerMillisecondi;

                TimestampTipo tempoGiroSecondi = differenzaTimerMillisecondi / 1000;

                VelocitaLocomotivaCmSTipo velocitaLocomotivaGiro =
                    lunghezzaTracciatoMappingLocomotivaCm2Sensori / tempoGiroSecondi;
                velocitaLocomotiveGiro[numeroGiro] = velocitaLocomotivaGiro;
                numeroGiro++;

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Numero giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(numeroGiro).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Tempo giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(tempoGiroSecondi).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " s",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Velocita giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(velocitaLocomotivaGiro).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " cm/s",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Velocita reale giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                      false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      std::to_string(convertoVelocitaModellinoAReale(velocitaLocomotivaGiro)).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " km/h",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Tempo complessivo: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                      false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(tempoTuttiGiri).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " ms\n",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                // Aspetto un po' così la locomotiva può allontanarsi dal sensore

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Aspetto per consentire allontanamento dal sensore... ",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                delay(ATTESA_MODALITA_2SENSORI_RETROMARCIA_LOCOMOTIVA);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                      "Aspetto per consentire allontanamento dal sensore... OK",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                // Se ho raggiunto la soglia di cambiamento e la modalità 2 sensori non è stata
                // impostata dall'utente (sto dunque facendo il mapping completo) procedo a passare
                // alla modalità 1 sensore
                if (velocitaLocomotivaGiro > VELOCITA_CMS_THRESHOLD_SWITCH_DA2A1SENSORI && !modalita2SensoriUtente) {
                    LOG_MESSAGGIO_STATICO(
                        loggerReale, LivelloLog::INFO,
                        "Threshold switch da 2 a 1 sensore raggiunto: al prossimo giro passo a modalità 1 sensore!",
                        InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    modalita2SensoriFinita = true;
                }

                // Inverto direzione per il prossimo giro
                locomotiva->cambiaDirezione();

                sensoriInvertiti = !sensoriInvertiti;

            } else {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Modalita' 1 sensore",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);

                displayReale.locomotiva.mapping.schermataPrincipale.schermataPrincipale(locomotiva, i, numeroGiro - 1);

                // Imposto direzione avanti display
                if (r == 0) {
                    locomotiva->setDirezioneAvantiDisplay(true);
                    locomotiva->aggiornaDirezioneAvantiDecoder();
                } else if (r == 1) {
                    locomotiva->setDirezioneAvantiDisplay(false);
                    locomotiva->aggiornaDirezioneAvantiDecoder();
                }

                // Invio pacchetto velocità
                compongoPacchettoVelocitaEsteso(&pacchettoExtended.pacchetto, locomotiva->getIndirizzoDecoderDcc(), i,
                                                locomotiva->getDirezioneAvantiDecoder(), false);
                protocolloDccCore.invioPacchetto(pacchettoExtended.pacchetto);

                // Aspetto che la locomotiva passi sul sensore
                aspettoPassaggioSensoreControlloCortoCircuito(numeroSensoreIr1, controlloCortoCircuito,
                                                              menuDisplayKeypadOperazioniMultipleReale);

                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Rilevato primo passaggio su sensore",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                // Aspetto che la locomotiva passi sul sensore di nuovo. Questo perché la locomotiva
                // la prima volta che passa sul sensore poteva essere ancora in fase di
                // accelerazione e quindi la misura è sbagliata.

                aspettoPassaggioSensoreControlloCortoCircuito(numeroSensoreIr1, controlloCortoCircuito,
                                                              menuDisplayKeypadOperazioniMultipleReale);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Rilevato secondo passaggio su sensore",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                timer1 = millis();
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Timer partito",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                // Per gli step veloci, faccia fare alla locomotiva vari giri e faccio la media.
                // Questo perché ci sono piccole variazioni.
                while (tempoTuttiGiri < INTERVALLO_TEMPO_MINIMO_STEP_DECODER_MAPPING_1SENSORE) {
                    displayReale.locomotiva.mapping.schermataPrincipale.schermataPrincipale(locomotiva, i, numeroGiro);

                    aspettoPassaggioSensoreControlloCortoCircuito(numeroSensoreIr1, controlloCortoCircuito,
                                                                  menuDisplayKeypadOperazioniMultipleReale);

                    timer2 = millis();
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Timer fermato",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                    differenzaTimerMillisecondi = timer2 - timer1;
                    tempoTuttiGiri += differenzaTimerMillisecondi;

                    // Faccio ripartire il timer per il prossimo giro, se c'è
                    timer1 = millis();
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Timer partito",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);

                    TimestampTipo tempoGiroSecondi = differenzaTimerMillisecondi / 1000;

                    float velocitaLocomotivaGiro = lunghezzaTracciatoMappingLocomotivaCm1Sensore / tempoGiroSecondi;
                    velocitaLocomotiveGiro[numeroGiro] = velocitaLocomotivaGiro;
                    numeroGiro++;

                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                          "Numero giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(numeroGiro).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                          "Tempo giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(tempoGiroSecondi).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " s",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                          "Velocita giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                          false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(velocitaLocomotivaGiro).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " cm/s",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                          "Velocita reale giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                          false);
                    LOG_MESSAGGIO_STATICO(
                        loggerReale, LivelloLog::INFO,
                        std::to_string(convertoVelocitaModellinoAReale(velocitaLocomotivaGiro)).c_str(),
                        InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " km/h",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                          "Tempo complessivo: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                          false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(tempoTuttiGiri).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " ms",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                }
            }

            TimestampTipo tempoGiriSecondi = tempoTuttiGiri / 1000;
            TimestampTipo tempoMedioGiroSecondi = tempoGiriSecondi / numeroGiro;

            if (modalita2Sensori) {
                velocitaStepLocomotiva[i] = lunghezzaTracciatoMappingLocomotivaCm2Sensori / tempoMedioGiroSecondi;
            } else {
                velocitaStepLocomotiva[i] = lunghezzaTracciatoMappingLocomotivaCm1Sensore / tempoMedioGiroSecondi;
            }

            // Scarto quadratico medio
            float sommaGiri = 0;
            float scartoQuadraticoMedio;
            for (int z = 0; z < numeroGiro; z++) {
                // Alla velocità del giro sottraggo la velocità media
                sommaGiri += pow(velocitaLocomotiveGiro[z] - velocitaStepLocomotiva[i], 2);
            }
            // Divido per il numero di campioni e poi estraggo radice quadrata
            scartoQuadraticoMedio = sqrt(sommaGiri / (float)numeroGiro);

            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "RESOCONTO GIRO",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  "Step Decoder: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(i).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  "Tempo medio giro: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(tempoMedioGiroSecondi).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " s",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  "Velocita media: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(velocitaStepLocomotiva[i]).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " cm/s",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  "Velocita media reale: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  std::to_string(convertoVelocitaModellinoAReale(velocitaStepLocomotiva[i])).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " km/h",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                  "Scarto quadratico medio: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(scartoQuadraticoMedio).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " cm/s",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

            if (i == 126) {
                fprintf(usb.getFile(), "%s",
                        "Si tratta dello step finale: posso dunque calcolare la velocità massima "
                        "della locomotiva in "
                        "questa direzione.\n\n");
                // Se si tratta dello step finale in direzione avanti, posso calcolare la velocità
                // massima
                if (r == 0) {
                    velocitaMassimaLocomotivaCmSAvanti = velocitaStepLocomotiva[i];

                    fprintf(usb.getFile(), "%s", "Velocita massima avanti locomotiva modellino: ");
                    fprintf(usb.getFile(), "%f", velocitaMassimaLocomotivaCmSAvanti);
                    fprintf(usb.getFile(), "%s", " cm/s\n");
                    fprintf(usb.getFile(), "%s", " \n");

                } else if (r == 1) {
                    velocitaMassimaLocomotivaCmSIndietro = velocitaStepLocomotiva[i];

                    fprintf(usb.getFile(), "%s", "Velocita massima indietro locomotiva modellino: ");
                    fprintf(usb.getFile(), "%f", velocitaMassimaLocomotivaCmSIndietro);
                    fprintf(usb.getFile(), "%s", " cm/s\n");
                    fprintf(usb.getFile(), "%s", " \n");

                    // La velocità in avanti potrebbe essere differente dalla velocità indietro:
                    // consideriamo dunque come velocità massima della locomotiva il valore minimo
                    // tra le due velocità
                    VelocitaLocomotivaCmSTipo velocitaMassimaLocomotivaCmS =
                        min(velocitaMassimaLocomotivaCmSAvanti, velocitaMassimaLocomotivaCmSIndietro);
                    VelocitaRotabileKmHTipo velocitaMassimaLocomotivaKmHReale =
                        convertoVelocitaModellinoAReale(velocitaMassimaLocomotivaCmS);
                    locomotiva->setVelocitaMassimaKmH(velocitaMassimaLocomotivaKmHReale);

                    fprintf(usb.getFile(), "%s", "Calcolo velocità massima locomotiva: ");
                    fprintf(usb.getFile(), "%d", velocitaMassimaLocomotivaKmHReale);
                    fprintf(usb.getFile(), "%s", " km/h\n\n");
                }
            }
        }

        // Fermo locomotiva
        compongoPacchettoVelocitaEsteso(&pacchettoExtended.pacchetto, locomotiva->getIndirizzoDecoderDcc(), 0,
                                        locomotiva->getDirezioneAvantiDecoder(), false);
        protocolloDccCore.invioPacchetto(pacchettoExtended.pacchetto);

        // Imposto la direzione della locomotiva a come era prima di iniziare il mapping. Altrimenti se la locomotiva
        // mappata apparteneva a un convoglio, ci troveremmo con la direzione delle locomotive del convoglio non
        // sincronizzate.
        locomotiva->setDirezioneAvantiDisplay(direzioneAvantiDisplayLocomotivaPrecedenteAMapping);

        // Per ogni step della velocità display (km/h) capisco qual è lo step decoder che si
        // avvicina di più

        byte mappingVelocitaStepTemporaneo[NUMERO_STEP_VELOCITA_DISPLAY_KMH + 1];
        // Imposto valore a indice 0 (che non viene utilizzato) a 0
        mappingVelocitaStepTemporaneo[0] = 0;

        for (int i = 1; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
            VelocitaRotabileKmHTipo velocitaTarget = i;

            byte stepPiuVicino = 0;
            float differenzaMiglioreAlTarget = FLT_MAX;

            for (int z = 1; z < NUMERO_STEP_VELOCITA_DECODER; z++) {
                float differenzaTarget =
                    abs(convertoVelocitaModellinoAReale(velocitaStepLocomotiva[z]) - velocitaTarget);

                if (differenzaTarget < differenzaMiglioreAlTarget) {
                    stepPiuVicino = z;
                    differenzaMiglioreAlTarget = differenzaTarget;
                }
            }

            mappingVelocitaStepTemporaneo[i] = stepPiuVicino;
        }

        if (r == 0) {
            fprintf(usb.getFile(), "%s", "---------------------------------");
            fprintf(usb.getFile(), "%s", "\n");
            fprintf(usb.getFile(), "%s", "DIREZIONE AVANTI\n");
            fprintf(usb.getFile(), "%s", "\n");
        } else if (r == 1) {
            fprintf(usb.getFile(), "%s", "---------------------------------");
            fprintf(usb.getFile(), "%s", "\n");
            fprintf(usb.getFile(), "%s", "DIREZIONE INDIETRO\n");
            fprintf(usb.getFile(), "%s", "\n");
        }

        fprintf(usb.getFile(), "%s", "Step | Velocita modello | Velocita reale\n");

        for (int i = stepIniziale; i < stepFinale; i++) {
            if (i < 10) {
                fprintf(usb.getFile(), "%s", "  ");
            } else if (i < 100 && i >= 10) {
                fprintf(usb.getFile(), "%s", " ");
            }
            fprintf(usb.getFile(), "%d", i);
            fprintf(usb.getFile(), "%s", "      ");
            if (velocitaStepLocomotiva[i] < 10) {
                fprintf(usb.getFile(), "%s", "  ");
            } else if (velocitaStepLocomotiva[i] < 100 && velocitaStepLocomotiva[i] >= 10) {
                fprintf(usb.getFile(), "%s", " ");
            }
            fprintf(usb.getFile(), "%f", velocitaStepLocomotiva[i]);
            fprintf(usb.getFile(), "%s", " cm/s       ");
            VelocitaRotabileKmHTipo velocitaReale = convertoVelocitaModellinoAReale(velocitaStepLocomotiva[i]);
            if (velocitaReale < 10) {
                fprintf(usb.getFile(), "%s", "  ");
            } else if (velocitaReale < 100 && velocitaReale >= 10) {
                fprintf(usb.getFile(), "%s", " ");
            }
            fprintf(usb.getFile(), "%d", velocitaReale);
            fprintf(usb.getFile(), "%s", " km/h\n");
        }
        fprintf(usb.getFile(), "%s", "\n");

        // Mapping velocità km/h -> mapping decoder

        fprintf(usb.getFile(), "%s", "---------------------------------\n");
        fprintf(usb.getFile(), "%s", "\n");

        fprintf(usb.getFile(), "%s", "Velocita Display | Step Decoder | Velocità Locomotiva\n");

        for (int i = 1; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
            if (i < 10) {
                fprintf(usb.getFile(), "%s", "  ");
            } else if (i < 100 && i >= 10) {
                fprintf(usb.getFile(), "%s", " ");
            }
            fprintf(usb.getFile(), "%s", "   ");
            fprintf(usb.getFile(), "%d", i);
            fprintf(usb.getFile(), "%s", " km/h            ");
            if (mappingVelocitaStepTemporaneo[i] < 10) {
                fprintf(usb.getFile(), "%s", "  ");
            } else if (mappingVelocitaStepTemporaneo[i] < 100 && mappingVelocitaStepTemporaneo[i] >= 10) {
                fprintf(usb.getFile(), "%s", " ");
            }
            fprintf(usb.getFile(), "%d", mappingVelocitaStepTemporaneo[i]);
            fprintf(usb.getFile(), "%s", "           ");
            VelocitaLocomotivaCmSTipo velocitaReale =
                convertoVelocitaModellinoAReale(velocitaStepLocomotiva[mappingVelocitaStepTemporaneo[i]]);
            if (velocitaReale < 10) {
                fprintf(usb.getFile(), "%s", "  ");
            } else if (velocitaReale < 100 && velocitaReale >= 10) {
                fprintf(usb.getFile(), "%s", " ");
            }
            fprintf(usb.getFile(), "%f", velocitaReale);
            fprintf(usb.getFile(), "%s", " km/h    \n");
        }
        fprintf(usb.getFile(), "%s", "\n");

        fprintf(usb.getFile(), "%s", "---------------------------------\n");
        fprintf(usb.getFile(), "%s", "\n");
        fprintf(usb.getFile(), "%s", "Mapping compatto:\n");

        fprintf(usb.getFile(), "%s", "{0");
        for (int i = 1; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
            fprintf(usb.getFile(), "%s", ",");
            fprintf(usb.getFile(), "%d", mappingVelocitaStepTemporaneo[i]);
        }
        fprintf(usb.getFile(), "%s", "}");
        fprintf(usb.getFile(), "%s", "\n\n");

        // Se si tratta del mapping completo, salvo mapping nella struct delle Locomotive
        if (isMappingCompleto(stepIniziale, stepFinale)) {
            LOG_MESSAGGIO_STATICO(
                loggerReale, LivelloLog::INFO,
                "Questo è un mapping completo: salvo dunque il mapping della locomotiva nella relativa struct...",
                InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            for (int i = 0; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
                // Se r == 0 allora sono dati riguardanti la locomotiva che va in avanti, altrimenti
                // indietro
                if (r == 0) {
                    locomotiva->setMappingVelocitaStepAvanti(i, mappingVelocitaStepTemporaneo[i]);
                } else if (r == 1) {
                    locomotiva->setMappingVelocitaStepIndietro(i, mappingVelocitaStepTemporaneo[i]);
                }
            }
            LOG_MESSAGGIO_STATICO(
                loggerReale, LivelloLog::INFO,
                "Questo è un mapping completo: salvo dunque il mapping della locomotiva nella relativa struct... OK",
                InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }

        if (r == 0) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Ho finito il mapping con direzione avanti.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
    usb.chiudeFile(true);

    // Leggo e stampo byte dopo byte del file appena salvato
    usb.apreFileTestoLettura(pathFile);
    int data;
    while ((data = fgetc(usb.getFile())) != EOF) {
        fprintf(usb.getFile(), "%c", data);
    }

    usb.chiudeFile(true);

    locomotiva->fermaTimerParziale();

    TimestampTipo timerFineMapping = millis();
    TimestampTipo durataMappingMillis = timerFineMapping - timerInizioMapping;
    TimestampTipo durataMappingMinuti = convertoMillisecondiAMinuti(durataMappingMillis);

    displayReale.locomotiva.mapping.schermataPrincipale.mappingTerminato(durataMappingMinuti);
    buzzerRealeArduinoMaster.beepOk();

    // Aspetto un input dal keypad per feedback ricezione avviso
    int sceltaMenu = keypadReale.leggoNumero1Cifra(false);
    while (sceltaMenu == -1) {
        sceltaMenu = keypadReale.leggoNumero1Cifra(false);
    }
    // Aggiorno il file delle locomotive in quanto c'è da salvare il mapping della locomotiva su USB
    // (finora salvato solo nella memoria di Arduino)
    uscitaMenuReale.escoMenu(false, true, false, false);

    // Reimposto la modalità di lettura del sensore
    modalitaLetturaSensore = ModalitaLetturaSensore::LETTURA_CONVOGLIO;
}

bool MappingStepLocomotiva::isMappingCompleto(StepVelocitaDecoderTipo stepIniziale,
                                              StepVelocitaDecoderTipo stepFinale) {
    if (stepIniziale == 1 && stepFinale == NUMERO_STEP_VELOCITA_DECODER) {
        return true;
    } else {
        return false;
    }
}

void MappingStepLocomotiva::inizializzaLunghezzeTracciatoMapping() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo lunghezze tracciato mapping...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    LunghezzaTracciatoTipo lunghezzaSezioneConSensore1 =
        sezioniReali.getSezione(IdSezioneTipo(2, 32))->getDistanzaDaSensoreAFineSezioneCm(true, 1);
    LunghezzaTracciatoTipo lunghezzaSezioneConSensore2 =
        sezioniReali.getSezione(IdSezioneTipo(2, 12))->getDistanzaDaSensoreAFineSezioneCm(false, 1);

    lunghezzaTracciatoMappingLocomotivaCm1Sensore = sezioniReali.getSezione(IdSezioneTipo(2, 32))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 55, 1))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 30))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 38, 2))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 28))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 28, 1))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 12))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 27, 2))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 11))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 26, 1))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 9))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 6, 2))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 5))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 8, 2))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 9, 2))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 23))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 14, 1))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 24))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 37, 2))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(2, 43))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 52, 1))->getLunghezzaCm() +
                                                    sezioniReali.getSezione(IdSezioneTipo(3, 53, 1))->getLunghezzaCm();

    lunghezzaTracciatoMappingLocomotivaCm2Sensori =
        lunghezzaSezioneConSensore1 + sezioniReali.getSezione(IdSezioneTipo(3, 55, 1))->getLunghezzaCm() +
        sezioniReali.getSezione(IdSezioneTipo(2, 30))->getLunghezzaCm() +
        sezioniReali.getSezione(IdSezioneTipo(3, 38, 2))->getLunghezzaCm() +
        sezioniReali.getSezione(IdSezioneTipo(2, 28))->getLunghezzaCm() +
        sezioniReali.getSezione(IdSezioneTipo(3, 28, 1))->getLunghezzaCm() + lunghezzaSezioneConSensore2;

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo lunghezze tracciato mapping... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MappingStepLocomotiva::inizializzaModalitaTuttiStep() {
    stepInizialeDisplayKeypad = 1;
    stepFinaleDisplayKeypad = NUMERO_STEP_VELOCITA_DECODER;
    modalita2SensoriIntDisplayKeypad = 0;
    modalita2SensoriDisplayKeypad = false;
}

void MappingStepLocomotiva::inizializzaMappingVelocitaSingoloStep() {
    stepFinaleDisplayKeypad = stepInizialeDisplayKeypad + 1;

    if (modalita2SensoriIntDisplayKeypad == 1) {
        modalita2SensoriDisplayKeypad = false;
    } else if (modalita2SensoriIntDisplayKeypad == 2) {
        modalita2SensoriDisplayKeypad = true;
    }
}

void MappingStepLocomotiva::inizializzaMappingVelocitaIntervalloStep() {
    if (modalita2SensoriIntDisplayKeypad == 1) {
        modalita2SensoriDisplayKeypad = false;
    } else if (modalita2SensoriIntDisplayKeypad == 2) {
        modalita2SensoriDisplayKeypad = true;
    }
}
