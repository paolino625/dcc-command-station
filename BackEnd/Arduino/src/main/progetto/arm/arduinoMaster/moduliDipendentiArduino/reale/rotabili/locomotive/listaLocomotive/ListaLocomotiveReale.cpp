// *** INCLUDE *** //

#include "ListaLocomotiveReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

ListaLocomotiveAbstract listaLocomotiveReale =
    ListaLocomotiveAbstract(locomotiveReali, convogliReali, programmaReale, loggerReale);