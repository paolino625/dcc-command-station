// *** INCLUDE *** //

#include "UscitaMenuAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/arduinoSlave/comune/controlloPing/ControlloPing.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/bufferMenuDisplayKeypad/BufferMenuDisplayKeypad.h"

// *** DEFINIZIONE METODI *** //

void UscitaMenuAbstract::escoMenu(bool fileScambiDaAggiornareLocale, bool fileLocomotiveDaAggiornareLocale,
                                  bool fileConvogliDaAggiornareLocale, bool fileUtentiDaAggiornareLocale) {
    // Riattivo il controllo presenza di Arduino Slave nel caso in cui sia stato messo a false (ad
    // esempio durante mapping locomotiva)
    controlloPresenzaArduinoSlaveAttivo = true;
    display.menuPrincipale.resettoPosizione();

    if (fileScambiDaAggiornareLocale) {
        fileScambiDaAggiornare = true;
        buzzer.beepOk();
    }
    if (fileLocomotiveDaAggiornareLocale) {
        fileLocomotiveDaAggiornare = true;
        buzzer.beepOk();
    }
    if (fileConvogliDaAggiornareLocale) {
        fileConvogliDaAggiornare = true;
        buzzer.beepOk();
    }

    if (fileUtentiDaAggiornareLocale) {
        fileUtentiDaAggiornare = true;
        buzzer.beepOk();
    }
    display.homepage.displayDaAggiornare = true;
    display.getDisplayCoreAbstract().numeroPaginaSuccessivaDisplay = 1;
    display.getDisplayCoreAbstract().singolaPaginaDisplayMostrata = false;
    idLocomotivaDisplayKeypad = 0;
    idConvoglioDisplayKeypad = 0;
    display.getDisplayCoreAbstract().avvisoDaMostrare = true;

    display.getDisplayCoreAbstract().timestampDisplayPaginaPrecedente = 0;
}