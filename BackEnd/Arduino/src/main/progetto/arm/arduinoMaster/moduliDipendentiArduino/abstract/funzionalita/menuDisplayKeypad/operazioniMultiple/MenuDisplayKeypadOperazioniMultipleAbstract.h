#ifndef DCC_COMMAND_STATION_MENU_DISPLAY_KEYPAD_H
#define DCC_COMMAND_STATION_MENU_DISPLAY_KEYPAD_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** CLASSE *** //

class MenuDisplayKeypadOperazioniMultipleAbstract {
    // *** VARIABILI *** //

    ConvogliAbstract &convogli;
    DisplayAbstract &display;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   public:
    MenuDisplayKeypadOperazioniMultipleAbstract(ConvogliAbstract &convogli, DisplayAbstract &display,
                                                LoggerAbstract &logger)
        : convogli(convogli), display(display), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void cambioConvoglioCorrenteDisplay(int *idConvoglio);
    void cambioConvoglioCorrente1EAggiornoDisplay();
    void cambioConvoglioCorrente2EAggiornoDisplay();
    void cambioLuciConvoglio1EAggiornoDisplay(ConvogliAbstract &convogli);
    void cambioLuciConvoglio2EAggiornoDisplay(ConvogliAbstract &convogli);
};

#endif