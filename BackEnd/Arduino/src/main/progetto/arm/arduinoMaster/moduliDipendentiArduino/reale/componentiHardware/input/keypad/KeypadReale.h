#ifndef DCC_COMMAND_STATION_KEYPADREALE_H
#define DCC_COMMAND_STATION_KEYPADREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/KeypadAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern KeypadAbstract keypadReale;

#endif
