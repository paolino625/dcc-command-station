#ifndef DCC_COMMAND_STATION_I2CREALE_H
#define DCC_COMMAND_STATION_I2CREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/i2c/I2cAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern I2cAbstract I2CReale;

#endif
