#ifndef LOCOMOTIVADTOFACTORY_H
#define LOCOMOTIVADTOFACTORY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/locomotiva/LocomotivaRequestDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/locomotiva/LocomotivaResponseDTO.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_LOCOMOTIVA_STRINGA_JSON 100

// *** CLASSE *** //

class LocomotivaDTOFactory {
   public:
    // RESPONSE DTO

    LocomotivaResponseDTO* creaLocomotivaResponseDTO(IdLocomotivaTipo idLocomotiva);
    char* converteLocomotivaResponseDTOToStringJson(LocomotivaResponseDTO* locomotivaDTO);
};

// *** DICHIARAZIONE VARIABILI *** //

extern LocomotivaDTOFactory locomotivaDTOFactory;

#endif
