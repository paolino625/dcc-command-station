#ifndef DCC_COMMAND_STATION_DISPLAYABSTRACT_H
#define DCC_COMMAND_STATION_DISPLAYABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/DisplayStampaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/logIn/utenti/UtentiReali.h"

// *** STRUCT *** //

struct Errori {
    DisplayStampaAbstract::Errori::Inizializzazione inizializzazione;
    DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso componenteHardwareDisconnesso;
    DisplayStampaAbstract::Errori::Generico generico;
    DisplayStampaAbstract::Errori::Usb usb;
    DisplayStampaAbstract::Errori::Mqtt mqtt;
    DisplayStampaAbstract::Errori::Ethernet ethernet;
    DisplayStampaAbstract::Errori::ArduinoMasterHelper arduinoMasterHelper;

    Errori(DisplayStampaAbstract::Errori::Inizializzazione inizializzazione,
           DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso componenteHardwareDisconnesso,
           DisplayStampaAbstract::Errori::Generico generico, DisplayStampaAbstract::Errori::Usb usb,
           DisplayStampaAbstract::Errori::Mqtt mqtt, DisplayStampaAbstract::Errori::Ethernet ethernet,
           DisplayStampaAbstract::Errori::ArduinoMasterHelper arduinoMasterHelper)
        : inizializzazione(inizializzazione),
          componenteHardwareDisconnesso(componenteHardwareDisconnesso),
          generico(generico),
          usb(usb),
          mqtt(mqtt),
          ethernet(ethernet),
          arduinoMasterHelper(arduinoMasterHelper) {}
};

struct Monitoraggio {
    DisplayStampaAbstract::Monitoraggio::Assorbimenti assorbimenti;
    DisplayStampaAbstract::Monitoraggio menu;
    DisplayStampaAbstract::Monitoraggio::Tensioni tensioni;

    Monitoraggio(DisplayStampaAbstract::Monitoraggio::Assorbimenti assorbimenti,
                 DisplayStampaAbstract::Monitoraggio menu, DisplayStampaAbstract::Monitoraggio::Tensioni tensioni)
        : assorbimenti(assorbimenti), menu(menu), tensioni(tensioni) {}
};

struct Vagoni {
    DisplayStampaAbstract::Convoglio::Vagoni menu;

    Vagoni(DisplayStampaAbstract::Convoglio::Vagoni displayMenuVagoni) : menu(displayMenuVagoni) {}
};

struct Convoglio {
    DisplayStampaAbstract::Convoglio menu;
    DisplayStampaAbstract::Convoglio::Scelta scelta;
    DisplayStampaAbstract::Convoglio::DifferenzaVelocitaLocomotivaTrazioneSpinta
        differenzaVelocitaLocomotivaTrazioneSpinta;
    Vagoni vagoni;

    Convoglio(DisplayStampaAbstract::Convoglio displayMenuConvogli,
              DisplayStampaAbstract::Convoglio::Scelta displayMenuConvogliScelta,
              DisplayStampaAbstract::Convoglio::DifferenzaVelocitaLocomotivaTrazioneSpinta
                  differenzaVelocitaLocomotivaTrazioneSpinta,
              Vagoni vagoni)
        : menu(displayMenuConvogli),
          scelta(displayMenuConvogliScelta),
          differenzaVelocitaLocomotivaTrazioneSpinta(differenzaVelocitaLocomotivaTrazioneSpinta),
          vagoni(vagoni) {}
};

struct Mapping {
    DisplayStampaAbstract::Locomotiva::Mapping schermataPrincipale;
    DisplayStampaAbstract::Locomotiva::Mapping::Scelta scelta;

    Mapping(DisplayStampaAbstract::Locomotiva::Mapping displayMappingLocomotiva,
            DisplayStampaAbstract::Locomotiva::Mapping::Scelta displayMappingLocomotivaInserimento)
        : schermataPrincipale(displayMappingLocomotiva), scelta(displayMappingLocomotivaInserimento) {}
};

struct Locomotiva {
    DisplayStampaAbstract::Locomotiva menu;
    DisplayStampaAbstract::Locomotiva::Scelta scelta;
    DisplayStampaAbstract::Locomotiva::TempoFunzionamentoLocomotive tempoFunzionamento;
    DisplayStampaAbstract::Locomotiva::SetDecoderLocomotiva setDecoderLocomotiva;
    Mapping mapping;

    Locomotiva(DisplayStampaAbstract::Locomotiva menu, DisplayStampaAbstract::Locomotiva::Scelta scelta,
               DisplayStampaAbstract::Locomotiva::TempoFunzionamentoLocomotive tempoFunzionamento,
               DisplayStampaAbstract::Locomotiva::SetDecoderLocomotiva setDecoderLocomotiva, Mapping mapping)
        : menu(menu),
          scelta(scelta),
          tempoFunzionamento(tempoFunzionamento),
          setDecoderLocomotiva(setDecoderLocomotiva),
          mapping(mapping) {}
};

struct Tracciato {
    DisplayStampaAbstract::Tracciato menu;
    DisplayStampaAbstract::Tracciato::Scambi scambi;
    DisplayStampaAbstract::Tracciato::SensoriPosizione sensoriPosizione;

    Tracciato(DisplayStampaAbstract::Tracciato displayTracciato, DisplayStampaAbstract::Tracciato::Scambi displayScambi,
              DisplayStampaAbstract::Tracciato::SensoriPosizione displaySensoriPosizione)
        : menu(displayTracciato), scambi(displayScambi), sensoriPosizione(displaySensoriPosizione) {}
};

struct Accessori {
    DisplayStampaAbstract::Accessori menuPrincipale;
    DisplayStampaAbstract::Accessori::Led led;
    DisplayStampaAbstract::Accessori::Audio audio;
    DisplayStampaAbstract::Accessori::SensoreImpronte sensoreImpronte;
    DisplayStampaAbstract::Accessori::Buzzer buzzer;

    Accessori(DisplayStampaAbstract::Accessori displayAccessori, DisplayStampaAbstract::Accessori::Led displayLed,
              DisplayStampaAbstract::Accessori::Audio displayAudio,
              DisplayStampaAbstract::Accessori::SensoreImpronte displaySensoreImpronte,
              DisplayStampaAbstract::Accessori::Buzzer displayBuzzer)
        : menuPrincipale(displayAccessori),
          led(displayLed),
          audio(displayAudio),
          sensoreImpronte(displaySensoreImpronte),
          buzzer(displayBuzzer) {}
};

// *** CLASSE *** //

class DisplayAbstract {
   public:
    DisplayCoreAbstract& displayCore;
    Errori errori;
    Monitoraggio monitoraggio;
    Convoglio convoglio;
    Locomotiva locomotiva;
    Tracciato tracciato;
    Accessori accessori;
    DisplayStampaAbstract::WebServer webServer;
    DisplayStampaAbstract::MenuPrincipale menuPrincipale;
    DelayAbstract& delay;
    LoggerAbstract& logger;

    DisplayAbstract(DisplayCoreAbstract& displayCoreAbstract, Errori errori, Monitoraggio monitoraggio,
                    Convoglio convoglio, Locomotiva locomotiva, Tracciato tracciato, Accessori accessori,
                    DisplayStampaAbstract::WebServer webServer, DisplayStampaAbstract::MenuPrincipale menuPrincipale,
                   DelayAbstract& delay, LoggerAbstract& logger)
        : displayCore(displayCoreAbstract),
          errori(errori),
          monitoraggio(monitoraggio),
          convoglio(convoglio),
          locomotiva(locomotiva),
          tracciato(tracciato),
          accessori(accessori),
          webServer(webServer),
          menuPrincipale(menuPrincipale),
          delay(delay),
          logger(logger) {}

   public:
    DisplayStampaAbstract::Homepage homepage = DisplayStampaAbstract::Homepage(displayCore);

    DisplayStampaAbstract::Avvisi avvisi = DisplayStampaAbstract::Avvisi(displayCore);
    DisplayStampaAbstract::LogIn logIn = DisplayStampaAbstract::LogIn(delay, displayCore, utentiReali);

    DisplayCoreAbstract& getDisplayCoreAbstract() { return displayCore; }
};

#endif
