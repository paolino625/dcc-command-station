// *** INCLUDE *** //

#include "AccelerazioneDecelerazioneAbstract.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"

// *** DEFINIZIONE METODI *** //

/*
Se la velocità del convoglio è 0 e viene impostata 100, il cambiamento non è immediato.
La velocità del convoglio reale viene incrementata/decrementata di unità 1 ogni
INTERVALLO_AGGIORNAMENTO_VELOCITA_ATTUALE_CONVOGLIO Se si riduce troppo questo parametro, oltre
ad accelerazioni/decelerazioni del convoglio non realistiche, possono verificarsi deragliamenti
di convogli in curva.
*/

void AccelerazioneDecelerazioneAbstract::aggiorna() {
    // Scansiono tutti i convogli e per ogni convoglio verifico se la velocità attuale è diversa da quella impostata
    for (IdConvoglioTipo idConvoglio = 1; idConvoglio < NUMERO_CONVOGLI; idConvoglio++) {
        boolean convoglioAggiornato = false;

        // Verifico che il convoglio esista
        if (convogli.esisteConvoglio(idConvoglio)) {
            ConvoglioAbstract *convoglio = convogli.getConvoglio(idConvoglio, true);

            if (convoglio->isInUso()) {
                VelocitaRotabileKmHTipo velocitaAttualeConvoglio = convoglio->getVelocitaAttuale();
                if (velocitaAttualeConvoglio < convoglio->getVelocitaImpostata()) {
                    LocomotivaAbstract *locomotiva1 = convoglio->getLocomotiva1();
                    // Avvio timer quando passo da velocità attuale 0 a 1, il convoglio è in movimento
                    if (locomotiva1->getVelocitaAttuale() == 0) {
                        locomotiva1->avviaTimerParziale();
                    }
                    locomotiva1->setVelocitaAttuale(velocitaAttualeConvoglio + 1);

                    // Se il convoglio ha una seconda locomotiva, faccio lo stesso
                    if (convoglio->hasDoppiaLocomotiva()) {
                        LocomotivaAbstract *locomotiva2 = convoglio->getLocomotiva2();
                        if (locomotiva2->getVelocitaAttuale() == 0) {
                            locomotiva2->avviaTimerParziale();
                        }
                        locomotiva2->setVelocitaAttuale(velocitaAttualeConvoglio + 1);
                    }

                    convoglioAggiornato = true;
                } else if (convoglio->getVelocitaAttuale() > convoglio->getVelocitaImpostata()) {
                    LocomotivaAbstract *locomotiva1 = convoglio->getLocomotiva1();
                    // Se sto passando da 1 a 0, allora fermo il timer e aggiungo il tempo.
                    if (locomotiva1->getVelocitaAttuale() == 1) {
                        locomotiva1->fermaTimerParziale();
                    }
                    locomotiva1->setVelocitaAttuale(velocitaAttualeConvoglio - 1);

                    // Se il convoglio ha una seconda locomotiva, faccio lo stesso
                    if (convoglio->hasDoppiaLocomotiva()) {
                        LocomotivaAbstract *locomotiva2 = convoglio->getLocomotiva2();
                        if (locomotiva2->getVelocitaAttuale() == 1) {
                            locomotiva2->fermaTimerParziale();
                        }
                        locomotiva2->setVelocitaAttuale(velocitaAttualeConvoglio - 1);
                    }

                    convoglioAggiornato = true;
                }
            } else {
                // Se la velocità impostata è uguale a quella attuale non devo fare nulla
            }

            // Se il convoglio è stato aggiornato, richiedo l'aggiornamento su MQTT al Front-End
            if (convoglioAggiornato) {
                if (DEBUGGING_AGGIORNAMENTO_VELOCITA_ATTUALE_LOCOMOTIVE) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Convoglio n° ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(convoglio->getId()).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                          "Velocita Attuale: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                                          false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                          std::to_string(convoglio->getVelocitaAttuale()).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                }
                convogliAggiornamentoMqttRichiesto[idConvoglio] = true;
            }
        }
    }
}