// *** INCLUDE *** //

#include "DistanzaFrenataLocomotivaAbstract.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/AutopilotConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/utility/ConversioneVelocita.h"

// *** DEFINIZIONE METODI *** //

void DistanzaFrenataLocomotivaAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo Distanza Frenata...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 0; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
        distanzaFrenataLocomotivaCm[i] = calcoloDistanzaFrenataLocomotiva(i);
    }
    if (DEBUGGING_DISTANZA_FRENATA_LOCOMOTIVA) {
        char* distanzaFrenataLocomotiva = toString();
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, distanzaFrenataLocomotiva,
                               InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true, true);
        delete distanzaFrenataLocomotiva;
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Inizializzo Distanza Frenata... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

LunghezzaTracciatoTipo DistanzaFrenataLocomotivaAbstract::calcoloDistanzaFrenataLocomotiva(
    VelocitaRotabileKmHTipo velocitaKmHIniziale) {
    LunghezzaTracciatoTipo distanzaPercorsaCm = 0;
    for (int i = velocitaKmHIniziale; i > 0; i--) {
        VelocitaLocomotivaCmSTipo velocitaConvoglioCmS = convertoVelocitaRealeModellino(i);
        float intervalloAggiornamentoVelocitaAttualeLocomotiveSecondi =
            (float)INTERVALLO_AGGIORNAMENTO_VELOCITA_CONVOGLIO_AUTOPILOT / 1000;
        distanzaPercorsaCm += velocitaConvoglioCmS * intervalloAggiornamentoVelocitaAttualeLocomotiveSecondi;
    }
    return distanzaPercorsaCm;
}

LunghezzaTracciatoTipo DistanzaFrenataLocomotivaAbstract::getDistanzaFrenataPerVelocitaKmH(
    VelocitaRotabileKmHTipo velocitaKmH) {
    return distanzaFrenataLocomotivaCm[velocitaKmH];
}

// Dopo aver usato la stringa sarebbe buona cosa liberare la memoria allocata dinamicamente.
char* DistanzaFrenataLocomotivaAbstract::toString() {
    std::string stringa;

    stringa += "DISTANZE FRENATE\n";

    for (int i = 0; i < NUMERO_STEP_VELOCITA_DISPLAY_KMH; i++) {
        LunghezzaTracciatoTipo distanza = distanzaFrenataLocomotivaCm[i];

        stringa += std::to_string(i);
        stringa += " km/h: ";
        stringa += std::to_string(distanza / 100);
        stringa += " m\n";
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}