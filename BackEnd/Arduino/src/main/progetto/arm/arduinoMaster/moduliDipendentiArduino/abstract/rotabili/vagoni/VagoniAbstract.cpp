// *** INCLUDE *** //

#include "VagoniAbstract.h"

#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void VagoniAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo vagoni... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);
    for (int i = 1; i < NUMERO_VAGONI; i++) {
        VagoneAbstract* vagone = new VagoneAbstract(i, logger);
        vagoni[i] = vagone;
    }
    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo vagoni... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

char* VagoniAbstract::toString() {
    std::string stringa;

    stringa += "\n";
    stringa += "STATO VAGONI: \n";
    for (int i = 1; i < NUMERO_VAGONI; i++) {
        char* stringaVagone = getVagone(i)->toString();
        stringa += stringaVagone;
        delete[] stringaVagone;
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

VagoneAbstract* VagoniAbstract::getVagone(byte idVagone) {
    for (int i = 1; i < NUMERO_VAGONI; i++) {
        if (vagoni[i]->getId() == idVagone) {
            return vagoni[i];
        }
    }
    return vagoni[0];
}