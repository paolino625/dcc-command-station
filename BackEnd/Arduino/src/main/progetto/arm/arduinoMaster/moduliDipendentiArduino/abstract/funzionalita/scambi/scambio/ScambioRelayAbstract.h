#ifndef DCC_COMMAND_STATION_SCAMBIO_H
#define DCC_COMMAND_STATION_SCAMBIO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/scambi/PosizioneScambio/PosizioneScambio.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** CLASSE *** //

class ScambioRelayAbstract {
    // *** DICHIARAZIONE VARIABILI *** //

    IdScambioTipo id;
    PosizioneScambio posizioneScambio = PosizioneScambio ::NON_CONOSCIUTA;

    // *** COSTRUTTORE *** //

   public:
    ScambioRelayAbstract(IdScambioTipo id) : id(id) {}

    // *** DICHIARAZIONE METODI *** //
   public:
    void reset();
    void azionaSinistra();
    void azionaDestra();
    void setPosizioneNonConosciuta();

    bool isSinistra();
    bool isDestra();

    void setSinistra();
    void setDestra();

    int getStatoNumero();

    const char* getStatoStringa();
};

#endif