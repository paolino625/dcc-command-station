#ifndef DCC_COMMAND_STATION_LEDINTEGRATOABSTRACT_H
#define DCC_COMMAND_STATION_LEDINTEGRATOABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/DigitalPinAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class LedIntegratoAbstract {
   private:
    // *** VARIABILI *** //

    DigitalPinAbstract& pinLedBlu;
    DigitalPinAbstract& pinLedRosso;
    DigitalPinAbstract& pinLedVerde;
    DelayAbstract& delay;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    LedIntegratoAbstract(DigitalPinAbstract& pinLedBlu, DigitalPinAbstract& pinLedRosso,
                         DigitalPinAbstract& pinLedVerde, DelayAbstract& delay, LoggerAbstract& logger)
        : pinLedBlu{pinLedBlu}, pinLedRosso{pinLedRosso}, pinLedVerde{pinLedVerde}, delay{delay}, logger{logger} {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void accendiRosso();
    void accendiVerde();
    void accendiBlu();
    void spegne();
    void lampeggiaRosso(int intervalloLampeggiamentoMillisecondi);
};

#endif