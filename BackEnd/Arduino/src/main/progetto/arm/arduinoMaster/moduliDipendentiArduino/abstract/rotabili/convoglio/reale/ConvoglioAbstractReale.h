#ifndef DCC_COMMAND_STATION_CONVOGLIO_H
#define DCC_COMMAND_STATION_CONVOGLIO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/AutopilotConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/reale/PercorsiAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/ConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/vagoni/VagoniAbstract.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_NOME_CONVOGLIO 30

// *** CLASSE *** //

class ConvoglioAbstractReale : public ConvoglioAbstract {
   public:
    // *** VARIABILI SALVATI IN FILE *** //

    LocomotiveAbstract& locomotive;
    VagoniAbstract& vagoni;

    IdConvoglioTipo id;
    ProgrammaAbstract& programma;
    TempoAbstract& tempo;

    SezioniAbstract& sezioni;

    LoggerAbstract& logger;

    LunghezzaRotabileTipo lunghezzaConvoglio = 0;

    IdLocomotivaTipo idLocomotiva1 = 0;
    IdLocomotivaTipo idLocomotiva2 = 0;

    char nome[NUMERO_CARATTERI_NOME_CONVOGLIO];

    bool presenteSulTracciato = false;  // False se il convoglio non è attualmente presente sul tracciato.

    IdVagoneTipo idVagoni[NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO] = {0};  // Id dei vagoni associati a questo
                                                                       // convoglio. Parte da indice 1.

    DifferenzaVelocitaLocomotivaTrazioneSpinta differenzaVelocitaLocomotivaTrazioneSpinta;

    // *** VARIABILI VOLATILI *** //

    TimestampTipo millisPrecedenteAggiornamentoVelocitaAttualeConvoglio;

    bool luciAccese = true;

    AutopilotConvoglioAbstract* autopilotConvoglio;

   public:
    // *** COSTRUTTORE *** //

    ConvoglioAbstractReale(LocomotiveAbstract& locomotive, VagoniAbstract& vagoni, IdConvoglioTipo id,
                           ProgrammaAbstract& programma, PercorsiAbstractReale& percorsi, TempoAbstract& tempo,
                           SensoriPosizioneAbstract& sensoriPosizione, SezioniAbstract& sezioni,
                           StazioniAbstract& stazioni, DistanzaFrenataLocomotivaAbstract& distanzaFrenataLocomotiva,
                           LoggerAbstract& logger)
        : locomotive(locomotive),
          vagoni(vagoni),
          id(id),
          programma(programma),
          tempo(tempo),
          sezioni(sezioni),
          logger(logger),
          autopilotConvoglio(new AutopilotConvoglioAbstract(*this, sensoriPosizione, sezioni, stazioni, tempo,
                                                            locomotive, percorsi, programma, distanzaFrenataLocomotiva,
                                                            logger)) {}

    // *** METODI *** //

    void aggiungeSecondaLocomotiva(IdLocomotivaTipo idLocomotiva) override;
    void eliminaSecondaLocomotiva() override;
    bool isInUso() const override;
    bool hasDoppiaLocomotiva() const override;
    void reset() override;
    void resetPosizione() override;
    void cambiaVelocita(VelocitaRotabileKmHTipo nuovaVelocitaDaImpostare, bool graduale,
                        StatoAutopilot statoAutopilot) override;
    void cambiaDirezione(StatoAutopilot statoAutopilot) override;
    bool esisteVagone(byte idVagone) override;
    void aggiungeVagone(IdVagoneTipo idVagone) override;
    void eliminaVagone(IdVagoneTipo idVagone) override;
    void inverteLuci() override;
    void aggiornaLuci() const override;
    void invalidaPosizioneConvoglio(bool sezioneOccupataDalConvoglioDaLiberare) const override;
    char* toString();
    void aggiornaLunghezza() override;
    IdLocomotivaTipo getLocomotivaPiuVeloce() override;

    // *** GETTER *** //

    IdConvoglioTipo getId() const override;
    const char* getNome() const override;
    IdLocomotivaTipo getIdLocomotiva1() const override;
    IdConvoglioTipo getIdLocomotiva2() const override;
    LocomotivaAbstract* getLocomotiva1() const override;
    LocomotivaAbstract* getLocomotiva2() const override;
    bool isLuciAccese() override;
    IdVagoneTipo getIdVagone(IdVagoneTipo idVagone) override;
    StepVelocitaKmHTipo getDifferenzaStepVelocitaTraLocomotive() const override;
    TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta getTipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta()
        override;
    TimestampTipo getMillisPrecedenteAggiornamentoVelocitaAttuale() const override;
    VelocitaRotabileKmHTipo getVelocitaImpostata() const override;
    VelocitaRotabileKmHTipo getVelocitaAttuale() const override;
    LunghezzaRotabileTipo getLunghezzaCm() const override;
    VelocitaRotabileKmHTipo getVelocitaMassima() const override;
    int getNumeroVagoni() override;
    bool isPresenteSulTracciato() const override;
    LocomotivaAbstract* getLocomotivaInSpinta() const override;
    LocomotivaAbstract* getLocomotivaInTrazione() const override;
    PosizioneConvoglio getPosizione() const override;

    // *** SETTER *** //

    void setId(IdConvoglioTipo idNuovo) override;
    void setNome(const char* nomeNuovo) override;
    void setIdLocomotiva1(IdLocomotivaTipo idLocomotiva) override;
    void setIdLocomotiva2(IdLocomotivaTipo idLocomotiva) override;
    void setPresenteSulTracciato(bool presenteSulTracciato) override;
    void setLuciAccese(bool luciAcceseNuovo) override;
    void setDifferenzaVelocitaLocomotivaTrazioneSpinta(
        TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta tipologiaDifferenzaVelocita) override;
    void setDifferenzaStepVelocitaTraLocomotive(StepVelocitaKmHTipo differenzaStepVelocita) override;
    void setVagoneId(IdVagoneTipo idVagone, IdVagoneTipo idVagoneDaSettare) override;

   private:
    LunghezzaRotabileTipo calcoloLunghezzaCm() override;
    void resetVagoni();
};

#endif
