#ifndef DCC_COMMAND_STATION_SCAMBIRELAY_H
#define DCC_COMMAND_STATION_SCAMBIRELAY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/scambio/ScambioRelayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/scambi/numero/NumeroScambi.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

// *** CLASSE *** //

class ScambiRelayAbstract {
    // *** DICHIARAZIONE VARIABILI *** //

    ScambioRelayAbstract** scambi = new ScambioRelayAbstract*[NUMERO_SCAMBI];

    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    ScambiRelayAbstract(LoggerAbstract& logger) : logger{logger} {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void reset();

    void azionoScambio(int numeroScambioCodificato);
    void azionoScambioSeNecessario(int numeroScambioCodificato);

    bool esisteNumeroScambio(byte numeroScambio);
    char* toString();

    ScambioRelayAbstract* getScambio(int id);
};

#endif
