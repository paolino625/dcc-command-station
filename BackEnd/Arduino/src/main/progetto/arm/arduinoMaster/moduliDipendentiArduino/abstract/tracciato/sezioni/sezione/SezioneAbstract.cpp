// *** INCLUDE *** //

#include "SezioneAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"

// *** DEFINIZIONE METODI *** //

bool SezioneAbstract::hasSensore() {
    if (getNumeroSensori() > 0) {
        return true;
    } else {
        return false;
    }
}

bool SezioneAbstract::hasDueSensori() const {
    if (sensoreId2 != 0) {
        return true;
    } else {
        return false;
    }
}

bool SezioneAbstract::hasInversioneDiPolarita() const { return inversioneDiPolarita; }

int SezioneAbstract::getNumeroSensori() const {
    if (sensoreId1 != 0 && sensoreId2 != 0) {
        return 2;
    } else if (sensoreId1 != 0) {
        return 1;
    } else {
        return 0;
    }
}

IdSezioneTipo SezioneAbstract::getId() { return idSezione; }
LunghezzaTracciatoTipo SezioneAbstract::getLunghezzaCm() const { return lunghezzaCm; }
void SezioneAbstract::setLunghezzaCm(LunghezzaTracciatoTipo lunghezzaCmLocale) {
    this->lunghezzaCm = lunghezzaCmLocale;
}
TipologiaSezioneCritica SezioneAbstract::getTipologiaSezioneCritica() { return tipologiaSezioneCritica; }
IdSensorePosizioneTipo SezioneAbstract::getSensoreId1() const { return sensoreId1; }
IdSensorePosizioneTipo SezioneAbstract::getSensoreId2() const { return sensoreId2; }
LunghezzaTracciatoTipo SezioneAbstract::getDistanzaDaSensoreId1AFineSezioneSinistra() const {
    return distanzaDaSensoreId1AFineSezioneSinistra;
}
LunghezzaTracciatoTipo SezioneAbstract::getDistanzaDaSensoreId1AFineSezioneDestra() const {
    return distanzaDaSensoreId1AFineSezioneDestra;
}
LunghezzaTracciatoTipo SezioneAbstract::getDistanzaDaSensoreId2AFineSezioneSinistra() const {
    return distanzaDaSensoreId2AFineSezioneSinistra;
}
LunghezzaTracciatoTipo SezioneAbstract::getDistanzaDaSensoreId2AFineSezioneDestra() const {
    return distanzaDaSensoreId2AFineSezioneDestra;
}

StatoSezione SezioneAbstract::getStatoSezione() const { return stato; }

LunghezzaTracciatoTipo SezioneAbstract::getDistanzaDaSensoreAFineSezioneCm(bool direzioneDestra, int numeroSensore) {
    if (numeroSensore != 1 && numeroSensore != 2) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Il numero del sensore deve essere 1 o 2.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }
    if (hasDueSensori()) {
        if (direzioneDestra) {
            if (numeroSensore == 1) {
                return distanzaDaSensoreId1AFineSezioneDestra;
            } else if (numeroSensore == 2) {
                return distanzaDaSensoreId2AFineSezioneDestra;
            }
        } else {
            if (numeroSensore == 1) {
                return distanzaDaSensoreId2AFineSezioneSinistra;
            } else if (numeroSensore == 2) {
                return distanzaDaSensoreId1AFineSezioneSinistra;
            }
        }
    } else {
        if (direzioneDestra) {
            return distanzaDaSensoreId1AFineSezioneDestra;
        } else {
            return distanzaDaSensoreId1AFineSezioneSinistra;
        }
    }
}

LunghezzaTracciatoTipo SezioneAbstract::getDistanzaDaSensoreAInizioSezioneCm(bool direzioneDestra, int numeroSensore) {
    if (numeroSensore != 1 && numeroSensore != 2) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Il numero del sensore deve essere 1 o 2.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }
    if (hasDueSensori()) {
        if (direzioneDestra) {
            if (numeroSensore == 1) {
                return distanzaDaSensoreId1AFineSezioneSinistra;
            } else if (numeroSensore == 2) {
                return distanzaDaSensoreId2AFineSezioneSinistra;
            }
        } else {
            if (numeroSensore == 1) {
                return distanzaDaSensoreId2AFineSezioneDestra;
            } else if (numeroSensore == 2) {
                return distanzaDaSensoreId1AFineSezioneDestra;
            }
        }
    } else {
        if (direzioneDestra) {
            return distanzaDaSensoreId1AFineSezioneSinistra;
        } else {
            return distanzaDaSensoreId1AFineSezioneDestra;
        }
    }
}

IdSensorePosizioneTipo SezioneAbstract::getSensore1() const { return sensoreId1; }
IdSensorePosizioneTipo SezioneAbstract::getSensore2() const { return sensoreId2; }

// N.B. Non usare questo metodo per effettuare lock/unlock di una sezione ma usare sempre il metodo lock/unlock
// dell'oggetto Sezioni.
bool SezioneAbstract::lock(IdConvoglioTipo idConvoglio) {
    // Se il lock non è stato preso, allora posso prenderlo
    if (!stato.statoLock) {
        stato.statoLock = true;
        stato.idConvoglio = idConvoglio;
        // Se cambio lo stato, devo inviare il nuovo stato della sezione al broker MQTT
        sezioniAggiornamentoMqttRichiesto[indiceArraySezioni] = true;
        return true;
    }
    return false;
}

// N.B. Non usare questo metodo per effettuare lock/unlock di una sezione ma usare sempre il metodo lock/unlock
// dell'oggetto Sezioni.
void SezioneAbstract::unlock() {
    if (stato.statoLock) {
        resetLock();
    } else {
        // Non faccio nulla. Il lock non era stato preso.
    }
}

bool SezioneAbstract::isLocked() const { return stato.statoLock; }

bool SezioneAbstract::isCritica(bool direzioneDestra) {
    if (tipologiaSezioneCritica == TipologiaSezioneCritica::MAI) {
        return false;
    } else if (tipologiaSezioneCritica == TipologiaSezioneCritica::SEMPRE) {
        return true;
    } else if (tipologiaSezioneCritica == TipologiaSezioneCritica::SOLO_DIREZIONE_CONVOGLIO_SINISTRA) {
        if (direzioneDestra) {
            return false;
        } else {
            return true;
        }
    } else if (tipologiaSezioneCritica == TipologiaSezioneCritica::SOLO_DIREZIONE_CONVOGLIO_DESTRA) {
        if (direzioneDestra) {
            return true;
        } else {
            return false;
        }
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Non mi aspettavo di entrare in questo else!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

        programma.ferma(true);
        return false;
    }
}

// Questa funzione deve essere chiamata quando si vuole sapere se una sezione è sufficientemente lunga per ospitare il convoglio nella modalità manuale (non autopilot).
// Non viene infatti considerato il possibile errore dell'autopilot.
// Se si vuole sapere se una sezione è sufficientemente lunga per ospitare il convoglio in modalità autopilot usare la funzione presente in AutopilotConvoglioAbstract.
bool SezioneAbstract::isSezioneLungaASufficienzaPerConvoglio(ConvoglioAbstract * convoglio) const {
    // Se la sezione è lunga 2 metri non vogliamo che un convoglio lungo 2 metri ci possa entrare.
    // Lasciamo un po' di spazio extra per facilitare le manovre e per considerare l'eventuale presenza del paraurti se si tratta di una sezione di una stazione di testa.
    if (convoglio->getLunghezzaCm() >= lunghezzaCm - MARGINE_SEZIONE_LUNGA_A_SUFFICIENZA_MODALITA_MANUALE_CM) {
            return false;
    } else {
            return true;
    }
}

IdSensorePosizioneTipo SezioneAbstract::getSensorePassaggioLocomotiva(bool direzionePrimaLocomotivaDestra,
                                                                      int numeroSensore) {
    if (numeroSensore != 1 && numeroSensore != 2) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Il numero del sensore deve essere 1 o 2.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }

    if (getNumeroSensori() == 0) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                              "Necessario usare la funzione "
                              "hasSensore() per verificare la presenza di sensori.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    } else if (getNumeroSensori() == 1) {
        // Se la sezione presenta un solo sensore restituiamo quello
        return sensoreId1;
    } else {
        // Se la sezione presenta due sensori, significa che si tratta di un binario di una stazione simmetrica.
        // In base alla direzione della locomotiva e in base al numero sensore in input, restituiamo il sensore
        // corretto.
        if (direzionePrimaLocomotivaDestra) {
            // Nel caso della stazione simmetrica, il sensore 2 è sempre quello a destra.
            if (numeroSensore == 1) {
                // Se il convoglio sta entrando nella stazione da sinistra (ovvero ha direzione destra) allora il primo
                // sensore che incontra è il sensoreId1
                return sensoreId1;
            } else {
                return sensoreId2;
            }
        } else {
            if (numeroSensore == 1) {
                // Se il convoglio sta entrando nella stazione da destra (ovvero ha direzione sinistra) allora il primo
                // sensore che incontra è il sensoreId2
                return sensoreId2;
            } else {
                return sensoreId1;
            }
        }
    }

    return 0;  // Utile solo per evitare Warning
}

void SezioneAbstract::setSensore1DistanzaDaSensoreAFineSezioneSinistra(
    IdSensorePosizioneTipo sensoreId1Locale, LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneSinistraLocale) {
    this->sensoreId1 = sensoreId1Locale;
    this->distanzaDaSensoreId1AFineSezioneSinistra = distanzaDaSensoreId1AFineSezioneSinistraLocale;
    // L'utente inserisce solo una misura: noi calcoliamo la distanza fino alla sezione della parte opposta
    this->distanzaDaSensoreId1AFineSezioneDestra = lunghezzaCm - distanzaDaSensoreId1AFineSezioneSinistraLocale;
}

void SezioneAbstract::setSensore1DistanzaDaSensoreAFineSezioneDestra(
    IdSensorePosizioneTipo sensoreId1Locale, LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneDestraLocale) {
    this->sensoreId1 = sensoreId1Locale;
    this->distanzaDaSensoreId1AFineSezioneDestra = distanzaDaSensoreId1AFineSezioneDestraLocale;
    // L'utente inserisce solo una misura: noi calcoliamo la distanza fino alla sezione della parte opposta
    this->distanzaDaSensoreId1AFineSezioneSinistra = lunghezzaCm - distanzaDaSensoreId1AFineSezioneDestraLocale;
}

void SezioneAbstract::setSensore2DistanzaDaSensoreAFineSezioneSinistra(
    IdSensorePosizioneTipo sensoreId2Locale, LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneSinistraLocale) {
    this->sensoreId2 = sensoreId2Locale;
    this->distanzaDaSensoreId2AFineSezioneSinistra = distanzaDaSensoreId2AFineSezioneSinistraLocale;
    this->distanzaDaSensoreId2AFineSezioneDestra = lunghezzaCm - distanzaDaSensoreId2AFineSezioneSinistraLocale;
}

void SezioneAbstract::setSensore2DistanzaDaSensoreAFineSezioneDestra(
    IdSensorePosizioneTipo sensoreId2Locale, LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneDestraLocale) {
    this->sensoreId2 = sensoreId2Locale;
    this->distanzaDaSensoreId2AFineSezioneDestra = distanzaDaSensoreId2AFineSezioneDestraLocale;
    this->distanzaDaSensoreId2AFineSezioneSinistra = lunghezzaCm - distanzaDaSensoreId2AFineSezioneDestraLocale;
}

void SezioneAbstract::resetLock() {
    // Se lo stato del lock è cambiato devo inviare lo stato aggiornato al broker MQTT
    if (stato.statoLock) {
        stato.statoLock = false;
        stato.idConvoglio = -1;
        sezioniAggiornamentoMqttRichiesto[indiceArraySezioni] = true;
    }
}