// *** INCLUDE *** //

#include "RealTimeClock.h"

#include <Arduino.h>
#include <mbed_mktime.h>

#include "EthernetUdp.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

unsigned int localPort = 2390;
const int ntpPacketSize = 48;
byte packetBuffer[ntpPacketSize];
EthernetUDP ethernetUdp;

// *** DEFINIZIONE METODI *** //

RealTimeClock realTimeClock;

// send an NTP request to the time-server at the given address
void RealTimeClock::invioPacchettoNtp(const char *address) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio pacchetto NTP... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    memset(packetBuffer, 0, ntpPacketSize);
    packetBuffer[0] = 0b11100011;  // LI, Version, Mode
    packetBuffer[1] = 0;           // Stratum, or type of clock
    packetBuffer[2] = 6;           // Polling Interval
    packetBuffer[3] = 0xEC;        // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    packetBuffer[12] = 49;
    packetBuffer[13] = 0x4E;
    packetBuffer[14] = 49;
    packetBuffer[15] = 52;

    ethernetUdp.beginPacket(address, 123);  // NTP requests are to port 123
    ethernetUdp.write(packetBuffer, ntpPacketSize);
    ethernetUdp.endPacket();

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio pacchetto NTP... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}
unsigned long RealTimeClock::leggoPacchettoNtp() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Leggo pacchetto NTP...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    bool parsePacket;
    parsePacket = ethernetUdp.parsePacket();
    if (!parsePacket) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR,
                              "Funzione leggoPacchettoNtp() in RealTimeClock. Pacchetto NTP vuoto.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        return 0;
    } else {
        ethernetUdp.read(packetBuffer, ntpPacketSize);
        const unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
        const unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
        const unsigned long secsSince1900 = highWord << 16 | lowWord;
        constexpr unsigned long seventyYears = 2208988800UL;
        unsigned long epoch = secsSince1900 - seventyYears;

        // Aggiungo offset per conversione da UTC a Local Time
        epoch += 3600 * OFFSET_UTC_LOCALTIME;  // 2 ore in secondi
        set_time(epoch);

#if defined(VERBOSE)
        Serial.print("Seconds since Jan 1 1900 = ");
        Serial.println(secsSince1900);

        // now convert NTP time into everyday time:
        Serial.print("Unix time = ");
        // print Unix time:
        Serial.println(epoch);

        // print the hour, minute and second:
        Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
        Serial.print((epoch % 86400L) / 3600);  // print the hour (86400 equals secs per day)
        Serial.print(':');
        if (((epoch % 3600) / 60) < 10) {
            // In the first 10 minutes of each hour, we'll want a leading '0'
            Serial.print('0');
        }
        Serial.print((epoch % 3600) / 60);  // print the minute (3600 equals secs per minute)
        Serial.print(':');
        if ((epoch % 60) < 10) {
            // In the first 10 seconds of each minute, we'll want a leading '0'
            Serial.print('0');
        }
        Serial.println(epoch % 60);  // print the second
#endif

        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Leggo pacchetto NTP... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        return epoch;
    }
}

void RealTimeClock::leggoTempoNtp() {
    ethernetUdp.begin(localPort);

    unsigned long pacchettoNtpRicevuto = 0;
    // Fin tanto che non ricevo un pacchetto NTP valido, ne invio un altro.
    while (pacchettoNtpRicevuto == 0) {
        invioPacchettoNtp(timeServer);
        delay(1000);
        pacchettoNtpRicevuto = leggoPacchettoNtp();
    }
}

String RealTimeClock::getDataEOra() {
    char buffer[32];
    tm t;
    _rtc_localtime(time(nullptr), &t, RTC_FULL_LEAP_YEAR_SUPPORT);
    strftime(buffer, 32, "%d-%m-%Y %k:%M:%S", &t);
    return String(buffer);
}

String RealTimeClock::getData() {
    char buffer[16];
    tm t;
    _rtc_localtime(time(nullptr), &t, RTC_FULL_LEAP_YEAR_SUPPORT);
    strftime(buffer, 16, "%d-%m-%Y", &t);
    return String(buffer);
}

void RealTimeClock::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo clock di sistema...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    loggerReale.cambiaTabulazione(1);
    if(ETHERNET_PRESENTE) {
        ethernetUdp.begin(localPort);

        leggoTempoNtp();
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                              "Clock di sistema: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, getDataEOra().c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    }
    loggerReale.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo clock di sistema... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}