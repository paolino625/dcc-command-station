#ifndef ARDUINO_STOPEMERGENZAABSTRACT_H
#define ARDUINO_STOPEMERGENZAABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoAbstract.h"

// *** CLASSE *** //

class StopEmergenzaAbstract {
    // *** VARIABILI *** //

    InvioStatoTracciatoAbstract &invioStatoTracciatoAbstract;
    LocomotiveAbstract &locomotive;
    ConvogliAbstract &convogli;
    BuzzerAbstract &buzzer;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   public:
    StopEmergenzaAbstract(InvioStatoTracciatoAbstract &invioStatoTracciatoAbstract, LocomotiveAbstract &locomotive,
                          ConvogliAbstract &convogli, BuzzerAbstract &buzzer, LoggerAbstract &logger)
        : invioStatoTracciatoAbstract(invioStatoTracciatoAbstract),
          locomotive(locomotive),
          convogli(convogli),
          buzzer(buzzer),
          logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void aziona(bool fermaAutopilot);
};

#endif
