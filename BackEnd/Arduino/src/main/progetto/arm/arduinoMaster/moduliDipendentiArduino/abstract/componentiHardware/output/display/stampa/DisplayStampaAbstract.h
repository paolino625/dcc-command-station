#ifndef DCC_COMMAND_STATION_DISPLAYSTAMPAABSTRACT_H
#define DCC_COMMAND_STATION_DISPLAYSTAMPAABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/alimentazioni/AlimentazioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriPosizione/reale/SensoriPosizioneAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/DisplayCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/motorShield/MotorShieldAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/utenti/UtentiAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/listaConvogli/ListaConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/reale/ConvoglioAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotiva/LocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/listaLocomotive/ListaLocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/vagoni/listaVagoni/ListaVagoniAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** VARIABILI *** //

// Non sono riuscito a metterle dentro menuPrincipale
extern char* listaIndiceMenu[];
extern int numeroElementiLista;

// *** CLASSE *** //

class DisplayStampaAbstract {
   public:
    class Errori {
       public:
        class ComponenteHardwareDisconnesso {
            DisplayCoreAbstract& displayCore;

           public:
            ComponenteHardwareDisconnesso(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {};

            void display();
            void ina219();
            void ina260();
            void amplificatoreAudio();
        };

        class Generico {
            DisplayCoreAbstract& displayCore;

           public:
            Generico(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {};

            void inputNonValido(KeypadCoreAbstract& keypadCore);
            void fermoProgramma();
            void inizializzazionePortaSeriale(int numeroPortaSeriale);
        };

        class Usb {
            DisplayCoreAbstract& displayCore;

           public:
            Usb(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {};
            void inizializzazione();
            void fileInesistente(char* nomeFile);
            void numeroLocomotiveNonCombacia();
            void numeroScambiNonCombacia();
        };

        class Mqtt {
            DisplayCoreAbstract& displayCore;

           public:
            Mqtt(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {};

            void connessioneNonRiuscita();
            void connessionePersa();
        };

        class Ethernet {
            DisplayCoreAbstract& displayCore;

           public:
            Ethernet(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {};

            void connessioneNonRiuscita();
            void connessionePersa();
        };

        class ArduinoMasterHelper {
            DisplayCoreAbstract& displayCore;

           public:
            ArduinoMasterHelper(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {};

            void connessioneNonRiuscita();
        };

       public:
        class Inizializzazione {
           public:
            DisplayCoreAbstract& displayCore;

            Inizializzazione(DisplayCoreAbstract& display) : displayCore(display) {}

            void amplificatoreAudio();
            void ina219();
            void ina260();
            void sensoreImpronte();
        };
    };

    class Monitoraggio {
        DisplayCoreAbstract& displayCore;

       public:
        Monitoraggio(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

        void mostra();

       public:
        class Assorbimenti {
            DisplayCoreAbstract& displayCore;

           public:
            Assorbimenti(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void parteFissa();
            void parteVariabile(int correnteIna219, int correnteIna260);
        };

        class Tensioni {
#define INTERVALLO_STAMPA_MONITORAGGIO_TENSIONI 1000

            DisplayCoreAbstract& displayCore;
            AlimentazioneAbstract& alimentazioneCDU;
            AlimentazioneAbstract& alimentazioneTracciato;
            AlimentazioneAbstract& alimentazione12V;
            AlimentazioneAbstract& alimentazioneSensoriPosizione;

            AlimentazioneAbstract& alimentazioneArduino;
            AlimentazioneAbstract& alimentazioneComponentiAusiliari5V;
            AlimentazioneAbstract& alimentazioneComponentiAusiliari3V;
            AlimentazioneAbstract& alimentazioneLED1;

           public:
            Tensioni(DisplayCoreAbstract& displayCore, AlimentazioneAbstract& alimentazioneCDU,
                     AlimentazioneAbstract& alimentazioneTracciato, AlimentazioneAbstract& alimentazione12V,
                     AlimentazioneAbstract& alimentazioneSensoriPosizione, AlimentazioneAbstract& alimentazioneArduino,
                     AlimentazioneAbstract& alimentazioneComponentiAusiliari5V,
                     AlimentazioneAbstract& alimentazioneComponentiAusiliari3V,
                     AlimentazioneAbstract& alimentazioneLED1)
                : displayCore(displayCore),
                  alimentazioneCDU(alimentazioneCDU),
                  alimentazioneTracciato(alimentazioneTracciato),
                  alimentazione12V(alimentazione12V),
                  alimentazioneSensoriPosizione(alimentazioneSensoriPosizione),
                  alimentazioneArduino(alimentazioneArduino),
                  alimentazioneComponentiAusiliari5V(alimentazioneComponentiAusiliari5V),
                  alimentazioneComponentiAusiliari3V(alimentazioneComponentiAusiliari3V),
                  alimentazioneLED1(alimentazioneLED1) {}

            void displayTensioniParteFissa1();
            void displayTensioniParteVariabile1();

            void displayTensioniParteFissa2();

            void displayTensioniParteVariabile2();
            TimestampTipo millisPrecedenteMonitoraggio;
        };
    };

    class Avvisi {
        DisplayCoreAbstract& displayCore;

       public:
        Avvisi(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

        void effettuaLogIn();
        void inCaricamento();
        void cortoCircuito();
        void credits(DelayAbstract& delay);
    };

    class LogIn {
        DelayAbstract& delay;
        DisplayCoreAbstract& displayCore;
        UtentiAbstract& utenti;

       public:
        LogIn(DelayAbstract& delay, DisplayCoreAbstract& displayCore, UtentiAbstract& utenti)
            : delay(delay), displayCore(displayCore), utenti(utenti) {}

        void sceltaTipologia();
        void digitaPassword();
        void passwordNonValida();
        void appoggiaDito();
        void benvenuto();

        void erroreNumeroUtenti();
    };

    class Convoglio {
       public:
        DisplayCoreAbstract& displayCore;
        LoggerAbstract& logger;

        Convoglio(DisplayCoreAbstract& displayCore, LoggerAbstract& logger)
            : displayCore(displayCore), logger(logger) {}

        // MENU CONVOGLI -> SCELTA IN CORSO
        void mostra();

        void lista(ListaConvogliAbstract& listaConvogli, TempoAbstract& tempo, bool convogliInUso,
                   bool convogliNonInUso, bool mostraStatoConvoglio);
        void erroreNessunConvoglioTrovato();

        class Scelta {
            DisplayCoreAbstract& displayCore;

           public:
            Scelta(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            // MENU CONVOGLI -> GESTISCI -> SCELTA IN CORSO
            void gestioneConvoglio();

            // MENU CONVOGLI -> GESTISCI LOCO -> INSERIMENTO CONVOGLIO
            void inserimentoConvoglio();

            // MENU CONVOGLI -> GESTISCI VAGONI -> AGGIUNGI ELIMINA
            void aggiuntaEliminazioneVagone();

            // MENU CONVOGLI -> GESTISCI PRESENZA -> SCELTA IN CORSO
            void gestionePresenza();

            void gestioneDiffLoco(ConvoglioAbstractReale* convoglio);

            // MENU CONVOGLI -> GESTISCI LOCO -> AGGIUNGI ELIMINA -> ELIMINA
            void eliminazioneLoco();

            void confermaReset();
        };

        class DifferenzaVelocitaLocomotivaTrazioneSpinta {
            DisplayCoreAbstract& displayCore;

           public:
            DifferenzaVelocitaLocomotivaTrazioneSpinta(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void inserisciStep();
        };

        class Vagoni {
            DisplayCoreAbstract& displayCore;
            LoggerAbstract& logger;

           public:
            Vagoni(DisplayCoreAbstract& displayCore, LoggerAbstract& logger)
                : displayCore(displayCore), logger(logger) {}

            void displayVagoniConvoglio(int convoglioCorrente);
            void displayErroreNessunVagoneTrovato();
            void lista(TempoAbstract& tempo, bool vagoniInUso, bool vagoniNonInUso, ConvoglioAbstractReale* convoglio,
                       ListaVagoniAbstract& listaVagoni);
        };
    };

    class Locomotiva {
        LoggerAbstract& logger;

       public:
        class Scelta {
            DisplayCoreAbstract& displayCore;

           public:
            Scelta(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}
            void operazioneLocomotiva();
            void indirizzoLoco();
            void idLoco();
            void valoreKeepAlive();
        };

        DisplayCoreAbstract& displayCore;

       public:
        Locomotiva(DisplayCoreAbstract& displayCore, LoggerAbstract& logger)
            : logger(logger), displayCore(displayCore) {}

        void lista(ListaLocomotiveAbstract& listaLocomotive, TempoAbstract& tempo, bool locomotiveInUso,
                   bool locomotiveNonInUso);

        void erroreListaNessunaLocomotivaTrovata();

        class TempoFunzionamentoLocomotive {
            DisplayCoreAbstract& displayCore;

           public:
            TempoFunzionamentoLocomotive(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void menu();
            void schermataPrincipale(KeypadCoreAbstract& keypadCore, LocomotivaAbstract* locomotiva);
        };

        class SetDecoderLocomotiva {
            DisplayCoreAbstract& displayCore;

           public:
            SetDecoderLocomotiva(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void menu();
        };

        class Mapping {
            DisplayCoreAbstract& displayCore;

           public:
            Mapping(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void schermataPrincipale(LocomotivaAbstract* locomotiva, StepVelocitaKmHTipo stepVelocita, int numeroGiro);
            void mappingTerminato(TimestampTipo minutiDurataMapping);
            void posizionamentoLocomotivaSulTracciato();

            class Scelta {
                DisplayCoreAbstract& displayCore;

               public:
                Scelta(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

                void sensore(IdSensorePosizioneTipo idSensore);
                void modalita();
                void step();
                void stepIniziale();
                void stepFinale();
                void modalitaSensori();
            };
        };
    };

    class Homepage {
#define colonnaMenu 0
#define colonnaConvoglio1Loco1 5
#define colonnaConvoglio1Loco2 9
#define colonnaSeparazione 11
#define colonnaConvoglio2Loco1 13
#define colonnaConvoglio2Loco2 17
#define rigaConvoglio1VelocitaImpostata 3
#define rigaConvoglio2VelocitaImpostata 3

        DisplayCoreAbstract& displayCore;

       public:
        IdConvoglioTipo idConvoglioCorrente1Display = 1;
        IdConvoglioTipo idConvoglioCorrente2Display = 2;

        Homepage(DisplayCoreAbstract& displayCore)
            : displayCore(displayCore){}

        void luciConvogli(ConvogliAbstract& convogli);

        void idLocomotiveConvoglio1(ConvogliAbstract& convogli);
        void idLocomotiveConvoglio2(ConvogliAbstract& convogli);

        void velocitaConvoglio1(ConvogliAbstract& convogli);
        void velocitaConvoglio2(ConvogliAbstract& convogli);

        void direzioneConvoglio1(ConvogliAbstract& convogli);
        void direzioneConvoglio2(ConvogliAbstract& convogli);

        void luciConvoglio1(ConvogliAbstract& convogli);
        void luciConvoglio2(ConvogliAbstract& convogli);

        void display(ConvogliAbstract& convogli);

        DisplayCoreAbstract& getDisplayCore() { return displayCore; }
        bool displayDaAggiornare = true;

       private:
        void idLocomotiva(byte colonnaConvoglioLoco, ConvoglioAbstractReale* convoglio, byte posizioneLocomotiva);
        void velocitaLocomotiva(byte colonnaConvoglioLoco, byte rigaConvoglioLoco, LocomotivaAbstract* locomotiva);
        void direzioneLocomotiva(byte colonnaConvoglioLoco, ConvoglioAbstractReale* convoglio,
                                 byte posizioneLocomotiva);

        void idLocomotiveConvoglio(ConvoglioAbstractReale* convoglio, byte colonnaConvoglioLoco1,
                                   byte colonnaConvoglioLoco2);
        void velocitaConvoglio(ConvoglioAbstractReale* convoglio, byte colonnaConvoglioLoco1,
                               byte colonnaConvoglioLoco2, byte rigaConvoglioVelocitaImpostata);
        void direzioneConvoglio(ConvoglioAbstractReale* convoglio, byte colonnaConvoglioLoco1,
                                byte colonnaConvoglioLoco2);
        void luciConvoglio(ConvoglioAbstractReale* convoglio, byte colonnaConvoglioLoco1, byte colonnaConvoglioLoco2);

        void velocitaLuciDirezioneConvoglio(ConvoglioAbstractReale* convoglio, byte colonnaConvoglioLoco1,
                                            byte colonnaConvoglioLoco2, byte rigaConvoglioVelocitaImpostata);
    };

    class Tracciato {
        DisplayCoreAbstract& displayCore;

       public:
        Tracciato(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

        void mostra(MotorShieldAbstract& motorShield);

       public:
        class Scambi {
            DisplayCoreAbstract& displayCore;

           public:
            Scambi(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void menu();

            void reset();
            void cambia();
        };

        class SensoriPosizione {
            DisplayCoreAbstract& displayCore;

           public:
            SensoriPosizione(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void displayMenuSensoriIr();

            void inserisciSensore();
            void parteFissa();
            void parteVariabile(DelayAbstract& delay, SensoriPosizioneAbstractReale& sensoriPosizione);
        };
    };

    class Accessori {
        DisplayCoreAbstract& displayCore;

       public:
        Accessori(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

        void displayMenuAccessori();
        class Led {
            DisplayCoreAbstract& displayCore;

           public:
            Led(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void menu();
        };

        class Audio {
            DisplayCoreAbstract& displayCore;

           public:
            Audio(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void inserimentoVolume();

            void menu();
        };

        class Buzzer {
            DisplayCoreAbstract& displayCore;

           public:
            Buzzer(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

            void menu();
        };

        class SensoreImpronte {
            DisplayCoreAbstract& displayCore;
            UtentiAbstract& utenti;

           public:
            SensoreImpronte(DisplayCoreAbstract& displayCore, UtentiAbstract& utenti)
                : displayCore(displayCore), utenti(utenti) {}

            void confermaResetDatabase();

            void salvataggioNuovaImpronta();
            void menu();
        };
    };

    class WebServer {
        DisplayCoreAbstract& displayCore;

       public:
        WebServer(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

        void ip();
        void menu();
    };

    class MenuPrincipale {
        DisplayCoreAbstract& displayCore;

       public:
        MenuPrincipale(DisplayCoreAbstract& displayCore) : displayCore(displayCore) {}

        void mostra(TempoAbstract& tempo);

        static void resettoPosizione();
    };
};

#endif