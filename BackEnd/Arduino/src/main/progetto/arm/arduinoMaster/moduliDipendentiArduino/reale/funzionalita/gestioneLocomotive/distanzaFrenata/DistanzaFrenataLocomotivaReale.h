#ifndef ARDUINO_DISTANZAFRENATALOCOMOTIVAREALE_H
#define ARDUINO_DISTANZAFRENATALOCOMOTIVAREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/distanzaFrenata/DistanzaFrenataLocomotivaAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern DistanzaFrenataLocomotivaAbstract distanzaFrenataLocomotivaReale;

#endif
