// *** INCLUDE *** //

#include "MqttArduinoMasterReale.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/autopilot/AutopilotDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/convoglio/ConvoglioDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/locomotiva/LocomotivaDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/scambio/ScambioDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/sensorePosizione/SensorePosizioneDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/dto/factory/sezione/SezioneDTOFactory.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/sezioni/SezioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/autopilot/InfoAutopilotResponseDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/convoglio/ConvoglioResponseDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/locomotiva/LocomotivaResponseDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/sensorePosizione/SensorePosizioneDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/reale/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmReale.h"
#include "main/progetto/arm/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

void MqttArduinoMasterReale::inizializzaVariabiliTriggerInvioMqtt() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Resetto variabili trigger invio MQTT... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);

    /*
    È necessario inviare lo stato iniziale degli oggetti affinché ArduinoMasterHelper sia allineato e possa gestire
    correttamente le richieste GET.
    */

    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        locomotiveAggiornamentoMqttRichiesto[i] = true;
    }

    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        convogliAggiornamentoMqttRichiesto[i] = true;
    }

    for (int i = 1; i < NUMERO_SCAMBI; i++) {
        scambiAggiornamentoMqttRichiesto[i] = true;
    }

    for (int i = 1; i < sezioniReali.numeroSezioni + 1; i++) {
        sezioniAggiornamentoMqttRichiesto[i] = true;
    }

    for (int i = 1; i < NUMERO_SENSORI_POSIZIONE_TOTALI; i++) {
        sensoriPosizioneAggiornamentoMqttRichiesto[i] = true;
    }

    infoAutopilotAggiornamentoMqttRichiesto = true;

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Resetto variabili trigger invio MQTT... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// Controllo le variabili flag per capire quali oggetti sono stati aggiornati che devo inviare al broker MQTT
void MqttArduinoMasterReale::inviaStatoOggettiAggiornati() {
    if (MQTT_ATTIVO) {
        invioStatoLocomotiveAggiornate();
        invioStatoConvogliAggiornati();
        invioStatoScambiAggiornati();
        invioStatoSensoriPosizioneAggiornati();
        invioStatoSezioniAggiornate();
        invioInfoAutopilotAggiornato();
    }
}

void MqttArduinoMasterReale::inviaStatoLocomotiva(IdLocomotivaTipo idLocomotiva) {
    if (DEBUGGING_MQTT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato locomotiva n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idLocomotiva).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
        logger.cambiaTabulazione(1);
    }

    LocomotivaResponseDTO* locomotivaDTO = locomotivaDTOFactory.creaLocomotivaResponseDTO(idLocomotiva);
    char* stringaJson = locomotivaDTOFactory.converteLocomotivaResponseDTOToStringJson(locomotivaDTO);
    mqttCore.inviaMessaggio(TopicMqtt::LOCOMOTIVA, stringaJson, true);

    // Dealloco
    delete locomotivaDTO;
    // Non devo deallocare stringaJson perchè è un puntatore a un buffer

    if (DEBUGGING_MQTT) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato locomotiva n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idLocomotiva).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void MqttArduinoMasterReale::inviaStatoConvoglio(IdConvoglioTipo idConvoglio) {
    if (DEBUGGING_MQTT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato convoglio n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idConvoglio).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
        logger.cambiaTabulazione(1);
    }

    ConvoglioResponseDTO* convoglioDTO = convoglioDTOFactory.creaConvoglioResponseDTO(idConvoglio);
    char* stringaJson = convoglioDTOFactory.converteConvoglioResponseDTOToStringJson(convoglioDTO);
    mqttCore.inviaMessaggio(TopicMqtt::CONVOGLIO, stringaJson, true);

    // Dealloco
    delete convoglioDTO;
    // Non devo deallocare stringaJson perchè è un puntatore a un buffer

    if (DEBUGGING_MQTT) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato convoglio n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idConvoglio).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void MqttArduinoMasterReale::inviaStatoScambio(IdScambioTipo idScambio) {
    if (DEBUGGING_MQTT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato scambio n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idScambio).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
        logger.cambiaTabulazione(1);
    }

    ScambioResponseDTO* scambioDTO = scambioDTOFactory.creaScambioResponseDTO(idScambio);
    char* stringaJson = scambioDTOFactory.converteScambioResponseDTOToStringJson(scambioDTO);
    mqttCore.inviaMessaggio(TopicMqtt::SCAMBIO, stringaJson, true);

    // Dealloco
    delete scambioDTO;
    // Non devo deallocare stringaJson perchè è un puntatore a un buffer

    if (DEBUGGING_MQTT) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato scambio n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idScambio).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void MqttArduinoMasterReale::inviaStatoSezione(int indiceSezione) {
    if (DEBUGGING_MQTT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato sezione n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(indiceSezione).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
        logger.cambiaTabulazione(1);
    }

    SezioneResponseDTO* sezioneDto = sezioneDTOFactory.creaSezioneResponseDTO(indiceSezione);
    char* stringaJson = sezioneDTOFactory.converteSezioneResponseDTOToStringJson(sezioneDto);
    mqttCore.inviaMessaggio(TopicMqtt::SEZIONE, stringaJson, true);

    // Dealloco
    delete sezioneDto;
    // Non devo deallocare stringaJson perchè è un puntatore a un buffer

    if (DEBUGGING_MQTT) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato sezione n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(indiceSezione).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void MqttArduinoMasterReale::inviaStatoSensorePosizione(IdSensorePosizioneTipo idSensore) {
    if (DEBUGGING_MQTT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato sensore n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idSensore).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
        logger.cambiaTabulazione(1);
    }

    SensorePosizioneDTO* sensorePosizioneDto = sensorePosizioneDTOFactory.creaSensorePosizioneDTO(idSensore);
    char* stringaJson = sensorePosizioneDTOFactory.converteSensorePosizioneDTOToStringJson(sensorePosizioneDto);

    mqttCore.inviaMessaggio(TopicMqtt::SENSORE_POSIZIONE, stringaJson, true);

    // Dealloco
    delete sensorePosizioneDto;
    // Non devo deallocare stringaJson perché è un puntatore a un buffer

    if (DEBUGGING_MQTT) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio stato sensore n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(idSensore).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void MqttArduinoMasterReale::inviaInfoAutopilot() {
    if (DEBUGGING_MQTT) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio info autopilot...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        logger.cambiaTabulazione(1);
    }

    InfoAutopilotResponseDTO* infoAutopilotDTO = AutopilotDTOFactory::creaInfoAutopilotResponseDTO();

    char* stringaJson = AutopilotDTOFactory::converteInfoAutopilotResponseDTOToStringJson(infoAutopilotDTO);

    mqttCore.inviaMessaggio(TopicMqtt::INFO_AUTOPILOT, stringaJson, true);

    // Dealloco
    delete infoAutopilotDTO;
    // Non devo deallocare stringaJson perchè è un puntatore a un buffer

    if (DEBUGGING_MQTT) {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Invio info autopilot... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    }
}

void MqttArduinoMasterReale::invioStatoLocomotiveAggiornate() {
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        bool condizione = locomotiveAggiornamentoMqttRichiesto[i];
        if (condizione) {
            inviaStatoLocomotiva(i);

            locomotiveAggiornamentoMqttRichiesto[i] = false;
        }
    }
}
void MqttArduinoMasterReale::invioStatoConvogliAggiornati() {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        bool condizione = convogliAggiornamentoMqttRichiesto[i];
        ConvoglioAbstractReale* convoglio = convogliReali.getConvoglio(i, true);
        if (condizione && convoglio->isInUso()) {
            inviaStatoConvoglio(i);

            convogliAggiornamentoMqttRichiesto[i] = false;
        }
    }
}

void MqttArduinoMasterReale::invioStatoScambiAggiornati() {
    for (int i = 1; i < NUMERO_SCAMBI; i++) {
        bool condizione = scambiAggiornamentoMqttRichiesto[i];
        if (condizione) {
            inviaStatoScambio(i);

            scambiAggiornamentoMqttRichiesto[i] = false;
        }
    }
}

void MqttArduinoMasterReale::invioStatoSensoriPosizioneAggiornati() {
    for (int i = 1; i < NUMERO_SENSORI_POSIZIONE_TOTALI; i++) {
        bool condizione = sensoriPosizioneAggiornamentoMqttRichiesto[i];
        if (condizione) {
            inviaStatoSensorePosizione(i);

            sensoriPosizioneAggiornamentoMqttRichiesto[i] = false;
        }
    }
}

void MqttArduinoMasterReale::invioStatoSezioniAggiornate() {
    for (int i = 1; i < sezioniReali.numeroSezioni + 1; i++) {
        bool condizione = sezioniAggiornamentoMqttRichiesto[i];
        if (condizione) {
            inviaStatoSezione(i);

            sezioniAggiornamentoMqttRichiesto[i] = false;
        }
    }
}

void MqttArduinoMasterReale::invioInfoAutopilotAggiornato() {
    bool condizione = infoAutopilotAggiornamentoMqttRichiesto;
    if (condizione) {
        inviaInfoAutopilot();

        infoAutopilotAggiornamentoMqttRichiesto = false;
    }
}

void MqttArduinoMasterReale::invioTriggerArduinoReady() {
    if (MQTT_ATTIVO) {
        const char* message = R"({"info": "ready"})";   // Il payload non è importante
        char* payload = new char[strlen(message) + 1];  // +1 per il carattere null
        strcpy(payload, message);
        mqttCore.inviaMessaggio(TopicMqtt::ARDUINO_READY, payload, true);
        delete[] payload;
    }
}

void MqttArduinoMasterReale::inviaMessaggioLog(char* payload) {
    // Imposto loggerAttivo a false, altrimenti si creerebbe una dipendenza circolare.
    // Voglio scrivere un log "ARDUINO MASTER", entro in questa funzione ma poi mi accorgo che devo prima pubblicare
    // un messaggio di log, quindi viene richiamata questa funzione e così via, fino all'infinito.
    mqttCore.inviaMessaggio(TopicMqtt::LOG, payload, false);
}

MqttCoreArduinoArmAbstract& MqttArduinoMasterReale::getMqttCore() {
    return mqttCore;
}

// *** DEFINIZIONE VARIABILI *** //

MqttArduinoMasterReale mqttArduinoMasterReale(mqttCoreArduinoArmReale, loggerReale);