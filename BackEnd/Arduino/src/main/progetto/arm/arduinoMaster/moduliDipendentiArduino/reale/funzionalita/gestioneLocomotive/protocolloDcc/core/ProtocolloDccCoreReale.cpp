// *** INCLUDE *** //

#include "ProtocolloDccCoreReale.h"

#include "Arduino.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/codaPacchettiDcc/CodaPacchettiDcc.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/base/superExtended/PacchettoSuperExtended.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/idle/PacchettoIdle.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/reset/PacchettoReset.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/scritturaCv/ScritturaCv.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityCopia.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

// Funzione invocata in timerIsr (funzione invocata a ogni interrupt)
void ProtocolloDccCoreReale::segnaleDcc() {
    int impulsoInviato = 0;

    // Se c'è un bit già in codifica che è in attesa di un altro impulso

    if (codaPacchetti.numeroPulsazioniRimanenti != 0) {
        impulsoInviato = 1;

        if (codaPacchetti.bitCorrente == 1) {
            if (MOTOR_SHIELD_PRESENTE) {
                digitalWrite(PIN_DIR_1, HIGH);
            }

            codaPacchetti.indiceBitCorrente += 1;
            codaPacchetti.numeroPulsazioniRimanenti -= 1;
        }

        else {
            if (codaPacchetti.numeroPulsazioniRimanenti != NUMERO_IMPULSI_0 / 2) {
                codaPacchetti.numeroPulsazioniRimanenti -= 1;

                if (codaPacchetti.numeroPulsazioniRimanenti == 1) {
                    codaPacchetti.indiceBitCorrente += 1;
                }
            }

            else {
                if (MOTOR_SHIELD_PRESENTE) {
                    digitalWrite(PIN_DIR_1, HIGH);
                }

                codaPacchetti.numeroPulsazioniRimanenti -= 1;
            }
        }
    }

    // Il pacchetto è stato totalmente inviato. Passiamo alla prossima ripetizione del pacchetto, o,
    // se abbiamo finito al pacchetto successivo. Se non c'è un pacchetto successivo imponiamo
    // pacchetto idle.

    else if (codaPacchetti.indiceBitCorrente == codaPacchetti.pacchetto.numeroBitPacchetto) {
        // Aumento il contatore delle ripetizioni del pacchetto
        codaPacchetti.numeroRipetizioniPacchetto += 1;
        codaPacchetti.indiceBitCorrente = 0;

        if (codaPacchetti.numeroRipetizioniPacchetto == RIPETIZIONI_PACCHETTO_TRACCIATO) {
            codaPacchetti.numeroRipetizioniPacchetto = 0;

            if (codaPacchetti.pacchettoSuccessivoAttivo) {
                // Prendo nuovo pacchetto dalla coda
                copioPacchettoVolatile(&codaPacchetti.pacchettoSuccessivo, &codaPacchetti.pacchetto);

                codaPacchetti.pacchettoSuccessivoAttivo = false;
            }

            else {
                // Non ci sono pacchetti da inviare. Copio pacchetto IDLE.
                copioPacchetto(pacchettoIdle.pacchetto, &codaPacchetti.pacchetto);
            }
        } else {
            // Avvio ripetizione successiva del pacchetto corrente
        }
    }

    // Se non c'è nessun bit in codifica in attesa capiamo il valore del
    // prossimo bit da inviare

    if (!impulsoInviato) {
        if (codaPacchetti.pacchetto.bitPacchetto[codaPacchetti.indiceBitCorrente] == 1) {  // Il bit è 1

            if (MOTOR_SHIELD_PRESENTE) {
                digitalWrite(PIN_DIR_1, LOW);
            }

            codaPacchetti.bitCorrente = 1;
            codaPacchetti.numeroPulsazioniRimanenti = 1;
        }

        // Il bit è 0

        else {
            if (MOTOR_SHIELD_PRESENTE) {
                digitalWrite(PIN_DIR_1, LOW);
            }

            codaPacchetti.bitCorrente = 0;
            codaPacchetti.numeroPulsazioniRimanenti = NUMERO_IMPULSI_0 - 1;
        }
    }
}

void ProtocolloDccCoreReale::invioPacchetto(volatile Pacchetto pacchetto) {
    // Se il pacchetto inviato precedentemente non è stato ancora inviato
    // aspetto.

    while (codaPacchetti.pacchettoSuccessivoAttivo) {
        // busy-waiting
    }

    copioPacchettoVolatile(&pacchetto, &codaPacchetti.pacchettoSuccessivo);
    codaPacchetti.pacchettoSuccessivoAttivo = true;
}

void ProtocolloDccCoreReale::scrivoIndirizzoCv(int indirizzoCv, byte datoDaScrivere) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scrivo indirizzo CV...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO,
                          "Indirizzo CV: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(indirizzoCv).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO,
                          "Dato da scrivere: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(datoDaScrivere).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    logger.cambiaTabulazione(-1);

    // Secondo lo standard bisogna inviare almeno 3 pacchetti reset.
    // [In realtà ne invio più di 3 considerando che ogni pacchetto viene ripetuto 5 volte]

    for (int i = 0; i < 2; i++) {
        invioPacchetto(pacchettoReset.pacchetto);
        // stampo("Inviato pacchetto reset!\n");
    }

    // Secondo lo standard bisogna inviare almeno 5 pacchetti write al CV che si vuole modificare
    // Indirizzo di CVn è n-1: ad esempio indirizzo di CV1 è 0
    compongoPacchettoScritturaCv(&pacchettoSuperExtended.pacchetto, indirizzoCv - 1, datoDaScrivere);
    for (int i = 0; i < 5; i++) {
        invioPacchetto(pacchettoSuperExtended.pacchetto);
    }

    // Secondo lo standard bisogna inviare almeno 6 pacchetti reset

    for (int i = 0; i < 6; i++) {
        invioPacchetto(pacchettoReset.pacchetto);
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scrivo indirizzo CV... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// *** DEFINIZIONE VARIABILI *** //

ProtocolloDccCoreReale protocolloDccCoreReale = ProtocolloDccCoreReale(loggerReale);