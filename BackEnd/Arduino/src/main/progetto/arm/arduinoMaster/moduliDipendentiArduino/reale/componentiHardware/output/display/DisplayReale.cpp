// *** INCLUDE *** //

#include "DisplayReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/alimentazioni/AlimentazioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/stampa/core/DisplayCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/autopilot/autopilotReale/AutopilotReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

DisplayStampaAbstract::Errori::Inizializzazione displayErroriInizializzazione =
    DisplayStampaAbstract::Errori::Inizializzazione(displayCoreReale);
DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso displayErroriComponenteHardwareDisconnesso =
    DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso(displayCoreReale);
DisplayStampaAbstract::Errori::Generico displayErroriGenerici =
    DisplayStampaAbstract::Errori::Generico(displayCoreReale);
DisplayStampaAbstract::Errori::Usb displayErroriUsb = DisplayStampaAbstract::Errori::Usb(displayCoreReale);
DisplayStampaAbstract::Errori::Mqtt displayErroriMqtt = DisplayStampaAbstract::Errori::Mqtt(displayCoreReale);
DisplayStampaAbstract::Errori::Ethernet displayErroriEthernet =
    DisplayStampaAbstract::Errori::Ethernet(displayCoreReale);
DisplayStampaAbstract::Errori::ArduinoMasterHelper displayErroriArduinoMasterHelper =
    DisplayStampaAbstract::Errori::ArduinoMasterHelper(displayCoreReale);

Errori displayErroriReale =
    Errori(displayErroriInizializzazione, displayErroriComponenteHardwareDisconnesso, displayErroriGenerici,
           displayErroriUsb, displayErroriMqtt, displayErroriEthernet, displayErroriArduinoMasterHelper);

DisplayStampaAbstract::Monitoraggio::Assorbimenti displayAssorbimentiReale =
    DisplayStampaAbstract::Monitoraggio::Assorbimenti(displayCoreReale);
DisplayStampaAbstract::Monitoraggio displayMonitoraggioReale = DisplayStampaAbstract::Monitoraggio(displayCoreReale);
DisplayStampaAbstract::Monitoraggio::Tensioni displayTensioniReale = DisplayStampaAbstract::Monitoraggio::Tensioni(
    displayCoreReale, alimentazioneCDUReale, alimentazioneTracciatoReale, alimentazione12VReale,
    alimentazioneSensoriPosizioneReale, alimentazioneArduinoReale, alimentazioneComponentiAusiliari5VReale,
    alimentazioneComponentiAusiliari3VReale, alimentazioneLED1Reale);

Monitoraggio displayMonitoraggioStructReale =
    Monitoraggio(displayAssorbimentiReale, displayMonitoraggioReale, displayTensioniReale);

DisplayStampaAbstract::Convoglio displayConvoglioMenuReale =
    DisplayStampaAbstract::Convoglio(displayCoreReale, loggerReale);
DisplayStampaAbstract::Convoglio::Scelta displayConvoglioSceltaReale =
    DisplayStampaAbstract::Convoglio::Scelta(displayCoreReale);
DisplayStampaAbstract::Convoglio::DifferenzaVelocitaLocomotivaTrazioneSpinta
    displayConvoglioDifferenzaVelocitaLocomotivaTrazioneSpinta =
        DisplayStampaAbstract::Convoglio::DifferenzaVelocitaLocomotivaTrazioneSpinta(displayCoreReale);
DisplayStampaAbstract::Convoglio::Vagoni displayVagoniReale =
    DisplayStampaAbstract::Convoglio::Vagoni(displayCoreReale, loggerReale);

Convoglio displayConvoglioReale =
    Convoglio(displayConvoglioMenuReale, displayConvoglioSceltaReale,
              displayConvoglioDifferenzaVelocitaLocomotivaTrazioneSpinta, displayVagoniReale);

DisplayStampaAbstract::Locomotiva displayMenuLocomotiveReale =
    DisplayStampaAbstract::Locomotiva(displayCoreReale, loggerReale);
DisplayStampaAbstract::Locomotiva::Scelta displayMenuLocomotiveSceltaReale =
    DisplayStampaAbstract::Locomotiva::Scelta(displayCoreReale);
DisplayStampaAbstract::Locomotiva::TempoFunzionamentoLocomotive displayTempoFunzionamentoLocomotiveReale =
    DisplayStampaAbstract::Locomotiva::TempoFunzionamentoLocomotive(displayCoreReale);

DisplayStampaAbstract::Locomotiva::Mapping displayMappingLocomotiva =
    DisplayStampaAbstract::Locomotiva::Mapping(displayCoreReale);
DisplayStampaAbstract::Locomotiva::Mapping::Scelta displayMappingLocomotivaInserimento =
    DisplayStampaAbstract::Locomotiva::Mapping::Scelta(displayCoreReale);
DisplayStampaAbstract::Locomotiva::SetDecoderLocomotiva displaySetDecoderLocomotiva =
    DisplayStampaAbstract::Locomotiva::SetDecoderLocomotiva(displayCoreReale);

Mapping displayMappingLocomotivaStructReale = Mapping(displayMappingLocomotiva, displayMappingLocomotivaInserimento);

Locomotiva displayLocomotivaStructReale =
    Locomotiva(displayMenuLocomotiveReale, displayMenuLocomotiveSceltaReale, displayTempoFunzionamentoLocomotiveReale,
               displaySetDecoderLocomotiva, displayMappingLocomotivaStructReale);

DisplayStampaAbstract::Tracciato displayTracciatoReale = DisplayStampaAbstract::Tracciato(displayCoreReale);
DisplayStampaAbstract::Tracciato::Scambi displayTracciatoScambioReale =
    DisplayStampaAbstract::Tracciato::Scambi(displayCoreReale);
DisplayStampaAbstract::Tracciato::SensoriPosizione displayTracciatoSensoriPosizioneReale =
    DisplayStampaAbstract::Tracciato::SensoriPosizione(displayCoreReale);

Tracciato displayTracciatoStructReale =
    Tracciato(displayTracciatoReale, displayTracciatoScambioReale, displayTracciatoSensoriPosizioneReale);

DisplayStampaAbstract::Accessori displayAccessori = DisplayStampaAbstract::Accessori(displayCoreReale);
DisplayStampaAbstract::Accessori::Led displayAccessoriLed = DisplayStampaAbstract::Accessori::Led(displayCoreReale);
DisplayStampaAbstract::Accessori::Audio displayAccessoriAudio =
    DisplayStampaAbstract::Accessori::Audio(displayCoreReale);
DisplayStampaAbstract::Accessori::SensoreImpronte displayAccessoriSensoreImpronte =
    DisplayStampaAbstract::Accessori::SensoreImpronte(displayCoreReale, utentiReali);
DisplayStampaAbstract::Accessori::Buzzer displayAccessoriBuzzer =
    DisplayStampaAbstract::Accessori::Buzzer(displayCoreReale);

Accessori displayAccessoriStructReale = Accessori(displayAccessori, displayAccessoriLed, displayAccessoriAudio,
                                                  displayAccessoriSensoreImpronte, displayAccessoriBuzzer);

DisplayStampaAbstract::WebServer displayWebServerReale = DisplayStampaAbstract::WebServer(displayCoreReale);

DisplayStampaAbstract::MenuPrincipale displayMenuPrincipaleReale =
    DisplayStampaAbstract::MenuPrincipale(displayCoreReale);

DisplayAbstract displayReale =
    DisplayAbstract(displayCoreReale, displayErroriReale, displayMonitoraggioStructReale, displayConvoglioReale,
                    displayLocomotivaStructReale, displayTracciatoStructReale, displayAccessoriStructReale,
                    displayWebServerReale, displayMenuPrincipaleReale, delayReale, loggerReale);
