// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/stazioni/stazioni/StazioniAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/sezioni/SezioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

StazioniAbstract stazioniReali(programmaReale, sezioniReali, loggerReale);
