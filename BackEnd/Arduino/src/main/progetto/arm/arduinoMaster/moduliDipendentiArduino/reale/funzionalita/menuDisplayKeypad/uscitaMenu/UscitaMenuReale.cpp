// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/menuDisplayKeypad/uscitaMenu/UscitaMenuAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"

// *** DEFINIZIONE VARIABILI *** //

UscitaMenuAbstract uscitaMenuReale = UscitaMenuAbstract(buzzerRealeArduinoMaster, displayReale);