#ifndef DCC_COMMAND_STATION_SENSORIPOSIZIONEABSTRACT_H
#define DCC_COMMAND_STATION_SENSORIPOSIZIONEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensorePosizione/SensorePosizioneAbstract.h"

// +1
// Ci sono fino a 96 sensori IR e poi sensori Block Detector che partono da indice 101.
#define NUMERO_SENSORI_POSIZIONE_TOTALI 107

class SensoriPosizioneAbstract {
    // *** DICHIARAZIONE VARIABILI *** //

   public:
    virtual void inizializza() = 0;
    virtual void reset(boolean stampaLog) = 0;
    virtual void resetInAttesaPassaggioPrimaLocomotivaConvoglio() = 0;
    virtual bool esisteIdSensore(byte idSensore) = 0;

    // *** DICHIARAZIONE METODI *** //

    virtual SensorePosizioneAbstract* getSensorePosizione(int idSensore) = 0;
};
#endif