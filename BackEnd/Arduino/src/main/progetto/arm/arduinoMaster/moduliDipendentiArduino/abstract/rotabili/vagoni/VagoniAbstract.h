#ifndef DCC_COMMAND_STATION_VAGONI_H
#define DCC_COMMAND_STATION_VAGONI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/vagone/VagoneAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

// Inserire il numero di vagoni + 1, perché l'array parte da 1.
#define NUMERO_VAGONI 20
// +1
#define NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO 11

// *** CLASSE *** //

class VagoniAbstract {
    // *** VARIABILI *** //

   private:
    VagoneAbstract** vagoni = new VagoneAbstract*[NUMERO_VAGONI];
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    VagoniAbstract(LoggerAbstract& logger) : logger(logger) {}

    // *** DEFINIZIONE METODI *** //

    void inizializza();
    char* toString();
    VagoneAbstract* getVagone(byte idVagone);
};

#endif