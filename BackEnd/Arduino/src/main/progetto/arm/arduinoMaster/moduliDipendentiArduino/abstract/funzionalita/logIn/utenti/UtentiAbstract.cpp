// *** INCLUDE *** //

#include "UtentiAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/LogInAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/logIn/statoLogIn/StatoLogIn.h"

// *** DEFINIZIONE METODI *** //

void UtentiAbstract::trovoImpostoUtenteConImpronta(int idSensoreImpronte) {
    for (int i = 1; i < NUMERO_UTENTI; i++) {
        Utente utenteAttuale = lista[i];
        for (int r = 0; r < NUMERO_IMPRONTE_PER_UTENTE; r++) {
            if (utenteAttuale.idImpronte[r] == idSensoreImpronte) {
                statoLogIn.indiceUtenteLoggato = i;
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Utente trovato.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::INFO, getUtenteToString(i),
                                       InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true, true);
                return;
            }
        }
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                          "Non ho trovato l'utente anche se la sua "
                          "impronta è stata salvata.",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    programma.ferma(true);
}

bool UtentiAbstract::trovoImpostoUtenteConPassword(int password) {
    for (int i = 1; i < NUMERO_UTENTI; i++) {
        Utente utenteAttuale = lista[i];
        if (utenteAttuale.password == password) {
            statoLogIn.indiceUtenteLoggato = i;
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Utente trovato.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::INFO, getUtenteToString(i),
                                   InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true, true);
            return true;
        }
    }
    return false;
}

char* UtentiAbstract::getUtenteToString(int indiceUtente) {
    std::string stringa;

    Utente utenteAttuale = lista[indiceUtente];

    stringa += "\n";
    stringa += "INFO UTENTE ";
    stringa += std::to_string(indiceUtente);
    stringa += "\n\n";

    stringa += "Nome: ";
    stringa += utenteAttuale.nome;
    stringa += "\n";

    stringa += "Tipologia utente: ";
    stringa += utenteAttuale.tipologiaUtente;
    stringa += "\n";

    stringa += "Sesso: ";
    stringa += utenteAttuale.sesso;
    stringa += "\n";

    stringa += "Numero impronte salvate: ";
    stringa += std::to_string(utenteAttuale.numeroImpronteSalvate);
    stringa += "\n";

    stringa += "ID Impronte:";

    for (int i = 0; i < utenteAttuale.numeroImpronteSalvate; i++) {
        stringa += " ";
        stringa += std::to_string(utenteAttuale.idImpronte[i]);
    }

    stringa += "\n";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente.
char* UtentiAbstract::toString() {
    std::string stringa;

    stringa += "\n";
    stringa += "UTENTI\n";
    for (int i = 1; i < NUMERO_UTENTI; i++) {
        char* stringaUtente = getUtenteToString(i);
        stringa += stringaUtente;
        delete[] stringaUtente;
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}