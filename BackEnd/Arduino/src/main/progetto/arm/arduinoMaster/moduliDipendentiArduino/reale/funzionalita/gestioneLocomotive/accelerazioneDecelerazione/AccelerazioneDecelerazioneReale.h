#ifndef DCC_COMMAND_STATION_ACCELERAZIONEDECELERAZIONEREALE_H
#define DCC_COMMAND_STATION_ACCELERAZIONEDECELERAZIONEREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/accelerazioneDecelerazione/AccelerazioneDecelerazioneAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern AccelerazioneDecelerazioneAbstract accelerazioneDecelerazioneReale;

#endif
