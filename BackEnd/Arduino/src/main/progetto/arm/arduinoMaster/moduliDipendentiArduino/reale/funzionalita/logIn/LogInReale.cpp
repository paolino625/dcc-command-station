// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/LogInAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/KeypadReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoreImpronte/SensoreImpronteReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/logIn/utenti/UtentiReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINIZIONE VARIABILI *** //

LogInAbstract logInReale = LogInAbstract(keypadReale, displayReale, utentiReali, buzzerRealeArduinoMaster,
                                         sensoreImpronteReale, tempoReale, delayReale, loggerReale);