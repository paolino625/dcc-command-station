// *** INCLUDE *** //

#include "ConvoglioAbstractReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/AutopilotAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/displayVariabiliFlagAggiornamento/DisplayVariabiliFlagAggiornamento.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"

// *** DEFINIZIONE METODI *** //

void ConvoglioAbstractReale::resetVagoni() {
    for (int i = 0; i < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; i++) {
        idVagoni[i] = 0;
    }
}

void ConvoglioAbstractReale::aggiungeSecondaLocomotiva(IdLocomotivaTipo idLocomotiva) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiungo locomotiva n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(idLocomotiva).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " al convoglio n° ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    idLocomotiva2 = idLocomotiva;
    getLocomotiva2()->setDirezioneAvantiDisplay(!getLocomotiva1()->getDirezioneAvantiDisplay());

    convogliAggiornamentoMqttRichiesto[id] = true;

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;
}

void ConvoglioAbstractReale::eliminaSecondaLocomotiva() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Elimino seconda loco dal convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    idLocomotiva2 = 0;

    convogliAggiornamentoMqttRichiesto[id] = true;

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;
}

// L'array convogli viene allocato sempre ma non sempre contiene le informazioni di un convoglio.
bool ConvoglioAbstractReale::isInUso() const {
    if (idLocomotiva1 != 0) {
        return true;
    } else {
        return false;
    }
}

bool ConvoglioAbstractReale::hasDoppiaLocomotiva() const {
    if (idLocomotiva1 != 0 && idLocomotiva2 != 0) {
        return true;
    } else {
        return false;
    }
}

void ConvoglioAbstractReale::reset() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    idLocomotiva1 = 0;
    idLocomotiva2 = 0;
    lunghezzaConvoglio = 0;
    presenteSulTracciato = false;
    resetVagoni();

    convogliAggiornamentoMqttRichiesto[id] = true;
}

void ConvoglioAbstractReale::resetPosizione(){
    // Devo aggiornare lo stato della sezione precedentemente occupata dal convoglio, ora non più bloccata.
    IdSezioneTipo idSezionePrecedentementeOccupata = autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata;
    sezioni.unlock(idSezionePrecedentementeOccupata);

    // Resetto la posizione del convoglio
    autopilotConvoglio->posizioneConvoglio.validita = ValiditaPosizioneConvoglio::POSIZIONE_NON_VALIDA;
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(0,0,0);

    fileConvogliDaAggiornare = true;
    convogliAggiornamentoMqttRichiesto[id] = true;

    // Nel caso in cui l'autopilot del convoglio sia stato già inizializzato, devo resettarlo
        autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
                StatoInizializzazioneAutopilotConvoglio::
    PRONTO_INIZIALIZZAZIONE_CONVOGLIO;
}

void ConvoglioAbstractReale::cambiaVelocita(VelocitaRotabileKmHTipo nuovaVelocitaDaImpostare, bool graduale,
                                            StatoAutopilot statoAutopilot) {
    VelocitaRotabileKmHTipo velocitaAttualePrecedente = getVelocitaAttuale();
    // Controllo che la nuova velocità sia una velocità accettabile per il convoglio; altrimenti, imposto la
    // velocità massima del convoglio.
    VelocitaRotabileKmHTipo velocitaMassima = getVelocitaMassima();
    VelocitaRotabileKmHTipo nuovaVelocita;
    if (nuovaVelocitaDaImpostare > velocitaMassima) {
        nuovaVelocita = velocitaMassima;
    } else {
        nuovaVelocita = nuovaVelocitaDaImpostare;
    }

    // Se il convoglio ha doppia locomotiva devo tenere conto della differenza di velocità tra le due locomotive
    if (hasDoppiaLocomotiva()) {
        int velocitaLocomotiveSpinta = 0;
        int velocitaLocomotiveTrazione = 0;

        if (differenzaVelocitaLocomotivaTrazioneSpinta.tipologia == NESSUNA_DIFFERENZA) {
            velocitaLocomotiveSpinta = nuovaVelocita;
            velocitaLocomotiveTrazione = nuovaVelocita;
        } else if (differenzaVelocitaLocomotivaTrazioneSpinta.tipologia == TRAZIONE_PIU_VELOCE) {
            // Se la nuovaVelocita è 1, voglio evitare di settare la velocità
            // dell'altra locomotiva a -1
            if (nuovaVelocita > differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza) {
                velocitaLocomotiveTrazione = nuovaVelocita;
                velocitaLocomotiveSpinta = nuovaVelocita - differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza;
            } else {
                velocitaLocomotiveTrazione = nuovaVelocita;
                velocitaLocomotiveSpinta = nuovaVelocita;
            }
        } else if (differenzaVelocitaLocomotivaTrazioneSpinta.tipologia == SPINTA_PIU_VELOCE) {
            if (nuovaVelocita > differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza) {
                velocitaLocomotiveTrazione = nuovaVelocita - differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza;
                velocitaLocomotiveSpinta = nuovaVelocita;
            } else {
                velocitaLocomotiveTrazione = nuovaVelocita;
                velocitaLocomotiveSpinta = nuovaVelocita;
            }

        } else {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                                  "Non mi aspettavo di entrare in questo else. Deve esserci un caso di "
                                  "enumerazione non contemplato.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            programma.ferma(true);
        }

        LocomotivaAbstract* locomotiva1 = getLocomotiva1();
        LocomotivaAbstract* locomotiva2 = getLocomotiva2();

        if (locomotiva1->getDirezioneAvantiDisplay()) {
            locomotiva1->setVelocitaImpostata(velocitaLocomotiveTrazione);
            if (!graduale) {
                locomotiva1->setVelocitaAttuale(velocitaLocomotiveTrazione);
            }
        } else {
            locomotiva1->setVelocitaImpostata(velocitaLocomotiveSpinta);
            if (!graduale) {
                locomotiva1->setVelocitaAttuale(velocitaLocomotiveSpinta);
            }
        }

        if (locomotiva2->getDirezioneAvantiDisplay()) {
            locomotiva2->setVelocitaImpostata(velocitaLocomotiveTrazione);
            if (!graduale) {
                locomotiva2->setVelocitaAttuale(velocitaLocomotiveTrazione);
            }
        } else {
            locomotiva2->setVelocitaImpostata(velocitaLocomotiveSpinta);
            if (!graduale) {
                locomotiva2->setVelocitaAttuale(velocitaLocomotiveSpinta);
            }
        }

    } else {
        LocomotivaAbstract* locomotiva1 = getLocomotiva1();
        if (locomotiva1->getDirezioneAvantiDisplay()) {
            locomotiva1->setVelocitaImpostata(nuovaVelocita);
            if (!graduale) {
                locomotiva1->setVelocitaAttuale(nuovaVelocita);
            }
        } else {
            locomotiva1->setVelocitaImpostata(nuovaVelocita);
            if (!graduale) {
                locomotiva1->setVelocitaAttuale(nuovaVelocita);
            }
        }
    }
    convogliAggiornamentoMqttRichiesto[id] = true;

    // Aggiorno la velocità dei convogli stampati sul display, nel caso in cui il convoglio sia presente sul tracciato
    velocitaConvogliDisplayDaAggiornare = true;

    /*
    Se la velocità del convoglio viene cambiata quando l'autopilot non è attivo, significa che se era stata
    registrata la sua posizione in passato, è necessario invalidarla.
    Invalidiamo la posizione solo quando il convoglio passa da velocità 0 a 1: altrimenti anche nel caso dello stop
    d'emergenza, in cui viene impostata a 0 la velocità di tutti i convogli, verrebbe persa la posizione del
    convoglio anche se questo non si è mosso.
    */
    if (  // Se l'autopilot non è attivo
        statoAutopilot == StatoAutopilot::NON_ATTIVO &&
        // Se il convoglio da fermo inizia a muoversi
        velocitaAttualePrecedente == 0 && getVelocitaImpostata() != 0 &&
        // Se la posizione del convoglio risulta valida
        autopilotConvoglio->posizioneConvoglio.validita == ValiditaPosizioneConvoglio::POSIZIONE_VALIDA) {
        invalidaPosizioneConvoglio(true);
        // Nel caso in cui l'autopilot del convoglio sia stato già inizializzato, devo resettarlo
        autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
            StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;
    }
}

void ConvoglioAbstractReale::cambiaDirezione(StatoAutopilot statoAutopilot) {
    autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra = !autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra;

    // Spengo luci del convoglio prima di cambiare direzione; altrimenti, la locomotiva in trazione accende subito
    // le luci nella direzione opposta (in spinta), prima di essere spente da Arduino, il che non è bello da vedere.
    LocomotivaAbstract* locomotivaInTrazione = getLocomotivaInTrazione();
    locomotivaInTrazione->spegneLuciBasilari();

    // Devo chiamare getVelocitaImpostata() prima di cambiare direzione della locomotiva; altrimenti, non sono
    // in grado di individuare la vera locomotiva in trazione del convoglio.
    int velocitaPrecedente = getVelocitaImpostata();

    getLocomotiva1()->cambiaDirezione();

    if (hasDoppiaLocomotiva()) {
        getLocomotiva2()->cambiaDirezione();
    }

    // Se una locomotiva sta andando a velocità 50 e cambio direzione, la locomotiva non deve decelerare
    // gradualmente. Deve ripartire da 0 e raggiungere la velocità 50 gradualmente.
    cambiaVelocita(0, false, statoAutopilot);
    cambiaVelocita(velocitaPrecedente, true, statoAutopilot);

    aggiornaLuci();

    // Devo aggiornare la nuova direzione delle locomotive sul file.
    convogliAggiornamentoMqttRichiesto[id] = true;

    direzioneConvogliDisplayDaAggiornare = true;
    luciConvogliDisplayDaAggiornare = true;
}

bool ConvoglioAbstractReale::esisteVagone(byte idVagone) {
    if (idVagoni[idVagone] != 0) {
        return true;
    }

    return false;
}

void ConvoglioAbstractReale::aggiungeVagone(IdVagoneTipo idVagone) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiungo vagone n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(idVagone).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " al convoglio n° ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    for (int i = 1; i < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; i++) {
        // Se è 0, lo slot è libero
        if (idVagoni[i] == 0) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Trovato slot libero n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(i).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

            idVagoni[i] = idVagone;

            // Dopo aver aggiunto il vagone, aggiorno la lunghezza del convoglio
            aggiornaLunghezza();

            // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
            fileConvogliDaAggiornare = true;

            return;
        }
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                          "Non ho trovato spazio per aggiungere nuovo vagone al "
                          "convoglio!",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    programma.ferma(true);
}

void ConvoglioAbstractReale::eliminaVagone(IdVagoneTipo idVagone) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Elimino vagone n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(idVagone).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " dal convoglio n° ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(idVagone).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    for (int i = 1; i < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; i++) {
        if (idVagoni[i] == idVagone) {
            idVagoni[i] = 0;
        }
    }

    // Dopo aver aggiunto il vagone, aggiorno la lunghezza del convoglio
    aggiornaLunghezza();

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;
}

void ConvoglioAbstractReale::inverteLuci() {
    if (luciAccese) {
        setLuciAccese(false);
    } else {
        setLuciAccese(true);
    }
    aggiornaLuci();
    convogliAggiornamentoMqttRichiesto[id] = true;

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;

    luciConvogliDisplayDaAggiornare = true;
}

void ConvoglioAbstractReale::aggiornaLuci() const {
    if (luciAccese) {
        // Se un convoglio ha due locomotive, devo accendere le luci soltanto
        // della locomotiva a trazione
        if (hasDoppiaLocomotiva()) {
            LocomotivaAbstract* locomotivaInSpinta = getLocomotivaInSpinta();
            LocomotivaAbstract* locomotivaInTrazione = getLocomotivaInTrazione();

            locomotivaInSpinta->spegneLuciBasilari();
            locomotivaInTrazione->accendeLuciBasilari();

            locomotivaInSpinta->aggiornaLuciAusiliarie(true);
            locomotivaInTrazione->aggiornaLuciAusiliarie(true);

        } else {
            getLocomotiva1()->accendeLuciBasilari();
            getLocomotiva1()->aggiornaLuciAusiliarie(true);
        }
    } else {
        // Se un convoglio ha due locomotive, devo accendere le luci soltanto
        // della locomotiva a trazione
        if (hasDoppiaLocomotiva()) {
            LocomotivaAbstract* locomotivaInSpinta = getLocomotivaInSpinta();
            LocomotivaAbstract* locomotivaInTrazione = getLocomotivaInTrazione();

            locomotivaInSpinta->spegneLuciBasilari();
            locomotivaInTrazione->spegneLuciBasilari();

            locomotivaInSpinta->aggiornaLuciAusiliarie(false);
            locomotivaInTrazione->aggiornaLuciAusiliarie(false);

        } else {
            getLocomotiva1()->spegneLuciBasilari();
            getLocomotiva1()->aggiornaLuciAusiliarie(false);
        }
    }
}

void ConvoglioAbstractReale::invalidaPosizioneConvoglio(bool sezioneOccupataDalConvoglioDaLiberare) const {
    autopilotConvoglio->posizioneConvoglio.validita = ValiditaPosizioneConvoglio::POSIZIONE_NON_VALIDA;
    fileConvogliDaAggiornare = true;

    if (sezioneOccupataDalConvoglioDaLiberare) {
        IdSezioneTipo idSezioneCorrenteOccupata = autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata;
        sezioni.unlock(idSezioneCorrenteOccupata);
    }
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente con delete[].
char* ConvoglioAbstractReale::toString() {
    std::string stringa;

    stringa += "\nINFO CONVOGLIO\n";
    stringa += "\nID: ";
    stringa += std::to_string(id);
    stringa += "\n";

    stringa += "\nNOME: ";
    stringa += nome;
    stringa += "\n";

    stringa += "ID Locomotiva 1: ";
    stringa += std::to_string(idLocomotiva1);
    stringa += "\n";

    stringa += "ID Locomotiva 2: ";
    stringa += std::to_string(idLocomotiva2);
    stringa += "\n";

    stringa += "Lunghezza convoglio: ";
    stringa += std::to_string(lunghezzaConvoglio);
    stringa += "\n";

    stringa += "Convogli sul tracciato: ";
    stringa += std::to_string(presenteSulTracciato);
    stringa += "\n";

    stringa += "Tipologia differenza velocità locomotiva trazione spinta: ";
    stringa += std::to_string(differenzaVelocitaLocomotivaTrazioneSpinta.tipologia);
    stringa += "\n";

    stringa += "Step differenza velocità tra locomotive convoglio: ";
    stringa += std::to_string(differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza);
    stringa += "\n";

    stringa += "Lista vagoni: ";
    for (int i = 1; i < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; i++) {
        // Stampiamo solo i vagoni inizializzati, ovvero che esistono
        if (esisteVagone(i)) {
            int idVagoneCorrente = idVagoni[i];
            stringa += std::to_string(idVagoneCorrente);
            stringa += " ";
        }
    }

    stringa += "\n";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

void ConvoglioAbstractReale::aggiornaLunghezza() {
    lunghezzaConvoglio = calcoloLunghezzaCm();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno lunghezza convoglio n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    logger.cambiaTabulazione(1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Lunghezza: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(lunghezzaConvoglio).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " cm", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(-1);
}

IdLocomotivaTipo ConvoglioAbstractReale::getLocomotivaPiuVeloce() {
    // Questa funzione è utile considerando che non sempre la locomotiva in trazione è la più veloce.
    // Dipende dalle impostazioni decise dall'utente.

    if (hasDoppiaLocomotiva()) {
        VelocitaRotabileKmHTipo velocitaLocomotiva1 = getLocomotiva1()->getVelocitaImpostata();
        VelocitaRotabileKmHTipo velocitaLocomotiva2 = getLocomotiva2()->getVelocitaImpostata();

        // Ritorno l'id della locomotiva più veloce
        if (velocitaLocomotiva1 > velocitaLocomotiva2) {
            return idLocomotiva1;
        } else {
            return idLocomotiva2;
        }
    } else {
        return idLocomotiva1;
    }
}

LunghezzaRotabileTipo ConvoglioAbstractReale::calcoloLunghezzaCm() {
    LunghezzaRotabileTipo lunghezzaConvoglioTotale = 0;

    //  Sommo la lunghezza delle locomotive e dei vagoni del convoglio.

    LocomotivaAbstract* locomotiva1 = getLocomotiva1();
    lunghezzaConvoglioTotale += locomotiva1->getLunghezzaCm();

    // Se il convoglio ha due locomotive, considero anche la lunghezza della seconda.
    if (hasDoppiaLocomotiva()) {
        LocomotivaAbstract* locomotiva2 = getLocomotiva2();
        lunghezzaConvoglioTotale += locomotiva2->getLunghezzaCm();
    }

    for (int i = 1; i < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; i++) {
        IdVagoneTipo idVagone = idVagoni[i];
        if (idVagone != 0) {
            VagoneAbstract* vagone = vagoni.getVagone(idVagone);
            lunghezzaConvoglioTotale += vagone->getLunghezzaCm();
        }
    }
    return lunghezzaConvoglioTotale;
}

IdConvoglioTipo ConvoglioAbstractReale::getId() const { return id; }

const char* ConvoglioAbstractReale::getNome() const { return nome; }

IdLocomotivaTipo ConvoglioAbstractReale::getIdLocomotiva1() const { return idLocomotiva1; }

IdConvoglioTipo ConvoglioAbstractReale::getIdLocomotiva2() const { return idLocomotiva2; }

LocomotivaAbstract* ConvoglioAbstractReale::getLocomotiva1() const {
    return locomotive.getLocomotiva(idLocomotiva1, true);
}

LocomotivaAbstract* ConvoglioAbstractReale::getLocomotiva2() const {
    return locomotive.getLocomotiva(idLocomotiva2, true);
}

bool ConvoglioAbstractReale::isLuciAccese() { return luciAccese; }

IdVagoneTipo ConvoglioAbstractReale::getIdVagone(IdVagoneTipo idVagone) { return idVagoni[idVagone]; }

StepVelocitaKmHTipo ConvoglioAbstractReale::getDifferenzaStepVelocitaTraLocomotive() const {
    return differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza;
}

TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta
ConvoglioAbstractReale::getTipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta() {
    return differenzaVelocitaLocomotivaTrazioneSpinta.tipologia;
}

TimestampTipo ConvoglioAbstractReale::getMillisPrecedenteAggiornamentoVelocitaAttuale() const {
    return millisPrecedenteAggiornamentoVelocitaAttualeConvoglio;
}

VelocitaRotabileKmHTipo ConvoglioAbstractReale::getVelocitaImpostata() const {
    // Ritorno la velocità massima delle locomotive del convoglio; non sempre quella a spinta o trazione è la più
    // veloce.

    int velocitaLocomotiva1 = getLocomotiva1()->getVelocitaImpostata();
    int velocitaLocomotiva2 = 0;

    if (hasDoppiaLocomotiva()) {
        velocitaLocomotiva2 = getLocomotiva2()->getVelocitaImpostata();
    }

    // Ritorno la velocità massima tra le due
    if (velocitaLocomotiva1 > velocitaLocomotiva2) {
        return velocitaLocomotiva1;
    } else {
        return velocitaLocomotiva2;
    }
}

VelocitaRotabileKmHTipo ConvoglioAbstractReale::getVelocitaAttuale() const {
    return getLocomotiva1()->getVelocitaAttuale();
}

LunghezzaRotabileTipo ConvoglioAbstractReale::getLunghezzaCm() const { return lunghezzaConvoglio; }

VelocitaRotabileKmHTipo ConvoglioAbstractReale::getVelocitaMassima() const {
    VelocitaRotabileKmHTipo velocitaMassima;
    if (hasDoppiaLocomotiva()) {
        VelocitaRotabileKmHTipo velocitaLocomotiva1 = getLocomotiva1()->getVelocitaMassimaKmH();
        VelocitaRotabileKmHTipo velocitaLocomotiva2 = getLocomotiva2()->getVelocitaMassimaKmH();

        // Ritorno la velocita minimale tra le due
        if (velocitaLocomotiva1 < velocitaLocomotiva2) {
            velocitaMassima = velocitaLocomotiva1;
        } else {
            velocitaMassima = velocitaLocomotiva2;
        }
    } else {
        velocitaMassima = getLocomotiva1()->getVelocitaMassimaKmH();
    }
    return velocitaMassima;
}

int ConvoglioAbstractReale::getNumeroVagoni() {
    int contatore = 0;
    for (int i = 1; i < NUMERO_MASSIMO_VAGONI_PER_CONVOGLIO; i++) {
        if (idVagoni[i] != 0) {
            contatore += 1;
        }
    }
    return contatore;
}

bool ConvoglioAbstractReale::isPresenteSulTracciato() const { return presenteSulTracciato; }

// *** IMPLEMENTAZIONE DEI METODI GETTER AVANZATI *** //

LocomotivaAbstract* ConvoglioAbstractReale::getLocomotivaInSpinta() const {
    if (!hasDoppiaLocomotiva()) {
        // Se il convoglio comprende una sola locomotiva, ritorno quella
        return getLocomotiva1();
    } else {
        if (getLocomotiva1()->getDirezioneAvantiDisplay()) {
            return getLocomotiva2();
        } else if (getLocomotiva2()->getDirezioneAvantiDisplay()) {
            return getLocomotiva1();
        } else {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                                  "Non mi aspettavo di entrare in questo else: getLocomotivaInSpinta().",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
    // Utile per zittire un warning.
    return nullptr;
}

LocomotivaAbstract* ConvoglioAbstractReale::getLocomotivaInTrazione() const {
    if (!hasDoppiaLocomotiva()) {
        // Se il convoglio comprende una sola locomotiva, ritorno quella
        return getLocomotiva1();
    } else {
        if (getLocomotiva1()->getDirezioneAvantiDisplay()) {
            return getLocomotiva1();

        } else if (getLocomotiva2()->getDirezioneAvantiDisplay()) {
            return getLocomotiva2();
        } else {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG,
                                  "Non mi aspettavo di entrare in questo else: getLocomotivaInTrazione().",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
    return nullptr;
}

PosizioneConvoglio ConvoglioAbstractReale::getPosizione() const {
    return autopilotConvoglio->posizioneConvoglio;
}

// *** IMPLEMENTAZIONE DEI METODI SETTER *** //

void ConvoglioAbstractReale::setId(IdConvoglioTipo idNuovo) { this->id = idNuovo; }

void ConvoglioAbstractReale::setNome(const char* nomeNuovo) { strcpy(nome, nomeNuovo); }

void ConvoglioAbstractReale::setIdLocomotiva1(IdLocomotivaTipo idLocomotiva) {
    this->idLocomotiva1 = idLocomotiva;
    convogliAggiornamentoMqttRichiesto[id] = true;
}

void ConvoglioAbstractReale::setIdLocomotiva2(IdLocomotivaTipo idLocomotiva) {
    this->idLocomotiva2 = idLocomotiva;
    convogliAggiornamentoMqttRichiesto[id] = true;
}

void ConvoglioAbstractReale::setLuciAccese(bool luciAcceseNuovo) {
    this->luciAccese = luciAcceseNuovo;
    aggiornaLuci();
    convogliAggiornamentoMqttRichiesto[id] = true;
    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;
}

void ConvoglioAbstractReale::setPresenteSulTracciato(bool presenteSulTracciato) {
    this->presenteSulTracciato = presenteSulTracciato;
    convogliAggiornamentoMqttRichiesto[id] = true;
    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileConvogliDaAggiornare = true;
}

void ConvoglioAbstractReale::setDifferenzaVelocitaLocomotivaTrazioneSpinta(
    TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta tipologiaDifferenzaVelocita) {
    differenzaVelocitaLocomotivaTrazioneSpinta.tipologia = tipologiaDifferenzaVelocita;
}

void ConvoglioAbstractReale::setDifferenzaStepVelocitaTraLocomotive(StepVelocitaKmHTipo differenzaStepVelocita) {
    differenzaVelocitaLocomotivaTrazioneSpinta.stepDifferenza = differenzaStepVelocita;
}

void ConvoglioAbstractReale::setVagoneId(IdVagoneTipo idVagone, IdVagoneTipo idVagoneDaSettare) {
    idVagoni[idVagone] = idVagoneDaSettare;
}
