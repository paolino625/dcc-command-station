// *** INCLUDE *** //

#include "StatoAutopilotModalitaApproccioCasuale.h"

// *** DEFINIZIONE FUNZIONI *** //

const char *getStatoAutopilotModalitaApproccioCasualeAsAString(
    StatoAutopilotModalitaApproccioCasuale statoAutopilotModalitaApproccioCasualeLocale, LoggerAbstract &logger) {
    switch (statoAutopilotModalitaApproccioCasualeLocale) {
        case StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA:
            return "FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA";
        case StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK:
            return "SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK";
        case StatoAutopilotModalitaApproccioCasuale::
            FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA:
            return "FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_"
                   "CORRENTE_A_STAZIONE_SUCCESSIVA";
        case StatoAutopilotModalitaApproccioCasuale::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO:
            return "FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO";
        case StatoAutopilotModalitaApproccioCasuale::
            FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE:
            return "FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_"
                   "PROSSIMA_"
                   "SEZIONE_CAPIENTE";
        case StatoAutopilotModalitaApproccioCasuale::IN_PARTENZA_STAZIONE:
            return "IN_PARTENZA_STAZIONE";
        case StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE:
            return "IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE";
        case StatoAutopilotModalitaApproccioCasuale::IN_MOVIMENTO:
            return "IN_MOVIMENTO";
        case StatoAutopilotModalitaApproccioCasuale::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI:
            return "FERMO_SEZIONE_ATTESA_LOCK_SEZIONI";
        case StatoAutopilotModalitaApproccioCasuale::ARRIVATO_STAZIONE_DESTINAZIONE:
            return "ARRIVATO_STAZIONE_DESTINAZIONE";
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Enumerazione non mappata nella funzione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
    }
}