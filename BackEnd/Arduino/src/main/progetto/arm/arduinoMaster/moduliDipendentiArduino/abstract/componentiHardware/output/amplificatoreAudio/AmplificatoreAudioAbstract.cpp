// *** INCLUDE *** //

#include "AmplificatoreAudioAbstract.h"

// *** DEFINIZIONE METODI *** //

void AmplificatoreAudioAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo amplificatore audio... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    // In questo caso non c'è una libreria specifica per la gestione dell'amplificatore audio.
    // Per essere sicuri della presenza dell'amplificatore devo fare un controllo manuale sulla sua presenza sul bus I2C
    if (!i2c.isIndirizzoBusI2cAttivo(indirizzoI2C)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non trovo l'amplificatore audio.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(false);
    }
    setVolume(volume);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo amplificatore audio... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

int AmplificatoreAudioAbstract::getIndirizzoI2C() { return indirizzoI2C; }

VolumeTipo AmplificatoreAudioAbstract::getVolume() const { return volume; }

void AmplificatoreAudioAbstract::setVolume(VolumeTipo volumeAmplificatoreDaImpostare) {
    if (volumeAmplificatoreDaImpostare > 63) {
        volume = 63;
    } else if (volumeAmplificatoreDaImpostare < 0) {
        volume = 0;
    } else {
        volume = volumeAmplificatoreDaImpostare;
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Imposto volume a ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(volume).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    wire.beginTransmission(indirizzoI2C);
    wire.write(volume);
}