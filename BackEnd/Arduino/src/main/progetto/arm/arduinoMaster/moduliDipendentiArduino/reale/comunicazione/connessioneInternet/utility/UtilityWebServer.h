#ifndef DCC_COMMAND_STATION_UTILITYWEBSERVER_H
#define DCC_COMMAND_STATION_UTILITYWEBSERVER_H

// *** INCLUDE *** //

#include <Arduino.h>

// *** DICHIARAZIONE FUNZIONI *** //

void stampoMacAddress(byte mac[]);
String convertoIndirizzoIPaStringa(const IPAddress &ipAddress);

#endif
