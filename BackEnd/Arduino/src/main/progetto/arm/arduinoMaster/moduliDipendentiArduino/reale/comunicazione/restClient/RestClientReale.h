#ifndef ARDUINO_RESTCLIENTREALE_H
#define ARDUINO_RESTCLIENTREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/restClient/RestClientAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/restClient/enum/TipologiaRichiestaHttpRestClient.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_MASSIMI_REQUEST_HEADER 700
#define NUMERO_CARATTERI_MASSIMI_REQUEST_BODY 1000
#define NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER 500
#define NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY 5000

// *** CLASSE *** //

class RestClientReale : public RestClientAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    void inviaRichiestaHTTP(TipologiaRichiestaHttpRestClient tipologiaRichiestaHttp, char* indirizzoServer, int porta,
                            char* endPoint, char* requestBody, char* responseHeader, char* responseBody) override;

   private:
    void compongoRequestHeader(TipologiaRichiestaHttpRestClient tipologiaRichiestaHttp, char* indirizzoServer,
                               char* endPoint, char* requestHeader, char* requestBody) override;
    void compongoRequestBody(char* requestBody, char* requestBodyDaInviare) override;
    void invioAlServer(char* requestHeader, char* requestBody) override;
    void leggoRispostaDelServer(char* responseHeader, char* responseBody) override;
    void stampoRequest(char* requestHeader, char* requestBody) override;
    void stampoResponse(char* responseHeader, char* responseBody) override;
};

// *** DICHIARAZIONE VARIABILI *** //

extern RestClientReale restClientReale;

#endif
