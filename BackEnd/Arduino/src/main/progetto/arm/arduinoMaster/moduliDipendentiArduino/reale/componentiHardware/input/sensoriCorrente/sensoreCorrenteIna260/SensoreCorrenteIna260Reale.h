#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA260REALE_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA260REALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/SensoreCorrenteIna260Abstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SensoreCorrenteIna260Abstract sensoreCorrenteIna260Reale;

#endif