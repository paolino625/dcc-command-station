#ifndef DCC_COMMAND_STATION_RIPRODUZIONEAUDIOCORE_H
#define DCC_COMMAND_STATION_RIPRODUZIONEAUDIOCORE_H

// *** INCLUDE *** //

#include "AdvancedDAC.h"
#include "Arduino_AdvancedAnalog.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/NumeroCaratteriPathFile.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define STAMPO_HEADER_FILE_AUDIO 0

#define NUMERO_FILE_AUDIO_CODA 15

// *** DICHIARAZIONE VARIABILI *** //

class RiproduzioneAudioCore {
    AdvancedDAC dac0;
    unsigned long numeroCicli = 0;
    ProgrammaAbstract &programma;
    LoggerAbstract &logger;

    char codaFileAudio[NUMERO_FILE_AUDIO_CODA][NUMERO_CARATTERI_PATH_FILE];
    byte indiceElementoCodaFileAudioInRiproduzione = 0;
    byte indiceElementoCodaFileAudioInSalvataggio = 0;

    // *** COSTRUTTORE *** //

   public:
    RiproduzioneAudioCore(AdvancedDAC dac0, ProgrammaAbstract &programma, LoggerAbstract &logger)
        : dac0(dac0), programma(programma), logger(logger) {}

    // *** DICHIARAZIONE FUNZIONI *** //

    void inizializzaDac(unsigned long frequenza);
    void riproduceFileAudioInCoda();

    char *toString();
    bool isCodaVuota();
    void controllaCoda();
    void aggiungeFileCoda(char pathFile[NUMERO_CARATTERI_PATH_FILE]);
    void eliminaFileCoda();

    bool riproduzioneInCorso = false;
    FILE *fileAudio;
    char fileCorrentementeAperto[NUMERO_CARATTERI_PATH_FILE];
    int sampleSize = 0;
    int samplesCount = 0;
};

// *** DICHIARAZIONE VARIABILI *** //

extern RiproduzioneAudioCore riproduzioneAudioCoreReale;

#endif
