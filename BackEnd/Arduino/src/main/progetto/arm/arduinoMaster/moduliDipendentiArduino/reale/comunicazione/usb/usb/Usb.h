#ifndef DCC_COMMAND_STATION_USB_H
#define DCC_COMMAND_STATION_USB_H

// *** NOTA IMPORTANTE *** //

/*
La USB deve essere formattata in FAT32 con schema MBR.
Alcune chiavette USB, anche se formattate correttamente, non funzionano.
Alcune funzionano solo in lettura e non in scrittura: quando si prova una nuova chiavetta USB, provare ad aggiornare un file (impostare la velocità di un convoglio > 0 e avviare lo stop di emergenza).
*/

// *** INCUDE *** //

#include "USBHostMSD/USBHostMSD.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/AutopilotConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/utenti/UtentiAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/ScambiRelayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/tipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta/TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/NumeroCaratteriPathFile.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"

// N.B. Classe troppo complessa da rendere Abstract (USBHostMSD, mbed::FATFileSystem ecc.)

// *** DEFINE *** //

#define NUMERO_TENTATIVI_INIZIALIZZAZIONE_USB 200

// *** CLASSE *** //

extern mbed::FATFileSystem usbCore;

class Usb {
    // *** VARIABILI *** //

    USBHostMSD msd;

    FILE *file = nullptr;

    String pathUsbBuffer;

    ScambiRelayAbstract &scambiRelay;

    DelayAbstract &delay;

    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   public:
    Usb(ScambiRelayAbstract &scambiRelay, DelayAbstract &delay, LoggerAbstract &logger)
        : scambiRelay(scambiRelay), delay(delay), logger{logger} {}

    // *** METODI PUBBLICI *** //

    void inizializza();
    void resetVariabiliFlagTriggerAggiornamentoFile();
    void aggiornaFile();
    void apreFileTestoLettura(char pathFile[]);
    void apreFileTestoScrittura(char pathFile[], bool cancellaContenuto, bool stampaLogger);
    void chiudeFile(bool stampaLogger);
    void apreLeggeChiudeFileTesto(char pathFile[], char *localBuffer, unsigned long numeroCaratteriBuffer);
    void apreFileAudioLettura(char pathFile[]);
    void apreFileAudio(char pathFile[NUMERO_CARATTERI_PATH_FILE]);
    void leggeFileTesto(char *stringa, unsigned int numeroCaratteriBuffer);
    void leggeInfoScambi();
    void leggeInfoLocomotive();
    void leggeInfoVagoni();
    void leggeInfoConvogli();
    void leggeInfoUtenti();
    void leggeFile();
    TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta
    getEnumerazioneDifferenzaVelocitaLocomotivaTrazioneSpintaDaStringa(char *stringa);
    ValiditaPosizioneConvoglio getEnumerazionePosizioneConvoglioDaStringa(char *stringa);
    void scriveInfoScambi();
    void scriveInfoLocomotive();
    void scriveInfoConvogli();
    void scriveInfoUtenti(UtentiAbstract &utenti);
    void setStringaEnumerazioneDifferenzaVelocitaLocomotiva(char *stringaSalvataggio,
                                                            TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta valore);
    void setStringaEnumerazioneValiditaPosizioneConvoglio(char *stringaSalvataggio, ValiditaPosizioneConvoglio valore);
    void scrivoFileTesto(char *stringa, unsigned int numeroCaratteri);
    void copioDaUsbA(String stringaSalvataggio);
    FILE *getFile();
    const char *getNome();
    void setFile(FILE *file);
};

// *** DICIHIARAZIONE VARIABILI *** //

extern Usb usb;

#endif
