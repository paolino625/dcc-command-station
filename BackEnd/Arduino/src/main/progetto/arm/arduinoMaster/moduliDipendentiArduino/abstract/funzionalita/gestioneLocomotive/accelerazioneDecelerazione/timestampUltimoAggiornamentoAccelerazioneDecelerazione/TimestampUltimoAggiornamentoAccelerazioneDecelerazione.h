#ifndef ARDUINO_TIMESTAMPULTIMOAGGIORNAMENTOACCELERAZIONEDECELERAZIONE_H
#define ARDUINO_TIMESTAMPULTIMOAGGIORNAMENTOACCELERAZIONEDECELERAZIONE_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DICHIARAZIONE VARIABILI *** //

extern TimestampTipo timestampUltimoAggiornamentoAccelerazioneDecelerazione;

#endif
