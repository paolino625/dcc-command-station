#ifndef DCC_COMMAND_STATION_LISTE_H
#define DCC_COMMAND_STATION_LISTE_H

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** DICHIARAZIONE FUNZIONI *** //

void liberoLista(char *lista[], byte numeroElementiLista);
void stampoLista(char *arrayStringhe[], byte numeroElementiLista);

#endif