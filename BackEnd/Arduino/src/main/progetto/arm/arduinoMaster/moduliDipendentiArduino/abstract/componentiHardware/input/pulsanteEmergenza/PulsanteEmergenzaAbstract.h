#ifndef DCC_COMMAND_STATION_PULSANTEEMERGENZAABSTRACT_H
#define DCC_COMMAND_STATION_PULSANTEEMERGENZAABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/DigitalPinAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class PulsanteEmergenzaAbstract {
    // *** VARIABILI *** //

   private:
    DigitalPinAbstract& pin;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    PulsanteEmergenzaAbstract(DigitalPinAbstract& pin, LoggerAbstract& logger) noexcept : pin{pin}, logger{logger} {}

    // *** DEFINIZIONE METODI *** //

    void inizializza();
    [[nodiscard]] auto isPremuto() const -> bool;
};

#endif