// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/ModalitaAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/StatoAutopilot.h"

// *** DEFINIZIONE VARIABILI *** //

ModalitaAutopilot modalitaAutopilot;
StatoAutopilot statoAutopilot = StatoAutopilot::NON_ATTIVO;