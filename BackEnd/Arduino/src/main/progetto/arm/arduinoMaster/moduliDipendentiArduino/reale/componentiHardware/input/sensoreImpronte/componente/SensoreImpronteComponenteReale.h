#ifndef DCC_COMMAND_STATION_SENSOREIMPRONTECOMPONENTEREALE_H
#define DCC_COMMAND_STATION_SENSOREIMPRONTECOMPONENTEREALE_H

// *** INCLUDE *** //

#include "Adafruit_Fingerprint.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreImpronte/componente/SensoreImpronteComponenteAbstract.h"

// *** CLASSE *** //

class SensoreImpronteComponenteReale : public SensoreImpronteComponenteAbstract {
    // *** VARIABILI *** //

    Adafruit_Fingerprint adafruitFingerprint = Adafruit_Fingerprint(&Serial4);

    // *** DICHIARAZIONE METODI *** //

    void begin() override { adafruitFingerprint.begin(57600); }

    bool verifyPassword() override { return adafruitFingerprint.verifyPassword(); }

    uint8_t getImage() override { return adafruitFingerprint.getImage(); }

    uint8_t image2Tz() override { return adafruitFingerprint.image2Tz(); }

    uint8_t image2Tz(int numero) override { return adafruitFingerprint.image2Tz(numero); }

    uint8_t fingerSearch() override { return adafruitFingerprint.fingerSearch(); }

    uint8_t createModel() override { return adafruitFingerprint.createModel(); }

    uint8_t storeModel(uint16_t id) override { return adafruitFingerprint.storeModel(id); }

    uint8_t emptyDatabase() override { return adafruitFingerprint.emptyDatabase(); }

    uint8_t getTemplateCount() override { return adafruitFingerprint.getTemplateCount(); }

    uint8_t getFingerprintIDVariabile() override { return adafruitFingerprint.fingerID; }

    uint8_t getTemplateCountVariabile() override { return adafruitFingerprint.templateCount; }

    uint8_t getConfidenceVariabile() override { return adafruitFingerprint.confidence; }

    uint8_t LEDcontrol(uint8_t control, uint8_t speed, uint8_t coloridx, uint8_t count = 0) override {
        return adafruitFingerprint.LEDcontrol(control, speed, coloridx, count);
    }
};

extern SensoreImpronteComponenteReale sensoreImpronteComponenteReale;

#endif