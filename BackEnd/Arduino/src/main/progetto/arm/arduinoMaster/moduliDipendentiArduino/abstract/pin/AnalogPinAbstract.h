#ifndef DCC_COMMAND_STATION_ANALOGPINABSTRACT_H
#define DCC_COMMAND_STATION_ANALOGPINABSTRACT_H

// *** CLASSE *** //

class AnalogPinAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    virtual auto legge() const -> int = 0;
    virtual auto scrive(int value) const -> void = 0;
};

#endif
