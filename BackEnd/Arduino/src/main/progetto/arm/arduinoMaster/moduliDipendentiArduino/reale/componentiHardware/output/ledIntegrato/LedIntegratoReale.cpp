// *** INCLUDE *** //

#include "LedIntegratoReale.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

DigitalPinReale pinLedIntegratoBlu(LEDB);
DigitalPinReale pinLedIntegratoRosso(LEDR);
DigitalPinReale pinLedIntegratoVerde(LEDG);

LedIntegratoAbstract ledIntegratoReale(pinLedIntegratoBlu, pinLedIntegratoRosso, pinLedIntegratoVerde, delayReale,
                                       loggerReale);