#ifndef DCC_COMMAND_STATION_ALIMENTAZIONIREALI_H
#define DCC_COMMAND_STATION_ALIMENTAZIONIREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/alimentazioni/AlimentazioneAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern AlimentazioneAbstract alimentazione12VReale;
extern AlimentazioneAbstract alimentazioneLED1Reale;
extern AlimentazioneAbstract alimentazioneLED2Reale;
extern AlimentazioneAbstract alimentazioneLED3Reale;
extern AlimentazioneAbstract alimentazioneCDUReale;
extern AlimentazioneAbstract alimentazioneTracciatoReale;
extern AlimentazioneAbstract alimentazione5VTotaleReale;
extern AlimentazioneAbstract alimentazioneSensoriPosizioneReale;
extern AlimentazioneAbstract alimentazioneArduinoReale;
extern AlimentazioneAbstract alimentazioneComponentiAusiliari5VReale;
extern AlimentazioneAbstract alimentazioneComponentiAusiliari3VReale;

#endif
