#ifndef DCC_COMMAND_STATION_PING_H
#define DCC_COMMAND_STATION_PING_H

// *** INCLUDE *** //

// *** DEFINE *** //

/*
Definisco massimo tempo ammissibile senza ricevere un messaggio da Arduino Slave Relay (utile per garantire validità
interrupt) Lo sketch è continuamente in loop eccetto quando vanno inseriti dei numeri a doppia/tripla cifra: in questo
caso lo sketch si ferma in attesa di un input. Per questo motivo, consideriamo il caso peggiore, ovvero quando vanno
inseriti 3 numeri, e aggiungiamo un offset.
*/
#define ATTESA_MASSIMA_RICEZIONE_PING_ARDUINO_SLAVE_RELAY (INTERVALLO_INVIO_PACCHETTO_ALIVE_ARDUINO_MASTER * 2)

#endif