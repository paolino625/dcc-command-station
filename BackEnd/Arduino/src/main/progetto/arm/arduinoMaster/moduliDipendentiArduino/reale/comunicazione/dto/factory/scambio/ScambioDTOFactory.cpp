// *** INCLUDE *** //

#include "ScambioDTOFactory.h"

#include <string>

#include "ArduinoJson.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/scambi/ScambiRelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
ScambioResponseDTO* ScambioDTOFactory::creaScambioResponseDTO(IdScambioTipo idScambio) {
    if (DEBUGGING_FACTORY) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Creo ScambioResponseDTO con ID: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idScambio).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    ScambioRelayAbstract* scambio = scambiRelayReale.getScambio(idScambio);

    ScambioResponseDTO* scambioResponseDTO = new ScambioResponseDTO();
    scambioResponseDTO->id = idScambio;
    scambioResponseDTO->posizione = scambio->getStatoStringa();
    return scambioResponseDTO;
}

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
char* ScambioDTOFactory::converteScambioResponseDTOToStringJson(ScambioResponseDTO* scambioResponseDTO) {
    JsonDocument bufferJson;
    bufferJson["id"] = scambioResponseDTO->id;
    bufferJson["posizione"] = scambioResponseDTO->posizione;

    serializzoJson(bufferJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_SCAMBIO_STRINGA_JSON, true);
    return stringaJsonUsbBuffer;
}

// *** DEFINIZIONE VARIABILI *** //

ScambioDTOFactory scambioDTOFactory = ScambioDTOFactory();
