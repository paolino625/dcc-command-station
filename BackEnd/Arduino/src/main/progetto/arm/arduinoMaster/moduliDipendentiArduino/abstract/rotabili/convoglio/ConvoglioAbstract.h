#ifndef DCC_COMMAND_STATION_CONVOGLIOABSTRACT_H
#define DCC_COMMAND_STATION_CONVOGLIOABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/StatoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/tipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta/TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotiva/LocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/struct/PosizioneConvoglio.h"

// *** STRUCT *** //

typedef struct DifferenzaVelocitaLocomotivaTrazioneSpinta {
    // Inizializzo con valori di default: in questo modo quando viene stampato un convoglio non inizializzato non
    // vengono stampati valori casuali.
    TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta tipologia = NESSUNA_DIFFERENZA;
    StepVelocitaDecoderTipo stepDifferenza = 0;
} DifferenzaVelocitaLocomotivaTrazioneSpinta;

// *** CLASSE *** //

class ConvoglioAbstract {
    // *** COSTRUTTORE *** //
   public:
    virtual ~ConvoglioAbstract() {}

    // *** DICHIARAZIONE FUNZIONI *** //

    virtual void aggiungeSecondaLocomotiva(IdLocomotivaTipo idLocomotiva) = 0;
    virtual void eliminaSecondaLocomotiva() = 0;
    virtual bool isInUso() const = 0;
    virtual bool hasDoppiaLocomotiva() const = 0;
    virtual void reset() = 0;
    virtual void resetPosizione() = 0;
    virtual void cambiaVelocita(VelocitaRotabileKmHTipo nuovaVelocitaDaImpostare, bool graduale,
                                StatoAutopilot statoAutopilot) = 0;
    virtual void cambiaDirezione(StatoAutopilot statoAutopilot) = 0;
    virtual bool esisteVagone(byte idVagone) = 0;
    virtual void aggiungeVagone(IdVagoneTipo idVagone) = 0;
    virtual void eliminaVagone(IdVagoneTipo idVagone) = 0;
    virtual void inverteLuci() = 0;
    virtual void aggiornaLuci() const = 0;
    virtual void invalidaPosizioneConvoglio(bool sezioneOccupataDalConvoglioDaLiberare) const = 0;
    char* toString();
    virtual void aggiornaLunghezza() = 0;
    virtual IdLocomotivaTipo getLocomotivaPiuVeloce() = 0;

    // *** GETTER *** //

    virtual IdConvoglioTipo getId() const = 0;
    virtual const char* getNome() const = 0;
    virtual IdLocomotivaTipo getIdLocomotiva1() const = 0;
    virtual IdConvoglioTipo getIdLocomotiva2() const = 0;
    virtual LocomotivaAbstract* getLocomotiva1() const = 0;
    virtual LocomotivaAbstract* getLocomotiva2() const = 0;
    virtual bool isLuciAccese() = 0;
    virtual IdVagoneTipo getIdVagone(IdVagoneTipo idVagone) = 0;
    virtual StepVelocitaKmHTipo getDifferenzaStepVelocitaTraLocomotive() const = 0;
    virtual TIPOLOGIA_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta
    getTipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta() = 0;
    virtual TimestampTipo getMillisPrecedenteAggiornamentoVelocitaAttuale() const = 0;
    virtual VelocitaRotabileKmHTipo getVelocitaImpostata() const = 0;
    virtual VelocitaRotabileKmHTipo getVelocitaAttuale() const = 0;
    virtual LunghezzaRotabileTipo getLunghezzaCm() const = 0;
    virtual VelocitaRotabileKmHTipo getVelocitaMassima() const = 0;
    virtual int getNumeroVagoni() = 0;
    virtual PosizioneConvoglio getPosizione() const = 0;
    virtual bool isPresenteSulTracciato() const = 0;
    virtual LocomotivaAbstract* getLocomotivaInSpinta() const = 0;
    virtual LocomotivaAbstract* getLocomotivaInTrazione() const = 0;

    // *** SETTER *** //

    virtual void setId(IdConvoglioTipo idNuovo) = 0;
    virtual void setNome(const char* nomeNuovo) = 0;
    virtual void setIdLocomotiva1(IdLocomotivaTipo idLocomotiva) = 0;
    virtual void setIdLocomotiva2(IdLocomotivaTipo idLocomotiva) = 0;
    virtual void setPresenteSulTracciato(bool sulTracciato) = 0;
    virtual void setLuciAccese(bool luciAcceseNuovo) = 0;
    virtual void setDifferenzaVelocitaLocomotivaTrazioneSpinta(
        TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta tipologiaDifferenzaVelocita) = 0;
    virtual void setDifferenzaStepVelocitaTraLocomotive(StepVelocitaKmHTipo differenzaStepVelocita) = 0;
    virtual void setVagoneId(IdVagoneTipo idVagone, IdVagoneTipo idVagoneDaSettare) = 0;

   protected:
    virtual LunghezzaRotabileTipo calcoloLunghezzaCm() = 0;

   private:
};
#endif