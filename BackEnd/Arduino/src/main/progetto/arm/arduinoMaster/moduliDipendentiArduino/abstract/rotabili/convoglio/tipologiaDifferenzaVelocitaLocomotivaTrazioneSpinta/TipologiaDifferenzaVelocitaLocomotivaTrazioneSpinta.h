#ifndef TIPOLOGIA_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA
#define TIPOLOGIA_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA

// *** ENUMERAZIONI *** //

enum TIPOLOGIA_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA {
    NESSUNA_DIFFERENZA,
    SPINTA_PIU_VELOCE,
    TRAZIONE_PIU_VELOCE
} typedef TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta;

// *** DICHIARAZIONE FUNZIONI *** //

extern const char* getTipologiaDifferenzaVelocitaLocomotivaTrazioneSpintaAsAString(
    TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta tipologia);

#endif
