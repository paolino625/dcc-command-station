#ifndef DCC_COMMAND_STATION_PROGRAMMAABSTRACT_H
#define DCC_COMMAND_STATION_PROGRAMMAABSTRACT_H

// *** CLASSE *** //

class ProgrammaAbstract {
    // *** COSTRUTTORE *** //

   public:
    virtual ~ProgrammaAbstract() {}

    // *** DICHIARAZIONE METODI *** //

    virtual void ferma(bool stampaMessaggioErroreGenerico) = 0;
};

#endif
