#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA260COREABSTRACT_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA260COREABSTRACT_H

// *** CLASSE *** //

class SensoreCorrenteIna260CoreAbstract {
   public:
    virtual bool begin(int indirizzoI2c) = 0;
    virtual float readBusVoltage() = 0;
    virtual float readCurrent() = 0;
};

#endif
