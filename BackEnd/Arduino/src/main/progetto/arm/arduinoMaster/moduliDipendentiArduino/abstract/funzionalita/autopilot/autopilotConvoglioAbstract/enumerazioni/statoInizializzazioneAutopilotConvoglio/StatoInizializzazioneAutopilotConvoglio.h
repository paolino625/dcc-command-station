#ifndef ARDUINO_STATOINIZIALIZZAZIONEAUTOPILOTCONVOGLIO_H
#define ARDUINO_STATOINIZIALIZZAZIONEAUTOPILOTCONVOGLIO_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** ENUMERAZIONI *** //

enum class StatoInizializzazioneAutopilotConvoglio {
    PRONTO_INIZIALIZZAZIONE_CONVOGLIO,
    PRONTO_PER_CONFIGURAZIONE_AUTOPILOT,
    CONFIGURAZIONE_AUTOPILOT_COMPLETATA
};

// *** DICHIARAZIONE FUNZIONI *** //

extern const char *getStatoInizializzazioneAutopilotConvoglioAsAString(
    StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglioLocale, LoggerAbstract &logger);

#endif
