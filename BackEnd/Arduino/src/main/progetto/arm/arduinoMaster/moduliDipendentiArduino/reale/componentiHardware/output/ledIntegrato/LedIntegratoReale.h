#ifndef DCC_COMMAND_STATION_LEDINTEGRATOREALE_H
#define DCC_COMMAND_STATION_LEDINTEGRATOREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/ledIntegrato/LedIntegratoAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"

// *** DICHIARAZIONE VARIABILI *** //

extern DigitalPinReale pinLedIntegratoBlu;
extern DigitalPinReale pinLedIntegratoRosso;
extern DigitalPinReale pinLedIntegratoVerde;

extern LedIntegratoAbstract ledIntegratoReale;

#endif
