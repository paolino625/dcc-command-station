#ifndef DCC_COMMAND_STATION_CONVOGLI_H
#define DCC_COMMAND_STATION_CONVOGLI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/reale/ConvoglioAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/vagone/VagoneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINE *** //

// L'array parte da 1.
#define NUMERO_CONVOGLI NUMERO_LOCOMOTIVE

// *** CLASSE *** //

class ConvogliAbstract {
    // *** COSTRUTTORE *** //
   public:
    virtual ~ConvogliAbstract() {}

    // *** DICHIARAZIONE FUNZIONI *** //

    virtual void inizializza() = 0;
    virtual bool esisteConvoglio(IdConvoglioTipo idConvoglio) = 0;
    virtual void reset() = 0;
    virtual bool isLocomotivaInUso(IdLocomotivaTipo idLocomotiva) = 0;
    virtual bool isVagoneInUso(VagoneAbstract* vagone) = 0;
    virtual void aggiornaLunghezza() = 0;
    virtual void aggiornaLuci() = 0;
    virtual char* toString() = 0;
    virtual ConvoglioAbstractReale* getConvoglio(int id, bool fermaProgramma) = 0;
    virtual ConvoglioAbstractReale* getConvoglioDellaLocomotiva(IdLocomotivaTipo idLocomotiva) = 0;
    virtual void bloccoSezioniOccupate() = 0;
};

#endif