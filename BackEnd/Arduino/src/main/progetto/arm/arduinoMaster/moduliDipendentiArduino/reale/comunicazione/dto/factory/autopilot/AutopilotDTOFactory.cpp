// *** INCLUDE *** //

#include "AutopilotDTOFactory.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
InfoAutopilotResponseDTO* AutopilotDTOFactory::creaInfoAutopilotResponseDTO() {
    if (DEBUGGING_FACTORY) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Creo InfoAutopilotResponseDTO.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    }

    InfoAutopilotResponseDTO* infoAutopilotResponseDTO = new InfoAutopilotResponseDTO();
    infoAutopilotResponseDTO->modalitaAutopilot = modalitaAutopilot;
    infoAutopilotResponseDTO->statoAutopilot = statoAutopilot;
    return infoAutopilotResponseDTO;
}

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
char* AutopilotDTOFactory::converteInfoAutopilotResponseDTOToStringJson(
    InfoAutopilotResponseDTO* infoAutopilotResponseDTO) {
    JsonDocument bufferJson;

    switch (infoAutopilotResponseDTO->modalitaAutopilot) {
        case ModalitaAutopilot::STAZIONE_SUCCESSIVA:
            bufferJson["modalitaAutopilot"] = "STAZIONE_SUCCESSIVA";
            break;

        case ModalitaAutopilot::APPROCCIO_CASUALE_SCELTA_CONVOGLI:
            bufferJson["modalitaAutopilot"] = "APPROCCIO_CASUALE_SCELTA_CONVOGLI";
            break;

        case ModalitaAutopilot::APPROCCIO_CASUALE_TUTTI_CONVOGLI:
            bufferJson["modalitaAutopilot"] = "APPROCCIO_CASUALE_TUTTI_CONVOGLI";
            break;
    }

    switch (infoAutopilotResponseDTO->statoAutopilot) {
        case StatoAutopilot::NON_ATTIVO:
            bufferJson["statoAutopilot"] = "NON_ATTIVO";
            break;

        case StatoAutopilot::TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO:
            bufferJson["statoAutopilot"] = "TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO";
            break;

        case StatoAutopilot::ATTIVO:
            bufferJson["statoAutopilot"] = "ATTIVO";
            break;
    }

    serializzoJson(bufferJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_INFO_AUTOPILOT_STRINGA_JSON, true);
    return stringaJsonUsbBuffer;
}

// *** DEFINIZIONE VARIABILI *** //

AutopilotDTOFactory autopilotDTOFactory;