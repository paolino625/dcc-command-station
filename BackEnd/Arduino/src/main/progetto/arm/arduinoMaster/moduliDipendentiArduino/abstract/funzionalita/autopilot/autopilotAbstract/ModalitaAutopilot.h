#ifndef ARDUINO_MODALITAAUTOPILOT_H
#define ARDUINO_MODALITAAUTOPILOT_H

// *** ENUMERAZIONI *** //

enum class ModalitaAutopilot {
    STAZIONE_SUCCESSIVA,
    APPROCCIO_CASUALE_SCELTA_CONVOGLI,
    APPROCCIO_CASUALE_TUTTI_CONVOGLI
};

#endif
