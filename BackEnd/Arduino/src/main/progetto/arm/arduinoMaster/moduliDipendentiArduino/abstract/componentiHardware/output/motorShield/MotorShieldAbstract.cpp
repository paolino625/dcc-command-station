// *** INCLUDE *** //

#include "MotorShieldAbstract.h"

// *** DEFINIZIONE METODI *** //

void MotorShieldAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo Motor Shield... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    pinPwm.setOutputMode();
    pinDir1.setOutputMode();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo Motor Shield... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MotorShieldAbstract::accende() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Accendo Motor Shield... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    pinPwm.setHigh();
    accesa = true;

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Accendo Motor Shield... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MotorShieldAbstract::spegne() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Spengo Motor Shield... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    pinPwm.setLow();
    accesa = false;

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Spengo Motor Shield... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

bool MotorShieldAbstract::isAccesa() const { return accesa; }