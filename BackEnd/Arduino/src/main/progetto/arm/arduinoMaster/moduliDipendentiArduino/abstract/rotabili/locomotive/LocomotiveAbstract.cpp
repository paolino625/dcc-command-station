// *** INCLUDE *** //

#include "LocomotiveAbstract.h"

#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void LocomotiveAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo locomotive...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LocomotivaAbstract* locomotiva = new LocomotivaAbstract(i, tempo, logger);
        locomotive[i] = locomotiva;
    }
    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo locomotive... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void LocomotiveAbstract::inizializzaLocomotive() {
    // Non posso farlo dentro inizializza perché la funzione inizializza deve essere chiamata prima di leggere i file da
    // USB ma l'inizializzazione delle locomotive deve essere fatta dopo la lettura dei file. Questo in particolare
    // affinché la funzione aggiornaDirezioneAvantiDecoder possa leggere la direzione della locomotiva salvata su File.
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LocomotivaAbstract* locomotiva = new LocomotivaAbstract(i, tempo, logger);
        locomotiva->inizializza();
    }
}

bool LocomotiveAbstract::esisteLocomotiva(IdLocomotivaTipo idLocomotiva) {
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        if (locomotive[i]->getId() == idLocomotiva) {
            return true;
        }
    }
    return false;
}

LocomotivaAbstract* LocomotiveAbstract::getLocomotiva(int id, bool fermaProgramma) {
    if (esisteLocomotiva(id)) {
        return locomotive[id];
    } else {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "La locomotiva con id ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, std::to_string(id).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, " non esiste",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        if (fermaProgramma) {
            programma.ferma(true);
        }
    }
    return nullptr;  // Utile solo per evitare Warning
}

char* LocomotiveAbstract::toString() {
    // Provato a usare allocazione dinamica con char * ma bisogna allocare molta più memoria di quella che è necessaria
    // affinché Arduino non crashi, quindi uso std::string.
    std::string stringa;
    stringa += "\n";
    stringa += "STATO LOCOMOTIVE:\n";
    for (int i = 1; i < NUMERO_LOCOMOTIVE; i++) {
        LocomotivaAbstract* locomotiva = getLocomotiva(i, true);
        char* stringaLocomotiva = locomotiva->toString();
        stringa += stringaLocomotiva;
        delete[] stringaLocomotiva;
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}