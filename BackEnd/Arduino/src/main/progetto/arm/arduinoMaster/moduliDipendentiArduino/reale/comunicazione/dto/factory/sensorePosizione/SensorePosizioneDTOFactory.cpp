// *** INCLUDE *** //

#include "SensorePosizioneDTOFactory.h"

#include <string>

#include "ArduinoJson.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensorePosizione/SensorePosizioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/sensorePosizione/SensorePosizioneDTO.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
SensorePosizioneDTO* SensorePosizioneDTOFactory::creaSensorePosizioneDTO(int idSensore) {
    if (DEBUGGING_FACTORY) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Creo sensore posizione DTO con ID: ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(idSensore).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    SensorePosizioneAbstract* sensorePosizione = sensoriPosizioneReali.getSensorePosizione(idSensore);

    SensorePosizioneDTO* sensorePosizioneResponseDTO = new SensorePosizioneDTO();
    sensorePosizioneResponseDTO->id = sensorePosizione->getId();
    sensorePosizioneResponseDTO->stato = sensorePosizione->getStato();
    sensorePosizioneResponseDTO->idConvoglioInAttesa = sensorePosizione->getIdConvoglioInAttesa();
    return sensorePosizioneResponseDTO;
}

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
char* SensorePosizioneDTOFactory::converteSensorePosizioneDTOToStringJson(SensorePosizioneDTO* sensorePosizioneDTO) {
    JsonDocument bufferJson;

    bufferJson["id"] = sensorePosizioneDTO->id;
    bufferJson["stato"] = sensorePosizioneDTO->stato;
    bufferJson["idConvoglioInAttesa"] = sensorePosizioneDTO->idConvoglioInAttesa;

    serializzoJson(bufferJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_SENSORE_POSIZIONE_STRINGA_JSON, true);

    return stringaJsonUsbBuffer;
}

// *** DEFINIZIONE VARIABILI *** //

SensorePosizioneDTOFactory sensorePosizioneDTOFactory;