#ifndef ARDUINO_VALIDITAPOSIZIONECONVOGLIO_H
#define ARDUINO_VALIDITAPOSIZIONECONVOGLIO_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** ENUMERAZIONI *** //

enum ValiditaPosizioneConvoglio { POSIZIONE_VALIDA, POSIZIONE_NON_VALIDA };

// *** DICHIARAZIONE FUNZIONI *** //

extern const char* getValiditaPosizioneConvoglioAsAString(ValiditaPosizioneConvoglio validitaPosizioneConvoglio,
                                                          LoggerAbstract& logger);
#endif
