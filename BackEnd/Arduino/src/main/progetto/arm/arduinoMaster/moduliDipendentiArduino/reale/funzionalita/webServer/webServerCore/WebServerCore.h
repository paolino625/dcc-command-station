#ifndef DCC_COMMAND_STATION_WEBSERVERCORE_H
#define DCC_COMMAND_STATION_WEBSERVERCORE_H

// *** INCLUDE *** //

#include <Arduino.h>
#include <WiFi.h>

#include "ArduinoJson.h"
#include "EthernetServer.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/AutopilotAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_MASSIMI_REQUEST_HEADER 700
#define NUMERO_CARATTERI_MASSIMI_REQUEST_BODY 300
#define NUMERO_CARATTERI_MASSIMI_RESPONSE_HEADER 100

// Non aumentare troppo perché Arduino non ha memoria a sufficienza e crasha (in particolare quando
// DEBUGGING_WEB_SERVER_RESPONSE = 1)
#define NUMERO_CARATTERI_MASSIMI_RESPONSE_BODY 5000

// *** ENUMERAZIONI *** //

enum TipologiaRichiestaHttpWebServer { WEB_SERVER_OPTIONS, WEB_SERVER_GET, WEB_SERVER_POST, WEB_SERVER_PUT };
enum StatoHTTP {
    OK_200,
    OK_204,
    BAD_REQUEST_400,
    UNAUTHORIZED_401,
    NOT_FOUND_404,
    INTERNAL_SERVER_ERROR_500,
};

enum TipologiaLogResponseBody { TIPOLOGIA_LOG_RESPONSE_BODY_INFO, TIPOLOGIA_LOG_RESPONSE_BODY_ERROR };

// *** DICHIARAZIONE VARIABILI *** //

extern EthernetServer ethernetServer;
extern WiFiServer wifiServer;

// *** CLASSE *** //

class WebServerCore {
   public:
    // *** METODI *** //

    static void inizializza();
    void gestisceClient();

   private:
    static TipologiaRichiestaHttpWebServer getTipologiaRichiestaHTTP(char* requestHeader);
    static StatoHTTP gestiscoRichiestaGET(char* requestHeader, char* responseBody);
    static StatoHTTP gestiscoRichiestaPOST(char* requestHeader, char* requestBody, char* responseBody);
    StatoHTTP gestiscoRichiestaPUT(char* requestHeader, char* requestBody, char* responseBody);
    static void leggoBody(Client* client, char* requestBody);
    static void compongoResponseHTTP200(char* responseHeader);
    static void compongoResponseHTTP204(char* responseHeader);
    static void compongoResponseHTTP400(char* responseHeader);
    static void compongoResponseHTTP401(char* responseHeader);
    static void compongoResponseHTTP404(char* responseHeader);
    static void compongoResponseHTTP500(char* responseHeader);
    static void invioResponseAlClient(Client* client, StatoHTTP statoHttp, char* responseHeader, char* responseBody);

    static int estraiIdDaHeader(char* requestHeader, const char* stringaDaCercare);

    void stampoRequest(TipologiaRichiestaHttpWebServer tipologiaRichiestaHTTP, char* requestHeader, char* requestBody);
    void stampoResponse(char* responseHeader, char* responseBody);
    // StatoHTTP salvoStatoLocomotiva(IdLocomotivaTipo idLocomotiva, char* requestBody, char* responseBody);
    StatoHTTP salvoStatoConvoglio(IdConvoglioTipo idConvoglio, char* requestBody, char* responseBody);
    StatoHTTP salvoStatoScambio(IdScambioTipo idScambio, char* requestBody, char* responseBody);
    static void creoResponseBodyInfoJson(char* responseBody, const char* responseMessage,
                                         TipologiaLogResponseBody tipologiaLog);
    static void creoResponseBodyBooleanResultJson(char* responseBody, bool result);
    static void invioAlClient(Client* client, char* responseBody);
    static bool autentico(char* requestHeader);
    static IdSezioneTipo leggoIdSezioneBody(char* nomeParametroSezione, JsonObject body, char* responseBody);
};

// *** DICHIARAZIONE VARIABILI *** //

extern WebServerCore webServerCore;

#endif