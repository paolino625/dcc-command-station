#ifndef ARDUINO_INFOAUTOPILOT_H
#define ARDUINO_INFOAUTOPILOT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/ModalitaAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/StatoAutopilot.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ModalitaAutopilot modalitaAutopilot;
extern StatoAutopilot statoAutopilot;

#endif
