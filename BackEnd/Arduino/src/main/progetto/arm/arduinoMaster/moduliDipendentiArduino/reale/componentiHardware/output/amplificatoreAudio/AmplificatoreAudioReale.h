#ifndef DCC_COMMAND_STATION_AMPLIFICATOREAUDIOREALE_H
#define DCC_COMMAND_STATION_AMPLIFICATOREAUDIOREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/amplificatoreAudio/AmplificatoreAudioAbstract.h"

// *** DEFINIZIONE VARIABILI *** //

extern AmplificatoreAudioAbstract amplificatoreAudioReale;

#endif
