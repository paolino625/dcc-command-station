// *** INCLUDE *** //

#include "StopEmergenzaReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/autopilot/autopilotReale/AutopilotReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

StopEmergenzaAbstract stopEmergenzaReale = StopEmergenzaAbstract(invioStatoTracciatoReale, locomotiveReali,
                                                                 convogliReali, buzzerRealeArduinoMaster, loggerReale);