// *** INCLUDE *** //

#include "ModificaCvAbstract.h"

// *** DEFINIZIONE METODI *** //

// Moltiplicatore di 0.0328 secondi
void ModificaCvAbstract::tempoLimiteUsoKeepAlive(byte moltiplicatoreTempoUnita) {
    /*
    È possibile modificare il CV113 del decoder per determinare dopo quanto tempo il decoder deve
    spegnersi se riceve l'alimentazione soltanto dai condensatori e non dal tracciato. Bisogna
    valutare attentamente quale sia il valore migliore da impostare: è sconsigliato metterlo troppo
    alto. Questo per il seguente motivo: se viene inviato uno stop di emergenza e la locomotiva non
    fa contatto con il tracciato, la locomotiva si fermerà soltanto quanto farà di nuovo contatto
    con il tracciato o quando i condensatori si scaricano completamente. Sul manuale dei decoder ESU
    viene consigliato di impostare un tempo da 0.3 a 1 secondo. Sul CV113 va indicato il
    moltiplicatore di 0.0328 secondi.
    */

    protocolloDccCore.scrivoIndirizzoCv(113, moltiplicatoreTempoUnita);
}

void ModificaCvAbstract::indirizzoLocomotiva(int nuovoIndirizzoLocomotiva) {
    protocolloDccCore.scrivoIndirizzoCv(1, nuovoIndirizzoLocomotiva);
}

void ModificaCvAbstract::accelerazioneLocomotiva(byte valore) {
    // Nei decoder ESU, il CV 3 contiene un valore da 0 a 255 che, moltiplicato per 0.25, indica i secondi che la locomotiva impiega per andare da 0 alla velocità massima
    protocolloDccCore.scrivoIndirizzoCv(3, valore);
}

void ModificaCvAbstract::decelerazioneLocomotiva(byte valore) {
    // Nei decoder ESU, il CV 4 contiene un valore da 0 a 255 che, moltiplicato per 0.25, indica i secondi che la locomotiva impiega per andare dalla velocità massima a 0
    protocolloDccCore.scrivoIndirizzoCv(4, valore);
}

void ModificaCvAbstract::resetAccelerazioneDecelerazioneLocomotiva() {
    accelerazioneLocomotiva(0);
    decelerazioneLocomotiva(0);
}