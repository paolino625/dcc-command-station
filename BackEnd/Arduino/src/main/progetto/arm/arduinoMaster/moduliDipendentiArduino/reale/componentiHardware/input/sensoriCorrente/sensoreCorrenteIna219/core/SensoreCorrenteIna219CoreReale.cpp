// *** INCLUDE *** //

#include "SensoreCorrenteIna219CoreReale.h"

// *** DEFINIZIONE METODI *** //

// Di default la libreria usa Wire. Dato che sto usando SDA1 e SCL1 di ArduinoGiga, specifico che voglio usare Wire1.
bool SensoreCorrenteIna219CoreReale::begin() { return ina219.begin(&Wire1); }

float SensoreCorrenteIna219CoreReale::getCurrent_mA() { return ina219.getCurrent_mA(); }

float SensoreCorrenteIna219CoreReale::getBusVoltage_V() { return ina219.getBusVoltage_V(); }

float SensoreCorrenteIna219CoreReale::getShuntVoltage_mV() { return ina219.getShuntVoltage_mV(); }

// *** DEFINIZIONE VARIABILI *** //

SensoreCorrenteIna219CoreReale sensoreCorrenteIna219CoreReale = SensoreCorrenteIna219CoreReale();