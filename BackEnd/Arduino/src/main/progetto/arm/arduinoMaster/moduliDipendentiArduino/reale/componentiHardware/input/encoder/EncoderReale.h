#ifndef DCC_COMMAND_STATION_ENCODERREALE_H
#define DCC_COMMAND_STATION_ENCODERREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/encoder/EncoderAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"

// *** DEFINIZIONE VARIABILI *** //

extern DigitalPinReale pinEncoderClk1;
extern DigitalPinReale pinEncoderDt1;
extern DigitalPinReale pinEncoderSw1;

extern DigitalPinReale pinEncoderClk2;
extern DigitalPinReale pinEncoderDt2;
extern DigitalPinReale pinEncoderSw2;

extern EncoderAbstract encoderReale1;
extern EncoderAbstract encoderReale2;

#endif
