#ifndef DCC_COMMAND_STATION_ORARIOPLASTICOABSTRACT_H
#define DCC_COMMAND_STATION_ORARIOPLASTICOABSTRACT_H

// *** INCLUDE ***

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/tempo/TempoAbstract.h"

// *** STRUCT *** //

struct Orario {
    int giornata;
    int ore;
    int minuti;

    friend bool operator==(const Orario& orario1, const Orario& orario2) {
        return orario1.ore == orario2.ore && orario1.minuti == orario2.minuti && orario1.giornata == orario2.giornata;
    }
};

// *** CLASSE *** //

class OrarioPlasticoAbstract {
    // *** VARIABILI *** //

    TempoAbstract& tempo;
    LoggerAbstract& logger;

    // Inizializzo ore e minuti a 0 mentre la giornata ad 1.
    Orario orario{1, 0, 0};

    unsigned long ultimoAggiornamento =
        0;                     // L'ultimo momento in cui l'orario è stato aggiornato. È necessario inizializzarlo a 0.
    int fattoreAccelerazione;  // Il fattore di accelerazione del tempo plastico

    // *** COSTRUTTORE *** //

   public:
    OrarioPlasticoAbstract(int lunghezzaGiornataInMinuti, TempoAbstract& tempo, LoggerAbstract& logger)
        : tempo(tempo), logger(logger) {
        fattoreAccelerazione = calcoloFattoreVelocita(lunghezzaGiornataInMinuti);
    }

    // *** METODI *** //

    void aggiorna();
    char* toString() const;

    Orario getOrario();
    int getGiornata() const;
    int getOre() const;
    int getMinuti() const;

   private:
    static int calcoloFattoreVelocita(int lunghezzaGiornataInMinutiMondoPlastico);
};

#endif
