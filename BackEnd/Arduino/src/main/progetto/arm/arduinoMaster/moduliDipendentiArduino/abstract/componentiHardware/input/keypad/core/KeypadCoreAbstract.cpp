// *** INCLUDE *** //

#include "KeypadCoreAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/KeypadAbstract.h"

// *** DEFINIZIONE VARIABILI *** //

TimestampTipo tempoUltimoTastoPremuto;

// *** DEFINIZIONE METODI *** //

char KeypadCoreAbstract::leggoCarattere() {
    char carattereLetto = '-';
    // Utile per evitare di leggere più di una volta un tasto premuto.
    // Se quindi questa funzione viene chiamata due volte di seguito, la seconda volta si aspetta
    // questo tempo, dando la possibilità all'utente di lasciare il tasto in tempo.

    TimestampTipo tempoCorrente = tempo.milliseconds();
    // Se sono pronto per leggere un altro carattere, tengo in considerazione il carattere appena
    // letto, altrimenti no.
    if (tempoCorrente - tempoUltimoTastoPremuto > DELAY_KEYPAD_TRA_DUE_INPUT) {
        carattereLetto = keypadComponente.leggeCarattere();
        if (carattereLetto != '-') {
            char buffer[2] = {carattereLetto, '\0'};
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                  "Carattere letto: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, buffer,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            tempoUltimoTastoPremuto = tempo.milliseconds();
        }
    }

    return carattereLetto;
}

char KeypadCoreAbstract::leggoCarattereTempoLimite(double tempoLimiteSecondi) {
    double tempoLimiteMilliSecondi = tempoLimiteSecondi * 1000;

    char carattereLetto = '-';

    TimestampTipo tempoInizioFunzione = tempo.milliseconds();

    // Utile per evitare di leggere più di una volta un tasto premuto.
    // Se quindi questa funzione viene chiamata due volte di seguito, la seconda volta si aspetta
    // questo tempo, dando la possibilità all'utente di lasciare il tasto in tempo.

    while (carattereLetto == '-') {
        TimestampTipo tempoCorrente = tempo.milliseconds();
        // Se sono pronto per leggere un altro carattere, tengo in considerazione il carattere
        // appena letto, altrimenti no.
        if (tempoCorrente - tempoUltimoTastoPremuto > DELAY_KEYPAD_TRA_DUE_INPUT) {
            carattereLetto = keypadComponente.leggeCarattere();
            if (carattereLetto != '-') {
                char buffer[2] = {carattereLetto, '\0'};
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                                      "Carattere letto: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, buffer,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                tempoUltimoTastoPremuto = tempo.milliseconds();

                break;
            }
        }
        // Se è passato il tempo limite, ritorno dalla funzione
        if (tempoCorrente - tempoInizioFunzione > tempoLimiteMilliSecondi) {
            break;
        }
    }

    return carattereLetto;
}
