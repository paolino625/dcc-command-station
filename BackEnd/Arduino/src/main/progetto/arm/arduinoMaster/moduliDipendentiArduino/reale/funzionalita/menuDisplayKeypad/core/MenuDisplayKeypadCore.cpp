// *** INCLUDE *** //

#include "MenuDisplayKeypadCore.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/core/KeypadCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoreImpronte/SensoreImpronteReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/SensoreCorrenteIna219Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/SensoreCorrenteIna260Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/amplificatoreAudio/AmplificatoreAudioReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/stampa/core/DisplayCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/motorShield/MotorShieldReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/mappingStep/MappingStepLocomotiva.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/modificaCv/ModificaCvReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/logIn/LogInReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/menuDisplayKeypad/uscitaMenu/UscitaMenuReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/menuDisplayKeypad/utility/UtilityMenuDisplayKeypadVarie.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/riproduzioneAudio/test/TestAudio.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/scambi/ScambiRelayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/listaConvogli/ListaConvogliReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/listaLocomotive/ListaLocomotiveReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/vagoni/vagoni/VagoniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/vagoni/vagoni/listaVagoni/ListaVagoniReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/displayVariabiliFlagAggiornamento/DisplayVariabiliFlagAggiornamento.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/arduinoSlave/comune/controlloPing/ControlloPing.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/logIn/statoLogIn/StatoLogIn.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/bufferMenuDisplayKeypad/BufferMenuDisplayKeypad.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/statiMenu/StatiMenu.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

MenuDisplayKeypadCore menuDisplayKeypadCoreReale(scambiRelayReale);

// *** DEFINIZIONE METODI *** //

void MenuDisplayKeypadCore::gestiscoDisplayKeypad(TempoAbstract &tempo, KeypadAbstract &keypad,
                                                  DisplayAbstract &display,
                                                  ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuito,
                                                  MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypad,
                                                  UtentiAbstract &utenti) {
    if (fuoriMenu == FuoriMenuEnum::LOG_IN_NON_EFFETTUATO) {
        // Se il log in non è stato effettuato, non devo aggiornare il display, non faccio nulla
        if (display.homepage.displayDaAggiornare) {
            // Se è stato richiesto un refresh del display, devo mostrare di nuovo l'avviso per effettuare il log-in
            displayReale.avvisi.effettuaLogIn();
            display.homepage.displayDaAggiornare = false;
        }

        char carattere = keypad.keypadCore.leggoCarattere();
        if (carattere != '-') {
            // Se l'utente preme un tasto, lo porto alla schermata di log in
            bool risultatoLogIn = logInReale.effettuoLogIn();
            if (risultatoLogIn) {
                fuoriMenu = FuoriMenuEnum::NO_INDICE;
            } else {
            }
            displayReale.avvisi.effettuaLogIn();
        }
    } else if (fuoriMenu == FuoriMenuEnum::INDICE) {
        display.menuPrincipale.mostra(tempo);

        char sceltaMenu = keypad.keypadCore.leggoCarattere();
        if (sceltaMenu == '0') {
            fuoriMenu = FuoriMenuEnum::NO_INDICE;
            display.homepage.displayDaAggiornare = true;
            display.getDisplayCoreAbstract().timestampDisplayPaginaPrecedente = 0;
        }
        gestiscoMenuPrincipale(sceltaMenu, menuDisplayKeypad);
    } else if (fuoriMenu == FuoriMenuEnum::NO_INDICE) {
        // Se è stato richiesto un render completo del display procedo, altrimenti verifico se ci sono solo alcune parti
        // da aggiornare e aggiorno solo quelle.
        if (display.homepage.displayDaAggiornare) {
            displayCoreReale.getDisplayComponente().pulisce();
            display.homepage.display(convogliReali);
            display.homepage.displayDaAggiornare = false;

        } else if (velocitaConvogliDisplayDaAggiornare) {
            // Se il log-in non è stato effettuato, non dobbiamo aggiornare il display perché c'è la scritta per poter
            // effettuare il log-in
            if (statoLogIn.logInEffettuato) {
                if (display.homepage.idConvoglioCorrente1Display != 0) {
                    display.homepage.velocitaConvoglio1(convogliReali);
                }
                if (display.homepage.idConvoglioCorrente2Display != 0) {
                    display.homepage.velocitaConvoglio2(convogliReali);
                }
                velocitaConvogliDisplayDaAggiornare = false;
            }
        } else if (direzioneConvogliDisplayDaAggiornare) {
            // Se il log-in non è stato effettuato, non dobbiamo aggiornare il display perché c'è la scritta per poter
            // effettuare il log-in
            if (statoLogIn.logInEffettuato) {
                if (display.homepage.idConvoglioCorrente1Display != 0) {
                    display.homepage.direzioneConvoglio1(convogliReali);
                }
                if (display.homepage.idConvoglioCorrente2Display != 0) {
                    display.homepage.direzioneConvoglio2(convogliReali);
                }
                direzioneConvogliDisplayDaAggiornare = false;
            }
        } else if (luciConvogliDisplayDaAggiornare) {
            // Se il log-in non è stato effettuato, non dobbiamo aggiornare il display perché c'è la scritta per poter
            // effettuare il log-in
            if (statoLogIn.logInEffettuato) {
                // Aggiorno soltanto le luci del convoglio che cambiano
                if (display.homepage.idConvoglioCorrente1Display != 0) {
                    display.homepage.luciConvoglio1(convogliReali);
                }
                if (display.homepage.idConvoglioCorrente2Display != 0) {
                    display.homepage.luciConvoglio2(convogliReali);
                }
                luciConvogliDisplayDaAggiornare = false;
            }
        }

        char sceltaMenu = keypad.keypadCore.leggoCarattere();
        if (sceltaMenu == '0') {
            fuoriMenu = FuoriMenuEnum::INDICE;
            display.homepage.displayDaAggiornare = true;
            display.getDisplayCoreAbstract().timestampDisplayPaginaPrecedente = 0;
        }
        gestiscoMenuPrincipale(sceltaMenu, menuDisplayKeypad);

    } else if (fuoriMenu == FuoriMenuEnum::MENU_PRINCIPALE) {
        // Scendo al livello inferiore, ovvero al menù principale
        if (menuPrincipale == MenuPrincipaleEnum::CONVOGLI) {
            // Entrato nel menu convogli
            if (menuConvogli == MenuConvogliEnum::SCELTA_IN_CORSO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU CONVOGLI -> SCELTA IN CORSO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.convoglio.menu.mostra();
                    display.homepage.displayDaAggiornare = false;
                }

                int sceltaMenu = keypad.leggoSceltaMenuUtente();
                if (sceltaMenu == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaMenu != -1) {
                    if (sceltaMenu == 1) {
                        // Aggiungi convoglio
                        menuConvogli = MenuConvogliEnum::AGGIUNGI;
                        display.homepage.displayDaAggiornare = true;
                    } else if (sceltaMenu == 2) {
                        // Elimina convoglio
                        menuConvogli = MenuConvogliEnum::ELIMINA;
                        display.homepage.displayDaAggiornare = true;
                    } else if (sceltaMenu == 3) {
                        // Gestisci convoglio
                        menuConvogli = MenuConvogliEnum::GESTISCI;
                        display.homepage.displayDaAggiornare = true;
                    } else if (sceltaMenu == 4) {
                        // Reset convoglio
                        menuConvogli = MenuConvogliEnum::RESET;
                        display.homepage.displayDaAggiornare = true;
                    }
                }
            } else if (menuConvogli == MenuConvogliEnum::AGGIUNGI) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU CONVOGLI -> AGGIUNGI",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.homepage.displayDaAggiornare = false;
                }
                display.locomotiva.menu.lista(listaLocomotiveReale, tempo, false, true);

                idLocomotivaDisplayKeypad = keypad.leggoIdLocomotivaEVerifico(false, true);
                if (idLocomotivaDisplayKeypad == 0) {
                    // Voglio uscire
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else

                    if (idLocomotivaDisplayKeypad > 0) {
                    // Setto la locomotiva al primo struct convoglio libero che trovo
                    bool convoglioTrovato = false;
                    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
                        ConvoglioAbstract *convoglio = convogliReali.getConvoglio(i, false);
                        if (!convoglio->isInUso()) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Aggiungo locomotiva ",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                                                  std::to_string(idLocomotivaDisplayKeypad).c_str(),
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " al convoglio n° ",
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(i).c_str(),
                                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                            convoglio->setIdLocomotiva1(idLocomotivaDisplayKeypad);
                            // Imposto il convoglio sul tracciato in modo tale che appena dopo
                            // l'aggiunta sia visibile nel menù principale
                            convoglio->setPresenteSulTracciato(true);
                            convoglioTrovato = true;
                            convoglio->aggiornaLunghezza();
                            convoglio->setDifferenzaVelocitaLocomotivaTrazioneSpinta(NESSUNA_DIFFERENZA);

                            assegnoConvogliInUsoDisplay();

                            uscitaMenuReale.escoMenu(false, false, true, false);

                            // Se sono riuscito ad assegnare la locomotiva, esco dal ciclo
                            break;
                        }
                    }

                    if (!convoglioTrovato) {
                        LOG_MESSAGGIO_STATICO(
                            loggerReale, LivelloLog::CRITICAL,
                            "Case MENU_CONVOGLI_AGGIUNGI MenuDisplayKeypadCore(). Non ho trovato nessun convoglio "
                            "libero a cui "
                            "assegnare la nuova locomotiva!",
                            InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        programmaReale.ferma(true);
                    }
                }
            } else if (menuConvogli == MenuConvogliEnum::ELIMINA) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU CONVOGLI -> ELIMINA",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.homepage.displayDaAggiornare = false;
                    display.convoglio.scelta.inserimentoConvoglio();
                }

                int numeroConvoglio = keypad.leggoPosizioneConvoglioDisplay(false);
                if (numeroConvoglio == 0) {
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (numeroConvoglio > 0) {
                    if (esisteNumeroConvoglioDisplay(numeroConvoglio)) {
                        if (numeroConvoglio == 1) {
                            convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente1Display;
                        } else if (numeroConvoglio == 2) {
                            convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente2Display;
                        }

                        convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->reset();

                        assegnoConvogliInUsoDisplay();

                        uscitaMenuReale.escoMenu(false, false, true, false);
                    }
                }
            } else if (menuConvogli == MenuConvogliEnum::GESTISCI) {
                if (menuConvogliGestisci == MenuConvogliGestisciEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU CONVOGLI -> GESTISCI -> SCELTA IN CORSO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.convoglio.scelta.gestioneConvoglio();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaMenu = keypad.leggoSceltaMenuUtente();
                    if (sceltaMenu == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaMenu != -1) {
                        if (sceltaMenu == 1) {
                            // Gestisci loco
                            menuConvogliGestisci = MenuConvogliGestisciEnum::GESTISCI_LOCO;
                            display.homepage.displayDaAggiornare = true;

                        } else if (sceltaMenu == 2) {
                            // Gestisci vagoni
                            menuConvogliGestisci = MenuConvogliGestisciEnum::GESTISCI_VAGONI;
                            display.homepage.displayDaAggiornare = true;
                        } else if (sceltaMenu == 3) {
                            // Gestisci presenza
                            menuConvogliGestisci = MenuConvogliGestisciEnum::GESTISCI_PRESENZA;
                            display.homepage.displayDaAggiornare = true;
                        } else if (sceltaMenu == 4) {
                            // Gestisci differenza velocità locomotiva trazione/spinta
                            menuConvogliGestisci =
                                MenuConvogliGestisciEnum::GESTISCI_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA;
                            display.homepage.displayDaAggiornare = true;
                        }
                    }
                } else if (menuConvogliGestisci == MenuConvogliGestisciEnum::GESTISCI_LOCO) {
                    if (menuConvogliGestisciLoco == MenuConvogliGestisciLocoEnum::INSERIMENTO_CONVOGLIO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI LOCO -> INSERIMENTO CONVOGLIO",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.inserimentoConvoglio();
                            display.homepage.displayDaAggiornare = false;
                        }
                        int posizioneConvoglio = keypad.leggoPosizioneConvoglioDisplay(false);
                        if (posizioneConvoglio == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (posizioneConvoglio > 0) {
                            if (esisteNumeroConvoglioDisplay(posizioneConvoglio)) {
                                if (posizioneConvoglio == 1) {
                                    convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente1Display;
                                } else if (posizioneConvoglio == 2) {
                                    convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente2Display;
                                }

                                if (!convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)
                                         ->hasDoppiaLocomotiva()) {
                                    menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::AGGIUNGI;
                                    display.homepage.displayDaAggiornare = true;
                                } else {
                                    menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::ELIMINA;
                                    display.homepage.displayDaAggiornare = true;
                                }
                            }
                        }

                    } else if (menuConvogliGestisciLoco == MenuConvogliGestisciLocoEnum::AGGIUNGI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI LOCO -> AGGIUNGI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                            display.homepage.displayDaAggiornare = false;
                        }
                        display.locomotiva.menu.lista(listaLocomotiveReale, tempo, false, true);

                        idLocomotivaDisplayKeypad = keypad.leggoIdLocomotivaEVerifico(false, true);

                        if (idLocomotivaDisplayKeypad == 0) {
                            // Voglio uscire
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (idLocomotivaDisplayKeypad > 0) {
                            convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)
                                ->aggiungeSecondaLocomotiva(idLocomotivaDisplayKeypad);
                            convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->aggiornaLunghezza();
                            uscitaMenuReale.escoMenu(false, false, true, false);
                        }

                    } else if (menuConvogliGestisciLoco == MenuConvogliGestisciLocoEnum::ELIMINA) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI LOCO -> AGGIUNGI ELIMINA -> ELIMINA",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.eliminazioneLoco();
                            display.homepage.displayDaAggiornare = false;
                        }

                        int tastoPremuto = keypad.leggoSceltaMenuUtente();
                        if (tastoPremuto == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (tastoPremuto > 0) {
                            if (tastoPremuto == 1) {
                                convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)
                                    ->eliminaSecondaLocomotiva();
                                convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->aggiornaLunghezza();
                                uscitaMenuReale.escoMenu(false, false, true, false);
                            } else if (tastoPremuto == 2) {
                                uscitaMenuReale.escoMenu(false, false, false, false);
                            }
                        }

                    } else if (menuConvogliGestisciLoco == MenuConvogliGestisciLocoEnum::INVERTI_LUCI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI LOCO -> INVERTI LUCI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.inserimentoConvoglio();
                            display.homepage.displayDaAggiornare = false;
                        }
                        int numeroConvoglio = keypad.leggoPosizioneConvoglioDisplay(false);
                        int posizioneConvoglio = keypad.leggoPosizioneConvoglioDisplay(false);
                        if (posizioneConvoglio == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (numeroConvoglio > 0) {
                            if (esisteNumeroConvoglioDisplay(numeroConvoglio)) {
                                if (numeroConvoglio == 1) {
                                    convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente1Display;
                                } else if (numeroConvoglio == 2) {
                                    convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente2Display;
                                }
                            }
                            if (numeroConvoglio == 1) {
                                menuDisplayKeypad.cambioLuciConvoglio1EAggiornoDisplay(convogliReali);
                            } else if (numeroConvoglio == 2) {
                                menuDisplayKeypad.cambioLuciConvoglio2EAggiornoDisplay(convogliReali);
                            }
                            uscitaMenuReale.escoMenu(false, false, true, false);
                        } else {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        }
                    }
                } else if (menuConvogliGestisci == MenuConvogliGestisciEnum::GESTISCI_VAGONI) {
                    if (menuConvogliGestisciVagoni == MenuConvogliGestisciVagoniEnum::SCELTA_IN_CORSO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI -> GESTISCI VAGONI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.inserimentoConvoglio();
                            display.homepage.displayDaAggiornare = false;
                        }

                        int numeroConvoglio = keypad.leggoPosizioneConvoglioDisplay(false);
                        if (numeroConvoglio > 0) {
                            if (numeroConvoglio == 1) {
                                convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente1Display;
                            } else if (numeroConvoglio == 2) {
                                convoglioCorrenteDisplay = &display.homepage.idConvoglioCorrente2Display;
                            }

                            byte numeroVagoni =
                                convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->getNumeroVagoni();

                            // Se non c'è nessun vagone assegnato, mostro solo l'opzione per
                            // aggiungerne
                            if (numeroVagoni == 0) {
                                menuConvogliGestisciVagoni = MenuConvogliGestisciVagoniEnum::AGGIUNGI;
                                display.homepage.displayDaAggiornare = true;

                            } else {
                                // Se c'è almeno un vagone già assegnato, devo dare la possibilità
                                // di eliminare e aggiungere
                                menuConvogliGestisciVagoni = MenuConvogliGestisciVagoniEnum::AGGIUNGI_ELIMINA;
                                display.homepage.displayDaAggiornare = true;
                            }
                        }
                    } else if (menuConvogliGestisciVagoni == MenuConvogliGestisciVagoniEnum::AGGIUNGI_ELIMINA) {
                        // displayMenuAggiungiEliminaVagoneConvoglio
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI VAGONI -> AGGIUNGI ELIMINA",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.aggiuntaEliminazioneVagone();
                            display.homepage.displayDaAggiornare = false;
                        }

                        int sceltaUtente = keypad.leggoSceltaMenuUtente();
                        if (sceltaUtente == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sceltaUtente > 0) {
                            if (sceltaUtente == 1) {
                                // Aggiungo nuovo vagone
                                menuConvogliGestisciVagoni = MenuConvogliGestisciVagoniEnum::AGGIUNGI;

                            } else if (sceltaUtente == 2) {
                                // Elimino nuovo vagone
                                menuConvogliGestisciVagoni = MenuConvogliGestisciVagoniEnum::ELIMINA;
                            }
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuConvogliGestisciVagoni == MenuConvogliGestisciVagoniEnum::AGGIUNGI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI VAGONI -> AGGIUNGI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.homepage.displayDaAggiornare = false;
                        }

                        // Non posso inserirlo sotto l'if display.homepage.displayDaAggiornare perché potrebbe essere
                        // una lista lunga più di una pagina
                        display.convoglio.vagoni.menu.lista(tempo, false, true, 0, listaVagoniReale);
                        display.homepage.displayDaAggiornare = true;

                        int idVagone = keypad.leggoIdVagoneEVerifico(false, true, vagoniReali);
                        if (idVagone == 0) {
                            // Voglio uscire
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (idVagone > 0) {
                            convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->aggiungeVagone(idVagone);
                            convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->aggiornaLunghezza();

                            uscitaMenuReale.escoMenu(false, false, true, false);
                        }

                    } else if (menuConvogliGestisciVagoni == MenuConvogliGestisciVagoniEnum::ELIMINA) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI VAGONI -> ELIMINA",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.homepage.displayDaAggiornare = false;
                        }

                        display.convoglio.vagoni.menu.lista(
                            tempo, true, false, convogliReali.getConvoglio(*convoglioCorrenteDisplay, false),
                            listaVagoniReale);
                        display.homepage.displayDaAggiornare = true;

                        int idVagone = keypad.leggoIdVagoneEVerifico(true, false, vagoniReali);
                        if (idVagone == 0) {
                            // Voglio uscire
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (idVagone > 0) {
                            convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->eliminaVagone(idVagone);
                            convogliReali.getConvoglio(idConvoglioDisplayKeypad, false)->aggiornaLunghezza();
                            uscitaMenuReale.escoMenu(false, false, true, false);
                        }
                    }

                } else if (menuConvogliGestisci == MenuConvogliGestisciEnum::GESTISCI_PRESENZA) {
                    if (menuConvogliGestisciPresenza == MenuConvogliGestisciPresenzaEnum::SCELTA_IN_CORSO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI PRESENZA -> SCELTA IN CORSO",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.gestionePresenza();
                            display.homepage.displayDaAggiornare = false;
                        }

                        int sceltaUtente = keypad.leggoSceltaMenuUtente();
                        if (sceltaUtente == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sceltaUtente > 0) {
                            if (sceltaUtente == 1) {
                                // Togli convoglio
                                menuConvogliGestisciPresenza = MenuConvogliGestisciPresenzaEnum::TOGLI;

                            } else if (sceltaUtente == 2) {
                                // Metti convoglio
                                menuConvogliGestisciPresenza = MenuConvogliGestisciPresenzaEnum::METTI;

                            } else if (sceltaUtente == 3) {
                                // Lista convogli
                                menuConvogliGestisciPresenza = MenuConvogliGestisciPresenzaEnum::LISTA;
                            }
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuConvogliGestisciPresenza == MenuConvogliGestisciPresenzaEnum::TOGLI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI PRESENZA -> TOGLI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.homepage.displayDaAggiornare = false;
                        }
                        display.convoglio.menu.lista(listaConvogliReale, tempo, true, false, false);

                        int idConvoglio = keypad.leggoIdConvoglioEVerifico(true, false);
                        if (idConvoglio == 0) {
                            // Voglio uscire
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (idConvoglio > 0) {
                            ConvoglioAbstract *convoglio = convogliReali.getConvoglio(idConvoglio, false);
                            convoglio->setPresenteSulTracciato(false);

                            assegnoConvogliInUsoDisplay();

                            uscitaMenuReale.escoMenu(false, false, true, false);
                        }

                    } else if (menuConvogliGestisciPresenza == MenuConvogliGestisciPresenzaEnum::METTI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI PRESENZA -> METTI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.homepage.displayDaAggiornare = false;
                        }
                        display.convoglio.menu.lista(listaConvogliReale, tempo, false, true, false);

                        idConvoglioDisplayKeypad = keypad.leggoIdConvoglioEVerifico(false, true);
                        if (idConvoglioDisplayKeypad == 0) {
                            // Voglio uscire
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (idConvoglioDisplayKeypad > 0) {
                            ConvoglioAbstract *convoglio = convogliReali.getConvoglio(idConvoglioDisplayKeypad, false);
                            convoglio->setPresenteSulTracciato(true);
                            display.homepage.displayDaAggiornare = true;
                        }
                    }
                } else if (menuConvogliGestisci ==
                           MenuConvogliGestisciEnum::GESTISCI_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA) {
                    if (menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta ==
                        MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::INSERIMENTO_CONVOGLIO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI DIFFERENZA VELOCITA LOCOMOTIVA TRAZIONE "
                                                  "SPINTA -> INSERIMENTO CONVOGLIO",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.homepage.displayDaAggiornare = false;
                            display.convoglio.scelta.inserimentoConvoglio();
                        }

                        int numeroConvoglio = keypad.leggoPosizioneConvoglioDisplay(false);
                        if (numeroConvoglio == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (numeroConvoglio > 0) {
                            if (esisteNumeroConvoglioDisplay(numeroConvoglio)) {
                                if (numeroConvoglio == 1) {
                                    idConvoglioDisplayKeypad = display.homepage.idConvoglioCorrente1Display;
                                } else if (numeroConvoglio == 2) {
                                    idConvoglioDisplayKeypad = display.homepage.idConvoglioCorrente2Display;
                                }

                                menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta =
                                    MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::SCELTA_IN_CORSO;

                                display.homepage.displayDaAggiornare = true;
                            }
                        }
                    } else if (menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta ==
                               MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::SCELTA_IN_CORSO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI DIFFERENZA VELOCITA LOCOMOTIVA TRAZIONE "
                                                  "SPINTA -> SCELTA IN CORSO",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.scelta.gestioneDiffLoco(
                                convogliReali.getConvoglio(idConvoglioDisplayKeypad, false));
                            display.homepage.displayDaAggiornare = false;
                        }

                        int sceltaUtente = keypad.leggoSceltaMenuUtente();
                        if (sceltaUtente == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sceltaUtente > 0) {
                            if (sceltaUtente == 1) {
                                menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta =
                                    MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::RESET_A_0;

                            } else if (sceltaUtente == 2) {
                                menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta =
                                    MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::
                                        SET_VELOCITA_POSITIVA;

                            } else if (sceltaUtente == 3) {
                                menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta =
                                    MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::
                                        SET_VELOCITA_NEGATIVA;
                            }
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta ==
                               MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::RESET_A_0) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI DIFFERENZA VELOCITA LOCOMOTIVA TRAZIONE "
                                                  "SPINTA -> RESET A 0",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.homepage.displayDaAggiornare = false;
                        }

                        ConvoglioAbstractReale *convoglio = convogliReali.getConvoglio(idConvoglioDisplayKeypad, false);
                        convoglio->setDifferenzaStepVelocitaTraLocomotive(0);

                        uscitaMenuReale.escoMenu(false, false, true, false);
                    } else if (menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta ==
                               MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::
                                   SET_VELOCITA_POSITIVA) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI DIFFERENZA VELOCITA LOCOMOTIVA TRAZIONE "
                                                  "SPINTA -> SET VELOCITA POSITIVA",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.differenzaVelocitaLocomotivaTrazioneSpinta.inserisciStep();
                            display.homepage.displayDaAggiornare = false;
                        }

                        StepVelocitaDecoderTipo stepVelocita = keypad.leggoStepVelocita();
                        if (stepVelocita == 0) {
                            uscitaMenuReale.escoMenu(false, false, true, false);
                        } else if (stepVelocita >= 0) {
                            ConvoglioAbstractReale *convoglio =
                                convogliReali.getConvoglio(idConvoglioDisplayKeypad, false);
                            convoglio->setDifferenzaStepVelocitaTraLocomotive(stepVelocita);

                            uscitaMenuReale.escoMenu(false, false, true, false);
                        }
                    } else if (menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta ==
                               MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::
                                   SET_VELOCITA_NEGATIVA) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU CONVOGLI -> GESTISCI DIFFERENZA VELOCITA LOCOMOTIVA TRAZIONE "
                                                  "SPINTA -> SET VELOCITA NEGATIVA",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.convoglio.differenzaVelocitaLocomotivaTrazioneSpinta.inserisciStep();
                            display.homepage.displayDaAggiornare = false;
                        }

                        StepVelocitaDecoderTipo stepVelocita = keypad.leggoStepVelocita();
                        if (stepVelocita == 0) {
                            uscitaMenuReale.escoMenu(false, false, true, false);
                        } else if (stepVelocita >= 0) {
                            ConvoglioAbstractReale *convoglio =
                                convogliReali.getConvoglio(idConvoglioDisplayKeypad, false);
                            convoglio->setDifferenzaStepVelocitaTraLocomotive(-stepVelocita);

                            uscitaMenuReale.escoMenu(false, false, true, false);
                        }
                    }
                }

            } else if (menuConvogli == MenuConvogliEnum::RESET) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU CONVOGLI -> RESET",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                    display.convoglio.menu.mostra();
                    display.homepage.displayDaAggiornare = false;
                }

                int tastoPremuto = keypad.leggoSceltaMenuUtente();
                if (tastoPremuto == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (tastoPremuto > 0) {
                    if (tastoPremuto == 1) {
                        convogliReali.reset();

                        uscitaMenuReale.escoMenu(false, false, true, false);
                    } else if (tastoPremuto == 2) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    }
                }
            }
        } else if (menuPrincipale == MenuPrincipaleEnum::LOCOMOTIVE) {
            if (menuLocomotive == MenuLocomotiveEnum::INSERISCI_LOCO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                          "MENU PRINCIPALE -> LOCOMOTIVE -> INSERISCI LOCO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.homepage.displayDaAggiornare = false;
                    display.locomotiva.scelta.idLoco();
                }

                idLocomotivaDisplayKeypad = keypad.leggoIdLocomotivaEVerifico(true, true);
                if (idLocomotivaDisplayKeypad == 0) {
                    // Voglio uscire
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (idLocomotivaDisplayKeypad > 0) {
                    menuLocomotive = MenuLocomotiveEnum::SCELTA_IN_CORSO;
                    display.homepage.displayDaAggiornare = true;
                }
            } else if (menuLocomotive == MenuLocomotiveEnum::SCELTA_IN_CORSO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                          "MENU PRINCIPALE -> LOCOMOTIVE -> SCELTA IN CORSO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.homepage.displayDaAggiornare = false;
                    display.locomotiva.scelta.operazioneLocomotiva();
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    if (sceltaUtente == 1) {
                        menuLocomotive = MenuLocomotiveEnum::TEMPO_USO;

                    } else if (sceltaUtente == 2) {
                        menuLocomotive = MenuLocomotiveEnum::MAPPING_VELOCITA;

                    } else if (sceltaUtente == 3) {
                        menuLocomotive = MenuLocomotiveEnum::SET_DECODER;
                    }
                    display.homepage.displayDaAggiornare = true;
                }
            } else if (menuLocomotive == MenuLocomotiveEnum::TEMPO_USO) {
                if (menuLocomotiveTempoUso == MenuLocomotiveTempoUsoEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU LOCOMOTIVE -> TEMPO USO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.locomotiva.tempoFunzionamento.menu();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuLocomotiveTempoUso = MenuLocomotiveTempoUsoEnum::STATO_LOCO;

                        } else if (sceltaUtente == 2) {
                            menuLocomotiveTempoUso = MenuLocomotiveTempoUsoEnum::MANUTENZIONE_EFFETTUATA;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuLocomotiveTempoUso == MenuLocomotiveTempoUsoEnum::STATO_LOCO) {
                    display.locomotiva.tempoFunzionamento.schermataPrincipale(
                        keypadCoreReale, locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true));
                    uscitaMenuReale.escoMenu(false, true, false, false);
                } else if (menuLocomotiveTempoUso == MenuLocomotiveTempoUsoEnum::MANUTENZIONE_EFFETTUATA) {
                    LocomotivaAbstract *locomotiva = locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true);
                    locomotiva->setTempoFunzionamentoUltimaManutenzione(0);
                    uscitaMenuReale.escoMenu(false, true, false, false);
                }
            } else if (menuLocomotive == MenuLocomotiveEnum::MAPPING_VELOCITA) {
                if (menuLocomotiveMappingVelocita == MenuLocomotiveMappingVelocitaEnum::AVVISO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU LOCOMOTIVE -> MAPPING VELOCITA -> AVVISO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.locomotiva.mapping.schermataPrincipale.posizionamentoLocomotivaSulTracciato();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        menuLocomotiveMappingVelocita = MenuLocomotiveMappingVelocitaEnum::SCELTA_IN_CORSO;
                        display.homepage.displayDaAggiornare = true;
                    }

                } else if (menuLocomotiveMappingVelocita == MenuLocomotiveMappingVelocitaEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU LOCOMOTIVE -> MAPPING VELOCITA -> SCELTA IN CORSO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.locomotiva.mapping.scelta.modalita();
                        // Disattivo controllo presenza arduino slave per il seguente motivo: in
                        // questa funzione rimango tanto tempo entrando e riuscendo. Questo
                        // provocherebbe un reset delle porte seriali degli Arduino Slave in
                        // continuazione. Quindi disattivo e riattivo quando finisco.
                        controlloPresenzaArduinoSlaveAttivo = false;
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuLocomotiveMappingVelocita = MenuLocomotiveMappingVelocitaEnum::TUTTI_STEP;

                        } else if (sceltaUtente == 2) {
                            menuLocomotiveMappingVelocita = MenuLocomotiveMappingVelocitaEnum::SINGOLO_STEP;

                        } else if (sceltaUtente == 3) {
                            menuLocomotiveMappingVelocita = MenuLocomotiveMappingVelocitaEnum::INTERVALLO_STEP;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuLocomotiveMappingVelocita == MenuLocomotiveMappingVelocitaEnum::TUTTI_STEP) {
                    if (menuLocomotiveMappingVelocitaTuttiStep ==
                        MenuLocomotiveMappingVelocitaTuttiStepEnum::INSERISCI_SENSORE_1) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> TUTTI STEP -> INSERISCI \"\n"
                                                  "                                \"SENSORE 1",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(1);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare1 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare1 > 0) {
                            menuLocomotiveMappingVelocitaTuttiStep =
                                MenuLocomotiveMappingVelocitaTuttiStepEnum::INSERISCI_SENSORE_2;
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuLocomotiveMappingVelocitaTuttiStep ==
                               MenuLocomotiveMappingVelocitaTuttiStepEnum::INSERISCI_SENSORE_2) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> TUTTI STEP -> INSERISCI "
                                                  "SENSORE 2",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(2);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare2 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare2 > 0) {
                            mappingStepLocomotiva.inizializzaModalitaTuttiStep();
                            mappingStepLocomotiva.mappoVelocitaLoco(
                                locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true), sensoreDaUsare1,
                                sensoreDaUsare2, stepInizialeDisplayKeypad, stepFinaleDisplayKeypad,
                                modalita2SensoriDisplayKeypad, invioStatoTracciatoReale, controlloCortoCircuito,
                                protocolloDccCoreReale);
                            uscitaMenuReale.escoMenu(false, true, false, false);
                        }
                    }

                } else if (menuLocomotiveMappingVelocita == MenuLocomotiveMappingVelocitaEnum::SINGOLO_STEP) {
                    if (menuLocomotiveMappingVelocitaSingoloStep ==
                        MenuLocomotiveMappingVelocitaSingoloStepEnum::INSERISCI_STEP) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> SINGOLO STEP -> INSERISCI "
                                                  "STEP",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.step();
                            display.homepage.displayDaAggiornare = false;
                        }

                        stepInizialeDisplayKeypad = keypad.leggoStepVelocitaEVerifico();

                        if (stepInizialeDisplayKeypad == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else

                            if (stepInizialeDisplayKeypad > 0) {
                            menuLocomotiveMappingVelocitaSingoloStep =
                                MenuLocomotiveMappingVelocitaSingoloStepEnum::INSERISCI_MODALITA_SENSORI;
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuLocomotiveMappingVelocitaSingoloStep ==
                               MenuLocomotiveMappingVelocitaSingoloStepEnum::INSERISCI_MODALITA_SENSORI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> SINGOLO STEP -> INSERISCI "
                                                  "MODALITA SENSORI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.modalitaSensori();
                            display.homepage.displayDaAggiornare = false;
                        }
                        int sceltaUtente = keypad.leggoSceltaMenuUtente();
                        if (sceltaUtente == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sceltaUtente > 0) {
                            modalita2SensoriIntDisplayKeypad = sceltaUtente;
                            if (sceltaUtente == 1) {
                                menuLocomotiveMappingVelocitaSingoloStep =
                                    MenuLocomotiveMappingVelocitaSingoloStepEnum::
                                        MODALITA_1_SENSORE_INSERISCI_SENSORE_1;

                            } else if (sceltaUtente == 2) {
                                menuLocomotiveMappingVelocitaSingoloStep =
                                    MenuLocomotiveMappingVelocitaSingoloStepEnum::
                                        MODALITA_2_SENSORI_INSERISCI_SENSORE_1;
                            }
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuLocomotiveMappingVelocitaSingoloStep ==
                               MenuLocomotiveMappingVelocitaSingoloStepEnum::MODALITA_1_SENSORE_INSERISCI_SENSORE_1) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> SINGOLO STEP -> MODALITA 1 "
                                                  "SENSORE -> "
                                                  "INSERISCI SENSORE 1",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(1);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare1 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare1 > 0) {
                            mappingStepLocomotiva.inizializzaMappingVelocitaSingoloStep();
                            mappingStepLocomotiva.mappoVelocitaLoco(
                                locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true), sensoreDaUsare1,
                                sensoreDaUsare2, stepInizialeDisplayKeypad, stepFinaleDisplayKeypad,
                                modalita2SensoriDisplayKeypad, invioStatoTracciatoReale, controlloCortoCircuito,
                                protocolloDccCoreReale);
                            uscitaMenuReale.escoMenu(false, true, false, false);
                        }
                    } else if (menuLocomotiveMappingVelocitaSingoloStep ==
                               MenuLocomotiveMappingVelocitaSingoloStepEnum::MODALITA_2_SENSORI_INSERISCI_SENSORE_1) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> SINGOLO STEP -> MODALITA 2 "
                                                  "SENSORI -> "
                                                  "INSERISCI SENSORE 1",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(1);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare1 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare1 > 0) {
                            menuLocomotiveMappingVelocitaSingoloStep =
                                MenuLocomotiveMappingVelocitaSingoloStepEnum::MODALITA_2_SENSORI_INSERISCI_SENSORE_2;
                            display.homepage.displayDaAggiornare = true;
                        }

                    } else if (menuLocomotiveMappingVelocitaSingoloStep ==
                               MenuLocomotiveMappingVelocitaSingoloStepEnum::MODALITA_2_SENSORI_INSERISCI_SENSORE_2) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> SINGOLO STEP -> MODALITA 2 "
                                                  "SENSORI ->  "
                                                  "INSERISCI SENSORE 2",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(2);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare2 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare2 > 0) {
                            mappingStepLocomotiva.inizializzaMappingVelocitaSingoloStep();
                            mappingStepLocomotiva.mappoVelocitaLoco(
                                locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true), sensoreDaUsare1,
                                sensoreDaUsare2, stepInizialeDisplayKeypad, stepFinaleDisplayKeypad,
                                modalita2SensoriDisplayKeypad, invioStatoTracciatoReale, controlloCortoCircuito,
                                protocolloDccCoreReale);
                            uscitaMenuReale.escoMenu(false, true, false, false);
                        }
                    }
                    //  menuLocomotiveMappingVelocitaSingoloStep
                } else if (menuLocomotiveMappingVelocita == MenuLocomotiveMappingVelocitaEnum::INTERVALLO_STEP) {
                    if (menuLocomotiveMappingVelocitaIntervalloStep ==
                        MenuLocomotiveMappingVelocitaIntervalloStepEnum::INSERISCI_STEP_1) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> INTERVALLO STEP -> "
                                                  "INSERISCI STEP 1",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.stepIniziale();
                            display.homepage.displayDaAggiornare = false;
                        }

                        stepInizialeDisplayKeypad = keypad.leggoStepVelocitaEVerifico();
                        if (stepInizialeDisplayKeypad == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (stepInizialeDisplayKeypad > 0) {
                            menuLocomotiveMappingVelocitaIntervalloStep =
                                MenuLocomotiveMappingVelocitaIntervalloStepEnum::INSERISCI_STEP_2;
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuLocomotiveMappingVelocitaIntervalloStep ==
                               MenuLocomotiveMappingVelocitaIntervalloStepEnum::INSERISCI_STEP_2) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> INTERVALLO STEP -> "
                                                  "INSERISCI STEP 2",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                            display.locomotiva.mapping.scelta.stepFinale();
                            display.homepage.displayDaAggiornare = false;
                        }

                        stepFinaleDisplayKeypad = keypad.leggoStepVelocitaEVerifico() + 1;
                        if (stepInizialeDisplayKeypad == 1) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (stepFinaleDisplayKeypad > 0) {
                            menuLocomotiveMappingVelocitaIntervalloStep =
                                MenuLocomotiveMappingVelocitaIntervalloStepEnum::INSERISCI_MODALITA_SENSORI;
                            display.homepage.displayDaAggiornare = true;
                        }

                    } else if (menuLocomotiveMappingVelocitaIntervalloStep ==
                               MenuLocomotiveMappingVelocitaIntervalloStepEnum::INSERISCI_MODALITA_SENSORI) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> INTERVALLO STEP -> "
                                                  "INSERISCI MODALITA "
                                                  "SENSORI",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.modalitaSensori();
                            display.homepage.displayDaAggiornare = false;
                        }

                        int sceltaUtente = keypad.leggoSceltaMenuUtente();
                        if (sceltaUtente == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sceltaUtente > 0) {
                            modalita2SensoriIntDisplayKeypad = sceltaUtente;
                            if (sceltaUtente == 1) {
                                menuLocomotiveMappingVelocitaIntervalloStep =
                                    MenuLocomotiveMappingVelocitaIntervalloStepEnum::
                                        MODALITA_1_SENSORE_INSERISCI_SENSORE_1;

                            } else if (sceltaUtente == 2) {
                                menuLocomotiveMappingVelocitaIntervalloStep =
                                    MenuLocomotiveMappingVelocitaIntervalloStepEnum::
                                        MODALITA_2_SENSORI_INSERISCI_SENSORE_1;
                            }
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuLocomotiveMappingVelocitaIntervalloStep ==
                               MenuLocomotiveMappingVelocitaIntervalloStepEnum::
                                   MODALITA_1_SENSORE_INSERISCI_SENSORE_1) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> INTERVALLO STEP -> "
                                                  "MODALITA 1 SENSORE -> "
                                                  "INSERISCI SENSORE 1",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(1);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare1 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare1 > 0) {
                            mappingStepLocomotiva.inizializzaMappingVelocitaIntervalloStep();
                            mappingStepLocomotiva.mappoVelocitaLoco(
                                locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true), sensoreDaUsare1,
                                sensoreDaUsare2, stepInizialeDisplayKeypad, stepFinaleDisplayKeypad,
                                modalita2SensoriDisplayKeypad, invioStatoTracciatoReale, controlloCortoCircuito,
                                protocolloDccCoreReale);
                            uscitaMenuReale.escoMenu(false, true, false, false);
                        }
                    } else if (menuLocomotiveMappingVelocitaIntervalloStep ==
                               MenuLocomotiveMappingVelocitaIntervalloStepEnum::
                                   MODALITA_2_SENSORI_INSERISCI_SENSORE_1) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> INTERVALLO STEP -> "
                                                  "MODALITA 2 SENSORI -> "
                                                  "INSERISCI SENSORE 1",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(1);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare1 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare1 > 0) {
                            menuLocomotiveMappingVelocitaIntervalloStep =
                                MenuLocomotiveMappingVelocitaIntervalloStepEnum::MODALITA_2_SENSORI_INSERISCI_SENSORE_2;
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuLocomotiveMappingVelocitaIntervalloStep ==
                               MenuLocomotiveMappingVelocitaIntervalloStepEnum::
                                   MODALITA_2_SENSORI_INSERISCI_SENSORE_2) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU LOCOMOTIVE -> MAPPING VELOCITA -> INTERVALLO STEP -> "
                                                  "MODALITA 2 SENSORI -> "
                                                  "INSERISCI SENSORE 2",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.locomotiva.mapping.scelta.sensore(2);
                            display.homepage.displayDaAggiornare = false;
                        }

                        sensoreDaUsare2 = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (sensoreDaUsare2 > 0) {
                            mappingStepLocomotiva.inizializzaMappingVelocitaIntervalloStep();
                            mappingStepLocomotiva.mappoVelocitaLoco(
                                locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true), sensoreDaUsare1,
                                sensoreDaUsare2, stepInizialeDisplayKeypad, stepFinaleDisplayKeypad,
                                modalita2SensoriDisplayKeypad, invioStatoTracciatoReale, controlloCortoCircuito,
                                protocolloDccCoreReale);
                            uscitaMenuReale.escoMenu(false, true, false, false);
                        }
                    }
                }

            } else if (menuLocomotive == MenuLocomotiveEnum::SET_DECODER) {
                if (menuLocomotiveDecoder == MenuLocomotiveDecoderEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU LOCOMOTIVE -> SET DECODER",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.locomotiva.setDecoderLocomotiva.menu();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuLocomotiveDecoder = MenuLocomotiveDecoderEnum::CAMBIA_INDIRIZZO;
                        } else if (sceltaUtente == 2) {
                            menuLocomotiveDecoder = MenuLocomotiveDecoderEnum::CAMBIA_KEEP_ALIVE;
                        } else if (sceltaUtente == 3) {
                            menuLocomotiveDecoder = MenuLocomotiveDecoderEnum::RESET_ACCELERAZIONE_DECELERAZIONE;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuLocomotiveDecoder == MenuLocomotiveDecoderEnum::CAMBIA_INDIRIZZO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU LOCOMOTIVE -> SET DECODER -> CAMBIA INDIRIZZO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.locomotiva.scelta.indirizzoLoco();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int indirizzoLocomotiva = keypad.leggoIndirizzoLocomotiva();
                    if (indirizzoLocomotiva == 0) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (indirizzoLocomotiva > 0) {
                        LocomotivaAbstract *locomotiva = locomotiveReali.getLocomotiva(idLocomotivaDisplayKeypad, true);
                        locomotiva->setIndirizzoDcc(indirizzoLocomotiva);
                        modificaCvReale.indirizzoLocomotiva(indirizzoLocomotiva);
                        uscitaMenuReale.escoMenu(false, true, false, false);
                    }

                } else if (menuLocomotiveDecoder == MenuLocomotiveDecoderEnum::CAMBIA_KEEP_ALIVE) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU LOCOMOTIVE -> SET DECODER -> CAMBIA KEEP ALIVE",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.locomotiva.scelta.valoreKeepAlive();
                        display.homepage.displayDaAggiornare = false;
                    }
                    int numeroMoltiplicatoreKeepAlive = keypad.leggoMoltiplicatoreKeepAlive();
                    if (numeroMoltiplicatoreKeepAlive == 0) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (numeroMoltiplicatoreKeepAlive > 0) {
                        modificaCvReale.tempoLimiteUsoKeepAlive(numeroMoltiplicatoreKeepAlive);
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    }
                } else if (menuLocomotiveDecoder == MenuLocomotiveDecoderEnum::RESET_ACCELERAZIONE_DECELERAZIONE) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                          "MENU LOCOMOTIVE -> SET DECODER -> RESET ACCELERAZIONE DECELERAZIONE",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    modificaCvReale.resetAccelerazioneDecelerazioneLocomotiva();

                    uscitaMenuReale.escoMenu(false, false, false, false);
                }
            }
        } else if (menuPrincipale == MenuPrincipaleEnum::WEB_SERVER) {
            if (menuWebServer == MenuWebServerEnum::SCELTA_IN_CORSO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                          "MENU PRINCIPALE -> WEB SERVER -> SCELTA IN CORSO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.webServer.menu();
                    display.homepage.displayDaAggiornare = false;
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    if (sceltaUtente == 1) {
                        menuWebServer = MenuWebServerEnum::MOSTRO_INDIRIZZO_IP;
                    }
                    display.homepage.displayDaAggiornare = true;
                }
            } else if (menuWebServer == MenuWebServerEnum::MOSTRO_INDIRIZZO_IP) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                          "MENU PRINCIPALE -> WEB SERVER -> MOSTRO INDIRIZZO IP",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.webServer.ip();
                    display.homepage.displayDaAggiornare = false;
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                }
            }
        } else if (menuPrincipale == MenuPrincipaleEnum::TRACCIATO) {
            if (menuTracciato == MenuTracciatoEnum::SCELTA_IN_CORSO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU TRACCIATO -> SCELTA IN CORSO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.tracciato.menu.mostra(motorShieldReale);
                    display.homepage.displayDaAggiornare = false;
                }
                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    if (sceltaUtente == 1) {
                        menuTracciato = MenuTracciatoEnum::CAMBIA_STATO_MOTOR_SHIELD;

                    } else if (sceltaUtente == 2) {
                        menuTracciato = MenuTracciatoEnum::SCAMBI;

                    } else if (sceltaUtente == 3) {
                        menuTracciato = MenuTracciatoEnum::SENSORI_IR;
                    }
                    display.homepage.displayDaAggiornare = true;
                }
            } else if (menuTracciato == MenuTracciatoEnum::CAMBIA_STATO_MOTOR_SHIELD) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                          "MENU TRACCIATO -> CAMBIA STATO MOTOR SHIELD",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.homepage.displayDaAggiornare = false;
                }

                if (motorShieldReale.isAccesa()) {
                    motorShieldReale.spegne();
                } else {
                    motorShieldReale.accende();
                }

                uscitaMenuReale.escoMenu(false, false, false, false);

            } else if (menuTracciato == MenuTracciatoEnum::SCAMBI) {
                if (menuTracciatoScambi == MenuTracciatoScambiEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU TRACCIATO -> SCAMBI",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.tracciato.scambi.menu();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuTracciatoScambi = MenuTracciatoScambiEnum::AZIONA;

                        } else if (sceltaUtente == 2) {
                            menuTracciatoScambi = MenuTracciatoScambiEnum::RESET;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuTracciatoScambi == MenuTracciatoScambiEnum::AZIONA) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU TRACCIATO -> SCAMBI -> AZIONA",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.tracciato.scambi.cambia();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int numeroScambioInt = keypad.leggoNumeroScambio();
                    if (numeroScambioInt == 0) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (scambiRelay.esisteNumeroScambio(numeroScambioInt)) {
                        if (scambiRelay.getScambio(numeroScambioInt)->isDestra()) {
                            scambiRelay.azionoScambio(-numeroScambioInt);
                        }

                        else {
                            scambiRelay.azionoScambio(numeroScambioInt);
                        }
                        uscitaMenuReale.escoMenu(true, false, false, false);
                    }

                } else if (menuTracciatoScambi == MenuTracciatoScambiEnum::RESET) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU TRACCIATO -> SCAMBI -> RESET",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.tracciato.scambi.reset();
                        display.homepage.displayDaAggiornare = false;
                    }

                    scambiRelay.reset();
                    uscitaMenuReale.escoMenu(true, false, false, false);
                }

            } else if (menuTracciato == MenuTracciatoEnum::SENSORI_IR) {
                if (menuTracciatoSensoriIr == MenuTracciatoSensoriIrEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU TRACCIATO -> SENSORI IR -> SCELTA IN CORSO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.tracciato.sensoriPosizione.displayMenuSensoriIr();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuTracciatoSensoriIr = MenuTracciatoSensoriIrEnum::TEST;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuTracciatoSensoriIr == MenuTracciatoSensoriIrEnum::TEST) {
                    if (menuTracciatoSensoriIrTest == MenuTracciatoSensoriIrTestEnum::INSERISCI_SENSORE) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU TRACCIATO -> SENSORI IR -> TEST -> INSERISCI SENSORE",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.tracciato.sensoriPosizione.inserisciSensore();
                            display.homepage.displayDaAggiornare = false;
                        }
                        numeroSensoreDisplayKeypad = keypad.leggoNumeroSensore();
                        if (sensoreDaUsare1 == 0) {
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (numeroSensoreDisplayKeypad > 0) {
                            menuTracciatoSensoriIrTest = MenuTracciatoSensoriIrTestEnum::STATO_SENSORE;
                            display.homepage.displayDaAggiornare = true;
                        }
                    } else if (menuTracciatoSensoriIrTest == MenuTracciatoSensoriIrTestEnum::STATO_SENSORE) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU TRACCIATO -> SENSORI IR -> TEST -> STATO SENSORE",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.tracciato.sensoriPosizione.parteFissa();
                            display.homepage.displayDaAggiornare = false;
                        }
                        display.tracciato.sensoriPosizione.parteVariabile(delayReale, sensoriPosizioneReali);

                        int sceltaUtente = keypad.leggoSceltaMenuUtente();
                        if (sceltaUtente == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        }
                    }
                }
            }

        } else if (menuPrincipale == MenuPrincipaleEnum::ACCESSORI) {
            if (menuAccessori == MenuAccessoriEnum::SCELTA_IN_CORSO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU ACCESSORI -> SCELTA IN CORSO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.accessori.menuPrincipale.displayMenuAccessori();
                    display.homepage.displayDaAggiornare = false;
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    if (sceltaUtente == 1) {
                        menuAccessori = MenuAccessoriEnum::LED;
                    } else if (sceltaUtente == 2) {
                        menuAccessori = MenuAccessoriEnum::AUDIO;
                    } else if (sceltaUtente == 3) {
                        menuAccessori = MenuAccessoriEnum::SENSORE_IMPRONTE;
                    } else if (sceltaUtente == 4) {
                        menuAccessori = MenuAccessoriEnum::BUZZER;
                    }
                    display.homepage.displayDaAggiornare = true;
                }
            } else if (menuAccessori == MenuAccessoriEnum::LED) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU ACCESSORI -> LED",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.accessori.led.menu();
                    display.homepage.displayDaAggiornare = false;
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    // DA FARE
                    /*
                        if (sceltaUtente == 1) {
                        setStripLedBianca();
                    } else if (sceltaUtente == 2) {
                        setStripLedRossa();
                    } else if (sceltaUtente == 3) {
                        setStripLedVerde();
                    } else if (sceltaUtente == 4) {
                        setStripLedBlu();
                    } else if (sceltaUtente == 5) {
                        setStripLedSpenta();
                        */
                }
                uscitaMenuReale.escoMenu(false, false, false, false);

            } else if (menuAccessori == MenuAccessoriEnum::AUDIO) {
                if (menuAccessoriAudio == MenuAccessoriAudioEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU ACCESSORI -> AUDIO -> SCELTA IN CORSO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.accessori.audio.menu();

                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuAccessoriAudio = MenuAccessoriAudioEnum::TEST;

                        } else if (sceltaUtente == 2) {
                            menuAccessoriAudio = MenuAccessoriAudioEnum::VOLUME;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuAccessoriAudio == MenuAccessoriAudioEnum::TEST) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU ACCESSORI -> AUDIO -> TEST",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

                        display.homepage.displayDaAggiornare = false;
                    }
                    riproducoFileAudioTest();
                    // riproducoPartenzaConvoglio(1, 9998, 14, 30, STAZIONE_CENTRALE, true, 3);
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (menuAccessoriAudio == MenuAccessoriAudioEnum::VOLUME) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU ACCESSORI -> AUDIO -> VOLUME",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.accessori.audio.inserimentoVolume();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int volume = keypad.leggoLivelloVolume();
                    if (volume == 0) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (volume > 0 && volume < 10) {
                        amplificatoreAudioReale.setVolume(63 / 9 * volume);
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    }
                }
            } else if (menuAccessori == MenuAccessoriEnum::SENSORE_IMPRONTE) {
                if (menuAccessoriSensoreImpronte == MenuAccessoriSensoreImpronteEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU ACCESSORI -> SENSORE IMPRONTE -> SCELTA IN CORSO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.accessori.sensoreImpronte.menu();

                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuAccessoriSensoreImpronte = MenuAccessoriSensoreImpronteEnum::AGGIUNGI_IMPRONTA;

                        } else if (sceltaUtente == 2) {
                            menuAccessoriSensoreImpronte = MenuAccessoriSensoreImpronteEnum::RESET_IMPRONTE;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuAccessoriSensoreImpronte == MenuAccessoriSensoreImpronteEnum::AGGIUNGI_IMPRONTA) {
                    if (menuAccessoriSensoreImpronteAggiungiImpronta ==
                        MenuAccessoriSensoreImpronteAggiungiImprontaEnum::SCELTA_IN_CORSO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU ACCESSORI -> SENSORE IMPRONTE -> AGGIUNGI IMPRONTA -> SCELTA "
                                                  "IN CORSO",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.accessori.sensoreImpronte.salvataggioNuovaImpronta();
                            display.homepage.displayDaAggiornare = false;
                        }

                        indiceUtenteSalvataggio = keypad.leggoSceltaMenuUtente();
                        if (indiceUtenteSalvataggio == 0) {
                            // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                            // principale
                            uscitaMenuReale.escoMenu(false, false, false, false);
                        } else if (indiceUtenteSalvataggio > 0) {
                            if (indiceUtenteSalvataggio <= NUMERO_UTENTI) {
                                menuAccessoriSensoreImpronteAggiungiImpronta =
                                    MenuAccessoriSensoreImpronteAggiungiImprontaEnum::ATTESA_DITO;
                                display.homepage.displayDaAggiornare = true;
                            }
                        }

                    } else if (menuAccessoriSensoreImpronteAggiungiImpronta ==
                               MenuAccessoriSensoreImpronteAggiungiImprontaEnum::ATTESA_DITO) {
                        if (display.homepage.displayDaAggiornare) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "MENU ACCESSORI -> SENSORE IMPRONTE -> AGGIUNGI IMPRONTA -> ATTESA "
                                                  "DITO",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            display.logIn.appoggiaDito();
                            display.homepage.displayDaAggiornare = false;
                        }

                        // Aggiorno numero delle impronte presenti sul database
                        byte improntePresenti = sensoreImpronteReale.getNumeroImpronteDatabase();

                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Sono presenti ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(improntePresenti).c_str(),
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, " impronte nel database",
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                        byte indiceSalvataggioNuovaImprontaDatabaseSensore = improntePresenti + 1;

                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Salvo impronta con ID n° ",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              std::to_string(indiceSalvataggioNuovaImprontaDatabaseSensore).c_str(),
                                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                        // Rimango in attesa dell'impronta sul sensore
                        if (sensoreImpronteReale.getFingerprintEnroll(indiceSalvataggioNuovaImprontaDatabaseSensore)) {
                            byte indiceSalvataggioArrayImpronteUtente =
                                utenti.lista[indiceUtenteSalvataggio].numeroImpronteSalvate;
                            utenti.lista[indiceUtenteSalvataggio].idImpronte[indiceSalvataggioArrayImpronteUtente] =
                                indiceSalvataggioNuovaImprontaDatabaseSensore;

                            // Dopo aver registrato l'impronta aumento il contatore
                            utenti.lista[indiceUtenteSalvataggio].numeroImpronteSalvate += 1;

                            uscitaMenuReale.escoMenu(false, false, false, true);
                        }
                    }

                } else if (menuAccessoriSensoreImpronte == MenuAccessoriSensoreImpronteEnum::RESET_IMPRONTE) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU ACCESSORI -> SENSORE IMPRONTE -> RESET IMPRONTE",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.accessori.sensoreImpronte.confermaResetDatabase();
                        display.homepage.displayDaAggiornare = false;
                    }

                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "Reset del database delle impronte...",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            sensoreImpronteReale.resetDatabase();
                            for (int i = 1; i < NUMERO_UTENTI; i++) {
                                utenti.lista[i].numeroImpronteSalvate = 0;
                                for (int r = 0; r < NUMERO_IMPRONTE_PER_UTENTE; r++) {
                                    utenti.lista[i].idImpronte[r] = 0;
                                }
                            }
                            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                                  "Reset del database delle impronte... OK",
                                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                            uscitaMenuReale.escoMenu(false, false, false, true);
                        }
                    }
                }
            } else if (menuAccessori == MenuAccessoriEnum::BUZZER) {
                if (menuAccessoriBuzzer == MenuAccessoriBuzzerEnum::SCELTA_IN_CORSO) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU ACCESSORI -> BUZZER -> SCELTA IN CORSO",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.accessori.buzzer.menu();
                        display.homepage.displayDaAggiornare = false;
                    }
                    int sceltaUtente = keypad.leggoSceltaMenuUtente();
                    if (sceltaUtente == 0) {
                        // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                        // principale
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (sceltaUtente > 0) {
                        if (sceltaUtente == 1) {
                            menuAccessoriBuzzer = MenuAccessoriBuzzerEnum::TEST;

                        } else if (sceltaUtente == 2) {
                            menuAccessoriBuzzer = MenuAccessoriBuzzerEnum::CAMBIO_VOLUME;
                        }
                        display.homepage.displayDaAggiornare = true;
                    }
                } else if (menuAccessoriBuzzer == MenuAccessoriBuzzerEnum::TEST) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU ACCESSORI -> BUZZER -> TEST",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.homepage.displayDaAggiornare = false;
                    }
                    buzzerRealeArduinoMaster.beepErrore();

                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (menuAccessoriBuzzer == MenuAccessoriBuzzerEnum::CAMBIO_VOLUME) {
                    if (display.homepage.displayDaAggiornare) {
                        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                              "MENU ACCESSORI -> BUZZER -> CAMBIO VOLUME",
                                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                        display.accessori.audio.inserimentoVolume();
                        display.homepage.displayDaAggiornare = false;
                    }
                    int volume = keypad.leggoLivelloVolume();
                    if (volume == 0) {
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    } else if (volume > 0 && volume < 10) {
                        byte volumeMassimo = 255;
                        buzzerRealeArduinoMaster.setVolume(volumeMassimo / 9 * volume);
                        buzzerRealeArduinoMaster.beepOk();
                        uscitaMenuReale.escoMenu(false, false, false, false);
                    }
                }
            }
        } else if (menuPrincipale == MenuPrincipaleEnum::MONITORAGGIO) {
            if (menuMonitoraggio == MenuMonitoraggioEnum::SCELTA_IN_CORSO) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU MONITORAGGIO -> SCELTA IN CORSO",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.monitoraggio.menu.mostra();
                    display.homepage.displayDaAggiornare = false;
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                    // principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    if (sceltaUtente == 1) {
                        menuMonitoraggio = MenuMonitoraggioEnum::TENSIONI_1;

                    } else if (sceltaUtente == 2) {
                        menuMonitoraggio = MenuMonitoraggioEnum::ASSORBIMENTI;
                    }
                    display.homepage.displayDaAggiornare = true;
                }

            } else if (menuMonitoraggio == MenuMonitoraggioEnum::TENSIONI_1) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU MONITORAGGIO -> TENSIONI 1",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.monitoraggio.tensioni.displayTensioniParteFissa1();
                    display.homepage.displayDaAggiornare = false;
                }

                // Se è passato abbastanza tempo, allora leggeStato e stampo sul display i valori
                if (millis() - display.monitoraggio.tensioni.millisPrecedenteMonitoraggio >=
                    INTERVALLO_STAMPA_MONITORAGGIO_TENSIONI) {
                    display.monitoraggio.tensioni.millisPrecedenteMonitoraggio = millis();

                    display.monitoraggio.tensioni.displayTensioniParteVariabile1();
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                    // principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                } else if (sceltaUtente > 0) {
                    menuMonitoraggio = MenuMonitoraggioEnum::TENSIONI_2;
                    display.homepage.displayDaAggiornare = true;
                }

            } else if (menuMonitoraggio == MenuMonitoraggioEnum::TENSIONI_2) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU MONITORAGGIO -> TENSIONI 2",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.monitoraggio.tensioni.displayTensioniParteFissa2();
                    display.homepage.displayDaAggiornare = false;
                }

                // Se è passato abbastanza tempo, allora leggeStato e stampo sul display i valori
                if (millis() - display.monitoraggio.tensioni.millisPrecedenteMonitoraggio >=
                    INTERVALLO_STAMPA_MONITORAGGIO_TENSIONI) {
                    display.monitoraggio.tensioni.millisPrecedenteMonitoraggio = millis();

                    display.monitoraggio.tensioni.displayTensioniParteVariabile2();
                }
                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                    // principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                }

            } else if (menuMonitoraggio == MenuMonitoraggioEnum::ASSORBIMENTI) {
                if (display.homepage.displayDaAggiornare) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "MENU MONITORAGGIO -> ASSORBIMENTI",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                    display.monitoraggio.assorbimenti.parteFissa();
                    display.homepage.displayDaAggiornare = false;
                }

                // Se è passato abbastanza tempo, allora leggeStato e stampo sul display i valori
                if (millis() - display.monitoraggio.tensioni.millisPrecedenteMonitoraggio >=
                    INTERVALLO_STAMPA_MONITORAGGIO_TENSIONI) {
                    display.monitoraggio.tensioni.millisPrecedenteMonitoraggio = millis();

                    /*
                    stampo("Tracciato: ");
                    bufferConversioneNumeroInStringa =
                    std::to_string(statoSensoreCorrenteIna219Campionato);
                    stampo(bufferConversioneNumeroInStringa.c_str());
                    stampo("\n");

                    stampo("Microcontroller e componenti ausiliari: ");
                    bufferConversioneNumeroInStringa =
                    std::to_string(statoSensoreCorrenteIna260Campionato);
                    stampo(bufferConversioneNumeroInStringa.c_str());
                    stampo("\n");
                    */

                    display.monitoraggio.assorbimenti.parteVariabile(sensoreCorrenteIna219Reale.getCorrente(),
                                                                     sensoreCorrenteIna260Reale.getCorrente());
                }

                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 0) {
                    // Se viene premuto il tasto 0 durante una scelta dell'utente allora torno al menù
                    // principale
                    uscitaMenuReale.escoMenu(false, false, false, false);
                }
            }
        }
    }
}

void MenuDisplayKeypadCore::assegnoConvogliInUsoDisplay() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Assegno convogli in uso display...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    loggerReale.cambiaTabulazione(1);
    // Trovo due convogli non vuoti da assegnare inizialmente a quelli visualizzati sul display
    bool convoglioCorrente1Trovato = false;

    // Imposto idConvoglioCorrente1Display e idConvoglioCorrente2Display a 0.
    // In questo modo se non trovo nessun convoglio sul tracciato stampo 0 sul display
    displayReale.homepage.idConvoglioCorrente1Display = 0;
    displayReale.homepage.idConvoglioCorrente2Display = 0;

    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        if (!convoglioCorrente1Trovato) {
            if (convogliReali.getConvoglio(i, false)->isInUso() &&
                convogliReali.getConvoglio(i, false)->isPresenteSulTracciato()) {
                displayReale.homepage.idConvoglioCorrente1Display = i;
                convoglioCorrente1Trovato = true;
            }
        } else {
            if (convogliReali.getConvoglio(i, false)->isInUso() &&
                convogliReali.getConvoglio(i, false)->isPresenteSulTracciato()) {
                displayReale.homepage.idConvoglioCorrente2Display = i;
                break;
            }
        }
    }
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                          "Convoglio 1: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                          std::to_string(displayReale.homepage.idConvoglioCorrente1Display).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                          "Convoglio 2: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                          std::to_string(displayReale.homepage.idConvoglioCorrente2Display).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    loggerReale.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Assegno convogli in uso display... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MenuDisplayKeypadCore::gestiscoMenuPrincipale(char sceltaMenu,
                                                   MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypad) {
    switch (sceltaMenu) {
        case '1': {
            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::CONVOGLI;
            displayReale.homepage.displayDaAggiornare = true;
            break;
        }
        case '2': {
            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::LOCOMOTIVE;
            displayReale.homepage.displayDaAggiornare = true;
            break;
        }
        case '3': {
            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::WEB_SERVER;
            displayReale.homepage.displayDaAggiornare = true;
            break;
        }
        case '4': {
            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::TRACCIATO;
            displayReale.homepage.displayDaAggiornare = true;
            break;
        }

        case '5': {
            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::ACCESSORI;
            displayReale.homepage.displayDaAggiornare = true;
            break;
        }

        case '6': {
            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::MONITORAGGIO;
            displayReale.homepage.displayDaAggiornare = true;
            break;
        }

        case 'A': {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Shortcut tasto A",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            convoglioCorrenteDisplay = &displayReale.homepage.idConvoglioCorrente1Display;

            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::CONVOGLI;
            menuConvogli = MenuConvogliEnum::GESTISCI;
            menuConvogliGestisci = MenuConvogliGestisciEnum::GESTISCI_LOCO;

            if (!convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->hasDoppiaLocomotiva()) {
                menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::AGGIUNGI;

            } else {
                menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::ELIMINA;
            }

            displayReale.homepage.displayDaAggiornare = true;
            break;
        }

        case 'B': {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Shortcut tasto B",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Cambio luci convoglio 1",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            menuDisplayKeypad.cambioLuciConvoglio1EAggiornoDisplay(convogliReali);
            break;
        }
        case 'C': {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Shortcut tasto C",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            convoglioCorrenteDisplay = &displayReale.homepage.idConvoglioCorrente2Display;

            fuoriMenu = FuoriMenuEnum::MENU_PRINCIPALE;
            menuPrincipale = MenuPrincipaleEnum::CONVOGLI;
            menuConvogli = MenuConvogliEnum::GESTISCI;
            menuConvogliGestisci = MenuConvogliGestisciEnum::GESTISCI_LOCO;

            if (!convogliReali.getConvoglio(*convoglioCorrenteDisplay, false)->hasDoppiaLocomotiva()) {
                menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::AGGIUNGI;
                displayReale.homepage.displayDaAggiornare = true;
            } else {
                menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::ELIMINA;
                displayReale.homepage.displayDaAggiornare = true;
            }

            displayReale.homepage.displayDaAggiornare = true;
            break;
        }
        case 'D': {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Shortcut tasto D",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Cambio luci convoglio 2",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            menuDisplayKeypad.cambioLuciConvoglio2EAggiornoDisplay(convogliReali);
            break;
        }
        case '*': {
            menuDisplayKeypad.cambioConvoglioCorrente1EAggiornoDisplay();
            break;
        }
        case '#': {
            if (convogliReali.getConvoglio(displayReale.homepage.idConvoglioCorrente2Display, false)->isInUso()) {
                menuDisplayKeypad.cambioConvoglioCorrente2EAggiornoDisplay();
            }
            break;
        }
    }
}