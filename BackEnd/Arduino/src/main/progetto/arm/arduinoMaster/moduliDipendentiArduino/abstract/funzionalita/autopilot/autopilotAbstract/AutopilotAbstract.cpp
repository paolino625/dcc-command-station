// *** INCLUDE *** //

#include "AutopilotAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/stopAutopilot/StopAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"

// *** DEFINIZIONE FUNZIONI *** //

bool AutopilotAbstract::avviaAutopilotModalitaStazioneSuccessiva() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Avvio autopilot modalità stazione successiva",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(1);

    // Se l'autopilot è già attivo, non posso avviarlo di nuovo
    if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "L'autopilot è già attivo",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-1);
        return false;
    }

    inizializzoStatoSezioni();

    aggiornoStatoInizializzazioneAutopilotConvogli();

    // Quando l'autopilot viene attivato, alcuni sensori potrebbero ritrovarsi nello stato in cui si aspettano il
    // passaggio di seconda locomotiva. È necessario dunque assicurarsi che tutti i sensori siano resettati.
    sensoriPosizione.resetInAttesaPassaggioPrimaLocomotivaConvoglio();

    if (!convogliPresentiSulTracciatoInizializzati()) {
        logger.cambiaTabulazione(-1);
        return false;
    }

    statoAutopilot = StatoAutopilot::ATTIVO;
    modalitaAutopilot = ModalitaAutopilot::STAZIONE_SUCCESSIVA;

    infoAutopilotAggiornamentoMqttRichiesto = true;

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Avvio autopilot modalità stazione successiva... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    return true;
}

bool AutopilotAbstract::avviaAutopilotModalitaCasuale(bool ottimizzazioneAssegnazioneConvogliAiBinari) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Avvio autopilot modalità casuale",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(1);

    // Se l'autopilot è già attivo, non posso avviarlo di nuovo
    if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "L'autopilot è già attivo",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-1);
        return false;
    }

    inizializzoStatoSezioni();

    aggiornoStatoInizializzazioneAutopilotConvogli();

    // Quando l'autopilot viene attivato, alcuni sensori potrebbero ritrovarsi nello stato in cui si aspettano il
    // passaggio di seconda locomotiva. È necessario dunque assicurarsi che tutti i sensori siano resettati.
    sensoriPosizione.resetInAttesaPassaggioPrimaLocomotivaConvoglio();

    if (!convogliPresentiSulTracciatoInizializzati()) {
        logger.cambiaTabulazione(-1);
        return false;
    }

    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, true);

        if (convoglio->isInUso()) {
            convoglio->autopilotConvoglio->inizializzoAutopilotModalitaCasuale(
                ottimizzazioneAssegnazioneConvogliAiBinari);
        }
    }

    statoAutopilot = StatoAutopilot::ATTIVO;
    modalitaAutopilot = ModalitaAutopilot::APPROCCIO_CASUALE_TUTTI_CONVOGLI;

    infoAutopilotAggiornamentoMqttRichiesto = true;

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Avvio autopilot modalità casuale... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    return true;
};

bool AutopilotAbstract::avviaAutopilotModalitaCasualeSceltaConvogli(IdConvoglioTipo *arrayIdConvogli,
                                                                    int numeroIdConvogli,
                                                                    bool ottimizzazioneAssegnazioneConvogliAiBinari) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Avvio autopilot modalità casuale scelta convogli",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    logger.cambiaTabulazione(1);

    // Se l'autopilot è già attivo, non posso avviarlo di nuovo
    if (statoAutopilot != StatoAutopilot::NON_ATTIVO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "L'autopilot è già attivo",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(-1);
        return false;
    }

    inizializzoStatoSezioni();

    aggiornoStatoInizializzazioneAutopilotConvogli();

    // Quando l'autopilot viene attivato, alcuni sensori potrebbero ritrovarsi nello stato in cui si aspettano il
    // passaggio di seconda locomotiva. È necessario dunque assicurarsi che tutti i sensori siano resettati.
    sensoriPosizione.resetInAttesaPassaggioPrimaLocomotivaConvoglio();

    if (!convogliPresentiSulTracciatoInizializzati()) {
        logger.cambiaTabulazione(-1);
        return false;
    }

    inizializzaModalitaAutopilotApproccioCasualeSceltaConvogli(arrayIdConvogli, numeroIdConvogli);

    // Imposto l'approccio casuale solo per i convogli selezionati dall'utente
    for (int i = 0; i < numeroConvogliSceltiModalitaAutopilotApproccioCasuale; i++) {
        IdConvoglioTipo idConvoglio = arrayIdConvogli[i];
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(idConvoglio, true);
        convoglio->autopilotConvoglio->inizializzoAutopilotModalitaCasuale(ottimizzazioneAssegnazioneConvogliAiBinari);
    }

    statoAutopilot = StatoAutopilot::ATTIVO;
    modalitaAutopilot = ModalitaAutopilot::APPROCCIO_CASUALE_SCELTA_CONVOGLI;

    infoAutopilotAggiornamentoMqttRichiesto = true;

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Avvio autopilot modalità casuale scelta convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    return true;
}

/*
Quando l'utente vuole fermare l'autopilot, deve chiamare questa funzione.
Lo stato dell'autopilot non può essere cambiato direttamente in NON_ATTIVO: questo perché potrebbero esserci
ancora dei convogli in movimento che non hanno finito il percorso a loro assegnato e non vogliamo che si fermino
a caso sul tracciato. I convogli, invece, devono fermarsi sui binari di stazione, in questo modo non ci
sarebbero problemi nel caso in cui l'autopilot dovesse venire riavviato di nuovo. Lo stato dell'autopilot viene
cambiato in TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO.
 */
void AutopilotAbstract::ferma(bool shutdownForzato, bool inviaPacchettoTracciatoStopNelCasoDiShutdownForzato) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Fermo autopilot...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);

    // Nota: effettuo l'operazione di reset Autopilot anche quando lo stato è NON_ATTIVO. Potrebbe sembrare superfluo ma
    // non lo è. Potrebbe succedere che l'autopilot non è stato avviato ma potrebbero essere state chiamate le funzioni
    // setPosizioneConvoglio: in questo caso alcune sezioni sarebbero bloccate. È necessario dunque effettuare il
    // reset in ogni caso per essere sicuri di avere una situazione pulita.

    if (shutdownForzato) {
        // Se è stato richiesto uno shutdown forzato effettuo le seguenti operazioni.

        if (inviaPacchettoTracciatoStopNelCasoDiShutdownForzato) {
            // Aziono immediatamente lo stop di emergenza.
            stopEmergenza.aziona(false);
        }

        spengoDefinitivamente();
    } else {
        if (statoAutopilot == StatoAutopilot::ATTIVO) {
            statoAutopilot = StatoAutopilot::TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO;
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Stato autopilot: TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        } else {
            if (inviaPacchettoTracciatoStopNelCasoDiShutdownForzato) {
                // Fermo immediatamente tutti i convogli
                invioStatoTracciato.stopEmergenza();
            }

            spengoDefinitivamente();
        }
    }

    infoAutopilotAggiornamentoMqttRichiesto = true;

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Fermo autopilot... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// Questa funzione viene chiamata nel loop di ArduinoMaster in modo tale che l'autopilot possa essere eseguito
// in base al suo stato attuale
void AutopilotAbstract::processo() {
    // Se è stato richiesto lo stop dell'autopilot, lo eseguo
    if (stopAutopilot) {
        ferma(true, false);
        // Resetto la variabile flag
        stopAutopilot = false;
    }

    switch (statoAutopilot) {
        case StatoAutopilot::ATTIVO:
            controlloPresenzaTrenoFantasma();
            eseguoAutopilotConvogli();
            break;

        case StatoAutopilot::NON_ATTIVO:
            break;

        case StatoAutopilot::TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO:
            controlloPresenzaTrenoFantasma();
            /*
            Prima che l'autopilot passi allo stato NON_ATTIVO, sarà necessario aspettare che tutti i convogli
            che stanno eseguendo un percorso lo finiscano (in caso di un percorso loop, il convoglio arriverà
            alla stazione di destinazione successiva).
             */
            eseguoAutopilotConvogli();

            // Se tutti i convogli sono fermi, allora posso cambiare lo stato dell'autopilot in NON_ATTIVO
            bool convogliFermi = true;
            for (int i = 1; i < NUMERO_CONVOGLI; i++) {
                ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, true);
                if (convoglio->presenteSulTracciato) {
                    if (convoglio->autopilotConvoglio->statoInizializzazioneAutopilotConvoglio !=
                        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT) {
                        convogliFermi = false;
                    }
                }
            }

            if (convogliFermi) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Tutti i convogli sono fermi...",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Stato autopilot: NON_ATTIVO",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                spengoDefinitivamente();
            }

            break;
    }
}

void AutopilotAbstract::inizializzoStatoSezioni() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo stato sezioni...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);

    // Per prima cosa resetto lo stato di tutte le sezioni
    sezioni.resetLock();

    // Blocco le sezioni occupate dai convogli
    convogli.bloccoSezioniOccupate();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo stato sezioni... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
}

void AutopilotAbstract::eseguoAutopilotConvogli() {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, true);
        convoglio->autopilotConvoglio->processo(true);
    }
}

/*
Controlla che tutti i convogli presenti sul tracciato siano stati inizializzati, ovvero l'autopilot conosce la
loro posizione

Durante l'avvio di qualsiasi configurazione Autopilot bisogna controllare che tutti i convogli presenti sul
tracciato siano su uno dei seguenti stati:
- PRONTO_PER_CONFIGURAZIONE_AUTOPILOT: il convoglio è stato registrato sul tracciato ma non è stato configurato
l'autopilot.
- CONFIGURAZIONE_AUTOPILOT_COMPLETATA: il convoglio è stato registrato sul tracciato e l'autopilot è stato
impostato.

 Se c'è un convoglio che non è in uno di questi due stati, l' autopilot non può essere avviato. Questo perché c'è
almeno un convoglio sul tracciato di cui non si conosce la posizione: questo potrebbe essere un problema in quanto
il sistema non è a conoscenza della sua posizione e potrebbero verificarsi degli incidenti con altri convogli.
 */

bool AutopilotAbstract::convogliPresentiSulTracciatoInizializzati() {
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        // Se il convoglio è presente sul tracciato, mi aspetto che questo sia stato configurato per l'autopilot
        // o quanto meno inizializzato: l'autopilot deve conoscere la sua posizione.
        if (convogli.getConvoglio(i, true)->presenteSulTracciato) {
            ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, true);
            if (convoglio->autopilotConvoglio->statoInizializzazioneAutopilotConvoglio !=
                    StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT &&
                convoglio->autopilotConvoglio->statoInizializzazioneAutopilotConvoglio !=
                    StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "L'autopilot del convoglio n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, std::to_string(i).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR,
                                      " non è in uno dei due stati ammissibili: "
                                      "PRONTO_PER_CONFIGURAZIONE_AUTOPILOT oppure "
                                      "CONFIGURAZIONE_AUTOPILOT_COMPLETATA",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                return false;
            }
        }
    }
    return true;
}

// Questa funzione deve essere chiamata prima di avviare l'autopilot in modalità
// MODALITA_AUTOPILOT_APPROCCIO_CASUALE_SCELTA_CONVOGLI
bool AutopilotAbstract::inizializzaModalitaAutopilotApproccioCasualeSceltaConvogli(IdConvoglioTipo *arrayIdConvogli,
                                                                                   int numeroIdConvogli) {
    modalitaAutopilotApproccioCasualeSceltaConvogliInizializzata = true;

    // Devo controllare che i convogli siano presenti sul tracciato
    for (int i = 0; i < numeroIdConvogli; i++) {
        if (!convogli.getConvoglio(arrayIdConvogli[i], true)->presenteSulTracciato) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Il convoglio n°",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, std::to_string(arrayIdConvogli[i]).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "non è presente sul tracciato",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

            return false;
        }
    }

    // Copio le info in variabili locali

    // Controllo se convogliSceltiModalitaAutopilotApproccioCasuale è già stato allocato in precedenza e in caso lo
    // dealloco
    if (convogliSceltiModalitaAutopilotApproccioCasuale != nullptr) {
        delete[] convogliSceltiModalitaAutopilotApproccioCasuale;
    }
    convogliSceltiModalitaAutopilotApproccioCasuale = new IdConvoglioTipo[numeroIdConvogli];
    for (int i = 0; i < numeroIdConvogli; i++) {
        convogliSceltiModalitaAutopilotApproccioCasuale[i] = arrayIdConvogli[i];
    }

    numeroConvogliSceltiModalitaAutopilotApproccioCasuale = numeroIdConvogli;

    return true;
}

void AutopilotAbstract::spengoDefinitivamente() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Spengo definitivamente autopilot...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    resetAutopilot();
    statoAutopilot = StatoAutopilot::NON_ATTIVO;

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Spengo definitivamente autopilot... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// Arduino salva le posizioni dei convogli su USB. Questa funzione viene chiamata prima di avviare l'autopilot.
// I convogli che hanno già una posizione salvata devono essere considerati pronti, anche se non è stata chiamata la
// funzione di avvio autopilot da quando Arduino è stato acceso
void AutopilotAbstract::aggiornoStatoInizializzazioneAutopilotConvogli() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno stato inizializzazione autopilot convogli...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, true);
        if (convoglio->presenteSulTracciato) {
            if (convoglio->autopilotConvoglio->posizioneConvoglio.validita ==
                ValiditaPosizioneConvoglio::POSIZIONE_VALIDA) {
                // Se il convoglio ha una posizione registrata, allora è pronto per l'autopilot
                convoglio->autopilotConvoglio->setStatoInizializzazioneAutopilotConvoglio(
                    StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT);
            } else {
                // Altrimenti il convoglio non è pronto per l'autopilot
                convoglio->autopilotConvoglio->setStatoInizializzazioneAutopilotConvoglio(
                    StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);
            }
        }
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aggiorno stato inizializzazione autopilot convogli... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void AutopilotAbstract::resetAutopilot() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset autopilot...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    logger.cambiaTabulazione(1);
    modalitaAutopilotApproccioCasualeSceltaConvogliInizializzata = false;

    // Resetto lo stato AutopilotConvoglio di tutti i convogli. Questo significa che dopo aver fermato l'autopilot,
    // è necessario reimpostare la posizione di tutti i convogli. Questo perché nel frattempo la posizione dei
    // convogli potrebbe essere cambiata. In particolare se non c'è stato un softShutdown (è stato premuto il
    // pulsante di emergenza), il sistema non sa la posizione dei convogli.
    for (int i = 1; i < NUMERO_CONVOGLI; i++) {
        ConvoglioAbstractReale *convoglio = convogli.getConvoglio(i, true);
        convoglio->autopilotConvoglio->resetTotale();
    }

    inizializzoStatoSezioni();

    sensoriPosizione.reset(true);

    logger.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset autopilot... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void AutopilotAbstract::controlloPresenzaTrenoFantasma() {
    /*
    Per ogni sensore di posizione, verifico il suo stato.
    Se un sensore è attivo, significa che è stato appena attivato: devo assicurarmi che ci fosse un convoglio il cui
    passaggio era previsto sopra il sensore. Altrimenti significa che c'è un problema: per qualche motivo la
    situazione reale non rispecchia quella prevista dall'Autopilot. In questo caso è necessario fermare
    immediatamente l'autopilot.
    */
    for (int i = 1; i < NUMERO_SENSORI_POSIZIONE_TOTALI; i++) {
        SensorePosizioneAbstract *sensorePosizione = sensoriPosizione.getSensorePosizione(i);
        // Uso getStato affinché la classe AutopilotConvoglioAbstract possa usare fetchStato() per leggere lo stato.
        if (sensorePosizione->getStato()) {
            SezioneAbstract *sezione = sezioni.getSezioneDelSensore(sensorePosizione->getId());
            // Se si tratta di un sensore posizionato su un binario di stazione e la sezione è bloccata, il sensore può essere stato azionato dal convoglio in partenza. In questo caso non considero l'azionamento del sensore come un errore.
            if (!(sezione->getId().isBinarioStazione() && sezione->isLocked())) {
                // Se nessun convoglio era previsto sul sensore, allora fermiamo l'autopilot immediatamente.
                if (sensorePosizione->getIdConvoglioInAttesa() == 0) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Treno fantasma rilevato!",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Il sensore n° ",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, std::to_string(i).c_str(),
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR,
                                          " è stato attivato ma non era previsto il passaggio di nessun "
                                          "convoglio sopra di esso.",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

                    IdSensorePosizioneTipo idSensore = i;
                    SezioneAbstract *sezione = sezioni.getSezioneDelSensore(idSensore);

                    char *idSezioneStringa = sezione->getId().toString();

                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Il sensore si trova sulla sezione con ",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::ERROR, idSezioneStringa,
                                           InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true, true);

                    // Quando un treno fantasma passa sopra un sensore, il sensore da quel momento in poi si aspetta la
                    // seconda locomotiva del convoglio. Ma nel caso di un treno fantasma questo non deve avvenire, è
                    // necessario resettare il sensore in modo tale che si aspetti di nuovo la prima locomotiva di un
                    // convoglio.
                    sensorePosizione->resetInAttesaPassaggioPrimaLocomotivaConvoglio();

                    logger.cambiaTabulazione(1);
                    ferma(true, true);
                    logger.cambiaTabulazione(-1);
                }
            }
        }
    }
}