#ifndef DCC_COMMAND_STATION_LOCOMOTIVAABSTRACT_H
#define DCC_COMMAND_STATION_LOCOMOTIVAABSTRACT_H

// *** INCLUDE *** //

#include "NumeroStepVelocita.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/Rotabili.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/tempo/TempoAbstract.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

// Parte da posizione 0 perché c'è F0
#define NUMERO_FUNZIONI_AUSILIARI_DECODER 13

// *** STRUCT *** //

typedef struct Direzione {
    bool invertita;      // Salvato nel file JSON
    bool avantiDisplay;  // Salvato nel file JSON
    bool avantiDecoder;  // Non salvata nel file JSON
} Direzione;

typedef struct MappingVelocitaStep {
    StepVelocitaDecoderTipo avanti[NUMERO_STEP_VELOCITA_DISPLAY_KMH];
    StepVelocitaDecoderTipo indietro[NUMERO_STEP_VELOCITA_DISPLAY_KMH];
} MappingVelocitaStep;

typedef struct TempoFunzionamento {
    TimestampTipo ultimaManutenzione;  // In secondi
    TimestampTipo totale;              // In secondi
} TempoFunzionamento;

typedef struct Velocita {
    VelocitaRotabileKmHTipo massimaKmH;
    VelocitaRotabileKmHTipo attuale = 0;
    VelocitaRotabileKmHTipo impostata = 0;
} Velocita;

// *** CLASSE *** //

class LocomotivaAbstract {
    // *** VARIABILI *** //

   private:
    TempoAbstract& tempo;
    LoggerAbstract& logger;

    IdLocomotivaTipo id = 0;  // Imposto 0 per capire quando una Locomotiva nell'array locomotive
                              // viene allocata ma non usata.
    IndirizzoDecoderTipo indirizzoDecoderDCC;

    Direzione direzione;
    InfoRotabile infoRotabile;
    LunghezzaRotabileTipo lunghezzaCm;
    TempoFunzionamento tempoFunzionamento;
    MappingVelocitaStep mappingVelocitaStep;
    Velocita velocita;
    TimestampTipo timerInizio;

    bool statoFunzioniAusiliari[NUMERO_FUNZIONI_AUSILIARI_DECODER];

    // *** COSTRUTTORE *** //

   public:
    LocomotivaAbstract(IdLocomotivaTipo id, TempoAbstract& tempo, LoggerAbstract& logger)
        : tempo(tempo), logger(logger) {
        this->id = id;
        for (int i = 0; i < NUMERO_FUNZIONI_AUSILIARI_DECODER; i++) {
            statoFunzioniAusiliari[i] = false;
        }
    }

    // *** METODI PUBBLICI *** //
   public:
    void inizializza();
    void cambiaDirezione();
    void aggiornaDirezioneAvantiDecoder();
    void accendeLuciVagoni();
    void aggiornaLuciAusiliarie(bool luciConvoglioAccese);
    void spegneLuciAusiliarie();
    void accendeLuciBasilari();
    void spegneLuciBasilari();
    void avviaTimerParziale();
    void fermaTimerParziale();

    // *** GETTER *** //

    IdLocomotivaTipo getId() const;
    IndirizzoDecoderTipo getIndirizzoDecoderDcc() const;
    char* getNome();
    char* getCodice();
    TipologiaRotabile getTipologia();
    CategoriaRotabilePasseggeri getCategoria();
    LivreaRotabile getLivrea();
    ProduttoreRotabile getProduttore();
    char* getNumeroModello();
    LunghezzaRotabileTipo getLunghezzaCm() const;
    bool getDirezioneInvertita() const;
    bool getDirezioneAvantiDisplay() const;
    TimestampTipo getTempoFunzionamentoUltimaManutenzione() const;
    TimestampTipo getTempoFunzionamentoTotale() const;
    VelocitaRotabileKmHTipo getVelocitaMassimaKmH() const;
    StepVelocitaDecoderTipo getMappingVelocitaStepAvanti(StepVelocitaKmHTipo step);
    StepVelocitaDecoderTipo getMappingVelocitaStepIndietro(StepVelocitaKmHTipo step);
    VelocitaRotabileKmHTipo getVelocitaMassima() const;
    VelocitaRotabileKmHTipo getVelocitaAttuale() const;
    VelocitaRotabileKmHTipo getVelocitaImpostata() const;
    bool getDirezioneAvantiDecoder() const;
    TimestampTipo getTimerInizio() const;
    bool getStatoFunzioniAusiliari(byte numeroFunzioneAusiliare);

    // *** SETTER *** //

    void setId(IdLocomotivaTipo idNuovo);
    void setIndirizzoDecoderDcc(IndirizzoDecoderTipo indirizzoDecoderDCCNuovo);
    void setNome(char* nomeNuovo);
    void setCodice(char* codiceNuovo);
    void setTipologia(TipologiaRotabile tipologiaNuovo);
    void setCategoria(CategoriaRotabilePasseggeri categoriaNuovo);
    void setLivrea(LivreaRotabile livreaNuovo);
    void setProduttore(ProduttoreRotabile produttoreNuovo);
    void setNumeroModello(char* numeroModelloNuovo);
    void setLunghezzaCm(LunghezzaRotabileTipo lunghezzaCmNuovo);
    void setDirezioneInvertita(bool direzioneInvertitaNuovo);
    void setDirezioneAvantiDisplay(bool direzioneAvantiDisplayNuovo);
    void setTempoFunzionamentoUltimaManutenzione(TimestampTipo tempoFunzionamentoUltimaManutenzioneNuovo);
    void setTempoFunzionamentoTotale(TimestampTipo tempoFunzionamentoTotaleNuovo);
    void setVelocitaMassimaKmH(VelocitaRotabileKmHTipo velocitaMassimaKmHNuovo);
    void setVelocitaAttuale(int velocitaAttualeNuovo);
    void setVelocitaImpostata(int velocitaImpostataNuovo);
    void setDirezioneAvantiDecoder(bool direzioneAvantiDecoderNuovo);
    void setTimerInizio(TimestampTipo timerInizioNuovo);
    void setStatoFunzioniAusiliari(int idFunzioneAusiliare, bool stato);
    void setMappingVelocitaStepAvanti(StepVelocitaKmHTipo stepVelocitaKmH, StepVelocitaDecoderTipo stepVelocitaDecoder);
    void setMappingVelocitaStepIndietro(StepVelocitaKmHTipo stepVelocitaKmH,
                                        StepVelocitaDecoderTipo stepVelocitaDecoder);
    void setIndirizzoDcc(int nuovoIndirizzoLocomotiva);

    char* toString();
};

#endif