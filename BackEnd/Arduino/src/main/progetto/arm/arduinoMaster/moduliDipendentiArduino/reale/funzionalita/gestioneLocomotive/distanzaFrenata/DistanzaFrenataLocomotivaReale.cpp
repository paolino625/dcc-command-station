// *** INCLUDE *** //

#include "DistanzaFrenataLocomotivaReale.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DICHIARAZIONE VARIABILI *** //

DistanzaFrenataLocomotivaAbstract distanzaFrenataLocomotivaReale = DistanzaFrenataLocomotivaAbstract(loggerReale);