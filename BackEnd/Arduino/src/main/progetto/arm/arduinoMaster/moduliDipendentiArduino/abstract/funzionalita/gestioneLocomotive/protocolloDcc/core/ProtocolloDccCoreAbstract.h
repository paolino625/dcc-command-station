#ifndef DCC_COMMAND_STATION_PROTOCOLLODCCCOREABSTRACT_H
#define DCC_COMMAND_STATION_PROTOCOLLODCCCOREABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** DEFINE *** //

/*
Il timer dell'interrupt verrà inizializzato a seconda del valore inserito qui (microsecondi)
Nota Tecnica: affinché il decoder legga in maniera corretta i bit codificati il minimo valore inseribile
è 35, 68 è il massimo, assumendo non venga chiamata la funzione millis() che va in conflitto con
l'interrupt. Questo discosta dal documento di NMRA nel quale vengono indicate le specifiche protocollo
GestioneLocomotive: qui si dice che affinché il decoder riconosca il bit 1, l'impulso debba durare 58
microsecondi con un errore di 3 microsecondi (dunque in un periodo compreso tra 55 e 61 microsecondi).

Se impostiamo il tempo impulso bit a 58, come suggerito dallo standard, quando la funzione millis
viene eseguita (in leggoCarattere()), questa si basa su interrupt e durante la sua esecuzione disabilita
tutti gli altri interrupt, incluso il nostro. Questo potrebbe portare a volte a un ritardo del nostro
interrupt. Se tale interrupt supera 68 microsecondi il decoder Hornby impazzisce. Devo evitare di
utilizzare chiamate millis(). Quando si cambia tale parametro controllare non solo che i decoder leggano
i pacchetti velocità ma anche quello per il cambiamento dell'indirizzo.
*/
#define TEMPO_IMPULSO_BIT_1 58

/*
Se il bit 1 per essere codificato ha bisogno di due impulsi da 58 microsecondi, un bit 0 ha bisogno di
due impulsi da più di 100 microsecondi ciascuno. Dunque ogni bit 0 avrà bisogno di 6 impulsi totali, a
metà dei quali dovrà invertire polarità.
*/
#define NUMERO_IMPULSI_0 6

/*
Per ogni pacchetto che vogliamo inviare sul tracciato, ripetiamo l'invio per varie volte per
assicurarci che il pacchetto arrivi. Se infatti inviamo un pacchetto una sola volta, il pacchetto non
arriva ai decoder metà delle volte.
*/

#define RIPETIZIONI_PACCHETTO_TRACCIATO 5

// *** CLASSE *** //

class ProtocolloDccCoreAbstract {
    // *** VARIABILI *** //

   protected:
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   protected:
    ProtocolloDccCoreAbstract(LoggerAbstract &logger) : logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

   public:
    virtual void invioPacchetto(volatile Pacchetto pacchetto) = 0;
    virtual void segnaleDcc() = 0;
    virtual void scrivoIndirizzoCv(int indirizzoCv, byte datoDaScrivere) = 0;
};

#endif
