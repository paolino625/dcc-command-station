#ifndef DCC_COMMAND_STATION_PERCORSOABSTRACT_H
#define DCC_COMMAND_STATION_PERCORSOABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class PercorsoAbstract {
    // *** VARIABILI *** //

   public:
    IdSezioneTipo idSezionePartenza;
    IdSezioneTipo idSezioneArrivo;
    IdSezioneTipo *idSezioniIntermedie;
    int numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva;

    LoggerAbstract &logger;

    // *** COSTRUTTORI *** //

    PercorsoAbstract(LoggerAbstract &logger) : logger(logger) {}
    PercorsoAbstract(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo, IdSezioneTipo *idSezioneIntermedie,
                     int numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva,
                     bool direzionePrimaLocomotivaDestra, LoggerAbstract &logger)
        : idSezionePartenza(idSezionePartenza),
          idSezioneArrivo(idSezioneArrivo),
          idSezioniIntermedie(idSezioneIntermedie),
          numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva(
              numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva),
          logger(logger) {}

    // *** METODI *** //

    bool equals(PercorsoAbstract *percorso2) {
        if (idSezionePartenza.equals(percorso2->idSezionePartenza) &&
            idSezioneArrivo.equals(percorso2->idSezioneArrivo)) {
            return true;
        } else {
            return false;
        }
    }
};

#endif
