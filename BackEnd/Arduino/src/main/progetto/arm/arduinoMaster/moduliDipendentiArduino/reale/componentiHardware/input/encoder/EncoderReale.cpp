// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/encoder/EncoderAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/stampa/core/DisplayCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINIZIONE VARIABILI *** //

DigitalPinReale pinEncoderClk1(CLK1);
DigitalPinReale pinEncoderDt1(DT1);
DigitalPinReale pinEncoderSw1(SW1);

DigitalPinReale pinEncoderClk2(CLK2);
DigitalPinReale pinEncoderDt2(DT2);
DigitalPinReale pinEncoderSw2(SW2);

EncoderAbstract encoderReale1(1, pinEncoderClk1, pinEncoderDt1, pinEncoderSw1, tempoReale, delayReale, displayCoreReale,
                              loggerReale);
EncoderAbstract encoderReale2(2, pinEncoderClk2, pinEncoderDt2, pinEncoderSw2, tempoReale, delayReale, displayCoreReale,
                              loggerReale);