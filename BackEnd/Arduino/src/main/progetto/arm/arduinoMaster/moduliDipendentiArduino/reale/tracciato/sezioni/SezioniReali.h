#ifndef DCC_COMMAND_STATION_SEZIONIREALI_H
#define DCC_COMMAND_STATION_SEZIONIREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezioni/SezioniAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SezioniAbstract sezioniReali;

#endif
