#ifndef DCC_COMMAND_STATION_USCITAMENUABSTRACT_H
#define DCC_COMMAND_STATION_USCITAMENUABSTRACT_H

// *** INCLUDE *** //

// Non riesco a metterlo sul core a causa di riferimenti reciproci
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"

// *** CLASSE *** //

class UscitaMenuAbstract {
    // *** VARIABILI *** //
   public:
    BuzzerAbstractReale &buzzer;
    DisplayAbstract &display;

    // *** COSTRUTTORE *** //

    UscitaMenuAbstract(BuzzerAbstractReale &buzzer, DisplayAbstract &display) : buzzer(buzzer), display(display) {}

    // *** DICHIARAZIONE METODI *** //

    void escoMenu(bool fileScambiDaAggiornareLocale, bool fileLocomotiveDaAggiornareLocale,
                  bool fileConvogliDaAggiornareLocale, bool fileUtentiDaAggiornareLocale);
};

#endif
