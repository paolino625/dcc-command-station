#ifndef CONVOGLIO_DTO_FACTORY_H
#define CONVOGLIO_DTO_FACTORY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/convoglio/ConvoglioRequestDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/convoglio/ConvoglioResponseDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_CONVOGLIO_STRINGA_JSON 1000

// *** CLASSE *** //

class ConvoglioDTOFactory {
   public:
    // RESPONSE DTO

    ConvoglioResponseDTO* creaConvoglioResponseDTO(IdConvoglioTipo idConvoglio);
    static char* converteConvoglioResponseDTOToStringJson(ConvoglioResponseDTO* convoglioDTO);
};

// *** DICHIARAZIONE VARIABILI *** //

extern ConvoglioDTOFactory convoglioDTOFactory;

#endif
