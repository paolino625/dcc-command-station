// *** INCLUDE *** //

#include "LedIntegratoAbstract.h"

#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE METODI *** //

void LedIntegratoAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo LED integrato... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    pinLedBlu.setOutputMode();
    pinLedRosso.setOutputMode();
    pinLedVerde.setOutputMode();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo LED integrato... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void LedIntegratoAbstract::accendiRosso() {
    spegne();
    pinLedRosso.setLow();
}

void LedIntegratoAbstract::accendiVerde() {
    spegne();
    pinLedVerde.setLow();
}

void LedIntegratoAbstract::accendiBlu() {
    spegne();
    pinLedBlu.setLow();
}

void LedIntegratoAbstract::spegne() {
    pinLedBlu.setHigh();
    pinLedRosso.setHigh();
    pinLedVerde.setHigh();
}

void LedIntegratoAbstract::lampeggiaRosso(int intervalloLampeggiamentoMillisecondi) {
    accendiRosso();
    delay.milliseconds(intervalloLampeggiamentoMillisecondi);
    spegne();
    delay.milliseconds(intervalloLampeggiamentoMillisecondi);
}