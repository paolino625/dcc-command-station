#ifndef DCC_COMMAND_STATION_LOGINABSTRACT_H
#define DCC_COMMAND_STATION_LOGINABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/KeypadAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreImpronte/SensoreImpronteAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/utenti/UtentiAbstract.h"

// *** DEFINE *** //

#define TEMPO_ATTESA_LOGIN_MILLISECONDI 5000

// *** ENUMERAZIONI *** //

enum ModalitaLogIn { IMPRONTA, PASSWORD };

// *** CLASSE *** //

class LogInAbstract {
    // *** VARIABILI *** //

    KeypadAbstract &keypad;
    DisplayAbstract &display;
    UtentiAbstract &utenti;
    BuzzerAbstractReale &buzzer;
    SensoreImpronteAbstract &sensoreImpronte;
    TempoAbstract &tempo;
    DelayAbstract &delay;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

   public:
    LogInAbstract(KeypadAbstract &keypad, DisplayAbstract &display, UtentiAbstract &utenti, BuzzerAbstractReale &buzzer,
                  SensoreImpronteAbstract &sensoreImpronte, TempoAbstract &tempo, DelayAbstract &delay,
                  LoggerAbstract &logger)
        : keypad(keypad),
          display(display),
          utenti(utenti),
          buzzer(buzzer),
          sensoreImpronte(sensoreImpronte),
          tempo(tempo),
          delay(delay),
          logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    bool effettuoLogIn();
    bool verificoImpronta();

    bool verificoPassword(int password);
};

#endif