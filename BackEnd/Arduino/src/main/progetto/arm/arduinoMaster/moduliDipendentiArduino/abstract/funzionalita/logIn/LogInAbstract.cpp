// *** INCLUDE *** //

#include "LogInAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/DisplayTempiLimiteScritta.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/logIn/statoLogIn/StatoLogIn.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/statiMenu/StatiMenu.h"

// *** DEFINIZIONE METODI *** //

bool LogInAbstract::verificoImpronta() {
    int risultatoSensoreImpronte = sensoreImpronte.getFingerprintId();

    if (risultatoSensoreImpronte == 0) {
        return false;
    } else {
        utenti.trovoImpostoUtenteConImpronta(risultatoSensoreImpronte);
        return true;
    }
}

bool LogInAbstract::verificoPassword(int password) { return utenti.trovoImpostoUtenteConPassword(password); }

bool LogInAbstract::effettuoLogIn() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "LOG IN...", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                          true);
    logger.cambiaTabulazione(1);
    display.logIn.sceltaTipologia();

    sensoreImpronte.controllaDatabaseImpronte();

    ModalitaLogIn modalitaLogIn = IMPRONTA;
    bool messaggioMostraPasswordDaMostrare = true;

    sensoreImpronte.getSensoreImpronteComponente()->LEDcontrol(FINGERPRINT_LED_ON, 0, FINGERPRINT_LED_PURPLE);

    TimestampTipo tempoInizioLogIn = tempo.milliseconds();
    // Fin tanto che il tempo riservato al login non è trascorso e il login non è stato effettuato
    while (tempo.milliseconds() - tempoInizioLogIn <= TEMPO_ATTESA_LOGIN_MILLISECONDI && !statoLogIn.logInEffettuato) {
        switch (modalitaLogIn) {
            case IMPRONTA: {
                statoLogIn.logInEffettuato = verificoImpronta();

                // Rimango in ascolto del keypad per capire se l'utente vuole effettuare il log in
                // tramite password
                int sceltaUtente = keypad.leggoSceltaMenuUtente();
                if (sceltaUtente == 1) {
                    modalitaLogIn = PASSWORD;
                }
                break;
            }
            case PASSWORD: {
                if (messaggioMostraPasswordDaMostrare) {
                    display.logIn.digitaPassword();
                    messaggioMostraPasswordDaMostrare = false;
                }
                int password = keypad.leggoNumero3Cifre(true);
                if (password != -1) {
                    bool passwordValida = verificoPassword(password);
                    if (passwordValida) {
                        statoLogIn.logInEffettuato = true;
                        // Anche nel caso in cui l'utente abbia inserito la password corretta, devo
                        // impostare il LED del sensore impronte a blu per indicare il successo del Log In
                        sensoreImpronte.getSensoreImpronteComponente()->LEDcontrol(FINGERPRINT_LED_ON, 0,
                                                                                   FINGERPRINT_LED_BLUE);
                        break;
                    } else {
                        display.logIn.passwordNonValida();
                        messaggioMostraPasswordDaMostrare = true;
                        delay.milliseconds(TEMPO_LIMITE_SCRITTA_DISPLAY_MEDIO);
                    }
                }
            }
        }
    }

    logger.cambiaTabulazione(-1);

    if (statoLogIn.logInEffettuato) {
        // Quando il log in viene effettuato, il fuoriMenu deve essere impostato a MENU_PRINCIPALE: da questa variabile
        // dipende il comportamento della funzione gestiscoDisplayKeypad
        fuoriMenu = FuoriMenuEnum::NO_INDICE;
        buzzer.beepOk();
        display.logIn.benvenuto();

        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "LOG IN... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        return true;
    } else {
        // Se il log in non è stato effettuato, significa che è terminato il tempo a disposizione
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "LOG IN... TIMEOUT",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        return false;
    }
}