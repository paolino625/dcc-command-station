// *** INCLUDE *** //

#include "AlimentazioneAbstract.h"

#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

void AlimentazioneAbstract::controllaFunzionamento() {
    sensoreTensioneAbstract.effettuaLetturaAggiornaTutteEntryCampionamento();

    TensioneTipo tensione = 0;
    // Il sensore di tensione, in base alla tensione applicata in input, restituisce sul segnale di uscita una tensione
    // compresa tra 0 e 5 V. Da notare che Arduino Giga è una scheda a 3.3 V, dunque non solo non è in grado di leggere
    // tensioni superiori a 3.3 V, ma portare tensioni maggiori su un pin potrebbe danneggiarlo.
    // Dunque, nel caso di alimentatori con tensione superiore a 16.5 V, è stato aggiunto un partitore di tensione sulla
    // scheda di alimentazione in modo tale da dimezzare la tensione in ingresso al sensore di tensione. Tuttavia, in
    // questi casi, la tensione letta dal sensore deve essere moltiplicata per 2 per ottenere il valore
    // reale.
    if (tensioneMinimaCorrettoFunzionamento <= 16.5) {
        tensione = sensoreTensioneAbstract.getTensione();
    } else {
        tensione = sensoreTensioneAbstract.getTensione() * 2;
    }

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Controllo alimentazione ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, nomeAlimentazione,
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
    logger.cambiaTabulazione(1);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO,
                          "Tensione rilevata: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(tensione).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, " V", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    if (tensione < tensioneMinimaCorrettoFunzionamento) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Non rilevo alimentazione! ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Tensione attuale: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(tensione).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, " V", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL,
                              "Tensione minima richiesta: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, std::to_string(tensioneMinimaCorrettoFunzionamento).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, " V", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);

        fermoProgramma.ferma(true);
    } else {
        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Controllo alimentazione ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, nomeAlimentazione,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... OK", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                              true);
    }
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente con delete[].
char* AlimentazioneAbstract::toString() {
    std::string stringa;

    stringa += "Tensione ";
    stringa += nomeAlimentazione;
    stringa += ": ";
    stringa += std::to_string(sensoreTensioneAbstract.getTensione());
    stringa += " V\n";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

float AlimentazioneAbstract::getTensione() { return sensoreTensioneAbstract.getTensione(); }