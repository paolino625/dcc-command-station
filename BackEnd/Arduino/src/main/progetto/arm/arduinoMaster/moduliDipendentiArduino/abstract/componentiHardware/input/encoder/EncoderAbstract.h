#ifndef DCC_COMMAND_STATION_ENCODERABSTRACT_H
#define DCC_COMMAND_STATION_ENCODERABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/DisplayCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/DigitalPinAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/ConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

// Definisco lo step di velocità della locomotiva per ogni step dell'encoder

#define STEP_ENCODER_MODE1 1
#define STEP_ENCODER_MODE2 10

// *** CLASSE *** //

class EncoderAbstract {
    // *** VARIABILI *** //

   public:
    IdEncoderTipo id;
    DigitalPinAbstract& clk;
    DigitalPinAbstract& dt;
    DigitalPinAbstract& sw;

    TempoAbstract& tempoAbstract;
    DelayAbstract& delay;
    DisplayCoreAbstract& display;
    LoggerAbstract& logger;

    byte currentStateClk;
    byte lastStateClk;
    int velocitaPenultimoStep;
    byte modalitaStepEncoder = STEP_ENCODER_MODE1;

    bool pulsanteEncoderPremuto;
    TimestampTipo pressTime;

    // *** COSTRUTTORE *** //

   public:
    EncoderAbstract(IdEncoderTipo id, DigitalPinAbstract& clk, DigitalPinAbstract& dt, DigitalPinAbstract& sw,
                    TempoAbstract& tempoAbstract, DelayAbstract& delay, DisplayCoreAbstract& displayCore,
                    LoggerAbstract& logger)
        : id(id),
          clk{clk},
          dt{dt},
          sw{sw},
          tempoAbstract{tempoAbstract},
          delay{delay},
          display{displayCore},
          logger{logger} {}

    // *** METODI PUBBLICI *** //

    void inizializza();

    bool leggeStato(ConvoglioAbstract* convoglio);

    byte getModalitaStepEncoder() { return modalitaStepEncoder; }

    void setModalitaStepEncoder(byte modalita) { modalitaStepEncoder = modalita; }
};

#endif