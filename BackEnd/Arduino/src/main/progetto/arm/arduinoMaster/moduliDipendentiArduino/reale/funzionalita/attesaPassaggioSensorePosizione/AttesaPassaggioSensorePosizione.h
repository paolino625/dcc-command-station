#ifndef DCC_COMMAND_STATION_ATTESAPASSAGGIOSENSOREPOSIZIONE_H
#define DCC_COMMAND_STATION_ATTESAPASSAGGIOSENSOREPOSIZIONE_H

// *** DICHIARAZIONE FUNZIONI *** //

void aspettoPassaggioSensore(byte sensoreInLettura);

void aspettoPassaggioSensoreControlloCortoCircuito(
    byte sensoreInLettura, ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuitoTracciato,
    MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypadOperazioniMultiple);

#endif
