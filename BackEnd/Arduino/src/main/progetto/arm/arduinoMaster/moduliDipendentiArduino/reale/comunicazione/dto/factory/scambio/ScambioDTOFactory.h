#ifndef SCAMBIO_DTO_FACTORY_H
#define SCAMBIO_DTO_FACTORY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/scambio/ScambioRequestDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/scambio/ScambioResponseDTO.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_SCAMBIO_STRINGA_JSON 60

// *** CLASSE *** //

class ScambioDTOFactory {
   public:
    // RESPONSE DTO

    static ScambioResponseDTO* creaScambioResponseDTO(IdScambioTipo idScambio);
    static char* converteScambioResponseDTOToStringJson(ScambioResponseDTO* scambioResponseDTO);
};

// *** DICHIARAZIONE VARIABILI *** //

extern ScambioDTOFactory scambioDTOFactory;

#endif
