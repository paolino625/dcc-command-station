#ifndef DCC_COMMAND_STATION_PERCORSIABSTRACT_H
#define DCC_COMMAND_STATION_PERCORSIABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/percorso/PercorsoAbstract.h"

// *** CLASSE *** //

class PercorsiAbstract {
    // *** METODI *** //

   public:
    virtual bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo) = 0;
    virtual bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo,
                                bool direzionePrimaLocomotivaDestra) = 0;
    virtual PercorsoAbstract* getPercorso(IdSezioneTipo idSezionePartenzaRequest,
                                          IdSezioneTipo idSezioneArrivoRequest) = 0;
};

#endif
