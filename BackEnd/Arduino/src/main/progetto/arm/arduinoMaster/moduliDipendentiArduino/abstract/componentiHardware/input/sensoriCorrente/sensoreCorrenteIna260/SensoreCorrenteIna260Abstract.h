#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA260ABSTRACT_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA260ABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/core/SensoreCorrenteIna260CoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"

// *** DEFINE *** //

// Attenzione a incrementare questo numero, potrebbe portare a un consumo eccessivo di memoria
#define NUMERO_CAMPIONI_SENSORE_CORRENTE_INA260 100

// *** CLASSE *** //

class SensoreCorrenteIna260Abstract : public LetturaTensioneAbstract {
    // *** VARIABILI *** //

   private:
    SensoreCorrenteIna260CoreAbstract& sensoreCorrenteIna260Core;
    ProgrammaAbstract& programma;
    int indirizzoI2C;
    DisplayAbstract& display;
    LoggerAbstract& logger;

    int statoSensoreCorrenteIna260Campionato;
    CorrenteTipo bufferCorrente[NUMERO_CAMPIONI_SENSORE_CORRENTE_INA260];
    int indiceAttualeBufferStatoSensoreCorrenteIna260;

    // *** COSTRUTTORE *** //

   public:
    SensoreCorrenteIna260Abstract(SensoreCorrenteIna260CoreAbstract& sensoreCorrenteIna260Core,
                                  ProgrammaAbstract& programma, int indirizzoI2C, DisplayAbstract& display,
                                  LoggerAbstract& logger)
        : sensoreCorrenteIna260Core{sensoreCorrenteIna260Core},
          programma{programma},
          indirizzoI2C{indirizzoI2C},
          display{display},
          logger{logger} {}

    // *** METODI PUBBLICI *** //

    void inizializza();
    void aggiornaCampionamentoCorrente();
    char* toString() const;
    void effettuaLetturaAggiornaUnaEntryCampionamento();
    void effettuaLetturaAggiornaTutteEntryCampionamento();

    // *** METODI PRIVATI *** //

   private:
    void misuraMediaCampioniSensoreCorrenteIna260();

    // *** GETTER *** //
   public:
    int getIndirizzoI2C() const;
    int getCorrente() const;
    float getTensione();
};

#endif