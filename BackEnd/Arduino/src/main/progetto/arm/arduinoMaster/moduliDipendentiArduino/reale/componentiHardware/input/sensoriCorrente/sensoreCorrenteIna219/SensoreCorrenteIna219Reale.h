#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA219REALE_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA219REALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/SensoreCorrenteIna219Abstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SensoreCorrenteIna219Abstract sensoreCorrenteIna219Reale;

#endif
