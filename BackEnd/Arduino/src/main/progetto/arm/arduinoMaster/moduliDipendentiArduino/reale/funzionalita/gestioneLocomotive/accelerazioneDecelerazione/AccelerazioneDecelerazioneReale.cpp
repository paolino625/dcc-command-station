// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/accelerazioneDecelerazione/AccelerazioneDecelerazioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AccelerazioneDecelerazioneAbstract accelerazioneDecelerazioneReale =
    AccelerazioneDecelerazioneAbstract(convogliReali, loggerReale);