#ifndef DCC_COMMAND_STATION_UTENTIABSTRACT_H
#define DCC_COMMAND_STATION_UTENTIABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

// Aggiungere +1
#define NUMERO_UTENTI 5

#define LUNGHEZZA_NOME_UTENTE 20

/*
Aggiungere +1
Un numero più alto delle dita di un utente perché potrebbe essere utile riconfigurare le impronte (che non sono più
riconosciute) senza dover fare il reset completo del database delle impronte
*/
#define NUMERO_IMPRONTE_PER_UTENTE 15

#define NUMERO_CARATTERI_TIPOLOGIA_UTENTE 15
#define NUMERO_CARATTERI_SESSO 10

// *** STRUCT *** //

struct Utente {
    char nome[LUNGHEZZA_NOME_UTENTE];
    char tipologiaUtente[NUMERO_CARATTERI_TIPOLOGIA_UTENTE];
    char sesso[NUMERO_CARATTERI_SESSO];
    byte numeroImpronteSalvate;
    IdImprontaTipo idImpronte[NUMERO_IMPRONTE_PER_UTENTE];
    int password;  // 3 cifre
};

// *** CLASSE *** //

class UtentiAbstract {
    // *** VARIABILI *** //
   public:
    ProgrammaAbstract &programma;
    LoggerAbstract &logger;
    Utente lista[NUMERO_UTENTI];

    // *** COSTRUTTORE *** //
    UtentiAbstract(ProgrammaAbstract &programma, LoggerAbstract &logger) : programma(programma), logger(logger) {}

    // *** DICHIARAZIONE FUNZIONI *** //

    void trovoImpostoUtenteConImpronta(int idSensoreImpronte);
    bool trovoImpostoUtenteConPassword(int password);
    char *getUtenteToString(int indiceUtente);
    char *toString();
};

#endif