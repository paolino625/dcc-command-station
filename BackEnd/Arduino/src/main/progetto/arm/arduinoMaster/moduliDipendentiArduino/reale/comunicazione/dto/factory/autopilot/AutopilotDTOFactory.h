#ifndef ARDUINO_AUTOPILOTDTOFACTORY_H
#define ARDUINO_AUTOPILOTDTOFACTORY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/StatoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/autopilot/InfoAutopilotResponseDTO.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_INFO_AUTOPILOT_STRINGA_JSON 150

// *** CLASSE *** //

class AutopilotDTOFactory {
   public:
    static InfoAutopilotResponseDTO* creaInfoAutopilotResponseDTO();
    static char* converteInfoAutopilotResponseDTOToStringJson(InfoAutopilotResponseDTO* infoAutopilotResponseDTO);
};

// *** DICHIARAZIONE VARIABILI *** //

extern AutopilotDTOFactory autopilotDTOFactory;

#endif
