#ifndef ARDUINO_SEZIONEDTOFACTORY_H
#define ARDUINO_SEZIONEDTOFACTORY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezione/SezioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/sezione/SezioneResponseDTO.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_SEZIONE_STRINGA_JSON 200

// *** CLASSE *** //

class SezioneDTOFactory {
   public:
    // RESPONSE DTO

    SezioneResponseDTO* creaSezioneResponseDTO(int indiceSezione);
    static char* converteSezioneResponseDTOToStringJson(SezioneResponseDTO* sezioneResponseDTO);
};

// *** DICHIARAZIONE VARIABILI *** //

extern SezioneDTOFactory sezioneDTOFactory;

#endif