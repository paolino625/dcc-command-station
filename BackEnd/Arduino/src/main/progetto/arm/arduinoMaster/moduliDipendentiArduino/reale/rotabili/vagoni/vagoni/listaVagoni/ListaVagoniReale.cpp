// *** INCLUDE *** //

#include "ListaVagoniReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/vagoni/vagoni/VagoniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

ListaVagoniAbstract listaVagoniReale = ListaVagoniAbstract(convogliReali, programmaReale, vagoniReali, loggerReale);