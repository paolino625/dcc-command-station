#ifndef ARDUINO_RESTCLIENTABSTRACT_H
#define ARDUINO_RESTCLIENTABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/restClient/enum/TipologiaRichiestaHttpRestClient.h"

// *** CLASSE *** //

class RestClientAbstract {
   public:
    virtual void inviaRichiestaHTTP(TipologiaRichiestaHttpRestClient tipologiaRichiestaHttp, char* indirizzoServer,
                                    int porta, char* endPoint, char* requestBody, char* responseHeader,
                                    char* responseBody) = 0;

   private:
    virtual void compongoRequestHeader(TipologiaRichiestaHttpRestClient tipologiaRichiestaHttp, char* indirizzoServer,
                                       char* endPoint, char* requestHeader, char* requestBody) = 0;
    virtual void compongoRequestBody(char* requestBody, char* requestBodyDaInviare) = 0;
    virtual void invioAlServer(char* requestHeader, char* requestBody) = 0;
    virtual void leggoRispostaDelServer(char* responseHeader, char* responseBody) = 0;
    virtual void stampoRequest(char* requestHeader, char* requestBody) = 0;
    virtual void stampoResponse(char* responseHeader, char* responseBody) = 0;
};

#endif
