// *** INCLUDE *** //

#include "StatoInizializzazioneAutopilotConvoglio.h"

// *** DEFINIZIONE FUNZIONI *** //

const char *getStatoInizializzazioneAutopilotConvoglioAsAString(
    StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglioLocale, LoggerAbstract &logger) {
    switch (statoInizializzazioneAutopilotConvoglioLocale) {
        case StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO:
            return "PRONTO_INIZIALIZZAZIONE_CONVOGLIO";
        case StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT:
            return "PRONTO_PER_CONFIGURAZIONE_AUTOPILOT";
        case StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA:
            return "CONFIGURAZIONE_AUTOPILOT_COMPLETATA";
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Enumerazione non mappata nella funzione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
    }
}
