#ifndef DCC_COMMAND_STATION_SENSORECORRENTEINA219COREABSTRACT_H
#define DCC_COMMAND_STATION_SENSORECORRENTEINA219COREABSTRACT_H

// *** CLASSE *** //

class SensoreCorrenteIna219CoreAbstract {
   public:
    virtual bool begin() = 0;
    virtual float getCurrent_mA() = 0;
    virtual float getBusVoltage_V() = 0;
    virtual float getShuntVoltage_mV() = 0;
};

#endif
