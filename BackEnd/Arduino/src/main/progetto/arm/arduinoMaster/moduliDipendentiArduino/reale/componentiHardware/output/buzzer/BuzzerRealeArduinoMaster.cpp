// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/AnalogPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AnalogPinReale pinBuzzerArduinoMaster(PIN_BUZZER);
BuzzerAbstractReale buzzerRealeArduinoMaster(pinBuzzerArduinoMaster, delayReale, loggerReale);