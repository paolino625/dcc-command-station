#ifndef DCC_COMMAND_STATION_LOGINREALE_H
#define DCC_COMMAND_STATION_LOGINREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/LogInAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern LogInAbstract logInReale;

#endif
