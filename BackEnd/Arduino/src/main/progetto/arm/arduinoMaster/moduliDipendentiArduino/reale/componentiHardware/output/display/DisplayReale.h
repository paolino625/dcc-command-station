#ifndef DCC_COMMAND_STATION_DISPLAYREALE_H
#define DCC_COMMAND_STATION_DISPLAYREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern DisplayAbstract displayReale;

#endif
