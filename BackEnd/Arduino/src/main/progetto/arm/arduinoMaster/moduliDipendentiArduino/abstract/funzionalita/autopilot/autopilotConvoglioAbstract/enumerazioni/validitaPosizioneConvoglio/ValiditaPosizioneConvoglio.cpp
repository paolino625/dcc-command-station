// *** INCLUDE *** //

#include "ValiditaPosizioneConvoglio.h"

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINIZIONE FUNZIONI *** //

const char* getValiditaPosizioneConvoglioAsAString(ValiditaPosizioneConvoglio validitaPosizioneConvoglio,
                                                   LoggerAbstract& logger) {
    switch (validitaPosizioneConvoglio) {
        case ValiditaPosizioneConvoglio::POSIZIONE_VALIDA:
            return "POSIZIONE_VALIDA";
        case ValiditaPosizioneConvoglio::POSIZIONE_NON_VALIDA:
            return "POSIZIONE_NON_VALIDA";
        default:
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Enumerazione non mappata nella funzione.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
    }
}