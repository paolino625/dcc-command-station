#ifndef DCC_COMMAND_STATION_MQTTARDUINOMASTERABSTRACT_H
#define DCC_COMMAND_STATION_MQTTARDUINOMASTERABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/abstract/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmAbstract.h"
#include "main/progetto/arm/comune/moduliDipendentiArduino/abstract/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmAbstract.h"

// *** CLASSE *** //

class MqttArduinoMasterAbstract {
    public:
     virtual void inizializzaVariabiliTriggerInvioMqtt() = 0;

     virtual void inviaStatoOggettiAggiornati() = 0;
     virtual void inviaStatoLocomotiva(IdLocomotivaTipo idLocomotiva) = 0;
     virtual void inviaStatoConvoglio(IdConvoglioTipo idConvoglio) = 0;
     virtual void inviaStatoScambio(IdScambioTipo idScambio) = 0;
     virtual void inviaStatoSezione(int indiceSezione) = 0;
     virtual void inviaStatoSensorePosizione(IdSensorePosizioneTipo idSensore) = 0;
     virtual void inviaInfoAutopilot() = 0;
     virtual void invioStatoLocomotiveAggiornate() = 0;
     virtual void invioStatoConvogliAggiornati() = 0;
     virtual void invioStatoScambiAggiornati() = 0;
     virtual void invioStatoSensoriPosizioneAggiornati() = 0;
     virtual void invioStatoSezioniAggiornate() = 0;
     virtual void invioInfoAutopilotAggiornato() = 0;
     virtual void invioTriggerArduinoReady() = 0;
     virtual void inviaMessaggioLog(char* payload) = 0;

     virtual MqttCoreArduinoArmAbstract& getMqttCore() = 0;

};

#endif
