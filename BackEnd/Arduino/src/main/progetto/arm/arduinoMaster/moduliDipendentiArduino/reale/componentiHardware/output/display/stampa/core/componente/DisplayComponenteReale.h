#ifndef DCC_COMMAND_STATION_DISPLAYCORE_H
#define DCC_COMMAND_STATION_DISPLAYCORE_H

// *** INCLUDE *** //

#include "Wire.h"
#include "hd44780.h"
#include "hd44780ioClass/hd44780_I2Cexp.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/componente/DisplayComponenteAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/tempo/TempoAbstract.h"

// *** CLASSE *** //

// Specifica che la classe DisplayCore eredita da DisplayCoreAbstract
class DisplayComponenteReale : public DisplayComponenteAbstract {
    // *** VARIABILI *** //

    hd44780_I2Cexp displayCore;
    ProgrammaAbstract& programma;

    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    DisplayComponenteReale(TempoAbstract& tempo, LoggerAbstract& logger, ProgrammaAbstract& programma, int indirizzoI2C)
        : DisplayComponenteAbstract(logger, indirizzoI2C), programma(programma), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void pulisce();
    void posizionaCursore(int colonna, int riga);
    void print(const __FlashStringHelper* stringa);
    virtual void print(const char* stringa);
    void print(int numero);
    void print(char carattere);
    void print(float numero);
    void print(String stringa);
    void print(unsigned long numero);
};

#endif

extern DisplayComponenteReale displayComponenteReale;
