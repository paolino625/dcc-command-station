#ifndef DCC_COMMAND_STATION_LOCOMOTIVEREALI_H
#define DCC_COMMAND_STATION_LOCOMOTIVEREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern LocomotiveAbstract locomotiveReali;

#endif
