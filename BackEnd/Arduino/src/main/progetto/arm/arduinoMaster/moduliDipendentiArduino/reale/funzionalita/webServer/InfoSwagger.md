È necessario aggiornare manualmente lo swagger.yaml dopo aver modificato le API.

Se si vuole hostare il file swagger.yaml online, è possibile usare [SwaggerHub](https://swagger.io/tools/swaggerhub/).
