#ifndef DCC_COMMAND_STATION_SENSORITENSIONEREALI_H
#define DCC_COMMAND_STATION_SENSORITENSIONEREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreTensione/SensoreTensioneAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SensoreTensioneAbstract sensoreTensioneLuciInterruttoriReale;
extern SensoreTensioneAbstract sensoreTensioneLED1Reale;
extern SensoreTensioneAbstract sensoreTensioneLED2Reale;
extern SensoreTensioneAbstract sensoreTensioneLED3Reale;
extern SensoreTensioneAbstract sensoreTensioneCDUReale;
extern SensoreTensioneAbstract sensoreTensioneSensoriPosizioneReale;
extern SensoreTensioneAbstract sensoreTensioneArduinoReale;
extern SensoreTensioneAbstract sensoreTensioneComponentiAusiliari5VReale;
extern SensoreTensioneAbstract sensoreTensioneComponentiAusiliari3VReale;

#endif
