#ifndef ARDUINO_ARDUINOMASTERHELPERABSTRACT_H
#define ARDUINO_ARDUINOMASTERHELPERABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/percorso/PercorsoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"

// *** CLASSE *** //

class ArduinoMasterHelperAbstract {
   public:
    // *** METODI *** //
    virtual bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo) = 0;
    virtual bool esistePercorso(IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo,
                                bool direzionePrimaLocomotivaDestra) = 0;
    virtual PercorsoAbstract* getPercorso(IdSezioneTipo idSezionePartenzaRequest,
                                          IdSezioneTipo idSezioneArrivoRequest) = 0;
    virtual bool verificaConnessione() = 0;
    virtual bool reset() = 0;
};

#endif
