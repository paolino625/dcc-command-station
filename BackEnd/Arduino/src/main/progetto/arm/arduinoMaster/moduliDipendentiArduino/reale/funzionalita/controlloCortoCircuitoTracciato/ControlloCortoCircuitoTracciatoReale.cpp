// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/controlloCortoCircuitoTracciato/ControlloCortoCircuitoTracciatoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/KeypadReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/SensoreCorrenteIna219Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/motorShield/MotorShieldReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/autopilot/autopilotReale/AutopilotReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/stopEmergenza/StopEmergenzaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

ControlloCortoCircuitoTracciatoAbstract controlloCortoCircuitoTracciatoReale = ControlloCortoCircuitoTracciatoAbstract(
    keypadReale, sensoreCorrenteIna219Reale, motorShieldReale, buzzerRealeArduinoMaster, displayReale,
    stopEmergenzaReale, delayReale, autopilotReale, loggerReale);