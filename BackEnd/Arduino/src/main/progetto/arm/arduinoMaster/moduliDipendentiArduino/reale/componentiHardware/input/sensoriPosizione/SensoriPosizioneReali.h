#ifndef DCC_COMMAND_STATION_SENSORIPOSIZIONEREALE_H
#define DCC_COMMAND_STATION_SENSORIPOSIZIONEREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriPosizione/reale/SensoriPosizioneAbstractReale.h"

// *** VARIABILI *** //

extern SensoriPosizioneAbstractReale sensoriPosizioneReali;

#endif