// *** INCLUDE *** //

#include "UtentiReali.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

UtentiAbstract utentiReali = UtentiAbstract(programmaReale, loggerReale);