// *** INCLUDE *** //

#include "ScambioRelayAbstract.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/mqtt/FlagInvioMqtt.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/scambi/posizioneDefault/PosizioneDefaultScambi.h"

// *** DEFINIZIONE METODI *** //

void ScambioRelayAbstract::reset() { posizioneScambio = posizioneDefaultScambi.getPosizioneScambio(id); }

void ScambioRelayAbstract::azionaSinistra() {
    posizioneScambio = PosizioneScambio::SINISTRA;
    scambiAggiornamentoMqttRichiesto[id] = true;

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileScambiDaAggiornare = true;
}

void ScambioRelayAbstract::azionaDestra() {
    posizioneScambio = PosizioneScambio::DESTRA;
    scambiAggiornamentoMqttRichiesto[id] = true;

    // Questa variabile è salvata nel file USB. Quindi è necessario aggiornare il file.
    fileScambiDaAggiornare = true;
}

void ScambioRelayAbstract::setPosizioneNonConosciuta() {
    posizioneScambio = PosizioneScambio::NON_CONOSCIUTA;
    scambiAggiornamentoMqttRichiesto[id] = true;
}

bool ScambioRelayAbstract::isSinistra() {
    if (posizioneScambio == PosizioneScambio::SINISTRA) {
        return true;
    } else {
        return false;
    }
}

bool ScambioRelayAbstract::isDestra() {
    if (posizioneScambio == PosizioneScambio::DESTRA) {
        return true;
    } else {
        return false;
    }
}

int ScambioRelayAbstract::getStatoNumero() {
    if (posizioneScambio == PosizioneScambio::SINISTRA) {
        return 1;
    } else if (posizioneScambio == PosizioneScambio::DESTRA) {
        return 2;
    } else {
        return 0;
    }
}

void ScambioRelayAbstract::setSinistra() { posizioneScambio = PosizioneScambio::SINISTRA; }

void ScambioRelayAbstract::setDestra() { posizioneScambio = PosizioneScambio::DESTRA; }

const char* ScambioRelayAbstract::getStatoStringa() {
    if (posizioneScambio == PosizioneScambio::SINISTRA) {
        return "SINISTRA";
    } else if (posizioneScambio == PosizioneScambio::DESTRA) {
        return "DESTRA";
    } else {
        return "NON_CONOSCIUTA";
    }
}