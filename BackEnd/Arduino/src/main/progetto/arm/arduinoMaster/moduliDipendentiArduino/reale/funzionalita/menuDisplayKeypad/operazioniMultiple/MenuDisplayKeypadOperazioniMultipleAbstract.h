#ifndef DCC_COMMAND_STATION_MENUDISPLAYKEYPADOPERAZIONIMULTIPLEABSTRACT_H
#define DCC_COMMAND_STATION_MENUDISPLAYKEYPADOPERAZIONIMULTIPLEABSTRACT_H

// *** INCLUDE *** //

#include "MenuDisplayKeypadOperazioniMultipleAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/menuDisplayKeypad/operazioniMultiple/MenuDisplayKeypadOperazioniMultipleAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern MenuDisplayKeypadOperazioniMultipleAbstract menuDisplayKeypadOperazioniMultipleReale;

#endif
