#ifndef DCC_COMMAND_STATION_LISTALOCOMOTIVEREALE_H
#define DCC_COMMAND_STATION_LISTALOCOMOTIVEREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/listaLocomotive/ListaLocomotiveAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ListaLocomotiveAbstract listaLocomotiveReale;

#endif