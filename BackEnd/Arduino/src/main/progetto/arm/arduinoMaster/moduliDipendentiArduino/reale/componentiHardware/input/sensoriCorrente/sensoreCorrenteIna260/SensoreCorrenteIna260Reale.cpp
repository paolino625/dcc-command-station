// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/SensoreCorrenteIna260Abstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/core/SensoreCorrenteIna260CoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

SensoreCorrenteIna260Abstract sensoreCorrenteIna260Reale(sensoreCorrenteIna260CoreReale, programmaReale,
                                                         INDIRIZZO_I2C_INA_260, displayReale, loggerReale);