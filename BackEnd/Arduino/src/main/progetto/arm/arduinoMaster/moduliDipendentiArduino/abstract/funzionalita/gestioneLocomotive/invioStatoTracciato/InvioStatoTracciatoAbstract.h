#ifndef DCC_COMMAND_STATION_INVIOSTATOTRACCIATOABSTRACT_H
#define DCC_COMMAND_STATION_INVIOSTATOTRACCIATOABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"

// *** ENUMERAZIONI *** //

enum StepInvioStatoLocomotiva {
    PACCHETTO_VELOCITA,
    PACCHETTO_FUNZIONI_AUSILIARI_1,
    PACCHETTO_FUNZIONI_AUSILIARI_2,
    PACCHETTO_FUNZIONI_AUSILIARI_3
};

// *** CLASSE *** //

class InvioStatoTracciatoAbstract {
    // *** VARIABILI *** //

    ConvogliAbstract &convogli;
    LocomotiveAbstract &locomotive;
    BuzzerAbstractReale &buzzer;
    ProtocolloDccCoreAbstract &protocolloDccCore;
    LoggerAbstract &logger;

    IdLocomotivaTipo idLocomotivaRefreshAttuale = 1;  // Partiamo dalla locomotiva 1
    StepInvioStatoLocomotiva stepInvioStatoLocomotiva =
        PACCHETTO_VELOCITA;  // Partiamo dall'invio del pacchetto velocità

    // *** COSTRUTTORE *** //

   public:
    InvioStatoTracciatoAbstract(ConvogliAbstract &convogli, LocomotiveAbstract &locomotive, BuzzerAbstractReale &buzzer,
                                ProtocolloDccCoreAbstract &protocolloDccCore, LoggerAbstract &logger)
        : convogli(convogli),
          locomotive(locomotive),
          buzzer(buzzer),
          protocolloDccCore(protocolloDccCore),
          logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

   public:
    void invioPacchettiStatoTutteLocomotive();
    bool inviaPacchettoStatoLocomotivaAggiornataSePresente();
    void processo();
    void stopEmergenza();
    void invioPacchettoStopEmergenza();

    void invioPacchettoVelocitaLocomotiva(LocomotivaAbstract *locomotiva);
    void invioPacchettiLuciLocomotiva(LocomotivaAbstract *locomotiva);
    void invioPacchettoLuciLocomotiva1(LocomotivaAbstract *locomotiva);
    void invioPacchettoLuciLocomotiva2(LocomotivaAbstract *locomotiva);
    void invioPacchettoLuciLocomotiva3(LocomotivaAbstract *locomotiva);
};

#endif
