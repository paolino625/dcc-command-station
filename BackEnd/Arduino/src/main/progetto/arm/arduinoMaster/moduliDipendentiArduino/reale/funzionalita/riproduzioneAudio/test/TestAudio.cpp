// *** INCLUDE *** //

#include "TestAudio.h"

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/usb/pathFile/PathFile.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/riproduzioneAudio/core/RiproduzioneAudioCore.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

bool statoBottonePrecedente = false;
bool bottonePremuto = false;

// *** DEFINIZIONE FUNZIONI *** //

void controlloPulsante() {
    int statoBottone = digitalRead(PC_13);
    if (statoBottone && !statoBottonePrecedente) {
        bottonePremuto = true;
    } else {
        bottonePremuto = false;
    }
    statoBottonePrecedente = statoBottone;

    if (bottonePremuto) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Bottone premuto",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        riproducoFileAudioTest();
    }
}

void riproducoFileAudioTest() {
    riproduzioneAudioCoreReale.aggiungeFileCoda(pathFileAudioTest1);
    riproduzioneAudioCoreReale.aggiungeFileCoda(pathFileAudioTest2);
    riproduzioneAudioCoreReale.aggiungeFileCoda(pathFileAudioTest3);
    riproduzioneAudioCoreReale.aggiungeFileCoda(pathFileAudioTest4);
    riproduzioneAudioCoreReale.aggiungeFileCoda(pathFileAudioTest5);
}
