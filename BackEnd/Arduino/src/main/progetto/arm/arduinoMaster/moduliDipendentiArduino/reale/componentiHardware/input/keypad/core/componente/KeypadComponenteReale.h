#ifndef DCC_COMMAND_STATION_KEYPADCOMPONENTEREALE_H
#define DCC_COMMAND_STATION_KEYPADCOMPONENTEREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/core/componente/KeypadComponenteAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"

// *** DICHIARAZIONE VARIABILI *** //

extern DigitalPinReale pinKeypadRiga1;
extern DigitalPinReale pinKeypadRiga2;
extern DigitalPinReale pinKeypadRiga3;
extern DigitalPinReale pinKeypadRiga4;
extern DigitalPinReale pinKeypadColonna1;
extern DigitalPinReale pinKeypadColonna2;
extern DigitalPinReale pinKeypadColonna3;
extern DigitalPinReale pinKeypadColonna4;

extern KeypadComponenteAbstract keypadComponenteReale;

#endif
