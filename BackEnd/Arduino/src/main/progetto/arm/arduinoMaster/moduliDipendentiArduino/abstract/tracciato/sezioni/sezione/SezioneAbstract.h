#ifndef DCC_COMMAND_STATION_SEZIONEABSTRACT_H
#define DCC_COMMAND_STATION_SEZIONEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/ScambiRelayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/ConvoglioAbstract.h"

// *** DEFINE *** //

#define MARGINE_SEZIONE_LUNGA_A_SUFFICIENZA_MODALITA_MANUALE_CM 10

// *** ENUMERAZIONI *** //

enum TipologiaSezioneCritica {
    MAI,     // Ad esempio le sezioni di stazione non sono mai critiche
    SEMPRE,  // Alcune sezioni, ad esempio gli scambi o i binari in entrata a una stazione sono sempre sezioni critiche
    SOLO_DIREZIONE_CONVOGLIO_DESTRA,   // Le sezioni che sono critiche solo se la direzione del convoglio è verso destra
    SOLO_DIREZIONE_CONVOGLIO_SINISTRA  // Le sezioni che sono critiche solo se la direzione del convoglio è verso
                                       // sinistra
};

struct StatoSezione {
    // Variabile utile per memorizzare lo stato del lock della sezione
    bool statoLock = false;
    IdConvoglioTipo idConvoglio = -1;
};

// *** CLASSE *** //

class SezioneAbstract {
    // *** VARIABILI *** //

   private:
    StatoSezione stato;

    ScambiRelayAbstract& scambiRelay;
    int indiceArraySezioni;
    IdSezioneTipo idSezione;
    LunghezzaTracciatoTipo lunghezzaCm;
    TipologiaSezioneCritica tipologiaSezioneCritica;
    bool inversioneDiPolarita = false;
    // Necessario inizializzarli a 0 altrimenti assumono valori casuali
    IdSensorePosizioneTipo sensoreId1 = 0;
    IdSensorePosizioneTipo sensoreId2 = 0;  // Solo i binari di una stazione simmetrica possono avere due sensori.
    // Per capire qual è la destra e quale la sinistra, si fa riferimento ai binari della stazione centrale.
    LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneSinistra = 0;
    LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneDestra = 0;
    LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneSinistra = 0;
    LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneDestra = 0;
    ProgrammaAbstract& programma;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    SezioneAbstract(ScambiRelayAbstract& scambiRelay, int indiceArraySezioni, IdSezioneTipo idSezione,
                    LunghezzaTracciatoTipo lunghezzaCm, TipologiaSezioneCritica tipologiaSezioneCritica,
                    bool inversioneDiPolarita, ProgrammaAbstract& programma, LoggerAbstract& logger)
        : scambiRelay(scambiRelay),
          indiceArraySezioni(indiceArraySezioni),
          idSezione(idSezione),
          lunghezzaCm(lunghezzaCm),
          tipologiaSezioneCritica(tipologiaSezioneCritica),
          inversioneDiPolarita(inversioneDiPolarita),
          programma(programma),
          logger(logger) {}

    // *** DEFINIZIONE METODI *** //

   private:
    int getNumeroSensori() const;
    IdSensorePosizioneTipo getSensoreId1() const;
    IdSensorePosizioneTipo getSensoreId2() const;
    LunghezzaTracciatoTipo getDistanzaDaSensoreId1AFineSezioneSinistra() const;
    LunghezzaTracciatoTipo getDistanzaDaSensoreId1AFineSezioneDestra() const;
    LunghezzaTracciatoTipo getDistanzaDaSensoreId2AFineSezioneSinistra() const;
    LunghezzaTracciatoTipo getDistanzaDaSensoreId2AFineSezioneDestra() const;

   public:
    bool hasSensore();
    bool hasDueSensori() const;
    bool hasInversioneDiPolarita() const;

    IdSezioneTipo getId();
    LunghezzaTracciatoTipo getLunghezzaCm() const;
    TipologiaSezioneCritica getTipologiaSezioneCritica();
    StatoSezione getStatoSezione() const;

    LunghezzaTracciatoTipo getDistanzaDaSensoreAFineSezioneCm(bool direzioneDestra, int numeroSensore);
    LunghezzaTracciatoTipo getDistanzaDaSensoreAInizioSezioneCm(bool direzioneDestra, int numeroSensore);
    IdSensorePosizioneTipo getSensore1() const;
    IdSensorePosizioneTipo getSensore2() const;

    bool lock(IdConvoglioTipo idConvoglio);
    void unlock();

    bool isLocked() const;

    bool isCritica(bool direzioneDestra);

    bool isSezioneLungaASufficienzaPerConvoglio(ConvoglioAbstract * convoglio) const;

    IdSensorePosizioneTipo getSensorePassaggioLocomotiva(bool direzionePrimaLocomotivaDestra, int numeroSensore);

    void setSensore1DistanzaDaSensoreAFineSezioneSinistra(
        IdSensorePosizioneTipo sensoreId1Locale, LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneSinistraLocale);
    void setSensore1DistanzaDaSensoreAFineSezioneDestra(
        IdSensorePosizioneTipo sensoreId1Locale, LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneDestraLocale);
    void setSensore2DistanzaDaSensoreAFineSezioneSinistra(
        IdSensorePosizioneTipo sensoreId2Locale, LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneSinistraLocale);
    void setSensore2DistanzaDaSensoreAFineSezioneDestra(
        IdSensorePosizioneTipo sensoreId2Locale, LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneDestraLocale);

    void resetLock();

    // Utile solo per la classe di test. La lunghezza delle sezioni non devono essere modificate.
    void setLunghezzaCm(LunghezzaTracciatoTipo lunghezzaCm);
};

#endif