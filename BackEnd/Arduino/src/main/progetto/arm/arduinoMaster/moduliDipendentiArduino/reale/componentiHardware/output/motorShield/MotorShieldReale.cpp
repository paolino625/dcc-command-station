// *** INCLUDE *** //

#include "MotorShieldReale.h"

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

DigitalPinReale pinMotorShieldPwm(PIN_PWM);
DigitalPinReale pinMotorShieldDir1(PIN_DIR_1);

MotorShieldAbstract motorShieldReale(pinMotorShieldPwm, pinMotorShieldDir1, loggerReale);