#ifndef DCC_COMMAND_STATION_DISPLAYCOMPONENTEABSTRACT_H
#define DCC_COMMAND_STATION_DISPLAYCOMPONENTEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class DisplayComponenteAbstract {
    // *** VARIABILI *** //

    LoggerAbstract& logger;
    int indirizzoI2C;

    // *** COSTRUTTORE *** //

   public:
    DisplayComponenteAbstract(LoggerAbstract& logger, int indirizzoI2C) : logger(logger), indirizzoI2C(indirizzoI2C) {}

    // *** DICHIARAZIONE METODI *** //

   public:
    virtual void inizializza() = 0;
    virtual void pulisce() = 0;
    virtual void posizionaCursore(int colonna, int riga) = 0;
    // virtual void stampa(const __FlashStringHelper* stringa) = 0;
    virtual void print(const char* stringa) = 0;
    virtual void print(int numero) = 0;
    virtual void print(char carattere) = 0;
    virtual void print(float numero) = 0;
    // virtual void stampa(String stringa) = 0;
    virtual void print(unsigned long numero) = 0;
    int getIndirizzoI2C() const { return indirizzoI2C; };
    LoggerAbstract& getLogger() const { return logger; }
};

#endif
