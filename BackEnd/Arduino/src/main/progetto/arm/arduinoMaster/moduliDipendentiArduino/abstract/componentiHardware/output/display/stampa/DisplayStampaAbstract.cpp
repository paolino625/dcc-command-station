// *** INCLUDE *** //

#include "DisplayStampaAbstract.h"

#include <cstring>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/logIn/LogInAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/DisplayTempiLimiteScritta.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/seriale/PorteSeriali.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/logIn/statoLogIn/StatoLogIn.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/bufferMenuDisplayKeypad/BufferMenuDisplayKeypad.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/menuDisplayKeypad/statiMenu/StatiMenu.h"
#include "main/progetto/comune/moduliIndipendentiArduino/funzionalita/connessioneInternet/indirizzoIp/IndirizzoIp.h"

// *** DEFINIZIONE VARIABILI *** //

char *listaIndiceMenu[] = {
    " ", "1. Convogli", "2. Locomotive", "3. Web Server", "4. Tracciato", "5. Accessori", "6. Monitoraggio"};
int numeroElementiLista = 6;

// *** DEFINIZIONE METODI *** //

void DisplayStampaAbstract::Errori::Inizializzazione::amplificatoreAudio() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Inizializzazione";
    const char *terzaRiga = "amplificatore audio";
    const char *quartaRiga = "non riuscita!";
    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Errori::Inizializzazione::ina219() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Inizializzazione";
    const char *terzaRiga = "INA219";
    const char *quartaRiga = "non riuscita!";
    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Errori::Inizializzazione::ina260() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Inizializzazione";
    const char *terzaRiga = "INA260";
    const char *quartaRiga = "non riuscita!";
    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Errori::Inizializzazione::sensoreImpronte() {
    displayCore.getDisplayComponente().pulisce();
    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Inizializzazione";
    const char *terzaRiga = "sensore impronte";
    const char *quartaRiga = "non riuscita!";
    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

// *** DEFINIZIONE METODI *** //

void DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso::display() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Display disconnesso!";
    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso::ina219() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "INA219 disconnesso!";
    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso::ina260() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "INA260 disconnesso!";
    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Errori::ComponenteHardwareDisconnesso::amplificatoreAudio() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Amplificatore audio";
    const char *terzaRiga = "disconnesso!";
    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Generico::inputNonValido(KeypadCoreAbstract &keypadCore) {
    const char *primaRiga = "INPUT NON VALIDO";

    displayCore.stampaTesto1Riga(primaRiga);

    displayCore.attendeConEscapeKey(TEMPO_LIMITE_SCRITTA_DISPLAY_BREVE, keypadCore);
}

void DisplayStampaAbstract::Errori::Generico::fermoProgramma() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Controlla log!";
    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Errori::Generico::inizializzazionePortaSeriale(int numeroPortaSeriale) {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("ERRORE!");

    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print("Inizializzazione");

    displayCore.getDisplayComponente().posizionaCursore(0, 2);

    if (numeroPortaSeriale == PORTA_SERIALE_ARDUINO_SLAVE_RELAY) {
        displayCore.getDisplayComponente().print("Arduino Relay");

    } else if (numeroPortaSeriale == PORTA_SERIALE_ARDUINO_SLAVE_SENSORI) {
        displayCore.getDisplayComponente().print("Arduino Sensori");
    } else {
        displayCore.getDisplayComponente().print("porta seriale ");
        displayCore.getDisplayComponente().print(numeroPortaSeriale);

        displayCore.getDisplayComponente().posizionaCursore(0, 3);
        displayCore.getDisplayComponente().print("non riuscita!");
    }
}

void DisplayStampaAbstract::Errori::Usb::inizializzazione() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Inizializzazione";
    const char *terzaRiga = "USB non riuscita!";
    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Usb::fileInesistente(char *nomeFile) {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Il file");
    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print(nomeFile);
    displayCore.getDisplayComponente().posizionaCursore(0, 2);
    displayCore.getDisplayComponente().print("non esiste!");
}

void DisplayStampaAbstract::Errori::Usb::numeroLocomotiveNonCombacia() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Numero loco";
    const char *secondaRiga = "lette da file";
    const char *terzaRiga = "non corrisponde!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Usb::numeroScambiNonCombacia() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Numero scambi";
    const char *secondaRiga = "letti da file";
    const char *terzaRiga = "non corrisponde!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Mqtt::connessioneNonRiuscita() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Connessione MQTT";
    const char *terzaRiga = "non riuscita!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Mqtt::connessionePersa() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Connessione MQTT";
    const char *terzaRiga = "persa!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Ethernet::connessioneNonRiuscita() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Connessione Ethernet";
    const char *terzaRiga = "non riuscita!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::Ethernet::connessionePersa() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Connessione Ethernet";
    const char *terzaRiga = "persa!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Errori::ArduinoMasterHelper::connessioneNonRiuscita() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "ERRORE!";
    const char *secondaRiga = "Connessione";
    const char *terzaRiga = "non riuscita con";
    const char *quartaRiga = "ArduinoMasterHelper!";
    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Monitoraggio::Assorbimenti::parteFissa() {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Tracciato: ");

    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print("Alim. 5 V: ");
}

void DisplayStampaAbstract::Monitoraggio::Assorbimenti::parteVariabile(int correnteIna219, int correnteIna260) {
    displayCore.getDisplayComponente().posizionaCursore(11, 0);
    displayCore.getDisplayComponente().print(correnteIna219);
    displayCore.getDisplayComponente().print(" mA   ");

    displayCore.getDisplayComponente().posizionaCursore(11, 1);
    displayCore.getDisplayComponente().print("-");
    displayCore.getDisplayComponente().print(correnteIna260);
    displayCore.getDisplayComponente().print(" mA ");
}

void DisplayStampaAbstract::Avvisi::effettuaLogIn() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Premi un tasto";
    const char *secondaRiga = "per effettuare";
    const char *terzaRiga = "il login...";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Avvisi::inCaricamento() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Caricamento in corso";
    const char *secondaRiga = "Attendere prego...";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Avvisi::cortoCircuito() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "CORTO CIRCUITO!";
    const char *secondaRiga = "Premi 1";
    const char *terzaRiga = "per ripristinare";
    const char *quartaRiga = "la corrente...";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Avvisi::credits(DelayAbstract &delay) {
    displayCore.getDisplayComponente().pulisce();

    int delayTransizione = 1000;

    displayCore.getDisplayComponente().posizionaCursore(0, 0);

    displayCore.getDisplayComponente().print("DCC COMMAND STATION");

    delay.milliseconds(delayTransizione);

    displayCore.getDisplayComponente().posizionaCursore(9, 1);
    displayCore.getDisplayComponente().print("by");

    delay.milliseconds(delayTransizione);

    displayCore.getDisplayComponente().posizionaCursore(3, 2);
    displayCore.getDisplayComponente().print("Paolo Calcagni");

    delay.milliseconds(delayTransizione);

    displayCore.getDisplayComponente().posizionaCursore(3, 3);
    displayCore.getDisplayComponente().print("Aldo  Picciani");
}

void DisplayStampaAbstract::LogIn::sceltaTipologia() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Appoggia il dito";
    const char *secondaRiga = "sul sensore";
    const char *terzaRiga = "oppure digita 1 per";
    const char *quartaRiga = "inserire password";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::LogIn::digitaPassword() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Digita la password";

    displayCore.stampaTesto1Riga(primaRiga);

    // Riposiziono il cursore alla seconda riga in modo tale che la password venga visualizzata lì
    displayCore.getDisplayComponente().posizionaCursore(0, 1);
}

void DisplayStampaAbstract::LogIn::passwordNonValida() {
    displayCore.getDisplayComponente().pulisce();
    const char *primaRiga = "Password non valida!";

    displayCore.stampaTesto1Riga(primaRiga);
}

void DisplayStampaAbstract::LogIn::appoggiaDito() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Appoggia il dito";
    const char *secondaRiga = "sul sensore";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::LogIn::benvenuto() {
    displayCore.getDisplayComponente().pulisce();

    if (strcmp(utenti.lista[statoLogIn.indiceUtenteLoggato].sesso, "MASCHIO") == 0) {
        displayCore.getDisplayComponente().posizionaCursore(0, 0);
        displayCore.getDisplayComponente().print("Bentornato ");
        displayCore.getDisplayComponente().print(utenti.lista[statoLogIn.indiceUtenteLoggato].nome);
        displayCore.getDisplayComponente().print("!");
    } else if (strcmp(utenti.lista[statoLogIn.indiceUtenteLoggato].sesso, "FEMMINA") == 0) {
        displayCore.getDisplayComponente().posizionaCursore(0, 0);
        displayCore.getDisplayComponente().print("Bentornata ");
        displayCore.getDisplayComponente().print(utenti.lista[statoLogIn.indiceUtenteLoggato].nome);
        displayCore.getDisplayComponente().print("!");
    }

    delay.milliseconds(TEMPO_LIMITE_SCRITTA_DISPLAY_MEDIO);
}

void DisplayStampaAbstract::LogIn::erroreNumeroUtenti() {
    const char *primaRiga = "Numero utenti";
    const char *secondaRiga = "letti da file";
    const char *terzaRiga = "non corrisponde!";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Convoglio::lista(ListaConvogliAbstract &listaConvogli, TempoAbstract &tempo,
                                             bool convogliInUso, bool convogliNonInUso, bool mostraStatoConvoglio) {
    int numeroConvogliTrovati = listaConvogli.crea(convogliInUso, convogliNonInUso, mostraStatoConvoglio);
    if (numeroConvogliTrovati == 0) {
        if (displayCore.avvisoDaMostrare) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Nessun convoglio da mostrare sul display.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            erroreNessunConvoglioTrovato();
            displayCore.avvisoDaMostrare = false;
        }
        return;
    }

    // stampoLista(listaLocomotive, numeroLocomotiveTrovate);

    displayCore.displayLista(tempo, listaConvogli.get(), numeroConvogliTrovati);
}

void DisplayStampaAbstract::Convoglio::erroreNessunConvoglioTrovato() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Nessun convoglio";
    const char *secondaRiga = "trovato!";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::confermaReset() {
    const char *primaRiga = "Resetto i convogli?";
    const char *secondaRiga = "1. Si";
    const char *terzaRiga = "2. No";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Convoglio::mostra() {
    const char *primaRiga = "1. Aggiungi";
    const char *secondaRiga = "2. Rimuovi";
    const char *terzaRiga = "3. Gestisci";
    const char *quartaRiga = "4. Reset";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::gestioneConvoglio() {
    const char *primaRiga = "1. Locomotive";
    const char *secondaRiga = "2. Vagoni";
    const char *terzaRiga = "3. Presenza";
    const char *quartaRiga = "4. Diff loco";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::inserimentoConvoglio() {
    const char *primaRiga = "Specifica convoglio: ";
    const char *secondaRiga = "1. Sinistra";
    const char *terzaRiga = "2. Destra";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::aggiuntaEliminazioneVagone() {
    const char *primaRiga = "1. Aggiungi vagone";
    const char *secondaRiga = "2. Elimina vagone";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::gestionePresenza() {
    const char *primaRiga = "1. Togli convoglio";
    const char *secondaRiga = "2. Metti convoglio";
    const char *terzaRiga = "3. Lista convogli";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::gestioneDiffLoco(ConvoglioAbstractReale *convoglio) {
    StepVelocitaKmHTipo differenzaStepVelocitaTraLocomotive = convoglio->getDifferenzaStepVelocitaTraLocomotive();
    static char buffer[64];
    snprintf(buffer, sizeof(buffer), "Diff attuale: %d", differenzaStepVelocitaTraLocomotive);
    const char *primaRiga = buffer;

    const char *secondaRiga = "1. Reset a 0";
    const char *terzaRiga = "2. Set positivo";
    const char *quartaRiga = "3. Set negativo";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Convoglio::Scelta::eliminazioneLoco() {
    const char *primaRiga = "Elimino loco?";
    const char *secondaRiga = "1. Si";
    const char *terzaRiga = "2. No";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Locomotiva::Scelta::indirizzoLoco() {
    const char *primaRiga = "Inserisci il nuovo";
    const char *secondaRiga = "indirizzo: ";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Locomotiva::Scelta::idLoco() {
    const char *primaRiga = "Inserisci loco: ";

    displayCore.stampaTesto1Riga(primaRiga);
}

void DisplayStampaAbstract::Locomotiva::Locomotiva::lista(ListaLocomotiveAbstract &listaLocomotive,
                                                          TempoAbstract &tempo, bool locomotiveInUso,
                                                          bool locomotiveNonInUso) {
    int numeroLocomotiveTrovate = listaLocomotive.crea(locomotiveInUso, locomotiveNonInUso);
    if (numeroLocomotiveTrovate == 0) {
        if (displayCore.avvisoDaMostrare) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Nessuna locomotiva da mostrare sul display.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            erroreListaNessunaLocomotivaTrovata();
            displayCore.avvisoDaMostrare = false;
        }
        return;
    }

    // stampoLista(listaLocomotive, numeroLocomotiveTrovate);

    displayCore.displayLista(tempo, listaLocomotive.get(), numeroLocomotiveTrovate);
}

void DisplayStampaAbstract::Locomotiva::erroreListaNessunaLocomotivaTrovata() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Nessuna loco";
    const char *secondaRiga = "trovata!";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Locomotiva::Scelta::valoreKeepAlive() {
    const char *primaRiga = "Cambio tempo ON";
    const char *secondaRiga = "del Keep Alive.";
    const char *terzaRiga = "Inserisci numero ";
    const char *quartaRiga = "da 0 a 255: ";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Locomotiva::Scelta::operazioneLocomotiva() {
    const char *primaRiga = "1. Tempo uso";
    const char *secondaRiga = "2. Mapping velocita'";
    const char *terzaRiga = "3. Set Decoder";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Homepage::idLocomotiva(byte colonnaConvoglioLoco, ConvoglioAbstractReale *convoglio,
                                                   byte posizioneLocomotiva) {
    if (posizioneLocomotiva == 1) {
        displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco, 0);
        // Se il convoglio è vuoto stampo 0
        displayCore.getDisplayComponente().print(convoglio->getIdLocomotiva1());

    } else if (posizioneLocomotiva == 2) {
        // Se la seconda locomotiva non è settata non la visualizzo.
        if (convoglio->hasDoppiaLocomotiva()) {
            displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco, 0);
            displayCore.getDisplayComponente().print(convoglio->getIdLocomotiva2());

        } else {
            displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco, 0);
            displayCore.getDisplayComponente().print(" ");
        }
    }
}

void DisplayStampaAbstract::Homepage::velocitaLocomotiva(byte colonnaConvoglioLoco, byte rigaConvoglioLoco,
                                                         LocomotivaAbstract *locomotiva) {
    int velocitaImpostata = locomotiva->getVelocitaImpostata();

    byte colonna = colonnaConvoglioLoco - 1;

    if (velocitaImpostata >= 100) {
        displayCore.getDisplayComponente().posizionaCursore(colonna, rigaConvoglioLoco);
        displayCore.getDisplayComponente().print(velocitaImpostata);
    } else if (velocitaImpostata <= 100 && velocitaImpostata >= 10) {
        displayCore.getDisplayComponente().posizionaCursore(colonna, rigaConvoglioLoco);
        displayCore.getDisplayComponente().print(" ");
        displayCore.getDisplayComponente().posizionaCursore(colonna + 1, rigaConvoglioLoco);
        displayCore.getDisplayComponente().print(velocitaImpostata);
    } else {
        displayCore.getDisplayComponente().posizionaCursore(colonna, rigaConvoglioLoco);
        displayCore.getDisplayComponente().posizionaCursore(colonna, rigaConvoglioLoco);
        displayCore.getDisplayComponente().print(" ");
        displayCore.getDisplayComponente().posizionaCursore(colonna + 1, rigaConvoglioLoco);
        displayCore.getDisplayComponente().print(velocitaImpostata);
        displayCore.getDisplayComponente().posizionaCursore(colonna + 2, rigaConvoglioLoco);
        displayCore.getDisplayComponente().print(" ");
    }
}

void DisplayStampaAbstract::Homepage::direzioneLocomotiva(byte colonnaConvoglioLoco, ConvoglioAbstractReale *convoglio,
                                                          byte posizioneLocomotiva) {
    displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco, 1);
    if (posizioneLocomotiva == 1) {
        // Stampo la direzione solo se il convoglio è in uso
        if (convoglio->isInUso()) {
            if (convoglio->getLocomotiva1()->getDirezioneAvantiDisplay()) {
                displayCore.getDisplayComponente().print(">");
            }

            else {
                displayCore.getDisplayComponente().print("<");
            }
        }

    } else if (posizioneLocomotiva == 2) {
        // Se la locomotiva non è settata non la visualizzo.
        if (convoglio->hasDoppiaLocomotiva()) {
            if (convoglio->getLocomotiva2()->getDirezioneAvantiDisplay()) {
                displayCore.getDisplayComponente().print(">");
            } else {
                displayCore.getDisplayComponente().print("<");
            }

        } else {
            displayCore.getDisplayComponente().print(" ");
        }
    }
}

void DisplayStampaAbstract::Homepage::idLocomotiveConvoglio(ConvoglioAbstractReale *convoglio,
                                                            byte colonnaConvoglioLoco1, byte colonnaConvoglioLoco2) {
    idLocomotiva(colonnaConvoglioLoco1, convoglio, 1);
    idLocomotiva(colonnaConvoglioLoco2, convoglio, 2);

    idLocomotiva(colonnaConvoglioLoco1, convoglio, 1);
    idLocomotiva(colonnaConvoglioLoco2, convoglio, 2);
}

void DisplayStampaAbstract::Homepage::velocitaConvoglio(ConvoglioAbstractReale *convoglio, byte colonnaConvoglioLoco1,
                                                        byte colonnaConvoglioLoco2,
                                                        byte rigaConvoglioVelocitaImpostata) {
    if (convoglio->isInUso()) {
        velocitaLocomotiva(colonnaConvoglioLoco1, rigaConvoglioVelocitaImpostata, convoglio->getLocomotiva1());
        LocomotivaAbstract *locomotiva = convoglio->getLocomotiva1();
        // Se l'autopilot non è inserito, stampo un asterisco per indicare che la velocità impostata non è stata ancora
        // raggiunta
        if (locomotiva->getVelocitaAttuale() != locomotiva->getVelocitaImpostata()) {
            displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco1 + 2,
                                                                rigaConvoglioVelocitaImpostata);
            displayCore.getDisplayComponente().print("*");
        } else {
            displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco1 + 2,
                                                                rigaConvoglioVelocitaImpostata);
            displayCore.getDisplayComponente().print(" ");
        }
    }

    if (convoglio->hasDoppiaLocomotiva()) {
        velocitaLocomotiva(colonnaConvoglioLoco2, rigaConvoglioVelocitaImpostata, convoglio->getLocomotiva2());
    } else {
        displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco2 - 1, rigaConvoglioVelocitaImpostata);
        displayCore.getDisplayComponente().print("   ");
    }
}

void DisplayStampaAbstract::Homepage::direzioneConvoglio(ConvoglioAbstractReale *convoglio, byte colonnaConvoglioLoco1,
                                                         byte colonnaConvoglioLoco2) {
    direzioneLocomotiva(colonnaConvoglioLoco1, convoglio, 1);
    direzioneLocomotiva(colonnaConvoglioLoco2, convoglio, 2);
}

void DisplayStampaAbstract::Homepage::luciConvoglio(ConvoglioAbstractReale *convoglio, byte colonnaConvoglioLoco1,
                                                    byte colonnaConvoglioLoco2) {
    displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco1, 2);

    // Se la prima locomotiva c'è stampo, altrimenti no
    if (convoglio->isInUso()) {
        if (convoglio->getLocomotiva1()->getStatoFunzioniAusiliari(0)) {
            displayCore.getDisplayComponente().print("*");
        } else {
            displayCore.getDisplayComponente().print("O");
        }
    }

    // Se c'è anche la seconda locomotiva, stampiamo anche lo stato delle sue luci
    displayCore.getDisplayComponente().posizionaCursore(colonnaConvoglioLoco2, 2);
    if (convoglio->hasDoppiaLocomotiva()) {
        if (convoglio->getLocomotiva2()->getStatoFunzioniAusiliari(0)) {
            displayCore.getDisplayComponente().print("*");
        } else {
            displayCore.getDisplayComponente().print("O");
        }

    } else {
        displayCore.getDisplayComponente().print(" ");
    }
}

void DisplayStampaAbstract::Homepage::velocitaLuciDirezioneConvoglio(ConvoglioAbstractReale *convoglio,
                                                                     byte colonnaConvoglioLoco1,
                                                                     byte colonnaConvoglioLoco2,
                                                                     byte rigaConvoglioVelocitaImpostata) {
    velocitaConvoglio(convoglio, colonnaConvoglioLoco1, colonnaConvoglioLoco2, rigaConvoglioVelocitaImpostata);
    direzioneConvoglio(convoglio, colonnaConvoglioLoco1, colonnaConvoglioLoco2);
    luciConvoglio(convoglio, colonnaConvoglioLoco1, colonnaConvoglioLoco2);
}

void DisplayStampaAbstract::Homepage::luciConvogli(ConvogliAbstract &convogli) {
    luciConvoglio(convogli.getConvoglio(idConvoglioCorrente1Display, false), colonnaConvoglio1Loco1,
                  colonnaConvoglio1Loco2);
    if (convogli.getConvoglio(idConvoglioCorrente2Display, false)->isInUso()) {
        luciConvoglio(convogli.getConvoglio(idConvoglioCorrente2Display, false), colonnaConvoglio2Loco1,
                      colonnaConvoglio2Loco2);
    }
}

void DisplayStampaAbstract::Homepage::idLocomotiveConvoglio1(ConvogliAbstract &convogli) {
    idLocomotiveConvoglio(convogli.getConvoglio(idConvoglioCorrente1Display, false), colonnaConvoglio1Loco1,
                          colonnaConvoglio1Loco2);
}

void DisplayStampaAbstract::Homepage::idLocomotiveConvoglio2(ConvogliAbstract &convogli) {
    idLocomotiveConvoglio(convogli.getConvoglio(idConvoglioCorrente2Display, false), colonnaConvoglio2Loco1,
                          colonnaConvoglio2Loco2);
}

void DisplayStampaAbstract::Homepage::velocitaConvoglio1(ConvogliAbstract &convogli) {
    velocitaConvoglio(convogli.getConvoglio(idConvoglioCorrente1Display, false), colonnaConvoglio1Loco1,
                      colonnaConvoglio1Loco2, rigaConvoglio1VelocitaImpostata);
}

void DisplayStampaAbstract::Homepage::velocitaConvoglio2(ConvogliAbstract &convogli) {
    velocitaConvoglio(convogli.getConvoglio(idConvoglioCorrente2Display, false), colonnaConvoglio2Loco1,
                      colonnaConvoglio2Loco2, rigaConvoglio2VelocitaImpostata);
}

void DisplayStampaAbstract::Homepage::direzioneConvoglio1(ConvogliAbstract &convogli) {
    direzioneConvoglio(convogli.getConvoglio(idConvoglioCorrente1Display, false), colonnaConvoglio1Loco1,
                       colonnaConvoglio1Loco2);
}

void DisplayStampaAbstract::Homepage::direzioneConvoglio2(ConvogliAbstract &convogli) {
    direzioneConvoglio(convogli.getConvoglio(idConvoglioCorrente2Display, false), colonnaConvoglio2Loco1,
                       colonnaConvoglio2Loco2);
}

void DisplayStampaAbstract::Homepage::luciConvoglio1(ConvogliAbstract &convogli) {
    luciConvoglio(convogli.getConvoglio(idConvoglioCorrente1Display, false), colonnaConvoglio1Loco1,
                  colonnaConvoglio1Loco2);
}

void DisplayStampaAbstract::Homepage::luciConvoglio2(ConvogliAbstract &convogli) {
    luciConvoglio(convogli.getConvoglio(idConvoglioCorrente2Display, false), colonnaConvoglio2Loco1,
                  colonnaConvoglio2Loco2);
}

void DisplayStampaAbstract::Homepage::display(ConvogliAbstract &convogli) {
    displayCore.getDisplayComponente().pulisce();

    // TITOLI

    getDisplayCore().getDisplayComponente().posizionaCursore(colonnaMenu, 0);
    getDisplayCore().getDisplayComponente().print("LOCO ");

    getDisplayCore().getDisplayComponente().posizionaCursore(colonnaMenu, 1);
    getDisplayCore().getDisplayComponente().print("Dir ");

    getDisplayCore().getDisplayComponente().posizionaCursore(colonnaMenu, 2);
    getDisplayCore().getDisplayComponente().print("Luci ");

    getDisplayCore().getDisplayComponente().posizionaCursore(colonnaMenu, 3);
    getDisplayCore().getDisplayComponente().print("Vel ");

    // 1° CONVOGLIO

    // Verifichiamo che dobbiamo mostrare il primo convoglio
    if (idConvoglioCorrente1Display != 0) {
        idLocomotiveConvoglio1(convogli);

        direzioneConvoglio1(convogli);

        luciConvoglio1(convogli);

        velocitaConvoglio1(convogli);
    }

    // LINEA DI MEZZO

    displayCore.getDisplayComponente().posizionaCursore(colonnaSeparazione, 0);
    displayCore.getDisplayComponente().print("|");

    displayCore.getDisplayComponente().posizionaCursore(colonnaSeparazione, 1);
    displayCore.getDisplayComponente().print("|");

    displayCore.getDisplayComponente().posizionaCursore(colonnaSeparazione, 2);
    displayCore.getDisplayComponente().print("|");

    displayCore.getDisplayComponente().posizionaCursore(colonnaSeparazione, 3);
    displayCore.getDisplayComponente().print("|");

    // 2° CONVOGLIO

    // Il convoglio 2 potrebbe non essere presente
    if (idConvoglioCorrente2Display != 0) {
        idLocomotiveConvoglio2(convogli);

        direzioneConvoglio2(convogli);

        luciConvoglio2(convogli);

        velocitaConvoglio2(convogli);
    }
}

void DisplayStampaAbstract::Tracciato::Scambi::reset() {
    const char *primaRiga = "Reset scambi";
    const char *secondaRiga = "in corso...";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Tracciato::Scambi::cambia() {
    const char *primaRiga = "Inserisci il numero";
    const char *secondaRiga = "dello scambio: ";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Tracciato::SensoriPosizione::inserisciSensore() {
    const char *primaRiga = "Inserisci il numero";
    const char *secondaRiga = "del sensore: ";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Tracciato::SensoriPosizione::parteFissa() {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Stato sensore: ");
}

void DisplayStampaAbstract::Tracciato::SensoriPosizione::parteVariabile(
    DelayAbstract &delay, SensoriPosizioneAbstractReale &sensoriPosizione) {
    byte colonnaParteVariabileInizio = 15;
    displayCore.getDisplayComponente().posizionaCursore(colonnaParteVariabileInizio, 0);
    if (sensoriPosizione.getSensorePosizione(numeroSensoreDisplayKeypad)->fetchStato()) {
        // Se il sensore è stato azionato, mostro asterisco per 1 secondo.
        displayCore.getDisplayComponente().print("*");
        delay.milliseconds(1000);

        displayCore.getDisplayComponente().posizionaCursore(colonnaParteVariabileInizio, 0);
        displayCore.getDisplayComponente().print(" ");
    }
}

void DisplayStampaAbstract::Locomotiva::Mapping::schermataPrincipale(LocomotivaAbstract *locomotiva,
                                                                     StepVelocitaKmHTipo stepVelocita, int numeroGiro) {
    displayCore.getDisplayComponente().pulisce();
    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Loco: ");
    displayCore.getDisplayComponente().posizionaCursore(6, 0);
    displayCore.getDisplayComponente().print(locomotiva->getId());

    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print("Step: ");
    displayCore.getDisplayComponente().posizionaCursore(6, 1);
    displayCore.getDisplayComponente().print(stepVelocita);

    displayCore.getDisplayComponente().posizionaCursore(0, 2);
    displayCore.getDisplayComponente().print("Dir: ");
    if (locomotiva->getDirezioneAvantiDisplay()) {
        displayCore.getDisplayComponente().print(">");
    } else {
        displayCore.getDisplayComponente().print("<");
    }

    displayCore.getDisplayComponente().posizionaCursore(0, 3);
    displayCore.getDisplayComponente().print("Giro: ");
    displayCore.getDisplayComponente().print(numeroGiro + 1);
}

void DisplayStampaAbstract::Locomotiva::Mapping::mappingTerminato(TimestampTipo minutiDurataMapping) {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Mapping finito!");
    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print("Durata: ");
    displayCore.getDisplayComponente().print(minutiDurataMapping);
    displayCore.getDisplayComponente().print(" minuti");
}

void DisplayStampaAbstract::Locomotiva::Mapping::posizionamentoLocomotivaSulTracciato() {
    displayCore.getDisplayComponente().pulisce();
    const char *primaRiga = "Posiziona la loco";
    const char *secondaRiga = "sul tracciato...";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Locomotiva::TempoFunzionamentoLocomotive::menu() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "1. Stato loco";
    const char *secondaRiga = "2. Manutenzione";
    const char *terzaRiga = "   effettuata";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Locomotiva::SetDecoderLocomotiva::menu() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "1. Cambia indirizzo";
    const char *secondaRiga = "2. Cambia keep alive";
    const char *terzaRiga = "3. Reset acc/dec";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Locomotiva::TempoFunzionamentoLocomotive::schermataPrincipale(
    KeypadCoreAbstract &keypadCore, LocomotivaAbstract *locomotiva) {
    displayCore.getDisplayComponente().pulisce();
    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Ultima manutenzione: ");
    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print(locomotiva->getTempoFunzionamentoUltimaManutenzione());
    displayCore.getDisplayComponente().print(" minuti");
    displayCore.getDisplayComponente().posizionaCursore(0, 2);
    displayCore.getDisplayComponente().print("Tempo totale uso: ");
    displayCore.getDisplayComponente().posizionaCursore(0, 3);
    displayCore.getDisplayComponente().print(locomotiva->getTempoFunzionamentoTotale());
    displayCore.getDisplayComponente().print(" minuti");

    displayCore.attendeConEscapeKey(TEMPO_LIMITE_SCRITTA_DISPLAY_LUNGO, keypadCore);
}

void DisplayStampaAbstract::Locomotiva::Mapping::Scelta::sensore(IdSensorePosizioneTipo idSensore) {
    displayCore.getDisplayComponente().pulisce();
    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("Inserisci il numero");
    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print("del sensore ");
    displayCore.getDisplayComponente().print(idSensore);
    displayCore.getDisplayComponente().print(": ");
}

void DisplayStampaAbstract::Locomotiva::Mapping::Scelta::modalita() {
    const char *primaRiga = "1. Tutti gli step";
    const char *secondaRiga = "2. Singolo step";
    const char *terzaRiga = "3. Intervallo step";

    displayCore.stampaTesto3Righe(primaRiga, secondaRiga, terzaRiga);
}

void DisplayStampaAbstract::Locomotiva::Mapping::Scelta::step() {
    const char *primaRiga = "Inserisci step: ";

    displayCore.stampaTesto1Riga(primaRiga);
}

void DisplayStampaAbstract::Locomotiva::Mapping::Scelta::stepIniziale() {
    const char *primaRiga = "Inserisci lo step";
    const char *secondaRiga = "iniziale: ";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Locomotiva::Mapping::Scelta::stepFinale() {
    const char *primaRiga = "Inserisci lo step";
    const char *secondaRiga = "finale: ";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Locomotiva::Mapping::Scelta::modalitaSensori() {
    const char *primaRiga = "1. Un sensore";
    const char *secondaRiga = "2. Due sensori";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Accessori::displayMenuAccessori() {
    const char *primaRiga = "1. LED";
    const char *secondaRiga = "2. Audio";
    const char *terzaRiga = "3. Sensore impronte";
    const char *quartaRiga = "4. Buzzer";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Accessori::Buzzer::menu() {
    const char *primaRiga = "1. Test";
    const char *secondaRiga = "2. Cambia volume";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Accessori::SensoreImpronte::salvataggioNuovaImpronta() {
    displayCore.getDisplayComponente().pulisce();
    for (int i = 1; i < NUMERO_UTENTI; i++) {
        displayCore.getDisplayComponente().posizionaCursore(0, i - 1);
        displayCore.getDisplayComponente().print(i);
        displayCore.getDisplayComponente().print(". ");
        displayCore.getDisplayComponente().print(utenti.lista[i].nome);
    }
}

void DisplayStampaAbstract::Accessori::SensoreImpronte::menu() {
    const char *primaRiga = "1. Aggiungi impronta";
    const char *secondaRiga = "2. Reset impronte";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Accessori::Audio::inserimentoVolume() {
    const char *primaRiga = "Inserisci numero";
    const char *secondaRiga = "da 1 a 9: ";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Accessori::Audio::menu() {
    const char *primaRiga = "1. Test";
    const char *secondaRiga = "2. Cambia volume";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Tracciato::Scambi::menu() {
    const char *primaRiga = "1. Aziona";
    const char *secondaRiga = "2. Reset";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Tracciato::SensoriPosizione::displayMenuSensoriIr() {
    const char *primaRiga = "1. Test";

    displayCore.stampaTesto1Riga(primaRiga);
}

void DisplayStampaAbstract::Monitoraggio::mostra() {
    const char *primaRiga = "1. Tensioni";
    const char *secondaRiga = "2. Assorbimenti";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Tracciato::mostra(MotorShieldAbstract &motorShield) {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    if (motorShield.isAccesa()) {
        displayCore.getDisplayComponente().print("1. Motor Shield OFF");
    } else {
        displayCore.getDisplayComponente().print("1. Motor Shield ON");
    }

    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print("2. Scambi");

    displayCore.getDisplayComponente().posizionaCursore(0, 2);
    displayCore.getDisplayComponente().print("3. Sensori IR");
}

void DisplayStampaAbstract::Accessori::SensoreImpronte::confermaResetDatabase() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Premi 1";
    const char *secondaRiga = "per confermare il";
    const char *terzaRiga = "reset del database";
    const char *quartaRiga = "delle impronte";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Accessori::Led::menu() {
    const char *primaRiga = "1. Bianco";
    const char *secondaRiga = "2. Rosso";
    const char *terzaRiga = "3. Verde";
    const char *quartaRiga = "4. Blu 5. Spegni";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Convoglio::Vagoni::displayErroreNessunVagoneTrovato() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "Nessun vagone";
    const char *secondaRiga = "trovato!";

    displayCore.stampaTesto2Righe(primaRiga, secondaRiga);
}

void DisplayStampaAbstract::Convoglio::Vagoni::lista(TempoAbstract &tempo, bool vagoniInUso, bool vagoniNonInUso,
                                                     ConvoglioAbstractReale *convoglio,
                                                     ListaVagoniAbstract &listaVagoni) {
    int numeroVagoniTrovati = listaVagoni.creo(vagoniInUso, vagoniNonInUso, convoglio);
    if (numeroVagoniTrovati == 0) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Nessun vagone da mostrare sul display.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        displayErroreNessunVagoneTrovato();
        return;
    }

    displayCore.displayLista(tempo, listaVagoni.get(), numeroVagoniTrovati);
}

void DisplayStampaAbstract::Convoglio::DifferenzaVelocitaLocomotivaTrazioneSpinta::inserisciStep() {
    const char *primaRiga = "Inserisci lo step: ";

    displayCore.stampaTesto1Riga(primaRiga);
}

void DisplayStampaAbstract::Monitoraggio::Tensioni::displayTensioniParteFissa1() {
    displayCore.getDisplayComponente().pulisce();

    const char *primaRiga = "CDU: ";
    const char *secondaRiga = "Tracciato: ";
    const char *terzaRiga = "Relay: ";
    const char *quartaRiga = "Sensori IR: ";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Monitoraggio::Tensioni::displayTensioniParteVariabile1() {
    displayCore.getDisplayComponente().posizionaCursore(5, 0);
    displayCore.getDisplayComponente().print(alimentazioneCDU.getTensione());
    displayCore.getDisplayComponente().print(" V");

    displayCore.getDisplayComponente().posizionaCursore(11, 1);
    displayCore.getDisplayComponente().print(alimentazioneTracciato.getTensione());
    displayCore.getDisplayComponente().print(" V");

    displayCore.getDisplayComponente().posizionaCursore(7, 2);
    displayCore.getDisplayComponente().print(alimentazione12V.getTensione());
    displayCore.getDisplayComponente().print(" V");

    displayCore.getDisplayComponente().posizionaCursore(12, 3);
    displayCore.getDisplayComponente().print(alimentazioneSensoriPosizione.getTensione());
    displayCore.getDisplayComponente().print(" V");
}

void DisplayStampaAbstract::Monitoraggio::Tensioni::displayTensioniParteFissa2() {
    displayCore.getDisplayComponente().pulisce();
    const char *primaRiga = "Arduino: ";
    const char *secondaRiga = "Elementi 5 V: ";
    const char *terzaRiga = "Elementi 3 V: ";
    const char *quartaRiga = "LED 1: ";

    displayCore.stampaTesto4Righe(primaRiga, secondaRiga, terzaRiga, quartaRiga);
}

void DisplayStampaAbstract::Monitoraggio::Tensioni::displayTensioniParteVariabile2() {
    displayCore.getDisplayComponente().posizionaCursore(9, 0);
    displayCore.getDisplayComponente().print(alimentazioneArduino.getTensione());
    displayCore.getDisplayComponente().print(" V");

    displayCore.getDisplayComponente().posizionaCursore(14, 1);
    displayCore.getDisplayComponente().print(alimentazioneComponentiAusiliari5V.getTensione());
    displayCore.getDisplayComponente().print(" V");

    displayCore.getDisplayComponente().posizionaCursore(14, 2);
    displayCore.getDisplayComponente().print(alimentazioneComponentiAusiliari3V.getTensione());
    displayCore.getDisplayComponente().print(" V");

    displayCore.getDisplayComponente().posizionaCursore(7, 3);
    displayCore.getDisplayComponente().print(alimentazioneLED1.getTensione());
    displayCore.getDisplayComponente().print(" V");
}

void DisplayStampaAbstract::WebServer::ip() {
    displayCore.getDisplayComponente().pulisce();

    displayCore.getDisplayComponente().posizionaCursore(0, 0);
    displayCore.getDisplayComponente().print("INDIRIZZO IP");

    displayCore.getDisplayComponente().posizionaCursore(0, 1);
    displayCore.getDisplayComponente().print(indirizzoIp);
}

void DisplayStampaAbstract::WebServer::menu() {
    const char *primaRiga = "1. Mostra IP";

    displayCore.stampaTesto1Riga(primaRiga);
}

void DisplayStampaAbstract::MenuPrincipale::mostra(TempoAbstract &tempo) {
    displayCore.displayLista(tempo, listaIndiceMenu, numeroElementiLista);
}

void DisplayStampaAbstract::MenuPrincipale::resettoPosizione() {
    fuoriMenu = FuoriMenuEnum::LOG_IN_NON_EFFETTUATO;
    menuConvogli = MenuConvogliEnum::SCELTA_IN_CORSO;
    menuLocomotive = MenuLocomotiveEnum::INSERISCI_LOCO;
    menuConvogliGestisci = MenuConvogliGestisciEnum::SCELTA_IN_CORSO;
    menuConvogliGestisciLoco = MenuConvogliGestisciLocoEnum::INSERIMENTO_CONVOGLIO;
    menuConvogliGestisciVagoni = MenuConvogliGestisciVagoniEnum::SCELTA_IN_CORSO;
    menuConvogliGestisciPresenza = MenuConvogliGestisciPresenzaEnum::SCELTA_IN_CORSO;
    menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta =
        MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum::INSERIMENTO_CONVOGLIO;
    menuLocomotiveDecoder = MenuLocomotiveDecoderEnum::SCELTA_IN_CORSO;
    menuLocomotiveTempoUso = MenuLocomotiveTempoUsoEnum::SCELTA_IN_CORSO;
    menuLocomotiveMappingVelocita = MenuLocomotiveMappingVelocitaEnum::AVVISO;
    menuLocomotiveMappingVelocitaTuttiStep = MenuLocomotiveMappingVelocitaTuttiStepEnum::INSERISCI_SENSORE_1;
    menuLocomotiveMappingVelocitaSingoloStep = MenuLocomotiveMappingVelocitaSingoloStepEnum::INSERISCI_STEP;
    menuLocomotiveMappingVelocitaIntervalloStep = MenuLocomotiveMappingVelocitaIntervalloStepEnum::INSERISCI_STEP_1;
    menuWebServer = MenuWebServerEnum::SCELTA_IN_CORSO;
    menuTracciato = MenuTracciatoEnum::SCELTA_IN_CORSO;
    menuAccessori = MenuAccessoriEnum::SCELTA_IN_CORSO;
    menuAccessoriAudio = MenuAccessoriAudioEnum::SCELTA_IN_CORSO;
    menuAccessoriSensoreImpronte = MenuAccessoriSensoreImpronteEnum::SCELTA_IN_CORSO;
    menuAccessoriBuzzer = MenuAccessoriBuzzerEnum::SCELTA_IN_CORSO;
    menuAccessoriSensoreImpronteAggiungiImpronta = MenuAccessoriSensoreImpronteAggiungiImprontaEnum::SCELTA_IN_CORSO;
    menuMonitoraggio = MenuMonitoraggioEnum::SCELTA_IN_CORSO;
    menuTracciatoScambi = MenuTracciatoScambiEnum::SCELTA_IN_CORSO;
    menuTracciatoSensoriIr = MenuTracciatoSensoriIrEnum::SCELTA_IN_CORSO;
    menuTracciatoSensoriIrTest = MenuTracciatoSensoriIrTestEnum::INSERISCI_SENSORE;
}
