#ifndef DCC_COMMAND_STATION_ACCELERAZIONEDECELERAZIONE_H
#define DCC_COMMAND_STATION_ACCELERAZIONEDECELERAZIONE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** CLASSE *** //

class AccelerazioneDecelerazioneAbstract {
    // *** VARIABILI *** //

   public:
    ConvogliAbstract &convogli;
    LoggerAbstract &logger;

    // *** COSTRUTTORE *** //

    AccelerazioneDecelerazioneAbstract(ConvogliAbstract &convogli, LoggerAbstract &logger)
        : convogli(convogli), logger(logger) {}

    // *** DICHIARAZIONE FUNZIONI *** //

    void aggiorna();
};

#endif
