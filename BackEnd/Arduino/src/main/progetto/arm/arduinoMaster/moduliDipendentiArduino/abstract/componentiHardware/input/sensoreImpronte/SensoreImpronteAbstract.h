#ifndef DCC_COMMAND_STATION_SENSOREIMPRONTEABSTRACT_H
#define DCC_COMMAND_STATION_SENSOREIMPRONTEABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoreImpronte/componente/SensoreImpronteComponenteAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define TEMPO_LED_ROSSO_IMPRONTA_NON_VALIDA_MILLISECONDI 1000

// Copiato da Adafruit_Fingerprint.h per evitare di includere la libreria Adafruit_Fingerprint.h in questo file.

#define FINGERPRINT_OK 0x00                //!< Command execution is complete
#define FINGERPRINT_PACKETRECIEVEERR 0x01  //!< Error when receiving data package
#define FINGERPRINT_NOFINGER 0x02          //!< No finger on the sensor
#define FINGERPRINT_IMAGEFAIL 0x03         //!< Failed to enroll the finger
#define FINGERPRINT_IMAGEMESS \
    0x06  //!< Failed to generate character file due to overly disorderly
          //!< fingerprint image
#define FINGERPRINT_FEATUREFAIL \
    0x07                                     //!< Failed to generate character file due to the lack of character point
                                             //!< or small fingerprint image
#define FINGERPRINT_NOMATCH 0x08             //!< Finger doesn't match
#define FINGERPRINT_NOTFOUND 0x09            //!< Failed to find matching finger
#define FINGERPRINT_ENROLLMISMATCH 0x0A      //!< Failed to combine the character files
#define FINGERPRINT_BADLOCATION 0x0B         //!< Addressed PageID is beyond the finger library
#define FINGERPRINT_DBRANGEFAIL 0x0C         //!< Error when reading template from library or invalid template
#define FINGERPRINT_UPLOADFEATUREFAIL 0x0D   //!< Error when uploading template
#define FINGERPRINT_PACKETRESPONSEFAIL 0x0E  //!< Module failed to receive the following data packages
#define FINGERPRINT_UPLOADFAIL 0x0F          //!< Error when uploading image
#define FINGERPRINT_DELETEFAIL 0x10          //!< Failed to delete the template
#define FINGERPRINT_DBCLEARFAIL 0x11         //!< Failed to clear finger library
#define FINGERPRINT_PASSFAIL 0x13            //!< Find whether the fingerprint passed or failed
#define FINGERPRINT_INVALIDIMAGE 0x15        //!< Failed to generate image because of lac of valid primary image
#define FINGERPRINT_FLASHERR 0x18            //!< Error when writing flash
#define FINGERPRINT_INVALIDREG 0x1A          //!< Invalid register number
#define FINGERPRINT_ADDRCODE 0x20            //!< Address code
#define FINGERPRINT_PASSVERIFY 0x21          //!< Verify the fingerprint passed
#define FINGERPRINT_STARTCODE 0xEF01         //!< Fixed falue of EF01H; High byte transferred first

#define FINGERPRINT_COMMANDPACKET 0x1  //!< Command packet
#define FINGERPRINT_DATAPACKET 0x2     //!< Data packet, must follow command packet or acknowledge packet
#define FINGERPRINT_ACKPACKET 0x7      //!< Acknowledge packet
#define FINGERPRINT_ENDDATAPACKET 0x8  //!< End of data packet

#define FINGERPRINT_TIMEOUT 0xFF    //!< Timeout was reached
#define FINGERPRINT_BADPACKET 0xFE  //!< Bad packet was sent

#define FINGERPRINT_GETIMAGE 0x01        //!< Collect finger image
#define FINGERPRINT_IMAGE2TZ 0x02        //!< Generate character file from image
#define FINGERPRINT_SEARCH 0x04          //!< Search for fingerprint in slot
#define FINGERPRINT_REGMODEL 0x05        //!< Combine character files and generate template
#define FINGERPRINT_STORE 0x06           //!< Store template
#define FINGERPRINT_LOAD 0x07            //!< Read/load template
#define FINGERPRINT_UPLOAD 0x08          //!< Upload template
#define FINGERPRINT_DELETE 0x0C          //!< Delete templates
#define FINGERPRINT_EMPTY 0x0D           //!< Empty library
#define FINGERPRINT_READSYSPARAM 0x0F    //!< Read system parameters
#define FINGERPRINT_SETPASSWORD 0x12     //!< Sets passwords
#define FINGERPRINT_VERIFYPASSWORD 0x13  //!< Verifies the password
#define FINGERPRINT_HISPEEDSEARCH \
    0x1B                                //!< Asks the sensor to search for a matching fingerprint template to the
                                        //!< last model generated
#define FINGERPRINT_TEMPLATECOUNT 0x1D  //!< Read finger template numbers
#define FINGERPRINT_AURALEDCONFIG 0x35  //!< Aura LED control
#define FINGERPRINT_LEDON 0x50          //!< Turn on the onboard LED
#define FINGERPRINT_LEDOFF 0x51         //!< Turn off the onboard LED

#define FINGERPRINT_LED_BREATHING 0x01    //!< Breathing light
#define FINGERPRINT_LED_FLASHING 0x02     //!< Flashing light
#define FINGERPRINT_LED_ON 0x03           //!< Always on
#define FINGERPRINT_LED_OFF 0x04          //!< Always off
#define FINGERPRINT_LED_GRADUAL_ON 0x05   //!< Gradually on
#define FINGERPRINT_LED_GRADUAL_OFF 0x06  //!< Gradually off
#define FINGERPRINT_LED_RED 0x01          //!< Red LED
#define FINGERPRINT_LED_BLUE 0x02         //!< Blue LED
#define FINGERPRINT_LED_PURPLE 0x03       //!< Purple LEDpassword

#define FINGERPRINT_REG_ADDR_ERROR 0x1A  //!< shows register address error
#define FINGERPRINT_WRITE_REG 0x0E       //!< Write system register instruction

#define FINGERPRINT_BAUD_REG_ADDR 0x4    //!< BAUDRATE register address
#define FINGERPRINT_BAUDRATE_9600 0x1    //!< UART baud 9600
#define FINGERPRINT_BAUDRATE_19200 0x2   //!< UART baud 19200
#define FINGERPRINT_BAUDRATE_28800 0x3   //!< UART baud 28800
#define FINGERPRINT_BAUDRATE_38400 0x4   //!< UART baud 38400
#define FINGERPRINT_BAUDRATE_48000 0x5   //!< UART baud 48000
#define FINGERPRINT_BAUDRATE_57600 0x6   //!< UART baud 57600
#define FINGERPRINT_BAUDRATE_67200 0x7   //!< UART baud 67200
#define FINGERPRINT_BAUDRATE_76800 0x8   //!< UART baud 76800
#define FINGERPRINT_BAUDRATE_86400 0x9   //!< UART baud 86400
#define FINGERPRINT_BAUDRATE_96000 0xA   //!< UART baud 96000
#define FINGERPRINT_BAUDRATE_105600 0xB  //!< UART baud 105600
#define FINGERPRINT_BAUDRATE_115200 0xC  //!< UART baud 115200

#define FINGERPRINT_SECURITY_REG_ADDR 0x5  //!< Security level register address
// The safety level is 1 The highest rate of false recognition , The rejection
// rate is the lowest . The safety level is 5 The lowest tate of false
// recognition, The rejection rate is the highest .
#define FINGERPRINT_SECURITY_LEVEL_1 0X1  //!< Security level 1
#define FINGERPRINT_SECURITY_LEVEL_2 0X2  //!< Security level 2
#define FINGERPRINT_SECURITY_LEVEL_3 0X3  //!< Security level 3
#define FINGERPRINT_SECURITY_LEVEL_4 0X4  //!< Security level 4
#define FINGERPRINT_SECURITY_LEVEL_5 0X5  //!< Security level 5

#define FINGERPRINT_PACKET_REG_ADDR 0x6  //!< Packet size register address
#define FINGERPRINT_PACKET_SIZE_32 0X0   //!< Packet size is 32 Byte
#define FINGERPRINT_PACKET_SIZE_64 0X1   //!< Packet size is 64 Byte
#define FINGERPRINT_PACKET_SIZE_128 0X2  //!< Packet size is 128 Byte
#define FINGERPRINT_PACKET_SIZE_256 0X3  //!< Packet size is 256 Byte

// *** CLASSE *** //

class SensoreImpronteAbstract {
    // *** VARIABILI *** //

   protected:
    SensoreImpronteComponenteAbstract& sensoreImpronteComponente;
    ProgrammaAbstract& programma;
    DelayAbstract& delay;
    BuzzerAbstractReale& buzzer;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    SensoreImpronteAbstract(SensoreImpronteComponenteAbstract& sensoreImpronteComponenteAbstract, DelayAbstract& delay,
                            ProgrammaAbstract& programma, BuzzerAbstractReale& buzzer, LoggerAbstract& logger)
        : sensoreImpronteComponente(sensoreImpronteComponenteAbstract),
          programma{programma},
          delay{delay},
          buzzer(buzzer),
          logger{logger} {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void controllaDatabaseImpronte();
    uint8_t getFingerprintId();
    uint8_t getFingerprintEnroll(uint8_t idNuovaImpronta);
    void resetDatabase();

    // ** GETTER ** //

    SensoreImpronteComponenteAbstract* getSensoreImpronteComponente();
    int getNumeroImpronteDatabase();
};

#endif