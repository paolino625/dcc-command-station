#ifndef DCC_COMMAND_STATION_ROTABILI_H
#define DCC_COMMAND_STATION_ROTABILI_H

// *** INCLUDE *** //

// *** DEFINE *** //

#define NUMERO_CARATTERI_MODELLO 10
#define NUMERO_CARATTERI_NOME_ROTABILE 10
#define NUMERO_CARATTERI_CODICE 10

// *** ENUMERAZIONI *** //

enum TipologiaRotabile { MERCI, PASSEGGERI };

enum CategoriaRotabilePasseggeri { INTERCITY, FRECCIA, REGIONALE, ALTRO };

enum LivreaRotabile { XMPR, FRECCIA_BIANCA, GRIGIO, GRIGIO_ROSSO, AGIP, INTERCITY_NOTTE };

enum ProduttoreRotabile { RIVAROSSI, VITRAINS, JOUEF };

// *** STRUCT *** //

typedef struct InfoRotabile {
    char nome[NUMERO_CARATTERI_NOME_ROTABILE];
    char codice[NUMERO_CARATTERI_CODICE];
    char numeroModello[NUMERO_CARATTERI_MODELLO];
    TipologiaRotabile tipologia;
    CategoriaRotabilePasseggeri categoria;
    LivreaRotabile livrea;
    ProduttoreRotabile produttore;
} InfoRotabile;

// *** DICHIARAZIONE FUNZIONI *** //

TipologiaRotabile getEnumerazioneTipologiaRotabileDaStringa(char *stringa);
void setStringaEnumerazioneTipologiaRotabile(char *stringaSalvataggio, TipologiaRotabile valore);

CategoriaRotabilePasseggeri getEnumerazioneCategoriaRotabileDaStringa(char *stringa);
void setStringaEnumerazioneCategoriaRotabile(char *stringaSalvataggio, CategoriaRotabilePasseggeri valore);

LivreaRotabile getEnumerazioneLivreaRotabileDaStringa(char *stringa);
void setStringaEnumerazioneLivreaRotabile(char *stringaSalvataggio, LivreaRotabile valore);

ProduttoreRotabile getEnumerazioneProduttoreRotabileDaStringa(char *stringa);
void setStringaEnumerazioneProduttoreRotabile(char *stringaSalvataggio, ProduttoreRotabile valore);

#endif