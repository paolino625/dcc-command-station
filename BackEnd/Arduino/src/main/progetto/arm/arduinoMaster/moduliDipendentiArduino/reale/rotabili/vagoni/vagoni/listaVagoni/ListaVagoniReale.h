#ifndef DCC_COMMAND_STATION_LISTAVAGONIREALE_H
#define DCC_COMMAND_STATION_LISTAVAGONIREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/vagoni/listaVagoni/ListaVagoniAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ListaVagoniAbstract listaVagoniReale;

#endif
