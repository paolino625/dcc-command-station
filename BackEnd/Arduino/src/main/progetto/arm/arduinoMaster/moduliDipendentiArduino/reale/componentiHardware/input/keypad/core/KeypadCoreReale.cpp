// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/core/KeypadCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/core/componente/KeypadComponenteReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINIZIONE VARIABILI *** //

KeypadCoreAbstract keypadCoreReale = KeypadCoreAbstract(keypadComponenteReale, tempoReale, loggerReale);