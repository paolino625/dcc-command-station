#ifndef DCC_COMMAND_STATION_STAZIONI_H
#define DCC_COMMAND_STATION_STAZIONI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezioni/SezioniAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/stazioni/stazione/StazioneAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// Devo eseguire le macro FRIEND_TEST solo se il codice viene compilato per i test e non per Arduino. Altrimenti la
// build si spaccherebbe considerando che Google Test non esiste per Arduino.
#if !ARDUINO
#include <gtest/gtest_prod.h>
#endif

// *** DEFINE *** //

#define NUMERO_STAZIONI_MASSIMO 15

// *** CLASSE *** //

class StazioniAbstract {
#if !ARDUINO
    // Dichiarando la classe AutopilotConvoglioTest come friend, le funzioni al suo interno possono accedere a membri
    // privati di questa classe
    friend class AutopilotConvoglioTest;

    // I test marcati con FRIEND_TEST possono accedere a membri privati di questa classe
    FRIEND_TEST(AutopilotConvoglioTest, pippo);
#endif
    // *** VARIABILI *** //

   public:
    StazioneAbstract* stazioni[NUMERO_STAZIONI_MASSIMO + 1];
    ProgrammaAbstract& programma;
    SezioniAbstract& sezioni;
    LoggerAbstract& logger;

    int numeroStazioni = 0;
    // Parte da 1
    IdStazioneTipo idStazioniPresenti[NUMERO_STAZIONI_MASSIMO];

    // *** COSTRUTTORE *** //

   public:
    StazioniAbstract(ProgrammaAbstract& programma, SezioniAbstract& sezioni, LoggerAbstract& logger)
        : programma(programma), sezioni(sezioni), logger(logger) {}

    // *** DEFINIZIONE METODI *** //

   private:
    StazioneAbstract* getStazioneFromArray(int indiceStazione);

   public:
    void inizializza();

    StazioneAbstract* getStazione(IdStazioneTipo idStazione);
    StazioneAbstract* getStazioneFromSezione(IdSezioneTipo idSezione);
    const IdStazioneTipo* getIdStazioniPresenti() const {
        return idStazioniPresenti;
    }
    const int getNumeroStazioni() const {
        return numeroStazioni;
    }

    // Pubblico perché serve per i test
   private:
    void creaStazioneEAggiunge(IdStazioneTipo idStazione, NomeStazioneUnivoco nomeStazione, bool stazioneDiTesta, bool stessaProspettivaStazioneCentrale);
};

#endif