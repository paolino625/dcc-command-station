#ifndef DCC_COMMAND_STATION_PULSANTEEMERGENZAREALE_H
#define DCC_COMMAND_STATION_PULSANTEEMERGENZAREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/pulsanteEmergenza/PulsanteEmergenzaAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern DigitalPinReale pinPulsanteEmergenza;
extern PulsanteEmergenzaAbstract pulsanteEmergenzaReale;

#endif
