// *** INCLUDE *** //

#include "RiproduzioneAudioCore.h"

#include "AdvancedDAC.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/usb/usb/Usb.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/riproduzioneAudio/test/TestAudio.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

void RiproduzioneAudioCore::inizializzaDac(unsigned long frequenza) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo DAC... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    if (!dac0.begin(AN_RESOLUTION_12, frequenza, 256, 16)) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Errore durante l'inizializzazione del DAC",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo DAC... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void RiproduzioneAudioCore::riproduceFileAudioInCoda() {
    if (DEBUGGING_AUDIO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Riproduco file audio in coda...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    if (riproduzioneInCorso) {
        if (dac0.available() && !feof(fileAudio)) {
            numeroCicli += 1;

            // Leggo i dati dal file
            uint16_t sample_data[256] = {0};
            fread(sample_data, sampleSize, 256, fileAudio);

            // Ottengo un buffer libero per la scrittura
            SampleBuffer buf = dac0.dequeue();

            // Scrivo i dati sul buffer
            for (size_t i = 0; i < buf.size(); i++) {
                /* Scale down to 12 bit. */
                uint16_t const dac_val = ((static_cast<unsigned int>(sample_data[i]) + 32768) >> 4) & 0x0fff;
                buf[i] = dac_val;
            }

            /* Scrivo il buffer su DAC. */
            dac0.write(buf);

            if (feof(fileAudio)) {
                if (DEBUGGING_AUDIO) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Ho finito lettura file audio.",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Numero cicli lettura effettuati: ",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(numeroCicli).c_str(),
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }
                numeroCicli = 0;

                fclose(fileAudio);
                eliminaFileCoda();
                bottonePremuto = false;
                riproduzioneInCorso = false;
            }
        }
    }
    if (DEBUGGING_AUDIO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Riproduco file audio in coda... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente.
char* RiproduzioneAudioCore::toString() {
    std::string stringa;

    stringa += "CODA FILE AUDIO\n\n";
    for (int i = 0; i < NUMERO_FILE_AUDIO_CODA; i++) {
        stringa += "Elemento n° ";
        stringa += std::to_string(i);
        stringa += ": ";
        stringa += codaFileAudio[i];
        stringa += "\n";
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

void RiproduzioneAudioCore::eliminaFileCoda() {
    if (DEBUGGING_AUDIO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Elimino file audio coda...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    }
    strcpy(codaFileAudio[indiceElementoCodaFileAudioInRiproduzione], " ");
    if (DEBUGGING_AUDIO) {
        char* riproduzioneAudioCoreStringa = toString();
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "Coda file audio aggiornata: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_DINAMICO(logger, LivelloLog::DEBUG_, riproduzioneAudioCoreStringa,
                               InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true, true);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Elimino file audio coda... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

bool RiproduzioneAudioCore::isCodaVuota() {
    if (indiceElementoCodaFileAudioInRiproduzione == indiceElementoCodaFileAudioInSalvataggio) {
        return true;
    } else {
        return false;
    }
}

void RiproduzioneAudioCore::aggiungeFileCoda(char pathFile[NUMERO_CARATTERI_PATH_FILE]) {
    if (DEBUGGING_AUDIO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Aggiungo file audio coda...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    indiceElementoCodaFileAudioInSalvataggio += 1;
    if (indiceElementoCodaFileAudioInSalvataggio == NUMERO_FILE_AUDIO_CODA) {
        indiceElementoCodaFileAudioInSalvataggio = 0;
    }

    if (indiceElementoCodaFileAudioInSalvataggio == indiceElementoCodaFileAudioInRiproduzione) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Buffer file audio pieno!",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        programma.ferma(true);
    }
    strcpy(codaFileAudio[indiceElementoCodaFileAudioInSalvataggio], pathFile);

    if (DEBUGGING_AUDIO) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Aggiungo file audio coda... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

void RiproduzioneAudioCore::controllaCoda() {
    // Se non c'è nulla in riproduzione e la coda non è vuota (devo leggere una altro file), metto
    // in lettura il file audio successivo
    if (!riproduzioneInCorso) {
        if (!isCodaVuota()) {
            // Se la coda non è vuota, allora riproduco il file audio successivo
            indiceElementoCodaFileAudioInRiproduzione += 1;

            if (indiceElementoCodaFileAudioInRiproduzione == NUMERO_FILE_AUDIO_CODA) {
                indiceElementoCodaFileAudioInRiproduzione = 0;
            }
            usb.apreFileAudio(codaFileAudio[indiceElementoCodaFileAudioInRiproduzione]);
            riproduzioneInCorso = true;
        }
    }
}

// ATTENZIONE! Inizializzo dac0 perché se inizializzo dac1(A13) il display smette di funzionare e
// non ne ho capito il perché.
AdvancedDAC dac0(A12);

RiproduzioneAudioCore riproduzioneAudioCoreReale = RiproduzioneAudioCore(dac0, programmaReale, loggerReale);