// *** INCLUDE *** //

#include "OrarioPlasticoReale.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINE *** //

#define LUNGHEZZA_GIORNATA_PLASTICO_IN_MINUTI 20

// *** DICHIARAZIONE VARIABILI *** //

OrarioPlasticoAbstract orarioPlasticoReale(LUNGHEZZA_GIORNATA_PLASTICO_IN_MINUTI, tempoReale, loggerReale);