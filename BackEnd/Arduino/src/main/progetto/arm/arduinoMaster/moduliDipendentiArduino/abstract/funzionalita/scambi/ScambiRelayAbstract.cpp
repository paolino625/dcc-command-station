// *** INCLUDE *** //

#include "ScambiRelayAbstract.h"

#include <cstring>
#include <string>

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"

/*
Aggiungo queste dipendenze solo se la compilazione avviene per Arduino, nel caso in cui avvenga in ambiente nativo
(durante i test) queste librerie non vengono importate. Chiamo la funzione
arduinoSlaveRelay.codaRelayDaInviare.aggiungeRelay() solo se il codice viene compilato per Arduino e non per i test.
In questa maniera possiamo usare questa classe durante i test ma evitando di creare classi abstract per tutte le
classi riguardanti la comunicazione seriale.
*/
#if defined(ARDUINO)
#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/arduinoSlaveRelay/ArduinoSlaveRelay.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"

#endif

// *** DEFINIZIONE FUNZIONI *** //

void ScambiRelayAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo scambi... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_SCAMBI; i++) {
        ScambioRelayAbstract* scambio = new ScambioRelayAbstract(i);

        scambi[i] = scambio;
    }
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo scambi... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void ScambiRelayAbstract::reset() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset scambi... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_SCAMBI; i++) {
        scambi[i]->reset();
    }
    fileScambiDaAggiornare = true;
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Reset scambi... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

/*
Aziono uno scambio.
Dato un numero di uno scambio n, passo alla funzione +n nel caso in cui voglia azionarlo a destra,
-n a sinistra. Aggiorno la posizione dello scambio memorizzata in Arduino in questa funzione.
*/

void ScambiRelayAbstract::azionoScambio(int numeroScambioCodificato) {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aziono scambio ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(numeroScambioCodificato).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "...", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    bool sinistra;
    int numeroScambio;

    // Calcolo il valore assoluto senza usare la funzione abs (disponibile solo con librerie Arduino)
    if (numeroScambioCodificato < 0) {
        numeroScambio = -numeroScambioCodificato;
    } else {
        numeroScambio = numeroScambioCodificato;
    }

    if (numeroScambioCodificato < -NUMERO_SCAMBI || numeroScambioCodificato > NUMERO_SCAMBI) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::BUG, "Numero scambio codificato non valido.",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    if (numeroScambioCodificato < 0) {
        sinistra = true;
        getScambio(numeroScambio)->azionaSinistra();
        // scambi[numeroScambio] = -1;  // Aggiorno posizione dello scambio
    }

    else {
        sinistra = false;
        getScambio(numeroScambio)->azionaDestra();  // Aggiorno posizione dello scambio
    }

    /*
    Scambi sinistra
    1 = 1
    2 = 3
    3 = 5

    Scambi destra
    1 = 2
    2 = 4
    3 = 6
    */

#if defined(ARDUINO)
    if (sinistra) {
        arduinoSlaveRelay.codaRelayDaInviare.aggiungeRelay(((numeroScambio - 1) * 2) + 1);
    } else {
        arduinoSlaveRelay.codaRelayDaInviare.aggiungeRelay(((numeroScambio - 1) * 2) + 2);
    }
#endif

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Aziono scambio ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(numeroScambioCodificato).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... OK", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
}

// Aziono lo scambio solo se non è già posizionato nella posizione corretta.
void ScambiRelayAbstract::azionoScambioSeNecessario(int numeroScambioCodificato) {
    bool scambioDaAzionare;
    int numeroScambio;

    // Calcolo il valore assoluto senza usare la funzione abs (disponibile solo con librerie Arduino)
    if (numeroScambioCodificato < 0) {
        numeroScambio = -numeroScambioCodificato;
    } else {
        numeroScambio = numeroScambioCodificato;
    }

    if (numeroScambioCodificato < -NUMERO_SCAMBI || numeroScambioCodificato > NUMERO_SCAMBI) {
        return;
    }

    if (numeroScambioCodificato < 0) {
        // Lo scambio è a sinistra

        if (getScambio(numeroScambio)->isSinistra()) {  // Se lo scambio è già a sinistra non cambio la posizione
            scambioDaAzionare = false;
        } else {
            scambioDaAzionare = true;  // Se lo scambio è a destra, la posizione è da cambiare
        }
    }

    else {
        // Lo scambio è a destra

        if (!getScambio(numeroScambio)->isSinistra()) {
            scambioDaAzionare = false;
        } else {
            scambioDaAzionare = true;
        }
    }

    if (scambioDaAzionare) {
        azionoScambio(numeroScambioCodificato);
    }
}

bool ScambiRelayAbstract::esisteNumeroScambio(byte numeroScambio) {
    if (numeroScambio >= 1 && numeroScambio < NUMERO_SCAMBI - 1) {
        return true;
    } else {
        return false;
    }
}

// N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente.
char* ScambiRelayAbstract::toString() {
    std::string stringa;

    stringa += "\n";
    stringa += "\nSTATO SCAMBI \n\n";
    for (int i = 1; i < NUMERO_SCAMBI; i++) {
        stringa += "Scambio n° ";
        stringa += std::to_string(i);
        stringa += ": ";
        stringa += scambi[i]->getStatoStringa();
        stringa += "\n";
    }

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

ScambioRelayAbstract* ScambiRelayAbstract::getScambio(int id) { return scambi[id]; }