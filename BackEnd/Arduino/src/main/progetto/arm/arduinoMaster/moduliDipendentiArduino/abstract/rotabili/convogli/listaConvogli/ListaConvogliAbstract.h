#ifndef DCC_COMMAND_STATION_LISTACONVOGLIABSTRACT_H
#define DCC_COMMAND_STATION_LISTACONVOGLIABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_PER_CONVOGLIO_LISTA 40

// *** CLASSE *** //

class ListaConvogliAbstract {
   public:
    // *** VARIABILI *** //
    char* listaConvogli[NUMERO_CONVOGLI];
    ConvogliAbstract& convogli;
    ProgrammaAbstract& programma;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

    ListaConvogliAbstract(ConvogliAbstract& convogli, ProgrammaAbstract& programma, LoggerAbstract& logger)
        : convogli(convogli), programma(programma), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    int crea(bool mostraConvogliTracciato, bool mostraConvoglioFuoriTracciato, bool mostraStatoConvoglio);
    char** get() { return listaConvogli; }
};

#endif
