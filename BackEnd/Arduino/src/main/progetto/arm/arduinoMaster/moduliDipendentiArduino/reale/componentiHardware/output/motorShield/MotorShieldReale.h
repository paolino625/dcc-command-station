#ifndef DCC_COMMAND_STATION_MOTORSHIELDREALE_H
#define DCC_COMMAND_STATION_MOTORSHIELDREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/motorShield/MotorShieldAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/DigitalPinReale.h"

// *** DEFINIZIONE VARIABILI *** //

extern DigitalPinReale pinMotorShieldPwm;
extern DigitalPinReale pinMotorShieldDir1;

extern MotorShieldAbstract motorShieldReale;

#endif
