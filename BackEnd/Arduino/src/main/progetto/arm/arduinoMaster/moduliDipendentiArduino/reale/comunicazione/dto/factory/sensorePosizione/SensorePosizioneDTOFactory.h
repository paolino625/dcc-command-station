#ifndef ARDUINO_SENSOREPOSIZIONEDTOFACTORY_H
#define ARDUINO_SENSOREPOSIZIONEDTOFACTORY_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/dto/dto/sensorePosizione/SensorePosizioneDTO.h"

// *** CLASSE *** //

class SensorePosizioneDTOFactory {
   public:
    SensorePosizioneDTO* creaSensorePosizioneDTO(IdSensorePosizioneTipo idSensore);
    static char* converteSensorePosizioneDTOToStringJson(SensorePosizioneDTO* sensorePosizioneDTO);
};

// *** DICHIARAZIONE VARIABILI *** //

extern SensorePosizioneDTOFactory sensorePosizioneDTOFactory;

#endif
