// *** INCLUDE *** //

#include "SezioneDTOFactory.h"

#include "ArduinoJson.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/sezioni/SezioniReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
SezioneResponseDTO* SezioneDTOFactory::creaSezioneResponseDTO(int indiceSezione) {
    if (DEBUGGING_FACTORY) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Creo sezione response DTO con indice: ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, std::to_string(indiceSezione).c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    SezioneAbstract* sezione = sezioniReali.getSezioneFromIndex(indiceSezione);

    SezioneResponseDTO* sezioneResponseDTO = new SezioneResponseDTO();
    sezioneResponseDTO->id = sezione->getId();
    sezioneResponseDTO->stato = sezione->getStatoSezione();
    return sezioneResponseDTO;
}

// N.B. Ricordarsi di deallocare la memoria del puntatore ritornato
char* SezioneDTOFactory::converteSezioneResponseDTOToStringJson(SezioneResponseDTO* sezioneResponseDTO) {
    JsonDocument bufferJson;
    JsonObject idSezione = bufferJson["id"].to<JsonObject>();

    idSezione["tipologia"] = sezioneResponseDTO->id.tipologia;
    idSezione["id"] = sezioneResponseDTO->id.id;
    idSezione["specifica"] = sezioneResponseDTO->id.specifica;

    JsonObject stato = bufferJson["stato"].to<JsonObject>();

    stato["statoLock"] = sezioneResponseDTO->stato.statoLock;
    stato["idConvoglio"] = sezioneResponseDTO->stato.idConvoglio;

    serializzoJson(bufferJson, stringaJsonUsbBuffer, NUMERO_CARATTERI_SEZIONE_STRINGA_JSON, true);
    return stringaJsonUsbBuffer;
}

// *** DEFINIZIONE VARIABILI *** //

SezioneDTOFactory sezioneDTOFactory;