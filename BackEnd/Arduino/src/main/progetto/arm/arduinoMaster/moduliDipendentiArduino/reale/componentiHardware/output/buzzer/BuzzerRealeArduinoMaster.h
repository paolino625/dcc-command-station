#ifndef DCC_COMMAND_STATION_BUZZERREALE_ARDUINO_MASTER_H
#define DCC_COMMAND_STATION_BUZZERREALE_ARDUINO_MASTER_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"

// *** DICHIARAZIONE VARIABILI *** //

extern BuzzerAbstractReale buzzerRealeArduinoMaster;

#endif
