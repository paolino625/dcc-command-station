#ifndef DCC_COMMAND_STATION_MODIFICACVREALE_H
#define DCC_COMMAND_STATION_MODIFICACVREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/modificaCv/ModificaCvAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ModificaCvAbstract modificaCvReale;

#endif
