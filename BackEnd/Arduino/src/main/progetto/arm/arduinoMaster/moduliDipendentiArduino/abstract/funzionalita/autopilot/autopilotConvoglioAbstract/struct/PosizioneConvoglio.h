#ifndef ARDUINO_POSIZIONECONVOGLIO_H
#define ARDUINO_POSIZIONECONVOGLIO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/enumerazioni/validitaPosizioneConvoglio/ValiditaPosizioneConvoglio.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"

// *** STRUCT *** //

typedef struct PosizioneConvoglio {
    ValiditaPosizioneConvoglio validita = POSIZIONE_NON_VALIDA;
    IdSezioneTipo idSezioneCorrenteOccupata;
    bool direzionePrimaLocomotivaDestra =
        false;  // Come prima locomotiva si intende la prima che appare sul display. Ovvero quella
                // che corrisponde a indirizzoLocomotiva1 del convoglio. Destra e sinistra si
                // riferiscono al convoglio messo in un binario della stazione centrale.
} PosizioneConvoglio;

#endif
