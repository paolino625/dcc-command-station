// *** INCLUDE *** //

#include "ModalitaLetturaSensore.h"

// *** DEFINIZIONE VARIABILI *** //

// Di default la lettura del sensore è per ogni convoglio.
// Quando deve essere cambiata in LETTURA_LOCOMOTIVA (dalla funzione di mapping locomotiva), deve essere cambiata.
ModalitaLetturaSensore modalitaLetturaSensore = ModalitaLetturaSensore::LETTURA_CONVOGLIO;