// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE METODI *** //

void stampoMacAddress(byte mac[]) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                          false);

    for (int i = 5; i >= 0; i--) {
        if (mac[i] < 16) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "0",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        }
        char hexStr[3];  // Due caratteri per il numero esadecimale + terminatore null
        sprintf(hexStr, "%02X", mac[i]);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, hexStr,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        if (i > 0) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, ":",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        }
    }
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

String convertoIndirizzoIPaStringa(const IPAddress &ipAddress) {
    return String(ipAddress[0]) + String(".") + String(ipAddress[1]) + String(".") + String(ipAddress[2]) +
           String(".") + String(ipAddress[3]);
}