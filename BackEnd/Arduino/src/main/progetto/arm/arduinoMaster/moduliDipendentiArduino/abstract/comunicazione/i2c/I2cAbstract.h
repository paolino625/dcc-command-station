#ifndef DCC_COMMAND_STATION_I2CABSTRACT_H
#define DCC_COMMAND_STATION_I2CABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/DisplayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/DisplayCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/programma/ProgrammaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/utility/wire/WireAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/pin/PinReali.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/BuzzerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define NUMERO_TENTATIVI_CHECK_CONNESSIONE_I2C 10
#define INTERVALLO_TRA_TENTATIVI_MILLISECONDI 100

// *** CLASSE *** //

class I2cAbstract {
    WireAbstract& wire;
    DisplayAbstract& display;
    DisplayCoreAbstract& displayCore;
    BuzzerAbstract& buzzer;
    ProgrammaAbstract& programma;
    DelayAbstract& delay;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    I2cAbstract(WireAbstract& wire, DisplayAbstract& display, DisplayCoreAbstract& displayCore, BuzzerAbstract& buzzer,
                ProgrammaAbstract& programma, DelayAbstract& delay, LoggerAbstract& logger)
        : wire(wire),
          display(display),
          displayCore(displayCore),
          buzzer(buzzer),
          programma(programma),
          delay(delay),
          logger(logger) {}

    // *** METODI PUBBLICI *** //

    void controllaPresenzaDispositiviI2c() {
        if (DISPLAY_PRESENTE) {
            if (!isIndirizzoBusI2cAttivo(displayCore.getDisplayComponente().getIndirizzoI2C())) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Display disconnesso.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                display.errori.componenteHardwareDisconnesso.display();
                programma.ferma(false);
            }
        }

        if (SCHEDA_ALIMENTAZIONE_PRESENTE) {
            if (!isIndirizzoBusI2cAttivo(INDIRIZZO_I2C_INA_219)) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "INA219 disconnesso.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                display.errori.componenteHardwareDisconnesso.ina219();
                programma.ferma(false);
            }
        }

        if (SCHEDA_ALIMENTAZIONE_PRESENTE) {
            if (!isIndirizzoBusI2cAttivo(INDIRIZZO_I2C_INA_260)) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "INA260 disconnesso.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                display.errori.componenteHardwareDisconnesso.ina260();
                programma.ferma(false);
            }
        }

        if (SCHEDA_AMPLIFICATORE_AUDIO) {
            if (!isIndirizzoBusI2cAttivo(INDIRIZZO_I2C_AMPLIFICATORE_AUDIO_MAX_9744)) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::CRITICAL, "Amplificatore audio disconnesso.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                display.errori.componenteHardwareDisconnesso.amplificatoreAudio();
                programma.ferma(false);
            }
        }
    }

    void stampaDispositiviI2cTrovati() {
        byte error, address;
        int nDevices;
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scansiono dispositivi I2C...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        logger.cambiaTabulazione(1);
        nDevices = 0;
        for (address = 1; address < 127; address++) {
            // The i2c_scanner uses the return value of
            // the Write.endTransmission to see if
            // a device did acknowledge to the address.
            wire.beginTransmission(address);
            error = wire.endTransmission();
            if (error == 0) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Dispositivo I2C trovato all'indirizzo decimale ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                if (address < 16) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "0",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                }
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(address).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                nDevices++;
            } else if (error == 4) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "Errore sconosciuto all'indirizzo decimale ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                if (address < 16) {
                    LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, "0",
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                }
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::ERROR, std::to_string(address).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        }
        if (nDevices == 0)
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Nessun dispositivo I2C trovato",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        logger.cambiaTabulazione(-1);
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Scansiono dispositivi I2C... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    // *** GETTER *** //

    bool isIndirizzoBusI2cAttivo(byte indirizzoDaCercare) {
        bool primoTentativo = true;
        for (int i = 0; i < NUMERO_TENTATIVI_CHECK_CONNESSIONE_I2C; i++) {
            wire.beginTransmission(indirizzoDaCercare);
            byte busStatus = wire.endTransmission();
            if (busStatus == 0) {
                if (!primoTentativo) {
                    // Se non si tratta del primo tentativo, ho fatto suonare prima il buzzer in modalità avviso.
                    // Quindi ora faccio suonare il buzzer in modalità ok.
                    buzzer.beepOk();
                }
                // Se l'indirizzo è attivo ritorno true
                return true;
            }
            primoTentativo = false;

            buzzer.beepAvviso();

            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, "Indirizzo I2C ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, std::to_string(indirizzoDaCercare).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::WARNING, " non attivo.",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            delay.milliseconds(INTERVALLO_TRA_TENTATIVI_MILLISECONDI);
        }
        // Se l'indirizzo non è risultato attivo in nessuno dei tentativi ritorno false
        return false;
    }
};

#endif