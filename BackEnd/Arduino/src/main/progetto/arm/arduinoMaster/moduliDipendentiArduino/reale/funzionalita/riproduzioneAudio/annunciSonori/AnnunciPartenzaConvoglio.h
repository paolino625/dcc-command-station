#ifndef DCC_COMMAND_STATION_ANNUNCIPARTENZACONVOGLIO_H
#define DCC_COMMAND_STATION_ANNUNCIPARTENZACONVOGLIO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/stazioni/NomiStazioni.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** CLASSE *** //

class AnnunciSonori {
    class PartenzaConvoglio {
        void riproducoPartenzaConvoglio(IdConvoglioTipo idConvoglio, int numero, byte ore, byte minuti,
                                        NomeStazioneUnivoco stazioneArrivo, bool inRitardo, byte binario);
    };
};

#endif
