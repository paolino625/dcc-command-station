// *** INCLUDE *** //

#include <Arduino.h>  // ATTENZIONE! Non togliere. Questo import deve essere PRIMA dell'import di Interrupt.h, altrimenti la build si rompe male.

// Questo deve stare DOPO l'import di Arduino.h
#include "Interrupt.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/protocolloDcc/core/ProtocolloDccCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

// Nota: non sono riuscito a creare una classe Interrupt dove inserire queste funzioni: è come se dentro la classe non
// si riesca più a trovare la funzione di callback.

// Non posso usare interrupt per encoder o pulsante di emergenza perché operazioni molto lunghe e
// andrebbero in conflitto con l'interrupt segnale GestioneLocomotive. Inoltre non è possibile effettuare chiamate
// Serial.print o display.print dentro una callback Interrupt, quindi non riuscirei comunque a
// ottenere ciò che voglio.

void inizializzoInterruptTimer() {
    /*
    Arduino utilizza TIMER0 per la temporizzazione. Non dovresti cambiarlo. Puoi fare quello che
    vuoi con TIMER1 e TIMER2. Alcune funzioni o librerie utilizzano questi timer (pwm, tone(), e
    molte altre), quindi devi fare attenzione a come vengono utilizzati.
    https://forum.arduino.cc/t/timer-interrupts-and-pwm-pins/316380/5
    */

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo timer interrupt... \n",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    if (!ITimer1.attachInterruptInterval(TEMPO_IMPULSO_BIT_1, callbackTimerInterrupt1)) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL,
                              "Errore timer. Can't set ITimer1. Select another freq. or timer",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        programmaReale.ferma(true);
    }
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo timer interrupt... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void callbackTimerInterrupt1() { protocolloDccCoreReale.segnaleDcc(); }