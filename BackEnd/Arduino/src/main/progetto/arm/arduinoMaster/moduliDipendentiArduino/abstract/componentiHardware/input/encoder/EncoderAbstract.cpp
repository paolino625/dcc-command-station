// *** INCLUDE *** //

#include "EncoderAbstract.h"

#include <string>

// *** DEFINIZIONE METODI *** //

void EncoderAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo encoder n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    clk.setInputMode();

    dt.setInputMode();

    sw.setInputPullupMode();

    lastStateClk = clk.isHigh();

    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo encoder n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, std::to_string(id).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "... OK", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
}

// Restituisce true se la situazione è cambiata
bool EncoderAbstract::leggeStato(ConvoglioAbstract* convoglio) {
    bool statoCambiato = false;
    // Se il convoglio non è in uso non faccio nulla
    if (convoglio->isInUso()) {
        VelocitaRotabileKmHTipo velocitaMassimaConvoglio = convoglio->getVelocitaMassima();

        // Leggo lo stato corrente di clk
        currentStateClk = clk.legge();

        // Se l'ultimo stato e quello corrente di clk sono differenti, allora c'è stato un impulso.
        // Reagisco soltanto ai cambiamenti dello stato 1 per evitare di contare due volte
        if (currentStateClk != lastStateClk && currentStateClk == 1) {
            if (dt.legge() != currentStateClk) {
                int nextValue = convoglio->getVelocitaImpostata() + modalitaStepEncoder;

                if (nextValue > velocitaMassimaConvoglio) {
                    convoglio->cambiaVelocita(velocitaMassimaConvoglio, true, StatoAutopilot::NON_ATTIVO);

                } else {
                    convoglio->cambiaVelocita(nextValue, true, StatoAutopilot::NON_ATTIVO);
                }
                // Se lo stato dt è differente rispetto allo stato di clk allora l'encoder sta
                // girando a sinistra allora decremento il contatore.
            } else {
                // Imposto velocità penultimo step per encoder.
                int nextValue = convoglio->getVelocitaImpostata() - modalitaStepEncoder;

                if (nextValue < 0) {
                    convoglio->cambiaVelocita(0, true, StatoAutopilot::NON_ATTIVO);

                } else {
                    convoglio->cambiaVelocita(nextValue, true, StatoAutopilot::NON_ATTIVO);
                }
            }

            statoCambiato = true;
        }

        // Ricordo l'ultimo stato clk
        lastStateClk = currentStateClk;

        // Leggo lo stato del bottone
        int btnState = sw.legge();

        if (btnState == 0) {
            if (!pulsanteEncoderPremuto) {  // Se il pulsante è appena stato premuto
                pulsanteEncoderPremuto = true;
                pressTime = tempoAbstract.milliseconds();
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Encoder n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, ": Pulsante encoder appena premuto.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        } else {
            // Se il pulsante è stato rilasciato ed era premuto da almeno 1 secondo cambio
            // direzione, altrimenti accendo/spengo le luci
            if (pulsanteEncoderPremuto && (tempoAbstract.milliseconds() - pressTime >= 500)) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Encoder n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, ": pulsante encoder premuto per almeno un secondo.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                convoglio->cambiaDirezione(StatoAutopilot::NON_ATTIVO);
                statoCambiato = true;
            } else if (pulsanteEncoderPremuto && tempoAbstract.milliseconds() - pressTime >= 50) {
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "Encoder n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(id).c_str(),
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, ": pulsante encoder premuto per meno di un secondo.",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                if (modalitaStepEncoder == STEP_ENCODER_MODE1) {
                    modalitaStepEncoder = STEP_ENCODER_MODE2;
                } else {
                    modalitaStepEncoder = STEP_ENCODER_MODE1;
                }
            }
            pulsanteEncoderPremuto = false;  // Reset dello stato del pulsante
        }

        // Piccolo ritardo per aiutare il rimbalzo della lettura
        delay.microseconds(1);
    }

    return statoCambiato;
}