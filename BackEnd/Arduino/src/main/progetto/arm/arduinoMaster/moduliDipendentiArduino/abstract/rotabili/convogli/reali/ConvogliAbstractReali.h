#ifndef ARDUINO_CONVOGLIABSTRACTREALI_H
#define ARDUINO_CONVOGLIABSTRACTREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriPosizione/SensoriPosizioneAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/reale/PercorsiAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/reale/ConvoglioAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/vagoni/VagoniAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezioni/SezioniAbstract.h"

// *** CLASSE *** //

class ConvogliAbstractReali : public ConvogliAbstract {
    // *** VARIABILI *** //

   private:
    LocomotiveAbstract& locomotive;
    VagoniAbstract& vagoni;
    PercorsiAbstractReale& percorsi;
    ProgrammaAbstract& programma;
    TempoAbstract& tempo;
    ConvoglioAbstractReale** convogli = new ConvoglioAbstractReale*[NUMERO_CONVOGLI];
    SensoriPosizioneAbstract& sensoriPosizione;
    SezioniAbstract& sezioni;
    StazioniAbstract& stazioni;
    DistanzaFrenataLocomotivaAbstract& distanzaFrenataLocomotiva;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    ConvogliAbstractReali(LocomotiveAbstract& locomotive, VagoniAbstract& vagoni, PercorsiAbstractReale& percorsi,
                          ProgrammaAbstract& programma, TempoAbstract& tempo,
                          SensoriPosizioneAbstract& sensoriPosizione, SezioniAbstract& sezioni,
                          StazioniAbstract& stazioni, DistanzaFrenataLocomotivaAbstract& distanzaFrenataLocomotiva,
                          LoggerAbstract& logger)
        : locomotive(locomotive),
          vagoni(vagoni),
          percorsi(percorsi),
          programma(programma),
          tempo(tempo),
          sensoriPosizione(sensoriPosizione),
          sezioni(sezioni),
          stazioni(stazioni),
          distanzaFrenataLocomotiva(distanzaFrenataLocomotiva),
          logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    bool esisteConvoglio(IdConvoglioTipo idConvoglio);
    void reset();
    bool isLocomotivaInUso(IdLocomotivaTipo idLocomotiva);
    bool isVagoneInUso(VagoneAbstract* vagone);
    void aggiornaLunghezza();
    void aggiornaDirezioneLocomotive();
    void aggiornaLuci();
    char* toString();
    ConvoglioAbstractReale* getConvoglio(int id, bool fermaProgramma);
    ConvoglioAbstractReale* getConvoglioDellaLocomotiva(IdLocomotivaTipo idLocomotiva);
    void bloccoSezioniOccupate();
};

#endif