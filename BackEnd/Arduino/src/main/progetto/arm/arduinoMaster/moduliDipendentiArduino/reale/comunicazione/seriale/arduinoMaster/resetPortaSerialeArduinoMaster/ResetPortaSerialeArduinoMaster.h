#ifndef DCC_COMMAND_STATION_RESETPORTASERIALEARDUINOMASTER_H
#define DCC_COMMAND_STATION_RESETPORTASERIALEARDUINOMASTER_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DICHIARAZIONE FUNZIONI *** //

void resettoPortaSerialeArduinoMaster(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                      bool portaSerialeDaVerificare, bool beepAttivato);

#endif
