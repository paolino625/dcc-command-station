#ifndef DCC_COMMAND_STATION_LISTAVAGONIABSTRACT_H
#define DCC_COMMAND_STATION_LISTAVAGONIABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_PER_VAGONE_LISTA 40

// *** CLASSE *** //

class ListaVagoniAbstract {
    // *** VARIABILI *** //

    char* listaVagoni[NUMERO_VAGONI];
    ConvogliAbstract& convogli;
    ProgrammaAbstract& programma;
    VagoniAbstract& vagoni;
    LoggerAbstract& logger;

    // *** COSTRUTTORE *** //

   public:
    ListaVagoniAbstract(ConvogliAbstract& convogli, ProgrammaAbstract& programma, VagoniAbstract& vagoni,
                        LoggerAbstract& logger)
        : convogli(convogli), programma(programma), vagoni(vagoni), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    int creo(bool mostraVagoniInUso, bool mostraVagoniNonInUso, ConvoglioAbstractReale* convoglio);
    // get per listaVagoni senza indice
    char** get() { return listaVagoni; };
};

#endif
