#ifndef DCC_COMMAND_STATION_CONVOGLIREALI_H
#define DCC_COMMAND_STATION_CONVOGLIREALI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/reali/ConvogliAbstractReali.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ConvogliAbstractReali convogliReali;

#endif
