// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/DisplayCoreAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/stampa/core/componente/DisplayComponenteReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINIZIONE VARIABILI *** //

DisplayCoreAbstract displayCoreReale(displayComponenteReale, tempoReale, loggerReale);