#ifndef DCC_COMMAND_STATION_PULSANTEEMERGENZAABSTRACT_CPP
#define DCC_COMMAND_STATION_PULSANTEEMERGENZAABSTRACT_CPP

// *** INCLUDE *** //

#include "PulsanteEmergenzaAbstract.h"

// *** DEFINIZIONE METODI *** //

void PulsanteEmergenzaAbstract::inizializza() {
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo pulsante di emergenza... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    pin.setInputPullupMode();
    LOG_MESSAGGIO_STATICO(logger, LivelloLog::INFO, "Inizializzo pulsante di emergenza... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// Dato che il pin è INPUT_PULLUP, il pulsante è premuto quando il pin è LOW
[[nodiscard]] auto PulsanteEmergenzaAbstract::isPremuto() const -> bool { return pin.isLow(); }

#endif