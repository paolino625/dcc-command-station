#ifndef DCC_COMMAND_STATION_LETTURATENSIONEABSTRACT_H
#define DCC_COMMAND_STATION_LETTURATENSIONEABSTRACT_H

// Questa classe astratta funge da interfaccia per la lettura della tensione. È utile perché non si applica solo ai
// sensori di tensione, ma anche ai sensori di corrente come INA219 e INA260, che possono fornire letture di tensione.

// *** CLASSE *** //

class LetturaTensioneAbstract {
   public:
    virtual float getTensione() = 0;
    virtual void effettuaLetturaAggiornaUnaEntryCampionamento() = 0;
    virtual void effettuaLetturaAggiornaTutteEntryCampionamento() = 0;
};

#endif
