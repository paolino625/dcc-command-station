// *** INCLUDE *** //

#include "VagoneAbstract.h"

#include <cstring>
#include <string>

// *** DEFINIZIONE METODI *** //

char* VagoneAbstract::toString() {
    std::string stringa;

    stringa += "\nINFO VAGONE\n";
    stringa += "\nID: ";
    stringa += std::to_string(getId());
    stringa += "\n";
    stringa += "Nome: ";
    stringa += getNome();
    stringa += "\n";
    stringa += "Codice: ";
    stringa += infoRotabile.codice;
    stringa += "\n";
    stringa += "Numero modello: ";
    stringa += getNumeroModello();
    stringa += "\n";
    stringa += "Tipologia: ";
    stringa += std::to_string(getTipologia());
    stringa += "\n";
    stringa += "Categoria: ";
    stringa += std::to_string(infoRotabile.categoria);
    stringa += "\n";
    stringa += "Livrea: ";
    stringa += std::to_string(getLivrea());
    stringa += "\n";
    stringa += "Produttore: ";
    stringa += std::to_string(getProduttore());
    stringa += "\n";
    stringa += "Lunghezza: ";
    stringa += std::to_string(getLunghezzaCm());
    stringa += "\n";
    stringa += "Illuminazione: ";
    stringa += isLuciAccese() ? "true" : "false";
    stringa += "\n";

    char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
    strcpy(buffer, stringa.c_str());
    return buffer;
}

IdVagoneTipo VagoneAbstract::getId() const { return id; }
char* VagoneAbstract::getNome() { return infoRotabile.nome; }
TipologiaRotabile VagoneAbstract::getTipologia() const { return infoRotabile.tipologia; }
LivreaRotabile VagoneAbstract::getLivrea() const { return infoRotabile.livrea; }
ProduttoreRotabile VagoneAbstract::getProduttore() const { return infoRotabile.produttore; }
char* VagoneAbstract::getNumeroModello() { return infoRotabile.numeroModello; }
LunghezzaRotabileTipo VagoneAbstract::getLunghezzaCm() const { return lunghezzaCm; }

bool VagoneAbstract::isLuciAccese() const { return luciAccese; }
void VagoneAbstract::setId(IdVagoneTipo idNuovo) { this->id = idNuovo; }
void VagoneAbstract::setNome(char* nomeNuovo) { strcpy(this->infoRotabile.nome, nomeNuovo); }
void VagoneAbstract::setCodice(char* codiceNuovo) { strcpy(this->infoRotabile.codice, codiceNuovo); }
void VagoneAbstract::setNumeroModello(char* numeroModelloNuovo) {
    strcpy(this->infoRotabile.numeroModello, numeroModelloNuovo);
}
void VagoneAbstract::setTipologia(TipologiaRotabile tipologiaNuovo) { this->infoRotabile.tipologia = tipologiaNuovo; }
void VagoneAbstract::setCategoria(CategoriaRotabilePasseggeri categoriaNuovo) {
    this->infoRotabile.categoria = categoriaNuovo;
}
void VagoneAbstract::setLivrea(LivreaRotabile livreaNuovo) { this->infoRotabile.livrea = livreaNuovo; }
void VagoneAbstract::setProduttore(ProduttoreRotabile produttoreNuovo) {
    this->infoRotabile.produttore = produttoreNuovo;
}
void VagoneAbstract::setLunghezzaCm(LunghezzaRotabileTipo lunghezzaCmNuovo) { this->lunghezzaCm = lunghezzaCmNuovo; }
void VagoneAbstract::setLuciAccese(bool luciAcceseNuovo) { this->luciAccese = luciAcceseNuovo; }