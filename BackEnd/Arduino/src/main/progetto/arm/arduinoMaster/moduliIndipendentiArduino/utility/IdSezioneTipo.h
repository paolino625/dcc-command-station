#ifndef DCC_COMMAND_STATION_IDSEZIONETIPO_H
#define DCC_COMMAND_STATION_IDSEZIONETIPO_H

// *** INCLUDE *** //

#include <cstring>
#include <string>

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** CLASSE *** //

class IdSezioneTipo {
    // *** VARIABILI *** //

   public:
    /*
    - 1: stazione
    - 2: sezione intermedia
    - 3: scambio
     - 4: incrocio
     */
    byte tipologia;

    // Nel caso della stazione indica l'ID della stazione
    byte id;

    /*
     Per stazioni: indica il numero del binario
     Per scambio: 1 a sinistra e 2 a destra
     */

    byte specifica = 0;

    // *** COSTRUTTORI *** //

    IdSezioneTipo() {};

    IdSezioneTipo(byte tipologiaSezione, byte id, byte specifica)
        : tipologia(tipologiaSezione), id(id), specifica(specifica) {}

    IdSezioneTipo(byte tipologiaSezione, byte id) : tipologia(tipologiaSezione), id(id) {}

    // *** METODI *** //

    // N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente con delete[]
    char* toString() {
        std::string stringa;
        stringa += "ID Sezione = ";
        stringa += "Tipologia: ";
        stringa += std::to_string(tipologia);
        stringa += " - ";
        stringa += "ID: ";
        stringa += std::to_string(id);
        stringa += " - ";
        stringa += "Specifica: ";
        stringa += std::to_string(specifica);

        char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
        strcpy(buffer, stringa.c_str());
        return buffer;
    }

    // N.B. Dopo aver usato la stringa è necessario liberare la memoria allocata dinamicamente con delete[].
    char* toStringShort() const {
        std::string stringa;
        stringa += std::to_string(tipologia) + "." + std::to_string(id) + "." + std::to_string(specifica);

        char* buffer = new char[stringa.length() + 1];  // +1 per il carattere nullo
        strcpy(buffer, stringa.c_str());
        return buffer;
    }

    bool equals(IdSezioneTipo idSezioneTipo2) const {
        if (tipologia == idSezioneTipo2.tipologia && id == idSezioneTipo2.id && specifica == idSezioneTipo2.specifica) {
            return true;
        } else {
            return false;
        }
    }

    bool equals(IdSezioneTipo* idSezioneTipo2) const {
        if (tipologia == idSezioneTipo2->tipologia && id == idSezioneTipo2->id &&
            specifica == idSezioneTipo2->specifica) {
            return true;
        } else {
            return false;
        }
    }

    bool equals(IdSezioneTipo* idSezioneTipo2, int numeroElementiArray) const {
        for (int i = 0; i < numeroElementiArray; i++) {
            if (tipologia == idSezioneTipo2[i].tipologia && id == idSezioneTipo2[i].id &&
                specifica == idSezioneTipo2[i].specifica) {
                return true;
            }
        }
        return false;
    }

    bool isBinarioStazione() const {
        if (tipologia == 1) {
            return true;
        } else {
            return false;
        }
    }

    bool isSezioneIntermedia() const {
        if (tipologia == 2) {
            return true;
        } else {
            return false;
        }
    }

    bool isScambio() const {
        if (tipologia == 3) {
            return true;
        } else {
            return false;
        }
    }

    bool isIncrocio() const {
        if (tipologia == 4) {
            return true;
        } else {
            return false;
        }
    }

    // Assumo che questa funzione venga chiamata solo per gli scambi
    bool isScambioSinistra() const {
        // Se la specifica è 1 allora è uno scambio a sinistra, altrimenti è uno scambio a destra
        if (specifica == 1) {
            return true;
        } else {
            return false;
        }
    }

    // Definisco operatore di uguaglianza necessario per i test
    bool operator==(const IdSezioneTipo& idSezione) const {
        return tipologia == idSezione.tipologia && id == idSezione.id && specifica == idSezione.specifica;
    }
};

#endif
