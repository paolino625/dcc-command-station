#ifndef SCAMBIO_RESPONSE_DTO_H
#define SCAMBIO_RESPONSE_DTO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** CLASSE *** //

class ScambioResponseDTO {
    // *** VARIABILI *** //

   public:
    IdScambioTipo id;
    const char* posizione;
};

#endif
