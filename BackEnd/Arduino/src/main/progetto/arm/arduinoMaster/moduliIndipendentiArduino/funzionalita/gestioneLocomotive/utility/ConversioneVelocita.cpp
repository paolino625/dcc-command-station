// *** INCLUDE *** //

#include "ConversioneVelocita.h"

// *** DEFINIZIONE FUNZIONI *** //

VelocitaRotabileKmHTipo convertoVelocitaModellinoAReale(VelocitaLocomotivaCmSTipo velocitaLocomotivaModellinoCmS) {
    VelocitaLocomotivaCmSTipo velocitaLocomotivaKmHModellino = velocitaLocomotivaModellinoCmS / 100 * 3.6;
    auto velocitaLocomotivaKmHReale = (VelocitaRotabileKmHTipo)(velocitaLocomotivaKmHModellino * 87);

    return velocitaLocomotivaKmHReale;
}

VelocitaLocomotivaCmSTipo convertoVelocitaRealeModellino(VelocitaRotabileKmHTipo velocitaLocomotivaRealeKmH) {
    VelocitaLocomotivaCmSTipo velocitaLocomotivaCmSReale = velocitaLocomotivaRealeKmH * 100 / 3.6;
    VelocitaLocomotivaCmSTipo velocitaLocomotivaCmSModellino = velocitaLocomotivaCmSReale / 87;

    return velocitaLocomotivaCmSModellino;
}