#ifndef DCC_COMMAND_STATION_PACCHETTOEXTENDED_H
#define DCC_COMMAND_STATION_PACCHETTOEXTENDED_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** CLASSE *** //

class PacchettoExtended {
    // *** VARIABILI *** //
   public:
    Pacchetto pacchetto;

    // *** COSTRUTTORE *** //

    PacchettoExtended() { pacchetto.numeroBitPacchetto = 51; }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PacchettoExtended pacchettoExtended;

#endif
