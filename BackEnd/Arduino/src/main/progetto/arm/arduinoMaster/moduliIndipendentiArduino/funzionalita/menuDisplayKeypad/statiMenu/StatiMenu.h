#ifndef DCC_COMMAND_STATION_STATIMENU_H
#define DCC_COMMAND_STATION_STATIMENU_H

// *** ENUMERAZIONI *** //

enum class FuoriMenuEnum { INDICE, NO_INDICE, MENU_PRINCIPALE, LOG_IN_NON_EFFETTUATO };

enum class MenuPrincipaleEnum { CONVOGLI, LOCOMOTIVE, WEB_SERVER, TRACCIATO, ACCESSORI, MONITORAGGIO };

enum class MenuConvogliEnum {
    SCELTA_IN_CORSO,
    AGGIUNGI,
    ELIMINA,
    GESTISCI,
    RESET,
};

enum class MenuLocomotiveEnum { INSERISCI_LOCO, SCELTA_IN_CORSO, TEMPO_USO, MAPPING_VELOCITA, SET_DECODER };

enum class MenuTracciatoEnum { SCELTA_IN_CORSO, CAMBIA_STATO_MOTOR_SHIELD, SCAMBI, SENSORI_IR };

enum class MenuConvogliGestisciEnum {
    SCELTA_IN_CORSO,
    GESTISCI_LOCO,
    GESTISCI_VAGONI,
    GESTISCI_PRESENZA,
    GESTISCI_DIFFERENZA_VELOCITA_LOCOMOTIVA_TRAZIONE_SPINTA
};

enum class MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum {
    INSERIMENTO_CONVOGLIO,
    SCELTA_IN_CORSO,
    RESET_A_0,
    SET_VELOCITA_POSITIVA,
    SET_VELOCITA_NEGATIVA
};

enum class MenuConvogliGestisciPresenzaEnum {
    SCELTA_IN_CORSO,
    TOGLI,
    METTI,
    LISTA,
};

enum class MenuConvogliGestisciVagoniEnum { SCELTA_IN_CORSO, AGGIUNGI_ELIMINA, AGGIUNGI, ELIMINA };

enum class MenuConvogliGestisciLocoEnum { INSERIMENTO_CONVOGLIO, AGGIUNGI, ELIMINA, INVERTI_LUCI };

enum class MenuLocomotiveMappingVelocitaTuttiStepEnum { INSERISCI_SENSORE_1, INSERISCI_SENSORE_2 };

enum class MenuLocomotiveMappingVelocitaSingoloStepEnum {
    INSERISCI_STEP,
    INSERISCI_MODALITA_SENSORI,
    MODALITA_1_SENSORE_INSERISCI_SENSORE_1,
    MODALITA_2_SENSORI_INSERISCI_SENSORE_1,
    MODALITA_2_SENSORI_INSERISCI_SENSORE_2
};

enum class MenuLocomotiveMappingVelocitaIntervalloStepEnum {
    INSERISCI_STEP_1,
    INSERISCI_STEP_2,
    INSERISCI_MODALITA_SENSORI,
    MODALITA_1_SENSORE_INSERISCI_SENSORE_1,
    MODALITA_2_SENSORI_INSERISCI_SENSORE_1,
    MODALITA_2_SENSORI_INSERISCI_SENSORE_2
};

enum class MenuLocomotiveDecoderEnum {
    SCELTA_IN_CORSO,
    CAMBIA_INDIRIZZO,
    CAMBIA_KEEP_ALIVE,
    RESET_ACCELERAZIONE_DECELERAZIONE
};

enum class MenuLocomotiveMappingVelocitaEnum {
    AVVISO,
    SCELTA_IN_CORSO,
    TUTTI_STEP,
    SINGOLO_STEP,
    INTERVALLO_STEP

};

enum class MenuLocomotiveTempoUsoEnum { SCELTA_IN_CORSO, STATO_LOCO, MANUTENZIONE_EFFETTUATA };

enum class MenuWebServerEnum { SCELTA_IN_CORSO, MOSTRO_INDIRIZZO_IP };

enum class MenuTracciatoScambiEnum { SCELTA_IN_CORSO, AZIONA, RESET };

enum class MenuTracciatoSensoriIrEnum { SCELTA_IN_CORSO, TEST };

enum class MenuTracciatoSensoriIrTestEnum { INSERISCI_SENSORE, STATO_SENSORE };

enum class MenuAccessoriEnum { SCELTA_IN_CORSO, LED, AUDIO, SENSORE_IMPRONTE, BUZZER };

enum class MenuAccessoriAudioEnum { SCELTA_IN_CORSO, TEST, VOLUME };

enum class MenuAccessoriSensoreImpronteEnum { SCELTA_IN_CORSO, AGGIUNGI_IMPRONTA, RESET_IMPRONTE };

enum class MenuAccessoriBuzzerEnum { SCELTA_IN_CORSO, TEST, CAMBIO_VOLUME };

enum class MenuAccessoriSensoreImpronteAggiungiImprontaEnum { SCELTA_IN_CORSO, ATTESA_DITO };

enum class MenuMonitoraggioEnum { SCELTA_IN_CORSO, ASSORBIMENTI, TENSIONI_1, TENSIONI_2 };

// *** DICHIARAZIONE VARIABILI *** //

extern FuoriMenuEnum fuoriMenu;
extern MenuPrincipaleEnum menuPrincipale;
extern MenuConvogliEnum menuConvogli;
extern MenuLocomotiveEnum menuLocomotive;
extern MenuConvogliGestisciEnum menuConvogliGestisci;
extern MenuConvogliGestisciLocoEnum menuConvogliGestisciLoco;
extern MenuConvogliGestisciVagoniEnum menuConvogliGestisciVagoni;
extern MenuConvogliGestisciPresenzaEnum menuConvogliGestisciPresenza;
extern MenuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpintaEnum
    menuConvogliGestisciDifferenzaVelocitaLocomotivaTrazioneSpinta;
extern MenuLocomotiveTempoUsoEnum menuLocomotiveTempoUso;
extern MenuLocomotiveMappingVelocitaEnum menuLocomotiveMappingVelocita;
extern MenuLocomotiveMappingVelocitaTuttiStepEnum menuLocomotiveMappingVelocitaTuttiStep;
extern MenuLocomotiveMappingVelocitaSingoloStepEnum menuLocomotiveMappingVelocitaSingoloStep;
extern MenuLocomotiveMappingVelocitaIntervalloStepEnum menuLocomotiveMappingVelocitaIntervalloStep;
extern MenuLocomotiveDecoderEnum menuLocomotiveDecoder;
extern MenuWebServerEnum menuWebServer;
extern MenuTracciatoEnum menuTracciato;
extern MenuTracciatoScambiEnum menuTracciatoScambi;
extern MenuTracciatoSensoriIrEnum menuTracciatoSensoriIr;
extern MenuTracciatoSensoriIrTestEnum menuTracciatoSensoriIrTest;
extern MenuAccessoriEnum menuAccessori;
extern MenuAccessoriAudioEnum menuAccessoriAudio;
extern MenuAccessoriSensoreImpronteEnum menuAccessoriSensoreImpronte;
extern MenuAccessoriBuzzerEnum menuAccessoriBuzzer;
extern MenuAccessoriSensoreImpronteAggiungiImprontaEnum menuAccessoriSensoreImpronteAggiungiImpronta;
extern MenuMonitoraggioEnum menuMonitoraggio;

#endif
