#ifndef ARDUINO_DISPLAYVARIABILIFLAG_H
#define ARDUINO_DISPLAYVARIABILIFLAG_H

// *** DICHIARAZIONE VARIABILI *** //

extern bool velocitaConvogliDisplayDaAggiornare;
extern bool direzioneConvogliDisplayDaAggiornare;
extern bool luciConvogliDisplayDaAggiornare;

#endif
