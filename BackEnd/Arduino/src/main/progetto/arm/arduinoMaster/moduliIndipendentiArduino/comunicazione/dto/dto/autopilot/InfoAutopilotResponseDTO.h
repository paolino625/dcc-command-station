#ifndef ARDUINO_INFOAUTOPILOTRESPONSEDTO_H
#define ARDUINO_INFOAUTOPILOTRESPONSEDTO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/ModalitaAutopilot.h"

// *** CLASSE *** //

class InfoAutopilotResponseDTO {
   public:
    ModalitaAutopilot modalitaAutopilot;
    StatoAutopilot statoAutopilot;
};

#endif
