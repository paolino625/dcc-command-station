// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityCopia.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityErrorDetection.h"

/*
Crea il pacchetto luci in base agli input forniti.
Il parametro gruppoFunzioniAusiliarie può essere 0, 1 o 2:
- 1 per F0-F4
- 2 per F5-F8
- 3 per F9-F12
*/

// *** DEFINIZIONE FUNZIONI *** //

void compongoPacchettoScritturaCv(Pacchetto *pacchetto, int indirizzoCvInt, byte datoDaScrivereByte) {
    /*

    - Preambolo esteso: almeno 20 volte 1
    - 0
    - 0111
    - CC = tipo di istruzione (11 per scrittura byte)
    - AA = Inizio indirizzo CV da modificare
    - 0
    - AAAAAAAA = Fine indirizzo CV da modificare
    - 0
    - DDDDDDDD = Valore da scrivere
    - 0
    - EEEEEEEE = Error Detection Byte
    - 1

    FORMATO PACCHETTO: PREAMBOLO 0 0111CCAA 0 AAAAAAAA 0 DDDDDDDD 0 EEEEEEEE 1

    A: Indirizzo CV
    C: Tipo istruzione (11 per scrittura byte)
    D: Dato da scrivere
    E: Error detection

    */

    // PREAMBOLO ESTESO

    copioPreamboloInPacchetto(pacchetto, 30);

    // PACKET START BIT

    pacchetto->bitPacchetto[30] = 0;

    // 1° BYTE

    bool primoByteBool[8];

    primoByteBool[0] = 0;
    primoByteBool[1] = 1;
    primoByteBool[2] = 1;
    primoByteBool[3] = 1;
    primoByteBool[4] = 1;
    primoByteBool[5] = 1;
    primoByteBool[6] = (indirizzoCvInt >> 10) & 1;  // Shifta a destra di 9 posizioni e maschera con 1
    primoByteBool[7] = (indirizzoCvInt >> 9) & 1;

    copioArrayBoolInPacchetto(pacchetto, primoByteBool, 31, 39);

    // 0

    pacchetto->bitPacchetto[39] = 0;

    // 2° BYTE

    // AAAAAAAA

    bool indirizzoCvBitMenoSignificativi[8];

    copioByteInArrayBool(indirizzoCvBitMenoSignificativi, indirizzoCvInt);
    copioArrayBoolInPacchetto(pacchetto, indirizzoCvBitMenoSignificativi, 40, 48);

    // 0

    pacchetto->bitPacchetto[48] = 0;

    // 3° BYTE

    // DDDDDDDD

    bool datoDaScrivere[8];

    copioByteInArrayBool(datoDaScrivere, datoDaScrivereByte);
    copioArrayBoolInPacchetto(pacchetto, datoDaScrivere, 49, 57);

    // 0

    pacchetto->bitPacchetto[57] = 0;

    // 4° BYTE

    // Error Detection

    bool errorDetection[8];
    creoErrorDetectionEstesa(errorDetection, primoByteBool, indirizzoCvBitMenoSignificativi, datoDaScrivere);

    copioArrayBoolInPacchetto(pacchetto, errorDetection, 58, 66);

    // PACKET END BIT

    pacchetto->bitPacchetto[66] = 1;
}