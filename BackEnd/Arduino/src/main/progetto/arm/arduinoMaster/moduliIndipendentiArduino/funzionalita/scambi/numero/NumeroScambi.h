#ifndef ARDUINO_NUMEROSCAMBI_H
#define ARDUINO_NUMEROSCAMBI_H

/*
Numero di scambi nel plastico. Numero scambi reali + 1 perché non partiamo da 0 ma da 1.
Se si aggiungono nuovi scambi, ricordarsi di modificare anche PosizioneDefaultScambi.h.
*/
#define NUMERO_SCAMBI 94

#endif
