#ifndef ARDUINO_SENSOREPOSIZIONEDTO_H
#define ARDUINO_SENSOREPOSIZIONEDTO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINE *** //

#define NUMERO_CARATTERI_SENSORE_POSIZIONE_STRINGA_JSON 200

// *** CLASSE *** //

class SensorePosizioneDTO {
    // *** VARIABILI *** //

   public:
    IdSensorePosizioneTipo id;
    bool stato;
    IdConvoglioTipo idConvoglioInAttesa;
};

#endif
