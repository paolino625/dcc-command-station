#ifndef ARDUINO_TIMESTAMP_H
#define ARDUINO_TIMESTAMP_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DICHIARAZIONE VARIABILI *** //

extern TimestampTipo timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp;
extern TimestampTipo timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp;

#endif
