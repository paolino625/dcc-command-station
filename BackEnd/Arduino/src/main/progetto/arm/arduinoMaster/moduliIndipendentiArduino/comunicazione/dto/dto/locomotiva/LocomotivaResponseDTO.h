#ifndef LOCOMOTIVA_RESPONSE_DTO_H
#define LOCOMOTIVA_RESPONSE_DTO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/Rotabili.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** CLASSE *** //

class LocomotivaResponseDTO {
   public:
    IdLocomotivaTipo id;
    char* nome;
    char* codice;
};

#endif
