// *** INCLUDE *** //

#include "PacchettoDcc.h"

#include <cstring>
#include <string>

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"

// *** DEFINIZIONE FUNZIONI *** //

void stampoPacchetto(volatile Pacchetto *pacchetto, LoggerAbstract &logger) {
    if (DEBUGGING_PACCHETTO_CON_INDICI) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_,
                              "INIZIO: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    }

    for (int i = 0; i < pacchetto->numeroBitPacchetto; i++) {
        if (DEBUGGING_PACCHETTO_CON_INDICI) {
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "BIT N° ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, std::to_string(i).c_str(),
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, ": ",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, pacchetto->bitPacchetto[i] ? "1" : "0",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        }
    }

    if (DEBUGGING_PACCHETTO_CON_INDICI) {
        LOG_MESSAGGIO_STATICO(logger, LivelloLog::DEBUG_, "FINE", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA,
                              true);
    }
}
