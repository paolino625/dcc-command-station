#ifndef DCC_COMMAND_STATION_BUFFERMENUDISPLAYKEYPAD_H
#define DCC_COMMAND_STATION_BUFFERMENUDISPLAYKEYPAD_H

// *** DICHIARAZIONE VARIABILI *** //

extern IdLocomotivaTipo idLocomotivaDisplayKeypad;
extern IdConvoglioTipo idConvoglioDisplayKeypad;
extern int numeroSensoreDisplayKeypad;

extern StepVelocitaDecoderTipo stepInizialeDisplayKeypad, stepFinaleDisplayKeypad;
extern int modalita2SensoriIntDisplayKeypad;
extern int sensoreDaUsare1;
extern int sensoreDaUsare2;
extern bool modalita2SensoriDisplayKeypad;

extern byte indiceUtenteSalvataggio;

#endif
