#ifndef DCC_COMMAND_STATION_CONVERSIONITEMPO_H
#define DCC_COMMAND_STATION_CONVERSIONITEMPO_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DICHIARAZIONE FUNZIONI *** //

TimestampTipo convertoMillisecondiAMinuti(TimestampTipo tempoMillisecondi);
TimestampTipo convertoMillisecondiASecondi(TimestampTipo tempoMillisecondi);
TimestampTipo convertoMinutiAMillisecondi(TimestampTipo tempoMinuti);

#endif