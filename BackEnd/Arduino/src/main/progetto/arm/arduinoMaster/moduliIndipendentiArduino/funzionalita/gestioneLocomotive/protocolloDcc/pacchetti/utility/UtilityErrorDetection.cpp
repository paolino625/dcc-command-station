// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** DEFINIZIONE FUNZIONI *** //

void creoErrorDetection(bool *errorDetection, bool *byte1, bool *byte2) {
    for (byte k = 0; k < 8; k++) {
        errorDetection[k] = byte1[k] ^ byte2[k];  // ^ = OR
    }
}

void creoErrorDetectionEstesa(bool *errorDetection, bool *byte1, bool *byte2, bool *byte3) {
    for (int k = 0; k < 8; k++) {
        errorDetection[k] = byte1[k] ^ byte2[k] ^ byte3[k];  // OR
    }
}