// *** INCLUDE *** //

#include "FunzioniAusiliarie.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityCopia.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityErrorDetection.h"

// *** DEFINIZIONE METODI *** //

void compongoFunzioniAusiliarie(LocomotiveAbstract &locomotive, Pacchetto *pacchetto, IdLocomotivaTipo idLocomotiva,
                                byte gruppoFunzioniAusiliarie) {
    byte indirizzoLocomotivaByte = locomotive.getLocomotiva(idLocomotiva, true)->getIndirizzoDecoderDcc();
    /*

    - Preambolo: 14 volte 1
    - Packet Start Bit 0
    - Primo byte: indirizzo locomotiva A
    - Packet Start Bit 0
    - Secondo byte: velocità B
    - Packet Start Bit 0
    - Terzo byte: error detection C
    - Packet End Bit 1

   FORMATO PACCHETTO: 11111111 1111110A AAAAAAA0 BBBBBBBB 0CCCCCCC
   C1 Indice Decine                 111111 11112222 22222233
   33333333 44 Indice Unità       01234567 89012345 67890123
    45678901 23456789 01

   */

    // PREAMBOLO

    copioPreamboloInPacchetto(pacchetto, 14);

    // PACKET START BIT

    pacchetto->bitPacchetto[14] = 0;

    // PRIMO BYTE

    // Indirizzo locomotiva A

    bool indirizzoLocomotiva[8];
    copioByteInArrayBool(indirizzoLocomotiva, indirizzoLocomotivaByte);
    copioArrayBoolInPacchetto(pacchetto, indirizzoLocomotiva, 15, 23);

    // 0

    pacchetto->bitPacchetto[23] = 0;

    // SECONDO BYTE

    // Tipologia di istruzione e dettagli

    bool istruzioneLuci[8];
    if (gruppoFunzioniAusiliarie == 1) {
        creoIstruzioneFunzioneAusiliareFunzioniDa1A4(locomotive, istruzioneLuci, idLocomotiva);
    } else if (gruppoFunzioniAusiliarie == 2) {
        creoIstruzioneFunzioneAusiliareFunzioniDa5A8(locomotive, istruzioneLuci, idLocomotiva);
    } else if (gruppoFunzioniAusiliarie == 3) {
        creoIstruzioneFunzioneAusiliareFunzioniDa9A12(locomotive, istruzioneLuci, idLocomotiva);
    }
    copioArrayBoolInPacchetto(pacchetto, istruzioneLuci, 24, 32);

    // 0

    pacchetto->bitPacchetto[32] = 0;

    // TERZO BYTE

    // Error Detection

    bool errorDetection[8];
    creoErrorDetection(errorDetection, indirizzoLocomotiva, istruzioneLuci);
    copioArrayBoolInPacchetto(pacchetto, errorDetection, 33, 41);

    // PACKET END BIT 1

    pacchetto->bitPacchetto[41] = 1;
}

void creoIstruzioneFunzioneAusiliareFunzioniDa1A4(LocomotiveAbstract &locomotive, bool *byteFunzione,
                                                  IdLocomotivaTipo idLocomotiva) {
    LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(idLocomotiva, true);
    // FORMATO BYTE: 100EDCBA

    // EDCBA impostati a 0 per disabilitare l'uscita, 1 per abilitarla.

    // Nello specifico:
    // A = F1 (corrisponde AUX1 decoder ESU)
    // B = F2 (corrisponde a AUX2 decoder ESU)
    // C = F3
    // D = F4
    // E = FL (Luci base)

    byteFunzione[0] = 1;
    byteFunzione[1] = 0;
    byteFunzione[2] = 0;
    byteFunzione[3] = locomotiva->getStatoFunzioniAusiliari(0);
    byteFunzione[4] = locomotiva->getStatoFunzioniAusiliari(4);
    byteFunzione[5] = locomotiva->getStatoFunzioniAusiliari(3);
    byteFunzione[6] = locomotiva->getStatoFunzioniAusiliari(2);
    byteFunzione[7] = locomotiva->getStatoFunzioniAusiliari(1);
}

void creoIstruzioneFunzioneAusiliareFunzioniDa5A8(LocomotiveAbstract &locomotive, bool *byteLuci,
                                                  IdLocomotivaTipo idLocomotiva) {
    LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(idLocomotiva, true);
    // FORMATO BYTE: 1011DCBA

    // DCBA impostati a 0 per disabilitare l'uscita, 1 per abilitarla.

    // Nello specifico:
    // A = F5 (corrisponde a AUX3 decoder ESU)
    // B = F6 (corrisponde a AUX4 decoder ESU)
    // C = F7
    // D = F8

    byteLuci[0] = 1;
    byteLuci[1] = 0;
    byteLuci[2] = 1;
    byteLuci[3] = 1;
    byteLuci[4] = locomotiva->getStatoFunzioniAusiliari(8);
    byteLuci[5] = locomotiva->getStatoFunzioniAusiliari(7);
    byteLuci[6] = locomotiva->getStatoFunzioniAusiliari(6);
    byteLuci[7] = locomotiva->getStatoFunzioniAusiliari(5);
}

void creoIstruzioneFunzioneAusiliareFunzioniDa9A12(LocomotiveAbstract &locomotive, bool *byteLuci,
                                                   IdLocomotivaTipo idLocomotiva) {
    LocomotivaAbstract *locomotiva = locomotive.getLocomotiva(idLocomotiva, true);

    // FORMATO BYTE: 1011DCBA

    // DCBA impostati a 0 per disabilitare l'uscita, 1 per abilitarla.

    // Nello specifico:
    // A = F9
    // B = F10
    // C = F11
    // D = F12

    byteLuci[0] = 1;
    byteLuci[1] = 0;
    byteLuci[2] = 1;
    byteLuci[3] = 0;
    byteLuci[4] = locomotiva->getStatoFunzioniAusiliari(12);  // Corrisponde a F4 (AUX4)
    byteLuci[5] = locomotiva->getStatoFunzioniAusiliari(11);  // Corrisponde a F3 (AUX3)
    byteLuci[6] = locomotiva->getStatoFunzioniAusiliari(10);  // Corrisponde a F2 (AUX2)
    byteLuci[7] = locomotiva->getStatoFunzioniAusiliari(9);   // Corrisponde a F1 (AUX1)
}