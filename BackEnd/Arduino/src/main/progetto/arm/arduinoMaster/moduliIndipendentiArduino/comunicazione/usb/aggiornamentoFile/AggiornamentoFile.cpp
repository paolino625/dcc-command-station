// *** INCLUDE ***

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINIZIONE VARIABILI *** //

bool fileScambiDaAggiornare = false;
bool fileLocomotiveDaAggiornare = false;
bool fileConvogliDaAggiornare = false;
bool fileUtentiDaAggiornare = false;

TimestampTipo timestampUltimoAggiornamentoFileUsb;