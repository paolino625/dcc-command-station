#ifndef DCC_COMMAND_STATION_DISPLAYTEMPILIMITESCRITTA_H
#define DCC_COMMAND_STATION_DISPLAYTEMPILIMITESCRITTA_H

// *** DEFINE *** //

// Definisco il tempo limite dopo il quale una scritta che viene stampata sul display scompare

#define TEMPO_LIMITE_SCRITTA_DISPLAY_BREVE 2000
#define TEMPO_LIMITE_SCRITTA_DISPLAY_MEDIO 4000
#define TEMPO_LIMITE_SCRITTA_DISPLAY_LUNGO 6000

#endif
