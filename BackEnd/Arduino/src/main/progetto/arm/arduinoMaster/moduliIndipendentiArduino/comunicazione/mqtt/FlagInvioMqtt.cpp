// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"

// *** DEFINIZIONE VARIABILI *** //

bool locomotiveAggiornamentoMqttRichiesto[NUMERO_LOCOMOTIVE];
bool convogliAggiornamentoMqttRichiesto[NUMERO_CONVOGLI];
bool scambiAggiornamentoMqttRichiesto[NUMERO_SCAMBI];
bool sezioniAggiornamentoMqttRichiesto[NUMERO_SEZIONI_MASSIMO];
bool sensoriPosizioneAggiornamentoMqttRichiesto[NUMERO_SENSORI_POSIZIONE_TOTALI];
bool infoAutopilotAggiornamentoMqttRichiesto;