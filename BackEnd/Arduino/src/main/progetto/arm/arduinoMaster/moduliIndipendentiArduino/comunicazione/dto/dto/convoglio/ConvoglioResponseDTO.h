#ifndef CONVOGLIO_RESPONSE_DTO_H
#define CONVOGLIO_RESPONSE_DTO_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/AutopilotConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convoglio/reale/ConvoglioAbstractReale.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/IdSezioneTipo.h"

// *** STRUCT *** //

struct Autopilot {
    PosizioneConvoglio posizioneConvoglio;
    StatoInizializzazioneAutopilotConvoglio statoInizializzazioneAutopilotConvoglio;
    StatoAutopilotModalitaStazioneSuccessiva statoAutopilotModalitaStazioneSuccessiva;
    StatoAutopilotModalitaApproccioCasuale statoAutopilotModalitaApproccioCasuale;
};

// *** CLASSE *** //

// Struttura messaggio MQTT per Convoglio
class ConvoglioResponseDTO {
   public:
    IdConvoglioTipo id;
    char nome[NUMERO_CARATTERI_NOME_CONVOGLIO];
    IdLocomotivaTipo idLocomotiva1;
    IdLocomotivaTipo idLocomotiva2;
    VelocitaRotabileKmHTipo velocitaMassima;
    VelocitaRotabileKmHTipo velocitaImpostata;
    VelocitaRotabileKmHTipo velocitaAttuale;
    bool luciAccese;
    bool presenteSulTracciato;
    Autopilot autopilot;
};
#endif
