#ifndef DCC_COMMAND_STATION_AGGIORNAMENTOFILE_H
#define DCC_COMMAND_STATION_AGGIORNAMENTOFILE_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

// Intervallo in millisecondi che deve passare tra l'aggiornamento di due file USB.
#define INTERVALLO_AGGIORNAMENTO_FILE_USB 0  // Al momento disabilitato in quanto non si rilevano criticità.

// *** DICHIARAZIONE VARIABILI *** //

// Non posso salvare queste variabili dentro la classe Usb altrimenti poi bisogna chiamare usb per settare i valori, e
// questo si fa in tutti i file portando a ricorsione di #include

extern bool fileScambiDaAggiornare;
extern bool fileLocomotiveDaAggiornare;
extern bool fileConvogliDaAggiornare;
extern bool fileUtentiDaAggiornare;

extern TimestampTipo timestampUltimoAggiornamentoFileUsb;

#endif
