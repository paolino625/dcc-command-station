// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINIZIONE VARIABILI *** //

TimestampTipo timestampUltimoMessaggioRicevutoArduinoSlaveRelayTimestamp;
TimestampTipo timestampUltimoMessaggioRicevutoArduinoSlaveSensoriTimestamp;
