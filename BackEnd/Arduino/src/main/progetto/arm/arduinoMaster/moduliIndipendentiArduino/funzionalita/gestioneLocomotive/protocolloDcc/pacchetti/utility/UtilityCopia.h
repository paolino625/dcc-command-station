#ifndef DCC_COMMAND_STATION_UTILITYCOPIA_H
#define DCC_COMMAND_STATION_UTILITYCOPIA_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** DICHIARAZIONE FUNZIONI *** //

void copioPacchettoVolatile(volatile Pacchetto *pacchetto1, volatile Pacchetto *pacchetto2);
void copioPacchetto(Pacchetto pacchetto1, volatile Pacchetto *pacchetto2);
void copioPreamboloInPacchetto(Pacchetto *pacchetto, int numeroDegli1);
void copioByteInArrayBool(bool *arrayBool, byte byte);
void copioArrayBoolInPacchetto(Pacchetto *pacchetto, bool *byte, int indiceBitInizioPacchetto,
                               int indiceBitFinePacchetto);

#endif
