#ifndef DCC_COMMAND_STATION_VELOCITA_H
#define DCC_COMMAND_STATION_VELOCITA_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** DICHIARAZIONE FUNZIONI *** //

void creoIstruzioneVelocita(bool *byteVelocita, int velocita, bool fermataEmergenza, bool direzioneAvanti);
void creoIstruzioneVelocitaEstesa1Byte(bool *byteVelocita);
void creoIstruzioneVelocitaEstesa2Byte(bool *byteVelocita, int velocita, bool fermataEmergenza, bool direzioneAvanti);

void compongoPacchettoVelocita(Pacchetto *pacchetto, byte indirizzoLocomotivaByte, int velocita, bool direzioneAvanti,
                               bool fermataEmergenza);
void compongoPacchettoVelocitaEsteso(Pacchetto *pacchetto, byte indirizzoLocomotivaByte, int velocita,
                                     bool direzioneAvanti, bool fermataEmergenza);

#endif
