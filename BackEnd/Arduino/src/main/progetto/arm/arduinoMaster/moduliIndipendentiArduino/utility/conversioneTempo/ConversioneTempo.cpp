// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINIZIONE METODI *** //

TimestampTipo convertoMillisecondiAMinuti(TimestampTipo tempoMillisecondi) { return tempoMillisecondi / 60000; }

TimestampTipo convertoMillisecondiASecondi(TimestampTipo tempoMillisecondi) { return tempoMillisecondi / 1000; }

TimestampTipo convertoMinutiAMillisecondi(TimestampTipo tempoMinuti) { return tempoMinuti * 60000; }
