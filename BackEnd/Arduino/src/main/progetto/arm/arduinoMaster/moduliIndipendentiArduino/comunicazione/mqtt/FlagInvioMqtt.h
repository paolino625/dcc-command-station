#ifndef BACKEND_FLAGINVIOMQTT_H
#define BACKEND_FLAGINVIOMQTT_H

// Uso delle variabili flag per capire quando vengono modificate delle variabili di oggetti monitorati con MQTT.
// Non posso chiamare mqttInviaMessaggio() direttamente dalla classe in questione (ad
// esempio in setVelocitaImpostata() dentro il file LocomotivaAbstract) perché altrimenti avremmo una dipendenza
// circolare.

extern bool locomotiveAggiornamentoMqttRichiesto[];
extern bool convogliAggiornamentoMqttRichiesto[];
extern bool scambiAggiornamentoMqttRichiesto[];
extern bool sezioniAggiornamentoMqttRichiesto[];
extern bool sensoriPosizioneAggiornamentoMqttRichiesto[];
extern bool infoAutopilotAggiornamentoMqttRichiesto;

#endif
