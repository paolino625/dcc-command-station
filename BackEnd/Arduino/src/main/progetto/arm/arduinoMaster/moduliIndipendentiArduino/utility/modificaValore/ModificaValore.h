#ifndef DCC_COMMAND_STATION_MODIFICAVALORE_H
#define DCC_COMMAND_STATION_MODIFICAVALORE_H

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** DICHIARAZIONE FUNZIONI *** //

void modificaValore(int *valore, byte nuovoValore);

#endif
