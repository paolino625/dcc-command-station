#ifndef ARDUINO_SEZIONERESPONSEDTO_H
#define ARDUINO_SEZIONERESPONSEDTO_H

// *** CLASSE *** //

class SezioneResponseDTO {
    // *** VARIABILI *** //

   public:
    IdSezioneTipo id;
    StatoSezione stato;
};

#endif
