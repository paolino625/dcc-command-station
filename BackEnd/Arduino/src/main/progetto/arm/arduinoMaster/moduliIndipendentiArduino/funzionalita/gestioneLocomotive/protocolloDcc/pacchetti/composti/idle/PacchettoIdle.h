#ifndef DCC_COMMAND_STATION_PACCHETTOIDLE_H
#define DCC_COMMAND_STATION_PACCHETTOIDLE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** CLASSE *** //

class PacchettoIdle {
    // *** VARIABILI *** //

   public:
    Pacchetto pacchetto;  // Pacchetto Idle da inviare nel caso in cui non ci siano pacchetti da
                          // inviare per continuare ad alimentare il tracciato.

    // *** COSTRUTTORE *** //

    PacchettoIdle() {
        pacchetto.numeroBitPacchetto = 42;
        /*
        Indice Bit
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
        7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
        Bit Pacchetto Idle
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1};

        */

        pacchetto.bitPacchetto[0] = 1;
        pacchetto.bitPacchetto[1] = 1;
        pacchetto.bitPacchetto[2] = 1;
        pacchetto.bitPacchetto[3] = 1;
        pacchetto.bitPacchetto[4] = 1;
        pacchetto.bitPacchetto[5] = 1;
        pacchetto.bitPacchetto[6] = 1;
        pacchetto.bitPacchetto[7] = 1;
        pacchetto.bitPacchetto[8] = 1;
        pacchetto.bitPacchetto[9] = 1;
        pacchetto.bitPacchetto[10] = 1;
        pacchetto.bitPacchetto[11] = 1;
        pacchetto.bitPacchetto[12] = 1;
        pacchetto.bitPacchetto[13] = 1;
        pacchetto.bitPacchetto[14] = 0;
        pacchetto.bitPacchetto[15] = 1;
        pacchetto.bitPacchetto[16] = 1;
        pacchetto.bitPacchetto[17] = 1;
        pacchetto.bitPacchetto[18] = 1;
        pacchetto.bitPacchetto[19] = 1;
        pacchetto.bitPacchetto[20] = 1;
        pacchetto.bitPacchetto[21] = 1;
        pacchetto.bitPacchetto[22] = 1;
        pacchetto.bitPacchetto[23] = 0;
        pacchetto.bitPacchetto[24] = 0;
        pacchetto.bitPacchetto[25] = 0;
        pacchetto.bitPacchetto[26] = 0;
        pacchetto.bitPacchetto[27] = 0;
        pacchetto.bitPacchetto[28] = 0;
        pacchetto.bitPacchetto[29] = 0;
        pacchetto.bitPacchetto[30] = 0;
        pacchetto.bitPacchetto[31] = 0;
        pacchetto.bitPacchetto[32] = 0;
        pacchetto.bitPacchetto[33] = 1;
        pacchetto.bitPacchetto[34] = 1;
        pacchetto.bitPacchetto[35] = 1;
        pacchetto.bitPacchetto[36] = 1;
        pacchetto.bitPacchetto[37] = 1;
        pacchetto.bitPacchetto[38] = 1;
        pacchetto.bitPacchetto[39] = 1;
        pacchetto.bitPacchetto[40] = 1;
        pacchetto.bitPacchetto[41] = 1;
    }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PacchettoIdle pacchettoIdle;

#endif
