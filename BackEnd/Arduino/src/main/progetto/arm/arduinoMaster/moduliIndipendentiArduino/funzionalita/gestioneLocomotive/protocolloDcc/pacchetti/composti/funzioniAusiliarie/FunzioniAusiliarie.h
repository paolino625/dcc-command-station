#ifndef DCC_COMMAND_STATION_FUNZIONIAUSILIARIE_H
#define DCC_COMMAND_STATION_FUNZIONIAUSILIARIE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** FUNZIONI *** //

void creoIstruzioneFunzioneAusiliareFunzioniDa1A4(LocomotiveAbstract &locomotive, bool *byteLuci,
                                                  IdLocomotivaTipo idLocomotiva);
void creoIstruzioneFunzioneAusiliareFunzioniDa5A8(LocomotiveAbstract &locomotive, bool *byteLuci,
                                                  IdLocomotivaTipo idLocomotiva);
void creoIstruzioneFunzioneAusiliareFunzioniDa9A12(LocomotiveAbstract &locomotive, bool *byteLuci,
                                                   IdLocomotivaTipo idLocomotiva);

void compongoFunzioniAusiliarie(LocomotiveAbstract &locomotive, Pacchetto *pacchetto, IdLocomotivaTipo idLocomotiva,
                                byte gruppoFunzioniAusiliarie);

#endif
