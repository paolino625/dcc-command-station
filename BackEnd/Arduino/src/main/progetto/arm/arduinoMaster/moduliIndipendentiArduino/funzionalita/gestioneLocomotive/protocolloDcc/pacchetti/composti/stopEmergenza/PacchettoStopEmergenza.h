#ifndef DCC_COMMAND_STATION_PACCHETTOSTOPEMERGENZA_H
#define DCC_COMMAND_STATION_PACCHETTOSTOPEMERGENZA_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/composti/velocita/Velocita.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** CLASSE *** //

class PacchettoStopEmergenza {
    // *** VARIABILI *** //

   public:
    Pacchetto pacchetto;

    // *** COSTRUTTORE *** //

    PacchettoStopEmergenza() {
        pacchetto.numeroBitPacchetto = 42;
        compongoPacchettoVelocita(&pacchetto, 0, 0, false, true);
    }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PacchettoStopEmergenza pacchettoStopEmergenza;

#endif