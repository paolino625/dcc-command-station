#ifndef ARDUINO_STATOLOGIN_H
#define ARDUINO_STATOLOGIN_H

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** STRUCT *** //

struct StatoLogIn {
    bool logInEffettuato = false;
    byte indiceUtenteLoggato;
};

// *** DICHIARAZIONE VARIABILI *** //

extern StatoLogIn statoLogIn;

#endif
