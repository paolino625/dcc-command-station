// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DEFINIZIONE VARIABILI *** //

IdLocomotivaTipo idLocomotivaDisplayKeypad;
IdConvoglioTipo idConvoglioDisplayKeypad;
int numeroSensoreDisplayKeypad;

StepVelocitaDecoderTipo stepInizialeDisplayKeypad, stepFinaleDisplayKeypad;
int modalita2SensoriIntDisplayKeypad;
int sensoreDaUsare1 = -1;
int sensoreDaUsare2 = -1;
bool modalita2SensoriDisplayKeypad;

byte indiceUtenteSalvataggio;