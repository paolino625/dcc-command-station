#ifndef ARDUINO_POSIZIONEDEFAULTSCAMBI_H
#define ARDUINO_POSIZIONEDEFAULTSCAMBI_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/scambio/ScambioRelayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/scambi/PosizioneScambio/PosizioneScambio.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/scambi/numero/NumeroScambi.h"

// *** CLASSE *** //

class PosizioneDefaultScambi {
    // *** VARIABILI *** //

   public:
    PosizioneScambio posizioneScambi[NUMERO_SCAMBI];

    // *** COSTRUTTORE *** //

    PosizioneDefaultScambi() {
        // Inizializzo le posizioni dei singoli scambi
        posizioneScambi[1] = PosizioneScambio::DESTRA;
        posizioneScambi[2] = PosizioneScambio::SINISTRA;
        posizioneScambi[3] = PosizioneScambio::DESTRA;
        posizioneScambi[4] = PosizioneScambio::SINISTRA;
        posizioneScambi[5] = PosizioneScambio::SINISTRA;
        posizioneScambi[6] = PosizioneScambio::DESTRA;
        posizioneScambi[7] = PosizioneScambio::DESTRA;
        posizioneScambi[8] = PosizioneScambio::DESTRA;
        posizioneScambi[9] = PosizioneScambio::DESTRA;
        posizioneScambi[10] = PosizioneScambio::SINISTRA;
        posizioneScambi[11] = PosizioneScambio::SINISTRA;
        posizioneScambi[12] = PosizioneScambio::SINISTRA;
        posizioneScambi[13] = PosizioneScambio::SINISTRA;
        posizioneScambi[14] = PosizioneScambio::SINISTRA;
        posizioneScambi[15] = PosizioneScambio::SINISTRA;
        posizioneScambi[16] = PosizioneScambio::DESTRA;
        posizioneScambi[17] = PosizioneScambio::SINISTRA;
        posizioneScambi[18] = PosizioneScambio::SINISTRA;
        posizioneScambi[19] = PosizioneScambio::SINISTRA;
        posizioneScambi[20] = PosizioneScambio::SINISTRA;
        posizioneScambi[21] = PosizioneScambio::SINISTRA;
        posizioneScambi[22] = PosizioneScambio::SINISTRA;
        posizioneScambi[23] = PosizioneScambio::SINISTRA;
        posizioneScambi[24] = PosizioneScambio::SINISTRA;
        posizioneScambi[25] = PosizioneScambio::SINISTRA;
        posizioneScambi[26] = PosizioneScambio::SINISTRA;
        posizioneScambi[27] = PosizioneScambio::DESTRA;
        posizioneScambi[28] = PosizioneScambio::SINISTRA;
        posizioneScambi[29] = PosizioneScambio::SINISTRA;
        posizioneScambi[30] = PosizioneScambio::DESTRA;
        posizioneScambi[31] = PosizioneScambio::SINISTRA;
        posizioneScambi[32] = PosizioneScambio::DESTRA;
        posizioneScambi[33] = PosizioneScambio::SINISTRA;
        posizioneScambi[34] = PosizioneScambio::DESTRA;
        posizioneScambi[35] = PosizioneScambio::SINISTRA;
        posizioneScambi[36] = PosizioneScambio::DESTRA;
        posizioneScambi[37] = PosizioneScambio::DESTRA;
        posizioneScambi[38] = PosizioneScambio::DESTRA;
        posizioneScambi[39] = PosizioneScambio::DESTRA;
        posizioneScambi[40] = PosizioneScambio::DESTRA;
        posizioneScambi[41] = PosizioneScambio::DESTRA;
        posizioneScambi[42] = PosizioneScambio::DESTRA;
        posizioneScambi[43] = PosizioneScambio::DESTRA;
        posizioneScambi[44] = PosizioneScambio::DESTRA;
        posizioneScambi[45] = PosizioneScambio::DESTRA;
        posizioneScambi[46] = PosizioneScambio::DESTRA;
        posizioneScambi[47] = PosizioneScambio::DESTRA;
        posizioneScambi[48] = PosizioneScambio::DESTRA;
        posizioneScambi[49] = PosizioneScambio::DESTRA;
        posizioneScambi[50] = PosizioneScambio::DESTRA;
        posizioneScambi[51] = PosizioneScambio::DESTRA;
        posizioneScambi[52] = PosizioneScambio::SINISTRA;
        posizioneScambi[53] = PosizioneScambio::SINISTRA;
        posizioneScambi[54] = PosizioneScambio::SINISTRA;
        posizioneScambi[55] = PosizioneScambio::SINISTRA;
        posizioneScambi[56] = PosizioneScambio::DESTRA;
        posizioneScambi[57] = PosizioneScambio::SINISTRA;
        posizioneScambi[58] = PosizioneScambio::SINISTRA;
        posizioneScambi[59] = PosizioneScambio::DESTRA;
        posizioneScambi[60] = PosizioneScambio::SINISTRA;
        posizioneScambi[61] = PosizioneScambio::SINISTRA;
        posizioneScambi[62] = PosizioneScambio::SINISTRA;
        posizioneScambi[63] = PosizioneScambio::SINISTRA;
        posizioneScambi[64] = PosizioneScambio::SINISTRA;
        posizioneScambi[65] = PosizioneScambio::SINISTRA;
        posizioneScambi[66] = PosizioneScambio::SINISTRA;
        posizioneScambi[67] = PosizioneScambio::DESTRA;
        posizioneScambi[68] = PosizioneScambio::DESTRA;
        posizioneScambi[69] = PosizioneScambio::DESTRA;
        posizioneScambi[70] = PosizioneScambio::DESTRA;
        posizioneScambi[71] = PosizioneScambio::DESTRA;
        posizioneScambi[72] = PosizioneScambio::DESTRA;
        posizioneScambi[73] = PosizioneScambio::DESTRA;
        posizioneScambi[74] = PosizioneScambio::DESTRA;
        posizioneScambi[75] = PosizioneScambio::DESTRA;
        posizioneScambi[76] = PosizioneScambio::DESTRA;
        posizioneScambi[77] = PosizioneScambio::DESTRA;
        posizioneScambi[78] = PosizioneScambio::DESTRA;
        posizioneScambi[79] = PosizioneScambio::DESTRA;
        posizioneScambi[80] = PosizioneScambio::DESTRA;
        posizioneScambi[81] = PosizioneScambio::SINISTRA;
        posizioneScambi[82] = PosizioneScambio::SINISTRA;
        posizioneScambi[83] = PosizioneScambio::SINISTRA;
        posizioneScambi[84] = PosizioneScambio::DESTRA;
        posizioneScambi[85] = PosizioneScambio::DESTRA;
        posizioneScambi[86] = PosizioneScambio::SINISTRA;
        posizioneScambi[87] = PosizioneScambio::DESTRA;
        posizioneScambi[88] = PosizioneScambio::DESTRA;
        posizioneScambi[89] = PosizioneScambio::DESTRA;
        posizioneScambi[90] = PosizioneScambio::SINISTRA;
        posizioneScambi[91] = PosizioneScambio::SINISTRA;
        posizioneScambi[92] = PosizioneScambio::DESTRA;
        posizioneScambi[93] = PosizioneScambio::DESTRA;
    }

    // *** METODI *** //

    PosizioneScambio getPosizioneScambio(int numeroScambio) { return posizioneScambi[numeroScambio]; }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PosizioneDefaultScambi posizioneDefaultScambi;

#endif
