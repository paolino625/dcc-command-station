#ifndef DCC_COMMAND_STATION_TIPIDATI_H
#define DCC_COMMAND_STATION_TIPIDATI_H

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"

// *** TYPEDEF *** //

typedef int IdLocomotivaTipo;
typedef int IndirizzoDecoderTipo;
typedef int IdConvoglioTipo;
typedef int IdVagoneTipo;
typedef int IdSensorePosizioneTipo;
typedef int IdScambioTipo;
typedef int IdStazioneTipo;
typedef int IdEncoderTipo;
typedef float LunghezzaRotabileTipo;
typedef float VelocitaLocomotivaCmSTipo;
typedef int VelocitaRotabileKmHTipo;
typedef float LunghezzaTracciatoTipo;
typedef int IdImprontaTipo;
typedef byte StepVelocitaDecoderTipo;  // Da 0 a 126
typedef int StepVelocitaKmHTipo;
typedef float TensioneTipo;
typedef int CorrenteTipo;  // int perché sono mA

#endif