#ifndef DCC_COMMAND_STATION_CODAPACCHETTIDCC_H
#define DCC_COMMAND_STATION_CODAPACCHETTIDCC_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** STRUCT *** //

// Creiamo una struttura che ci consenta di memorizzare variabili di cui avremo spesso bisogno.
typedef struct CodaPacchetti {
    Pacchetto pacchetto;
    Pacchetto pacchettoSuccessivo;
    bool pacchettoSuccessivoAttivo = false;
    byte indiceBitCorrente = 0;
    byte numeroRipetizioniPacchetto = 0;  // Numero di ripetizioni del pacchetto già fatte
    byte bitCorrente = 0;                 // Se è 0 o 1
    byte numeroPulsazioniRimanenti = 0;

} CodaPacchetti;

// *** DICHIARAZIONE VARIABILI *** //

// È necessario rendere questa variabile volatile perché deve essere possibile aggiornarla durante una routine
// interrupt.
extern volatile CodaPacchetti codaPacchetti;

#endif