#ifndef DCC_COMMAND_STATION_UTILITYERRORDETECTION_H
#define DCC_COMMAND_STATION_UTILITYERRORDETECTION_H

// *** DICHIAZIONE FUNZIONI *** //

void creoErrorDetection(bool *errorDetection, bool *byte1, bool *byte2);
void creoErrorDetectionEstesa(bool *errorDetection, bool *byte1, bool *byte2, bool *byte3);

#endif
