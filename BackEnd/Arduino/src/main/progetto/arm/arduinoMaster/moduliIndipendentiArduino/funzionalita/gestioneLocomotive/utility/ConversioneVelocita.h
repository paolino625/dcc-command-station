#ifndef DCC_COMMAND_STATION_CONVERSIONEVELOCITA_H
#define DCC_COMMAND_STATION_CONVERSIONEVELOCITA_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/TipiDati.h"

// *** DICHIARAZIONE FUNZIONI *** //

VelocitaRotabileKmHTipo convertoVelocitaModellinoAReale(VelocitaLocomotivaCmSTipo velocitaLocomotivaModellinoCmS);
VelocitaLocomotivaCmSTipo convertoVelocitaRealeModellino(VelocitaRotabileKmHTipo velocitaLocomotivaRealeKmH);

#endif