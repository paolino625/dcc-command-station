#ifndef DCC_COMMAND_STATION_PACCHETTOSUPEREXTENDED_H
#define DCC_COMMAND_STATION_PACCHETTOSUPEREXTENDED_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** CLASSE *** //

class PacchettoSuperExtended {
    // *** VARIABILI *** //

   public:
    Pacchetto pacchetto;

    // *** COSTRUTTORE *** //

    PacchettoSuperExtended() { pacchetto.numeroBitPacchetto = 67; }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PacchettoSuperExtended pacchettoSuperExtended;

#endif
