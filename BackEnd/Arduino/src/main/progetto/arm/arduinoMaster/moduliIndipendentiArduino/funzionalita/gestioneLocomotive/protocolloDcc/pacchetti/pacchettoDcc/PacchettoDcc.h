#ifndef DCC_COMMAND_STATION_PACCHETTODCC_H
#define DCC_COMMAND_STATION_PACCHETTODCC_H

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

/*
Andiamo a creare una nuova struttura dati per il pacchetto GestioneLocomotive.
Un pacchetto GestioneLocomotive standard contiene 42 bit, un pacchetto GestioneLocomotive extended può contenerne molti
di più. Nel nostro caso utilizziamo esclusivamente pacchetti GestioneLocomotive extended di dimensione massima di 51
bit.

Un bit può assumere esclusivamente due valori: 0 oppure 1.
Possiamo definire un pacchetto come un array di 51 elementi di tipo bool.

N.B: Alcuni progetti visti su Internet definiscono un pacchetto GestioneLocomotive come un array di 6 byte utilizzando
poi le maschere e le funzioni bitWrite e bitRead per la gestione. Inizialmente ho provato questo approccio ma con
pessimi risultati: l'utilizzo di maschere e delle funzioni bitWrite e bitRead è molto macchinoso: se è vero che messa
alla mano la cosa si fa semplice è anche vero che in questo modo la probabilità di fare errori di distrazione rimane
comunque molto alta. Successivamente non notando un effettiva utilità nel gestire i pacchetti in forma di 6 byte ho
deciso di optare per un array di bool.
 */

/*
Numero di BIT presenti in un pacchetto GestioneLocomotive.
Un pacchetto standard è di 42 bit ma quelli extended da noi utilizzati raggiungono al massimo 51 bit (8 bit + Packet
Start Bit).
*/

// *** DEFINE *** //

#define BIT_PACCHETTO_MASSIMO 67

// *** STRUCT *** //

typedef struct {
    /*
    Per semplificazione a ogni pacchetto verrà riservato un array bool di 51
    elementi, indipendentemente se Standard o Extended. Nel caso di pacchetti
    Standard si andranno a utilizzare soltanto 42 di questi.
    */
    bool bitPacchetto[BIT_PACCHETTO_MASSIMO];

    byte numeroBitPacchetto;  // I Pacchetti standard sono da 42 bit. Nel caso di utilizzo di Pacchetti Extended bisogna
                              // modificare tale valore.

} Pacchetto;

// *** DICHIARAZIONE FUNZIONI *** //

extern void stampoPacchetto(volatile Pacchetto *pacchetto, LoggerAbstract &logger);

#endif