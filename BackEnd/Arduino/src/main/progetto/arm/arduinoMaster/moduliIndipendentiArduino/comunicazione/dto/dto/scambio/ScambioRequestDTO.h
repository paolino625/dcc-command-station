#ifndef SCAMBIO_REQUEST_DTO_H
#define SCAMBIO_REQUEST_DTO_H

// *** CLASSE *** //

class ScambioRequestDTO {
   public:
    /*
    Non salviamo la posizione dello scambio come stringa dell'enum in quanto non è efficiente.
    Ci limitiamo a inviare un numero:
    - 0 = Posizione non conosciuta
    - -1 SINISTRA
    - 1 = DESTRA
    */
    int posizione;
};

#endif
