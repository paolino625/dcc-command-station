#ifndef DCC_COMMAND_STATION_PACCHETTOSTANDARD_H
#define DCC_COMMAND_STATION_PACCHETTOSTANDARD_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** CLASSE *** //

class PacchettoStandard {
    // *** VARIABILI *** //

   public:
    Pacchetto pacchetto;

    // *** COSTRUTTORE *** //

    PacchettoStandard() { pacchetto.numeroBitPacchetto = 42; }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PacchettoStandard pacchettoStandard;

#endif
