#ifndef DCC_COMMAND_STATION_RESET_H
#define DCC_COMMAND_STATION_RESET_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** CLASSE *** //

class PacchettoReset {
    // *** VARIABILI *** //

   public:
    Pacchetto pacchetto;  // Reset della memoria volatile dei decoder

    // *** COSTRUTTORE *** //

    PacchettoReset() {
        pacchetto.numeroBitPacchetto = 42;

        /*

        FORMATO PACCHETTO
        111111111111 0 00000000 0 00000000 0 00000000 1

        */

        // Preambolo

        for (int i = 0; i < 14; i++) {
            pacchetto.bitPacchetto[i] = 1;
        }

        // 0

        pacchetto.bitPacchetto[14] = 0;

        // 1° BYTE

        for (int i = 15; i < 23; i++) {
            pacchetto.bitPacchetto[i] = 0;
        }
        pacchetto.bitPacchetto[23] = 0;

        // 2° BYTE

        for (int i = 24; i < 32; i++) {
            pacchetto.bitPacchetto[i] = 0;
        }

        pacchetto.bitPacchetto[32] = 0;

        // 3° BYTE

        for (int i = 33; i < 41; i++) {
            pacchetto.bitPacchetto[i] = 0;
        }

        pacchetto.bitPacchetto[41] = 1;
    }
};

// *** DICHIARAZIONE VARIABILI *** //

extern PacchettoReset pacchettoReset;

#endif
