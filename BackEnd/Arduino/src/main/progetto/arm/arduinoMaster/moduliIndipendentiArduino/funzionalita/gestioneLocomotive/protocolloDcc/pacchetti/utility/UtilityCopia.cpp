// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/pacchettoDcc/PacchettoDcc.h"

// *** DEFINIZIONE FUNZIONI *** //

void copioPacchettoVolatile(volatile Pacchetto *pacchetto1, volatile Pacchetto *pacchetto2) {
    for (int i = 0; i < pacchetto1->numeroBitPacchetto; i++) {
        pacchetto2->bitPacchetto[i] = pacchetto1->bitPacchetto[i];
    }

    pacchetto2->numeroBitPacchetto = pacchetto1->numeroBitPacchetto;
}

void copioPacchetto(Pacchetto pacchetto1, volatile Pacchetto *pacchetto2) {
    for (int i = 0; i < pacchetto1.numeroBitPacchetto; i++) {
        pacchetto2->bitPacchetto[i] = pacchetto1.bitPacchetto[i];
    }

    pacchetto2->numeroBitPacchetto = pacchetto1.numeroBitPacchetto;
}

void copioPreamboloInPacchetto(Pacchetto *pacchetto, int numeroDegli1) {
    for (int i = 0; i < numeroDegli1; i++) {
        pacchetto->bitPacchetto[i] = 1;
    }
}

void copioByteInArrayBool(bool *arrayBool, byte byte) {
    int j = 7;

    for (int i = 0; i < 8; i++) {
        arrayBool[i] = (byte >> (7 - i)) & 1;
        // Vecchio: arrayBool[i] = bitRead(byte, j);
        j--;
    }
}

void copioArrayBoolInPacchetto(Pacchetto *pacchetto, bool *byte, int indiceBitInizioPacchetto,
                               int indiceBitFinePacchetto) {
    int j = 0;

    for (int i = indiceBitInizioPacchetto; i < indiceBitFinePacchetto; i++) {
        pacchetto->bitPacchetto[i] = byte[j];
        j++;
    }
}
