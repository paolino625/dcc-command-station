// *** INCLUDE ***

// Crea il pacchetto velocità in base agli input forniti.
// Accetta come parametro velocità un numero da 0 a 28.

#include "Velocita.h"

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityCopia.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/gestioneLocomotive/protocolloDcc/pacchetti/utility/UtilityErrorDetection.h"

// *** DEFINIZIONE FUNZIONI *** //

void compongoPacchettoVelocita(Pacchetto *pacchetto, byte indirizzoLocomotivaByte, int velocita, bool direzioneAvanti,
                               bool fermataEmergenza) {
    /*

    - Preambolo: 14 volte 1
    - Packet Start Bit 0
    - Primo byte: indirizzo locomotiva A
    - Packet Start Bit 0
    - Secondo byte: velocità B
    - Packet Start Bit 0
    - Terzo byte: error detection C
    - Packet End Bit 1

    FORMATO PACCHETTO: 11111111 1111110A AAAAAAA0 BBBBBBBB 0CCCCCCC C1
    Indice Decine                 111111 11112222 22222233 33333333 44
    Indice Unità       01234567 89012345 67890123 45678901 23456789 01

    */

    // PREAMBOLO

    copioPreamboloInPacchetto(pacchetto, 14);

    // PACKET START BIT 0

    pacchetto->bitPacchetto[14] = 0;

    // PRIMO BYTE
    // Indirizzo locomotiva A

    bool indirizzoLocomotiva[8];
    copioByteInArrayBool(indirizzoLocomotiva, indirizzoLocomotivaByte);
    copioArrayBoolInPacchetto(pacchetto, indirizzoLocomotiva, 15, 23);

    // 0

    pacchetto->bitPacchetto[23] = 0;

    // SECONDO BYTE
    // Tipologia di istruzione e dettagli

    bool istruzioneVelocita[8];
    creoIstruzioneVelocita(istruzioneVelocita, velocita, fermataEmergenza, direzioneAvanti);

    copioArrayBoolInPacchetto(pacchetto, istruzioneVelocita, 24, 32);

    // 0

    pacchetto->bitPacchetto[32] = 0;

    // TERZO BYTE
    // Error Detection

    bool errorDetection[8];
    creoErrorDetection(errorDetection, indirizzoLocomotiva, istruzioneVelocita);

    copioArrayBoolInPacchetto(pacchetto, errorDetection, 33, 41);

    // PACKET END BIT 1

    pacchetto->bitPacchetto[41] = 1;
}

// Crea il pacchetto velocità in base agli input forniti.
// Accetta come parametro velocità un numero da 0 a 126.

void compongoPacchettoVelocitaEsteso(Pacchetto *pacchetto, byte indirizzoLocomotivaByte, int velocita,
                                     bool direzioneAvanti, bool fermataEmergenza) {
    /*

    - Preambolo: 14 volte 1
    - Packet Start Bit 0
    - Primo byte: indirizzo locomotiva A
    - 0
    - Secondo byte: 1° byte velocità B
    - 0
    - Terzo Byte: 2° byte velocità C
    - 0
    - Terzo byte: error detection D
    - Packet End Bit 1

    FORMATO PACCHETTO: 11111111 1111110A AAAAAAA0 BBBBBBBB 0CCCCCCC C0DDDDDD DD1
    Indice Decine                 111111 11112222 22222233 33333333 44444444 445
    Indice Unità       01234567 89012345 67890123 45678901 23456789 01234567 890

    In particolare il secondo e il terzo byte:

    001CCCCC 0 EDDDDDDD

    CCCCC = 11111 -> Indica che l'istruzione è di tipo 128 Speed Step Control
    E = indica la direzione: 1 avanti, 0 indietro
    DDDDDDD = indicano la velocità

    Dunque:

    00111111 0 EDDDDDDD

    */

    // PREAMBOLO

    copioPreamboloInPacchetto(pacchetto, 14);

    // PACKET START BIT

    pacchetto->bitPacchetto[14] = 0;

    // PRIMO BYTE

    // Indirizzo locomotiva A

    bool indirizzoLocomotiva[8];
    copioByteInArrayBool(indirizzoLocomotiva, indirizzoLocomotivaByte);
    copioArrayBoolInPacchetto(pacchetto, indirizzoLocomotiva, 15, 23);

    // 0

    pacchetto->bitPacchetto[23] = 0;

    // SECONDO BYTE

    // Tipologia di istruzione ecc.

    // Formato Byte: 00111111

    bool istruzioneVelocitaEstesa1Byte[8];
    creoIstruzioneVelocitaEstesa1Byte(istruzioneVelocitaEstesa1Byte);

    copioArrayBoolInPacchetto(pacchetto, istruzioneVelocitaEstesa1Byte, 24, 32);

    // 0

    pacchetto->bitPacchetto[32] = 0;

    // TERZO BYTE

    // Dettagli sulla velocità

    bool istruzioneVelocitaEstesa2Byte[8];

    creoIstruzioneVelocitaEstesa2Byte(istruzioneVelocitaEstesa2Byte, velocita, fermataEmergenza, direzioneAvanti);

    copioArrayBoolInPacchetto(pacchetto, istruzioneVelocitaEstesa2Byte, 33, 41);

    // 0

    pacchetto->bitPacchetto[41] = 0;

    // TERZO BYTE

    // Error detection

    bool errorDetection[8];
    creoErrorDetectionEstesa(errorDetection, indirizzoLocomotiva, istruzioneVelocitaEstesa1Byte,
                             istruzioneVelocitaEstesa2Byte);

    copioArrayBoolInPacchetto(pacchetto, errorDetection, 42, 50);

    // PACKET END BIT 1

    pacchetto->bitPacchetto[50] = 1;
}

// Crea il byte che nell'istruzione indica la tipologia d'istruzione (in questo
// caso istruzione velocità) in base agli input forniti.

void creoIstruzioneVelocita(bool *byteVelocita, int velocita, bool fermataEmergenza, bool direzioneAvanti) {
    // Bit 0-1: contengono la sequenza di bit "01" che indica che il data byte
    // istruzione è per la velocità e la direzione.

    byteVelocita[0] = 0;
    byteVelocita[1] = 1;

    // Bit 2: bit per la direzione. Se il bit è 1 la locomotiva si muoverà in
    // avanti. Se il bit è 0 si muoverà all'indietro.

    byteVelocita[2] = direzioneAvanti;

    /*

    Bit 4: per impostazione predefinita deve contenere un bit di velocità
    aggiuntivo, che è il bit di velocità meno significativo.

    Bit 5-8: indicano la velocità dove 0 è la velocità meno significativa.

    Step di velocità (ultimi 5 bit):

    00000: Stop
    10000: Stop
    00001: E-Stop (I decoder devono interrompere immediatamente l'energia ai
    motori)
    10001: E-Stop
    00010: Step 1
    10010: Step 2
    00011: Step 3
    10011: Step 4
    00100: Step 5
    10100: Step 6
    00101: Step 7
    10101: Step 8
    00110: Step 9
    10110: Step 10
    00111: Step 11
    10111: Step 12
    01000: Step 13
    11000: Step 14
    01001: Step 15
    11001: Step 16
    01010: Step 17
    11010: Step 18
    01011: Step 19
    11011: Step 20
    01100: Step 21
    11100: Step 22
    01101: Step 23
    11101: Step 24
    01110: Step 25
    11110: Step 26
    01111: Step 27
    11111: Step 28

    */

    switch (velocita) {
        case 0:

            if (fermataEmergenza) {
                byteVelocita[3] = 0;
                byteVelocita[4] = 0;
                byteVelocita[5] = 0;
                byteVelocita[6] = 0;
                byteVelocita[7] = 1;
            }

            else {
                byteVelocita[3] = 0;
                byteVelocita[4] = 0;
                byteVelocita[5] = 0;
                byteVelocita[6] = 0;
                byteVelocita[7] = 0;
            }

            break;

        case 1:

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 2:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 3:

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 4:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 5:

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 6:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 7:

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 8:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 9:

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 10:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 11:

            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 12:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 13:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 14:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 15:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 16:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 17:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 18:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 19:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 20:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 21:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 22:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 23:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 24:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 25:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 26:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 27:

            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 28:

            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        default:

            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;
    }
}

void creoIstruzioneVelocitaEstesa1Byte(bool *byteVelocita) {
    byteVelocita[0] = 0;
    byteVelocita[1] = 0;
    byteVelocita[2] = 1;
    byteVelocita[3] = 1;
    byteVelocita[4] = 1;
    byteVelocita[5] = 1;
    byteVelocita[6] = 1;
    byteVelocita[7] = 1;
}

void creoIstruzioneVelocitaEstesa2Byte(bool *byteVelocita, int velocita, bool fermataEmergenza, bool direzioneAvanti) {
    // FORMATO BYTE: EDDDDDDD
    // E = direzione: 1 avanti, 0 indietro

    if (direzioneAvanti) {
        byteVelocita[0] = 1;
    }

    else {
        byteVelocita[0] = 0;
    }

    switch (velocita) {
        case 0:

            if (fermataEmergenza) {
                byteVelocita[1] = 0;
                byteVelocita[2] = 0;
                byteVelocita[3] = 0;
                byteVelocita[4] = 0;
                byteVelocita[5] = 0;
                byteVelocita[6] = 0;
                byteVelocita[7] = 1;
            }

            else {
                byteVelocita[1] = 0;
                byteVelocita[2] = 0;
                byteVelocita[3] = 0;
                byteVelocita[4] = 0;
                byteVelocita[5] = 0;
                byteVelocita[6] = 0;
                byteVelocita[7] = 0;
            }

            break;

        case 1:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 2:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 3:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 4:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 5:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 6:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 7:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 8:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 9:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 10:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 11:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 12:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 13:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 14:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 15:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 16:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 17:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 18:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 19:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 20:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 21:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 22:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 23:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 24:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 25:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 26:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 27:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 28:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 29:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 30:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 31:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 32:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 33:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 34:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 35:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 36:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 37:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 38:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 39:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 40:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 41:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 42:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 43:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 44:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 45:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 46:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 47:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 48:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 49:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 50:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 51:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 52:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 53:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 54:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 55:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 56:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 57:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 58:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 59:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 60:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 61:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 62:

            byteVelocita[1] = 0;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 63:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 64:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 65:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 66:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 67:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 68:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 69:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 70:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 71:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 72:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 73:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 74:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 75:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 76:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 77:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 78:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 79:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 80:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 81:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 82:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 83:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 84:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 85:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 86:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 87:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 88:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 89:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 90:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 91:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 92:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 93:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 94:

            byteVelocita[1] = 1;
            byteVelocita[2] = 0;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 95:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 96:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 97:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 98:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 99:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 100:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 101:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 102:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 103:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 104:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 105:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 106:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 107:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 108:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 109:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 110:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 0;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 111:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 112:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 113:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 114:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 115:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 116:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 117:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 118:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 0;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 119:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 120:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 121:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 122:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        case 123:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 0;

            break;

        case 124:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 0;
            byteVelocita[7] = 1;

            break;

        case 125:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;

        case 126:

            byteVelocita[1] = 1;
            byteVelocita[2] = 1;
            byteVelocita[3] = 1;
            byteVelocita[4] = 1;
            byteVelocita[5] = 1;
            byteVelocita[6] = 1;
            byteVelocita[7] = 1;

            break;

        default:

            byteVelocita[1] = 0;
            byteVelocita[2] = 0;
            byteVelocita[3] = 0;
            byteVelocita[4] = 0;
            byteVelocita[5] = 0;
            byteVelocita[6] = 1;
            byteVelocita[7] = 0;

            break;
    }
}