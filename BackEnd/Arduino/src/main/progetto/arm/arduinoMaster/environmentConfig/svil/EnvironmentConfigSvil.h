// *** INCLUDE *** //

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/arm/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"
#include "main/progetto/comune/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** VARIABILI *** //

#ifdef SVIL

// Nel caso in cui Arduino sia scollegato da tutto il resto
#define ARDUINO_SCOLLEGATO_COMPONENTI 1

// *** AUTOPILOT *** //

/*
Quando in modalità autopilot, un convoglio arriva in stazione e ci si accorge che un sensore atteso non viene
azionato, viene fermato il programma se questa variabile è a 1. In Svil non vogliamo sempre fermare il programma, in
particolare quando stiamo simulando l'azionamento dei sensori attraverso la funzionalità debugging sensori sul
FrontEnd.
*/
#define AUTOPILOT_SENSORE_ATTESO_MA_NON_AZIONATO_FERMA_PROGRAMMA 0

// *** COMUNICAZIONE *** //

#define ETHERNET_PRESENTE 1
#define INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_GIGA INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_MASTER_TORINO

#define ARDUINO_MASTER_HELPER_PRESENTE 1
static_assert(!(ETHERNET_PRESENTE == 0 && ARDUINO_MASTER_HELPER_PRESENTE != 0),
              "ArduinoMasterHelper deve essere disattivato quando ETHERNET_PRESENTE è 0");
#define INDIRIZZO_ARDUINO_MASTER_HELPER INDIRIZZO_SERVER
#define PORTA_ARDUINO_MASTER_HELPER 6102

// *** DEBUGGING AVANZATO *** //

#define DEBUGGING_MQTT 0

// Se 1 le stampe all'interno della funzione invioTracciatoStatoLocomotive() vengono abilitate
#define DEBUGGING_INVIO_TRACCIATO_STATO_LOCOMOTIVE 0

// Se 1, durante la log del pacchetto, viene stampato ogni bit del pacchetto indicando la sua
// posizione all'interno del pacchetto
#define DEBUGGING_PACCHETTO_CON_INDICI 0

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

#define DEBUGGING_FACTORY 0

#define DEBUGGING_PING_RICEVUTI_DA_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_STRINGHE_RICEVUTE_DA_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_MESSAGGI_INVIATI_AD_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_CODA_RELAY_ARDUINO_SLAVE_RELAY 0

#define DEBUGGING_STRINGHE_RICEVUTE_DA_ARDUINO_SLAVE_SENSORI 0
#define DEBUGGING_ACK_INVIATI_AD_ARDUINO_SLAVE_RELAY 0

#define DEBUGGING_AUDIO 0

#define DEBUGGING_DISTANZA_FRENATA_LOCOMOTIVA 0

#define DEBUGGING_WEB_SERVER 0
#define DEBUGGING_WEB_SERVER_REQUEST 0
// N.B. Fare attenzione quando abilitato. Viene aumentato la quantità di RAM utilizzata e questo potrebbe portare un
// crash di Arduino.
#define DEBUGGING_WEB_SERVER_RESPONSE 0

#define DEBUGGING_REST_CLIENT 0
#define DEBUGGING_REST_CLIENT_REQUEST 0
#define DEBUGGING_REST_CLIENT_RESPONSE 0

#define DEBUGGING_RICERCA_SEZIONI_APPARTENENTI_STAZIONE 0

// N.B. Questo porta a stampare dei messaggi molto grandi che verrebbero troncati se inviati su MQTT.
// Quindi quando si abilita questa opzione, bisogna disabilitare la pubblicazione su MQTT.
#define DEBUGGING_USB 0

#define DEBUGGING_TIMER_LOCOMOTIVE 0

#define DEBUGGING_AGGIORNAMENTO_VELOCITA_ATTUALE_LOCOMOTIVE 0

#define DEBUGGING_AUTOPILOT_CALCOLO_DISTANZA_PROSSIMO_STOP 0

#define DEBUGGING_CHECK_CONNESSIONE_MQTT 0

#define DEBUGGING_LOGICA_LETTURA_SENSORE 0

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

// Per info addizionali si intende il nome del file, la riga e la funzione in cui è stata chiamata la funzione di log
#define STAMPA_INFO_ADDIZIONALI_LOGGER_ARDUINO 0

// *** LOGIN *** //

#define LOGIN_ABILITATO 1

// *** MQTT *** //

#define MQTT_ATTIVO 1
// Mi assicuro che se ETHERNET_PRESENTE è 0, MQTT_ATTIVO sia 0
static_assert(!(ETHERNET_PRESENTE == 0 && MQTT_ATTIVO != 0),
              "MQTT_ATTIVO deve essere disattivato quando ETHERNET_PRESENTE è 0");

// *** PRESENZA HARDWARE *** //

// Può essere utile dover eseguire lo sketch su Arduino Giga anche quando questo non è collegato a tutti i componenti
// richiesti in ambiente di produzione (ad esempio per scopo di Debugging del WebServer)

#if ARDUINO_SCOLLEGATO_COMPONENTI

#define DISPLAY_PRESENTE 0
#define KEYPAD_PRESENTE 0
#define ENCODER_PRESENTI 0
#define SCHEDA_ALIMENTAZIONE_PRESENTE 0
#define SCHEDA_AMPLIFICATORE_AUDIO 0
#define MOTOR_SHIELD_PRESENTE 0
#define PULSANTE_EMERGENZA_PRESENTE 0
#define SENSORE_IMPRONTE_PRESENTE 0
#define ARDUINO_SLAVE_PRESENTI 0

#define ALIMENTATORE_12V_PRESENTE 0
#define ALIMENTATORE_5V_PRESENTE 0
#define ALIMENTATORE_24V_PRESENTE 0
#define ALIMENTATORE_15V_PRESENTE 0
#define ALIMENTATORE_5V_LED1_PRESENTE 0
#define ALIMENTATORE_5V_LED2_PRESENTE 0
#define ALIMENTATORE_5V_LED3_PRESENTE 0

#else

#define DISPLAY_PRESENTE 1
#define KEYPAD_PRESENTE 1
#define ENCODER_PRESENTI 1
#define SCHEDA_ALIMENTAZIONE_PRESENTE 1
#define SCHEDA_AMPLIFICATORE_AUDIO 0  // TODO Riabilitare
#define MOTOR_SHIELD_PRESENTE 1
#define PULSANTE_EMERGENZA_PRESENTE 1
#define SENSORE_IMPRONTE_PRESENTE 1
#define ARDUINO_SLAVE_PRESENTI 1

#define ALIMENTATORE_12V_PRESENTE 1
#define ALIMENTATORE_5V_PRESENTE 1
#define ALIMENTATORE_24V_PRESENTE 1
#define ALIMENTATORE_15V_PRESENTE 1
#define ALIMENTATORE_5V_LED1_PRESENTE 0
#define ALIMENTATORE_5V_LED2_PRESENTE 0
#define ALIMENTATORE_5V_LED3_PRESENTE 0

#endif

// *** WEBSERVER *** //

// Le credenziali sono salvate in formato Base64 nel formato "username:password"
#define CREDENZIALI_AUTENTICAZIONE_BASE_WEBSERVER_BASE64 "YWRtaW46NTQ2MzI2"

#endif
