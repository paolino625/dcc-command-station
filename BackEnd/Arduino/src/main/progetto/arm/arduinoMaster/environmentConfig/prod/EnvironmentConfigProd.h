// *** INCLUDE *** //

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"

// *** VARIABILI *** //

#ifdef PROD

// *** AUTOPILOT *** //

/*
Quando siamo in produzione e durante l'esecuzione dell'autopilot, un convoglio arriva in stazione di destinazione senza
essere passato su un sensore atteso, vogliamo fermare il programma. È necessario un intervento per controllare il
corretto funzionamento del sensore prima di avviare nuovamente l'autopilot.
*/
#define AUTOPILOT_SENSORE_ATTESO_MA_NON_AZIONATO_FERMA_PROGRAMMA 1

// *** COMUNICAZIONE *** //

#define ARDUINO_MASTER_HELPER_PRESENTE 0
#define ETHERNET_PRESENTE 1
#define INDIRIZZO_MAC_ETHERNET_SHIELD INDIRIZZO_MAC_ETHERNET_SHIELD_CHIETI

#define INDIRIZZO_ARDUINO_MASTER_HELPER INDIRIZZO_SERVER
#define PORTA_ARDUINO_MASTER_HELPER 6202

// *** DEBUGGING AVANZATO *** //

#define DEBUGGING_MQTT 0

// Se 1 le stampe all'interno della funzione invioTracciatoStatoLocomotive() vengono abilitate
#define DEBUGGING_INVIO_TRACCIATO_STATO_LOCOMOTIVE 0

// Se 1, durante la log del pacchetto, viene stampato ogni bit del pacchetto indicando la sua
// posizione all'interno del pacchetto
#define DEBUGGING_PACCHETTO_CON_INDICI 0

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

#define DEBUGGING_FACTORY 0

#define DEBUGGING_MQTT 0

#define DEBUGGING_PING_RICEVUTI_DA_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_STRINGHE_RICEVUTE_DA_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_MESSAGGI_INVIATI_AD_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_VALIDAZIONE_ACK_ARDUINO_SLAVE_RELAY 0
#define DEBUGGING_CODA_RELAY_ARDUINO_SLAVE_RELAY 0

#define DEBUGGING_STRINGHE_RICEVUTE_DA_ARDUINO_SLAVE_SENSORI 0
#define DEBUGGING_ACK_INVIATI_AD_ARDUINO_SLAVE_RELAY 0

#define DEBUGGING_AUDIO 0

#define DEBUGGING_DISTANZA_FRENATA_LOCOMOTIVA 0

#define DEBUGGING_WEB_SERVER 0
#define DEBUGGING_WEB_SERVER_REQUEST 0
#define DEBUGGING_WEB_SERVER_RESPONSE 0

#define DEBUGGING_REST_CLIENT 0
#define DEBUGGING_REST_CLIENT_REQUEST 0
#define DEBUGGING_REST_CLIENT_RESPONSE 0

#define DEBUGGING_RICERCA_SEZIONI_APPARTENENTI_STAZIONE 0

#define DEBUGGING_USB 0

#define DEBUGGING_TIMER_LOCOMOTIVE 0

#define DEBUGGING_AGGIORNAMENTO_VELOCITA_ATTUALE_LOCOMOTIVE 0

#define DEBUGGING_AUTOPILOT_CALCOLO_DISTANZA_PROSSIMO_STOP 0

#define DEBUGGING_CHECK_CONNESSIONE_MQTT 0

#define DEBUGGING_LOGICA_LETTURA_SENSORE 0

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

// Per info addizionali si intende il nome del file, la riga e la funzione in cui è stata chiamata la funzione di log
#define STAMPA_INFO_ADDIZIONALI_LOGGER_ARDUINO 0

// *** LOGIN *** //

#define LOGIN_ABILITATO 0

// *** MQTT *** //

#define MQTT_ATTIVO 1

// *** PRESENZA HARDWARE *** //

// Può essere utile dover eseguire lo sketch su Arduino Giga anche quando questo non è collegato a tutti i componenti
// richiesti in ambiente di produzione (ad esempio per scopo di Debugging del WebServer)

#define DISPLAY_PRESENTE 0
#define KEYPAD_PRESENTE 0
#define ENCODER_PRESENTI 0
#define SCHEDA_ALIMENTAZIONE_PRESENTE 0
#define SCHEDA_AMPLIFICATORE_AUDIO 0
#define MOTOR_SHIELD_PRESENTE 0
#define PULSANTE_EMERGENZA_PRESENTE 0
#define SENSORE_IMPRONTE_PRESENTE 0
#define ARDUINO_SLAVE_PRESENTI 0

#define ALIMENTATORE_12V_PRESENTE 0
#define ALIMENTATORE_5V_PRESENTE 0
#define ALIMENTATORE_24V_PRESENTE 0
#define ALIMENTATORE_15V_PRESENTE 0
#define ALIMENTATORE_5V_LED1_PRESENTE 0
#define ALIMENTATORE_5V_LED2_PRESENTE 0
#define ALIMENTATORE_5V_LED3_PRESENTE 0

// *** WEBSERVER *** //

// Le credenziali sono salvate in formato Base64 nel formato "username:password"
#define CREDENZIALI_AUTENTICAZIONE_BASE_WEBSERVER_BASE64 "YWRtaW46NTQ2MzI2"

#endif
