// *** VARIABILI GLOBALI *** //

// L'indirizzo MAC non è essenziale per stabilire la connessione, ma sarà semplicemente il MAC che la scheda di rete presenterà al router.
#define INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_MASTER_CHIETI {0xA8, 0x61, 0x0A, 0xAE, 0x0A, 0x32}
#define INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_MASTER_TORINO {0xA8, 0x61, 0x0A, 0xAE, 0x0F, 0x32}

#define ENDPOINT_COMPONI_MESSAGGIO "/log/componi"
#define ENDPOINT_PUBBLICA_MESSAGGIO "/log/pubblica"