// *** INCLUDE *** //

#include <Arduino.h>

#include "Portenta_H7_TimerInterrupt.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotAbstract/infoAutopilot/InfoAutopilot.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/accelerazioneDecelerazione/intervalloAggiornamentoVelocitaAttuale/IntervalloAggiornamentoVelocitaAttuale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/accelerazioneDecelerazione/timestampUltimoAggiornamentoAccelerazioneDecelerazione/TimestampUltimoAggiornamentoAccelerazioneDecelerazione.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/alimentazioni/AlimentazioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/encoder/EncoderReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/KeypadReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/keypad/core/componente/KeypadComponenteReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/pulsanteEmergenza/PulsanteEmergenzaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoreImpronte/SensoreImpronteReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna219/SensoreCorrenteIna219Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriCorrente/sensoreCorrenteIna260/SensoreCorrenteIna260Reale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/input/sensoriPosizione/SensoriPosizioneReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/amplificatoreAudio/AmplificatoreAudioReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/stampa/core/DisplayCoreReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/ledIntegrato/LedIntegratoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/motorShield/MotorShieldReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/i2c/I2CReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/mqtt/MqttArduinoMasterReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoMaster/inizializzazioneArduinoMaster/InizializzazioneArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/arduinoSlaveRelay/ArduinoSlaveRelay.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/arduinoSlaveSensori/ArduinoSlaveSensori.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/arduinoSlave/comune/inizializzazioneArduinoSlave/InizializzazioneArduinoSlave.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/usb/usb/Usb.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/arduinoMasterHelper/ArduinoMasterHelperReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/autopilot/autopilotReale/AutopilotReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/controlloCortoCircuitoTracciato/ControlloCortoCircuitoTracciatoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/accelerazioneDecelerazione/AccelerazioneDecelerazioneReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/distanzaFrenata/DistanzaFrenataLocomotivaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/invioStatoTracciato/InvioStatoTracciatoReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/mappingStep/MappingStepLocomotiva.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/protocolloDcc/interrupt/Interrupt.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/gestioneLocomotive/stopEmergenza/StopEmergenzaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/logIn/LogInReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/menuDisplayKeypad/core/MenuDisplayKeypadCore.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/realTimeClock/RealTimeClock.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/riproduzioneAudio/core/RiproduzioneAudioCore.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/riproduzioneAudio/test/TestAudio.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/scambi/ScambiRelayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/funzionalita/webServer/webServerCore/WebServerCore.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/convogli/ConvogliReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/convogli/listaConvogli/ListaConvogliReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/listaLocomotive/ListaLocomotiveReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/locomotive/locomotive/LocomotiveReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/vagoni/vagoni/VagoniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/rotabili/vagoni/vagoni/listaVagoni/ListaVagoniReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/sezioni/SezioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/tracciato/stazioni/StazioniReali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/wire/WireReale.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/componentiHardware/output/display/displayVariabiliFlagAggiornamento/DisplayVariabiliFlagAggiornamento.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/comunicazione/usb/aggiornamentoFile/AggiornamentoFile.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/funzionalita/logIn/statoLogIn/StatoLogIn.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/ethernet/core/EthernetCore.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/tempo/TempoReale.h"

// *** DEFINE *** //

#define ATTESA_INIZIALIZZAZIONE_PORTA_SERIALE 500

// *** COSTRUTTORI *** //

/*
I vari costruttori vengono chiamati nei vari file .cpp.
Nei costruttori vengono inizializzati solo le variabili, nulla di più.
Non possiamo chiamare pinMode() dentro il costruttore, perché questo viene chiamato ancora prima che il main (di
Arduino) chiami init, e dunque l'hardware venga settato. È necessario creare una funzione a parte per usare chiamate
relative all'hardware.
*/

/*
Depending on the board, you can select STM32H7 Hardware Timer from TIM1-TIM22
If you select a Timer not correctly, you'll get a message from compiler
'TIMxx' was not declared in this scope; did you mean 'TIMyy'?

Portenta_H7 OK       : TIM1, TIM4, TIM7, TIM8, TIM12, TIM13, TIM14, TIM15, TIM16, TIM17
Portenta_H7 Not OK   : TIM2, TIM3, TIM5, TIM6, TIM18, TIM19, TIM20, TIM21, TIM22
Portenta_H7 No timer : TIM9, TIM10, TIM11. Only for STM32F2, STM32F4 and STM32L1
Portenta_H7 No timer : TIM18, TIM19, TIM20, TIM21, TIM22
*/

// Init timer TIM15
Portenta_H7_Timer ITimer0(TIM15);
// Init  timer TIM16
Portenta_H7_Timer ITimer1(TIM16);

// *** SETUP *** //

void setup() {
    ledIntegratoReale.inizializza();
    ledIntegratoReale.accendiBlu();

    inizializzoPortaSerialeArduinoMaster(PORTA_SERIALE_COMPUTER, VELOCITA_PORTA_SERIALE_COMPUTER, false, false);

    // Devo aspettare affinché la porta seriale sia pronta, altrimenti non vengono stampati i primi messaggi
    delay(ATTESA_INIZIALIZZAZIONE_PORTA_SERIALE);

    // Inizializzo porta seriale computer
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Porta seriale Computer inizializzata.",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "ARDUINO MASTER",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    // Il display è una delle prime cose che inizializzo, in modo tale che possa mostrare errori di connessione
    // ethernet/mqtt.
    if (DISPLAY_PRESENTE) {
        displayCoreReale.getDisplayComponente().inizializza();
        displayReale.avvisi.inCaricamento();
    }

    if (ETHERNET_PRESENTE) {
        ethernetCore.inizializza(true);
    }

    // Mqtt è una delle prime cose che attivo, in questa maniera posso iniziare a inviare i log al broker immediatamente
    if (MQTT_ATTIVO) {
        mqttArduinoMasterReale.mqttCore.inizializza();
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "MQTT disattivato.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    arduinoMasterHelperReale.verificaConnessione();
    /*
     Eseguo il reset di ArduinoMasterHelper per cancellare tutti i dati salvati precedentemente,
     garantendo una situazione di partenza pulita.
     Questo è necessario affinché ArduinoMasterHelper possa gestire correttamente l'endpoint "ready" e segnalare lo
     stato "ready" solo dopo aver ricevuto tutti gli oggetti da Arduino. Senza un reset, esiste il rischio che
     ArduinoMasterHelper utilizzi dati già memorizzati in precedenza e segnali lo stato "ready" anche se Arduino è
     ancora impegnato nella trasmissione.
     */
    arduinoMasterHelperReale.reset();

    realTimeClock.inizializza();

    if (SCHEDA_ALIMENTAZIONE_PRESENTE) {
        /*
        In ambiente di produzione, i controlli successivi sull'alimentazione di 12 e 5V sono superflui. Se gli
        alimentatori non stessero funzionando, Arduino non potrebbe essere acceso. Tuttavia si è deciso di lasciare
        lo stesso questi controlli perché in caso di problemi con la scheda di alimentazione è possibile collegare
        Arduino con il computer e quindi individuare subito il problema.
        */

        // Verifichiamo prima l'alimentazione a 12 V. Questa è la più importante in quanto fornisce alimentazione ai
        // relay sulla scheda di alimentazione.
        if (ALIMENTATORE_12V_PRESENTE) {
            alimentazione12VReale.controllaFunzionamento();
        }

        if (ALIMENTATORE_5V_PRESENTE) {
            alimentazioneSensoriPosizioneReale.controllaFunzionamento();
            alimentazioneArduinoReale.controllaFunzionamento();
            alimentazioneComponentiAusiliari5VReale.controllaFunzionamento();
            alimentazioneComponentiAusiliari3VReale.controllaFunzionamento();
        }

        if (ALIMENTATORE_24V_PRESENTE) {
            alimentazioneCDUReale.controllaFunzionamento();
        }

        if (ALIMENTATORE_5V_LED1_PRESENTE) {
            alimentazioneLED1Reale.controllaFunzionamento();
        }
        if (ALIMENTATORE_5V_LED2_PRESENTE) {
            alimentazioneLED2Reale.controllaFunzionamento();
        }

        if (ALIMENTATORE_5V_LED3_PRESENTE) {
            alimentazioneLED3Reale.controllaFunzionamento();
        }
    }

    if (SCHEDA_AMPLIFICATORE_AUDIO) {
        riproduzioneAudioCoreReale.inizializzaDac(48000);  // Spostarlo potrebbe dare problemi al display
    }

    wireReale.begin();
    I2CReale.stampaDispositiviI2cTrovati();

    if (SCHEDA_AMPLIFICATORE_AUDIO) {
        amplificatoreAudioReale.inizializza();
    }

    if (ENCODER_PRESENTI) {
        encoderReale1.inizializza();
        encoderReale2.inizializza();
    }

    if (KEYPAD_PRESENTE) {
        keypadComponenteReale.inizializza();
    }

    if (MOTOR_SHIELD_PRESENTE) {
        motorShieldReale.inizializza();
    }

    if (PULSANTE_EMERGENZA_PRESENTE) {
        pulsanteEmergenzaReale.inizializza();
    }

    if (SCHEDA_ALIMENTAZIONE_PRESENTE) {
        sensoreCorrenteIna219Reale.inizializza();
        sensoreCorrenteIna260Reale.inizializza();
    }

    if (SENSORE_IMPRONTE_PRESENTE) {
        sensoreImpronteReale.inizializza();
    }

    sensoriPosizioneReali.inizializza();

    // Dopo aver inizializzato INA219 posso controllare la tensione di alimentazione del tracciato
    if (MOTOR_SHIELD_PRESENTE && ALIMENTATORE_15V_PRESENTE) {
        alimentazioneTracciatoReale.controllaFunzionamento();
    }

    vagoniReali.inizializza();
    locomotiveReali.inizializza();
    convogliReali.inizializza();
    scambiRelayReale.inizializza();
    sezioniReali.inizializzaInfoBasilari();
    // Le stazioni devono essere inizializzate dopo le sezioni
    stazioniReali.inizializza();
    sezioniReali.inizializzaPosizioneSensori();

    usb.inizializza();
    usb.leggeFile();

    // Dopo aver letto le info delle locomotive da USB, posso inizializzare le locomotive
    locomotiveReali.inizializzaLocomotive();

    /*
    Bisogna aggiornare luci e calcolare la lunghezza dei convogli dopo che questi sono stati inizializzati con le
    informazioni lette da USB
    */
    convogliReali.aggiornaLuci();
    convogliReali.aggiornaDirezioneLocomotive();

    convogliReali.bloccoSezioniOccupate();

    /*
    Meglio calcolare la lunghezza del convoglio a ogni riavvio di
    Arduino. L'utente potrebbe aver modificato la configurazione del
    convoglio direttamente modificando il file.
    */
    convogliReali.aggiornaLunghezza();

    // Inizializzo liste
    listaLocomotiveReale.inizializza();
    listaVagoniReale.inizializza();
    listaConvogliReale.inizializza();

    distanzaFrenataLocomotivaReale.inizializza();

    menuDisplayKeypadCoreReale.assegnoConvogliInUsoDisplay();  // Assegno 2 dei convogli in uso sul display

    webServerCore.inizializza();

    // Visualizzo credits sul display
    if (MOSTRA_CREDITS) {
        displayReale.avvisi.credits(delayReale);
    }

    if (ARDUINO_SLAVE_PRESENTI) {
        inizializzoArduinoSlave();
    }

    displayReale.menuPrincipale.resettoPosizione();

    inizializzoInterruptTimer();

    if (MOTOR_SHIELD_PRESENTE) {
        motorShieldReale.accende();
    }

    /*
    Appena inizializzo GestioneLocomotive, invio pacchetto di emergenza per inviare segnale di stop a tutti i
    decoder. In alcune locomotive i decoder potrebbero non essersi ancora spenti e aver perso la
    memoria volatile a causa del keep alive, e partirebbero all'ultima velocità preimpostata. In
    questo modo ci assicuriamo che all'accensione tutte le locomotive partano da velocità 0.
    */
    invioStatoTracciatoReale.invioPacchettoStopEmergenza();

    /*
    Invio sul tracciato lo stato di tutte le locomotive. Nel loop, invece, verranno inviati i pacchetti solo relative
    alle informazioni delle locomotive che cambiano.
    */
    invioStatoTracciatoReale.invioPacchettiStatoTutteLocomotive();

    // Inizializzo enumerazioni relative al display
    displayReale.menuPrincipale.resettoPosizione();

    mappingStepLocomotiva.inizializzaLunghezzeTracciatoMapping();

    // Evito che appena si entra nel loop vengano inviati dei messaggi MQTT
    mqttArduinoMasterReale.inizializzaVariabiliTriggerInvioMqtt();

    /*
    Invio lo stato degli oggetti aggiornati prima di entrare in loop, in questa maniera sono sicuro che quando il LED
    si accende di verde Arduino è disponibile a ricevere richieste. Altrimenti ci metterebbe un po' a inviare
    messaggi MQTT con lo stato di tutte le sezioni.
    */
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Invio stato oggetti aggiornati...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    loggerReale.cambiaTabulazione(1);
    mqttArduinoMasterReale.inviaStatoOggettiAggiornati();
    loggerReale.cambiaTabulazione(-1);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Invio stato oggetti aggiornati... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    /*
    Resetto le variabili che indicano che è necessario aggiornare i file su USB per evitare che appena entro nel loop
    aggiorno i file quando non è necessario
    */
    usb.resetVariabiliFlagTriggerAggiornamentoFile();

    // La fase di LogIn ha senso se oltre il display, è presente il sensore di impronte e il keypad
    if (KEYPAD_PRESENTE && DISPLAY_PRESENTE && SENSORE_IMPRONTE_PRESENTE && LOGIN_ABILITATO) {
        buzzerRealeArduinoMaster.beepOk();  // Avviso che sono in attesa di un input
        bool risultatoLogIn = logInReale.effettuoLogIn();
        if (risultatoLogIn) {
            displayReale.homepage.display(convogliReali);
        } else {
            displayReale.avvisi.effettuaLogIn();
        }
    } else {
        // Se il login non è abilitato oppure se manca uno dei componenti necessari, allora non chiedo di fare il login
        // ma mi comporto come se fosse stato effettuato
        statoLogIn.logInEffettuato = true;
        displayReale.homepage.display(convogliReali);
    }

    /*
    Dopo aver inizializzato la connessione MQTT, è necessario inviare il keep alive continuamente per mantenere la
    connessione attiva. Nel loop avviene l'invio costante del keep alive ma questo non succede nel setup(), di fatto la
    connessione viene interrotta nel frattempo. Per evitare che appena entrato nel loop, il check della connessione
    fallisca, ci ri-connettiamo noi appena prima di entrare nel loop.
    */
    if (MQTT_ATTIVO) {
        mqttArduinoMasterReale.mqttCore.connetteBroker(true);
    }

    buzzerRealeArduinoMaster.beepOk();

    // Invio messaggio per avvisare ArduinoMasterHelper che Arduino è pronto a ricevere eventuali richieste GET.
    mqttArduinoMasterReale.invioTriggerArduinoReady();

    // Accendo LED per indicare la corretta esecuzione di setup()
    ledIntegratoReale.accendiVerde();  // Accendo LED verde

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "SETUP COMPLETATO",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// *** LOOP *** //

void loop() {
    if (MQTT_ATTIVO) {
        mqttArduinoMasterReale.mqttCore.checkConnessione();
        mqttArduinoMasterReale.mqttCore.inviaKeepAlive();
        mqttArduinoMasterReale.inviaStatoOggettiAggiornati();
    }

    if (ETHERNET_PRESENTE) {
        ethernetCore.mantieneConnessione();
    }

    /*
    L'aggiornamento dei file USB deve avvenire subito dopo l'invio dello stato degli oggetti aggiornati con MQTT.
    In caso contrario, quando la velocità di una locomotiva passa da 1 a 0, viene attivato il salvataggio del file della
    locomotiva su USB per aggiornare il tempo di funzionamento. Questo processo richiede un po' di tempo, il che ritarda
    l'invio dello stato della locomotiva con velocitàAttuale pari a 0 da Arduino alla WebApp. Questo causerebbe un lag
    nello slider del front end da 1 a 0.
    */

    TimestampTipo millisCorrenteAggiornamentoFileUsb = millis();
    /*
    Aggiorno i file su USB ogni tot secondi e non continuamente.
    Questo perché se l'utente sta azionando lo slider della velocità del convoglio sul Front-End,
    l'aggiornamento dei file USB a ogni singolo valore fa rallentare di molto il ciclo loop di Arduino con varie
    conseguenze (ad esempio velocitaAttuale non aggiornato tanto velocemente quanto richiesto o lettura encoder non
    più reattiva)
    */
    if (millisCorrenteAggiornamentoFileUsb - timestampUltimoAggiornamentoFileUsb >= INTERVALLO_AGGIORNAMENTO_FILE_USB) {
        if (fileScambiDaAggiornare || fileLocomotiveDaAggiornare || fileConvogliDaAggiornare ||
            fileUtentiDaAggiornare) {
            ledIntegratoReale.accendiBlu();

            usb.aggiornaFile();
            ledIntegratoReale.accendiVerde();

            timestampUltimoAggiornamentoFileUsb = millisCorrenteAggiornamentoFileUsb;
        }
    }

    /*
    Se l'autopilot non è attivo, devo provvedere a fare il fetch dei sensori di posizione, altrimenti questi
    rimangono sempre a true. Questo si rende necessario perché a Front-End è possibile vedere lo stato dei sensori
    anche con autopilot disattivato. È necessario effettuare questa operazione dopo che viene inviato lo stato
    aggiornato dei sensori di posizione con MQTT, altrimenti Arduino non invierebbe mai lo stato aggiornato dei
    sensori.
    */
    if (statoAutopilot == StatoAutopilot::NON_ATTIVO) {
        sensoriPosizioneReali.reset(false);
    }

    if (ARDUINO_SLAVE_PRESENTI) {
        arduinoSlaveRelay.ricezione.riceve();
        arduinoSlaveSensori.riceve();

        // Controllo presenza Arduino Slave
        if (controlloPresenzaArduinoSlaveAttivo) {
            arduinoSlaveRelay.controlloPresenza.controllaPresenza();
            // TODO Controllo presenza Arduino Slave Sensori?
        }
    }
    I2CReale.controllaPresenzaDispositiviI2c();

    if (SCHEDA_AMPLIFICATORE_AUDIO) {
        controlloPulsante();  // TODO Eliminare dopo testing
        riproduzioneAudioCoreReale.controllaCoda();
        riproduzioneAudioCoreReale.riproduceFileAudioInCoda();
    }

    /*
    Aggiorno la velocità attuale delle locomotive in base alla velocità impostata.
    In altre parole gestisco accelerazione e decelerazione per renderle più graduali
    indipendentemente dall'input dell'utente. Questo avviene anche quando l'autopilot è inserito. Nella maggior parte
    dei casi l'autopilot va a modificare direttamente la velocità attuale e non la velocità impostata, in quei casi
    questa funzione viene eseguita comunque ma non ha nessun effetto.
    */
    TimestampTipo millisCorrenteAggiornamentoVelocitaAttualeLocomotive = millis();
    if (millisCorrenteAggiornamentoVelocitaAttualeLocomotive - timestampUltimoAggiornamentoAccelerazioneDecelerazione >=
        INTERVALLO_AGGIORNAMENTO_VELOCITA_ATTUALE_CONVOGLIO_GUIDA_MANUALE) {
        timestampUltimoAggiornamentoAccelerazioneDecelerazione = millisCorrenteAggiornamentoVelocitaAttualeLocomotive;
        accelerazioneDecelerazioneReale.aggiorna();
        // Stampo la velocità delle locomotive sul display.
        velocitaConvogliDisplayDaAggiornare = true;
    }

    invioStatoTracciatoReale.processo();

    if (PULSANTE_EMERGENZA_PRESENTE) {
        if (pulsanteEmergenzaReale.isPremuto()) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Premuto pulsante di emergenza!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            stopEmergenzaReale.aziona(true);
            // Devo disattivare l'autopilot se attivo, altrimenti l'autopilot potrebbe continuare a modificare la
            // velocità delle locomotive. Lo disattivo prima di inviare il segnale di stop sul tracciato.
            autopilotReale.ferma(true, false);

            // Del display aggiorno solo la velocità dei convogli
            displayReale.homepage.velocitaConvoglio1(convogliReali);
            displayReale.homepage.velocitaConvoglio2(convogliReali);
        }
    }

    if (DISPLAY_PRESENTE && KEYPAD_PRESENTE) {
        menuDisplayKeypadCoreReale.gestiscoDisplayKeypad(tempoReale, keypadReale, displayReale,
                                                         controlloCortoCircuitoTracciatoReale,
                                                         menuDisplayKeypadOperazioniMultipleReale, utentiReali);
    }

    if (ENCODER_PRESENTI) {
        // Solo se l'autopilot non è attivo e se il log in è stato effettuato, abilito la lettura dagli encoder
        if (statoAutopilot == StatoAutopilot::NON_ATTIVO && statoLogIn.logInEffettuato) {
            ConvoglioAbstractReale* convoglio1;
            if (displayReale.homepage.idConvoglioCorrente1Display != 0) {
                convoglio1 = convogliReali.getConvoglio(displayReale.homepage.idConvoglioCorrente1Display, false);
                // Leggo da encoder 1
                encoderReale1.leggeStato(convoglio1);
            }

            if (displayReale.homepage.idConvoglioCorrente2Display != 0) {
                ConvoglioAbstractReale* convoglio2 =
                    convogliReali.getConvoglio(displayReale.homepage.idConvoglioCorrente2Display, false);
                // Leggo da encoder 2
                encoderReale2.leggeStato(convoglio2);
            }
        }
    }

    webServerCore.gestisceClient();

    if (SCHEDA_ALIMENTAZIONE_PRESENTE) {
        sensoreCorrenteIna219Reale.aggiornaCampionamentoCorrente();
        controlloCortoCircuitoTracciatoReale.effettuaControllo(menuDisplayKeypadOperazioniMultipleReale);
        sensoreCorrenteIna260Reale.aggiornaCampionamentoCorrente();
    }

    if (ARDUINO_SLAVE_PRESENTI) {
        // Gestisco comunicazione con Arduino Slave Relay
        if (arduinoSlaveRelay.ricezione.ackInAttesa) {
            arduinoSlaveRelay.ricezione.controllaAck();
        } else {
            // Non sto aspettando un ACK, posso inviare un altro messaggio se la coda non è vuota
            if (!arduinoSlaveRelay.codaRelayDaInviare.isVuota()) {
                // Se c'è almeno un relay da inviare invio le info ad Arduino
                if (DEBUGGING_CODA_RELAY_ARDUINO_SLAVE_RELAY) {
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "C'è almeno un relay in coda!",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
                }

                arduinoSlaveRelay.inviaInfoRelay();
            }
        }
    }

    autopilotReale.processo();
}