#ifndef DCC_COMMAND_STATION_INDIRIZZOIP_H
#define DCC_COMMAND_STATION_INDIRIZZOIP_H

// *** DEFINE *** //

#define NUMERO_CARATTERI_INDIRIZZO_IP 20

// *** DICHIARAZIONE VARIABILI *** //

extern char indirizzoIp[NUMERO_CARATTERI_INDIRIZZO_IP];

#endif
