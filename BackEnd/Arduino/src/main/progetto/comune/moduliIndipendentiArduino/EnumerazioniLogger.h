#ifndef ARDUINO_ENUMERAZIONI_LOGGER_H
#define ARDUINO_ENUMERAZIONI_LOGGER_H

// *** ENUMERAZIONI *** //

// Uso DEBUG_ e non DEBUG perché DEBUG è già definito in Arduino.h
enum class LivelloLog { DEBUG_, INFO, WARNING, ERROR, CRITICAL, BUG };

enum class InfoFormattazioneLogger {
    NUOVO_MESSAGGIO_NUOVA_RIGA,
    STESSO_MESSAGGIO_NUOVA_RIGA,
    STESSO_MESSAGGIO_STESSA_RIGA
};

#endif
