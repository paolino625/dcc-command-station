// *** INCLUDE *** //

#include "LoggerAbstract.h"

#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// Solo se il codice viene eseguito su Arduino, includiamo la libreria Arduino Json, altrimenti questo preclude la
// possibilità di eseguire i test
#if ARDUINO_ARM
#include <cstring>
#include <string>

#include "ArduinoJson.h"
#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/logger/NumeroCaratteriMessaggioLogDaPubblicare.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#elif ARDUINO_AVR
#include <stdio.h>
#include <string.h>

#include "ArduinoJson.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/comunicazione/mqtt/MqttArduinoSlaveReale.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/utility/logger/NumeroCaratteriMessaggioLogDaPubblicare.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/gestioneFileJson/GestioneFileJson.h"
#else
// Unit Test
#include <cstdio>
#include <cstring>

#include "comunicazione/MqttArduinoMasterMock.h"
#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/logger/NumeroCaratteriMessaggioLogDaPubblicare.h"
#include "main/progetto/arm/environmentConfig/svil/EnvironmentConfigSvil.h"
#endif

// Definisco la stringa da utilizzare come Source nei log e il file env da usare

#ifdef ARDUINO_MASTER
#define SOURCE_LOG "Arduino Master"
#include "main/progetto/arm/arduinoMaster/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/mqtt/MqttArduinoMasterReale.h"
#elif ARDUINO_LED
#define SOURCE_LOG "Arduino Led"
#include "main/progetto/arm/arduinoLed/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/arm/arduinoLed/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoLed/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/comunicazione/mqtt/MqttArduinoLedReale.h"
#include "main/progetto/arm/environmentConfig/svil/EnvironmentConfigSvil.h"
#elif ARDUINO_SLAVE_RELAY
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/svil/EnvironmentConfigSvil.h"
#define SOURCE_LOG "Arduino Slave Relay"
#elif ARDUINO_SLAVE_SENSORI
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#define SOURCE_LOG "Arduino Slave Sensori"
#endif

// *** DEFINIZIONE METODI *** //

void LoggerAbstract::stampa(LivelloLog livelloLog, const char *messaggio,
                            InfoFormattazioneLogger infoFormattazioneLogger, bool ultimoChunkMessaggio,
                            const char *file, int line, const char *function) {
    stampaCore(livelloLog, messaggio, infoFormattazioneLogger, ultimoChunkMessaggio, file, line, function);

    // Convertiamo le info addizionali in una stringa.
    char infoAddizionali[200];
    snprintf(infoAddizionali, 200, "[%s:%d:%s]", file, line, function);
    loggerLocale.stampa(livelloLog, messaggio, infoAddizionali, infoFormattazioneLogger);
}

void LoggerAbstract::stampa(LivelloLog livelloLog, char *messaggio, InfoFormattazioneLogger infoFormattazioneLogger,
                            bool disallocaMemoriaMessaggio, bool ultimoChunkMessaggio, const char *file, int line,
                            const char *function) {
    stampaCore(livelloLog, messaggio, infoFormattazioneLogger, ultimoChunkMessaggio, file, line, function);

    char infoAddizionali[200];
    snprintf(infoAddizionali, 200, "[%s:%d:%s]", file, line, function);
    loggerLocale.stampa(livelloLog, messaggio, infoAddizionali, infoFormattazioneLogger, disallocaMemoriaMessaggio);
}

void LoggerAbstract::cambiaTabulazione(int differenza) { loggerLocale.cambiaTabulazione(differenza); }

template <typename Stringa>
void LoggerAbstract::pubblicaMessaggio(Stringa messaggio, bool ultimoChunkMessaggio, const char *file, int line,
                                       const char *function, LivelloLog livelloLog) {
    if (PUBBLICAZIONE_LOG) {
#ifdef ARDUINO
        JsonDocument jsonDocument;

        jsonDocument["source"] = SOURCE_LOG;
        switch (livelloLog) {
            case LivelloLog::DEBUG_:
                jsonDocument["level"] = "DEBUG";
                break;
            case LivelloLog::INFO:
                jsonDocument["level"] = "INFO";
                break;
            case LivelloLog::WARNING:
                jsonDocument["level"] = "WARNING";
                break;
            case LivelloLog::ERROR:
                jsonDocument["level"] = "ERROR";
                break;
            case LivelloLog::CRITICAL:
                jsonDocument["level"] = "CRITICAL";
                break;
            case LivelloLog::BUG:
                jsonDocument["level"] = "BUG";
                break;
        }
        jsonDocument["environment"] = NOME_ENVIRONMENT;
// La linea viene salvata solo per Arduino ARM, poiché per AVR la memoria è limitata e la linea può essere molto lunga.
#ifdef ARDUINO_ARM
        jsonDocument["file"] = file;
        jsonDocument["line"] = line;
        jsonDocument["function"] = function;
#endif
        jsonDocument["message"] = messaggio;
        jsonDocument["lastChunkMessage"] = ultimoChunkMessaggio;

        if (jsonDocument.size() > NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE) {
            loggerLocale.stampa(LivelloLog::ERROR,
                                "Il messaggio da pubblicare è più lungo della stringa buffer. Allocare più memoria per "
                                "la stringa buffer.",
                                nullptr, InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        }

        char buffer[NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE];

        // Imposto loggerAttivo a false, perché altrimenti avremmo una dipendenza circolare
        serializzoJson(jsonDocument, buffer, NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE, false);
        // Pubblico il messaggio su MQTT
#ifdef ARDUINO_MASTER
        mqttArduinoMasterReale.inviaMessaggioLog(buffer);
#elif ARDUINO_LED
        mqttArduinoLedReale.inviaMessaggioLog(buffer);
#elif ARDUINO_AVR
        mqttArduinoSlaveReale.inviaMessaggioLog(buffer);
#endif

#endif
    }
}

// Necessario per ArduinoSlave, altrimenti errore durante compilazione
template void LoggerAbstract::pubblicaMessaggio<const char *>(const char *, bool, const char *, int, const char *,
                                                              LivelloLog);

bool LoggerAbstract::isMessaggioPubblicabile(const char *messaggio) {
    return strlen(messaggio) <= NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE;
}

void LoggerAbstract::stampaCore(LivelloLog livelloLog, const char *messaggio,
                                InfoFormattazioneLogger infoFormattazioneLogger, bool ultimoChunkMessaggio,
                                const char *file, int line, const char *function) {
    int lunghezzaMessaggio = strlen(messaggio);

    int lunghezzaMessaggioConAccapo = lunghezzaMessaggio + 5;

    // Aggiungo il carattere di accapo in base alla casistica
    char messaggioConAccapo[lunghezzaMessaggioConAccapo];
    switch (infoFormattazioneLogger) {
        case InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA:
            snprintf(messaggioConAccapo, lunghezzaMessaggioConAccapo, "%s", messaggio);
            break;
        case InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA:
            snprintf(messaggioConAccapo, lunghezzaMessaggioConAccapo, "\n%s", messaggio);
            break;
        case InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA:
            snprintf(messaggioConAccapo, lunghezzaMessaggioConAccapo, "%s", messaggio);
            break;
    }

#ifdef ARDUINO_MASTER
    if ( mqttArduinoMasterReale.getMqttCore().isMqttInizializzato()) {
#elif ARDUINO_LED
        if (mqttArduinoLedReale.mqttCore.isMqttInizializzato()){
#elif ARDUINO_AVR
    if(mqttArduinoSlaveReale.mqttCore.isMqttInizializzato()) {
#else
    MqttArduinoMasterMock mqttArduinoMasterMock;
    if(mqttArduinoMasterMock.getMqttCore().isMqttInizializzato()) {
#endif
        if (!isMessaggioPubblicabile(messaggio)) {
            loggerLocale.stampa(LivelloLog::ERROR, "Il seguente messaggio: ", "",
                                InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA);
            loggerLocale.stampa(LivelloLog::ERROR, messaggio, "", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA);
            loggerLocale.stampa(LivelloLog::ERROR, "è troppo lungo per essere pubblicato su MQTT", "",
                                InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA);

            // Se il messaggio è troppo lungo per essere pubblicato su MQTT, pubblico un messaggio personalizzato.
            char messaggioPersonalizzato[70] = "Messaggio troppo lungo per essere pubblicato su MQTT";
            loggerLocale.stampa(LivelloLog::ERROR, messaggioPersonalizzato, "",
                                InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            // Faccio override del livello del log a ERROR
            pubblicaMessaggio(messaggioPersonalizzato, ultimoChunkMessaggio, file, line, function, LivelloLog::ERROR);
        } else {
            pubblicaMessaggio(messaggioConAccapo, ultimoChunkMessaggio, file, line, function, livelloLog);
        }
    }
}