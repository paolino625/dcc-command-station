#ifndef DCC_COMMAND_STATION_TEMPOABSTRACT_H
#define DCC_COMMAND_STATION_TEMPOABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class TempoAbstract {
   public:
    virtual ~TempoAbstract() {}
    virtual TimestampTipo milliseconds() = 0;
    virtual TimestampTipo microseconds() = 0;
};

#endif
