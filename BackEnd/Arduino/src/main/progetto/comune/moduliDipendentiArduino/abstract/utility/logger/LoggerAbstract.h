#ifndef ARDUINO_LOGGERABSTRACT_H
#define ARDUINO_LOGGERABSTRACT_H

// *** INCLUDE *** //

#ifdef ARDUINO_ARM
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/mqtt/MqttArduinoMasterAbstract.h"
#endif
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/loggerLocale/LoggerLocaleAbstract.h"

// *** DEFINE *** //

// Utilizzo un define per la stampa del log in modo da poter passare come parametro il numero di riga, il file e la
// funzione, tutto in maniera trasparente al chiamante
#define LOG_MESSAGGIO_STATICO(logger, livelloLog, messaggio, infoFormattazioneLogger, ultimoChunkMessaggio) \
    logger.stampa(livelloLog, messaggio, infoFormattazioneLogger, ultimoChunkMessaggio, __FILE__, __LINE__, \
                  __FUNCTION__)

#define LOG_MESSAGGIO_DINAMICO(logger, livelloLog, messaggio, infoFormattazioneLogger, ultimoChunkMessaggio,       \
                               disallocaMemoriaMessaggio)                                                          \
    logger.stampa(livelloLog, messaggio, infoFormattazioneLogger, ultimoChunkMessaggio, disallocaMemoriaMessaggio, \
                  __FILE__, __LINE__, __FUNCTION__)

// *** CLASSE *** //

class LoggerAbstract {
    //*** DICHIARAZIONE VARIABIILI ***//

    LoggerLocaleAbstract &loggerLocale;

    //*** COSTRUTTORE ***//

   public:
    LoggerAbstract(LoggerLocaleAbstract &loggerLocale) : loggerLocale{loggerLocale} {}

   public:
    //*** DICHIARAZIONE METODI ***//

    void stampa(LivelloLog livelloLog, const char *messaggio, InfoFormattazioneLogger infoFormattazioneLogger,
                bool ultimoChunkMessaggio, const char *file, int line, const char *function);
    void stampa(LivelloLog livelloLog, char *messaggio, InfoFormattazioneLogger infoFormattazioneLogger,
                bool ultimoChunkMessaggio, bool disallocaMemoriaMessaggio, const char *file, int line,
                const char *function);

    void cambiaTabulazione(int differenza);

    // TODO Rimettere private dopo test
    template <typename Stringa>
    void pubblicaMessaggio(Stringa messaggio, bool ultimoChunkMessaggio, const char *file, int line,
                           const char *function, LivelloLog livelloLog);

   private:
    static bool isMessaggioPubblicabile(const char *messaggio);
    void stampaCore(LivelloLog livelloLog, const char *messaggio, InfoFormattazioneLogger infoFormattazioneLogger,
                    bool ultimoChunkMessaggio, const char *file, int line, const char *function);
};

#endif