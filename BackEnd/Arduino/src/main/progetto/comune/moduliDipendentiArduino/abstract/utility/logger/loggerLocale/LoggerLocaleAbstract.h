#ifndef BACKEND_LOGGER_CORE_ABSTRACT_H
#define BACKEND_LOGGER_CORE_ABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/stampa/StampaAbstract.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINE *** //

#define NUMERO_SPAZI_IN_UN_TAB 5

// *** CLASSE *** //

// Logger Core si occupa di scrivere i Log sulla porta seriale di Arduino
class LoggerLocaleAbstract {
    // *** VARIABILI *** //

   private:
    LivelloLog livelloLog;
    StampaAbstract &stampaCore;

    int numeroSpaziAttuali = 0;

    // *** COSTRUTTORE *** //

   public:
    LoggerLocaleAbstract(LivelloLog livelloLog, StampaAbstract &stampaCore)
        : livelloLog{livelloLog}, stampaCore{stampaCore} {}
    virtual ~LoggerLocaleAbstract() {}

    // *** METODI *** //

    // Devo differenziare i casi in cui il messaggio che ricevo è stato allocato staticamente o dinamicamente.
    // Se è un messaggio che è stato allocato dinamicamente devo liberare la memoria per evitare a lungo andare la
    // memoria su Arduino possa finire. D'altra parte, non posso fare la delete[] a prescindere, in quanto se lo faccio
    // su un messaggio allocato staticamente il programma crasha.

    void stampa(LivelloLog livelloLogMessaggio, const char *messaggio, char *infoAddizionali,
                InfoFormattazioneLogger infoFormattazioneLogger);
    void stampa(LivelloLog livelloLogMessaggio, char *messaggio, char *infoAddizionali,
                InfoFormattazioneLogger infoFormattazioneLogger, bool disallocaMemoriaMessaggio);
    void cambiaTabulazione(int differenza);

   private:
    void stampaSpazi();
    void disallocoMemoriaMessaggio(const char *messaggio);
    void preparoTabulazioneEHeader(InfoFormattazioneLogger infoFormattazioneLogger, const char *livelloLog,
                                   char *infoAddizionali);
};

#endif
