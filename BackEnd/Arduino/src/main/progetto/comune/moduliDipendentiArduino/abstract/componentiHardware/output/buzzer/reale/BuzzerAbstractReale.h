#ifndef DCC_COMMAND_STATION_BUZZERABSTRACTREALE_H
#define DCC_COMMAND_STATION_BUZZERABSTRACTREALE_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/AnalogPinAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/BuzzerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class BuzzerAbstractReale : public BuzzerAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    void accende() const override;
    void spegne() const override;
    void beepSingolo(int tempoBeep) override;
    void beepMultiplo(int tempoBeep, int numeroBeep, int tempoIntervalloBeep) override;
    void beepErrore() override;
    void beepOk() override;
    void beepAvviso() override;
    VolumeTipo getVolume() const override;
    void setVolume(VolumeTipo volumeBuzzerDaImpostare) override;
    BuzzerAbstractReale(AnalogPinAbstract& pinBuzzerDaImpostare, DelayAbstract& delayDaImpostare,
                        LoggerAbstract& logger)
        : BuzzerAbstract(pinBuzzerDaImpostare, delayDaImpostare, logger) {}
};

#endif