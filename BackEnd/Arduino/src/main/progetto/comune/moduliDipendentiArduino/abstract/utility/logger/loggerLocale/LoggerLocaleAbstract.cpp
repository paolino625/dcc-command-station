// *** INCLUDE *** //

#include "LoggerLocaleAbstract.h"

#ifdef ARDUINO_AVR
#include <string.h>
#else
#include <cstring>
#endif

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"

// *** DEFINIZIONE METODI *** //

void LoggerLocaleAbstract::stampa(LivelloLog livelloLogMessaggio, const char *messaggio, char *infoAddizionali,
                                  InfoFormattazioneLogger infoFormattazioneLogger) {
    switch (livelloLogMessaggio) {
        case LivelloLog::DEBUG_:
            if (livelloLog == LivelloLog::DEBUG_) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[DEBUG] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            break;
        case LivelloLog::INFO:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[INFO] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            break;
        case LivelloLog::WARNING:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[WARNING] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            break;
        case LivelloLog::ERROR:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING || livelloLog == LivelloLog::ERROR) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[ERROR] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            break;
        case LivelloLog::CRITICAL:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING || livelloLog == LivelloLog::ERROR ||
                livelloLog == LivelloLog::CRITICAL) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[CRITICAL] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            break;
        case LivelloLog::BUG:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING || livelloLog == LivelloLog::ERROR ||
                livelloLog == LivelloLog::CRITICAL || livelloLog == LivelloLog::BUG) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[BUG] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            break;
    }
}

void LoggerLocaleAbstract::stampa(LivelloLog livelloLogMessaggio, char *messaggio, char *infoAddizionali,
                                  InfoFormattazioneLogger infoFormattazioneLogger, bool disallocaMemoriaMessaggio) {
    switch (livelloLogMessaggio) {
        case LivelloLog::DEBUG_:
            if (livelloLog == LivelloLog::DEBUG_) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[DEBUG] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            if (disallocaMemoriaMessaggio) {
                // Dato che il messaggio è stato allocato dinamicamente, devo liberare la memoria dopo aver stampato il
                // messaggio
                disallocoMemoriaMessaggio(messaggio);
            }
            break;
        case LivelloLog::INFO:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[INFO] ", infoAddizionali);
                stampaSpazi();
                stampaCore.print(messaggio);
            }
            if (disallocaMemoriaMessaggio) {
                // Dato che il messaggio è stato allocato dinamicamente, devo liberare la memoria dopo aver stampato il
                // messaggio
                disallocoMemoriaMessaggio(messaggio);
            }
            break;
        case LivelloLog::WARNING:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[WARNING] ", infoAddizionali);
                stampaCore.print(messaggio);
            }
            if (disallocaMemoriaMessaggio) {
                // Dato che il messaggio è stato allocato dinamicamente, devo liberare la memoria dopo aver stampato il
                // messaggio
                disallocoMemoriaMessaggio(messaggio);
            }
            break;
        case LivelloLog::ERROR:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING || livelloLog == LivelloLog::ERROR) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[ERROR] ", infoAddizionali);
                stampaCore.print(messaggio);
            }

            if (disallocaMemoriaMessaggio) {
                // Dato che il messaggio è stato allocato dinamicamente, devo liberare la memoria dopo aver stampato il
                // messaggio
                disallocoMemoriaMessaggio(messaggio);
            }
            break;
        case LivelloLog::CRITICAL:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                livelloLog == LivelloLog::WARNING || livelloLog == LivelloLog::ERROR ||
                livelloLog == LivelloLog::CRITICAL) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[CRITICAL] ", infoAddizionali);
                stampaCore.print(messaggio);
            }

            if (disallocaMemoriaMessaggio) {
                // Dato che il messaggio è stato allocato dinamicamente, devo liberare la memoria dopo aver stampato il
                // messaggio
                disallocoMemoriaMessaggio(messaggio);
            }
            break;
        case LivelloLog::BUG:
            if (livelloLog == LivelloLog::DEBUG_ || livelloLog == LivelloLog::INFO ||
                    livelloLog == LivelloLog::WARNING || livelloLog == LivelloLog::ERROR ||
                    livelloLog == LivelloLog::CRITICAL,
                livelloLog == LivelloLog::BUG) {
                preparoTabulazioneEHeader(infoFormattazioneLogger, "[BUG] ", infoAddizionali);
                stampaCore.print(messaggio);
            }

            if (disallocaMemoriaMessaggio) {
                // Dato che il messaggio è stato allocato dinamicamente, devo liberare la memoria dopo aver stampato il
                // messaggio
                disallocoMemoriaMessaggio(messaggio);
            }
            break;
    }
}

// Ho scelto una tabulazione relativa e non assoluta. Questo può essere utile in modo tale da non avere problemi quando
// la stessa funzione viene chiamata da più parti del codice. Esempio: funzione inizializza() di StazioneAbstract,
// potremmo impostare valore assoluto di 2 e funzione inizializza() di StazioniAbstract valore assoluto 1. Però in
// alcuni test andiamo a chiamare la funzione inizializza() di StazioneAbstract in maniera autonoma senza
// StazioniAbstract. Questo ci permette di non avere problemi di tabulazione.
void LoggerLocaleAbstract::cambiaTabulazione(int differenza) {
    numeroSpaziAttuali += differenza * NUMERO_SPAZI_IN_UN_TAB;
}

void LoggerLocaleAbstract::stampaSpazi() {
    for (int i = 0; i < numeroSpaziAttuali; i++) {
        stampaCore.print(" ");
    }
}

void LoggerLocaleAbstract::disallocoMemoriaMessaggio(const char *messaggio) { delete[] messaggio; }

void LoggerLocaleAbstract::preparoTabulazioneEHeader(InfoFormattazioneLogger infoFormattazioneLogger,
                                                     const char *livelloLog, char *infoAddizionali) {
    if (infoFormattazioneLogger == InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA) {
        stampaCore.print("\n");
        stampaCore.print(livelloLog);
        if (STAMPA_INFO_ADDIZIONALI_LOGGER_ARDUINO) {
            /*
            Non è detto che le infoAddizionali siano presenti: questo perché se viene chiamato direttamente il
            LoggerCore, le info addizionali non vengono aggiunte. Questo succede solo dentro RestClient, dove non è
            possibile chiamare il Logger causa dipendenza circolare.
            */
            if (infoAddizionali != nullptr) {
                stampaCore.print(infoAddizionali);
                stampaCore.print(" ");
            }
        }
        stampaSpazi();
    } else if (infoFormattazioneLogger == InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA) {
        stampaCore.print("\n");
        // Invece di stampare il livello del log, stampo degli spazi per mantenere la formattazione
        int numeroCaratteriLivelloLog = strlen(livelloLog);
        for (int i = 0; i < numeroCaratteriLivelloLog; i++) {
            stampaCore.print(" ");
        }
        stampaSpazi();
    } else if (infoFormattazioneLogger == InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA) {
    }
}