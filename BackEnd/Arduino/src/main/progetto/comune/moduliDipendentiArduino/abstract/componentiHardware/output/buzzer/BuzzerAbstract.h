#ifndef DCC_COMMAND_STATION_BUZZERABSTRACT_H
#define DCC_COMMAND_STATION_BUZZERABSTRACT_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/AnalogPinAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DEFINE *** //

#define VOLUME_BUZZER_DEFAULT 20

// *** CLASSE *** //

class BuzzerAbstract {
   protected:
    // *** VARIABILI *** //

    AnalogPinAbstract& pin;
    DelayAbstract& delay;
    LoggerAbstract& logger;
    int volume = VOLUME_BUZZER_DEFAULT;

    // *** COSTRUTTORE *** //

   public:
    BuzzerAbstract(AnalogPinAbstract& pinBuzzerDaImpostare, DelayAbstract& delayDaImpostare, LoggerAbstract& logger)
        : pin(pinBuzzerDaImpostare), delay(delayDaImpostare), logger(logger) {}

    // *** DICHIARAZIONE METODI *** //

    virtual void accende() const = 0;
    virtual void spegne() const = 0;
    virtual void beepSingolo(int tempoBeep) = 0;
    virtual void beepMultiplo(int tempoBeep, int numeroBeep, int tempoIntervalloBeep) = 0;
    virtual void beepErrore() = 0;
    virtual void beepOk() = 0;
    virtual void beepAvviso() = 0;
    virtual int getVolume() const = 0;
    virtual void setVolume(int volumeBuzzerDaImpostare) = 0;
};

#endif
