#ifndef DCC_COMMAND_STATION_LOGABSTRACT_H
#define DCC_COMMAND_STATION_LOGABSTRACT_H

// *** CLASSE *** //

class StampaAbstract {
    // *** COSTRUTTORE *** //

   public:
    virtual ~StampaAbstract() {}

    // *** METODI *** //

    virtual void print(const char* messaggio) = 0;
    virtual void print(double numero, int cifreDecimali) = 0;
    virtual void print(int numeroInt) = 0;
    virtual void print(char carattere) = 0;
    virtual void print(double numero) = 0;
    virtual void print(unsigned long numero) = 0;
    virtual void print(unsigned int numero) = 0;
};
#endif