// *** INCLUDE *** //

#include "BuzzerAbstractReale.h"

// *** DEFINIZIONE METODI *** //

void BuzzerAbstractReale::accende() const { pin.scrive(volume); }

void BuzzerAbstractReale::spegne() const { pin.scrive(0); }

void BuzzerAbstractReale::beepSingolo(int tempoBeep) {
    accende();
    delay.milliseconds(tempoBeep);
    spegne();
}

void BuzzerAbstractReale::beepMultiplo(int tempoBeep, int numeroBeep, int tempoIntervalloBeep) {
    for (int i = 0; i < numeroBeep; i++) {
        accende();
        delay.milliseconds(tempoBeep);
        spegne();

        // Se non è l'ultima iterazione aspetto
        if (i != numeroBeep - 1) {
            delay.milliseconds(tempoIntervalloBeep);
        }
    }
}

void BuzzerAbstractReale::beepErrore() { beepMultiplo(200, 5, 200); }

void BuzzerAbstractReale::beepOk() { beepMultiplo(25, 2, 200); }

void BuzzerAbstractReale::beepAvviso() { beepSingolo(25); }

VolumeTipo BuzzerAbstractReale::getVolume() const { return volume; }

void BuzzerAbstractReale::setVolume(VolumeTipo volumeBuzzerDaImpostare) { volume = volumeBuzzerDaImpostare; }