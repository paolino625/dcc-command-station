#ifndef DCC_COMMAND_STATION_DELAYABSTRACT_H
#define DCC_COMMAND_STATION_DELAYABSTRACT_H

// *** CLASSE *** //

class DelayAbstract {
   public:
    virtual ~DelayAbstract() {}
    virtual void milliseconds(unsigned long ms) = 0;
    virtual void microseconds(unsigned long us) = 0;
};

#endif
