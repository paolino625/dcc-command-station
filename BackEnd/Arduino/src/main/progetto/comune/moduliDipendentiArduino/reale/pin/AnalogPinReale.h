#ifndef DCC_COMMAND_STATION_ANALOGPINREALE_H
#define DCC_COMMAND_STATION_ANALOGPINREALE_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/AnalogPinAbstract.h"

// *** CLASSE *** //

class AnalogPinReale final : public AnalogPinAbstract {
    // *** VARIABILI *** //
   private:
    const uint8_t id;

    // *** COSTRUTTORE *** //

   public:
    explicit AnalogPinReale(const uint8_t id) : id{id} {}

    // *** DEFINIZIONE METODI *** //

    [[nodiscard]] auto getId() const -> uint8_t { return id; }

    auto legge() const -> int override {
#ifdef ARDUINO_MASTER
        // Non è inserire A8 nei vari oggetti perché altrimenti da problemi in compilazione. Questo perché pin A8-A11
        // sono di tipo PureAnalogPin e non int. Si è pensati a questo workaround.

        if (id == 118) {
            return analogRead(A8);
        } else if (id == 119) {
            return analogRead(A9);
        } else if (id == 120) {
            return analogRead(A10);
        } else if (id == 121) {
            return analogRead(A11);
        } else {
            return analogRead(id);
        }
#else
        return analogRead(id);
#endif
    }

    auto scrive(int value) const -> void override { analogWrite(id, value); }
};

#endif