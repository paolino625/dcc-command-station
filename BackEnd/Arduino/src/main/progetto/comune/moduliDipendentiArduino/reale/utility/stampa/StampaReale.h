#ifndef DCC_COMMAND_STATION_LOGREALE_H
#define DCC_COMMAND_STATION_LOGREALE_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/stampa/StampaAbstract.h"

// *** CLASSE *** //

class StampaReale : public StampaAbstract {
    // *** DICHIARAZIONE METODI *** //
   public:
    void print(const char* messaggio) override;
    void print(double numero, int cifreDecimali) override;
    void print(double numero) override;
    void print(int numero) override;
    void print(char carattere) override;
    void print(unsigned long numero) override;
    void print(unsigned int numero) override;
};

// *** DICHIARAZIONE VARIABILI *** //

extern StampaReale stampaReale;  // Esclusivo uso del LoggerReale

#endif
