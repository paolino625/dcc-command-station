// *** ATTENZIONE *** //

/*
Su Arduino Giga la nomenclatura delle Serial è diversa da quella degli Arduino Mega.
Arduino Giga infatti presenta 5 porte seriali mentre Arduino Mega solo 4: su Arduino Giga i pin RX0 e TX0 possono essere
utilizzati in contemporanea con la porta seriale USB. Dunque, mentre Serial1 su Arduino Mega si riferisce a RX1/TX1, ad
Arduino Giga si riferisce a RX0/TX0.

Questa è la nomenclatura di Arduino Giga:
Serial.begin = USB
Serial1.begin = RX0/TX0
Serial2.begin = RX1/TX1
Serial3.begin = RX2/TX2
Serial4.begin = RX3/TX3
*/

#ifndef DCC_COMMAND_STATION_PORTESERIALICOMUNE_H
#define DCC_COMMAND_STATION_PORTESERIALICOMUNE_H

// *** INCLUDE *** //

#include "Arduino.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

// Definisco timeout
#define TIMEOUT_PORTA_SERIALE 25  // Se impostato ad 1 si hanno dei problemi

// *** DICHIARAZIONE FUNZIONI *** //

bool inizializzoPortaSerialeArduinoGigaArduinoMega(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                                   bool portaSerialeDaVerificare);
void chiudoPortaSerialeArduinoGigaArduinoMaster(byte numeroPortaSeriale);
bool verificoPortaSerialeArduinoGigaArduinoMaster(byte numeroPortaSeriale);

bool inizializzoPortaSerialeArduinoMinima(VelocitaPorta velocitaPorta);

#endif