// *** INCLUDE *** //

#include "GestioneFileJson.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

#ifdef ARDUINO_MASTER
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#elif ARDUINO_LED
#include "main/progetto/arm/arduinoLed/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#elif ARDUINO_SLAVE_RELAY
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/utility/programma/ProgrammaReale.h"
#elif ARDUINO_SLAVE_SENSORI
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/utility/programma/ProgrammaReale.h"
#endif

// *** DEFINIZIONE VARIABILI *** //

char stringaJsonUsbBuffer[NUMERO_CARATTERI_FILE_JSON_BUFFER];

// *** DEFINIZIONE METODI *** //

void inizializzoStringa(char *stringa, unsigned int numeroCaratteriBuffer) {
    for (unsigned int i = 0; i < numeroCaratteriBuffer; i++) {
        stringa[i] = ' ';
    }
}

void serializzoJson(JsonDocument &documentoJson, char *stringaJson, int numeroCaratteriStringa, bool loggerAttivo) {
    if (loggerAttivo) {
        if (DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Serializzo JSON... ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }

    // Resetto la stringa JSON
    memset(stringaJson, 0, numeroCaratteriStringa);

    serializeJson(documentoJson, stringaJson, numeroCaratteriStringa);
    if (loggerAttivo) {
        if (DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Serializzo JSON... OK",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }
    }
}

bool deserializzoJson(JsonDocument &documentoJson, char *stringaJson, bool fermaProgrammaInCasoDiErrore) {
    if (DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Deserializzo JSON... ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    DeserializationError error = deserializeJson(documentoJson, stringaJson);

    if (error) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG,
                              "Errore: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, error.c_str(),
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        if (fermaProgrammaInCasoDiErrore) {
#ifdef ARDUINO_MASTER
            programmaReale.ferma(true);
#else
            programmaReale.ferma();
#endif
        }
    }

    if (DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Deserializzo JSON... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    if (error) {
        return false;
    } else {
        return true;
    }
}
