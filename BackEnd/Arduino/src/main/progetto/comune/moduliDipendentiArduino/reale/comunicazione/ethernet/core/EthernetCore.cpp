// *** INCLUDE *** //

#include "EthernetCore.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/funzionalita/connessioneInternet/indirizzoIp/IndirizzoIp.h"

#ifdef ARDUINO_MASTER
#include "main/progetto/arm/arduinoMaster/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoMaster.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/componentiHardware/output/display/DisplayReale.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#elif ARDUINO_LED
#include "main/progetto/arm/arduinoLed/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/arm/arduinoLed/environmentConfig/prod/EnvironmentConfigProd.h"
#include "main/progetto/arm/arduinoLed/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/componentiHardware/output/buzzer/BuzzerRealeArduinoLed.h"
#include "main/progetto/arm/arduinoLed/moduliDipendentiArduino/reale/utility/programma/ProgrammaReale.h"
#elif ARDUINO_SLAVE_RELAY
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/componentiHardware/buzzer/BuzzerRealeArduinoSlaveRelay.h"
#elif ARDUINO_SLAVE_SENSORI
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/componentiHardware/buzzer/BuzzerRealeArduinoSlaveSensori.h"
#endif

// *** DEFINIZIONE VARIABILI *** //

#ifdef ARDUINO_MASTER
byte indirizzoMac[] = INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_GIGA;
#elif ARDUINO_LED
byte indirizzoMac[] = INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_MINIMA;
#elif ARDUINO_SLAVE_RELAY
byte indirizzoMac[] = INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_SLAVE_RELAY;
#endif

#ifdef ARDUINO_SLAVE_SENSORI
byte indirizzoMac[] = INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_SLAVE_SENSORI;
#endif

// *** DEFINIZIONE METODI *** //

bool EthernetCore::inizializza(bool fermaProgramma) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Ethernet con DHCP...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    loggerReale.cambiaTabulazione(1);

    bool risultatoConnessioneEthernet = true;
    if (Ethernet.begin(indirizzoMac, TIMEOUT_CONNESSIONE_ETHERNET_MILLIS) == 0) {
        risultatoConnessioneEthernet = false;
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, "Fallimento configurazione Ethernet",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        if (Ethernet.hardwareStatus() == EthernetNoHardware) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL,
                                  "Ethernet shield non trovata. Impossibile continuare senza hardware.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        } else if (Ethernet.linkStatus() == LinkOFF) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, "Cavo Ethernet non collegato.",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        }

        if (fermaProgramma) {
#ifdef ARDUINO_MASTER
            displayReale.errori.ethernet.connessioneNonRiuscita();
            programmaReale.ferma(false);
#endif

#ifdef ARDUINO_LED
            programmaReale.ferma();
#endif
        }
    }

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Connessione Ethernet riuscita",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    loggerReale.cambiaTabulazione(1);
    stampaIp();
    loggerReale.cambiaTabulazione(-1);

    loggerReale.cambiaTabulazione(-1);
    if (risultatoConnessioneEthernet) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Ethernet con DHCP... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo Ethernet con DHCP... KO",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
    return risultatoConnessioneEthernet;
}

void EthernetCore::mantieneConnessione() {
    // Copiato da codice esempio di Arduino
    switch (Ethernet.maintain()) {
        case 1:
            buzzer.beepAvviso();
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Rinnovo Ethernet fallito: tentativo n° 1",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

            // Se il rinnovo della connessione Ethernet è fallita, riprovo varie volte prima di fermare il programma
            for (int i = 0; i < NUMERO_TENTATIVI_MANTENIMENTO_CONNESSIONE_ETHERNET; i++) {
                delay.milliseconds(INTERVALLO_TRA_TENTATIVI_MANTENIMENTO_CONNESSIONE_ETHERNET_MILLISECONDI);

                char buffer[10];
                itoa(i + 2, buffer, 10);

                int risultato = Ethernet.maintain();

                if (risultato == 0) {
                    buzzer.beepOk();
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Rinnovo Ethernet riuscito: tentativo n° ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, buffer,
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                    break;
                } else {
                    buzzer.beepAvviso();
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Rinnovo Ethernet fallito. Tentativo n° ",
                                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, buffer,
                                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                }
            }
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Rinnovo Ethernet fallito",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            break;

        case 2:
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Rinnovo Ethernet successo",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            stampaIp();
            break;

        case 3:
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Rebind Ethernet fallito",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            break;

        case 4:
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Rebind Ethernet eseguito con successo",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            stampaIp();
            break;

        default:
            // Non faccio nulla
            break;
    }
    if (Ethernet.linkStatus() != LinkON) {

        buzzer.beepAvviso();
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Connessione Ethernet interrotta.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

        for (int i = 0; i < NUMERO_TENTATIVI_RICONNESSIONE_ETHERNET; i++) {
            char buffer[10];
            itoa(i + 1, buffer, 10);

            buzzer.beepAvviso();
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Tentativo riconnessione Ethernet n° ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, buffer,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "...",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            loggerReale.cambiaTabulazione(1);

            // Provo a reinizializzare la connessione Ethernet
            bool risultatoInizializzazione = inizializza(false);

            loggerReale.cambiaTabulazione(-1);

            if (!risultatoInizializzazione) {
                buzzer.beepAvviso();
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Tentativo riconnessione Ethernet n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, buffer,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "... FALLITA",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

            } else {
                buzzer.beepOk();
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "Tentativo riconnessione Ethernet n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, buffer,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "... OK",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                return;
            }
        }

        // Se non sono riuscito a ripristinare la connessione Ethernet dopo vari tentativi, fermo il programma
#ifdef ARDUINO_MASTER
        displayReale.errori.ethernet.connessionePersa();
        programmaReale.ferma(false);
#endif

#ifdef ARDUINO_LED
        programmaReale.ferma();
#endif
    }
}

void EthernetCore::stampaIp() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                          "Indirizzo IP: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
#ifdef ARDUINO_ARM
    String ipStringa = Ethernet.localIP().toString();
    // Copio l'indirizzo IP nella variabile indirizzoIp affinché sia accessibile per poterlo stampare dal menù
    // display/keypad
    ipStringa.toCharArray(indirizzoIp, sizeof(indirizzoIp));

#else
    // Il metodo toString non è disponibile per AVR
    IPAddress ip = Ethernet.localIP();
    String ipStringa = String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);
#endif

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, ipStringa.c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
}

// *** DEFINIZIONE VARIABILI *** //

#ifdef ARDUINO_MASTER
EthernetCore ethernetCore(buzzerRealeArduinoMaster, delayReale);
#elif ARDUINO_LED
EthernetCore ethernetCore(buzzerRealeArduinoLed, delayReale);
#elif ARDUINO_SLAVE_RELAY
EthernetCore ethernetCore(buzzerRealeArduinoSlaveRelay, delayReale);
#elif ARDUINO_SLAVE_SENSORI
EthernetCore ethernetCore(buzzerRealeArduinoSlaveSensori, delayReale);
#endif
