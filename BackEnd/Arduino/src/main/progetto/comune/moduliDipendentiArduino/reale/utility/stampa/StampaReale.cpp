// *** INCLUDE *** //

#include "StampaReale.h"

// *** DEFINIZIONE METODI *** //

void StampaReale::print(const char* messaggio) {
    Serial.print(messaggio);
}

void StampaReale::print(double numero, int cifreDecimali) {
    Serial.print(numero, cifreDecimali);
    char buffer[10];
    sprintf(buffer, "%f", numero);
}

void StampaReale::print(int numero) {
    Serial.print(numero);
    char buffer[10];
    sprintf(buffer, "%d", numero);
}

void StampaReale::print(char carattere) {
    Serial.print(carattere);
}

void StampaReale::print(double numero) {
    Serial.print(numero);
    char buffer[10];
    sprintf(buffer, "%f", numero);
}

void StampaReale::print(unsigned long numero) {
    Serial.print(numero);
    char buffer[10];
    sprintf(buffer, "%lu", numero);
}

void StampaReale::print(unsigned int numero) {
    Serial.print(numero);
    char buffer[10];
    sprintf(buffer, "%u", numero);
}

// *** DEFINIZIONE VARIABILI *** //

StampaReale stampaReale;