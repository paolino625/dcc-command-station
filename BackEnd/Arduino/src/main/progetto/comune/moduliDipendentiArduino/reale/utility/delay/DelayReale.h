#ifndef DCC_COMMAND_STATION_DELAYREALE_H
#define DCC_COMMAND_STATION_DELAYREALE_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/delay/DelayAbstract.h"

// *** CLASSE *** //

class DelayReale : public DelayAbstract {
   public:
    void milliseconds(unsigned long ms) override;
    void microseconds(unsigned long us) override;
};

// *** DICHIARAZIONE VARIABILI *** //

extern DelayReale delayReale;

#endif