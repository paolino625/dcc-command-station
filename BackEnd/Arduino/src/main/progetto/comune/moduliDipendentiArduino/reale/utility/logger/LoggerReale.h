#ifndef ARDUINO_LOGGERREALE_H
#define ARDUINO_LOGGERREALE_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern LoggerAbstract loggerReale;

#endif
