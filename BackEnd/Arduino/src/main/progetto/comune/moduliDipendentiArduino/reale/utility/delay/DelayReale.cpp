// *** INCLUDE *** //

#include "DelayReale.h"
#include "Arduino.h"

// *** DEFINIZIONE METODI *** //

void DelayReale::milliseconds(unsigned long ms) {
    delay(ms);
}

void DelayReale::microseconds(unsigned long us) {
    delayMicroseconds(us);
}

// *** DEFINIZIONE VARIABILI *** //

DelayReale delayReale;