#ifndef BACKEND_CONNESSIONEETHERNETCORE_H
#define BACKEND_CONNESSIONEETHERNETCORE_H

/*
Circuit:
Ethernet shield attached to pins 10, 11, 12, 13
*/

// *** INCLUDE *** //

#include <SPI.h>

#include "Ethernet.h"

#ifdef ARDUINO_MASTER
#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/TipiPrimitiviArduino.h"
#endif
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/BuzzerAbstract.h"

// *** DEFINE *** //

#define TIMEOUT_CONNESSIONE_ETHERNET_MILLIS 10000

#define NUMERO_TENTATIVI_MANTENIMENTO_CONNESSIONE_ETHERNET 10
#define INTERVALLO_TRA_TENTATIVI_MANTENIMENTO_CONNESSIONE_ETHERNET_MILLISECONDI 100

#define NUMERO_TENTATIVI_RICONNESSIONE_ETHERNET 3

// *** DICHIARAZIONE VARIABILI *** //

extern byte indirizzoMac[];

// *** DEFINIZIONE METODI *** //

class EthernetCore {
    // *** VARIABILI *** //

    BuzzerAbstract &buzzer;
    DelayAbstract &delay;

    // *** COSTRUTTORE *** //

   public:
    EthernetCore(BuzzerAbstract &buzzer, DelayAbstract &delay) : buzzer(buzzer), delay{delay} {}

    // *** METODI *** //

   public:
    static bool inizializza(bool fermaProgramma);
    void mantieneConnessione();
    static void stampaIp();
};

// *** DICHIARAZIONE VARIABILI *** //

extern EthernetCore ethernetCore;

#endif
