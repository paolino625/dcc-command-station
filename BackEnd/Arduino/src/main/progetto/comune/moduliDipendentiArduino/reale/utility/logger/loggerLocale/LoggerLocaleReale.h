#ifndef ARDUINO_LOGGERLOCALEREALE_H
#define ARDUINO_LOGGERLOCALEREALE_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/loggerLocale/LoggerLocaleAbstract.h"

// *** DICHIARAZIONE VARIABILI *** //

extern LoggerLocaleAbstract loggerLocaleReale;

#endif
