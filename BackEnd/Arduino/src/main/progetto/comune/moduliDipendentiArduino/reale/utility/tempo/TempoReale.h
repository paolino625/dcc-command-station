#ifndef DCC_COMMAND_STATION_TEMPOREALE_H
#define DCC_COMMAND_STATION_TEMPOREALE_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/tempo/TempoAbstract.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class TempoReale : public TempoAbstract {
    // *** DICHIARAZIONE METODI *** //

   public:
    TimestampTipo milliseconds() override { return millis(); }
    TimestampTipo microseconds() override { return micros(); }
};

// *** DICHIARAZIONE VARIABILI *** //

extern TempoReale tempoReale;

#endif