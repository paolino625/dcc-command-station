#ifndef DCC_COMMAND_STATION_DIGITALPINREALE_H
#define DCC_COMMAND_STATION_DIGITALPINREALE_H

// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/pin/DigitalPinAbstract.h"

// Questa è l'implementazione reale di DigitalPin

// Estendo DigitalPinAbstract
class DigitalPinReale final : public DigitalPinAbstract {
    // *** VARIABILI *** //
   private:
    const uint8_t id;

    // *** COSTRUTTORE *** //

   public:
    explicit DigitalPinReale(const uint8_t id) : id{id} {}

    // *** DEFINIZIONE METODI *** //

    [[nodiscard]] auto getId() const -> uint8_t { return id; }

    auto setOutputMode() -> void override { pinMode(id, OUTPUT); }

    auto setInputMode() -> void override { pinMode(id, INPUT); }

    auto setInputPullupMode() -> void override { pinMode(id, INPUT_PULLUP); }

    auto setHigh() -> void override { digitalWrite(id, HIGH); }

    auto setLow() -> void override { digitalWrite(id, LOW); }

    [[nodiscard]] auto isHigh() const -> bool override { return digitalRead(id) == HIGH; }

    [[nodiscard]] auto isLow() const -> bool override { return digitalRead(id) == LOW; }

    [[nodiscard]] auto legge() const -> bool override { return digitalRead(id); }
};

static_assert(!std::is_abstract<DigitalPinReale>());

#endif
