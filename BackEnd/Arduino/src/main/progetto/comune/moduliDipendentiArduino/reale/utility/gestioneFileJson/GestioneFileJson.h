#ifndef DCC_COMMAND_STATION_GESTIONEFILEJSON_H
#define DCC_COMMAND_STATION_GESTIONEFILEJSON_H

// *** INCLUDE *** //

#include "ArduinoJson.h"

#ifdef ARDUINO_ARM
#include "main/progetto/arm/comune/moduliIndipendentiArduino/utility/NumeroCaratteriFileJsonBuffer.h"
#elif ARDUINO_AVR
#include "main/progetto/avr/arduinoSlave/comune/reale/NumeroCaratteriFileJsonBuffer.h"
#endif

// *** DICHIARAZIONE VARIABILI *** //

extern char stringaJsonUsbBuffer[NUMERO_CARATTERI_FILE_JSON_BUFFER];

// *** DICHIARAZIONE FUNZIONI *** //

void inizializzoStringa(char *stringa, unsigned int numeroCaratteriBuffer);
void serializzoJson(JsonDocument &documentoJson, char *stringaJson, int numeroCaratteriStringa, bool loggerAttivo);
bool deserializzoJson(JsonDocument &documentoJson, char *stringaJson, bool fermaProgrammaInCasoDiErrore);

#endif
