#ifndef DCC_COMMAND_STATION_PORTE_SERIALI_H
#define DCC_COMMAND_STATION_PORTE_SERIALI_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/comune/reale/PingComune.h"

// *** DEFINE *** //

/* Definisco velocità porte seriali.
Più di 115200 il segnale diventa instabile
*/
#define VELOCITA_PORTA_SERIALE_COMPUTER 115200

#define VELOCITA_PORTA_SERIALE_ARDUINO 9600

// Definisco il numero dei caratteri dei messaggi scambiati con Arduino Slave Sensori IR (sia in invio che ricezione)
#define NUMERO_CARATTERI_MESSAGGIO_ARDUINO 10

/*
Lo settiamo uguale così se Arduino Slave Sensori non riesce a inviare il messaggio di un sensore azionato perché
proprio in quel momento la connessione viene persa, Arduino Master riceve comunque entro il tempo limite il messaggio
per fare il reset della porta seriale senza crashare
*/
#define ATTESA_ACK INTERVALLO_INVIO_PACCHETTO_ALIVE_ARDUINO_MASTER

#endif