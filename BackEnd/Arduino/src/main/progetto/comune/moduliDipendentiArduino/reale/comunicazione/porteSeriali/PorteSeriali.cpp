// *** INCLUDE *** //

#include "PorteSeriali.h"

#include "Arduino.h"
#include "InfoPorteSeriali.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/reale/comunicazione/seriale/Ping.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE FUNZIONI *** //

void stampoLogInizializzazionePortaSeriale(byte numeroPortaSeriale, VelocitaPorta velocitaPorta) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo porta seriale ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    // Non posso usare std::to_string perché questa classe deve essere compatibile sia con Arduino AVR che con Arduino
    // ARM
    char buffer[10];
    sprintf(buffer, "%d", numeroPortaSeriale);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " con baud rate ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    sprintf(buffer, "%ld", velocitaPorta);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);
}

void stampoLogInizializzazionePortaSerialeOk(byte numeroPortaSeriale, VelocitaPorta velocitaPorta) {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo porta seriale ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    char buffer[10];
    sprintf(buffer, "%d", numeroPortaSeriale);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " con baud rate ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    sprintf(buffer, "%ld", velocitaPorta);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... OK",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
}

void stampoLogInizializzazionePortaSerialeArduinoMegaOk(byte numeroPortaSeriale, VelocitaPorta velocitaPorta) {}

bool inizializzoPortaSerialeArduinoGigaArduinoMega(byte numeroPortaSeriale, VelocitaPorta velocitaPorta,
                                                   bool portaSerialeDaVerificare) {
    // Se devo inizializzare la porta 0, non posso ancora usare la Serial.print
    if (numeroPortaSeriale != 0) {
        stampoLogInizializzazionePortaSeriale(numeroPortaSeriale, velocitaPorta);
    }

    if (numeroPortaSeriale == 0) {
        Serial.begin(velocitaPorta);
        delay(1000);  // Aspetto che la porta seriale si avvii.

        stampoLogInizializzazionePortaSeriale(numeroPortaSeriale, velocitaPorta);
        stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);

    } else if (numeroPortaSeriale == 1) {
        Serial1.begin(velocitaPorta);
        delay(1000);  // Aspetto che la porta seriale si avvii.

        if (portaSerialeDaVerificare) {
            if (verificoPortaSerialeArduinoGigaArduinoMaster(1)) {
                stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);
                return true;
            } else {
                Serial1.end();
                return false;
            }
        } else {
            stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);
        }
        Serial1.setTimeout(TIMEOUT_PORTA_SERIALE);
    } else if (numeroPortaSeriale == 2) {
        Serial2.begin(velocitaPorta);
        delay(1000);  // Aspetto che la porta seriale si avvii.

        if (portaSerialeDaVerificare) {
            if (verificoPortaSerialeArduinoGigaArduinoMaster(2)) {
                stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);
                return true;
            } else {
                Serial2.end();
                return false;
            }
        } else {
            stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);
        }
        Serial2.setTimeout(TIMEOUT_PORTA_SERIALE);
    } else if (numeroPortaSeriale == 3) {
        Serial3.begin(velocitaPorta);
        delay(1000);  // Aspetto che la porta seriale si avvii.

        if (portaSerialeDaVerificare) {
            if (verificoPortaSerialeArduinoGigaArduinoMaster(3)) {
                stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);
                return true;
            } else {
                Serial3.end();
                return false;
            }
        } else {
            stampoLogInizializzazionePortaSerialeOk(numeroPortaSeriale, velocitaPorta);
        }
        Serial3.setTimeout(TIMEOUT_PORTA_SERIALE);
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Numero porta seriale inesistente.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        return false;
    }
    return true;
}

void chiudoPortaSerialeArduinoGigaArduinoMaster(byte numeroPortaSeriale) {
    if (numeroPortaSeriale == 0) {
        Serial.end();

    } else if (numeroPortaSeriale == 1) {
        Serial1.end();
    } else if (numeroPortaSeriale == 2) {
        Serial2.end();

    } else if (numeroPortaSeriale == 3) {
        Serial3.end();
    } else {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "Numero porta seriale inesistente.",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

bool verificoPortaSerialeArduinoGigaArduinoMaster(byte numeroPortaSeriale) {
    char stringaDaInviare[NUMERO_CARATTERI_MESSAGGIO_ARDUINO] = "ping";
    int lunghezzaStringaDaInviare = sizeof(stringaDaInviare) - 1;  // Sottrai 1 per escludere il carattere nullo

    if (numeroPortaSeriale == 1) {
        Serial1.write(stringaDaInviare, lunghezzaStringaDaInviare);
    } else if (numeroPortaSeriale == 2) {
        Serial2.write(stringaDaInviare, lunghezzaStringaDaInviare);
    } else if (numeroPortaSeriale == 3) {
        Serial3.write(stringaDaInviare, lunghezzaStringaDaInviare);
    }

    TimestampTipo tempoInizio = millis();
    char stringaRicezione[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];

    while (millis() - tempoInizio < ATTESA_MASSIMA_RICEZIONE_PING_ARDUINO_SLAVE_RELAY) {
        // Leggi la stringa dalla porta seriale utilizzando Serial.readBytes()

        if (numeroPortaSeriale == 0) {
            if (Serial.available() > 0) {
                Serial.readBytes(stringaRicezione, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
            }
        } else if (numeroPortaSeriale == 1) {
            if (Serial1.available() > 0) {
                Serial1.readBytes(stringaRicezione, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
            }
        } else if (numeroPortaSeriale == 2) {
            if (Serial2.available() > 0) {
                Serial2.readBytes(stringaRicezione, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
            }
        } else if (numeroPortaSeriale == 3) {
            if (Serial3.available() > 0) {
                Serial3.readBytes(stringaRicezione, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
            }
        }

        // Se ricevo il messaggio di risposta al ping, esco dalla funzione senza aspettare il timer
        if (strcmp(stringaRicezione, "ping") == 0) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Ping ricevuto",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            return true;
        }
    }

    // Se sono arrivato qui, significa che non ho ricevuto una risposta, dunque il dispositivo non è
    // collegato alla porta seriale.
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR,
                          "Non ricevo risposta dal dispositivo collegato alla porta seriale ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);

    char buffer[10];
    sprintf(buffer, "%d", numeroPortaSeriale);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::ERROR, "!", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    return false;
}

bool inizializzoPortaSerialeArduinoMinima(VelocitaPorta velocitaPorta) {
#ifdef ARDUINO_LED
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo porta seriale ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " con baud rate ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, std::to_string(velocitaPorta).c_str(),
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);

    Serial.begin(velocitaPorta);
    delay(1000);
#endif
}
