// *** INCLUDE *** //

#include "LoggerLocaleReale.h"

#include "main/progetto/arm/arduinoMaster/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/stampa/StampaReale.h"

// *** DEFINIZIONE VARIABILI *** //

LoggerLocaleAbstract loggerLocaleReale = LoggerLocaleAbstract(livelloLogArduino, stampaReale);