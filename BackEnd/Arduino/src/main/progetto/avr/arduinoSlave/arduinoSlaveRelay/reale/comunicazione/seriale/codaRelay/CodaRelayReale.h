#ifndef ARDUINO_CODARELAYREALE_H
#define ARDUINO_CODARELAYREALE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/abstract/comunicazione/seriale/codaRelay/CodaRelay.h"

// *** DICHIARAZIONE VARIABILI *** //

extern CodaRelay codaRelayReale;

#endif
