// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/relay/RelaysReale.h"

// *** DEFINIZIONE FUNZIONI *** //

void testSingoloRelay() { relaysReale.azionaRelay(NUMERO_RELAY_TEST); }

void testTuttiRelay() {
    for (int i = 1; i < 241; i++) {
        relaysReale.azionaRelay(i);
        // delay(50);
    }
}
