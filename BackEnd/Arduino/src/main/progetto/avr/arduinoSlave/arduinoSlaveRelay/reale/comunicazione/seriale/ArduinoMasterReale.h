#ifndef ARDUINO_ARDUINOMASTERREALE_H
#define ARDUINO_ARDUINOMASTERREALE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/abstract/comunicazione/seriale/ArduinoMaster.h"

// *** DICHIARAZIONE VARIABILI *** //

extern ArduinoMaster arduinoMasterReale;

#endif
