// *** INCLUDE *** //

#include "MultiplexerMaster.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/pin/Pin.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/MultiplexerCore.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void MultiplexerMaster::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo multiplexer master... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    multiplexerCore.inizializza();
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo multiplexer master... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

// *** DEFINIZIONE VARIABILI *** //

TipologiaMultiplexer tipologiaMultiplexerMaster = {TipologiaPin::TIPOLOGIA_OUTPUT, ModalitaPin::DIGITALE, false};

MultiplexerCore multiplexerMasterCore =
    MultiplexerCore(pinSceltaMultiplexerMaster, pinSegnaleMultiplexerMaster, tipologiaMultiplexerMaster);
MultiplexerMaster multiplexerMasterReale = MultiplexerMaster(multiplexerMasterCore);
