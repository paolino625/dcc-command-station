#ifndef ARDUINO_ARDUINOMASTER_ARDUINO_SLAVE_RELAY_H
#define ARDUINO_ARDUINOMASTER_ARDUINO_SLAVE_RELAY_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class ArduinoMaster {
    // *** DICHIARAZIONE VARIABILI *** //

    TimestampTipo timestampUltimoInvioPacchettoAliveArduinoMaster;

    // *** DEFINIZIONE METODI *** //

   public:
    static void riceve();
    static void inviaAck(const char *stringaRicevuta);
    void inviaHealthCheck();

   private:
    static void inviaMessaggioHealthCheck();
};

#endif
