// *** INCLUDE *** //

#include "EnvironmentConfigProd.h"

#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

#ifdef PROD

// *** DEFINIZIONE VARIABILI *** //

LivelloLog livelloLogArduino = LivelloLog::DEBUG;

#endif