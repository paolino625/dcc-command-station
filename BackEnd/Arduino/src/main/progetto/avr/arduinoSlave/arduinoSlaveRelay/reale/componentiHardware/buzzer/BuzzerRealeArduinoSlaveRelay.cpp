// *** INCLUDE *** //

#include "BuzzerRealeArduinoSlaveRelay.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/pin/Pin.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/AnalogPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AnalogPinReale pinBuzzerArduinoSlaveRelay(PIN_BUZZER_ARDUINO_SLAVE_RELAY);
BuzzerAbstractReale buzzerRealeArduinoSlaveRelay(pinBuzzerArduinoSlaveRelay, delayReale, loggerReale);