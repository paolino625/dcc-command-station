#ifndef DCC_COMMAND_STATION_PIN_ARDUINO_SLAVE_RELAY_H
#define DCC_COMMAND_STATION_PIN_ARDUINO_SLAVE_RELAY_H

// *** DEFINE *** //

// Inizializzo Porta Seriale 0 che corrisponde alla porta USB-C
#define PORTA_SERIALE_COMPUTER 0

// Inizializzo Porta Seriale 1 che corrisponde a RX1/TX1 per comunicazione con Arduino Master
#define PORTA_SERIALE_ARDUINO_MASTER 1

#define PIN_BUZZER_ARDUINO_SLAVE_RELAY 11

// *** DICHIARAZIONE VARIABILI *** //

extern int pinSceltaMultiplexerMaster[4];
extern int pinSegnaleMultiplexerMaster;

extern int pinSceltaMultiplexerSlave[][4];

#endif