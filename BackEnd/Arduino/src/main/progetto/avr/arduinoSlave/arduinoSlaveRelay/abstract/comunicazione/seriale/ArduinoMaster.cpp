// *** INCLUDE *** //

#include "ArduinoMaster.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/comunicazione/seriale/codaRelay/CodaRelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void ArduinoMaster::riceve() {
    if (Serial1.available() > 0) {
        // Ogni volta che ricevo un messaggio resetto il timestamp, utile per accertarmi che Arduino
        // Mega sia attivo e il segnale Interrupt sia valido.
        char stringaRicevuta[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
        Serial1.readBytes(stringaRicevuta, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

        if (DEBUGGING_MESSAGGI_DA_ARDUINO_MASTER) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Ricevuta stringa da Arduino Master: ",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, stringaRicevuta,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
        }

        // Vediamo se è un ordine per azionare relay
        if (strncmp(stringaRicevuta, "relay", 5) == 0) {
            if (DEBUGGING_MESSAGGI_ARDUINO_MASTER_SOLO_RELAY) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Ho ricevuto delle info relay!",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
            }
            inviaAck(stringaRicevuta);

            // Utilizza strtok per ottenere le sottostringhe
            strtok(stringaRicevuta, " ");  // Elimino parola relay

            // Converto la sottostringa in numero intero
            char *str1 = strtok(NULL, " ");
            int numeroRelay = atoi(str1);

            codaRelayReale.aggiungeRelay(numeroRelay);
        }
    }
}

void ArduinoMaster::inviaMessaggioHealthCheck() {
    char stringaDaInviare[NUMERO_CARATTERI_MESSAGGIO_ARDUINO] = "ping";
    Serial1.write(stringaDaInviare, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
}

void ArduinoMaster::inviaAck(const char *stringaRicevuta) {
    // Rinvio stringa che ho ricevuto come ACK

    if (DEBUGGING_ACK) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Invio ACK ad Arduino Master: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, stringaRicevuta,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "... ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
    Serial1.write(stringaRicevuta, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
    if (DEBUGGING_ACK) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Invio ACK ad Arduino Master: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                              false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, stringaRicevuta,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void ArduinoMaster::inviaHealthCheck() {
    if (DEBUGGING_HEALTH_CHECK) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio health check ad Arduino Master...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }

    if (millis() - timestampUltimoInvioPacchettoAliveArduinoMaster > INTERVALLO_INVIO_PACCHETTO_ALIVE_ARDUINO_MASTER) {
        digitalWrite(LED_BUILTIN, HIGH);
        inviaMessaggioHealthCheck();
        timestampUltimoInvioPacchettoAliveArduinoMaster = millis();
        digitalWrite(LED_BUILTIN, LOW);
    }

    if (DEBUGGING_HEALTH_CHECK) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio health check ad Arduino Master... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}