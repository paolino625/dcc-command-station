// *** INCLUDE *** //

#include "CodaRelay.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/relay/RelaysReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE METODI *** //

void CodaRelay::controlla() {
    // Se c'è almeno un relay in coda allora entro nell'IF
    if (indiceCodaRelayAttuale != indiceCodaRelaySalvataggio) {
        TimestampTipo millisCorrenteAzionoRelay = millis();
        // Deve essere passato abbastanza tempo dall'ultimo azionamento del relay per consentire la
        // ricaricare della CDU. In tal caso procedo ad azionare il relay successivo.
        if (millisCorrenteAzionoRelay - millisPrecedenteAzionoRelay >= TEMPO_RICARICA_CDU) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "C'è almeno un relay in coda!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            millisPrecedenteAzionoRelay = millisCorrenteAzionoRelay;
            relaysReale.azionaRelay(codaNumeroRelay[indiceCodaRelayAttuale]);
            indiceCodaRelayAttuale += 1;
            if (indiceCodaRelayAttuale == NUMERO_ELEMENTI_CODA_RELAY) {
                indiceCodaRelayAttuale = 0;
            }
        }
    }
}

void CodaRelay::aggiungeRelay(int numeroRelay) {
    char buffer[20];

    codaNumeroRelay[indiceCodaRelaySalvataggio] = numeroRelay;
    indiceCodaRelaySalvataggio += 1;
    if (indiceCodaRelaySalvataggio == NUMERO_ELEMENTI_CODA_RELAY) {
        indiceCodaRelaySalvataggio = 0;
    }

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Aggiungo relay n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    itoa(numeroRelay, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " alla posizione ",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);

    itoa(indiceCodaRelaySalvataggio, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " della coda... OK",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
}