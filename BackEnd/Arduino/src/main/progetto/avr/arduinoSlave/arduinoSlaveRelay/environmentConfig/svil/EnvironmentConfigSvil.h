#ifndef ARDUINO_ENVIRONMENTCONFIGSVIL_H
#define ARDUINO_ENVIRONMENTCONFIGSVIL_H

// *** INCLUDE *** //

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

#ifdef SVIL

// *** COMUNICAZIONE *** //

#define ETHERNET_PRESENTE 1

// *** DEBUGGING *** //

#define DEBUGGING_ACK 0

#define DEBUGGING_HEALTH_CHECK 0

#define DEBUGGING_MESSAGGI_DA_ARDUINO_MASTER 0

#define DEBUGGING_MESSAGGI_ARDUINO_MASTER_SOLO_RELAY 0

#define DEBUGGING_MQTT 1

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

// Se ad 1, tutti i relay vengono azionati uno dopo l'altro
#define TEST_TUTTI_RELAY 0

#define TEST_SINGOLO_RELAY 0
#define NUMERO_RELAY_TEST 1

// Mi assicuro che non siano definiti entrambe le modalità di test a 1
#if (TEST_TUTTI_RELAY == 1) && (TEST_SINGOLO_RELAY == 1)
#error "Solo una delle macro TEST_TUTTI_RELAY o TEST_SINGOLO_RELAY può essere impostata a 1."

#endif

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

#define PUBBLICAZIONE_LOG 1

// *** MQTT *** //

#define MQTT_ATTIVO 1
// Mi assicuro che se Ethernet non è attivo, MQTT non possa essere attivo
#if (ETHERNET_PRESENTE == 0) && (MQTT_ATTIVO == 1)
#error "Se Ethernet non è presente, MQTT non può essere attivo."
#endif

#endif

#endif
