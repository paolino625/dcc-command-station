// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/abstract/multiplexer/multiplexersSlave/MultiplexersSlave.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/pin/Pin.h"

// *** DEFINIZIONE VARIABILI *** //

TipologiaMultiplexer tipologiaMultiplexerSlave = {TipologiaPin::TIPOLOGIA_OUTPUT, ModalitaPin::DIGITALE, false};

MultiplexerCore multiplexers[NUMERO_MULTIPLEXER_SLAVE] = {
    // I pin di segnale a 0 perché non sono collegati ad Arduino ma sono collegati alla Multiplexer Master
    MultiplexerCore(0, 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[1], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[2], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[3], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[4], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[5], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[6], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[7], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[8], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[9], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[10], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[11], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[12], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[13], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[14], 0, tipologiaMultiplexerSlave),
    MultiplexerCore(pinSceltaMultiplexerSlave[15], 0, tipologiaMultiplexerSlave)};

MultiplexersSlave multiplexersSlaveReale = MultiplexersSlave(multiplexers);