#ifndef ARDUINO_CODARELAY_H
#define ARDUINO_CODARELAY_H

// *** INCLUDE *** //

#include "Arduino.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

#define NUMERO_ELEMENTI_CODA_RELAY 50

// Millisecondi per i quali il relay viene eccitato
#define TEMPO_RELAY_ON 200  // 20 ok per azionare uno scambio

// Millisecondi per ricaricare la CDU (condensatori che alimentano i solenoidi dello scambio)
#define TEMPO_RICARICA_CDU 150

// *** CLASSE *** //

class CodaRelay {
    // *** DICHIARAZIONE VARIABILI *** //

    TimestampTipo millisPrecedenteAzionoRelay;

    // Coda per memorizzare le richieste di azionamento dei vari relay
    byte codaNumeroRelay[NUMERO_ELEMENTI_CODA_RELAY];

    byte indiceCodaRelayAttuale;
    byte indiceCodaRelaySalvataggio;

    // *** DICHIARAZIONE METODI *** //

   public:
    void controlla();
    void aggiungeRelay(int numeroRelay);
};

#endif
