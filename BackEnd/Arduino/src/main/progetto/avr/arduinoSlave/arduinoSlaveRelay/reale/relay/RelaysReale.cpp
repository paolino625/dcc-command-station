// *** INCLUDE *** //

#include "RelaysReale.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/multiplexer/multiplexerMaster/MultiplexerMaster.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/multiplexer/multiplexersSlave/MultiplexersSlaveReale.h"

// *** DEFINIZIONE VARIABILI *** //

Relays relaysReale = Relays(multiplexerMasterReale, multiplexersSlaveReale);