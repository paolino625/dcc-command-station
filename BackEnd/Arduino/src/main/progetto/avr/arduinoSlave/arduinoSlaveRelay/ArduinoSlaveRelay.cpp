// *** ARDUINO SLAVE RELAY *** //

#include <Arduino.h>
#include <MQTT.h>

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/comunicazione/seriale/ArduinoMasterReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/comunicazione/seriale/codaRelay/CodaRelayReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/multiplexer/multiplexerMaster/MultiplexerMaster.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/multiplexer/multiplexersSlave/MultiplexersSlaveReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/pin/Pin.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/test/TestRelay.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/comunicazione/mqtt/mqttCore/MqttCoreArduinoAvr.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/ethernet/core/EthernetCore.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/PorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DICHIARAZIONE FUNZIONI *** //

void setup() {
    // Durante inizializzazione delle porte seriali non richiedo la verifica perché è Arduino Master
    // a richiederle
    inizializzoPortaSerialeArduinoGigaArduinoMega(PORTA_SERIALE_COMPUTER, VELOCITA_PORTA_SERIALE_COMPUTER, false);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "ARDUINO SLAVE RELAY",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    if (ETHERNET_PRESENTE) {
        ethernetCore.inizializza(true);
    }

    if (MQTT_ATTIVO) {
        mqttCore.inizializza();
    }

    inizializzoPortaSerialeArduinoGigaArduinoMega(PORTA_SERIALE_ARDUINO_MASTER, VELOCITA_PORTA_SERIALE_ARDUINO, false);

    multiplexerMasterReale.inizializza();
    multiplexersSlaveReale.inizializza();

    // Inizializzo LED integrato
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    if (TEST_TUTTI_RELAY) {
        testTuttiRelay();
    } else if (TEST_SINGOLO_RELAY) {
        testSingoloRelay();
    } else {
        if (ETHERNET_PRESENTE) {
            ethernetCore.mantieneConnessione();
        }

        // Invio ping se è passato del tempo
        arduinoMasterReale.inviaHealthCheck();

        arduinoMasterReale.riceve();

        codaRelayReale.controlla();  // Controllo la coda dei relay e se c'è qualche relay da azionare, lo aziono
    }
}
