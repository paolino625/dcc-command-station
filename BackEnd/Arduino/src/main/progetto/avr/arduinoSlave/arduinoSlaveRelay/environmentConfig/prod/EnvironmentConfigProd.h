#ifndef ARDUINO_ENVIRONMENTCONFIGPROD_H
#define ARDUINO_ENVIRONMENTCONFIGPROD_H

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"

#ifdef PROD

// *** COMUNICAZIONE *** //

#define ETHERNET_PRESENTE 1

// *** DEBUGGING *** //

#define DEBUGGING_ACK 0

#define DEBUGGING_HEALTH_CHECK 0

#define DEBUGGING_MESSAGGI_DA_ARDUINO_MASTER 0

#define DEBUGGING_MESSAGGI_ARDUINO_MASTER_SOLO_RELAY 0

#define DEBUGGING_MQTT 0

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

// Se ad 1, tutti i relay vengono azionati uno dopo l'altro
#define TEST_TUTTI_RELAY 0

#define TEST_SINGOLO_RELAY 0
#define NUMERO_RELAY_TEST 1

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

// *** MQTT *** //

#define MQTT_ATTIVO 1

#endif

#endif
