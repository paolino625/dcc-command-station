#ifndef ARDUINO_MULTIPLEXERSSLAVEREALE_H
#define ARDUINO_MULTIPLEXERSSLAVEREALE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/abstract/multiplexer/multiplexersSlave/MultiplexersSlave.h"

// *** DICHIARAZIONE VARIABILI *** //

extern MultiplexersSlave multiplexersSlaveReale;

#endif
