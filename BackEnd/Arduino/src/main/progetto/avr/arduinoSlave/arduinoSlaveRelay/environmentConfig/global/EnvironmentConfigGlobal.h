#ifndef ARDUINO_ENVIRONMENTCONFIGGLOBAL_H
#define ARDUINO_ENVIRONMENTCONFIGGLOBAL_H

// *** VARIABILI GLOBALI *** //

// TODO Da controllare
// L'indirizzo MAC non è essenziale per stabilire la connessione, ma sarà semplicemente il MAC che la scheda di rete presenterà al router.
#define INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_SLAVE_RELAY {0xA8, 0x61, 0x0A, 0xAE, 0x0A, 0x32}

#endif
