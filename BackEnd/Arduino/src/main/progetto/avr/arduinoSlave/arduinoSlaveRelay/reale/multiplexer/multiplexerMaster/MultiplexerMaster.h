#ifndef ARDUINO_MULTIPLEXERMASTER_H
#define ARDUINO_MULTIPLEXERMASTER_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/MultiplexerCore.h"

// *** CLASSE *** //

class MultiplexerMaster {
    // *** VARIABILI *** //

   public:
    MultiplexerCore& multiplexerCore;

    // *** COSTRUTTORE *** //

   public:
    MultiplexerMaster(MultiplexerCore& multiplexerMasterReale) : multiplexerCore{multiplexerMasterReale} {}

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
};

// *** DICHIARAZIONE VARIABILI *** //

extern MultiplexerMaster multiplexerMasterReale;

#endif
