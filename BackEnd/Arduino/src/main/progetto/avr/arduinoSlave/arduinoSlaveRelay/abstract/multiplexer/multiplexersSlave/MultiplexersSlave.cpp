// *** INCLUDE *** //

#include "MultiplexersSlave.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/infoMultiplexer/InfoMultiplexer.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void MultiplexersSlave::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo multiplexer slave... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_MULTIPLEXER_SLAVE; i++) {
        MultiplexerCore* multiplexerSlave = getMultiplexer(i);
        multiplexerSlave->inizializza();
    }
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo multiplexer slave... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MultiplexersSlave::selezionaPinMultiplexer(int numeroMultiplexer, int numeroIngresso) {
    MultiplexerCore* multiplexerSlave = getMultiplexer(numeroMultiplexer);
    digitalWrite(multiplexerSlave->pinSceltaUscita[0], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][0]);
    digitalWrite(multiplexerSlave->pinSceltaUscita[1], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][1]);
    digitalWrite(multiplexerSlave->pinSceltaUscita[2], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][2]);
    digitalWrite(multiplexerSlave->pinSceltaUscita[3], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][3]);
}

int MultiplexersSlave::calcolaNumeroMultiplexer(int numeroRelay) {
    int numeroMultiplexer = ((numeroRelay - 1) / (NUMERO_INGRESSI_MULTIPLEXER - 1)) + 1;
    return numeroMultiplexer;
}

int MultiplexersSlave::calcolaNumeroPinMultiplexer(int numeroRelay) {
    // Ottengo il numero pin multiplexer
    int numeroPinMultiplexer = (numeroRelay - 1) % (NUMERO_INGRESSI_MULTIPLEXER - 1) + 1;
    return numeroPinMultiplexer;
}

MultiplexerCore* MultiplexersSlave::getMultiplexer(int numeroMultiplexer) {
    return multiplexerSlaveArray[numeroMultiplexer];
}
