// *** INCLUDE *** //

#include "Relays.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/abstract/comunicazione/seriale/codaRelay/CodaRelay.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE FUNZIONI *** //

void Relays::azionaRelay(int numeroRelay) {
    char buffer[10];

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Aziono relay n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    itoa(numeroRelay, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          true);

    int numeroMultiplexer = multiplexersSlave.calcolaNumeroMultiplexer(numeroRelay);
    int numeroPinMultiplexer = multiplexersSlave.calcolaNumeroPinMultiplexer(numeroRelay);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                          "Numero multiplexer: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    itoa(numeroMultiplexer, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, buffer,
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                          "Numero pin multiplexer: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
    itoa(numeroPinMultiplexer, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, buffer,
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    // Abilito uscita multiplexer relativa a multiplexer slave scelta

    multiplexerMaster.multiplexerCore.selezionaPin(numeroPinMultiplexer);

    itoa(numeroRelay, buffer, 10);

    accendeRelay(numeroRelay);
    delay(TEMPO_RELAY_ON);
    spegneRelay(numeroRelay);
}

void Relays::accendeRelay(int numeroRelay) {
    int numeroMultiplexer = multiplexersSlave.calcolaNumeroMultiplexer(numeroRelay);
    int numeroPinMultiplexer = multiplexersSlave.calcolaNumeroPinMultiplexer(numeroRelay);

    multiplexersSlave.selezionaPinMultiplexer(numeroMultiplexer, numeroPinMultiplexer);

    multiplexerMaster.multiplexerCore.abilitaSegnale();
}

void Relays::spegneRelay(int numeroRelay) {
    int numeroMultiplexer = multiplexersSlave.calcolaNumeroMultiplexer(numeroRelay);
    int numeroPinMultiplexer = multiplexersSlave.calcolaNumeroPinMultiplexer(numeroRelay);

    multiplexersSlave.selezionaPinMultiplexer(numeroMultiplexer, numeroPinMultiplexer);

    multiplexerMaster.multiplexerCore.disabilitaSegnale();
}