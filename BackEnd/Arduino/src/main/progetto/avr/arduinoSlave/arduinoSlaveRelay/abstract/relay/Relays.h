#ifndef ARDUINO_RELAYS_H
#define ARDUINO_RELAYS_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/abstract/multiplexer/multiplexersSlave/MultiplexersSlave.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/multiplexer/multiplexerMaster/MultiplexerMaster.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/MultiplexerCore.h"

// *** CLASSE *** //

class Relays {
    // *** DICHIARAZIONE VARIABILI *** //

    MultiplexerMaster &multiplexerMaster;
    MultiplexersSlave &multiplexersSlave;

    // *** COSTRUTTORE *** //

   public:
    Relays(MultiplexerMaster &multiplexerMaster, MultiplexersSlave &multiplexersSlave)
        : multiplexerMaster{multiplexerMaster}, multiplexersSlave{multiplexersSlave} {}

    // *** DICHIARAZIONE METODI *** //

   public:
    void azionaRelay(int numeroRelay);

   private:
    void accendeRelay(int numeroRelay);
    void spegneRelay(int numeroRelay);
};

#endif
