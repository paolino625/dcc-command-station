#ifndef ARDUINO_MULTIPLEXERSSLAVE_H
#define ARDUINO_MULTIPLEXERSSLAVE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/MultiplexerCore.h"

// *** DEFINE *** //

// Aggiungiamo +1
// Non considero multiplexer master
#define NUMERO_MULTIPLEXER_SLAVE 16

// *** CLASSE *** //

class MultiplexersSlave {
    // *** VARIABILI *** //

    MultiplexerCore **multiplexerSlaveArray = new MultiplexerCore *[NUMERO_MULTIPLEXER_SLAVE];

    // *** COSTRUTTORE *** //

   public:
    MultiplexersSlave(MultiplexerCore *multiplexers) {
        for (int i = 1; i < NUMERO_MULTIPLEXER_SLAVE; i++) {
            multiplexerSlaveArray[i] = new MultiplexerCore(multiplexers[i]);
        }
    }

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void selezionaPinMultiplexer(int numeroMultiplexer, int numeroIngresso);

    static int calcolaNumeroMultiplexer(int numeroRelay);
    static int calcolaNumeroPinMultiplexer(int numeroRelay);

   private:
    MultiplexerCore *getMultiplexer(int numeroMultiplexer);
};

#endif
