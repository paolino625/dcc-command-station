#ifndef ARDUINO_MQTTCOREARDUINOAVR_H
#define ARDUINO_MQTTCOREARDUINOAVR_H

// *** INCLUDE *** //

#include "EthernetClient.h"
#include "MQTTClient.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/reale/comunicazione/mqtt/topic/TopicMqtt.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/utility/logger/NumeroCaratteriMessaggioLogDaPubblicare.h"

// *** DEFINE *** //

// Dato che non dobbiamo leggere da MQTT, teniamo il buffer di lettura molto basso, in questa maniera risparmiamo
// memoria
#define BUFFER_MQTT_LETTURA 10
#define BUFFER_MQTT_SCRITTURA NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE

// *** CLASSE *** //

class MqttCoreArduinoAvr {
    // *** DICHIARAZIONE VARIABILI *** //

    MQTTClient mqttClient = MQTTClient(BUFFER_MQTT_LETTURA, BUFFER_MQTT_SCRITTURA);

    EthernetClient client;

    bool mqttInizializzato;

    // *** DICHIARAZIONE METODI *** //

   public:
    void inizializza();
    void inviaMessaggio(TopicMqtt topic, char* payload, bool loggerAttivo);
    bool isMqttInizializzato() const;

   private:
    static const char* calcoloNomeTopic(TopicMqtt topicMqtt, bool loggerAttivo);
};

extern MqttCoreArduinoAvr mqttCore;

#endif
