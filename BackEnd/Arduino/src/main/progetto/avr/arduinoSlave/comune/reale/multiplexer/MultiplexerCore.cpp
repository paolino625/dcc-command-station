// *** INCLUDE *** //

#include "MultiplexerCore.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/infoMultiplexer/InfoMultiplexer.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void MultiplexerCore::inizializza() {
    // Inizializziamo i pin di scelta.
    for (int j = 0; j < 4; j++) {
        pinMode(pinSceltaUscita[j], OUTPUT);
        digitalWrite(pinSceltaUscita[j], LOW);
    }

    // Inizializziamo il pin del segnale a seconda della tipologia della multiplexer
    if (tipologiaMultiplexer.modalitaPin == ModalitaPin::DIGITALE) {
        // Inizializziamo l'uscita
        if (tipologiaMultiplexer.tipologiaPin == TipologiaPin::TIPOLOGIA_OUTPUT) {
            pinMode(pinSegnale, OUTPUT);
            digitalWrite(pinSegnale, LOW);
        } else {
            if (tipologiaMultiplexer.pullUpAttivo) {
                pinMode(pinSegnale, INPUT_PULLUP);
            } else {
                pinMode(pinSegnale, INPUT);
            }
        }
    } else {
        if (tipologiaMultiplexer.tipologiaPin == TipologiaPin::TIPOLOGIA_INPUT) {
            if (tipologiaMultiplexer.pullUpAttivo) {
                pinMode(pinSegnale, INPUT_PULLUP);
            } else {
                pinMode(pinSegnale, INPUT);
            }
        } else {
            pinMode(pinSegnale, OUTPUT);
        }
    }
}

void MultiplexerCore::selezionaPin(int numeroIngresso) {
    digitalWrite(pinSceltaUscita[0], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][0]);
    digitalWrite(pinSceltaUscita[1], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][1]);
    digitalWrite(pinSceltaUscita[2], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][2]);
    digitalWrite(pinSceltaUscita[3], combinazioniPinSceltaIngressoMultiplexer[numeroIngresso - 1][3]);

    // Delay per permettere al demultiplexer di cambiare canale, altrimenti si avrebbero letture errate.
    delayMicroseconds(TEMPO_SWITCH_CANALE_DEMULTIPLEXER_MICROSECONDI);
}

void MultiplexerCore::abilitaSegnale() const { digitalWrite(pinSegnale, HIGH); }

void MultiplexerCore::disabilitaSegnale() const { digitalWrite(pinSegnale, LOW); }