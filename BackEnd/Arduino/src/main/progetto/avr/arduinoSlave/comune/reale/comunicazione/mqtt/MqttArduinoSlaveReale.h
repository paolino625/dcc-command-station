#ifndef ARDUINO_MQTTARDUINOSLAVEREALE_H
#define ARDUINO_MQTTARDUINOSLAVEREALE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/comune/reale/comunicazione/mqtt/mqttCore/MqttCoreArduinoAvr.h"

// *** CLASSE *** //

class MqttArduinoSlaveReale {
    // *** DICHIARAZIONE VARIABILI *** //

   public:
    MqttCoreArduinoAvr& mqttCore;

    // *** COSTRUTTORE *** //

    MqttArduinoSlaveReale(MqttCoreArduinoAvr& mqttCore) : mqttCore{mqttCore} {}

    // *** DICHIARAZIONE METODI *** //

    void inviaMessaggioLog(char* payload);
};

extern MqttArduinoSlaveReale mqttArduinoSlaveReale;

#endif
