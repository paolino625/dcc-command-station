#ifndef DCC_COMMAND_STATION_RELAYCOMUNE_H
#define DCC_COMMAND_STATION_RELAYCOMUNE_H

// *** INCLUDE *** //

#include <Arduino.h>

// *** DEFINE *** //

/*
Numero ascisse e ordinate scheda multiplexer
Inserire +1
*/
#define NUMERO_ASCISSE_SCHEDA_MULTIPLEXER 61
#define NUMERO_ORDINATE_SCHEDA_MULTIPLEXER 5

#endif