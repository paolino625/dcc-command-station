// *** INCLUDE *** //

#include "MqttArduinoSlaveReale.h"

// *** DEFINIZIONE METODI *** //

void MqttArduinoSlaveReale::inviaMessaggioLog(char* payload) {
    // Imposto loggerAttivo a false, altrimenti si creerebbe una dipendenza circolare.
    // Voglio scrivere un log "CIAO", entro in questa funzione ma poi mi accorgo che devo prima pubblicare
    // un messaggio di log, quindi viene richiamata questa funzione e così via, fino all'infinito.
    mqttCore.inviaMessaggio(TopicMqtt::LOG, payload, false);
}

// *** DEFINIZIONE VARIABILI *** //

MqttArduinoSlaveReale mqttArduinoSlaveReale(mqttCore);