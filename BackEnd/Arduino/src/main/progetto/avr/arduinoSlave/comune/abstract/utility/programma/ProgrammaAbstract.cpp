// *** INCLUDE *** //

#include "ProgrammaAbstract.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/componentiHardware/buzzer/BuzzerRealeArduinoSlaveSensori.h"

// *** DEFINIZIONE METODI *** //

void ProgrammaAbstract::ferma() {
    buzzerRealeArduinoSlaveSensori.beepErrore();
    while (true) {
    }
}
