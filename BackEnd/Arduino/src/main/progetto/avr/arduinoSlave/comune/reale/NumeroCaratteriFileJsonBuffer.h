#ifndef ARDUINO_NUMEROCARATTERIFILEJSONBUFFER_H
#define ARDUINO_NUMEROCARATTERIFILEJSONBUFFER_H

// La libreria supporta al massimo un unsigned int. Per unsigned int inserire U dopo il numero: ad esempio 32800U
#define NUMERO_CARATTERI_FILE_JSON_BUFFER 200

#endif
