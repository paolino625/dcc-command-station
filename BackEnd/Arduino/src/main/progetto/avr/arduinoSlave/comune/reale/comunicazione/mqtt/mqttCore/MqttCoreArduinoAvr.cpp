// *** INCLUDE *** //

#include "MqttCoreArduinoAvr.h"

#include "main/progetto/comune/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/arm/environmentConfig/svil/EnvironmentConfigSvil.h"

#ifdef ARDUINO_SLAVE_RELAY
#include "main/progetto/avr/arduinoSlave/arduinoSlaveRelay/environmentConfig/svil/EnvironmentConfigSvil.h"
#elif ARDUINO_SLAVE_SENSORI
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#endif

// *** DEFINIZIONE VARIABILI *** //

MqttCoreArduinoAvr mqttCore;

// *** DEFINIZIONE METODI *** //

void MqttCoreArduinoAvr::inizializza() {
    mqttInizializzato = true;

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo MQTT...",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    mqttClient.begin(INDIRIZZO_SERVER, PORTA_MQTT_BROKER, client);
    while (!mqttClient.connect("arduino", "Paolino625", "RPbxnbCRV2hM")) {
        delay(1000);
    }

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo MQTT... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void MqttCoreArduinoAvr::inviaMessaggio(TopicMqtt topicMqtt, char* payload, bool loggerAttivo) {
    if (MQTT_ATTIVO) {
        const char* nomeTopic = calcoloNomeTopic(topicMqtt, false);

        if (loggerAttivo) {
            if (DEBUGGING_MQTT) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio messaggio MQTT a topic ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, nomeTopic,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "... ",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

                loggerReale.cambiaTabulazione(1);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                      "Payload: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, payload,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                loggerReale.cambiaTabulazione(-1);
            }
        }
        mqttClient.publish(nomeTopic, payload);

        if (loggerAttivo) {
            if (DEBUGGING_MQTT) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Invio messaggio MQTT a topic ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, nomeTopic,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "... OK",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        }
    }
}

bool MqttCoreArduinoAvr::isMqttInizializzato() const { return mqttInizializzato; }

const char* MqttCoreArduinoAvr::calcoloNomeTopic(TopicMqtt topicMqtt, bool loggerAttivo) {
    switch (topicMqtt) {
        case TopicMqtt::LOG: {
            return NOME_TOPIC_MQTT_LOG;
        }

        default: {
            if (loggerAttivo) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::BUG, "Enumerazione non gestita.",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
            }
            return "";
        }
    }
}