// *** INCLUDE *** //

#include "InfoMultiplexer.h"

// *** DEFINIZIONE VARIABILI *** //

const int combinazioniPinSceltaIngressoMultiplexer[NUMERO_INGRESSI_MULTIPLEXER - 1][4] = {
    // Crea un Array con i valori binari da richiamare in base al canale Y scelto
    // s0, s1, s2, s3, canale
    {0, 0, 0, 0},  // Y0
    {1, 0, 0, 0},  // Y1
    {0, 1, 0, 0},  // Y2
    {1, 1, 0, 0},  // Y3
    {0, 0, 1, 0},  // Y4
    {1, 0, 1, 0},  // Y5
    {0, 1, 1, 0},  // Y6
    {1, 1, 1, 0},  // Y7
    {0, 0, 0, 1},  // Y8
    {1, 0, 0, 1},  // Y9
    {0, 1, 0, 1},  // Y10
    {1, 1, 0, 1},  // Y11
    {0, 0, 1, 1},  // Y12
    {1, 0, 1, 1},  // Y13
    {0, 1, 1, 1},  // Y14
    {1, 1, 1, 1}   // Y15
};