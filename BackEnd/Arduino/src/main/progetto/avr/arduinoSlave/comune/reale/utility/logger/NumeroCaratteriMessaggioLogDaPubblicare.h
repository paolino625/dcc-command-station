#ifndef ARDUINO_NUMEROCARATTERIMESSAGGIOLOGDAPUBBLICARE_H
#define ARDUINO_NUMEROCARATTERIMESSAGGIOLOGDAPUBBLICARE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/comune/reale/NumeroCaratteriFileJsonBuffer.h"

// *** DEFINE *** //

// Rispetto ad Arduino ARM, gli Arduino AVR hanno meno memoria disponibile.
// Se si esagera con questo parametro, si rischia che Arduino crashi all'avvio oppure durante l'esecuzione.
// Con 1000 caratteri, Arduino Slave Sensori si comporta in maniera errata durante l'esecuzione.

#define NUMERO_CARATTERI_MESSAGGIO_LOG_DA_PUBBLICARE NUMERO_CARATTERI_FILE_JSON_BUFFER

#endif
