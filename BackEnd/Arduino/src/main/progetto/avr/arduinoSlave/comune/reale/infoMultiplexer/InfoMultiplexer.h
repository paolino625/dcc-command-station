#ifndef ARDUINO_INFOMULTIPLEXER_H
#define ARDUINO_INFOMULTIPLEXER_H

// *** DEFINE *** //

// Aggiungiamo +1
#define NUMERO_INGRESSI_MULTIPLEXER 17

// *** DICHIARAZIONE VARIABILI *** //

extern const int combinazioniPinSceltaIngressoMultiplexer[][4];

#endif