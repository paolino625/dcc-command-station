#ifndef ARDUINO_MULTIPLEXERCORE_H
#define ARDUINO_MULTIPLEXERCORE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/tipologia/TipologiaMultiplexer.h"

// *** DEFINE *** //

// Questo valore è stato testato. 100 microsecondi non sono sufficienti.
#define TEMPO_SWITCH_CANALE_DEMULTIPLEXER_MICROSECONDI 250

// *** CLASSE *** //

class MultiplexerCore {
    // *** VARIABILI *** //

    TipologiaMultiplexer tipologiaMultiplexer;

   public:
    int pinSceltaUscita[4];
    int pinSegnale;

    // *** COSTRUTTORE *** //

    MultiplexerCore(int* pinScelta, int pinSegnale, TipologiaMultiplexer tipologiaMultiplexer)
        : tipologiaMultiplexer(tipologiaMultiplexer), pinSegnale(pinSegnale) {
        for (int i = 0; i < 4; i++) {
            this->pinSceltaUscita[i] = pinScelta[i];
        }
    }

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void selezionaPin(int numeroIngresso);
    void abilitaSegnale() const;
    void disabilitaSegnale() const;
};

#endif
