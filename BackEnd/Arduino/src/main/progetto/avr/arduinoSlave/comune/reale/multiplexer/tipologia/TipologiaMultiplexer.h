#ifndef ARDUINO_TIPOLOGIAMULTIPLEXER_H
#define ARDUINO_TIPOLOGIAMULTIPLEXER_H

// *** ENUMERAZIONI *** //

enum class TipologiaPin { TIPOLOGIA_INPUT, TIPOLOGIA_OUTPUT };
enum class ModalitaPin { DIGITALE, ANALOGICO };

// *** STRUCT *** //

struct TipologiaMultiplexer {
    TipologiaPin tipologiaPin;
    ModalitaPin modalitaPin;
    bool pullUpAttivo;
};

#endif
