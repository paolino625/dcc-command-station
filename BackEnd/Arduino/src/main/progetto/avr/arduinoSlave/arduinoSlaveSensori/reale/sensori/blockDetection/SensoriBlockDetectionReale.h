#ifndef ARDUINO_SENSORIBLOCKDETECTIONREALE_H
#define ARDUINO_SENSORIBLOCKDETECTIONREALE_H

// *** INCLUDE *** //

#include "SensoriBlockDetectionReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/sensori/blockDetection/sensori/SensoriBlockDetection.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SensoriBlockDetectionReale sensoriBlockDetectionReale;

#endif
