#ifndef ARDUINO_ENVIRONMENTCONFIGGLOBAL_H
#define ARDUINO_ENVIRONMENTCONFIGGLOBAL_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/multiplexer/multiplexers/Multiplexers.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/blockDetection/NumeroSensoriBlockDetection.h"

// *** VARIABILI GLOBALI *** //

// L'indirizzo MAC non è essenziale per stabilire la connessione, ma sarà semplicemente il MAC che la scheda di rete
// presenterà al router.
// TODO Da controllare
#define INDIRIZZO_MAC_ETHERNET_SHIELD_ARDUINO_SLAVE_SENSORI {0xA8, 0x61, 0x0A, 0xAE, 0x0A, 0x32}

#endif
