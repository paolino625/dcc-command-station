// *** INCLUDE *** //

#include "CodaSensori.h"

#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

// *** DEFINIZIONE METODI *** //

bool CodaSensori::isVuota() const {
    if (indiceCodaSensoriAzionatiAttuale == indiceCodaSensoriAzionatiSalvataggio) {
        return true;
    } else {
        return false;
    }
}

void CodaSensori::aggiunge(int numeroSensore) {
    codaSensoriAzionatiDaInviare[indiceCodaSensoriAzionatiSalvataggio] = numeroSensore;
    indiceCodaSensoriAzionatiSalvataggio += 1;
    if (indiceCodaSensoriAzionatiSalvataggio == NUMERO_ELEMENTI_CODA) {
        indiceCodaSensoriAzionatiSalvataggio = 0;
    }

    char buffer[10];
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Aggiungo sensore n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    itoa(numeroSensore, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " alla coda... OK",
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
}

int CodaSensori::legge() {
    if (isVuota()) {
        return -1;
    }

    int sensore = codaSensoriAzionatiDaInviare[indiceCodaSensoriAzionatiAttuale];
    indiceCodaSensoriAzionatiAttuale += 1;
    if (indiceCodaSensoriAzionatiAttuale == NUMERO_ELEMENTI_CODA) {
        indiceCodaSensoriAzionatiAttuale = 0;
    }

    return sensore;
}