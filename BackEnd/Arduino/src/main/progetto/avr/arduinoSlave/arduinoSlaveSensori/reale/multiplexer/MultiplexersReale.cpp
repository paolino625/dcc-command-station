// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/multiplexer/multiplexers/Multiplexers.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/pin/Pin.h"

// *** DEFINIZIONE VARIABILI *** //

TipologiaMultiplexer tipologiaMultiplexer = {TipologiaPin::TIPOLOGIA_INPUT, ModalitaPin::ANALOGICO, true};

MultiplexerCore multiplexerCore0 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[0], pinSegnaleDemultiplexerSensoriIr[0],
                    tipologiaMultiplexer);  // Non in uso ma necessario
MultiplexerCore multiplexerCore1 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[1], pinSegnaleDemultiplexerSensoriIr[1], tipologiaMultiplexer);
MultiplexerCore multiplexerCore2 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[2], pinSegnaleDemultiplexerSensoriIr[2], tipologiaMultiplexer);
MultiplexerCore multiplexerCore3 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[3], pinSegnaleDemultiplexerSensoriIr[3], tipologiaMultiplexer);
MultiplexerCore multiplexerCore4 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[4], pinSegnaleDemultiplexerSensoriIr[4], tipologiaMultiplexer);
MultiplexerCore multiplexerCore5 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[5], pinSegnaleDemultiplexerSensoriIr[5], tipologiaMultiplexer);
MultiplexerCore multiplexerCore6 =
    MultiplexerCore(pinSceltaDemultiplexerSensoriIr[6], pinSegnaleDemultiplexerSensoriIr[6], tipologiaMultiplexer);

Multiplexer multiplexers[NUMERO_MULTIPLEXER] = {Multiplexer(0, multiplexerCore0), Multiplexer(1, multiplexerCore1),
                                                Multiplexer(2, multiplexerCore2), Multiplexer(3, multiplexerCore3),
                                                Multiplexer(4, multiplexerCore4), Multiplexer(5, multiplexerCore5),
                                                Multiplexer(6, multiplexerCore6)};

/*
Impostiamo il pull-up sugli ingressi delle multiplexer: in questo modo se un sensore si stacca o non funziona, il
valore letto da Arduino sarà 1024, facilitando il debugging.
Bonus: durante le fasi di testing, è possibile lasciare sul codice tutti i sensori come presenti: anche se questi
non sono collegati, non causeranno letture errate.
*/
Multiplexers multiplexersReale = Multiplexers(multiplexers);