// *** INCLUDE *** //

#include "Multiplexers.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/infoMultiplexer/InfoMultiplexer.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void Multiplexers::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo multiplexers... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_MULTIPLEXER; i++) {
        Multiplexer *multiplexer = getMultiplexer(i);
        multiplexer->multiplexerCore.inizializza();
    }
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo multiplexers... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void Multiplexers::leggePin() {
    for (int i = 1; i < NUMERO_MULTIPLEXER; i++) {
        Multiplexer *multiplexer = getMultiplexer(i);
        multiplexer->leggeTuttiPin();
    }
}

IdSensoreTipo Multiplexers::calcoloIdSensoreIr(int numeroDemultiplexer, int numeroIngresso) {
    char buffer[10];

    // Calcolo l'ID del sensore in base al numero del demultiplexer e il numero di ingresso.

    if (DEBUGGING_CALCOLO_ID_SENSORE_IR) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Numero Demultiplexer: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroDemultiplexer, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Numero Ingresso: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroIngresso, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    int numeroSensore = (numeroDemultiplexer - 1) * (NUMERO_INGRESSI_MULTIPLEXER - 1) + numeroIngresso;

    if (DEBUGGING_CALCOLO_ID_SENSORE_IR) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Numero ID sensore: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroSensore, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    return numeroSensore;
}

Multiplexer *Multiplexers::getMultiplexer(int numeroMultiplexer) { return multiplexerArray[numeroMultiplexer]; }