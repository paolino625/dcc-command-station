#ifndef ARDUINO_BUZZERREALEARDUINOSLAVESENSORI_H
#define ARDUINO_BUZZERREALEARDUINOSLAVESENSORI_H

// *** INCLUDE *** //

#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/BuzzerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"

// *** DICHIARAZIONE VARIABILI *** //

extern BuzzerAbstractReale buzzerRealeArduinoSlaveSensori;

#endif
