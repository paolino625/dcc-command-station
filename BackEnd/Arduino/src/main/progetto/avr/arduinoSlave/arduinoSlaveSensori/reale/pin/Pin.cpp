// *** INCLUDE *** //

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/multiplexer/multiplexers/NumeroMultiplexer.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/sensori/blockDetection/sensori/SensoriBlockDetection.h"

// *** DEFINIZIONE VARIABILI *** //

int pinSceltaDemultiplexerSensoriIr[NUMERO_MULTIPLEXER][4] = {{0, 0, 0, 0},     {22, 23, 24, 25}, {26, 27, 28, 29},
                                                              {30, 31, 32, 33}, {34, 35, 36, 37}, {38, 39, 40, 41},
                                                              {42, 43, 44, 45}};

int pinSegnaleDemultiplexerSensoriIr[NUMERO_MULTIPLEXER] = {0, A0, A1, A2, A3, A4, A5};

int pinSensoriBlockDetector[NUMERO_SENSORI_BLOCK_DETECTOR] = {0, 46, 47, 48, 49, 50, 51};