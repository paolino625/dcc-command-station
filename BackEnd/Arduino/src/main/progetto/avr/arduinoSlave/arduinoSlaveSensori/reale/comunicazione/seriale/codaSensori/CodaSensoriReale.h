#ifndef ARDUINO_CODASENSORIREALE_H
#define ARDUINO_CODASENSORIREALE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/comunicazione/seriale/codaSensori/CodaSensori.h"

// *** DEFINIZIONE VARIABILI *** //

extern CodaSensori codaSensoriReale;

#endif
