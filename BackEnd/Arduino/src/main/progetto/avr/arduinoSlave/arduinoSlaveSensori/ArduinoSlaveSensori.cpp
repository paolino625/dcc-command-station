// *** INCLUDE *** //

#include <Arduino.h>

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/comunicazione/seriale/ArduinoMaster.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/comunicazione/seriale/codaSensori/CodaSensoriReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/multiplexer/MultiplexersReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/blockDetection/SensoriBlockDetectionReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/ir/SensoriIrReale.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/comunicazione/mqtt/mqttCore/MqttCoreArduinoAvr.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/ethernet/core/EthernetCore.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/PorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINE *** //

#define NUMERO_LETTURE_COMPLETE_DA_SCARTARE 10  // Valore testato, non scendere sotto

// *** SETUP *** //

void setup() {
    inizializzoPortaSerialeArduinoGigaArduinoMega(PORTA_SERIALE_COMPUTER, VELOCITA_PORTA_SERIALE_COMPUTER, false);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "ARDUINO SLAVE SENSORI",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

    if (ETHERNET_PRESENTE) {
        ethernetCore.inizializza(true);
    }

    if (MQTT_ATTIVO) {
        mqttCore.inizializza();
    }

    inizializzoPortaSerialeArduinoGigaArduinoMega(PORTA_SERIALE_ARDUINO_MASTER, VELOCITA_PORTA_SERIALE_ARDUINO, false);

    sensoriBlockDetectionReale.inizializza();

    multiplexersReale.inizializza();

    // Inizializzo LED integrato
    pinMode(LED_BUILTIN, OUTPUT);

    // Le prime letture risultano instabili. Non è sufficiente un delay, è necessario leggere.
    for (int r = 0; r < NUMERO_LETTURE_COMPLETE_DA_SCARTARE; r++) {
        multiplexersReale.leggePin();
    }

    sensoriIrReale.verificaFunzionamento();
}

// *** LOOP *** //

void loop() {
    if (ETHERNET_PRESENTE) {
        ethernetCore.mantieneConnessione();
    }

    multiplexersReale.leggePin();

    sensoriIrReale.controllaStato();
    // sensoriBlockDetectionReale.controllaStato();

    // Se sto attendendo un ACK di un precedente messaggio inviato, controllo la ricezione,
    // altrimenti sono pronto a inviare un ping o un sensore azionato ad Arduino Master
    if (arduinoMasterReale.inAttesaAck()) {
        arduinoMasterReale.controllaAck();
        arduinoMasterReale.controllaAck();
        /*
        Invio sempre e comunque il ping, altrimenti se Arduino Slave
        Sensori si aspetta sens 2 e quindi entra in questo if, ma
        Arduino Giga ha resettato la connessione della porta seriale,
        si aspetta di ricevere un ping
        */
        arduinoMasterReale.inviaHealthCheck();
    } else {
        if (codaSensoriReale.isVuota()) {
            // Se non ho nulla da inviare, invio health check
            arduinoMasterReale.inviaHealthCheck();
        } else {
            // Se c'è almeno un sensore da inviare invio il numero ad Arduino
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "C'è almeno un sensore in coda!",
                                  InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);

            arduinoMasterReale.inviaMessaggioSensoreAzionato(codaSensoriReale.legge());
        }
    }
}
