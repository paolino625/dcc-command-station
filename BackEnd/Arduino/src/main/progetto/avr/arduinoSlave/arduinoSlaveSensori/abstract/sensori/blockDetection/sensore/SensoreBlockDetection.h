#ifndef ARDUINO_SENSOREBLOCKDETECTION_H
#define ARDUINO_SENSOREBLOCKDETECTION_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/TipiDati.h"

// *** CLASSE *** //

class SensoreBlockDetection {
    // *** DICHIARAZIONE VARIABILI *** //

    IdSensoreTipo id;

    int valore;
    bool stato;

    int pin;
    bool presente;

    // *** COSTRUTTORE *** //

   public:
    SensoreBlockDetection(IdSensoreTipo id, int pin, bool presente) : id(id), pin(pin), presente(presente) {};

    // *** DICHIARAZIONE METODI *** //

   public:
    void inizializza() const;
    void controllaStato();
    bool isPresente() const;
};

#endif
