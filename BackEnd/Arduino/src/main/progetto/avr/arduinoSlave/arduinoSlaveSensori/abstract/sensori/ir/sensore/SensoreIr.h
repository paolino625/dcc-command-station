#ifndef ARDUINO_SENSOREIR_H
#define ARDUINO_SENSOREIR_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/TipiDati.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** DEFINE *** //

#define SOGLIA_ATTIVAZIONE_SENSORE_IR_STANDARD 200

/*
La soglia di disattivazione è calcolata in funzione della soglia di attivazione.
- Se la differenza è troppo bassa, questo potrebbe portare a falsi positivi: questo perché la targhetta argentata sotto
la locomotiva non è costante ma oscilla un po' (a seconda di zigrinature o leggeri piegature). Questo potrebbe portare a
considerare una sola targhetta come 2 o più. È necessario dunque considerare questo nella scelta della differenza.
- Se, al contrario, si sceglie una differenza troppo alta, potrebbe succedere che il sensore non vada mai in OFF tra la
prima locomotiva e la seconda locomotiva del convoglio: questo comporterebbe l'assenza della lettura della seconda
locomotiva.
*/
#define DIFFERENZA_TRA_SOGLIA_ATTIVAZIONE_E_DISATTIVAZIONE 100

// È possibile impostare un margine di errore per la soglia di attivazione e disattivazione.
#define MARGINE_ERRORE_SOGLIA_ATTIVAZIONE 0
#define MARGINE_ERRORE_SOGLIA_DISATTIVAZIONE 0

/*
Dopo che il sensore è stato attivato, viene ignorato il suo valore se la variazione avviene entro il tempo deciso qui.
Questo per evitare una doppia lettura quando viene letta la targhetta argentata sotto la locomotiva: il valore che
assume il sensore sotto la fascetta argentata è un po' variabile a seconda della piegatura della fascetta stessa.
*/
#define MILLISECONDI_SENSORE_IR_INATTIVO_DOPO_LETTURA 200

// *** CLASSE *** //

class SensoreIr {
    // *** DICHIARAZIONE VARIABILI *** //

    IdSensoreTipo id;

    int valore;          // Valore analogico letto dal sensore, da 0 a 1024
    bool stato = false;  // Off o On

    int numeroMultiplexer;
    int numeroIngressoMultiplexer;

    /*
    sogliaAttivazioneSensoreIr per far passare lo stato del sensore da OFF a ON
    Valore massimo = 1024
    */
    int sogliaAttivazione = SOGLIA_ATTIVAZIONE_SENSORE_IR_STANDARD;

    /*
    Risulta comodo separare la soglia di attivazione da quella di disattivazione.
    Nel caso in cui si usasse il sensore in digitale, queste soglie coinciderebbero.
    Ad esempio, al di sotto di 100 il sensore è ON, al di sopra è OFF.
    Invece, in questa modalità, possiamo considerare il sensore ON al di sotto al 100 ma non considerarlo OFF prima che
    salga sopra una certa soglia.
    */
    int sogliaDisattivazione =
        SOGLIA_ATTIVAZIONE_SENSORE_IR_STANDARD + DIFFERENZA_TRA_SOGLIA_ATTIVAZIONE_E_DISATTIVAZIONE;

    TimestampTipo timestampUltimaAttivazioneSensoreIr = 0;

    // *** COSTRUTTORE *** //

   public:
    SensoreIr(IdSensoreTipo id, int numeroMultiplexer, int numeroIngressoMultiplexer)
        : id{id}, numeroMultiplexer{numeroMultiplexer}, numeroIngressoMultiplexer{numeroIngressoMultiplexer} {}

    // *** DICHIARAZIONE METODI *** //

    void controllaStato();

    void stampa(LivelloLog livelloLog) const;

    // ** GETTER ** //

    int getNumeroMultiplexer() const;
    int getNumeroIngressoMultiplexer() const;
    int getValore() const;

    // ** SETTER ** //

    void setSogliaAttivazione(int sogliaAttivazione);
    void setSogliaDisattivazione(int sogliaDisattivazione);
    void setValore(int valore);
};

#endif
