// *** INCLUDE *** //

#include "SensoreBlockDetection.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/comunicazione/seriale/codaSensori/CodaSensoriReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/blockDetection/SensoriBlockDetectionReale.h"

// *** DEFINIZIONE METODI *** //

void SensoreBlockDetection::inizializza() const { pinMode(pin, INPUT_PULLUP); }

// TODO Da testare
void SensoreBlockDetection::controllaStato() {
    bool statoSensoreCorrente;
    // Leggo valore del sensore corrente: 0 o 1
    int valoreSensoreCorrente = valore;

    if (valoreSensoreCorrente == 1) {
        // Sezione libera
        statoSensoreCorrente = false;
    } else {
        // Sezione occupata
        statoSensoreCorrente = true;
    }

    // Verifico lo stato del sensore letto rispetto allo stato precedente salvato.
    // Se lo stato è passato da OFF a ON, devo inviare un messaggio ad Arduino Master, altrimenti mi
    // limito a salvare il nuovo stato.
    if (statoSensoreCorrente != stato) {
        if (statoSensoreCorrente) {
            // Lo stato è ON, quindi questo è un passaggio da OFF a ON. Devo comunicarlo ad Arduino
            // Master.
            int numeroGlobaleSensore = sensoriBlockDetectionReale.calcolaIdSensore(id);
            // Alla coda devo aggiungere il numero del sensore globale, non il numero del sensore che usiamo
            // internamente.
            codaSensoriReale.aggiunge(numeroGlobaleSensore);
        }
        // In entrambi i casi salvo il nuovo stato del sensore.
        stato = statoSensoreCorrente;
    }
}

bool SensoreBlockDetection::isPresente() const { return presente; }