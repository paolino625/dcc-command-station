#ifndef ARDUINO_SENSORIIRREALE_H
#define ARDUINO_SENSORIIRREALE_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/sensori/ir/sensori/SensoriIr.h"

// *** DICHIARAZIONE VARIABILI *** //

extern SensoriIr sensoriIrReale;

#endif
