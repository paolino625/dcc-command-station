// *** INCLUDE *** //

#include "SensoriIr.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/utility/programma/ProgrammaReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void SensoriIr::verificaFunzionamento() {
    if (VERIFICA_FUNZIONAMENTO_SENSORI) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Verifico funzionamento sensori IR...",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
        char buffer[10];
        for (int i = 1; i < NUMERO_SENSORI_IR; i++) {
            SensoreIr* sensoreIr = getSensore(i);
            int valore = sensoreIr->getValore();
            if (valore > SOGLIA_MINIMA_SENSORE_FUNZIONANTE) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, "Sensore IR n° ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                itoa(i, buffer, 10);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, buffer,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::CRITICAL, " non funzionante",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
                sensoreIr->stampa(LivelloLog::INFO);

                programmaReale.ferma();
            }
        }
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Verifico funzionamento sensori IR... OK",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    }
}

// Controlliamo lo stato solo dei sensori IR presenti
void SensoriIr::controllaStato() {
    for (int i = 1; i < NUMERO_SENSORI_IR; i++) {
        SensoreIr* sensoreIr = getSensore(i);
        if (sensoriIrPresenti[i]) {
            sensoreIr->controllaStato();
        }
    }
}

void SensoriIr::stampa(LivelloLog livelloLog) {
    for (int i = 1; i < NUMERO_SENSORI_IR; i++) {
        SensoreIr* sensoreIr = getSensore(i);
        sensoreIr->stampa(livelloLog);
    }
}

SensoreIr* SensoriIr::getSensore(int id) { return sensoriIr[id]; }

SensoreIr* SensoriIr::getSensore(int numeroMultiplexer, int numeroIngresso) {
    for (int i = 1; i < NUMERO_SENSORI_IR; i++) {
        SensoreIr* sensoreIr = getSensore(i);

        if (sensoreIr->getNumeroMultiplexer() == numeroMultiplexer &&
            sensoreIr->getNumeroIngressoMultiplexer() == numeroIngresso) {
            return sensoreIr;
        }
    }
}