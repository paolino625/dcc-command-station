// *** INCLUDE *** //

#include "ArduinoMaster.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void ArduinoMaster::inviaMessaggioSensoreAzionato(IdSensoreTipo numeroSensoreAttivato) {
    char stringaDaInviare[NUMERO_CARATTERI_MESSAGGIO_ARDUINO] = "";

    strcat(stringaDaInviare, "sens ");
    itoa(numeroSensoreAttivato, stringaDaInviare + strlen("sens "), 10);

    Serial1.write(stringaDaInviare, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                          "Invio ad Arduino Master: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, stringaDaInviare,
                          InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

    avviaControlloAck(stringaDaInviare);
}

void ArduinoMaster::inviaKeepAlive() {
    char stringaDaInviare[NUMERO_CARATTERI_MESSAGGIO_ARDUINO] = "ping";
    Serial1.write(stringaDaInviare, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

    if (DEBUGGING_PRINT_INVIO_PING_ARDUINO_MASTER) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                              "Invio ad Arduino Master: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, stringaDaInviare,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    avviaControlloAck(stringaDaInviare);
}

void ArduinoMaster::avviaControlloAck(const char *stringaInviata) {
    // Copio la stringa che ho inviato nell'apposito buffer
    strcpy(stringaInAttesaAck, stringaInviata);
    attendoAck = true;
    millisInvioUltimoMessaggio = millis();
}

void ArduinoMaster::controllaAck() {
    // stampo("Controllo ACK... \n");
    while (Serial1.available() > 0) {
        // Ogni volta che ricevo un messaggio resetto il timestamp, utile per accertarmi che Arduino
        // Mega sia attivo e il segnale Interrupt sia valido.
        char stringaRicevuta[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
        Serial1.readBytes(stringaRicevuta, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);

        if (DEBUGGING_PRINT_RICEZIONE_PORTA_SERIALE) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_,
                                  "Ricevuto da Arduino Master: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA,
                                  false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, stringaRicevuta,
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "...",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        }

        if (strcmp(stringaRicevuta, stringaInAttesaAck) == 0) {
            if (DEBUGGING_PRINT_RICEZIONE_PORTA_SERIALE) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, " OK",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
            attendoAck = false;

        } else {
            if (DEBUGGING_PRINT_RICEZIONE_PORTA_SERIALE) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, " Stringa ACK non corrisponde",
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
        }
    }

    // Se non ho ricevuto un ACK nelle righe di prima, rinvio il messaggio se è passato del tempo
    if (attendoAck) {
        // Se non ho ricevuto un ACK entro tot tempo, rinvio il messaggio
        if (millis() - millisInvioUltimoMessaggio >= ATTESA_ACK) {
            if (DEBUGGING_PRINT_ACK_TIMEOUT) {
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, "ACK Timeout: invio di nuovo il messaggio: ",
                                      InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
                LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::WARNING, stringaInAttesaAck,
                                      InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
            }
            Serial1.write(stringaInAttesaAck, NUMERO_CARATTERI_MESSAGGIO_ARDUINO);
            // Resetto il timer dato che ho appena rinviato il messaggio
            millisInvioUltimoMessaggio = millis();
        }
    }
    // stampo("Controllo ACK... OK\n");
}

void ArduinoMaster::inviaHealthCheck() {
    if (millis() - timestampUltimoInvioPacchettoAliveArduinoMaster > INTERVALLO_INVIO_PACCHETTO_ALIVE_ARDUINO_MASTER) {
        digitalWrite(LED_BUILTIN, HIGH);
        inviaKeepAlive();
        timestampUltimoInvioPacchettoAliveArduinoMaster = millis();
        digitalWrite(LED_BUILTIN, LOW);
    }
}

bool ArduinoMaster::inAttesaAck() const { return attendoAck; }