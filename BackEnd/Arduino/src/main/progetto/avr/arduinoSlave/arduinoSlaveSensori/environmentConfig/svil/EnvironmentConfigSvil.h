#ifndef ARDUINO_ENVIRONMENTCONFIGSVIL_ARDUINO_SLAVE_SENSORI_H
#define ARDUINO_ENVIRONMENTCONFIGSVIL_ARDUINO_SLAVE_SENSORI_H

// *** INCLUDE *** //

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/blockDetection/NumeroSensoriBlockDetection.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/ir/NumeroSensoriIr.h"
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"
#include "main/progetto/comune/moduliIndipendentiArduino/EnumerazioniLogger.h"

#ifdef SVIL

// *** BUZZER *** //

#define BUZZER_ATTIVO 1

// *** COMUNICAZIONE *** //

#define ETHERNET_PRESENTE 1

// *** DEBUGGING AVANZATO *** //

#define DEBUGGING_PRINT_RICEZIONE_PORTA_SERIALE 0

#define DEBUGGING_PRINT_INVIO_PING_ARDUINO_MASTER 0

#define DEBUGGING_PRINT_ACK_TIMEOUT 0

// ** CONTROLLO SENSORE ** //

// Questa variabile è legata a DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI e DEBUGGING_PRINT_LETTURA_SENSORE
#define DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI_NUMERO_SENSORE 50

// È possibile verificare se lo stato del sensore selezionato prima viene effettivamente controllato
#define DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI 0

// Stampa del valore letto del sensore
#define DEBUGGING_PRINT_LETTURA_SENSORE 0

#define DEBUGGING_CALCOLO_ID_SENSORE_IR 0

#define DEBUGGING_MQTT 0

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

// ** ALTRO ** //

#define VERIFICA_FUNZIONAMENTO_SENSORI 0

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

#define PUBBLICAZIONE_LOG 1

// *** MQTT *** //

#define MQTT_ATTIVO 1
// Mi assicuro che se Ethernet non è attivo, MQTT non possa essere attivo
#if (ETHERNET_PRESENTE == 0) && (MQTT_ATTIVO == 1)
#error "Se Ethernet non è presente, MQTT non può essere attivo."
#endif

// *** PRESENZA HARDWARE *** //

#define SENSORI_PRESENTI 0
extern bool sensoriIrPresenti[NUMERO_SENSORI_IR];
extern bool sensoriBlockDetectorPresenti[NUMERO_SENSORI_BLOCK_DETECTOR];

#endif

#endif
