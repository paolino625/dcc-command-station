#ifndef ARDUINO_SENSORIBLOCKDETECTION_H
#define ARDUINO_SENSORIBLOCKDETECTION_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/sensori/blockDetection/sensore/SensoreBlockDetection.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/pin/Pin.h"

// *** DICHIARAZIONE FUNZIONI *** //

class SensoriBlockDetectionReale {
    // *** DICHIARAZIONE VARIABILI *** //

    SensoreBlockDetection** sensoriBlockDetector = new SensoreBlockDetection*[NUMERO_SENSORI_BLOCK_DETECTOR];

    // *** COSTRUTTORE *** //

   public:
    SensoriBlockDetectionReale() {
        for (int i = 1; i < NUMERO_SENSORI_BLOCK_DETECTOR; i++) {
            sensoriBlockDetector[i] =
                new SensoreBlockDetection(pinSensoriBlockDetector[i], i, sensoriBlockDetectorPresenti[i]);
        }
    }

    // *** DEFINIZIONE METODI *** //

   public:
    void inizializza();
    void controllaStato();
    static int calcolaIdSensore(int numeroSensore);

   private:
    SensoreBlockDetection* getSensoreBlockDetector(int numeroSensore);
};

#endif