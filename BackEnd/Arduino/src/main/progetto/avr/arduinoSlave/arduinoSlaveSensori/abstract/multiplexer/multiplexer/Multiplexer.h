#ifndef ARDUINO_MULTIPLEXER_H
#define ARDUINO_MULTIPLEXER_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/sensori/ir/sensori/SensoriIr.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/MultiplexerCore.h"

/*

Attenzione: il numero di campionamenti è un numero molto importante e va tarato molto attentamente.

Da una parte aumentare il numero di campionamenti può causare la mancata rilevazione delle strisce bianche
delle locomotive da parte di Arduino all'aumentare della velocità delle stesse, in particolare per
quelle locomotive che hanno una striscia argentata corta. Questo è dovuto a due motivi:
- Il campionamento serve a rimuovere cambiamenti rapidi e a normalizzare il valore; nel nostro caso quando la
locomotiva va veloce e la targhetta è corta, è essenziale invece rilevare cambiamenti rapidi.
- Il campionamento aumenta il tempo che Arduino impiega per la lettura di ogni sensore (se infatti si abilita la
stampa dei valori dei sensori, è possibile notare facilmente che all'aumentare del numero di campionamento la stampa
risulta più lenta). Se abbiamo 96 sensori in totale, significa che dopo aver letto il sensore n° 1 dobbiamo aspettare
che Arduino legga tutti gli altri sensori per il numero di campionamenti. Questo allunga l'intervallo di lettura del
sensore, e potrebbe portare alla perdita di letture di informazioni importanti.

D'altra parte, tenere un numero di campionamento nullo o troppo basso potrebbe portare a letture spurie: un pezzo
della locomotiva, seppur corto, potrebbe azionare il sensore se passa molto vicino alla locomotiva. Con il campionamento
invece, è come se ci si assicurasse che il sensore abbia letto per un po' di tempo consecutivamente
degli infrarossi, il che siamo sicuri che avviene solo in presenza della striscia argentata. Esempio: si è notato che
con la E402B e il numero di campionamenti pari a 5, il sensore ha rilevato sempre il passaggio della locomotiva, anche
alla sua velocità massima (179 km/h).

Il problema del campionamento è che lascia il tempo che trova: se il campionamento a una velocità di 100 riuscirebbe a
ignorare una protuberanza della locomotiva, questo non riesce a evitarlo quando la locomotiva passa a velocità 1.

*/

#define NUMERO_CAMPIONAMENTO_SENSORI 1

// *** CLASSE *** //

class Multiplexer {
    // *** DICHIARAZIONE VARIABILI *** //

   public:
    int id;
    MultiplexerCore multiplexerCore;

    // *** COSTRUTTORI *** //

   public:
    Multiplexer(int numeroMultiplexer, MultiplexerCore multiplexerCore)
        : id{numeroMultiplexer}, multiplexerCore{multiplexerCore} {}

    // *** DICHIARAZIONE METODI *** //

    void leggeTuttiPin();
    void stampaValoriSensori(LivelloLog livelloLog) const;

   private:
    int leggePin(int numeroIngresso);
};

#endif
