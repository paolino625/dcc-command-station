#ifndef ARDUINO_CODASENSORI_H
#define ARDUINO_CODASENSORI_H

// *** INCLUDE *** //

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/TipiDati.h"

// *** DEFINE *** //

#define NUMERO_ELEMENTI_CODA 10

// *** CLASSE *** //

class CodaSensori {
    // *** VARIABILI *** //

    byte codaSensoriAzionatiDaInviare[NUMERO_ELEMENTI_CODA];
    byte indiceCodaSensoriAzionatiSalvataggio;
    byte indiceCodaSensoriAzionatiAttuale;

    // *** DICHIARAZIONE METODI *** //

   public:
    bool isVuota() const;
    void aggiunge(IdSensoreTipo numeroSensore);
    IdSensoreTipo legge();
};

#endif
