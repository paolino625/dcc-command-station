// *** INCLUDE *** //

#include "EnvironmentConfigSvil.h"

#ifdef SVIL

// *** DEFINIZIONE VARIABILI *** //

LivelloLog livelloLogArduino = LivelloLog::DEBUG_;

#if SENSORI_PRESENTI
// Il primo elemento di ogni lista è 0 e non è utilizzato, lasciare a false
bool sensoriIrPresenti[NUMERO_SENSORI_IR] = {
    false,                                                        // 0
    true,  true, true, true, true, true, true, true, true, true,  // 1-10
    true,  true, true, true, true, true, true, true, true, true,  // 11-20
    true,  true, true, true, true, true, true, true, true, true,  // 21-30
    true,  true, true, true, true, true, true, true, true, true,  // 31-40
    true,  true, true, true, true, true, true, true, true, true,  // 41-50
    true,  true, true, true, true, true, true, true, true, true,  // 51-60
    true,  true, true, true, true, true, true, true, true, true,  // 61-70
    true,  true, true, true, true, true, true, true, true, true,  // 71-80
    true,  true, true, true, true, true, true, true, true, true,  // 81-90
    true,  true, true, true, true, true                           // 91-96
};
#else
bool sensoriIrPresenti[NUMERO_SENSORI_IR] = {
    false,                                                                 // 0
    false, false, false, false, false, false, false, false, false, false,  // 1-10
    false, false, false, false, false, false, false, false, false, false,  // 11-20
    false, false, false, false, false, false, false, false, false, false,  // 21-30
    false, false, false, false, false, false, false, false, false, false,  // 31-40
    false, false, false, false, false, false, false, false, false, false,  // 41-50
    false, false, false, false, false, false, false, false, false, false,  // 51-60
    false, false, false, false, false, false, false, false, false, false,  // 61-70
    false, false, false, false, false, false, false, false, false, false,  // 71-80
    false, false, false, false, false, false, false, false, false, false,  // 81-90
    false, false, false, false, false, false                               // 91-96
};
#endif

bool sensoriBlockDetectorPresenti[NUMERO_SENSORI_BLOCK_DETECTOR] = {false, false, false, false, false, false, false};

#endif