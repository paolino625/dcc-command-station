#ifndef ARDUINO_SENSORIIR_H
#define ARDUINO_SENSORIIR_H

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/sensori/ir/sensore/SensoreIr.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/ir/NumeroSensoriIr.h"

// *** DEFINE *** //

// All’avvio, ArduinoSlaveSensori esegue una scansione di tutti i sensori presenti. Se la lettura di un sensore supera
// la soglia predefinita, il sensore viene considerato guasto o la connessione non funzionante.
#define SOGLIA_MINIMA_SENSORE_FUNZIONANTE 1000

// *** CLASSE *** //

class SensoriIr {
    // *** DICHIARAZIONE VARIABILI *** //

    SensoreIr** sensoriIr = new SensoreIr*[NUMERO_SENSORI_IR];

    // *** COSTRUTTORE *** //

   public:
    SensoriIr() {
        int numeroMultiplexer = 1;
        int numeroIngresso = 1;
        for (int i = 1; i < NUMERO_SENSORI_IR; i++) {
            sensoriIr[i] = new SensoreIr(i, numeroMultiplexer, numeroIngresso);

            // Passiamo all'ingresso successivo
            numeroIngresso += 1;

            // Se aumentare il numero di ingresso ci porta a 17, allora passiamo al multiplexer successivo
            if (numeroIngresso == 17) {
                numeroMultiplexer += 1;
                numeroIngresso = 1;
            }
        }
    }

    // *** DICHIARAZIONE METODI *** //

    void verificaFunzionamento();
    void controllaStato();
    void stampa(LivelloLog livelloLog);

    // ** GETTER ** //

    SensoreIr* getSensore(int id);
    SensoreIr* getSensore(int numeroMultiplexer, int numeroIngresso);
};

#endif
