// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/pin/Pin.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/componentiHardware/output/buzzer/reale/BuzzerAbstractReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/pin/AnalogPinReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/delay/DelayReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE VARIABILI *** //

AnalogPinReale pinBuzzerArduinoSlaveSensori(PIN_BUZZER_ARDUINO_SLAVE_SENSORI);
BuzzerAbstractReale buzzerRealeArduinoSlaveSensori(pinBuzzerArduinoSlaveSensori, delayReale, loggerReale);