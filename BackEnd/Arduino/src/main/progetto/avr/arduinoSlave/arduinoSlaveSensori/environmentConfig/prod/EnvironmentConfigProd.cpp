// *** INCLUDE *** //

#include "EnvironmentConfigProd.h"

#ifdef PROD

// *** DEFINIZIONE VARIABILI *** //

LivelloLog livelloLogArduino = LivelloLog::INFO;

// Il primo elemento di ogni lista è 0 e non è utilizzato, lasciare a false
bool sensoriIrPresenti[NUMERO_SENSORI_IR] = {
    false,                                                        // 0
    true,  true, true, true, true, true, true, true, true, true,  // 1-10
    true,  true, true, true, true, true, true, true, true, true,  // 11-20
    true,  true, true, true, true, true, true, true, true, true,  // 21-30
    true,  true, true, true, true, true, true, true, true, true,  // 31-40
    true,  true, true, true, true, true, true, true, true, true,  // 41-50
    true,  true, true, true, true, true, true, true, true, true,  // 51-60
    true,  true, true, true, true, true, true, true, true, true,  // 61-70
    true,  true, true, true, true, true, true, true, true, true,  // 71-80
    true,  true, true, true, true, true, true, true, true, true,  // 81-90
    true,  true, true, true, true, true                           // 91-96
};

bool sensoriBlockDetectorPresenti[NUMERO_SENSORI_BLOCK_DETECTOR] = {false, false, false, false, false, false, false};

#endif