// *** INCLUDE *** //

#include "SensoriBlockDetection.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/global/EnvironmentConfigGlobal.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE FUNZIONI *** //

void SensoriBlockDetectionReale::inizializza() {
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo sensori block detection... ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
    for (int i = 1; i < NUMERO_SENSORI_BLOCK_DETECTOR; i++) {
        SensoreBlockDetection* sensoreBlockDetector = getSensoreBlockDetector(i);
        sensoreBlockDetector->inizializza();
    }
    LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Inizializzo sensori block detection... OK",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, true);
}

void SensoriBlockDetectionReale::controllaStato() {
    for (int i = 1; i < NUMERO_SENSORI_BLOCK_DETECTOR; i++) {
        SensoreBlockDetection* sensoreBlockDetector = getSensoreBlockDetector(i);
        if (sensoreBlockDetector->isPresente()) {
            sensoreBlockDetector->controllaStato();
        }
    }
}

int SensoriBlockDetectionReale::calcolaIdSensore(int numeroSensore) {
    // Per Arduino Master i sensori BlockDetector partono dal numero 101: il sensore che qui internamente è il sensore
    // n° 1 per Arduino Master è il sensore numero 101. In questa maniera Arduino Master è in grado di distinguere i
    // sensori IR dai sensori BlockDetector.
    return numeroSensore + 100;
}

SensoreBlockDetection* SensoriBlockDetectionReale::getSensoreBlockDetector(int numeroSensore) {
    return sensoriBlockDetector[numeroSensore];
}