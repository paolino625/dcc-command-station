#ifndef DCC_COMMAND_STATION_PIN_ARDUINO_SLAVE_SENSORI_H
#define DCC_COMMAND_STATION_PIN_ARDUINO_SLAVE_SENSORI_H

/*
I multiplexer presentano anche il pin ENABLE.
Pin Enable
- se impostato HIGH interrotto fisicamente il collegamento tra il pin SIG e quello Yxx scelto
- se impostato LOW viene stabilito il collegamento tra il pin SIG e quello Yxx scelto

Dato che vogliamo tutti i demultiplexer sempre attivi, possiamo evitare di collegarlo a un Digital Input di Arduino
Per semplicità possiamo collegarlo al ground.
*/

// *** INCLUDE *** //

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/blockDetection/NumeroSensoriBlockDetection.h"

// *** DEFINE *** //

// Inizializzo Porta Seriale 0 che corrisponde alla porta USB-C
#define PORTA_SERIALE_COMPUTER 0

// Inizializzo Porta Seriale 1 che corrisponde a RX1/TX1 per comunicazione con Arduino Master
#define PORTA_SERIALE_ARDUINO_MASTER 1

#define PIN_BUZZER_ARDUINO_SLAVE_SENSORI 11

// *** DICHIARAZIONE VARIABILI *** //

extern int pinSceltaDemultiplexerSensoriIr[][4];

extern int pinSegnaleDemultiplexerSensoriIr[];

extern int pinSensoriBlockDetector[NUMERO_SENSORI_BLOCK_DETECTOR];

#endif