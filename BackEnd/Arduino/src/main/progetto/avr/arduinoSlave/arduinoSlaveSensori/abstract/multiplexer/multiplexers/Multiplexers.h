#ifndef ARDUINO_MULTIPLEXERS_H
#define ARDUINO_MULTIPLEXERS_H

// *** INCLUDE *** //

#include "NumeroMultiplexer.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/multiplexer/multiplexer/Multiplexer.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/TipiDati.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/multiplexer/MultiplexerCore.h"

// *** CLASSE *** //

class Multiplexers {
    // *** VARIABILI *** //

    Multiplexer **multiplexerArray = new Multiplexer *[NUMERO_MULTIPLEXER];

    // *** COSTRUTTORE *** //

   public:
    Multiplexers(Multiplexer *multiplexers) {
        for (int i = 1; i < NUMERO_MULTIPLEXER; i++) {
            multiplexerArray[i] = new Multiplexer(multiplexers[i]);
        }
    }

    // *** DICHIARAZIONE METODI *** //

    void inizializza();
    void leggePin();

    static IdSensoreTipo calcoloIdSensoreIr(int numeroDemultiplexer, int numeroIngresso);

    Multiplexer *getMultiplexer(int numeroMultiplexer);
};

#endif
