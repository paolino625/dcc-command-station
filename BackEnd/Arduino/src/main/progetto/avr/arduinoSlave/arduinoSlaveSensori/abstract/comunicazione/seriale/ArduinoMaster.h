#ifndef ARDUINO_ARDUINOMASTER_H
#define ARDUINO_ARDUINOMASTER_H

// *** INCLUDE *** //

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/abstract/comunicazione/seriale/codaSensori/CodaSensori.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/comunicazione/porteSeriali/InfoPorteSeriali.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class ArduinoMaster {
    // *** VARIABILI *** //

    char stringaInAttesaAck[NUMERO_CARATTERI_MESSAGGIO_ARDUINO];
    bool attendoAck;
    TimestampTipo millisInvioUltimoMessaggio;

    TimestampTipo timestampUltimoInvioPacchettoAliveArduinoMaster;

    // *** METODI *** //

   public:
    void inviaMessaggioSensoreAzionato(IdSensoreTipo numeroSensoreAttivato);
    void controllaAck();
    void inviaHealthCheck();
    bool inAttesaAck() const;

   private:
    void avviaControlloAck(const char *stringaInviata);
    void inviaKeepAlive();
};

#endif
