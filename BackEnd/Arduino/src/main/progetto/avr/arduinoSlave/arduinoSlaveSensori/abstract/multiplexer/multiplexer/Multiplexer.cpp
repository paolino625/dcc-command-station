// *** INCLUDE *** //

#include "Multiplexer.h"

#include "Arduino.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/sensori/ir/SensoriIrReale.h"
#include "main/progetto/avr/arduinoSlave/comune/reale/infoMultiplexer/InfoMultiplexer.h"

// *** DEFINIZIONE METODI *** //

int Multiplexer::leggePin(int numeroIngresso) {
    multiplexerCore.selezionaPin(numeroIngresso);

    // Effettuo campionamento della lettura del sensore.
    unsigned long somma = 0;
    for (int r = 0; r < NUMERO_CAMPIONAMENTO_SENSORI; r++) {
        int valore = analogRead(multiplexerCore.pinSegnale);
        somma += valore;
    }

    return somma / NUMERO_CAMPIONAMENTO_SENSORI;
}

// Leggo tutti i 16 pin del demultiplexer, salvando il valore letto.
void Multiplexer::leggeTuttiPin() {
    for (int i = 1; i < NUMERO_INGRESSI_MULTIPLEXER; i++) {
        int valorePesato = leggePin(i);

        SensoreIr* sensoreIr = sensoriIrReale.getSensore(id, i);
        sensoreIr->setValore(valorePesato);
    }
}

void Multiplexer::stampaValoriSensori(LivelloLog livelloLog) const {
    for (int i = 1; i < NUMERO_INGRESSI_MULTIPLEXER; i++) {
        SensoreIr* sensoreIr = sensoriIrReale.getSensore(id, i);
        sensoreIr->stampa(livelloLog);
    }
}