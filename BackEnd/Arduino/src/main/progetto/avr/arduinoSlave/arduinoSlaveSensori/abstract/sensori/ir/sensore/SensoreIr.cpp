// *** INCLUDE *** //

#include "SensoreIr.h"

#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/environmentConfig/svil/EnvironmentConfigSvil.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/componentiHardware/buzzer/BuzzerRealeArduinoSlaveSensori.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/comunicazione/seriale/codaSensori/CodaSensoriReale.h"
#include "main/progetto/avr/arduinoSlave/arduinoSlaveSensori/reale/multiplexer/MultiplexersReale.h"
#include "main/progetto/comune/moduliDipendentiArduino/reale/utility/logger/LoggerReale.h"

// *** DEFINIZIONE METODI *** //

void SensoreIr::controllaStato() {
    char buffer[10];
    int numeroSensore = multiplexersReale.calcoloIdSensoreIr(numeroMultiplexer, numeroIngressoMultiplexer);
    if (DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI &&
        numeroSensore == DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI_NUMERO_SENSORE) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Controllo stato sensore IR n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroSensore, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... ",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    bool statoSensoreCorrente;
    bool letturaSensoreOK = false;
    // Leggo valore del sensore corrente: da 0 a 1024 in analogico
    int valoreSensoreCorrente = valore;

    if (DEBUGGING_PRINT_LETTURA_SENSORE && numeroSensore == DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI_NUMERO_SENSORE) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, "Controllo stato sensore IR n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroSensore, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::DEBUG_, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);

        stampa(LivelloLog::DEBUG_);
    }

    // Più il sensore IR si avvicina al bianco, più il numero scende.
    if (valoreSensoreCorrente < sogliaAttivazione + MARGINE_ERRORE_SOGLIA_ATTIVAZIONE) {
        statoSensoreCorrente = true;
        timestampUltimaAttivazioneSensoreIr = millis();
        letturaSensoreOK = true;
    } else if (valoreSensoreCorrente > sogliaDisattivazione - MARGINE_ERRORE_SOGLIA_DISATTIVAZIONE) {
        // Se il sensore è stato azionato da poco, allora potrebbe essere una lettura doppia e
        // dunque non considero nulla.
        if (millis() - timestampUltimaAttivazioneSensoreIr > MILLISECONDI_SENSORE_IR_INATTIVO_DOPO_LETTURA) {
            statoSensoreCorrente = false;
            letturaSensoreOK = true;
        }
    } else {
        // Se non ho raggiunto né il margine soglia attivazione né margine soglia disattivazione,
        // sono in un limbo e non faccio nulla.
    }

    // Verifico lo stato del sensore letto rispetto allo stato precedente salvato.
    // Se lo stato è passato da OFF a ON, devo inviare un messaggio ad Arduino Master, altrimenti mi
    // limito a salvare il nuovo stato.
    if (letturaSensoreOK && statoSensoreCorrente != stato) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Sensore n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroSensore, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, " azionato",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        if (statoSensoreCorrente) {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "OFF -> ON",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
            if (BUZZER_ATTIVO) {
                buzzerRealeArduinoSlaveSensori.beepAvviso();
            }
            codaSensoriReale.aggiunge(numeroSensore);
        } else {
            LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "ON -> OFF",
                                  InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, true);
        }
        // In entrambi i casi salvo il nuovo stato del sensore.
        stato = statoSensoreCorrente;

        stampa(LivelloLog::INFO);

        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                              "Soglia attivazione: ", InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(sogliaAttivazione, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO,
                              "Soglia disattivazione: ", InfoFormattazioneLogger::STESSO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(sogliaDisattivazione, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }

    if (DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI &&
        numeroSensore == DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI_NUMERO_SENSORE) {
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "Controllo stato sensore IR n° ",
                              InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
        itoa(numeroSensore, buffer, 10);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, buffer,
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
        LOG_MESSAGGIO_STATICO(loggerReale, LivelloLog::INFO, "... OK",
                              InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, true);
    }
}

void SensoreIr::stampa(LivelloLog livelloLog) const {
    LOG_MESSAGGIO_STATICO(loggerReale, livelloLog, "Valore sensore n° ",
                          InfoFormattazioneLogger::NUOVO_MESSAGGIO_NUOVA_RIGA, false);
    char buffer[10];
    itoa(id, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, livelloLog, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
    LOG_MESSAGGIO_STATICO(loggerReale, livelloLog, ": ", InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA, false);
    itoa(valore, buffer, 10);
    LOG_MESSAGGIO_STATICO(loggerReale, livelloLog, buffer, InfoFormattazioneLogger::STESSO_MESSAGGIO_STESSA_RIGA,
                          false);
}

int SensoreIr::getNumeroMultiplexer() const { return numeroMultiplexer; }
int SensoreIr::getNumeroIngressoMultiplexer() const { return numeroIngressoMultiplexer; }
int SensoreIr::getValore() const { return valore; }

void SensoreIr::setSogliaAttivazione(int sogliaAttivazione) { this->sogliaAttivazione = sogliaAttivazione; }

void SensoreIr::setSogliaDisattivazione(int sogliaDisattivazione) { this->sogliaDisattivazione = sogliaDisattivazione; }

void SensoreIr::setValore(int valore) { this->valore = valore; }