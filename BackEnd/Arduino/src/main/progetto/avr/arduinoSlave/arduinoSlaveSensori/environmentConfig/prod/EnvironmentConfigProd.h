#ifndef ARDUINO_ENVIRONMENTCONFIGPROD_ARDUINO_SLAVE_SENSORI_H
#define ARDUINO_ENVIRONMENTCONFIGPROD_ARDUINO_SLAVE_SENSORI_H

// *** INCLUDE *** //

// Devo includere ActualEnvironment in modo tale che possa essere definita la variabile che indica in quale
// ambiente siamo.
#include "main/progetto/comune/environmentConfig/global/ActualEnvironment.h"

#ifdef PROD

// *** DEBUGGING AVANZATO *** //

#define DEBUGGING_PRINT_RICEZIONE_PORTA_SERIALE 0

#define DEBUGGING_PRINT_INVIO_PING_ARDUINO_MASTER 0

#define DEBUGGING_PRINT_ACK_TIMEOUT 0

#define DEBUGGING_PRINT_CONTROLLO_STATO_SENSORI 0

#define DEBUGGING_PRINT_LETTURA_SENSORE 0

#define DEBUGGING_CALCOLO_ID_SENSORE_IR 0

#define DEBUGGING_MQTT 0

#define DEBUGGING_SERIALIZZAZIONE_DESERIALIZZAZIONE_JSON 0

// *** IMPOSTAZIONE SOGLIA SENSORE *** //

#define IMPOSTAZIONE_SOGLIA_SENSORE_IR 0
#define NUMERO_MULTIPLEXER_SENSORE_IR_DA_IMPOSTARE 1
#define NUMERO_USCITA_MULTIPLEXER_SENSORE_IR_DA_IMPOSTARE 1

// ** ALTRO ** //

#define VERIFICA_FUNZIONAMENTO_SENSORI 1

// *** LOGGER *** //

extern LivelloLog livelloLogArduino;

// *** MQTT *** //

#define MQTT_ATTIVO 1

// *** PRESENZA HARDWARE *** //

extern bool sensoriIrPresenti[NUMERO_SENSORI_IR];
extern bool sensoriBlockDetectorPresenti[NUMERO_SENSORI_BLOCK_DETECTOR];

#endif

#endif
