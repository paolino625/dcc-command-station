# Introduzione

Abbiamo deciso di utilizzare Google Test per eseguire i test sul codice. Quest'ultima supporta solo piattaforma nativa e

Un'altra opzione sarebbe stata utilizzare UNITY per eseguire i test direttamente su Arduino, ma questa modalità offre
meno funzionalità (niente mock e difficoltà nell'eseguire test su board).

# Spiegazione

## Classi astratte senza dipendenze da Arduino.h

Per poter testare una classe con Google Test, è essenziale che non ci siano dipendenze da Arduino.h, altrimenti non
sarebbe possibile la compilazione sulla piattaforma nativa.
Pertanto, per ogni classe, abbiamo pensato di creare una classe astratta che non contiene riferimenti ad Arduino.h.
Successivamente, abbiamo una classe reale che implementa questi metodi astratti con le chiamate ad Arduino.h.

Prendiamo come esempio la funzione digital.write() che è una delle funzioni basilari di Arduino.h.
Abbiamo creato una classe DigitalPinAbstract che elenca dei metodi astratti per la gestione di un pin digitale di
Arduino, qui dunque non c'è nessun riferimento a digital.write() ma delle funzioni virtuali setHigh e setLow.
Abbiamo poi digitalPinReale che eredita da DigitalPinAbstract e implementa questi metodi con le chiamate ad Arduino.h,
come ad esempio digital.write().

## Classi astratte per creazione dei mock

Esiste una complicazione ulteriore. Prendiamo, ad esempio, la classe ConvoglioAbstract.cpp. Questa classe dipende
interamente da componenti astratti, quindi non include "Arduino.h", il che la rende compilabile sulla piattaforma nativa
e permette di testare la sua logica. Tuttavia, è necessario creare un mock per questa classe per poterla utilizzare nei
test di altre classi che dipendono da essa.

Una classe mock eredita da una classe astratta con metodi virtuali e li sovrascrive.
Di conseguenza, è necessario creare un ulteriore livello di astrazione: la situazione finale è la seguente:

- ConvoglioAbstract, che contiene i metodi astratti
- ConvoglioAbstractReale, che contiene le variabili ed eredita da ConvoglioAbstract le funzioni, implementandole con
  componenti astratti, senza chiamate ad Arduino.h
- ConvoglioAbstractMock, che eredita da ConvoglioAbstract e sovrascrive i metodi astratti con dei mock
- ConvoglioReale, che crea un'istanza di ConvoglioAbstract utilizzando componenti reali.

# Classi testate

Attualmente, abbiamo ritenuto opportuno concentrare i nostri sforzi di testing sulla classe Autopilot, data la sua
notevole complessità.