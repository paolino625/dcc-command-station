// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/conversioneTempo/ConversioneTempo.h"
#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class ConversioniTempoTest : public ::testing::Test {
   protected:
    ConversioniTempoTest() {
        // Puoi fare l'inizializzazione qui.
    }

    ~ConversioniTempoTest() override {
        // Pulizia del codice se necessario.
    }
};

TEST_F(ConversioniTempoTest, TestConvertoMillisecondiAMinuti) {
    TimestampTipo tempoMillisecondi;

    tempoMillisecondi = 60000;  // 1 minuto in millisecondi
    EXPECT_EQ(convertoMillisecondiAMinuti(tempoMillisecondi), 1);

    tempoMillisecondi = 120000;  // 2 minuti in millisecondi
    EXPECT_EQ(convertoMillisecondiAMinuti(tempoMillisecondi), 2);

    tempoMillisecondi = 0;  // 0 minuti in millisecondi
    EXPECT_EQ(convertoMillisecondiAMinuti(tempoMillisecondi), 0);
}

TEST_F(ConversioniTempoTest, TestConvertoMillisecondiASecondi) {
    TimestampTipo tempoMillisecondi;

    tempoMillisecondi = 1000;  // 1 secondo in millisecondi
    EXPECT_EQ(convertoMillisecondiASecondi(tempoMillisecondi), 1);

    tempoMillisecondi = 2000;  // 2 secondi in millisecondi
    EXPECT_EQ(convertoMillisecondiASecondi(tempoMillisecondi), 2);

    tempoMillisecondi = 0;  // 0 secondi in millisecondi
    EXPECT_EQ(convertoMillisecondiASecondi(tempoMillisecondi), 0);
}