// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "componentiHardware/output/display/stampa/core/componente/DisplayComponenteMock.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/encoder/EncoderAbstract.h"
#include "pin/digitalPin/DigitalPinMock.h"
#include "rotabili/convoglio/ConvoglioAbstractMock.h"
#include "utility/DelayMock.h"
#include "utility/StampaMock.h"
#include "utility/TempoMock.h"

// *** CLASSE *** //

class EncoderTest : public ::testing::Test {
   public:
    // *** VARIABILI *** //

    // ** ENCODER ** //

    DigitalPinMock pinClk = DigitalPinMock();
    DigitalPinMock pinDt = DigitalPinMock();
    DigitalPinMock pinSw = DigitalPinMock();

    TempoMock tempo;

    DelayMock delay;

    StampaMock stampa;

    LoggerLocaleAbstract loggerLocale = LoggerLocaleAbstract(LivelloLog::DEBUG_, stampa);
    LoggerAbstract logger = LoggerAbstract(loggerLocale);

    DisplayComponenteMock displayComponente = DisplayComponenteMock(logger);

    DisplayCoreAbstract displayCore = DisplayCoreAbstract(displayComponente, tempo, logger);

    EncoderAbstract encoder = EncoderAbstract(1, pinClk, pinDt, pinSw, tempo, delay, displayCore, logger);

    // ** Convoglio ** //

    ConvoglioAbstractMock convoglio;

    // *** FUNZIONI *** //

    void setupEncoder(int modalitaStepEncoder) {
        // Necessario altrimenti encoder.leggeStato esce subito senza fare nulla
        ON_CALL(convoglio, isInUso()).WillByDefault([]() { return true; });

        // Testo la modalità 1
        encoder.setModalitaStepEncoder(modalitaStepEncoder);

        // Setup encoder
        encoder.inizializza();

        // Necessario perché l'encoder deve memorizzare lastStateClk
        pinClk.setLow();
        encoder.leggeStato(&convoglio);

        // Nello stato iniziale il pulsante non è premuto
        encoder.pulsanteEncoderPremuto = false;
    }

    void ruotoSensoOrario() {
        pinClk.setHigh();
        pinDt.setLow();
    }

    void ruotoSensoAntiorario() {
        pinClk.setHigh();
        pinDt.setHigh();
    }

    void premo(int timeStamp) {
        pinSw.setLow();

        ON_CALL(tempo, milliseconds()).WillByDefault([timeStamp]() { return timeStamp; });
    }

    void rilascio(int timeStamp) {
        pinSw.setHigh();

        ON_CALL(tempo, milliseconds()).WillByDefault([timeStamp]() { return timeStamp; });
    }
};

// *** ROTAZIONE ORARIA BASE *** //

TEST_F(EncoderTest, RotazioneOrariaModalita1) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaInizialeConvoglio + 1, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoOrario();

    encoder.leggeStato(&convoglio);
}

TEST_F(EncoderTest, RotazioneOrariaModalita2) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE2);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaInizialeConvoglio + 10, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoOrario();

    encoder.leggeStato(&convoglio);
}

// *** ROTAZIONE ANTIORARIA BASE *** //

TEST_F(EncoderTest, RotazioneAntiorariaModalita1) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 20;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaInizialeConvoglio - 1, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoAntiorario();

    encoder.leggeStato(&convoglio);
}

TEST_F(EncoderTest, RotazioneAntiorariaModalita2) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 20;

    setupEncoder(STEP_ENCODER_MODE2);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaInizialeConvoglio - 10, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoAntiorario();

    encoder.leggeStato(&convoglio);
}

// *** ROTAZIONE ORARIA CASI LIMITE *** //

TEST_F(EncoderTest, RotazioneOrariaModalita1VelocitaMassimaLocomotiva) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 100;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaInizialeConvoglio, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoOrario();

    encoder.leggeStato(&convoglio);
}

TEST_F(EncoderTest, RotazioneOrariaModalita2StepSuperaVelocitaMassimaLocomotiva) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 95;

    setupEncoder(STEP_ENCODER_MODE2);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaMassimaLocomotiva, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoOrario();

    encoder.leggeStato(&convoglio);
}

// *** ROTAZIONE ANTIORARIA CASO LIMITE *** //

TEST_F(EncoderTest, RotazioneAntiorariaModalita1VelocitaMinimaConvoglio) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaVelocita(velocitaInizialeConvoglio, true, StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    ruotoSensoAntiorario();

    encoder.leggeStato(&convoglio);
}

// *** BOTTONE *** //

TEST_F(EncoderTest, EncoderPremuto) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** EXECUTE *** //

    int timestamp = 10;
    premo(timestamp);

    encoder.leggeStato(&convoglio);

    // *** VERIFY *** //

    // Verifico che la variabile interna pulsanteEncoderPremuto sia true
    EXPECT_EQ(encoder.pulsanteEncoderPremuto, true);

    // Verifico che il tempo sia stato settato correttamente quando l'encoder è stato premuto
    EXPECT_EQ(encoder.pressTime, timestamp);
}

TEST_F(EncoderTest, EncoderPremutoERilasciatoDopoBreveTempoStepEncoderMode1) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** EXECUTE *** //

    premo(100);
    encoder.leggeStato(&convoglio);

    rilascio(200);
    encoder.leggeStato(&convoglio);

    // *** VERIFY *** //

    encoder.modalitaStepEncoder = STEP_ENCODER_MODE2;
}

TEST_F(EncoderTest, EncoderPremutoERilasciatoDopoBreveTempoStepEncoderMode2) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE2);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** EXECUTE *** //

    premo(100);
    encoder.leggeStato(&convoglio);

    rilascio(200);
    encoder.leggeStato(&convoglio);

    // *** VERIFY *** //

    encoder.modalitaStepEncoder = STEP_ENCODER_MODE1;
}

TEST_F(EncoderTest, EncoderPremutoERilasciatoDopoTantoTempo) {
    // *** SETUP *** //

    VelocitaRotabileKmHTipo velocitaMassimaLocomotiva = 100;
    VelocitaRotabileKmHTipo velocitaInizialeConvoglio = 0;

    setupEncoder(STEP_ENCODER_MODE1);

    // *** SETUP MOCK *** //

    ON_CALL(convoglio, getVelocitaMassima()).WillByDefault([velocitaMassimaLocomotiva]() {
        return velocitaMassimaLocomotiva;
    });
    ON_CALL(convoglio, getVelocitaImpostata()).WillByDefault([velocitaInizialeConvoglio]() {
        return velocitaInizialeConvoglio;
    });

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio, cambiaDirezione(StatoAutopilot::NON_ATTIVO));

    // *** EXECUTE *** //

    premo(100);
    encoder.leggeStato(&convoglio);

    rilascio(1000);
    encoder.leggeStato(&convoglio);
}