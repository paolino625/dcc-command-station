// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/orarioPlastico/OrarioPlasticoAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliIndipendentiArduino/utility/conversioneTempo/ConversioneTempo.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "utility/StampaMock.h"
#include "utility/TempoMock.h"

// *** CLASSE *** //

class OrarioPlasticoTest : public ::testing::Test {
    // *** VARIABILI *** //

   protected:
    TempoMock tempo;
    StampaMock stampa;

    LoggerLocaleAbstract loggerLocale = LoggerLocaleAbstract(LivelloLog::DEBUG_, stampa);
    LoggerAbstract logger = LoggerAbstract(loggerLocale);

    OrarioPlasticoAbstract* orarioPlastico;

    void setup(int lunghezzaGiornataInMinuti) {
        orarioPlastico = new OrarioPlasticoAbstract(lunghezzaGiornataInMinuti, tempo, logger);
    }
};

// Se imposto la lunghezza della giornata del plastico a 20 minuti e passano nella realtà 0 minuti, mi aspetto che nel
// plastico sia mezzanotte del primo giorno.
TEST_F(OrarioPlasticoTest, aggiorna_1) {
    // *** SETUP *** //

    int lunghezzaGiornataInMinuti = 20;
    setup(lunghezzaGiornataInMinuti);

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(0));

    // *** EXECUTE *** //

    orarioPlastico->aggiorna();

    // *** VERIFY *** //

    Orario orarioExpected = {1, 0, 0};

    EXPECT_EQ(orarioPlastico->getOrario(), orarioExpected);
}

// Se imposto la lunghezza della giornata del plastico a 20 minuti e passano nella realtà 30 minuti, mi aspetto che nel
// plastico sia passata una giornata e mezza.
TEST_F(OrarioPlasticoTest, aggiorna_2) {
    // *** SETUP *** //

    int lunghezzaGiornataInMinuti = 20;
    setup(lunghezzaGiornataInMinuti);

    unsigned long millisecondi = convertoMinutiAMillisecondi(30);
    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(millisecondi));

    // *** EXECUTE *** //

    orarioPlastico->aggiorna();

    // *** VERIFY *** //

    EXPECT_EQ(orarioPlastico->getGiornata(), 2);
    EXPECT_EQ(orarioPlastico->getOre(), 12);
    EXPECT_EQ(orarioPlastico->getMinuti(), 0);
}

// Se imposto la lunghezza della giornata del plastico a 20 minuti e nella realtà passano 3 minuti, allora mi aspetto
// che siano passate 3 ore e 36 minuti.
//  Per ogni minuto nella realtà sono passati 72 minuti nel plastico: questo perché se si divide la quantità di minuti
//  in una giornata reale (1440) con la quantità di minuti in una giornata del mondo plastico, si ottiene 72.
// Dunque mi aspetto 72 * 3 = 216 minuti, che corrispondono a 3 ore e 36 minuti.
TEST_F(OrarioPlasticoTest, aggiorna_3) {
    // *** SETUP *** //

    int lunghezzaGiornataInMinuti = 20;
    setup(lunghezzaGiornataInMinuti);

    unsigned long millisecondi = convertoMinutiAMillisecondi(3);
    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(millisecondi));

    // *** EXECUTE *** //

    orarioPlastico->aggiorna();

    // *** VERIFY *** //

    EXPECT_EQ(orarioPlastico->getGiornata(), 1);
    EXPECT_EQ(orarioPlastico->getOre(), 3);
    EXPECT_EQ(orarioPlastico->getMinuti(), 36);
}