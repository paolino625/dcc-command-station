// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "comunicazione/MqttArduinoMasterMock.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/pulsanteEmergenza/PulsanteEmergenzaAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "pin/digitalPin/DigitalPinMock.h"
#include "utility/StampaMock.h"

// *** CLASSE *** //

class PulsanteEmergenzaTest : public ::testing::Test {
   protected:
    StampaMock stampa;
    DigitalPinMock pinPulsanteEmergenza{};

    LoggerLocaleAbstract loggerLocale = LoggerLocaleAbstract(LivelloLog::DEBUG_, stampa);
    LoggerAbstract logger = LoggerAbstract(loggerLocale);

    PulsanteEmergenzaAbstract pulsanteEmergenza{pinPulsanteEmergenza, logger};
};

TEST_F(PulsanteEmergenzaTest, nonPremuto) {
    pinPulsanteEmergenza.setHigh();

    EXPECT_FALSE(pulsanteEmergenza.isPremuto());
}

TEST_F(PulsanteEmergenzaTest, premuto) {
    pinPulsanteEmergenza.setLow();

    EXPECT_TRUE(pulsanteEmergenza.isPremuto());
}