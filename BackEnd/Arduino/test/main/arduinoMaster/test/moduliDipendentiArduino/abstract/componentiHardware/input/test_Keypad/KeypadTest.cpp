// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/keypad/core/componente/KeypadComponenteAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "pin/digitalPin/DigitalPinMock.h"
#include "utility/DelayMock.h"
#include "utility/StampaMock.h"

class KeypadTest : public ::testing ::Test {
   protected:
    DelayMock delay;
    StampaMock stampa;

    DigitalPinMock pinRiga1 = DigitalPinMock{};
    DigitalPinMock pinRiga2 = DigitalPinMock{};
    DigitalPinMock pinRiga3 = DigitalPinMock{};
    DigitalPinMock pinRiga4 = DigitalPinMock{};

    DigitalPinMock pinColonna1 = DigitalPinMock{};
    DigitalPinMock pinColonna2 = DigitalPinMock{};
    DigitalPinMock pinColonna3 = DigitalPinMock{};
    DigitalPinMock pinColonna4 = DigitalPinMock{};

    LoggerLocaleAbstract loggerLocale = LoggerLocaleAbstract(LivelloLog::DEBUG_, stampa);
    LoggerAbstract logger = LoggerAbstract(loggerLocale);

    KeypadComponenteAbstract keypad{pinRiga1,    pinRiga2,    pinRiga3,    pinRiga4, pinColonna1,
                                    pinColonna2, pinColonna3, pinColonna4, delay,    logger};

    char carattereLetto;
};

TEST_F(KeypadTest, PremutoTastoA) {
    // A

    pinRiga1.setLow();
    pinRiga2.setHigh();
    pinRiga3.setHigh();
    pinRiga4.setHigh();

    carattereLetto = keypad.leggeCarattere();

    // Confronto il carattere letto con il carattere atteso che è 1
    EXPECT_EQ(carattereLetto, 'A');
}

TEST_F(KeypadTest, PremutoTastoB) {
    // B

    pinRiga1.setHigh();
    pinRiga2.setLow();
    pinRiga3.setHigh();
    pinRiga4.setHigh();

    carattereLetto = keypad.leggeCarattere();

    EXPECT_EQ(carattereLetto, 'B');
}

TEST_F(KeypadTest, PremutoTastoC) {
    pinRiga1.setHigh();
    pinRiga2.setHigh();
    pinRiga3.setLow();
    pinRiga4.setHigh();

    carattereLetto = keypad.leggeCarattere();

    EXPECT_EQ(carattereLetto, 'C');
}

TEST_F(KeypadTest, PremutoTastoD) {
    // D

    pinRiga1.setHigh();
    pinRiga2.setHigh();
    pinRiga3.setHigh();
    pinRiga4.setLow();

    carattereLetto = keypad.leggeCarattere();

    EXPECT_EQ(carattereLetto, 'D');
}

// È difficile testare gli altri tasti perché dovrei creare un mock specifico per DigitalPinMock e far restituire a
// pingRiga1.setHigh un valore diverso a seconda di quante volte è stato chiamato (andremmo così a simulare il ciclo
// for sul NUMERO_COLONNE). Ad esempio, per testare il tasto 3, dovrei far sì che il pinRiga1.setHigh restituisca
// true la prima volta e false la seconda volta.
