// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/stazioni/stazione/StazioneAbstract.h"
#include "utility/ProgrammaMock.h"
#include "utility/StampaMock.h"

// *** CLASSE *** //

class StazioneTest : public ::testing::Test {
   protected:
    StazioneAbstract* stazione;
    ProgrammaMock programma;
    StampaMock stampa;

    LoggerLocaleAbstract loggerLocale = LoggerLocaleAbstract(LivelloLog::DEBUG_, stampa);
    LoggerAbstract logger = LoggerAbstract(loggerLocale);

    ScambiRelayAbstract scambiRelay = ScambiRelayAbstract(logger);
    SezioniAbstract sezioni = SezioniAbstract(scambiRelay, programma, logger);
};

// *** TEST *** //

// Nella funzione inizializza() di StazioneAbstract, tutte le sezioni vengono scansionate e i binari appartenenti alla
// stazione vengono salvati. L'ordine iniziale dei binari è di fatto casuale (varia a seconda dell'ordine dei binari
// all'interno dell'array sezioni). Verifico che la funzione ordinoBinariInBaseAllaLunghezza ordina i binari in base
// alla lunghezza, dal più corto al più lungo.
TEST_F(StazioneTest, inizializza) {
    // *** SETUP *** //

    sezioni.inizializzaInfoBasilari();
    stazione = new StazioneAbstract(3, STAZIONE_CENTRALE, true, true, programma, sezioni, logger);

    // *** EXECUTE *** //

    stazione->inizializza();

    // *** VERIFY *** //

    // Verifico che dopo la chiamata al metodo inizializza, i binari siano stati ordinati in base alla lunghezza.
    EXPECT_TRUE(stazione->getBinario(1)->getId().equals(IdSezioneTipo(1, 3, 7)));
    EXPECT_TRUE(stazione->getBinario(2)->getId().equals(IdSezioneTipo(1, 3, 6)));
    EXPECT_TRUE(stazione->getBinario(3)->getId().equals(IdSezioneTipo(1, 3, 5)));
    EXPECT_TRUE(stazione->getBinario(4)->getId().equals(IdSezioneTipo(1, 3, 4)));
    EXPECT_TRUE(stazione->getBinario(5)->getId().equals(IdSezioneTipo(1, 3, 3)));
    EXPECT_TRUE(stazione->getBinario(6)->getId().equals(IdSezioneTipo(1, 3, 2)));
    EXPECT_TRUE(stazione->getBinario(7)->getId().equals(IdSezioneTipo(1, 3, 1)));
}