// *** INCLUDE *** //

#include <gtest/gtest.h>

#include "autopilot/PercorsiMock.h"
#include "componentiHardware/input/SensoriPosizioneMock.h"
#include "comunicazione/MqttArduinoMasterMock.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/autopilotConvoglioAbstract/AutopilotConvoglioAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/gestioneLocomotive/distanzaFrenata/DistanzaFrenataLocomotivaAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/scambi/ScambiRelayAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/locomotive/LocomotiveAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/sezioni/sezioni/SezioniAbstract.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/tracciato/stazioni/stazioni/StazioniAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"
#include "rotabili/convogli/ConvogliAbstractMock.h"
#include "rotabili/convoglio/ConvoglioAbstractMock.h"
#include "utility/ProgrammaMock.h"
#include "utility/StampaMock.h"
#include "utility/TempoMock.h"

// *** CLASSE *** //

class AutopilotConvoglioTest : public ::testing::Test {
    // *** VARIABILI *** //

   protected:
    ProgrammaMock programma;
    TempoMock tempo;
    StampaMock stampa;
    ConvogliAbstractMock convogli;
    ConvoglioAbstractMock convoglio;
    SensoriPosizioneMock sensoriPosizione;
    MqttArduinoMasterMock mqttArduinoMasterMock;
    PercorsiMock percorsi;
    StazioniAbstract stazioni = StazioniAbstract(programma, sezioni, logger);
    ScambiRelayAbstract scambiRelay = ScambiRelayAbstract(logger);
    SezioniAbstract sezioni = SezioniAbstract(scambiRelay, programma, logger);
    LocomotiveAbstract locomotive = LocomotiveAbstract(programma, tempo, logger);
    AutopilotConvoglioAbstract* autopilotConvoglio;
    LoggerLocaleAbstract loggerLocale = LoggerLocaleAbstract(LivelloLog::DEBUG_, stampa);
    DistanzaFrenataLocomotivaAbstract distanzaFrenataLocomotiva = DistanzaFrenataLocomotivaAbstract(logger);
    LoggerAbstract logger = LoggerAbstract(loggerLocale);

    // *** METODI *** //

    void setup() {
        autopilotConvoglio =
            new AutopilotConvoglioAbstract(convoglio, sensoriPosizione, sezioni, stazioni, tempo, locomotive, percorsi,
                                           programma, distanzaFrenataLocomotiva, logger);
        locomotive.inizializza();
        scambiRelay.inizializza();
        // Inizializzo solo le info basilari delle sezioni.
        // Non inizializzo la posizione dei sensori perché vogliamo avere noi il controllo di dove posizionare i sensori
        // in base ai test che scriviamo.
        sezioni.inizializzaInfoBasilari();

        // Inizializzo le stazioni.
        // N.B. Aggiungo tutte le stazioni del primo piano.

        stazioni.creaStazioneEAggiunge(1, STAZIONE_ESTREMO_EST_PRIMO_PIANO, true, true);
        stazioni.creaStazioneEAggiunge(2, STAZIONE_EST_PRIMO_PIANO, true, false);
        stazioni.creaStazioneEAggiunge(3, STAZIONE_CENTRALE, false, true);
        stazioni.creaStazioneEAggiunge(4, STAZIONE_OVEST_PRIMO_PIANO, true, false);
        stazioni.creaStazioneEAggiunge(5, STAZIONE_ESTREMO_OVEST_PRIMO_PIANO, true, true);

        inizializzoPercorsi();

        // Far restituire l'ID è utile per le varie stampe
        ON_CALL(convoglio, getId()).WillByDefault(testing::Return(1));
    }

    void inizializzoPercorsi() {
        aggiungoPercorso_1_3_1_a_1_2_1();
        aggiungoPercorso_1_3_1_a_1_4_1();
        aggiungoPercorso_1_2_1_a_1_3_1();
        aggiungoPercorso_1_3_1_a_1_1_1();
    }

   private:
    void aggiungoPercorso_1_3_1_a_1_2_1() {
        IdSezioneTipo idSezionePartenza = IdSezioneTipo(1, 3, 1);
        IdSezioneTipo idSezioneArrivo = IdSezioneTipo(1, 2, 1);

        // Mock chiamata esistePercorso

        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(true));

        bool direzionePrimaLocomotivaDestra = true;
        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo, direzionePrimaLocomotivaDestra))
            .WillByDefault(testing::Return(true));

        // Mock chiamata getPercorso

        int numeroIdSezioniIntermedie = 14;
        IdSezioneTipo* idSezioniIntermedie = new IdSezioneTipo[numeroIdSezioniIntermedie]{
            IdSezioneTipo(3, 51, 1), IdSezioneTipo(2, 42),    IdSezioneTipo(3, 52, 2), IdSezioneTipo(3, 53, 1),
            IdSezioneTipo(2, 32),    IdSezioneTipo(3, 55, 1), IdSezioneTipo(2, 30),    IdSezioneTipo(3, 38, 2),
            IdSezioneTipo(2, 28),    IdSezioneTipo(3, 28, 1), IdSezioneTipo(2, 12),    IdSezioneTipo(3, 27, 1),
            IdSezioneTipo(2, 14),    IdSezioneTipo(3, 23, 2)};

        PercorsoAbstract* percorso = new PercorsoAbstract(idSezionePartenza, idSezioneArrivo, idSezioniIntermedie,
                                                          numeroIdSezioniIntermedie, true, logger);
        ON_CALL(percorsi, getPercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(percorso));
    }

    void aggiungoPercorso_1_3_1_a_1_4_1() {
        IdSezioneTipo idSezionePartenza = IdSezioneTipo(1, 3, 1);
        IdSezioneTipo idSezioneArrivo = IdSezioneTipo(1, 4, 1);

        // Mock chiamata esistePercorso

        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(true));

        bool direzionePrimaLocomotivaDestra = false;
        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo, direzionePrimaLocomotivaDestra))
            .WillByDefault(testing::Return(true));

        // Mock chiamata getPercorso

        int numeroIdSezioniIntermedie = 14;
        IdSezioneTipo* idSezioniIntermedie = new IdSezioneTipo[numeroIdSezioniIntermedie]{
            IdSezioneTipo(3, 10, 2), IdSezioneTipo(2, 22),    IdSezioneTipo(3, 9, 1), IdSezioneTipo(3, 8, 2),
            IdSezioneTipo(2, 5),     IdSezioneTipo(3, 6, 2),  IdSezioneTipo(2, 9),    IdSezioneTipo(3, 26, 1),
            IdSezioneTipo(2, 11),    IdSezioneTipo(3, 27, 2), IdSezioneTipo(2, 12),   IdSezioneTipo(3, 28, 2),
            IdSezioneTipo(2, 34),    IdSezioneTipo(3, 40, 1)};

        PercorsoAbstract* percorso = new PercorsoAbstract(idSezionePartenza, idSezioneArrivo, idSezioniIntermedie,
                                                          numeroIdSezioniIntermedie, true, logger);
        ON_CALL(percorsi, getPercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(percorso));
    }

    void aggiungoPercorso_1_2_1_a_1_3_1() {
        IdSezioneTipo idSezionePartenza = IdSezioneTipo(1, 2, 1);
        IdSezioneTipo idSezioneArrivo = IdSezioneTipo(1, 3, 1);

        // Mock chiamata esistePercorso

        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(true));

        bool direzionePrimaLocomotivaDestra = false;
        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo, direzionePrimaLocomotivaDestra))
            .WillByDefault(testing::Return(true));

        // Mock chiamata getPercorso

        int numeroIdSezioniIntermedie = 14;
        IdSezioneTipo* idSezioniIntermedie = new IdSezioneTipo[numeroIdSezioniIntermedie]{
            IdSezioneTipo(3, 23, 2), IdSezioneTipo(2, 14),   IdSezioneTipo(3, 27, 1), IdSezioneTipo(2, 12),
            IdSezioneTipo(3, 28, 1), IdSezioneTipo(2, 28),   IdSezioneTipo(3, 38, 2), IdSezioneTipo(2, 30),
            IdSezioneTipo(3, 55, 1), IdSezioneTipo(2, 32),   IdSezioneTipo(3, 53, 1), IdSezioneTipo(3, 52, 2),
            IdSezioneTipo(2, 42),    IdSezioneTipo(3, 51, 1)};

        PercorsoAbstract* percorso = new PercorsoAbstract(idSezionePartenza, idSezioneArrivo, idSezioniIntermedie,
                                                          numeroIdSezioniIntermedie, true, logger);
        ON_CALL(percorsi, getPercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(percorso));
    }

    void aggiungoPercorso_1_3_1_a_1_1_1() {
        IdSezioneTipo idSezionePartenza = IdSezioneTipo(1, 3, 1);
        IdSezioneTipo idSezioneArrivo = IdSezioneTipo(1, 1, 1);

        // Mock chiamata esistePercorso

        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(true));

        bool direzionePrimaLocomotivaDestra = false;
        ON_CALL(percorsi, esistePercorso(idSezionePartenza, idSezioneArrivo, direzionePrimaLocomotivaDestra))
            .WillByDefault(testing::Return(true));

        // Mock chiamata getPercorso

        int numeroIdSezioniIntermedie = 27;
        IdSezioneTipo* idSezioniIntermedie = new IdSezioneTipo[numeroIdSezioniIntermedie]{
            IdSezioneTipo(3, 10, 2), IdSezioneTipo(2, 22),    IdSezioneTipo(3, 9, 1),  IdSezioneTipo(3, 8, 2),
            IdSezioneTipo(2, 5),     IdSezioneTipo(3, 6, 1),  IdSezioneTipo(3, 7, 1),  IdSezioneTipo(2, 8),
            IdSezioneTipo(3, 22, 1), IdSezioneTipo(2, 10),    IdSezioneTipo(3, 39, 2), IdSezioneTipo(2, 29),
            IdSezioneTipo(3, 54, 1), IdSezioneTipo(2, 31),    IdSezioneTipo(3, 56, 2), IdSezioneTipo(2, 33),
            IdSezioneTipo(3, 36, 2), IdSezioneTipo(2, 44),    IdSezioneTipo(3, 34, 1), IdSezioneTipo(3, 32, 1),
            IdSezioneTipo(3, 31, 1), IdSezioneTipo(3, 30, 1), IdSezioneTipo(3, 16, 1), IdSezioneTipo(2, 7),
            IdSezioneTipo(3, 4, 1),  IdSezioneTipo(2, 2),     IdSezioneTipo(3, 1, 2)};

        PercorsoAbstract* percorso = new PercorsoAbstract(idSezionePartenza, idSezioneArrivo, idSezioniIntermedie,
                                                          numeroIdSezioniIntermedie, true, logger);
        ON_CALL(percorsi, getPercorso(idSezionePartenza, idSezioneArrivo)).WillByDefault(testing::Return(percorso));
    }

   public:
    void inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1() {
        autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
            StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(1, 2, 1), 0);
    }

    void inizializzoAutopilot_modalitaStazioneSuccessiva_1_3_1() {
        autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
            StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(1, 3, 1), 0);
    }

    void inizializzoAutopilot_modalitaStazioneSuccessiva_1_1_1() {
        autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
            StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(1, 1, 1), 0);
    }

    void inizializzoAutopilot_2Stazioni_trattaAndataRitornoNoLoop_direzioneOraria() {
        autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
            StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(1, 4, 1), 0);
    }

    void salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo idSezionePartenza,
                                                            IdSezioneTipo idSezioneArrivo) {
        // Simulo l'esecuzione dello stato SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA

        autopilotConvoglio->sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.arraySezioni =
            percorsi.getPercorso(idSezionePartenza, idSezioneArrivo)->idSezioniIntermedie;
        autopilotConvoglio->sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva.numeroElementiArray =
            percorsi.getPercorso(idSezionePartenza, idSezioneArrivo)
                ->numeroSezioniIntermedieDaStazioneAttualeAStazioneSuccessiva;
    }

    void lockSezioneCapiente() {
        // Simulo l'esecuzione dello stato FERMO_STAZIONE_ATTESA_LOCK_SEZIONE_CAPIENTE
        autopilotConvoglio
            ->lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();
    }

    void aggiungoSensore1AllaSezione(IdSensorePosizioneTipo idSensorePosizione, IdSezioneTipo idSezioneTipo,
                                     bool statoInizialeSensore,
                                     LunghezzaTracciatoTipo distanzaDaSensoreId1AFineSezioneSinistra) {
        // Aggiungo un sensore sul binario di stazione di partenza
        SezioneAbstract* sezione = sezioni.getSezione(idSezioneTipo);
        sezione->setSensore1DistanzaDaSensoreAFineSezioneSinistra(idSensorePosizione,
                                                                  distanzaDaSensoreId1AFineSezioneSinistra);

        SensorePosizioneAbstract* sensorePosizione =
            new SensorePosizioneAbstract(idSensorePosizione, programma, mqttArduinoMasterMock, logger);
        sensorePosizione->setStato(statoInizialeSensore);

        // Quando viene chiamato il mock sensoriPosizione restituiamo l'oggetto sensorePosizione creato da noi sotto il
        // nostro controllo
        ON_CALL(sensoriPosizione, getSensorePosizione(idSensorePosizione))
            .WillByDefault(testing::Return(sensorePosizione));
    }

    void aggiungoSensore2AllaSezione(IdSensorePosizioneTipo idSensorePosizione, IdSezioneTipo idSezioneTipo,
                                     bool statoInizialeSensore,
                                     LunghezzaTracciatoTipo distanzaDaSensoreId2AFineSezioneSinistra) {
        // Aggiungo un sensore sul binario di stazione di partenza
        SezioneAbstract* sezione = sezioni.getSezione(idSezioneTipo);
        sezione->setSensore2DistanzaDaSensoreAFineSezioneSinistra(idSensorePosizione,
                                                                  distanzaDaSensoreId2AFineSezioneSinistra);

        SensorePosizioneAbstract* sensorePosizione =
            new SensorePosizioneAbstract(idSensorePosizione, programma, mqttArduinoMasterMock, logger);
        sensorePosizione->setStato(statoInizialeSensore);

        // Quando viene chiamato il mock sensoriPosizione restituiamo l'oggetto sensorePosizione creato da noi sotto il
        // nostro controllo
        ON_CALL(sensoriPosizione, getSensorePosizione(idSensorePosizione))
            .WillByDefault(testing::Return(sensorePosizione));
    }
};

// *** TEST FUNZIONI VARIE *** //

// ** TROVO INDICE SEZIONE CAPIENTE PER CONVOGLIO ** //

// Verifico che quando viene chiamata la funzione
// trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva, viene trovato l'indice
// della sezione più vicina che sia idonea. Nel nostro caso, la prima sezione abbastanza lunga e non critica è la 4.
TEST_F(
    AutopilotConvoglioTest,
    trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaEsiste_SezioniPrecedentiAQuellaIdoneaNonLungheAbbastanza) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // *** EXECUTE *** //

    int indiceSezioneCapiente =
        autopilotConvoglio
            ->trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    // *** VERIFY *** //

    EXPECT_EQ(indiceSezioneCapiente, 4);
}

// Verifico che quando viene chiamata la funzione
// trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva, sebbene il convoglio
// sia irrealisticamente lungo 1 cm e quindi anche sezioni precedenti alla 4 potrebbero essere lunghe a sufficienza,
// queste non vengono considerate in quanto sezioni critiche. La sezione 4 non viene considerata sezione critica in
// quanto il convoglio sta procedendo in senso antiorario, quindi nel senso di marcia giusto.
TEST_F(
    AutopilotConvoglioTest,
    trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaEsiste_SezioniPrecedentiAQuellaIdoneaCritiche) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(1.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // *** EXECUTE *** //

    int indiceSezioneCapiente =
        autopilotConvoglio
            ->trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    // *** VERIFY *** //

    EXPECT_EQ(indiceSezioneCapiente, 4);
}

// Verifico che quando viene chiamata la funzione
// trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva e non c'è una sezione
// intermedia che sia idonea, venga restituito -1.
TEST_F(
    AutopilotConvoglioTest,
    trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaNonEsiste_SezioniCritiche) {
    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), false);
    inizializzoAutopilot_2Stazioni_trattaAndataRitornoNoLoop_direzioneOraria();

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 4, 1));

    // *** EXECUTE *** //

    int indiceSezioneCapiente =
        autopilotConvoglio
            ->trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    // *** VERIFY *** //

    EXPECT_EQ(indiceSezioneCapiente, -1);
}

// Verifico che quando viene chiamata la funzione
// trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva e non c'è una sezione
// intermedia che sia idonea, venga restituito -1.
TEST_F(
    AutopilotConvoglioTest,
    trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva_SezioneIdoneaNonEsiste_SezioniTroppoCorte) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(300.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // *** EXECUTE *** //

    int indiceSezioneCapiente =
        autopilotConvoglio
            ->trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    // *** VERIFY *** //

    EXPECT_EQ(indiceSezioneCapiente, -1);
}

// ** LOCK SEZIONI ** //

// Quando viene chiamata la funzione
// lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva, verifico
// che vengano bloccate le sezioni fino alla prima sezione idonea. Nel nostro caso, considerando il
// percorso esempio, la prima sezione idonea è la 2, 32. Le sezioni precedenti a
// questa devono essere bloccate, quelle successive no.
TEST_F(AutopilotConvoglioTest,
       lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE;

    // Serve per la funzione
    // trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // *** EXECUTE *** //

    autopilotConvoglio
        ->lockSezioniFinoProssimaSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva();

    // *** VERIFY *** //

    // Controllo manualmente che le sezioni intermedie siano state bloccate
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);

    // Controllo che si è aggiornato l'indice dell'ultima sezione bloccata
    EXPECT_EQ(autopilotConvoglio->indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva, 4);
}

// Verifico che se chiamo la funzione lockSezioni con un array di sezioni di cui una è occupata, la funzione lockSezioni
// non blocchi nessuna sezione e restituisca false
TEST_F(AutopilotConvoglioTest, lockSezioni_UnLockOccupato) {
    // *** SETUP *** //

    setup();

    IdSezioneTipo* arraySezioni = new IdSezioneTipo[3];
    arraySezioni[0] = IdSezioneTipo(1, 1, 1);
    arraySezioni[1] = IdSezioneTipo(1, 2, 1);
    arraySezioni[2] = IdSezioneTipo(1, 3, 1);

    // Simulo che uno dei lock sia stato preso
    sezioni.getSezione(arraySezioni[1])->lock(convoglio.getId());

    // *** EXECUTE *** //

    bool risultatoLock = autopilotConvoglio->lockSezioni(arraySezioni, 3);

    // *** VERIFY *** //

    EXPECT_FALSE(risultatoLock);

    // Mi aspetto che la situazione sia rimasta a quella prima dell'invocazione della funzione
    EXPECT_FALSE(sezioni.getSezione(arraySezioni[0])->isLocked());
    EXPECT_TRUE(sezioni.getSezione(arraySezioni[1])->isLocked());
    EXPECT_FALSE(sezioni.getSezione(arraySezioni[2])->isLocked());
}

// Verifico che se chiamo la funzione lockSezioni con un array di sezioni tutte libere, la funzione lockSezioni blocchi
// tutte le sezioni e restituisca true
TEST_F(AutopilotConvoglioTest, lockSezioni_TuttiLockLiberi) {
    // *** SETUP *** //

    setup();

    IdSezioneTipo* arraySezioni = new IdSezioneTipo[3];
    arraySezioni[0] = IdSezioneTipo(1, 1, 1);
    arraySezioni[1] = IdSezioneTipo(1, 2, 1);
    arraySezioni[2] = IdSezioneTipo(1, 3, 1);

    // *** EXECUTE *** //

    bool risultatoLock = autopilotConvoglio->lockSezioni(arraySezioni, 3);

    // *** VERIFY *** //

    EXPECT_TRUE(risultatoLock);

    EXPECT_TRUE(sezioni.getSezione(arraySezioni[0])->isLocked());
    EXPECT_TRUE(sezioni.getSezione(arraySezioni[1])->isLocked());
    EXPECT_TRUE(sezioni.getSezione(arraySezioni[2])->isLocked());
}

// *** TEST FUNZIONI STATI AUTOPILOT *** //

// ** FERMO STAZIONE ATTESA MOMENTO PARTENZA ** //

// Verifico che la funzione fermoStazioneAttesaMomentoPartenza restituisca true se è arrivato il momento della partenza,
// false altrimenti
TEST_F(AutopilotConvoglioTest, fermoStazioneAttesaMomentoPartenza) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA;
    autopilotConvoglio->millisInizioAttesaPartenzaStazione = 0;
    autopilotConvoglio->delayAttesaPartenzaStazione = 1000;

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(0));

    bool condizione;

    // *** EXECUTE *** //

    condizione = autopilotConvoglio->fermoStazioneAttesaMomentoAttesaPartenza();

    // *** VERIFY *** //

    // Non è passato ancora tempo, quindi mi aspetto che la funzione restituisca false
    EXPECT_FALSE(condizione);

    // *** SETUP *** //

    // Simulo che sia passato il tempo impostato
    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(2000));

    // *** EXECUTE *** //

    condizione = autopilotConvoglio->fermoStazioneAttesaMomentoAttesaPartenza();

    // *** VERIFY *** //

    // Mi aspetto che la funzione restituisca true
    EXPECT_TRUE(condizione);
}

// ** FERMO STAZIONE SALVATAGGIO PERCORSO DA STAZIONE CORRENTE A STAZIONE SUCCESSIVA ** //

// Verifico che la funzione fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva salvi effettivamente
// il percorso dalla stazione corrente a quella successiva
TEST_F(AutopilotConvoglioTest, fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva = StatoAutopilotModalitaStazioneSuccessiva::
        FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA;

    // *** EXECUTE *** //

    autopilotConvoglio->fermoStazioneSalvataggioPercorsoDaStazioneCorrenteAStazioneSuccessiva();

    // *** VERIFY *** //

    ArraySezioni percorsiSezioniIntermedie;
    percorsiSezioniIntermedie.arraySezioni =
        percorsi.getPercorso(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1))->idSezioniIntermedie;
    percorsiSezioniIntermedie.numeroElementiArray = 14;

    EXPECT_TRUE(autopilotConvoglio->sezioniIntermedieDaStazioneAttualeAStazioneSuccessiva == percorsiSezioniIntermedie);
}

// ** FERMO STAZIONE CALCOLO DIREZIONE CONVOGLIO ** //

// Verifico che quando viene chiamata la funzione fermoStazioneCalcoloDirezioneConvoglio, quando esiste un percorso
// dalla stazione di partenza alla stazione di arrivo senza la necessità del cambio di direzione del convoglio, non
// venga cambiata la direzione di quest'ultimo.
TEST_F(AutopilotConvoglioTest, fermoStazioneCalcoloDirezioneConvoglio_NoCambioDirezione) {
    // Se il percorso esiste con la direzione attuale del convoglio, la direzione non deve essere cambiata.

    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO;

    // *** SETUP EXPECTATIONS *** //

    // Mi aspetto che non venga chiamata la funzione cambiaDirezione
    EXPECT_CALL(convoglio, cambiaDirezione(testing::_)).Times(0);

    // *** EXECUTE *** //

    autopilotConvoglio->fermoStazioneCalcoloDirezioneConvoglio();
}

// Verifico che quando viene chiamata la funzione fermoStazioneCalcoloDirezioneConvoglio e non esiste un percorso dalla
// stazione di partenza alla stazione di arrivo con la direzione attuale del convoglio, venga cambiata la direzione di
// quest'ultimo.
TEST_F(AutopilotConvoglioTest, fermoStazioneCalcoloDirezioneConvoglio_CambioDirezione) {
    // Se il percorso esiste con la direzione attuale del convoglio, la direzione non deve essere cambiata.

    // *** SETUP *** //

    setup();
    // In questo caso inverto la direzione del convoglio in modo tale che non esista un percorso diretto
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), false);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO;

    // *** SETUP EXPECTATIONS *** //

    // Mi aspetto che non venga chiamata la funzione cambiaDirezione
    EXPECT_CALL(convoglio, cambiaDirezione(testing::_)).Times(1);

    // *** EXECUTE *** //

    autopilotConvoglio->fermoStazioneCalcoloDirezioneConvoglio();
}

// ** FERMO STAZIONE ATTESA LOCK SEZIONI FINO A PROSSIMA SEZIONE CAPIENTE ** //

// Verifico che quando viene chiamata la funzione fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente, questa
// restituisce true se i lock di tutte le sezioni successive sono liberi.
TEST_F(AutopilotConvoglioTest, fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE;

    // Serve per passare il controllo dell'if
    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(2000));
    autopilotConvoglio->ultimoTentativoLock = 0;

    // Serve per la funzione
    // trovoIndiceSezioneCapientePerConvoglio_SezioniIntermedieDaStazioneAttualeAStazioneSuccessiva
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // *** EXECUTE *** //

    bool lockPresi = autopilotConvoglio->fermoStazioneAttesaLockSezioniFinoAProssimaSezioneCapiente();

    // *** VERIFY *** //

    EXPECT_TRUE(lockPresi);
}

// ** IN PARTENZA STAZIONE ** //

// Verifico che quando la funzione inPartenzaStazione viene chiamata, la velocità del convoglio viene impostata
TEST_F(AutopilotConvoglioTest, inPartenzaStazione) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_PARTENZA_STAZIONE;

    // *** SETUP EXPECTATIONS *** //

    EXPECT_CALL(convoglio,
                cambiaVelocita(VELOCITA_USCITA_LOCOMOTIVA_STAZIONE_FINO_A_PRIMO_SENSORE, true, StatoAutopilot::ATTIVO))
        .Times(1);

    // *** EXECUTE *** //

    autopilotConvoglio->inPartenzaStazione();
}

// ** IN ATTESA PRIMO SENSORE ** //

// Verifico che nello stato IN_ATTESA_PRIMO_SENSORE, se lo stato del primo sensore su cui ci si aspetta il convoglio
// è ancora stato azionato, si passi allo stato IN_MOVIMENTO
TEST_F(AutopilotConvoglioTest, inAttesaPrimoSensore_SensoreAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE;

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // Aggiungo sensore alla sezione 2,32
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 32), true, 10);

    // *** EXECUTE *** //

    bool sensoreAzionato = autopilotConvoglio->inMovimentoInAttesaPrimoSensore();

    // *** VERIFY *** //

    EXPECT_TRUE(sensoreAzionato);
}

// Verifico che nello stato IN_ATTESA_PRIMO_SENSORE, se lo stato del primo sensore su cui ci si aspetta il convoglio non
// è ancora stato azionato, si rimanga nello stato IN_ATTESA_PRIMO_SENSORE
TEST_F(AutopilotConvoglioTest, inAttesaPrimoSensore_SensoreNonAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE;

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // Aggiungo sensore alla sezione 2.32
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 32), false, 10);

    // *** EXECUTE *** //

    bool sensoreAzionato = autopilotConvoglio->inMovimentoInAttesaPrimoSensore();

    // *** VERIFY *** //

    EXPECT_FALSE(sensoreAzionato);
}

// ** IN MOVIMENTO ** //

// * LEGGO PASSAGGIO SENSORE SUCCESSIVO SE ESISTE * //

// Verifico che nello stato IN_MOVIMENTO, se il convoglio non ha ancora raggiunto il secondo sensore del percorso,
// indiceSezioniIntermedie_UltimaSezioneSensoreAzionato sia ancora a -1 e distanzaPercorsaDaSensoreUltimaSezioneCm non
// sia variata.
TEST_F(AutopilotConvoglioTest,
       inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreSezioneIntermedia_SensoreNonAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 10;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    // Aggiungo sensore alla sezione 2.32
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 32), false, 10);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    autopilotConvoglio->leggoPassaggioSensoreSuccessivoSeEsiste();

    // *** VERIFY *** //

    EXPECT_EQ(autopilotConvoglio->indiceSezioniIntermedie_UltimaSezioneSensoreAzionato, -1);
    EXPECT_EQ(autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm, 10);
}

// Verifico che nello stato IN_MOVIMENTO, se il convoglio ha raggiunto il secondo sensore del percorso,
// indiceSezioniIntermedie_UltimaSezioneSensoreAzionato sia stato aggiornato con l'indice dell'ultima sezione su cui è
// stato azionato il sensore, ovvero 4. Inoltre, mi aspetto che distanzaPercorsaDaSensoreUltimaSezioneCm sia stata
// resettata e venga impostato correttamente tipologiaUltimoSensoreAzionato.
TEST_F(AutopilotConvoglioTest,
       inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreSezioneIntermedia_SensoreAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 10;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    // Aggiungo sensore alla sezione 2.32
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 32), true, 10);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    autopilotConvoglio->leggoPassaggioSensoreSuccessivoSeEsiste();

    // *** VERIFY *** //

    EXPECT_EQ(autopilotConvoglio->indiceSezioniIntermedie_UltimaSezioneSensoreAzionato, 4);
    EXPECT_EQ(autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm, 0);
    EXPECT_EQ(autopilotConvoglio->tipologiaUltimoSensoreAzionato, TipologiaUltimoSensoreAzionato::SEZIONE_INTERMEDIA);
}

// Verifico che nello stato IN_MOVIMENTO, se il sensore successivo è il sensore di una stazione di testa ma questo non
// sia stato azionato, la funzione leggoPassaggioSensoreSuccessivoSeEsiste non faccia nulla (ovvero non resetti
// distanzaPercorsaDaSensoreUltimaSezioneCm).
TEST_F(AutopilotConvoglioTest,
       inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneDiTesta_SensoreNonAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia già fatto strada dall'ultimo sensore.
    // In questo modo posso verificare se al passaggio del sensore viene resettata questa distanza.
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 100;

    // Imposto la lunghezza del convoglio superiore alla sezione 2.32 per far sì che durante la chiamata
    // lockSezioneCapiente vengano bloccate tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(217.0));

    // Aggiungo sensore al binario di stazione di arrivo
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 2, 1), false, 10);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    autopilotConvoglio->leggoPassaggioSensoreSuccessivoSeEsiste();

    // *** VERIFY *** //

    // Mi aspetto che la distanza percorsa non sia stata resettata
    EXPECT_EQ(autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm, 100);
}

// Verifico che nello stato IN_MOVIMENTO, se il sensore successivo è il sensore di una stazione di testa e questo sia
// stato azionato, la funzione leggoPassaggioSensoreSuccessivoSeEsiste resetti distanzaPercorsaDaSensoreUltimaSezioneCm.
TEST_F(AutopilotConvoglioTest,
       inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneDiTesta_SensoreAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia già fatto strada dall'ultimo sensore.
    // In questo modo posso verificare se al passaggio del sensore viene resettata questa distanza.
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 100;

    // Imposto la lunghezza del convoglio superiore alla sezione 2.32 per far sì che durante la chiamata
    // lockSezioneCapiente vengano bloccate tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(217.0));

    // Aggiungo sensore al binario di stazione di arrivo
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 2, 1), true, 10);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    autopilotConvoglio->leggoPassaggioSensoreSuccessivoSeEsiste();

    // *** VERIFY *** //

    // Mi aspetto che la distanza percorsa sia stata resettata
    EXPECT_EQ(autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm, 0);
    EXPECT_EQ(autopilotConvoglio->tipologiaUltimoSensoreAzionato, TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1);
}

// Verifico che nello stato IN_MOVIMENTO, se il sensore successivo è il primo sensore di una stazione simmetrica e
// questo sia stato azionato, la funzione leggoPassaggioSensoreSuccessivoSeEsiste resetti
// distanzaPercorsaDaSensoreUltimaSezioneCm e venga impostato correttamente tipologiaUltimoSensoreAzionato.
TEST_F(
    AutopilotConvoglioTest,
    inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneSimmetrica_PrimoSensoreAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 2, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_3_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia già fatto strada dall'ultimo sensore.
    // In questo modo posso verificare se al passaggio del sensore viene resettata questa distanza.
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 100;

    // Imposto la lunghezza del convoglio superiore alla sezione 2.32 per far sì che durante la chiamata
    // lockSezioneCapiente vengano bloccate tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(217.0));

    // Aggiungo sensori al binario di stazione di arrivo
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 3, 1), true, 10);
    aggiungoSensore2AllaSezione(102, IdSezioneTipo(1, 3, 1), true, 20);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 2, 1), IdSezioneTipo(1, 3, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    autopilotConvoglio->leggoPassaggioSensoreSuccessivoSeEsiste();

    // *** VERIFY *** //

    // Mi aspetto che la distanza percorsa sia stata resettata
    EXPECT_EQ(autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm, 0);
    EXPECT_EQ(autopilotConvoglio->tipologiaUltimoSensoreAzionato, TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1);
}

// Verifico che nello stato IN_MOVIMENTO, se il sensore successivo è il secondo sensore di una stazione simmetrica e
// questo sia stato azionato, la funzione leggoPassaggioSensoreSuccessivoSeEsiste resetti
// distanzaPercorsaDaSensoreUltimaSezioneCm e venga impostato correttamente tipologiaUltimoSensoreAzionato.
TEST_F(
    AutopilotConvoglioTest,
    inMovimento_leggoPassaggioSensoreSuccessivoSeEsiste_SensoreStazioneArrivo_StazioneSimmetrica_SecondoSensoreAzionato) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 2, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_3_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia già fatto strada dall'ultimo sensore.
    // In questo modo posso verificare se al passaggio del sensore viene resettata questa distanza.
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 100;

    // Simulo che il convoglio sia già passato sul primo sensore
    autopilotConvoglio->tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1;

    // Imposto la lunghezza del convoglio superiore alla sezione 2.32 per far sì che durante la chiamata
    // lockSezioneCapiente vengano bloccate tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(217.0));

    // Aggiungo sensori al binario di stazione di arrivo
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 3, 1), true, 10);
    aggiungoSensore2AllaSezione(102, IdSezioneTipo(1, 3, 1), true, 20);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 2, 1), IdSezioneTipo(1, 3, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    autopilotConvoglio->leggoPassaggioSensoreSuccessivoSeEsiste();

    // *** VERIFY *** //

    // Mi aspetto che la distanza percorsa sia stata resettata
    EXPECT_EQ(autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm, 0);
    EXPECT_EQ(autopilotConvoglio->tipologiaUltimoSensoreAzionato, TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_2);
}

// * AGGIORNO DISTANZA PROSSIMO STOP * //

// CALCOLO DISTANZA DA STAZIONE CORRENTE A STAZIONE SUCCESSIVA //

// Quando il convoglio è appena entrato nello stato IN_MOVIMENTO ed è passato sul primo sensore ed ha bloccato
// sezioni fino alla sezione 2.32 (che è una sezione lunga meno di
// SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE), viene calcolata in maniera corretta la
// distanzaFinoAProssimoStopCm
TEST_F(AutopilotConvoglioTest,
       inMovimento_aggiornoDistanzaProssimoStop_DaStazionePartenza_LockFinoASezioneIntermediaLunghezzaNormale) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio sia già passato sul primo sensore (avviene in IN_ATTESA_PRIMO_SENSORE)
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    // Simulo che la lunghezza della sezione di arrivo sia minore di
    // SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE
    sezioni.getSezione(IdSezioneTipo(2, 32))->setLunghezzaCm(100);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm =
        autopilotConvoglio->calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // *** VERIFY *** //

    LunghezzaTracciatoTipo distanzaFinoAProssimoStopExpected =
        sezioni.getSezione(IdSezioneTipo(1, 3, 1))->getDistanzaDaSensoreAFineSezioneCm(true, 0) +
        sezioni.getSezione(IdSezioneTipo(3, 51, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 42))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 52, 2))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 53, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 32))->getLunghezzaCm() -
        DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA;

    EXPECT_EQ(distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm, distanzaFinoAProssimoStopExpected);
}

// Quando il convoglio è appena entrato nello stato IN_MOVIMENTO ed è passato sul primo sensore ed ha bloccato
// sezioni fino alla sezione 2.32 (che è una sezione più lunga di
// SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE), viene calcolata in maniera corretta la
// distanzaFinoAProssimoStopCm
TEST_F(AutopilotConvoglioTest,
       inMovimento_aggiornoDistanzaProssimoStop_DaStazionePartenza_LockFinoASezioneIntermediaLunghezzaOltreLaSoglia) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio sia già passato sul primo sensore (avviene in IN_ATTESA_PRIMO_SENSORE)
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    // Simuliamo che la sezione sia più lunga di SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE
    LunghezzaTracciatoTipo lunghezzaSezioneOltreLaSoglia = 100;

    // Simulo che la lunghezza della sezione di arrivo sia maggiore di
    // SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE
    sezioni.getSezione(IdSezioneTipo(2, 32))
        ->setLunghezzaCm(SOGLIA_LUNGHEZZA_CM_SEZIONE_INTERMEDIA_ERRORE_BASILARE_SUFFICIENTE +
                         lunghezzaSezioneOltreLaSoglia);

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm =
        autopilotConvoglio->calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // *** VERIFY *** //

    // Calcolo i cm di errore di posizionamento che devono essere aggiunti all'errore di posizionamento basilare.
    // Ci aspettiamo che per ogni 10 cm in più della sezione, vengano aggiunti tot cm di errore di posizionamento.
    LunghezzaTracciatoTipo errorePosizionamentoDaAggiungere =
        lunghezzaSezioneOltreLaSoglia / 10 *
        DISTANZA_CM_ERRORE_POSIZIONAMENTO_AGGIUNTIVO_PER_OGNI_10_CM_SEZIONE_INTERMEDIA;

    LunghezzaTracciatoTipo distanzaFinoAProssimoStopExpected =
        sezioni.getSezione(IdSezioneTipo(1, 3, 1))->getDistanzaDaSensoreAFineSezioneCm(true, 0) +
        sezioni.getSezione(IdSezioneTipo(3, 51, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 42))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 52, 2))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 53, 1))->getLunghezzaCm() +

        sezioni.getSezione(IdSezioneTipo(2, 32))->getLunghezzaCm() -
        // La nuova distanza di stop dalla fine della sezione è quella minima + l'errore di posizionamento aggiuntivo.
        (DISTANZA_MINIMA_CM_STOP_CONVOGLIO_DA_FINE_SEZIONE_INTERMEDIA + errorePosizionamentoDaAggiungere);

    EXPECT_EQ(distanzaDaUltimoSensoreAzionatoFinoAProssimoStopCm, distanzaFinoAProssimoStopExpected);
}

// Quando il convoglio è appena entrato nello stato IN_MOVIMENTO ed è passato sul primo sensore ed ha bloccato
// sezioni fino alla sezione della stazione di arrivo, viene calcolata in maniera corretta la
// distanzaFinoAProssimoStopCm
TEST_F(AutopilotConvoglioTest, inMovimento_aggiornoDistanzaProssimoStop_DaStazionePartenza_LockTutteSezioniIntermedie) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio sia già passato sul primo sensore (avviene in IN_ATTESA_PRIMO_SENSORE)
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente. Facciamo il convoglio lungo così non viene bloccata la sezione
    // intermedia 2.32 ma tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(216.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // *** EXECUTE *** //

    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
        autopilotConvoglio->calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // *** VERIFY *** //

    LunghezzaTracciatoTipo distanzaFinoAProssimoStopExpected =
        sezioni.getSezione(IdSezioneTipo(1, 3, 1))->getDistanzaDaSensoreAFineSezioneCm(true, 0) +
        sezioni.getSezione(IdSezioneTipo(3, 51, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 42))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 52, 2))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 53, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 32))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 55, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 30))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 38, 2))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 28))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 28, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 12))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 27, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 14))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(3, 23, 2))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(1, 2, 1))->getLunghezzaCm() -
        DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA;

    EXPECT_EQ(distanzaDaUltimoSensoreAzionatoFinoAProssimoStop, distanzaFinoAProssimoStopExpected);
}

// Quando il convoglio è nello stato IN_MOVIMENTO da un po' ed è passato sul sensore della stazione di arrivo, viene
// calcolata in maniera corretta la distanzaFinoAProssimoStopCm
TEST_F(AutopilotConvoglioTest, inMovimento_aggiornoDistanzaProssimoStop_InArrivoStazioneDestinazione_StazioneDiTesta) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia bloccato fino alla sezione intermedia 14, ovvero l'ultima
    autopilotConvoglio->indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva = 14;

    // Simulo che sia stata già chiamata la funzione leggoPassaggioSensoreSuccessivoSeEsiste che imposta
    // tipologiaUltimoSensoreAzionato a STAZIONE quando il sensore sulla stazione viene azionato
    autopilotConvoglio->tipologiaUltimoSensoreAzionato = STAZIONE_SENSORE_1;

    // Simulo che il convoglio sia appena passato sul sensore di stazione
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente. Facciamo il convoglio lungo così non viene bloccata la sezione
    // intermedia 2.32 ma tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(216.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // Aggiungo sensore alla sezione 1.2.1
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 2, 1), true, 183);

    // *** EXECUTE *** //

    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
        autopilotConvoglio->calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // *** VERIFY *** //

    // Mi aspetto che la distanza fino al prossimo stop sia uguale alla distanza tra il sensore e la fine della sezione
    // meno la distanza che deve essere lasciata.
    SezioneAbstract* sezioneBinarioStazione = sezioni.getSezione(IdSezioneTipo(1, 2, 1));
    LunghezzaTracciatoTipo distanzaDalSensoreAFineSezioneCm =
        sezioneBinarioStazione->getDistanzaDaSensoreAFineSezioneCm(true, 0);
    LunghezzaTracciatoTipo distanzaFinoAProssimoStopExpected =
        distanzaDalSensoreAFineSezioneCm - DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA;

    EXPECT_EQ(distanzaDaUltimoSensoreAzionatoFinoAProssimoStop, distanzaFinoAProssimoStopExpected);
}

// Quando il convoglio è nello stato IN_MOVIMENTO da un po' ed è passato sul sensore della stazione di arrivo, viene
// calcolata in maniera corretta la distanzaFinoAProssimoStopCm
TEST_F(AutopilotConvoglioTest,
       inMovimento_aggiornoDistanzaProssimoStop_InArrivoStazioneDestinazione_StazioneSimmetrica_PrimoSensore) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 2, 1), false);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_3_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia bloccato fino alla sezione intermedia 14, ovvero l'ultima
    autopilotConvoglio->indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva = 14;

    // Simulo che sia stata già chiamata la funzione leggoPassaggioSensoreSuccessivoSeEsiste che imposta
    // tipologiaUltimoSensoreAzionato a STAZIONE_SENSORE_1 quando il primo sensore sulla stazione viene azionato
    autopilotConvoglio->tipologiaUltimoSensoreAzionato = STAZIONE_SENSORE_1;

    // Simulo che il convoglio sia appena passato sul sensore di stazione
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente. Facciamo il convoglio lungo così vengono bloccate tutte le sezioni intermedie
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(216.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 2, 1), IdSezioneTipo(1, 3, 1));

    // Aggiungo 1° sensore alla sezione di stazione
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 3, 1), true, 60);

    // Aggiungo 2° sensore alla sezione di stazione
    aggiungoSensore2AllaSezione(102, IdSezioneTipo(1, 3, 1), true, 40);

    // *** EXECUTE *** //

    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
        autopilotConvoglio->calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // *** VERIFY *** //

    // Dato che il sensore azionato è il primo, mi aspetto che la distanza fino al prossimo stop sia uguale alla
    // distanzaDaInizioSezioneAStopConvoglioBinarioStazione meno la distanza da inizio della sezione al sensore

    IdSezioneTipo idSezione = IdSezioneTipo(1, 3, 1);
    SezioneAbstract* sezione = sezioni.getSezione(idSezione);

    LunghezzaTracciatoTipo distanzaDaInizioSezioneAStopConvoglioBinarioStazione =
        autopilotConvoglio->calcoloDistanzaDaInizioSezioneAStopConvoglioBinarioStazione(sezione);

    LunghezzaTracciatoTipo distanzaDaInizioSezioneASensore = sezione->getDistanzaDaSensoreAInizioSezioneCm(true, 1);

    LunghezzaTracciatoTipo distanzaFinoAProssimoStopExpected =
        distanzaDaInizioSezioneAStopConvoglioBinarioStazione - distanzaDaInizioSezioneASensore;

    EXPECT_EQ(distanzaFinoAProssimoStopExpected, distanzaDaUltimoSensoreAzionatoFinoAProssimoStop);
}

// Quando il convoglio è nello stato IN_MOVIMENTO da un po' ed è passato sul sensore della stazione di arrivo, viene
// calcolata in maniera corretta la distanzaFinoAProssimoStopCm
TEST_F(AutopilotConvoglioTest,
       inMovimento_aggiornoDistanzaProssimoStop_InArrivoStazioneDestinazione_StazioneSimmetrica_SecondoSensore) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 2, 1), false);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_3_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio abbia bloccato fino alla sezione intermedia 14, ovvero l'ultima
    autopilotConvoglio->indiceUltimaSezioneBloccataSezioniIntermediaDaStazioneAttualeAStazioneSuccessiva = 14;

    // Simulo che sia stata già chiamata la funzione leggoPassaggioSensoreSuccessivoSeEsiste che imposta
    // tipologiaUltimoSensoreAzionato a STAZIONE_SENSORE_2 quando il secondo sensore sulla stazione viene azionato
    autopilotConvoglio->tipologiaUltimoSensoreAzionato = STAZIONE_SENSORE_2;

    // Simulo che il convoglio sia appena passato sul sensore di stazione
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente. Facciamo il convoglio lungo così vengono bloccate tutte le sezioni intermedie
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(216.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 2, 1), IdSezioneTipo(1, 3, 1));

    // Aggiungo 1° sensore alla sezione di stazione
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(1, 3, 1), true, 60);

    // Aggiungo 2° sensore alla sezione di stazione
    aggiungoSensore2AllaSezione(102, IdSezioneTipo(1, 3, 1), true, 40);

    // *** EXECUTE *** //

    LunghezzaTracciatoTipo distanzaDaUltimoSensoreAzionatoFinoAProssimoStop =
        autopilotConvoglio->calcoloDistanzaDaUltimoSensoreAzionatoFinoAProssimoStop();

    // *** VERIFY *** //

    // Dato che il sensore azionato è il primo, mi aspetto che la distanza fino al prossimo stop sia uguale alla
    // distanzaDaInizioSezioneAStopConvoglioBinarioStazione meno la distanza da inizio della sezione al sensore

    IdSezioneTipo idSezione = IdSezioneTipo(1, 3, 1);
    SezioneAbstract* sezione = sezioni.getSezione(idSezione);

    LunghezzaTracciatoTipo distanzaDaInizioSezioneAStopConvoglioBinarioStazione =
        autopilotConvoglio->calcoloDistanzaDaInizioSezioneAStopConvoglioBinarioStazione(sezione);

    LunghezzaTracciatoTipo distanzaDaInizioSezioneASensore = sezione->getDistanzaDaSensoreAInizioSezioneCm(true, 2);

    LunghezzaTracciatoTipo distanzaFinoAProssimoStopExpected =
        distanzaDaInizioSezioneAStopConvoglioBinarioStazione - distanzaDaInizioSezioneASensore;

    EXPECT_EQ(distanzaFinoAProssimoStopExpected, distanzaDaUltimoSensoreAzionatoFinoAProssimoStop);
}

// Quando il convoglio arriva alla stazione di destinazione, l'algoritmo inverte la direzione del convoglio per ogni
// sezione con inversione di polarità incontrata lungo il percorso. Nel caso in cui non ci siano sezioni con inversione
// di polarità, la direzione del convoglio non cambia
TEST_F(AutopilotConvoglioTest, arrivatoStazioneDestinazione_percorsoSenzaSezioniConInversioneDiPolarita) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::ARRIVATO_STAZIONE_DESTINAZIONE;

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));

    // Imposto la direzione del convoglio

    autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra = true;

    // *** EXECUTE *** //

    autopilotConvoglio->processo(false);

    // *** VERIFY *** //

    // Non essendoci sezioni con inversione di polarità, mi aspetto che la direzione del convoglio non cambi
    EXPECT_EQ(true, autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra);
}

// Quando il convoglio arriva alla stazione di destinazione, l'algoritmo inverte la direzione del convoglio per ogni
// sezione con inversione di polarità incontrata lungo il percorso. Nel caso in cui lungo il percorso sia presente una
// sezione con inversione di polarità, la direzione del convoglio viene invertita.
TEST_F(AutopilotConvoglioTest, arrivatoStazioneDestinazione_percorsoConSezioneConInversioneDiPolarita) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_1_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::ARRIVATO_STAZIONE_DESTINAZIONE;

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 1, 1));

    // Imposto la direzione del convoglio

    autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra = false;

    // *** EXECUTE *** //

    autopilotConvoglio->processo(false);

    // *** VERIFY *** //

    // Non essendoci sezioni con inversione di polarità, mi aspetto che la direzione del convoglio non cambi
    EXPECT_EQ(true, autopilotConvoglio->posizioneConvoglio.direzionePrimaLocomotivaDestra);
}

// * AGGIORNO VELOCITA CONVOGLIO IN BASE A DISTANZA PROSSIMO STOP * //

// Quando viene chiamata la funzione aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop(), la velocità del convoglio
// viene aumentata di 1 se lo spazio disponibile per la frenata è maggiore dello spazio di frenata dello step attuale
TEST_F(AutopilotConvoglioTest, inMovimento_aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio sia già passato sul primo sensore (avviene in IN_ATTESA_PRIMO_SENSORE)
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente. Facciamo il convoglio lungo così non viene bloccata la sezione
    // intermedia 2.32 ma tutte le sezioni intermedie.
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(216.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // Simulo che il convoglio sia già partito dalla stazione (avviene in IN_PARTENZA_STAZIONE)
    ON_CALL(convoglio, getVelocitaAttuale())
        .WillByDefault(testing::Return(VELOCITA_USCITA_LOCOMOTIVA_STAZIONE_FINO_A_PRIMO_SENSORE));

    // Imposto locomotiva
    LocomotivaAbstract* locomotiva = new LocomotivaAbstract(1, tempo, logger);
    ON_CALL(convoglio, getLocomotivaPiuVeloce()).WillByDefault(testing::Return(locomotiva->getId()));

    // *** SETUP EXPECTATIONS *** //

    // Mi aspetto che il convoglio acceleri.
    EXPECT_CALL(convoglio, cambiaVelocita(1, false, StatoAutopilot::ATTIVO)).Times(1);

    // *** EXECUTE *** //

    autopilotConvoglio->aggiornoVelocitaConvoglioInBaseADistanzaProssimoStop();
}

// * LOCK SEZIONI FUTURO * //

// Il convoglio è riuscito a bloccare fino a una sezione intermedia. Verifico che se viene chiamato
// autopilot->lockSezioniFuturo, il convoglio riesce a bloccare tutte le sezioni fino alla stazione di destinazione
// (dato che lunghezzaTotaleSezioniBloccabili è molto grande)
TEST_F(AutopilotConvoglioTest, inMovimento_lockSezioniFuturo) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Simulo che il convoglio sia già passato sul primo sensore (avviene in IN_ATTESA_PRIMO_SENSORE)
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 0;

    // Necessario per lockSezioneCapiente
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // Imposto la lunghezza totale delle sezioni bloccabili
    autopilotConvoglio->lunghezzaTotaleSezioniBloccabili = 10000;

    // *** PRE-VERIFY *** //

    // Mi aspetto che siano bloccate le sezioni fino a 2.32 incluso
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), false);

    // *** EXECUTE *** //

    autopilotConvoglio->lockSezioniFuturo();

    // *** VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);

    // Mi aspetto che siano bloccate anche le seguenti sezioni
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), true);
}

// * UNLOCK SEZIONI PASSAGGIO CONVOGLIO AVVENUTO * //

// Se il convoglio si trova alla fine della sezione 2.30 e un sensore nella sezione 2.32, il convoglio è lungo 40 cm;
// quando viene chiamata la funzione unlockSezioniPassaggioConvoglioAvvenuto, tutte le sezioni precedenti vengono
// sbloccate.
TEST_F(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinario1) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    // Aggiungo sensore alla sezione 2,32
    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 32), true, 100);

    // Necessario per lockSezioneCapiente
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    // Blocco fino alla sezione 2.32
    lockSezioneCapiente();

    // Blocco fino alla sezione 2.30
    lockSezioneCapiente();

    // Simulo che il convoglio sia arrivato alla fine della sezione 2.30
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm =
        sezioni.getSezione(IdSezioneTipo(2, 32))->getDistanzaDaSensoreAFineSezioneCm(true, 0) +
        sezioni.getSezione(IdSezioneTipo(3, 55, 1))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(2, 30))->getLunghezzaCm();
    autopilotConvoglio->indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = 4;

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
    // Questa viene bloccata prima che il convoglio parta in modalità autopilot
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);

    // *** EXECUTE *** //

    autopilotConvoglio->unlockSezioniPassaggioConvoglioAvvenuto();

    // *** VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);
}

// Se il convoglio si trova sulla sezione 1.2.1 (ma non ha ancora liberato 3.23.2) e l'ultimo sensore azionato è nella
// sezione 2.14; quando viene chiamata la funzione unlockSezioniPassaggioConvoglioAvvenuto, tutte le sezioni
// eccetto 3.23.2 vengono sbloccate.
TEST_F(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinario2) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 14), true, 20);

    // Necessario per lockSezioneCapiente
    LunghezzaTracciatoTipo lunghezzaConvoglioCm = 40;
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(lunghezzaConvoglioCm));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // Blocco tutte le sezioni del futuro (per fare ciò imposto 10 metri così posso bloccare tutte le sezioni
    // intermedie)
    autopilotConvoglio->lunghezzaTotaleSezioniBloccabili = 1000;
    autopilotConvoglio->lockSezioniFuturo();

    // Simulo che il convoglio sia arrivato quasi alla fine della sezione 1.2.1 ma che ci sia una parte ancora sullo
    // scambio 3.23.2.
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm =
        sezioni.getSezione(IdSezioneTipo(2, 14))->getDistanzaDaSensoreAFineSezioneCm(true, 0) +
        sezioni.getSezione(IdSezioneTipo(3, 23, 2))->getLunghezzaCm() +
        (lunghezzaConvoglioCm - 5);  // In questo modo il convoglio non entra
                                     // completamente sul binario di stazione ma rimane per 10 cm sullo scambio 3.23.2
    autopilotConvoglio->indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = 12;

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), true);
    // Questa viene bloccata prima che il convoglio parta in modalità autopilot
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);

    // *** EXECUTE *** //

    autopilotConvoglio->unlockSezioniPassaggioConvoglioAvvenuto();

    // *** VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);
}

// Se il convoglio è arrivato sulla sezione 1.2.1  e l'ultimo sensore azionato è nella sezione 2.14:
// quando viene chiamata la funzione unlockSezioniPassaggioConvoglioAvvenuto, tutte le sezioni precedenti vengono
// sbloccate (tranne la sezione della stazione di arrivo)
TEST_F(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinario3) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    aggiungoSensore1AllaSezione(101, IdSezioneTipo(2, 14), true, 20);

    // Necessario per lockSezioneCapiente
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // Blocco tutte le sezioni del futuro (per fare ciò imposto 10 metri così posso bloccare tutte le sezioni
    // intermedie)
    autopilotConvoglio->lunghezzaTotaleSezioniBloccabili = 1000;
    autopilotConvoglio->lockSezioniFuturo();

    // Simulo che il convoglio sia arrivato alla sezione della stazione di arrivo
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm =
        sezioni.getSezione(IdSezioneTipo(2, 14))->getDistanzaDaSensoreAFineSezioneCm(true, 0) +
        sezioni.getSezione(IdSezioneTipo(3, 23, 2))->getLunghezzaCm() +
        sezioni.getSezione(IdSezioneTipo(1, 2, 1))->getLunghezzaCm() -
        DISTANZA_CM_STOP_CONVOGLIO_DA_FINE_BINARIO_STAZIONE_DI_TESTA;  // Simulo che il convoglio sia entrato
                                                                       // completamente sul binario di stazione,
                                                                       // fermandosi al punto di stop.
    autopilotConvoglio->indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = 12;

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), true);
    // Questa viene bloccata prima che il convoglio parta in modalità autopilot
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);

    // *** EXECUTE *** //

    autopilotConvoglio->unlockSezioniPassaggioConvoglioAvvenuto();

    // *** VERIFY *** //

    // EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), false);

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);
}

// Se il convoglio è arrivato sul binario della stazione di destinazione (sezione 1.2.1) e ha azionato il sensore sulla
// medesima sezione: quando viene chiamata la funzione unlockSezioniPassaggioConvoglioAvvenuto, tutte le sezioni vengono
// sbloccate (tranne la sezione della stazione di arrivo)
TEST_F(AutopilotConvoglioTest, inMovimento_unlockSezioniPassaggioConvoglioAvvenuto_SensoreBinarioStazioneDestinazione) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::IN_MOVIMENTO;

    aggiungoSensore1AllaSezione(150, IdSezioneTipo(1, 2, 1), true, 150);

    // Necessario per lockSezioneCapiente
    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(100.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    // Blocco tutte le sezioni del futuro (per fare ciò imposto 10 metri così posso bloccare tutte le sezioni
    // intermedie)
    autopilotConvoglio->lunghezzaTotaleSezioniBloccabili = 1000;
    autopilotConvoglio->lockSezioniFuturo();

    autopilotConvoglio->indiceSezioniIntermedie_UltimaSezioneSensoreAzionato = 12;

    // Simulo che il convoglio si trovi a una certa distanza dal sensore (ormai passato)
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 5;

    // Simulo che l'ultimo sensore azionato sia quello di stazione
    autopilotConvoglio->tipologiaUltimoSensoreAzionato = TipologiaUltimoSensoreAzionato::STAZIONE_SENSORE_1;

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), true);
    // Questa viene bloccata prima che il convoglio parta in modalità autopilot
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);

    // *** EXECUTE *** //

    autopilotConvoglio->unlockSezioniPassaggioConvoglioAvvenuto();

    // *** VERIFY *** //

    // EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 3, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 38, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 28))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 28, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 12))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 27, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 14))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 23, 2))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(1, 2, 1))->isLocked(), true);
}

// ** FERMO SEZIONE ATTESA LOCK SEZIONI ** //

// Il convoglio si è fermato in una sezione intermedia. Quando viene chiamato autopilot->processo e le sezioni
// successive fino al primo blocco capiente sono libere, il convoglio riesce a bloccarle e lo stato dell'autopilot viene
// cambiato
TEST_F(AutopilotConvoglioTest, fermoSezioneAttesaLockSezioni_SezioniSuccessiveLibere) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI;

    // Simulo che il convoglio sia già passato su un sensore in delle sezioni precedenti
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 100;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(2000));

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), false);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), false);

    // *** EXECUTE *** //

    bool lockSezioni = autopilotConvoglio->fermoSezioneAttesaLockSezioni();

    // *** VERIFY *** //

    EXPECT_TRUE(lockSezioni);

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
}

// Il convoglio si è fermato in una sezione intermedia. Quando viene chiamato autopilot->processo e le sezioni
// successive fino al primo blocco capiente non sono libere, il convoglio non riesce a bloccarle e lo stato
// dell'autopilot non cambia
TEST_F(AutopilotConvoglioTest, fermoSezioneAttesaLockSezioni_SezioniSuccessiveOccupate) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->setPosizioneConvoglio(IdSezioneTipo(1, 3, 1), true);
    inizializzoAutopilot_modalitaStazioneSuccessiva_1_2_1();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
    autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva =
        StatoAutopilotModalitaStazioneSuccessiva::FERMO_SEZIONE_ATTESA_LOCK_SEZIONI;

    // Simulo che il convoglio sia già passato su un sensore in delle sezioni precedenti
    autopilotConvoglio->distanzaPercorsaConvoglioDaSensoreUltimaSezioneCm = 100;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(40.0));

    salvoPercorsoDaStazioneCorrenteAStazioneSuccessiva(IdSezioneTipo(1, 3, 1), IdSezioneTipo(1, 2, 1));
    lockSezioneCapiente();

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(2000));

    sezioni.getSezione(IdSezioneTipo(3, 55, 1))->lock(convoglio.getId());
    sezioni.getSezione(IdSezioneTipo(2, 30))->lock(convoglio.getId());

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);

    // *** EXECUTE *** //

    bool lockSezioni = autopilotConvoglio->fermoSezioneAttesaLockSezioni();

    // *** VERIFY *** //

    EXPECT_FALSE(lockSezioni);

    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 51, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 42))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 52, 2))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 53, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 32))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(3, 55, 1))->isLocked(), true);
    EXPECT_EQ(sezioni.getSezione(IdSezioneTipo(2, 30))->isLocked(), true);
}

// Verifico che quando viene chiamata la funzione scelgoBinarioStazioneSuccessivaELock con ottimizzazione attiva, venga
// salvato il binario più corto della prima stazione scelta casualmente (in questo caso la prima stazione registrata).
// In questo caso il binario più corto della stazione è libero.
TEST_F(
    AutopilotConvoglioTest,
    modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_primaStazioneScelta_BinarioPiuCortoLibero) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(1, 3, 1);
    autopilotConvoglio->statoAutopilotModalitaApproccioCasuale =
        StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK;

    // Inizializzo autopilot e abilito ottimizzazione
    autopilotConvoglio->inizializzoAutopilotModalitaCasuale(true);

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(0));

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(30.0));

    // *** EXECUTE *** //

    autopilotConvoglio->scelgoBinarioStazioneSuccessivaELock();

    // *** VERIFY *** //

    // Dato che viene calcolata la stazione facendo il modulo con il tempo, se il tempo è 0, mi aspetto che venga presa
    // la prima stazione.
    StazioneAbstract* stazioneSceltaCasualmente = stazioni.getStazione(IdStazioneTipo(1));
    IdSezioneTipo binarioSceltoExpected = stazioneSceltaCasualmente->getBinario(1)->getId();

    // Essendo la lunghezza del convoglio molto piccola, mi aspetto che venga scelto il primo binario della stazione,
    // ovvero quello più piccolo. Verifico che il binario scelto venga salvato correttamente.
    EXPECT_TRUE(binarioSceltoExpected.equals(autopilotConvoglio->percorsoConvoglioBinariStazioni[1]));
}

// Verifico che quando viene chiamata la funzione scelgoBinarioStazioneSuccessivaELock con ottimizzazione attiva, venga
// salvato il binario più corto della prima stazione scelta casualmente (in questo caso la prima stazione registrata).
// In questo caso tutti i binari della stazione sono già occupati tranne quello più lungo.
TEST_F(
    AutopilotConvoglioTest,
    modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_primaStazioneSceltaCasualmente_BinarioPiuLungoUnicoLibero) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(1, 3, 1);
    autopilotConvoglio->statoAutopilotModalitaApproccioCasuale =
        StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK;

    // Inizializzo autopilot e abilito ottimizzazione
    autopilotConvoglio->inizializzoAutopilotModalitaCasuale(true);

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(0));

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(30.0));

    // Dato che viene calcolata la stazione facendo il modulo con il tempo, se il tempo è 0, mi aspetto che venga presa
    // la prima stazione.
    StazioneAbstract* stazioneSceltaCasualmente = stazioni.getStazione(IdStazioneTipo(1));

    // Simulo che il primo binario della stazione sia stato già bloccato
    SezioneAbstract* sezione = sezioni.getSezione(stazioneSceltaCasualmente->getBinario(1)->getId());
    sezione->lock(convoglio.getId());

    // *** EXECUTE *** //

    autopilotConvoglio->scelgoBinarioStazioneSuccessivaELock();

    // *** VERIFY *** //

    // Viene scelto il secondo binario della stazione dato che il primo non era libero
    IdSezioneTipo binarioSceltoExpected = stazioneSceltaCasualmente->getBinario(2)->getId();

    // Essendo la lunghezza del convoglio molto piccola, mi aspetto che venga scelto il primo binario della stazione,
    // ovvero quello più piccolo. Verifico che il binario scelto venga salvato correttamente.
    EXPECT_TRUE(binarioSceltoExpected.equals(autopilotConvoglio->percorsoConvoglioBinariStazioni[1]));
}

// Verifico che quando viene chiamata la funzione scelgoBinarioStazioneSuccessivaELock con ottimizzazione attiva, se la
// prima stazione scelta casualmente (in questo caso la prima stazione registrata) ha tutti i binari bloccati, venga
// scelta la stazione successiva (in questo caso la seconda stazione registrata) e venga salvato il binario più corto
// della stazione.
TEST_F(
    AutopilotConvoglioTest,
    modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_primaStazioneSceltaCasualmente_NessunBinarioLibero) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(1, 3, 1);
    autopilotConvoglio->statoAutopilotModalitaApproccioCasuale =
        StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK;

    // Inizializzo autopilot e abilito ottimizzazione
    autopilotConvoglio->inizializzoAutopilotModalitaCasuale(true);

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(10.0));

    // Dato che viene calcolata la stazione facendo il modulo con il tempo, se il tempo è 0, mi aspetto che venga presa
    // la prima stazione.
    StazioneAbstract* primaStazioneSceltaCasualmente = stazioni.getStazione(IdStazioneTipo(1));

    // Simulo che tutti i binari della prima stazione siano occupati.
    for (int i = 1; i < primaStazioneSceltaCasualmente->numeroBinari + 1; i++) {
        // Simulo che tutti i binari della prima stazione siano stati già bloccati
        SezioneAbstract* sezione = sezioni.getSezione(primaStazioneSceltaCasualmente->getBinario(i)->getId());
        sezione->lock(convoglio.getId());
    }

    StazioneAbstract* secondaStazioneSceltaCasualmente = stazioni.getStazione(IdStazioneTipo(2));

    unsigned long tempoMock1 = 0;
    unsigned long tempoMock2 = 1;

    // Uso EXPECT_CALL al posto di ON_CALL in quanto necessario così per poter mockare risposte differenti
    testing::Sequence seq;
    EXPECT_CALL(tempo, milliseconds()).InSequence(seq).WillOnce(testing::Return(0));
    EXPECT_CALL(tempo, milliseconds()).InSequence(seq).WillOnce(testing::Return(1));

    // *** EXECUTE *** //

    autopilotConvoglio->scelgoBinarioStazioneSuccessivaELock();

    // *** VERIFY *** //

    // Viene scelto il primo binario della seconda stazione dato che tutti i binari della prima stazione erano occupati.
    IdSezioneTipo binarioSceltoExpected = secondaStazioneSceltaCasualmente->getBinario(1)->getId();

    // Essendo la lunghezza del convoglio molto piccola, mi aspetto che venga scelto il primo binario della stazione,
    // ovvero quello più piccolo. Verifico che il binario scelto venga salvato correttamente.
    EXPECT_TRUE(binarioSceltoExpected.equals(autopilotConvoglio->percorsoConvoglioBinariStazioni[1]));
}

// Verifico che quando viene chiamata la funzione scelgoBinarioStazioneSuccessivaELock con ottimizzazione attiva, venga
// salvato il binario più corto della prima stazione scansionata (in questo caso l'ultima stazione registrata). In
// questo caso il binario più corto della stazione è libero. Questo test serve in particolar modo per verificare il
// corner case della scelta della stazione in maniera casuale.
TEST_F(
    AutopilotConvoglioTest,
    modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_ottimizzazioneAssegnazioneConvogli_ultimaStazioneSceltaCasualmente_BinarioPiuCortoLibero) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(1, 3, 1);
    autopilotConvoglio->statoAutopilotModalitaApproccioCasuale =
        StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK;

    // Inizializzo autopilot e abilito ottimizzazione
    autopilotConvoglio->inizializzoAutopilotModalitaCasuale(true);

    ON_CALL(tempo, milliseconds()).WillByDefault(testing::Return(stazioni.numeroStazioni - 1));

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(30.0));

    // Dato che viene calcolata la stazione facendo il modulo con il tempo, se il tempo è il numero di stazioni - 1, mi
    // aspetto che venga presa l'ultima stazione.
    StazioneAbstract* stazioneSceltaCasualmente = stazioni.getStazione(IdStazioneTipo(5));

    // *** EXECUTE *** //

    autopilotConvoglio->scelgoBinarioStazioneSuccessivaELock();

    // *** VERIFY *** //

    // Viene scelto il secondo binario della stazione dato che il primo non era libero
    IdSezioneTipo binarioSceltoExpected = stazioneSceltaCasualmente->getBinario(1)->getId();

    // Essendo la lunghezza del convoglio molto piccola, mi aspetto che venga scelto il primo binario della stazione,
    // ovvero quello più piccolo. Verifico che il binario scelto venga salvato correttamente.
    EXPECT_TRUE(binarioSceltoExpected.equals(autopilotConvoglio->percorsoConvoglioBinariStazioni[1]));
}

// Verifico che quando viene chiamata la funzione scelgoBinarioStazioneSuccessivaELock con ottimizzazione disattivata,
// venga salvato uno dei binari della prima stazione scelta casualmente (in questo caso la prima stazione registrata).
// In questo caso il binario più corto della stazione è libero.
TEST_F(
    AutopilotConvoglioTest,
    modalitaAutopilotApproccioCasuale_sceltaBinarioStazioneSuccessivaELock_NoOttimizzazioneAssegnazioneConvogli_primaStazioneScelta_BinarioLibero) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(1, 3, 1);
    autopilotConvoglio->statoAutopilotModalitaApproccioCasuale =
        StatoAutopilotModalitaApproccioCasuale::SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK;

    // Inizializzo autopilot e abilito ottimizzazione
    autopilotConvoglio->inizializzoAutopilotModalitaCasuale(false);

    // Faccio restituire al mock del tempo 0, in questo modo la stazione scelta casualmente sarà la prima.
    testing::Sequence seq;
    EXPECT_CALL(tempo, milliseconds()).InSequence(seq).WillOnce(testing::Return(0));
    // Faccio restituire al mock del tempo un numero random, in questo modo l'algoritmo sceglierà un binario a caso
    // della stazione scelta
    EXPECT_CALL(tempo, milliseconds()).InSequence(seq).WillRepeatedly(testing::Return(rand() % 100 + 1));

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(30.0));

    // *** EXECUTE *** //

    autopilotConvoglio->scelgoBinarioStazioneSuccessivaELock();

    // *** VERIFY *** //

    // Dato che viene calcolata la stazione facendo il modulo con il tempo, mi aspetto che venga presa
    // la prima stazione.
    StazioneAbstract* stazioneSceltaCasualmente = stazioni.getStazione(IdStazioneTipo(1));

    // Verifico che in percorsoConvoglioBinariStazioni sia stato salvato uno dei binari della stazione scelta
    // casualmente. Dato che in questo caso il mock del tempo restituisce in maniera random, non controllo uno specifico
    // binario ma solo l'ID della stazione.
    EXPECT_EQ(autopilotConvoglio->percorsoConvoglioBinariStazioni[1].id, stazioneSceltaCasualmente->getId());
}

// TODO Test con stazione centrale con tutti i binari occupati. Verificare che percorsoConvoglioBinariStazioni sia
// ancora a null

// *** TEST INIZIALIZZAZIONE AUTOPILOT (GENERALE) *** //

// ** PROCESSO ** //

// Verifico che la funzione processo non faccia nulla se lo stato è ATTESA_INIZIALIZZAZIONE_CONVOGLIO
TEST_F(AutopilotConvoglioTest, inizializzazioneAutopilotConvoglio_AttesaInizializzazioneConvoglio) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;

    // *** EXECUTE *** //

    autopilotConvoglio->processo(false);

    // *** VERIFY *** //

    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);
}

// Verifico che la funzione processo non faccia nulla se lo stato è
// StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT
TEST_F(AutopilotConvoglioTest, inizializzazioneAutopilotConvoglio_ProntoPerConfigurazioneAutopilot) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    // *** EXECUTE *** //

    autopilotConvoglio->processo(false);

    // *** VERIFY *** //

    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT);
}

// ** SET INFORMAZIONI CONVOGLIO ** //

// Quando viene chiamata la funzione setPosizioneConvoglio con una sezione di stazione, lo stato deve cambiare da
// ATTESA_INIZIALIZZAZIONE_CONVOGLIO a StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT.
// Inoltre, controlliamo che la sezione venga bloccata.
TEST_F(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaStazione) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;
    IdSezioneTipo idSezioneOccupataDaConvoglio = IdSezioneTipo(1, 3, 1);

    // *** PRE-VERIFY *** //

    EXPECT_EQ(sezioni.getSezione(idSezioneOccupataDaConvoglio)->isLocked(), false);

    // *** EXECUTE *** //

    bool risultatoFunzione = autopilotConvoglio->setPosizioneConvoglio(idSezioneOccupataDaConvoglio, true);

    // *** VERIFY *** //

    // Mi aspetto che lo stato sia cambiato
    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT);
    // Mi aspetto che la funzione abbia bloccato la sezione inserita
    EXPECT_EQ(sezioni.getSezione(idSezioneOccupataDaConvoglio)->isLocked(), true);
    // Mi aspetto che la funzione abbia restituito true
    EXPECT_EQ(risultatoFunzione, true);
}

// Quando viene chiamata la funzione setPosizioneConvoglio con una sezione di stazione, ma questa sezione risulta già
// bloccata, la funzione deve restituire errore e ritornare false. bloccata.
TEST_F(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaStazione_sezioneGiaOccupata) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;
    IdSezioneTipo idSezioneOccupataDaConvoglio = IdSezioneTipo(1, 3, 1);

    // Simulo che la sezione sia già bloccata
    sezioni.getSezione(idSezioneOccupataDaConvoglio)->lock(convoglio.getId());

    // *** EXECUTE *** //

    bool risultatoFunzione = autopilotConvoglio->setPosizioneConvoglio(idSezioneOccupataDaConvoglio, true);

    // *** VERIFY *** //

    // Mi aspetto che lo stato non sia cambiato
    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);

    // Mi aspetto che la funzione abbia restituito false
    EXPECT_EQ(risultatoFunzione, false);
}

// Quando viene chiamata la funzione setPosizioneConvoglio con una sezione inesistente, la funzione deve restituire
// errore e lo stato non deve cambiare
TEST_F(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaNonEsiste) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;

    // *** EXECUTE *** //

    IdSezioneTipo idSezioneOccupataDaConvoglio = IdSezioneTipo(9, 9, 9);
    bool risultatoFunzione = autopilotConvoglio->setPosizioneConvoglio(idSezioneOccupataDaConvoglio, true);

    // *** VERIFY *** //

    // Mi aspetto che lo stato non sia cambiato
    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);

    // Mi aspetto che la funzione abbia restituito false
    EXPECT_EQ(risultatoFunzione, false);
}

// Quando viene chiamata la funzione setPosizioneConvoglio con una sezione non di stazione, la funzione deve
// restituire errore e lo stato non deve cambiare
TEST_F(AutopilotConvoglioTest, setPosizioneConvoglio_sezioneInseritaNonDiStazione) {
    // *** SETUP *** //

    setup();
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;

    // *** EXECUTE *** //

    IdSezioneTipo idSezioneOccupataDaConvoglio = IdSezioneTipo(2, 1, 1);
    bool risultatoFunzione = autopilotConvoglio->setPosizioneConvoglio(idSezioneOccupataDaConvoglio, true);

    // *** VERIFY *** //

    // Mi aspetto che lo stato non sia cambiato
    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO);

    // Mi aspetto che la funzione abbia restituito false
    EXPECT_EQ(risultatoFunzione, false);
}

// *** TEST MODALITA AUTOPILOT - MODALITA AUTOPILOT STAZIONE SUCCESSIVA *** //

// ** INIZIALIZZAZIONE ** //

// Verifico che quando viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva, la sezione del binario
// della stazione e il delay inseriti vengono salvati correttamente. Inoltre la sezione viene bloccata.
TEST_F(AutopilotConvoglioTest, modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Ok) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->posizioneConvoglio.idSezioneCorrenteOccupata = IdSezioneTipo(1, 3, 1);
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    IdSezioneTipo idSezioneBinarioStazione = IdSezioneTipo(1, 2, 1);

    // *** PRE-VERIFY *** //

    EXPECT_FALSE(sezioni.getSezione(idSezioneBinarioStazione)->isLocked());

    // *** EXECUTE *** //

    int secondiStopTraStazioni = 10;
    bool risultatoFunzione = autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(
        idSezioneBinarioStazione, secondiStopTraStazioni);

    // *** VERIFY *** //

    EXPECT_TRUE(autopilotConvoglio->percorsoConvoglioBinariStazioni[0].equals(IdSezioneTipo(1, 3, 1)));
    EXPECT_TRUE(autopilotConvoglio->percorsoConvoglioBinariStazioni[1].equals(IdSezioneTipo(1, 2, 1)));

    EXPECT_EQ(autopilotConvoglio->delayAttesaPartenzaStazione, secondiStopTraStazioni);

    EXPECT_EQ(autopilotConvoglio->statoInizializzazioneAutopilotConvoglio,
              StatoInizializzazioneAutopilotConvoglio::CONFIGURAZIONE_AUTOPILOT_COMPLETATA);
    EXPECT_EQ(autopilotConvoglio->statoAutopilotModalitaStazioneSuccessiva,
              StatoAutopilotModalitaStazioneSuccessiva::FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA);

    // Mi aspetto che la funzione abbia bloccato la sezione inserita
    EXPECT_TRUE(sezioni.getSezione(idSezioneBinarioStazione)->isLocked());

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, true);
}

// Verifico che se viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva ma lo
// statoInizializzazioneAutopilotConvoglio non è
// StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT, l'inizializzazione non va a buon fine e
// la funzione restituisce false.
TEST_F(AutopilotConvoglioTest,
       modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_NonProntoPerConfigurazioneAutopilot) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio non sia stata già chiamata
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_INIZIALIZZAZIONE_CONVOGLIO;

    // *** EXECUTE *** //

    int secondiStopTraStazioni = 10;
    bool risultatoFunzione = autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(
        IdSezioneTipo(1, 2, 1), secondiStopTraStazioni);

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, false);
}

// Verifico che se viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva con una sezione
// inesistente, l'inizializzazione non va a buon fine e la funzione restituisce false.
TEST_F(AutopilotConvoglioTest, modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_SezioneNonValida) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    // *** EXECUTE *** //

    int secondiStopTraStazioni = 10;
    bool risultatoFunzione = autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(
        IdSezioneTipo(9, 9, 9), secondiStopTraStazioni);

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, false);
}

// Verifico che se viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva con una sezione che non è
// binario non di stazione (2.32), l'inizializzazione non va a buon fine e la funzione restituisce false.
TEST_F(AutopilotConvoglioTest, modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_SezioneNonDiStazione) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    // *** EXECUTE *** //

    bool risultatoFunzione =
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(2, 32), 0);

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, false);
}

// Verifico che se viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva con una sezione già
// bloccata, l'inizializzazione non va a buon fine e la funzione restituisce false.
TEST_F(AutopilotConvoglioTest, modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_SezioneGiaBloccata) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    IdSezioneTipo idSezioneOccupataDaConvoglio = IdSezioneTipo(1, 3, 1);

    // Simulo che la sezione sia già bloccata
    sezioni.getSezione(idSezioneOccupataDaConvoglio)->lock(convoglio.getId());

    // *** EXECUTE *** //

    bool risultatoFunzione =
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(idSezioneOccupataDaConvoglio, 0);

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, false);
}

// Verifico che se viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva e non esiste un percorso
// tra la stazione attuale e quella di destinazione, l'inizializzazione non va a buon fine e la funzione restituisce
// false.
TEST_F(AutopilotConvoglioTest, modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_PercorsoNonEsiste) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(50.0));

    // *** EXECUTE *** //

    bool risultatoFunzione =
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(1, 2, 2), 0);

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, false);
}

// Verifico che se viene chiamata la funzione inizializzoAutopilotModalitaStazioneSuccessiva e il binario della stazione
// successiva inserito non è abbastanza lungo per poter ospitare il convoglio, l'inizializzazione non va a buon fine e
// la funzione restituisce false.
TEST_F(
    AutopilotConvoglioTest,
    modalitaAutopilotStazioneSuccessiva_inizializzoAutopilot_Errore_BinarioStazioneSuccessivaNonLungoASufficienzaPerConvoglio) {
    // *** SETUP *** //

    setup();

    // Simuliamo che la funzione setPosizioneConvoglio sia stata già chiamata
    autopilotConvoglio->statoInizializzazioneAutopilotConvoglio =
        StatoInizializzazioneAutopilotConvoglio::PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;

    ON_CALL(convoglio, getLunghezzaCm()).WillByDefault(testing::Return(300.0));

    // *** EXECUTE *** //

    bool risultatoFunzione =
        autopilotConvoglio->inizializzoAutopilotConvoglioModalitaStazioneSuccessiva(IdSezioneTipo(1, 2, 2), 0);

    // *** VERIFY *** //

    EXPECT_EQ(risultatoFunzione, false);
}