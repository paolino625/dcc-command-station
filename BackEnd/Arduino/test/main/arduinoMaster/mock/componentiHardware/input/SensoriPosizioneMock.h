#ifndef DCC_COMMAND_STATION_SENSORIPOSIZIONEMOCK_H
#define DCC_COMMAND_STATION_SENSORIPOSIZIONEMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/input/sensoriPosizione/SensoriPosizioneAbstract.h"

// *** CLASSE *** //

class SensoriPosizioneMock : public SensoriPosizioneAbstract {
   public:
    MOCK_METHOD(void, inizializza, (), (override));
    MOCK_METHOD(void, reset, (boolean stampaLog), (override));
    MOCK_METHOD(void, resetInAttesaPassaggioPrimaLocomotivaConvoglio, (), (override));
    MOCK_METHOD(bool, esisteIdSensore, (byte idSensore), (override));
    MOCK_METHOD(SensorePosizioneAbstract *, getSensorePosizione, (int idSensore), (override));
};

#endif
