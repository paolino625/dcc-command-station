#ifndef DCC_COMMAND_STATION_DISPLAYCOMPONENTEMOCK_H
#define DCC_COMMAND_STATION_DISPLAYCOMPONENTEMOCK_H

// *** INCLUDE *** //

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/componentiHardware/output/display/stampa/core/componente/DisplayComponenteAbstract.h"
#include "main/progetto/comune/moduliDipendentiArduino/abstract/utility/logger/LoggerAbstract.h"

// *** CLASSE *** //

class DisplayComponenteMock : public DisplayComponenteAbstract {
    // *** VARIABILI *** //

   private:
    int indirizzoI2C = 3;
    void inizializza() override {}
    void pulisce() override {}
    void posizionaCursore(int colonna, int riga) override {}
    void print(const char* stringa) override {}
    void print(int numero) override {}
    void print(char carattere) override {}
    void print(float numero) override {}
    void print(unsigned long numero) override {}

    // *** COSTRUTTORE *** //

   public:
    DisplayComponenteMock(LoggerAbstract& logger) : DisplayComponenteAbstract(logger, indirizzoI2C) {}
};

#endif
