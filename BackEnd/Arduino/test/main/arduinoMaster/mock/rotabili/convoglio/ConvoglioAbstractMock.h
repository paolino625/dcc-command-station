#ifndef DCC_COMMAND_STATION_CONVOGLIOABSTRACTMOCK_H
#define DCC_COMMAND_STATION_CONVOGLIOABSTRACTMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

// *** CLASSE *** //

class ConvoglioAbstractMock : public ConvoglioAbstract {
   public:
    MOCK_METHOD(void, aggiungeSecondaLocomotiva, (IdLocomotivaTipo idLocomotiva), (override));
    MOCK_METHOD(void, eliminaSecondaLocomotiva, (), (override));
    MOCK_METHOD(bool, isInUso, (), (const, override));
    MOCK_METHOD(bool, hasDoppiaLocomotiva, (), (const, override));
    MOCK_METHOD(void, reset, (), (override));
    MOCK_METHOD(void, resetPosizione, (), (override));
    MOCK_METHOD(void, cambiaVelocita, (VelocitaRotabileKmHTipo nuovaVelocitaDaImpostare, bool graduale, StatoAutopilot statoAutopilot), (override));
    MOCK_METHOD(void, cambiaDirezione, (StatoAutopilot statoAutopilot), (override));
    MOCK_METHOD(bool, esisteVagone, (byte idVagone), (override));
    MOCK_METHOD(void, aggiungeVagone, (IdVagoneTipo idVagone), (override));
    MOCK_METHOD(void, eliminaVagone, (IdVagoneTipo idVagone), (override));
    MOCK_METHOD(void, inverteLuci, (), (override));
    MOCK_METHOD(void, aggiornaLuci, (), (const, override));
    MOCK_METHOD(void, invalidaPosizioneConvoglio, (bool sezioneOccupataDalConvoglioDaLiberare), (const, override));
    MOCK_METHOD(void, aggiornaLunghezza, (), (override));
    MOCK_METHOD(IdLocomotivaTipo, getLocomotivaPiuVeloce, (), (override));

    MOCK_METHOD(IdConvoglioTipo, getId, (), (const, override));
    MOCK_METHOD(const char *, getNome, (), (const,override));
    MOCK_METHOD(IdLocomotivaTipo, getIdLocomotiva1, (), (const, override));
    MOCK_METHOD(IdLocomotivaTipo, getIdLocomotiva2, (), (const, override));
    MOCK_METHOD(LocomotivaAbstract*, getLocomotiva1, (), (const, override));
    MOCK_METHOD(LocomotivaAbstract*, getLocomotiva2, (), (const, override));
    MOCK_METHOD(bool, isLuciAccese, (), (override));
    MOCK_METHOD(IdVagoneTipo, getIdVagone, (IdVagoneTipo idVagone), (override));
    MOCK_METHOD(StepVelocitaKmHTipo, getDifferenzaStepVelocitaTraLocomotive, (), (const, override));
    MOCK_METHOD(TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta,
                getTipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta, (), (override));
    MOCK_METHOD(TimestampTipo, getMillisPrecedenteAggiornamentoVelocitaAttuale, (), (const, override));
    MOCK_METHOD(VelocitaRotabileKmHTipo, getVelocitaImpostata, (), (const, override));
    MOCK_METHOD(VelocitaRotabileKmHTipo, getVelocitaAttuale, (), (const, override));
    MOCK_METHOD(LunghezzaRotabileTipo, getLunghezzaCm, (), (const, override));
    MOCK_METHOD(VelocitaRotabileKmHTipo, getVelocitaMassima, (), (const, override));
    MOCK_METHOD(int, getNumeroVagoni, (), (override));
    MOCK_METHOD(PosizioneConvoglio, getPosizione, (), (const, override));
    MOCK_METHOD(bool, isPresenteSulTracciato, (), (const, override));
    MOCK_METHOD(LocomotivaAbstract*, getLocomotivaInSpinta, (), (const, override));
    MOCK_METHOD(LocomotivaAbstract*, getLocomotivaInTrazione, (), (const, override));

    MOCK_METHOD(void, setId, (IdConvoglioTipo idNuovo), (override));
    MOCK_METHOD(void, setNome, (const char *nomeNuovo), (override));
    MOCK_METHOD(void, setIdLocomotiva1, (IdLocomotivaTipo idLocomotiva), (override));
    MOCK_METHOD(void, setIdLocomotiva2, (IdLocomotivaTipo idLocomotiva), (override));
    MOCK_METHOD(void, setLuciAccese, (bool luciAcceseNuovo), (override));
    MOCK_METHOD(void, setPresenteSulTracciato, (bool sulTracciato), (override));
    MOCK_METHOD(void, setDifferenzaVelocitaLocomotivaTrazioneSpinta,
                (TipologiaDifferenzaVelocitaLocomotivaTrazioneSpinta differenza), (override));
    MOCK_METHOD(void, setDifferenzaStepVelocitaTraLocomotive, (StepVelocitaKmHTipo stepVelocita), (override));
    MOCK_METHOD(void, setVagoneId, (IdVagoneTipo idVagone, IdVagoneTipo idVagoneDaSettare), (override));

    MOCK_METHOD(LunghezzaRotabileTipo, calcoloLunghezzaCm, (), (override));
};

#endif