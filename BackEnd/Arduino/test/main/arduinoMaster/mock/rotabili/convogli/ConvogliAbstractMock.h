#ifndef DCC_COMMAND_STATION_CONVOGLIABSTRACTMOCK_H
#define DCC_COMMAND_STATION_CONVOGLIABSTRACTMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/rotabili/convogli/ConvogliAbstract.h"

// *** CLASSE *** //

class ConvogliAbstractMock : public ConvogliAbstract {
   public:
    MOCK_METHOD(void, inizializza, (), (override));
    MOCK_METHOD(bool, esisteConvoglio, (IdConvoglioTipo idConvoglio), (override));
    MOCK_METHOD(void, reset, (), (override));
    MOCK_METHOD(bool, isLocomotivaInUso, (IdLocomotivaTipo idLocomotiva), (override));
    MOCK_METHOD(bool, isVagoneInUso, (VagoneAbstract * vagone), (override));
    MOCK_METHOD(void, aggiornaLunghezza, (), (override));
    MOCK_METHOD(void, aggiornaLuci, (), (override));
    MOCK_METHOD(char*, toString, (), (override));
    MOCK_METHOD(ConvoglioAbstractReale*, getConvoglio, (int id, bool fermaProgramma), (override));
    MOCK_METHOD(ConvoglioAbstractReale*, getConvoglioDellaLocomotiva, (IdLocomotivaTipo idLocomotiva), (override));
    MOCK_METHOD(void, bloccoSezioniOccupate, (), (override));
};

#endif