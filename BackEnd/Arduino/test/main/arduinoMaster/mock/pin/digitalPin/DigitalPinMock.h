#pragma once

// *** INCLUDE *** //

#include <type_traits>  // std::is_abstract

// *** CLASSE *** //

class DigitalPinMock final : public DigitalPinAbstract {
   private:
    enum class Mode { INPUT, INPUT_PULLUP, OUTPUT };
    Mode modePin;

    bool low = true;

   public:
    [[nodiscard]] auto isOutputMode() const -> bool {
        if (modePin == Mode::OUTPUT) {
            return true;
        } else {
            return false;
        }
    }

    [[nodiscard]] auto isInputMode() const -> bool {
        if (modePin == Mode::INPUT) {
            return true;
        } else {
            return false;
        }
    }

    [[nodiscard]] auto isInputPullupMode() const -> bool {
        if (modePin == Mode::INPUT_PULLUP) {
            return true;
        } else {
            return false;
        }
    }

    auto setOutputMode() -> void override { modePin = Mode::OUTPUT; }

    auto setInputMode() -> void override { modePin = Mode::INPUT; }

    auto setInputPullupMode() -> void override { modePin = Mode::INPUT_PULLUP; }

    auto setHigh() -> void override { low = false; }

    auto setLow() -> void override { low = true; }

    [[nodiscard]] auto isHigh() const -> bool override { return !low; }

    [[nodiscard]] auto isLow() const -> bool override { return low; }

    [[nodiscard]] auto legge() const -> bool override { return isHigh(); }
};

// #static_assert(!std::is_abstract<DigitalPinMock>());
