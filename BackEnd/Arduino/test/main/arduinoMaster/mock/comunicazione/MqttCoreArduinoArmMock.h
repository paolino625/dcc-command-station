#ifndef DCC_COMMAND_STATION_MQTTCOREARDUINOARMMOCK_H
#define DCC_COMMAND_STATION_MQTTCOREARDUINOARMMOCK_H

// *** INCLUDE *** //

#include "main/progetto/arm/comune/moduliDipendentiArduino/abstract/comunicazione/mqtt/mqttCore/MqttCoreArduinoArmAbstract.h"

// *** CLASSE *** //

class MqttCoreArduinoArmMock : public MqttCoreArduinoArmAbstract{
        // *** VARIABILI *** //

       public:
        void inizializza() override {}
        void inviaKeepAlive() override {}
        void inviaMessaggio(TopicMqtt topicMqtt, char* payload, bool loggerAttivo) override {}
        void checkConnessione() override {}
        void connetteBroker(bool fermoProgrammaInCasoDiFallimento) override {}
        bool isMqttInizializzato() const override { return true; }

       private:
        const char* calcoloNomeTopic(TopicMqtt topicMqtt, bool loggerAttivo) override { return ""; }
};

#endif
