#ifndef DCC_COMMAND_STATION_MQTTARDUINOMASTERMOCK_H
#define DCC_COMMAND_STATION_MQTTARDUINOMASTERMOCK_H

// *** INCLUDE *** //

#include "MqttCoreArduinoArmMock.h"
#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/comunicazione/mqtt/MqttArduinoMasterAbstract.h"

// *** CLASSE *** //

class MqttArduinoMasterMock : public MqttArduinoMasterAbstract{
   MqttCoreArduinoArmMock mqttCoreArduinoArmMock;

        // *** VARIABILI *** //

       public:
        void inizializzaVariabiliTriggerInvioMqtt() override {}
        void inviaStatoOggettiAggiornati() override {}
        void inviaStatoLocomotiva(IdLocomotivaTipo idLocomotiva) override {}
        void inviaStatoConvoglio(IdConvoglioTipo idConvoglio) override {}
        void inviaStatoScambio(IdScambioTipo idScambio) override {}
        void inviaStatoSezione(int indiceSezione) override {}
        void inviaStatoSensorePosizione(IdSensorePosizioneTipo idSensore) override {}
        void inviaInfoAutopilot() override {}
        void invioStatoLocomotiveAggiornate() override {}
        void invioStatoConvogliAggiornati() override {}
        void invioStatoScambiAggiornati() override {}
        void invioStatoSensoriPosizioneAggiornati() override {}
        void invioStatoSezioniAggiornate() override {}
        void invioInfoAutopilotAggiornato() override {}
        void invioTriggerArduinoReady() override {}
        void inviaMessaggioLog(char* payload) override {}

        MqttCoreArduinoArmAbstract& getMqttCore() override {
            return mqttCoreArduinoArmMock;
        }

        // *** COSTRUTTORE *** //

        MqttArduinoMasterMock() : MqttArduinoMasterAbstract() {}
};

#endif
