#ifndef DCC_COMMAND_STATION_PERCORSIMOCK_H
#define DCC_COMMAND_STATION_PERCORSIMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

#include "main/progetto/arm/arduinoMaster/moduliDipendentiArduino/abstract/funzionalita/autopilot/percorsi/PercorsiAbstract.h"

// *** CLASSE *** //

class PercorsiMock : public PercorsiAbstract {
   public:
    MOCK_METHOD(bool, esistePercorso, (IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo), (override));
    MOCK_METHOD(bool, esistePercorso,
                (IdSezioneTipo idSezionePartenza, IdSezioneTipo idSezioneArrivo, bool direzionePrimaLocomotivaDestra),
                (override));
    MOCK_METHOD(PercorsoAbstract*, getPercorso,
                (IdSezioneTipo idSezionePartenzaRequest, IdSezioneTipo idSezioneArrivoRequest), (override));
};

#endif