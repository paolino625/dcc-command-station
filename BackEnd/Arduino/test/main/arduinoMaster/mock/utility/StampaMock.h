#ifndef DCC_COMMAND_STATION_STAMPAMOCK_H
#define DCC_COMMAND_STATION_STAMPAMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

#include <cstdio>

// *** CLASSE *** //

class StampaMock : public StampaAbstract {
   public:
    void print(const char* messaggio) override { fprintf(stdout, "%s", messaggio); }
    void print(double numero, int cifreDecimali) override { fprintf(stdout, "%f", numero); }
    void print(int numeroInt) override { fprintf(stdout, "%d", numeroInt); }

    void print(char carattere) override { fprintf(stdout, "%c", carattere); }

    void print(double numero) override { fprintf(stdout, "%f", numero); }

    void print(unsigned long numero) override { fprintf(stdout, "%lu", numero); }

    void print(unsigned int numero) override { fprintf(stdout, "%u", numero); }
};

#endif
