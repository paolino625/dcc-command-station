#ifndef DCC_COMMAND_STATION_PROGRAMMAMOCK_H
#define DCC_COMMAND_STATION_PROGRAMMAMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

// *** CLASSE *** //

class ProgrammaMock final : public ProgrammaAbstract {
   public:
    MOCK_METHOD(void, ferma, (bool stampaMessaggioErroreGenerico), (override));
};

#endif
