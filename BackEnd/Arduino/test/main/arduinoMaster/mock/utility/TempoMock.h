#ifndef DCC_COMMAND_STATION_TEMPOMOCK_H
#define DCC_COMMAND_STATION_TEMPOMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

#include "main/progetto/comune/moduliIndipendentiArduino/TipiDatiComune.h"

// *** CLASSE *** //

class TempoMock : public TempoAbstract {
   public:
    MOCK_METHOD(TimestampTipo, milliseconds, (), (override));
    MOCK_METHOD(TimestampTipo, microseconds, (), (override));
};

#endif
