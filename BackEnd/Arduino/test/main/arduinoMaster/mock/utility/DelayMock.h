#ifndef DCC_COMMAND_STATION_DELAYMOCK_H
#define DCC_COMMAND_STATION_DELAYMOCK_H

// *** INCLUDE *** //

#include <gmock/gmock.h>

// *** CLASSE *** //

class DelayMock : public DelayAbstract {
   public:
    MOCK_METHOD(void, milliseconds, (unsigned long ms), (override));
    MOCK_METHOD(void, microseconds, (unsigned long us), (override));
};

#endif
