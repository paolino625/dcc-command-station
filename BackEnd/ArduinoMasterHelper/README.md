# Scopo

I log vengono pubblicati su un'istanza Loki ospitata su un server locale. In questo modo è possibile avere uno storico
di tutti i log prodotti da Arduino, molto utile per verificare la salute del sistema o fare debugging.
Arduino Giga non effettua la chiamata POST direttamente a Loki a causa della memoria RAM limitata: i messaggi più lunghi
dovrebbero essere spezzati in più parti, rendendo la lettura difficile e le query poco efficienti.
Per questo motivo, Arduino Giga fa uso di questo microservizio creato ad hoc che concatena le stringhe e invia a Loki
solo quando Arduino Giga indica la fine di un messaggio.
La comunicazione tra Arduino Giga e il servizio intermediario avviene tramite MQTT (REST sarebbe troppo lento per la
quantità di messaggi da pubblicare).

# Deploy

Guida per containerizzazione Docker: https://spring.io/guides/gs/spring-boot-docker/

Assicurarsi che la variabile spring.profiles.active in application.properties sia impostata su "prod".
In questo modo i puntamenti non sono a localhost ma ai nomi dei vari container docker.

## Creazione build

### Modalità 1

Creazione immagine Docker con plugin Gradle:

- ./gradlew bootBuildImage --imageName=arduino-master-helper

### Modalità 2

Creazione della build con partendo dal DockerFile specificato in questo progetto.
La build appare nella sezione "build" di docker e non nelle immagini.

Per comporre la build:

- docker build -t arduino-master-helper .

## Esecuzione build

Per eseguire la build:

- docker run -p 8080:8080 --name ArduinoMasterHelper arduino-master-helper

## Push build

Per pushare la build:

- docker login registry.gitlab.com
- docker tag arduino-master-helper registry.gitlab.com/paolino625/dcc-command-station/arduino-master-helper:latest
- docker push registry.gitlab.com/paolino625/dcc-command-station/arduino-master-helper:latest