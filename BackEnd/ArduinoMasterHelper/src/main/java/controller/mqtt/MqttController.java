package controller.mqtt;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import service.ProcessoMessaggioMqtt;


@Configuration
public class MqttController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    ProcessoMessaggioMqtt processoMessaggioMqtt;

    @Value("${mqttBroker.url}")
    private String indirizzoServerMqtt;

    @Value("${mqttBroker.username}")
    private String mqttUsername;

    @Value("${mqttBroker.password}")
    private String mqttPassword;

    @Value("${mqttBroker.topic.locomotiva}")
    private String topicMqttLocomotiva;

    @Value("${mqttBroker.topic.convoglio}")
    private String topicMqttConvoglio;

    @Value("${mqttBroker.topic.scambio}")
    private String topicMqttScambio;

    @Value("${mqttBroker.topic.sensorePosizione}")
    private String topicMqttSensorePosizione;

    @Value("${mqttBroker.topic.sezione}")
    private String topicMqttSezione;

    @Value("${mqttBroker.topic.infoAutopilot}")
    private String topicMqttInfoAutopilot;

    @Value("${mqttBroker.topic.log}")
    private String topicMqttLog;

    @Value("${mqttBroker.topic.arduinoReady}")
    private String arduinoReady;

    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(20);
        executor.setQueueCapacity(50);
        executor.setThreadNamePrefix("mqttExecutor-");
        executor.initialize();
        return executor;
    }

    @Bean
    public DefaultMqttPahoClientFactory clientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
        options.setUserName(mqttUsername);
        options.setPassword(mqttPassword.toCharArray());
        options.setServerURIs(new String[]{indirizzoServerMqtt});
        options.setAutomaticReconnect(true); // Abilito riconnessione automatica
        /*
        // Il timeout potrebbe essere legato al problema di disconnessione/deadlock che ho rilevato.
        https://stackoverflow.com/questions/33540974/how-do-i-fix-this-issue-mqtt-paho-timed-out-but-did-not-report-as-connection-los
        https://stackoverflow.com/questions/33433820/paho-client-timed-out-as-no-activity
        */
        options.setConnectionTimeout(0);
        options.setKeepAliveInterval(60);
        factory.setConnectionOptions(options);
        return factory;
    }

    @Bean
    public MessageProducer producer() {

        logger.info("Inizializzo MQTT producer con indirizzo server: {}", indirizzoServerMqtt);

        // Genero un ID univoco per ogni connessione al client MQTT.
        // Altrimenti il broker MQTT riceve più messaggi con lo stesso client ID e li interpreta come connessioni differenti, chiudendo quindi la connessione precedente.
        // In parole povere, questo container andrebbe in loop con connessione persa.
        String clientIdUnivoco = "ID-" + System.currentTimeMillis();

        MqttPahoMessageDrivenChannelAdapterPersonalizzato adapter =
                new MqttPahoMessageDrivenChannelAdapterPersonalizzato(clientIdUnivoco, clientFactory(),
                        topicMqttLocomotiva, topicMqttConvoglio, topicMqttScambio, topicMqttSensorePosizione, topicMqttSezione, topicMqttInfoAutopilot, topicMqttLog, arduinoReady);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(2); // Ottengo ogni messaggio una volta sola
        adapter.setOutputChannel(mqttInputChannel());

        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {

        return new MessageHandler() {

            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                processoMessaggioMqtt.execute(message);
            }


        };
    }

}