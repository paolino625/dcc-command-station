package controller.mqtt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;

// Creiamo questa classe per aggiungere dei log quando il client MQTT si connette o riconnette al server, in modo tale da facilitare il debug.
public class MqttPahoMessageDrivenChannelAdapterPersonalizzato extends MqttPahoMessageDrivenChannelAdapter {
    public MqttPahoMessageDrivenChannelAdapterPersonalizzato(String clientId, DefaultMqttPahoClientFactory clientFactory, String... topic) {
        super(clientId, clientFactory, topic);
    }

    private static final Logger logger = LoggerFactory.getLogger(MqttPahoMessageDrivenChannelAdapterPersonalizzato.class);

    @Override
    public void connectComplete(boolean reconnect, String serverURI) {

        super.connectComplete(reconnect, serverURI);
        if (reconnect) {
            logger.warn("MQTT reconnected to server: {}", serverURI);
        } else {
            logger.info("MQTT connected to server: {}", serverURI);
        }
    }
}
