package controller.rest.arduino.get;

import controller.rest.arduino.get.convogli.infoBasilariConvogli.ArduinoGetConvogliControllerFactory;
import controller.rest.arduino.get.convogli.infoBasilariConvogli.ArduinoGetConvogliControllerResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import storage.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "")
@CrossOrigin(origins = "*") // Permetto le richieste da qualsiasi origine
public class ArduinoGetController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    LocomotiveStorage locomotiveStorage;

    @Autowired
    ConvogliStorage convogliStorage;

    @Autowired
    ScambiStorage scambiStorage;

    @Autowired
    SensoriPosizioneStorage sensoriPosizioneStorage;

    @Autowired
    ArduinoGetConvogliControllerFactory arduinoGetConvogliControllerFactory;

    @Autowired
    SezioniStorage sezioniStorage;

    @Autowired
    InfoAutopilotStorage infoAutopilotStorage;

    @Value("${logging.controller.rest.sezioni}")
    private boolean loggingAbilitato;

    @Operation(summary = "Get locomotive", description = "Recupera lo stato di tutte le locomotive")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Locomotive recuperate con successo"),
    })
    @GetMapping("/locomotive")
    public ResponseEntity<Object> getLocomotive() {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getLocomotive.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<Locomotiva> locomotive = locomotiveStorage.getLocomotive();
        if (locomotive.isEmpty()) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Locomotive non inizializzate. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(locomotive);
        }

    }

    @Operation(summary = "Get convogli", description = "Recupera le info basilari di tutti i convogli")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Convogli recuperati con successo"),
    })
    @GetMapping("/convogli")
    public ResponseEntity<Object> getConvogli() {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getConvogli.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<Convoglio> convogli = convogliStorage.getTuttiConvogli();

        ArduinoGetConvogliControllerResponse arduinoGetConvogliControllerResponse = arduinoGetConvogliControllerFactory.createArduinoGetConvogliResponse(convogli);

        if (arduinoGetConvogliControllerResponse.getConvogli().isEmpty()) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Convogli non inizializzati. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(arduinoGetConvogliControllerResponse.getConvogli());
        }
    }

    @Operation(summary = "Get sezioni di stazione compatibili convoglio modalità autopilot", description = "Recupera le sezioni che possono ospitare il convoglio in modalità autopilot")
    @GetMapping("/convoglio/{idConvoglio}/sezioniStazioneCompatibiliModalitaAutopilot")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sezioni compatibili con il convoglio recuperate con successo"),
    })
    public ResponseEntity<Object> getSezioniCompatibiliConvoglioModalitaAutopilot(@PathVariable int idConvoglio) {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getSezioniCompatibiliConvoglioModalitaAutopilot.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Convoglio convoglio = convogliStorage.getConvoglio(idConvoglio);

        if (convoglio == null) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Convogli non inizializzati. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        }

        List<Sezione> sezioniCompatibiliConvoglioModalitaAutopilot = convoglio.getSezioniStazioneCompatibiliModalitaAutopilot();
        if (sezioniCompatibiliConvoglioModalitaAutopilot == null) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Sezioni compatibili non inizializzate. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(sezioniCompatibiliConvoglioModalitaAutopilot);
        }
    }

    @Operation(summary = "Get sezioni di stazione compatibili convoglio modalità manuale", description = "Recupera le sezioni che possono ospitare il convoglio in modalità manuale")
    @GetMapping("/convoglio/{idConvoglio}/sezioniStazioneCompatibiliModalitaManuale")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sezioni compatibili con il convoglio recuperate con successo"),
    })
    public ResponseEntity<Object> getSezioniCompatibiliConvoglioModalitaManuale(@PathVariable int idConvoglio) {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getSezioniCompatibiliConvoglioModalitaManuale.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Convoglio convoglio = convogliStorage.getConvoglio(idConvoglio);

        if (convoglio == null) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Convogli non inizializzati. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        }

        List<Sezione> sezioniCompatibiliConvoglioModalitaManuale = convoglio.getSezioniStazioneCompatibiliModalitaManuale();
        if (sezioniCompatibiliConvoglioModalitaManuale == null) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Sezioni compatibili non inizializzate. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(sezioniCompatibiliConvoglioModalitaManuale);
        }
    }


    @Operation(summary = "Get scambi", description = "Recupera lo stato di tutti gli scambi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Scambi recuperati con successo"),
    })
    @GetMapping("/scambi")
    public ResponseEntity<Object> getScambi() {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getScambi.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<Scambio> scambi = scambiStorage.getScambi();
        if (scambi.isEmpty()) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Scambi non inizializzati. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(scambi);
        }
    }

    @Operation(summary = "Get sensori posizione", description = "Recupera lo stato di tutti i sensori di posizione")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sensori di posizione recuperati con successo"),
    })
    @GetMapping("/autopilot/sensoriPosizione")
    public ResponseEntity<Object> getSensoriDiPosizione() {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getSensoriPosizione.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<SensorePosizione> sensoriPosizione = sensoriPosizioneStorage.getSensoriPosizione();
        if (sensoriPosizione.isEmpty()) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Sensori posizione non inizializzati. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(sensoriPosizione);
        }
    }

    @Operation(summary = "Get sezioni", description = "Recupera lo stato di tutte le sezioni")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sezioni recuperate con successo"),
    })
    @GetMapping("/autopilot/sezioni")
    public ResponseEntity<Object> getSezioni() {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getSezioni.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<Sezione> sezioni = sezioniStorage.getTutteSezioni();
        if (sezioni.isEmpty()) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Sezioni non inizializzate. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(sezioni);
        }

    }

    @GetMapping("/autopilot/info")
    public ResponseEntity<Object> getInfoAutopilot() {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getInfoAutopilot.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        InfoAutopilot infoAutopilot = infoAutopilotStorage.getInfoAutopilot();
        if (infoAutopilot.getModalitaAutopilot() == null || infoAutopilot.getStatoAutopilot() == null) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", "Info autopilot non inizializzate. Riavviare Arduino per consentire aggiornamento.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .headers(headers)
                    .body(errorResponse);
        } else {
            return ResponseEntity.ok().headers(headers).body(infoAutopilot);
        }

    }

    // Gestione delle eccezioni di validazione, ovvero quando riceviamo una request senza dei parametri richiesti oppure a @NotNull
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
}
