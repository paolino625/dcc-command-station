package controller.rest.arduino.get.convogli.infoBasilariConvogli;

import model.ConvoglioInfoBase;
import model.Convoglio;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ArduinoGetConvogliControllerFactory {
    public ArduinoGetConvogliControllerResponse createArduinoGetConvogliResponse(List<Convoglio> convogli) {
        ArduinoGetConvogliControllerResponse response = new ArduinoGetConvogliControllerResponse();
        List<ConvoglioInfoBase> convogliInfoBase = new ArrayList<>();

        for (Convoglio convoglio : convogli) {
            ConvoglioInfoBase convoglioInfo = convoglio.getConvoglioInfoBase();
            convogliInfoBase.add(convoglioInfo);
        }

        response.setConvogli(convogliInfoBase);

        return response;
    }
}
