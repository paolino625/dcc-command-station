package controller.rest.arduino.get.convogli.sezioniCompatibiliConvoglio.dto;

import lombok.Data;

@Data
public class ArduinoGetSezioniCompatibiliConvoglioRequest {
    int idConvoglio;
}
