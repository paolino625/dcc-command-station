package controller.rest.arduino;


import exception.ArduinoTimeoutException;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import service.InvioRichiestaRestArduinoService;

@RestController
@RequestMapping("/arduinoProxy")
@CrossOrigin(origins = "*") // Permetto le richieste da qualsiasi origine
public class ArduinoProxyController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private InvioRichiestaRestArduinoService invioRichiestaRestArduinoService;

    @Value("${arduino.url}")
    private String arduinoUrl;

    // Quando la richiesta dall'esterno, devo togliere "/arduinoProxy" dall'URL e inoltrare la richiesta a Arduino
    @RequestMapping(value = "/**", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
    public ResponseEntity<Object> inoltraRichiestaArduino(HttpMethod method, HttpEntity<String> requestEntity, @RequestHeader Map<String, String> headers, HttpServletRequest request) throws ArduinoTimeoutException {
        String url = arduinoUrl + request.getRequestURI().substring("/arduinoProxy".length());
        return invioRichiestaRestArduinoService.execute(method, requestEntity, headers, url);
    }

}