package controller.rest.arduino.get.convogli.infoBasilariConvogli;

import model.ConvoglioInfoBase;
import lombok.Data;

import java.util.List;

@Data
public class ArduinoGetConvogliControllerResponse {
    List<ConvoglioInfoBase> convogli;
}
