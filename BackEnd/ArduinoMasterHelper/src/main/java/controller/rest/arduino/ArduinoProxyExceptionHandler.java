package controller.rest.arduino;

import exception.ArduinoTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

// Questa classe gestisce le eccezioni lanciate dai metodi del controller ArduinoProxyController.
@ControllerAdvice(assignableTypes = ArduinoProxyController.class)
public class ArduinoProxyExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    // Gestiamo l'eccezione ArduinoTimeoutException, che viene lanciata quando Arduino non risponde entro il timeout.
    @ExceptionHandler(ArduinoTimeoutException.class)
    public ResponseEntity<Object> handleArduinoTimeoutException(ArduinoTimeoutException ex, WebRequest request) {
        logger.error("Errore: ", ex);
        HttpHeaders headers = new HttpHeaders();
        Map<String, String> responseBodyMap = Map.of("error", "Arduino non ha risposto entro il timeout.");
        return new ResponseEntity<>(responseBodyMap, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}