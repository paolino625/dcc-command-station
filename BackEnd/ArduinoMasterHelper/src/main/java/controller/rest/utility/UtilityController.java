package controller.rest.utility;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import service.ResetStorageService;
import storage.*;
import storage.percorsi.PercorsiStorage;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/utility")
@CrossOrigin(origins = "*") // Permetto le richieste da qualsiasi origine
public class UtilityController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Value("${logging.controller.rest.percorsi}")
    private boolean loggingControllerMqtt;

    @Autowired
    ResetStorageService resetStorageService;

    @Autowired
    ConvogliStorage convogliStorage;

    @Autowired
    InfoAutopilotStorage infoAutopilotStorage;

    @Autowired
    LocomotiveStorage locomotiveStorage;

    @Autowired
    ScambiStorage scambiStorage;

    @Autowired
    SensoriPosizioneStorage sensoriPosizioneStorage;

    @Autowired
    SezioniStorage sezioniStorage;

    @Operation(summary = "Verifica connessione", description = "Verifica la connessione al server")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Connessione verificata con successo")
    })
    @GetMapping("/healthCheck")
    public ResponseEntity<Object> healthCheck() {
        if (loggingControllerMqtt) {
            logger.info("Richiesta REST: healthCheck.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Preparo il body di risposta
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("info", "Health Check OK");

        return ResponseEntity.ok().headers(headers).body(responseBody);
    }

    @Operation(summary = "Verifica se ArduinoMasterHelper è in status ready", description = "Per ready si intende che ArduinoMasterHelper ha ricevuto lo stato di tutti gli oggetti da ArduinoMaster")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Viene specificato nel body se ArduinoMasterHelper è ready o meno.")
    })
    @GetMapping("/ready")
    public ResponseEntity<Object> ready() {
        if (loggingControllerMqtt) {
            logger.info("Richiesta REST: ready.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        boolean ready = true;
        String messaggioDiErrore = "";

        // Effettuo i controlli nello stesso ordine in cui ArduinoMaster invia le informazioni a ArduinoMasterHelper
        if (!locomotiveStorage.isReady()) {
            String messaggio = "LocomotiveStorage non è pronto";
            logger.warn(messaggio);
            messaggioDiErrore = messaggio;
            ready = false;
        } else if (!convogliStorage.isReady()) {
            String messaggio = "ConvogliStorage non è pronto";
            logger.warn(messaggio);
            messaggioDiErrore = messaggio;
            ready = false;
        } else if (!scambiStorage.isReady()) {
            String messaggio = "ScambiStorage non è pronto";
            logger.warn(messaggio);
            messaggioDiErrore = messaggio;
            ready = false;
        } else if (!sensoriPosizioneStorage.isReady()) {
            String messaggio = "SensoriPosizioneStorage non è pronto";
            logger.warn(messaggio);
            messaggioDiErrore = messaggio;
            ready = false;
        } else if (!sezioniStorage.isReady()) {
            String messaggio = "SezioniStorage non è pronto";
            logger.warn(messaggio);
            messaggioDiErrore = messaggio;
            ready = false;
        } else if (!infoAutopilotStorage.isReady()) {
            String messaggio = "InfoAutopilotStorage non è pronto";
            logger.warn(messaggio);
            messaggioDiErrore = messaggio;
            ready = false;
        } else {
            logger.info("Tutti gli storage sono pronti");
        }

        // Preparo il response body
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("ready", ready);
        if (!ready) {
            responseBody.put("error", messaggioDiErrore);
        }

        return ResponseEntity.ok().headers(headers).body(responseBody);
    }

    @Operation(summary = "Reset storage", description = "Vengono cancellate tutte le informazioni memorizzate")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reset avvenuto con successo")
    })
    @PostMapping("/reset")
    public ResponseEntity<Object> reset() {
        if (loggingControllerMqtt) {
            logger.info("Richiesta REST: reset.");
        }

        resetStorageService.execute();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Preparo il body di risposta
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("info", "Reset storage OK");

        return ResponseEntity.ok().headers(headers).body(responseBody);
    }

    // Gestione delle eccezioni di validazione, ovvero quando riceviamo una request senza dei parametri richiesti oppure a @NotNull
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
}
