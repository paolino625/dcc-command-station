package controller.rest.percorsi.dto;

import lombok.Data;
import model.IdSezione;

import java.util.ArrayList;
import java.util.List;

@Data
public class PercorsoResponse {
    IdSezione idSezionePartenza;
    IdSezione idSezioneArrivo;
    List<IdSezione> idSezioniIntermedie = new ArrayList<>();
}
