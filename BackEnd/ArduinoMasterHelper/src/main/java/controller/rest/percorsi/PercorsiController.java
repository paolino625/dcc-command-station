package controller.rest.percorsi;

import controller.rest.percorsi.dto.PercorsoConEntrambeDirezioniRequest;
import controller.rest.percorsi.dto.PercorsoConUnaSolaDirezioneRequest;
import controller.rest.percorsi.dto.PercorsoResponse;
import controller.rest.percorsi.factory.PercorsoResponseFactory;
import exception.PercorsoNonEsisteException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import storage.percorsi.PercorsiStorage;
import model.Percorso;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/percorso")
@CrossOrigin(origins = "*") // Permetto le richieste da qualsiasi origine
public class PercorsiController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Value("${logging.controller.rest.percorsi}")
    private boolean loggingAbilitato;

    @Autowired
    private PercorsiStorage percorsiStorage;

    @Operation(summary = "Get percorso", description = "Recupera il percorso richiesto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Percorso trovato con successo"),
            @ApiResponse(responseCode = "400", description = "Richiesta non valida"),
            @ApiResponse(responseCode = "500", description = "Errore interno del server")
    })
    @PostMapping("/get")
    public ResponseEntity<Object> getPercorso(@Valid @RequestBody PercorsoConEntrambeDirezioniRequest percorsoConEntrambeDirezioniRequest) {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: getPercorso.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        try {
            // Recupero il percorso richiesto
            Percorso percorso = percorsiStorage.getPercorso(percorsoConEntrambeDirezioniRequest);

            PercorsoResponse percorsoResponse = PercorsoResponseFactory.createPercorsoResponse(percorso);

            return ResponseEntity.ok().headers(headers).body(percorsoResponse);
        } catch (PercorsoNonEsisteException e) {
            // Se non esiste il percorso richiesto sollevo un errore

            // Preparo il body di risposta
            Map<String, String> responseBody = new HashMap<>();
            responseBody.put("error", "Il percorso richiesto non esiste");

            return ResponseEntity.internalServerError().headers(headers).body(responseBody);
        }

    }

    @Operation(summary = "Esiste percorso - Entrambe direzioni", description = "Verifica se esiste il percorso in una delle due direzioni")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La richiesta è stata elaborata con successo"),
            @ApiResponse(responseCode = "400", description = "Richiesta non valida"),
            @ApiResponse(responseCode = "500", description = "Errore interno del server")
    })
    @PostMapping("/esiste/entrambeDirezioni")
    public ResponseEntity<Object> esistePercorsoEntrambeDirezioni(@Valid @RequestBody PercorsoConEntrambeDirezioniRequest percorsoConEntrambeDirezioniRequest) {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: esistePercorsoEntrambeDirezioni.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Controllo se esiste il percorso
        boolean risultato = percorsiStorage.esistePercorsoConEntrambeDirezioni(percorsoConEntrambeDirezioniRequest);

        // Preparo il body di risposta
        Map<String, Boolean> responseBody = new HashMap<>();
        responseBody.put("percorsoEsiste", risultato);

        return ResponseEntity.ok().headers(headers).body(responseBody);
    }

    @Operation(summary = "Esiste percorso - Una sola direzione", description = "Verifica se esiste il percorso in una sola delle due direzioni")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La richiesta è stata elaborata con successo"),
            @ApiResponse(responseCode = "400", description = "Richiesta non valida"),
            @ApiResponse(responseCode = "500", description = "Errore interno del server")
    })
    @PostMapping("/esiste/unaSolaDirezione")
    public ResponseEntity<Object> esistePercorsoUnaSolaDirezione(@Valid @RequestBody PercorsoConUnaSolaDirezioneRequest percorsoConUnaSolaDirezioneRequest) {
        if (loggingAbilitato) {
            logger.info("Richiesta REST: esistePercorsoUnaSolaDirezione.");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Controllo se esiste il percorso
        boolean risultato = percorsiStorage.esistePercorsoConUnaSolaDirezione(percorsoConUnaSolaDirezioneRequest);

        // Preparo il body di risposta
        Map<String, Boolean> responseBody = new HashMap<>();
        responseBody.put("percorsoEsiste", risultato);

        return ResponseEntity.ok().headers(headers).body(responseBody);
    }

    // Gestione delle eccezioni di validazione, ovvero quando riceviamo una request senza dei parametri richiesti oppure a @NotNull
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

}
