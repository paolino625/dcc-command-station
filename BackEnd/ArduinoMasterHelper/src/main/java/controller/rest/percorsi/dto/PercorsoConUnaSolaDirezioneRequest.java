package controller.rest.percorsi.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import model.IdSezione;

@Data
public class PercorsoConUnaSolaDirezioneRequest {
    @NotNull
    IdSezione idSezionePartenza;

    @NotNull
    IdSezione idSezioneArrivo;

    @NotNull
    boolean direzionePrimaLocomotivaDestra;

}
