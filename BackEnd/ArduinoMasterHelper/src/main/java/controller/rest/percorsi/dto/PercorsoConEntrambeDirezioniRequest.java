package controller.rest.percorsi.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import model.IdSezione;

@Data
public class PercorsoConEntrambeDirezioniRequest {
    @NotNull
    IdSezione idSezionePartenza;

    @NotNull
    IdSezione idSezioneArrivo;
}
