package controller.rest.percorsi.factory;

import controller.rest.percorsi.dto.PercorsoResponse;
import model.Percorso;
import org.springframework.stereotype.Component;

@Component
public class PercorsoResponseFactory {
    public static PercorsoResponse createPercorsoResponse(Percorso percorso) {
        PercorsoResponse percorsoResponse = new PercorsoResponse();
        percorsoResponse.setIdSezionePartenza(percorso.getIdSezionePartenza());
        percorsoResponse.setIdSezioneArrivo(percorso.getIdSezioneArrivo());
        percorsoResponse.setIdSezioniIntermedie(percorso.getIdSezioniIntermedie());
        return percorsoResponse;
    }
}
