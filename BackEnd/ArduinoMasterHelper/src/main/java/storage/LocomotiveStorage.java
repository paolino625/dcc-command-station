package storage;

import lombok.Getter;
import model.Locomotiva;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@Component
public class LocomotiveStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private final List<Locomotiva> locomotive = new ArrayList<>();

    public void salvaLocomotiva(Locomotiva locomotivaDaSalvare) {
        // Se c'è già una locomotiva con lo stesso ID la sovrascrivo
        for (int i = 0; i < locomotive.size(); i++) {
            if (locomotive.get(i).getId() == locomotivaDaSalvare.getId()) {
                locomotive.set(i, locomotivaDaSalvare);
                logger.info("Aggiornata locomotiva n° {}: {}", locomotivaDaSalvare.getId(), locomotivaDaSalvare);
                return;
            }
        }

        // Se non ho trovato la locomotiva, la aggiungo
        locomotive.add(locomotivaDaSalvare);

        logger.info("Aggiunta locomotiva: {}", locomotivaDaSalvare);
    }

    @Override
    public void reset() {
        locomotive.clear();
        logger.info("Storage locomotive resettato");
    }

    @Override
    public boolean isReady() {
        return !locomotive.isEmpty();
    }
}
