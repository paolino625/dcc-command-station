package storage;

import lombok.Getter;
import model.Scambio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@Component
public class ScambiStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private List<Scambio> scambi = new ArrayList<>();

    public void salvaScambio(Scambio scambioDaSalvare) {
        // Se c'è già uno scambio con lo stesso ID lo sovrascrivo
        for (int i = 0; i < scambi.size(); i++) {
            if (scambi.get(i).getId() == scambioDaSalvare.getId()) {
                scambi.set(i, scambioDaSalvare);
                logger.info("Aggiornato scambio: {}", scambioDaSalvare);
                return;
            }
        }

        // Se non ho trovato lo scambio, lo aggiungo
        scambi.add(scambioDaSalvare);

        logger.info("Aggiunto scambio: {}", scambioDaSalvare);
    }

    @Override
    public void reset() {
        scambi.clear();
        logger.info("Storage scambi resettato");
    }

    @Override
    public boolean isReady() {
        return !scambi.isEmpty();
    }
}
