package storage;

import model.Convoglio;
import model.ConvoglioInfoBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvogliStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private final List<Convoglio> convogli = new ArrayList<>();

    public void salvaConvoglio(ConvoglioInfoBase convoglioInfoBase) {
        // Se c'è già un convoglio con lo stesso ID lo sovrascrivo
        for (int i = 0; i < convogli.size(); i++) {
            Convoglio convoglioPrecedente = convogli.get(i);
            if (convoglioPrecedente.getConvoglioInfoBase().getId() == convoglioInfoBase.getId()) {
                Convoglio convoglioAggiornato = new Convoglio();
                convoglioAggiornato.setConvoglioInfoBase(convoglioInfoBase); // Salviamo le nuove info che ci sono arrivate
                convoglioAggiornato.setSezioniStazioneCompatibiliModalitaAutopilot(convoglioPrecedente.getSezioniStazioneCompatibiliModalitaAutopilot()); // Manteniamo le sezioni compatibili che avevamo già trovato
                convoglioAggiornato.setSezioniStazioneCompatibiliModalitaManuale(convoglioPrecedente.getSezioniStazioneCompatibiliModalitaManuale()); // Manteniamo le sezioni compatibili che avevamo già trovato
                
                convogli.set(i, convoglioAggiornato);
                logger.info("Aggiornato convoglio: {}", convoglioAggiornato);
                return;
            }
        }

        // Se non ho trovato il convoglio, lo aggiungo
        Convoglio convoglioDaSalvare = new Convoglio();
        convoglioDaSalvare.setConvoglioInfoBase(convoglioInfoBase);
        // Le sezioni compatibili saranno calcolate in un secondo momento
        convogli.add(convoglioDaSalvare);

        logger.info("Aggiunto convoglio: {}", convoglioDaSalvare);
    }

    public List<Convoglio> getTuttiConvogli() {
        return convogli;
    }

    public Convoglio getConvoglio(int idConvoglio) {
        for (Convoglio convoglio : convogli) {
            if (convoglio.getConvoglioInfoBase().getId() == idConvoglio) {
                return convoglio;
            }
        }
        return null;
    }

    @Override
    public void reset() {
        convogli.clear();
        logger.info("Storage convogli resettato");
    }

    @Override
    public boolean isReady() {
        if (convogli.isEmpty()) {
            return false;
        }
        for (Convoglio convoglio : convogli) {
            // Controllo per ogni convoglio che ci siano le informazioni base e le sezioni compatibili
            // Le informazioni base possono essere inizializzate senza che le sezioni compatibili lo siano, poiché sono chiamate REST differenti
            if (convoglio.getConvoglioInfoBase() == null || convoglio.getSezioniStazioneCompatibiliModalitaAutopilot() == null || convoglio.getSezioniStazioneCompatibiliModalitaManuale() == null) {
                return false;
            }
        }
        return true;
    }
}
