package storage;

import model.Sezione;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SezioniStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private List<Sezione> sezioni = new ArrayList<>();

    public void salvaSezione(Sezione sezioneDaSalvare) {
        // Se c'è già una sezione con lo stesso ID la sovrascrivo
        for (int i = 0; i < sezioni.size(); i++) {
            if (sezioni.get(i).getId().equals(sezioneDaSalvare.getId())) {
                sezioni.set(i, sezioneDaSalvare);
                logger.info("Aggiornata sezione: {}", sezioneDaSalvare);
                return;
            }
        }

        // Se non ho trovato la sezione, la aggiungo
        sezioni.add(sezioneDaSalvare);

        logger.info("Aggiunta sezione: {}", sezioneDaSalvare.toString());
    }

    public List<Sezione> getTutteSezioni() {
        return sezioni;
    }

    public List<Sezione> getSezioniDiStazione() {
        return sezioni.stream()
                .filter(sezione -> sezione.getId().isStazione())
                .collect(Collectors.toList());
    }

    @Override
    public void reset() {
        sezioni.clear();
        logger.info("Storage sezioni resettato");
    }

    @Override
    public boolean isReady() {
        return !sezioni.isEmpty();
    }
}
