package storage;

import lombok.Getter;
import model.SensorePosizione;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@Component
public class SensoriPosizioneStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private List<SensorePosizione> sensoriPosizione = new ArrayList<>();

    public void salvaSensorePosizione(SensorePosizione sensorePosizione) {
        for (int i = 0; i < sensoriPosizione.size(); i++) {
            if (sensoriPosizione.get(i).getId() == sensorePosizione.getId()) {
                sensoriPosizione.set(i, sensorePosizione);
                logger.info("Aggiornato sensore di posizione: {}", sensorePosizione);
                return;
            }
        }
        sensoriPosizione.add(sensorePosizione);
        logger.info("Aggiunto sensore di posizione: {}", sensorePosizione);
    }

    @Override
    public void reset() {
        sensoriPosizione.clear();
        logger.info("Storage sensori di posizione resettato");
    }

    @Override
    public boolean isReady() {
        return !sensoriPosizione.isEmpty();
    }
}
