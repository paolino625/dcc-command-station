package storage;

public interface Storage {
    void reset();

    boolean isReady();
}
