package storage.percorsi.percorsiDaStazione;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import model.IdSezione;
import model.Percorso;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class PercorsiDaStazione3 {

    private final List<Percorso> percorsi = new ArrayList<>();

    @PostConstruct
    public void init() {

        // *** PERCORSI DALLA STAZIONE 3 ALLA STAZIONE 1 *** //

        // ** PERCORSI DAL BINARIO 1 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_1_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_1_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_1_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_1_a_1_1_2_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 2 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_2_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_2_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_2_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_2_a_1_1_2_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 3 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_3_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_3_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_3_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_3_a_1_1_2_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 4 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_4_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_4_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_4_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_4_a_1_1_2_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 5 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_5_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_5_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_5_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_5_a_1_1_2_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 6 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_6_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_6_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_6_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_6_a_1_1_2_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 7 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 1 ** //

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_7_a_1_1_1_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_7_a_1_1_2_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_7_a_1_1_1_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_7_a_1_1_2_modalitaSensoAntiorario();

        // *** PERCORSI DALLA STAZIONE 3 ALLA STAZIONE 2 *** //

        // ** PERCORSI DAL BINARIO 1 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_1_a_1_2_1();
        aggiungoPercorso_da_1_3_1_a_1_2_2();
        aggiungoPercorso_da_1_3_1_a_1_2_3();
        aggiungoPercorso_da_1_3_1_a_1_2_4();
        aggiungoPercorso_da_1_3_1_a_1_2_5();
        aggiungoPercorso_da_1_3_1_a_1_2_6();
        aggiungoPercorso_da_1_3_1_a_1_2_7();

        // ** PERCORSI DAL BINARIO 2 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_2_a_1_2_1();
        aggiungoPercorso_da_1_3_2_a_1_2_2();
        aggiungoPercorso_da_1_3_2_a_1_2_3();
        aggiungoPercorso_da_1_3_2_a_1_2_4();
        aggiungoPercorso_da_1_3_2_a_1_2_5();
        aggiungoPercorso_da_1_3_2_a_1_2_6();
        aggiungoPercorso_da_1_3_2_a_1_2_7();

        // ** PERCORSI DAL BINARIO 3 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_3_a_1_2_1();
        aggiungoPercorso_da_1_3_3_a_1_2_2();
        aggiungoPercorso_da_1_3_3_a_1_2_3();
        aggiungoPercorso_da_1_3_3_a_1_2_4();
        aggiungoPercorso_da_1_3_3_a_1_2_5();
        aggiungoPercorso_da_1_3_3_a_1_2_6();
        aggiungoPercorso_da_1_3_3_a_1_2_7();

        // ** PERCORSI DAL BINARIO 4 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_4_a_1_2_1();
        aggiungoPercorso_da_1_3_4_a_1_2_2();
        aggiungoPercorso_da_1_3_4_a_1_2_3();
        aggiungoPercorso_da_1_3_4_a_1_2_4();
        aggiungoPercorso_da_1_3_4_a_1_2_5();
        aggiungoPercorso_da_1_3_4_a_1_2_6();
        aggiungoPercorso_da_1_3_4_a_1_2_7();

        // ** PERCORSI DAL BINARIO 5 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_5_a_1_2_1();
        aggiungoPercorso_da_1_3_5_a_1_2_2();
        aggiungoPercorso_da_1_3_5_a_1_2_3();
        aggiungoPercorso_da_1_3_5_a_1_2_4();
        aggiungoPercorso_da_1_3_5_a_1_2_5();
        aggiungoPercorso_da_1_3_5_a_1_2_6();
        aggiungoPercorso_da_1_3_5_a_1_2_7();

        // ** PERCORSI DAL BINARIO 6 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_6_a_1_2_1();
        aggiungoPercorso_da_1_3_6_a_1_2_2();
        aggiungoPercorso_da_1_3_6_a_1_2_3();
        aggiungoPercorso_da_1_3_6_a_1_2_4();
        aggiungoPercorso_da_1_3_6_a_1_2_5();
        aggiungoPercorso_da_1_3_6_a_1_2_6();
        aggiungoPercorso_da_1_3_6_a_1_2_7();

        // ** PERCORSI DAL BINARIO 7 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 2 ** //

        aggiungoPercorso_da_1_3_7_a_1_2_1();
        aggiungoPercorso_da_1_3_7_a_1_2_2();
        aggiungoPercorso_da_1_3_7_a_1_2_3();
        aggiungoPercorso_da_1_3_7_a_1_2_4();
        aggiungoPercorso_da_1_3_7_a_1_2_5();
        aggiungoPercorso_da_1_3_7_a_1_2_6();
        aggiungoPercorso_da_1_3_7_a_1_2_7();

        // *** PERCORSI DALLA STAZIONE 3 ALLA STAZIONE 4 *** //

        // ** PERCORSI DAL BINARIO 1 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_1_a_1_4_1();
        aggiungoPercorso_da_1_3_1_a_1_4_2();
        aggiungoPercorso_da_1_3_1_a_1_4_3();
        aggiungoPercorso_da_1_3_1_a_1_4_4();
        aggiungoPercorso_da_1_3_1_a_1_4_5();
        aggiungoPercorso_da_1_3_1_a_1_4_6();
        aggiungoPercorso_da_1_3_1_a_1_4_7();

        // ** PERCORSI DAL BINARIO 2 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_2_a_1_4_1();
        aggiungoPercorso_da_1_3_2_a_1_4_2();
        aggiungoPercorso_da_1_3_2_a_1_4_3();
        aggiungoPercorso_da_1_3_2_a_1_4_4();
        aggiungoPercorso_da_1_3_2_a_1_4_5();
        aggiungoPercorso_da_1_3_2_a_1_4_6();
        aggiungoPercorso_da_1_3_2_a_1_4_7();

        // ** PERCORSI DAL BINARIO 3 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_3_a_1_4_1();
        aggiungoPercorso_da_1_3_3_a_1_4_2();
        aggiungoPercorso_da_1_3_3_a_1_4_3();
        aggiungoPercorso_da_1_3_3_a_1_4_4();
        aggiungoPercorso_da_1_3_3_a_1_4_5();
        aggiungoPercorso_da_1_3_3_a_1_4_6();
        aggiungoPercorso_da_1_3_3_a_1_4_7();

        // ** PERCORSI DAL BINARIO 4 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_4_a_1_4_1();
        aggiungoPercorso_da_1_3_4_a_1_4_2();
        aggiungoPercorso_da_1_3_4_a_1_4_3();
        aggiungoPercorso_da_1_3_4_a_1_4_4();
        aggiungoPercorso_da_1_3_4_a_1_4_5();
        aggiungoPercorso_da_1_3_4_a_1_4_6();
        aggiungoPercorso_da_1_3_4_a_1_4_7();

        // ** PERCORSI DAL BINARIO 5 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_5_a_1_4_1();
        aggiungoPercorso_da_1_3_5_a_1_4_2();
        aggiungoPercorso_da_1_3_5_a_1_4_3();
        aggiungoPercorso_da_1_3_5_a_1_4_4();
        aggiungoPercorso_da_1_3_5_a_1_4_5();
        aggiungoPercorso_da_1_3_5_a_1_4_6();
        aggiungoPercorso_da_1_3_5_a_1_4_7();

        // ** PERCORSI DAL BINARIO 6 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_6_a_1_4_1();
        aggiungoPercorso_da_1_3_6_a_1_4_2();
        aggiungoPercorso_da_1_3_6_a_1_4_3();
        aggiungoPercorso_da_1_3_6_a_1_4_4();
        aggiungoPercorso_da_1_3_6_a_1_4_5();
        aggiungoPercorso_da_1_3_6_a_1_4_6();
        aggiungoPercorso_da_1_3_6_a_1_4_7();

        // ** PERCORSI DAL BINARIO 7 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 4 ** //

        aggiungoPercorso_da_1_3_7_a_1_4_1();
        aggiungoPercorso_da_1_3_7_a_1_4_2();
        aggiungoPercorso_da_1_3_7_a_1_4_3();
        aggiungoPercorso_da_1_3_7_a_1_4_4();
        aggiungoPercorso_da_1_3_7_a_1_4_5();
        aggiungoPercorso_da_1_3_7_a_1_4_6();
        aggiungoPercorso_da_1_3_7_a_1_4_7();

        // *** PERCORSI DALLA STAZIONE 3 ALLA STAZIONE 5 *** //

        // ** PERCORSI DAL BINARIO 1 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_1_a_1_5_1();
        aggiungoPercorso_da_1_3_1_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_1_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_1_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_1_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_1_a_1_5_4_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 2 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_2_a_1_5_1();
        aggiungoPercorso_da_1_3_2_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_2_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_2_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_2_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_2_a_1_5_4_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 3 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_3_a_1_5_1();
        aggiungoPercorso_da_1_3_3_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_3_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_3_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_3_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_3_a_1_5_4_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 4 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_4_a_1_5_1();
        aggiungoPercorso_da_1_3_4_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_4_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_4_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_4_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_4_a_1_5_4_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 5 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_5_a_1_5_1();
        aggiungoPercorso_da_1_3_5_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_5_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_5_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_5_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_5_a_1_5_4_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 6 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_6_a_1_5_1();
        aggiungoPercorso_da_1_3_6_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_6_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_6_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_6_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_6_a_1_5_4_modalitaSensoAntiorario();

        // ** PERCORSI DAL BINARIO 7 DELLA STAZIONE 3 AI BINARI DELLA STAZIONE 5 ** //

        aggiungoPercorso_da_1_3_7_a_1_5_1();
        aggiungoPercorso_da_1_3_7_a_1_5_2();

        // * MODALITA 1 - SENSO ORARIO * //

        aggiungoPercorso_da_1_3_7_a_1_5_3_modalitaSensoOrario();
        aggiungoPercorso_da_1_3_7_a_1_5_4_modalitaSensoOrario();

        // * MODALITA 1 - SENSO ANTIORARIO * //

        aggiungoPercorso_da_1_3_7_a_1_5_3_modalitaSensoAntiorario();
        aggiungoPercorso_da_1_3_7_a_1_5_4_modalitaSensoAntiorario();

    }

    private void aggiungoPercorso_da_1_3_1_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }


    private void aggiungoPercorso_da_1_3_6_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_1_1_modalitaSensoOrario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 1));
        idSezioniIntermedie.add(new IdSezione(2, 2));
        idSezioniIntermedie.add(new IdSezione(3, 1, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_1_2_modalitaSensoOrario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 2));
        idSezioniIntermedie.add(new IdSezione(2, 33));
        idSezioniIntermedie.add(new IdSezione(3, 36, 2));
        idSezioniIntermedie.add(new IdSezione(2, 44));
        idSezioniIntermedie.add(new IdSezione(3, 34, 1));
        idSezioniIntermedie.add(new IdSezione(3, 32, 1));
        idSezioniIntermedie.add(new IdSezione(3, 31, 1));
        idSezioniIntermedie.add(new IdSezione(3, 30, 1));
        idSezioniIntermedie.add(new IdSezione(3, 16, 1));
        idSezioniIntermedie.add(new IdSezione(2, 7));
        idSezioniIntermedie.add(new IdSezione(3, 4, 2));
        idSezioniIntermedie.add(new IdSezione(4, 1, 2));
        idSezioniIntermedie.add(new IdSezione(3, 2, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_1_1_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 1));
        idSezioniIntermedie.add(new IdSezione(4, 1, 1));
        idSezioniIntermedie.add(new IdSezione(3, 1, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_1_2_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 1, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 2));
        idSezioniIntermedie.add(new IdSezione(3, 22, 2));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 7, 2));
        idSezioniIntermedie.add(new IdSezione(2, 4));
        idSezioniIntermedie.add(new IdSezione(3, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 3));
        idSezioniIntermedie.add(new IdSezione(3, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 1));
        idSezioniIntermedie.add(new IdSezione(3, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_1() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_2() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_3() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_4() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_5() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_6() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_2_7() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_1() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_2() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_3() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_4() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_5() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_6() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_2_7() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_1() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_2() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_3() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_4() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_5() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_6() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_2_7() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_1() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_2() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_3() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_4() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_5() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_6() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_2_7() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_1() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_2() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_3() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_4() {
        Percorso percorso = new Percorso();
        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_2_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_2_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 2));
        idSezioniIntermedie.add(new IdSezione(4, 2, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 2));
        idSezioniIntermedie.add(new IdSezione(4, 3, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 2));
        idSezioniIntermedie.add(new IdSezione(3, 55, 2));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 53, 2));
        idSezioniIntermedie.add(new IdSezione(2, 41));
        idSezioniIntermedie.add(new IdSezione(3, 49, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 12, 2));
        idSezioniIntermedie.add(new IdSezione(4, 4, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_2_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 2, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 1));
        idSezioniIntermedie.add(new IdSezione(2, 14));
        idSezioniIntermedie.add(new IdSezione(3, 23, 1));
        idSezioniIntermedie.add(new IdSezione(4, 2, 2));
        idSezioniIntermedie.add(new IdSezione(2, 15));
        idSezioniIntermedie.add(new IdSezione(3, 18, 1));
        idSezioniIntermedie.add(new IdSezione(4, 3, 2));
        idSezioniIntermedie.add(new IdSezione(2, 18));
        idSezioniIntermedie.add(new IdSezione(3, 11, 1));
        idSezioniIntermedie.add(new IdSezione(4, 4, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 1));
        idSezioniIntermedie.add(new IdSezione(4, 5, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_3() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_4() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 1));
        idSezioniIntermedie.add(new IdSezione(4, 6, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_5() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 5));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_6() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 6));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 1));
        idSezioniIntermedie.add(new IdSezione(2, 21));
        idSezioniIntermedie.add(new IdSezione(3, 12, 1));
        idSezioniIntermedie.add(new IdSezione(2, 19));
        idSezioniIntermedie.add(new IdSezione(3, 19, 1));
        idSezioniIntermedie.add(new IdSezione(2, 16));
        idSezioniIntermedie.add(new IdSezione(3, 24, 1));
        idSezioniIntermedie.add(new IdSezione(2, 13));
        idSezioniIntermedie.add(new IdSezione(3, 41, 2));
        idSezioniIntermedie.add(new IdSezione(2, 36));
        idSezioniIntermedie.add(new IdSezione(3, 45, 2));
        idSezioniIntermedie.add(new IdSezione(2, 39));
        idSezioniIntermedie.add(new IdSezione(3, 49, 1));
        idSezioniIntermedie.add(new IdSezione(4, 7, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_4_7() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 4, 7));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 28, 2));
        idSezioniIntermedie.add(new IdSezione(2, 34));
        idSezioniIntermedie.add(new IdSezione(3, 40, 2));
        idSezioniIntermedie.add(new IdSezione(4, 5, 2));
        idSezioniIntermedie.add(new IdSezione(2, 35));
        idSezioniIntermedie.add(new IdSezione(3, 44, 2));
        idSezioniIntermedie.add(new IdSezione(4, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 38));
        idSezioniIntermedie.add(new IdSezione(3, 48, 2));
        idSezioniIntermedie.add(new IdSezione(4, 7, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 10, 2));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_1_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 1));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 51, 1));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 13, 2));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_2_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 2));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 50, 1));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 17, 2));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_3_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 3));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 47, 1));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 20, 2));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_4_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 4));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 21, 2));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 43, 1));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_5_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 5));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 46, 1));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 2));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_6_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 6));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 1));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_5_1() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 1));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_5_2() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 2));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_5_3_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_5_4_modalitaSensoOrario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 25, 1));
        idSezioniIntermedie.add(new IdSezione(3, 21, 1));
        idSezioniIntermedie.add(new IdSezione(2, 17));
        idSezioniIntermedie.add(new IdSezione(3, 20, 1));
        idSezioniIntermedie.add(new IdSezione(3, 17, 1));
        idSezioniIntermedie.add(new IdSezione(2, 20));
        idSezioniIntermedie.add(new IdSezione(3, 13, 1));
        idSezioniIntermedie.add(new IdSezione(3, 10, 1));
        idSezioniIntermedie.add(new IdSezione(2, 22));
        idSezioniIntermedie.add(new IdSezione(3, 9, 1));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 6, 1));
        idSezioniIntermedie.add(new IdSezione(3, 7, 1));
        idSezioniIntermedie.add(new IdSezione(2, 8));
        idSezioniIntermedie.add(new IdSezione(3, 22, 1));
        idSezioniIntermedie.add(new IdSezione(2, 10));
        idSezioniIntermedie.add(new IdSezione(3, 39, 2));
        idSezioniIntermedie.add(new IdSezione(2, 29));
        idSezioniIntermedie.add(new IdSezione(3, 54, 1));
        idSezioniIntermedie.add(new IdSezione(2, 31));
        idSezioniIntermedie.add(new IdSezione(3, 56, 1));
        idSezioniIntermedie.add(new IdSezione(3, 57, 2));
        idSezioniIntermedie.add(new IdSezione(3, 58, 2));
        idSezioniIntermedie.add(new IdSezione(2, 46));
        idSezioniIntermedie.add(new IdSezione(3, 61, 2));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(false);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_5_3_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 3));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 1));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }

    private void aggiungoPercorso_da_1_3_7_a_1_5_4_modalitaSensoAntiorario() {
        Percorso percorso = new Percorso();

        percorso.setIdSezionePartenza(new IdSezione(1, 3, 7));
        percorso.setIdSezioneArrivo(new IdSezione(1, 5, 4));

        List<IdSezione> idSezioniIntermedie = new ArrayList<>();

        idSezioniIntermedie.add(new IdSezione(3, 42, 2));
        idSezioniIntermedie.add(new IdSezione(3, 43, 2));
        idSezioniIntermedie.add(new IdSezione(2, 37));
        idSezioniIntermedie.add(new IdSezione(3, 46, 2));
        idSezioniIntermedie.add(new IdSezione(3, 47, 2));
        idSezioniIntermedie.add(new IdSezione(2, 40));
        idSezioniIntermedie.add(new IdSezione(3, 50, 2));
        idSezioniIntermedie.add(new IdSezione(3, 51, 2));
        idSezioniIntermedie.add(new IdSezione(2, 42));
        idSezioniIntermedie.add(new IdSezione(3, 52, 2));
        idSezioniIntermedie.add(new IdSezione(3, 53, 1));
        idSezioniIntermedie.add(new IdSezione(2, 32));
        idSezioniIntermedie.add(new IdSezione(3, 55, 1));
        idSezioniIntermedie.add(new IdSezione(2, 30));
        idSezioniIntermedie.add(new IdSezione(3, 38, 2));
        idSezioniIntermedie.add(new IdSezione(2, 28));
        idSezioniIntermedie.add(new IdSezione(3, 28, 1));
        idSezioniIntermedie.add(new IdSezione(2, 12));
        idSezioniIntermedie.add(new IdSezione(3, 27, 2));
        idSezioniIntermedie.add(new IdSezione(2, 11));
        idSezioniIntermedie.add(new IdSezione(3, 26, 1));
        idSezioniIntermedie.add(new IdSezione(2, 9));
        idSezioniIntermedie.add(new IdSezione(3, 6, 2));
        idSezioniIntermedie.add(new IdSezione(2, 5));
        idSezioniIntermedie.add(new IdSezione(3, 8, 2));
        idSezioniIntermedie.add(new IdSezione(3, 9, 2));
        idSezioniIntermedie.add(new IdSezione(2, 23));
        idSezioniIntermedie.add(new IdSezione(3, 14, 2));
        idSezioniIntermedie.add(new IdSezione(3, 15, 2));
        idSezioniIntermedie.add(new IdSezione(2, 25));
        idSezioniIntermedie.add(new IdSezione(3, 29, 2));
        idSezioniIntermedie.add(new IdSezione(3, 31, 2));
        idSezioniIntermedie.add(new IdSezione(3, 32, 2));
        idSezioniIntermedie.add(new IdSezione(3, 33, 2));
        idSezioniIntermedie.add(new IdSezione(3, 35, 2));
        idSezioniIntermedie.add(new IdSezione(2, 45));
        idSezioniIntermedie.add(new IdSezione(3, 61, 1));
        idSezioniIntermedie.add(new IdSezione(3, 62, 2));

        percorso.setIdSezioniIntermedie(idSezioniIntermedie);

        percorso.setDirezionePrimaLocomotivaDestra(true);

        percorsi.add(percorso);
    }
}
