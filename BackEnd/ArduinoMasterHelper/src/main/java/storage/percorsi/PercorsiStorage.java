package storage.percorsi;

import controller.rest.percorsi.dto.PercorsoConEntrambeDirezioniRequest;
import controller.rest.percorsi.dto.PercorsoConUnaSolaDirezioneRequest;
import exception.PercorsoNonEsisteException;
import jakarta.annotation.PostConstruct;
import model.Percorso;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storage.Storage;
import storage.percorsi.percorsiDaStazione.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class PercorsiStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private final List<Percorso> percorsi = new ArrayList<>();

    @Autowired
    PercorsiDaStazione1 percorsiDaStazione1;

    @Autowired
    PercorsiDaStazione2 percorsiDaStazione2;

    @Autowired
    PercorsiDaStazione3 percorsiDaStazione3;

    @Autowired
    PercorsiDaStazione4 percorsiDaStazione4;

    @Autowired
    PercorsiDaStazione5 percorsiDaStazione5;

    @PostConstruct
    public void init() {
        // Aggiungo percorsi da stazione 1

        percorsi.addAll(percorsiDaStazione1.getPercorsi());

        // Aggiungo percorsi da stazione 2

        percorsi.addAll(percorsiDaStazione2.getPercorsi());

        // Aggiungo percorsi da stazione 3

        percorsi.addAll(percorsiDaStazione3.getPercorsi());

        // Aggiungo percorsi da stazione 4

        percorsi.addAll(percorsiDaStazione4.getPercorsi());

        // Aggiungo percorsi da stazione 5

        percorsi.addAll(percorsiDaStazione5.getPercorsi());
    }

    public boolean esistePercorsoConEntrambeDirezioni(PercorsoConEntrambeDirezioniRequest percorsoConEntrambeDirezioniRequest) {
        for (Percorso percorso : percorsi) {
            if (percorso.getIdSezionePartenza().equals(percorsoConEntrambeDirezioniRequest.getIdSezionePartenza()) && percorso.getIdSezioneArrivo().equals(percorsoConEntrambeDirezioniRequest.getIdSezioneArrivo())) {
                return true;
            }
        }

        // Se non ho trovato il percorso restituisco false
        return false;
    }

    public boolean esistePercorsoConUnaSolaDirezione(PercorsoConUnaSolaDirezioneRequest percorsoConUnaSolaDirezioneRequest) {
        for (Percorso percorso : percorsi) {
            if (percorso.getIdSezionePartenza().equals(percorsoConUnaSolaDirezioneRequest.getIdSezionePartenza()) && percorso.getIdSezioneArrivo().equals(percorsoConUnaSolaDirezioneRequest.getIdSezioneArrivo()) && percorso.isDirezionePrimaLocomotivaDestra() == percorsoConUnaSolaDirezioneRequest.isDirezionePrimaLocomotivaDestra()) {
                return true;
            }
        }

        // Se non ho trovato il percorso restituisco false
        return false;
    }

    public Percorso getPercorso(PercorsoConEntrambeDirezioniRequest percorsoConEntrambeDirezioniRequest) throws PercorsoNonEsisteException {
        // Potrebbero esserci più percorsi con la stessa sezione di partenza e arrivo, dunque scansiono tutti i percorsi e seleziono quelli di interesse
        List<Percorso> percorsiDiInteresse = new ArrayList<>();
        for (Percorso percorso : percorsi) {
            if (percorso.getIdSezionePartenza().equals(percorsoConEntrambeDirezioniRequest.getIdSezionePartenza()) && percorso.getIdSezioneArrivo().equals(percorsoConEntrambeDirezioniRequest.getIdSezioneArrivo())) {
                percorsiDiInteresse.add(percorso);
            }
        }

        // Nel caso in cui la lista non sia vuota, scegliamo un percorso a caso tra quelli trovati
        if (percorsiDiInteresse.isEmpty()) {
            throw new PercorsoNonEsisteException();
        } else {
            Random random = new Random();
            int indiceElementoPercorso = random.nextInt(percorsiDiInteresse.size());
            return percorsiDiInteresse.get(indiceElementoPercorso);
        }
    }

    @Override
    public void reset() {
        percorsi.clear();
        logger.info("Storage percorsi resettato");
    }

    @Override
    public boolean isReady() {
        // Questo storage è sempre pronto dato che viene inizializzato all'avvio dell'applicazione Spring Boot
        return true;
    }
}




