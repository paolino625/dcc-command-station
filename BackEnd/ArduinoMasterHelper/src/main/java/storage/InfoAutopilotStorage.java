package storage;

import lombok.Getter;
import model.InfoAutopilot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Getter
@Component
public class InfoAutopilotStorage implements Storage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private InfoAutopilot infoAutopilot = new InfoAutopilot();

    public void salvaInfoAutopilot(InfoAutopilot infoAutopilotDaSalvare) {
        infoAutopilot = infoAutopilotDaSalvare;
        logger.info("Salvate info autopilot: {}", infoAutopilotDaSalvare);
    }

    @Override
    public void reset() {
        infoAutopilot = new InfoAutopilot();
        logger.info("Storage info autopilot resettato");
    }

    @Override
    public boolean isReady() {
        return !infoAutopilot.isEmpty();
    }
}
