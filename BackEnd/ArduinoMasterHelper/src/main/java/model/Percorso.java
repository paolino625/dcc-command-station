package model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Percorso {
    IdSezione idSezionePartenza;
    IdSezione idSezioneArrivo;
    List<IdSezione> idSezioniIntermedie = new ArrayList<>();

    boolean direzionePrimaLocomotivaDestra;
}
