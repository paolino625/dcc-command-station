package model.mqtt.messaggioConvoglio.autopilot.posizioneConvoglio;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.IdSezione;

public class PosizioneConvoglio {
    @JsonProperty("validita")
    String validita; // Lascio stringa anche se è un enumerazione

    @JsonProperty("idSezioneCorrenteOccupata")
    IdSezione idSezioneCorrenteOccupata;

    @JsonProperty("direzionePrimaLocomotivaDestra")
    boolean direzionePrimaLocomotivaDestra;
}
