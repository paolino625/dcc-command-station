package model.mqtt.messaggioConvoglio.autopilot;

import lombok.Data;
import model.mqtt.messaggioConvoglio.autopilot.posizioneConvoglio.PosizioneConvoglio;

@Data
public class Autopilot {
    PosizioneConvoglio posizioneConvoglio;
    String statoInizializzazioneAutopilotConvoglio; // Lascio stringa anche se è un enumerazione
    String statoAutopilotModalitaStazioneSuccessiva; // Lascio stringa anche se è un enumerazione
    String statoAutopilotModalitaApproccioCasuale; // Lascio stringa anche se è un enumerazione
}
