package model.mqtt.messaggioConvoglio;

import lombok.Data;
import model.mqtt.messaggioConvoglio.autopilot.Autopilot;

@Data
public class MessaggioConvoglio {
    int id;
    String nome;
    int idLocomotiva1;
    int idLocomotiva2;
    int velocitaMassima;
    int velocitaImpostata;
    int velocitaAttuale;
    boolean luciAccese;
    boolean presenteSulTracciato;

    Autopilot autopilot;
}
