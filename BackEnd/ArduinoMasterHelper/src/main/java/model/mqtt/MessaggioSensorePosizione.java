package model.mqtt;

import lombok.Data;

@Data
public class MessaggioSensorePosizione {
    int id;
    boolean stato;
    int idConvoglioInAttesa;
}
