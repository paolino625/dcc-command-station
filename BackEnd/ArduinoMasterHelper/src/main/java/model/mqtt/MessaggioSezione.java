package model.mqtt;

import lombok.Data;
import model.IdSezione;
import model.StatoSezione;

@Data
public class MessaggioSezione {
    IdSezione id;
    StatoSezione stato;
}
