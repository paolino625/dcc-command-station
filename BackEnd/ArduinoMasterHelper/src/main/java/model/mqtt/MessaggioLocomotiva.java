package model.mqtt;

import lombok.Data;

@Data
public class MessaggioLocomotiva {
    int id;
    String nome;
    String codice;
}
