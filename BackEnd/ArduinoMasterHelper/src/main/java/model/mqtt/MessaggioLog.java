package model.mqtt;

import lombok.Data;

@Data
public class MessaggioLog {
    // Inizializzo le stringhe perché se non lo faccio, Arduino AVR non inviano alcuni parametri e ci spacchiamo se inviamo i parametri a Loki a null
    String source = "";
    String level = "";
    String environment = "";
    String file = "";
    String line = "";
    String function = "";
    String message = "";
    boolean lastChunkMessage;
}
