package model.mqtt;

import lombok.Data;

@Data
public class MessaggioInfoAutopilot {
    String modalitaAutopilot;
    String statoAutopilot;
}
