package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class IdSezione {
    /*
    - 1: stazione
    - 2: sezione intermedia
    - 3: scambio
     - 4: incrocio
     */
    int tipologia;

    // Nel caso della stazione indica l'ID della stazione
    int id;

    /*
     Per stazioni: indica il numero del binario
     Per scambio: 1 a sinistra e 2 a destra
     */
    int specifica;

    public IdSezione(int tipologia, int id, int specifica) {
        this.tipologia = tipologia;
        this.id = id;
        this.specifica = specifica;
    }

    public IdSezione(int tipologia, int id) {
        this.tipologia = tipologia;
        this.id = id;
        this.specifica = 0;
    }

    public IdSezione() {
    }

    // Dobbiamo ignorare questo metodo per evitare che venga serializzato in JSON
    @JsonIgnore
    public boolean isStazione() {
        return tipologia == 1;
    }
}
