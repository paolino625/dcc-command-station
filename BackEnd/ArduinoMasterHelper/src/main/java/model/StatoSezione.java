package model;

import lombok.Data;

@Data
public class StatoSezione {
    boolean statoLock;
    int idConvoglio;
}
