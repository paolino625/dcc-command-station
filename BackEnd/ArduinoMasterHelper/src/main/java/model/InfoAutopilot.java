package model;

import lombok.Data;

@Data
public class InfoAutopilot {
    // Lascio stringa anche se è un enumerazione
    String modalitaAutopilot;

    // Lascio stringa anche se è un enumerazione
    String statoAutopilot;

    public boolean isEmpty() {
        return modalitaAutopilot == null || statoAutopilot == null;
    }
}
