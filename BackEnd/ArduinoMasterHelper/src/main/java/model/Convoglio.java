package model;

import lombok.Data;

import java.util.List;

@Data
public class Convoglio {
    ConvoglioInfoBase convoglioInfoBase; // Info del convoglio che arrivano tramite messaggi MQTT da Arduino e che il Front End chiede

    // Informazioni che il Front End richiede soltanto quando deve mostrare le sezioni compatibili di un convoglio (durante scelta del binario di stazione di destinazione)
    List<Sezione> sezioniStazioneCompatibiliModalitaAutopilot;
    List<Sezione> sezioniStazioneCompatibiliModalitaManuale;
}
