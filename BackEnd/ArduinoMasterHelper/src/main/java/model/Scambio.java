package model;

import lombok.Data;

@Data
public class Scambio {
    int id;
    String posizione;
}
