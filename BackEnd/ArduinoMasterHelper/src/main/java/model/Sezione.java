package model;

import lombok.Data;

@Data
public class Sezione {
    IdSezione id;
    StatoSezione stato;
}
