package model;

import lombok.Data;

@Data
public class SensorePosizione {
    int id;
    boolean stato;
    int idConvoglioInAttesa;
}
