package model;

import lombok.Data;

@Data
public class Locomotiva {
    int id;
    String nome;
    String codice;
}
