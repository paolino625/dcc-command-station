package exception;

public class PercorsoNonEsisteException extends Exception {
    public PercorsoNonEsisteException() {
        super("Il percorso richiesto non esiste!");
    }
}
