package exception;

public class ArduinoTimeoutException extends Exception {
    public ArduinoTimeoutException() {
        super("Timeout Arduino");
    }
}
