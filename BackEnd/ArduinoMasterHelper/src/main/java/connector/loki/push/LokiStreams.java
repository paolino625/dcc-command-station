package connector.loki.push;

import lombok.Data;

import java.util.List;

@Data
public class LokiStreams {
    private LokiStream stream;
    private List<List<String>> values;
}
