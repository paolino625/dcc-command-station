package connector.loki.push;

import lombok.Data;

@Data
public class LokiStream {
    // Qui aggiungo i tag che voglio associare al messaggio Loki
    private String source;
    private String level;
    private String environment;
    private String file;
    private String line;
    private String function;
}