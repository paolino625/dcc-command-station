package connector.arduino.dto;

import lombok.Data;
import model.IdSezione;

@Data
public class IsSezioneStazioneCompatibileConConvoglioConnectorRequest {
    IdSezione idSezione;
    int idConvoglio;
}
