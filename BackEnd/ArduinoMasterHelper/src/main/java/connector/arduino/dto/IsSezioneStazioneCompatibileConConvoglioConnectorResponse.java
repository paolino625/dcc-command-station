package connector.arduino.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IsSezioneStazioneCompatibileConConvoglioConnectorResponse {
    boolean result;
}
