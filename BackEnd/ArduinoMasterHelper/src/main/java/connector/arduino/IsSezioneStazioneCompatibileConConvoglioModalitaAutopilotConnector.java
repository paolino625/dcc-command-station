package connector.arduino;

import com.fasterxml.jackson.databind.ObjectMapper;
import connector.arduino.dto.IsSezioneStazioneCompatibileConConvoglioConnectorRequest;
import connector.arduino.dto.IsSezioneStazioneCompatibileConConvoglioConnectorResponse;
import controller.rest.arduino.ArduinoProxyController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import service.InvioRichiestaRestArduinoService;

import java.util.Map;

@Service
public class IsSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector {
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private ArduinoProxyController arduinoProxyController;

    @Value("${arduino.url}")
    private String arduinoUrl;

    @Value("${arduino.authorization-header}")
    private String authorizationHeaderValue;

    private final WebClient.Builder webClientBuilder;

    @Autowired
    private InvioRichiestaRestArduinoService invioRichiestaRestArduinoService;

    @Autowired
    public IsSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    // Effettua una chiamata REST ad Arduino per sapere se una determinata sezione è compatibile con il convoglio passato
    public IsSezioneStazioneCompatibileConConvoglioConnectorResponse modalitaAutopilot(IsSezioneStazioneCompatibileConConvoglioConnectorRequest request) {
        String url = arduinoUrl + "/sezione/puoOspitareConvoglioModalitaAutopilot";

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper.writeValueAsString(request);
            HttpEntity<String> requestEntity = new HttpEntity<>(requestBody);
            ResponseEntity<Object> responseEntity = invioRichiestaRestArduinoService.execute(HttpMethod.POST, requestEntity, Map.of("Authorization", authorizationHeaderValue), url);

            ObjectMapper responseMapper = new ObjectMapper();
            return responseMapper.readValue(responseEntity.getBody().toString(), IsSezioneStazioneCompatibileConConvoglioConnectorResponse.class);
        } catch (Exception e) {
            logger.error("Errore durante l'invio della richiesta ad Arduino", e);
            return null;
        }
    }

    // Effettua una chiamata REST ad Arduino per sapere se una determinata sezione è compatibile con il convoglio passato
    public IsSezioneStazioneCompatibileConConvoglioConnectorResponse modalitaManuale(IsSezioneStazioneCompatibileConConvoglioConnectorRequest request) {
        String url = arduinoUrl + "/sezione/puoOspitareConvoglioModalitaManuale";

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper.writeValueAsString(request);
            HttpEntity<String> requestEntity = new HttpEntity<>(requestBody);
            ResponseEntity<Object> responseEntity = invioRichiestaRestArduinoService.execute(HttpMethod.POST, requestEntity, Map.of("Authorization", authorizationHeaderValue), url);

            ObjectMapper responseMapper = new ObjectMapper();
            return responseMapper.readValue(responseEntity.getBody().toString(), IsSezioneStazioneCompatibileConConvoglioConnectorResponse.class);
        } catch (Exception e) {
            logger.error("Errore durante l'invio della richiesta ad Arduino", e);
            return null;
        }
    }

}

