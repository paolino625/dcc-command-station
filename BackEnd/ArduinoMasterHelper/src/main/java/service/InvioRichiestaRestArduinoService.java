package service;

import exception.ArduinoTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class InvioRichiestaRestArduinoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private final WebClient.Builder webClientBuilder;

    // Se Arduino non risponde entro questo timeout, Arduino Master Helper risponde al chiamante con un messaggio di errore personalizzato.
    private static final int timeoutRichiestaArduinoMillisecondi = 5000;

    @Autowired
    public InvioRichiestaRestArduinoService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    private final Lock lock = new ReentrantLock();

    // Ci assicura che tutte le richieste ad Arduino avvengano in sequenza.
    public ResponseEntity<Object> execute(HttpMethod method, HttpEntity<String> requestEntity, Map<String, String> headers, String url) throws ArduinoTimeoutException {
        lock.lock();
        WebClient webClient = webClientBuilder.build();
        WebClient.RequestBodySpec requestBodySpec = webClient.method(method).uri(url).headers(httpHeaders -> {
            httpHeaders.setAll(headers);
        });

        logger.info("Invio richiesta a Arduino: URL={}, Method={}, Headers={}, Body={}", url, method, headers, requestEntity.getBody());

        WebClient.ResponseSpec responseSpec;
        if (requestEntity.getBody() != null) {
            responseSpec = requestBodySpec.bodyValue(requestEntity.getBody()).retrieve();
        } else {
            responseSpec = requestBodySpec.retrieve();
        }

        try {
            String responseBody = responseSpec.bodyToMono(String.class).timeout(Duration.ofMillis(timeoutRichiestaArduinoMillisecondi)).block();
            // Registra i dettagli della risposta
            logger.info("Risposta da Arduino: Body={}", responseBody);
            return ResponseEntity.ok(responseBody);
        } catch (RuntimeException e) {
            if (e.getCause() instanceof java.util.concurrent.TimeoutException) {
                logger.error("Arduino non ha risposto entro il timeout", e);
                // Lancio qui l'eccezione che verrà gestita da ArduinoProxyExceptionHandler
                throw new ArduinoTimeoutException();
            } else {
                logger.error("Errore durante la richiesta ad Arduino", e);
                throw e;
            }

        } finally {
            lock.unlock();
        }
    }
}
