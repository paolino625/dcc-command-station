package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import storage.*;
import storage.percorsi.PercorsiStorage;

@Component
public class ResetStorageService {
    @Autowired
    ConvogliStorage convogliStorage;

    @Autowired
    InfoAutopilotStorage infoAutopilotStorage;

    @Autowired
    LocomotiveStorage locomotiveStorage;

    @Autowired
    ScambiStorage scambiStorage;

    @Autowired
    SensoriPosizioneStorage sensoriPosizioneStorage;

    @Autowired
    SezioniStorage sezioniStorage;

    public void execute() {
        // Resetto tutti gli storage

        convogliStorage.reset();

        infoAutopilotStorage.reset();

        locomotiveStorage.reset();

        scambiStorage.reset();

        sensoriPosizioneStorage.reset();

        sezioniStorage.reset();

    }
}
