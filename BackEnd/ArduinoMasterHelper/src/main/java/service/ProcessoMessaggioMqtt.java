package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import connector.loki.push.LokiPushRequest;
import factory.DeserializzazioneMessaggiMqttFactory;
import jakarta.annotation.PostConstruct;
import model.*;
import model.mqtt.*;
import model.mqtt.messaggioConvoglio.MessaggioConvoglio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import storage.*;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static factory.LokiPushRequestFactory.createLokiPushRequest;

@Component
public class ProcessoMessaggioMqtt {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Value("${loki.server.url}")
    private String lokiServerUrl;

    @Value("${loki.server.endPoint.push}")
    private String endPointLokiServerPush;

    @Autowired
    DeserializzazioneMessaggiMqttFactory deserializzazioneMessaggiMqttFactory;

    @Autowired
    LocomotiveStorage locomotiveStorage;

    @Autowired
    private ConvogliStorage convogliStorage;

    @Autowired
    private ScambiStorage scambiStorage;

    @Autowired
    private SensoriPosizioneStorage sensoriPosizioneStorage;

    @Autowired
    SezioniStorage sezioniStorage;

    @Autowired
    private InfoAutopilotStorage infoAutopilotStorage;

    @Autowired
    private SalvoBinariStazioniCompatibiliConvogliService salvoBinariStazioniCompatibiliConvogliService;

    @Value("${mqttBroker.topic.locomotiva}")
    private String topicMqttLocomotiva;

    @Value("${mqttBroker.topic.convoglio}")
    private String topicMqttConvoglio;

    @Value("${mqttBroker.topic.scambio}")
    private String topicMqttScambio;

    @Value("${mqttBroker.topic.sensorePosizione}")
    private String topicMqttSensorePosizione;

    @Value("${mqttBroker.topic.sezione}")
    private String topicMqttSezione;

    @Value("${mqttBroker.topic.infoAutopilot}")
    private String topicMqttInfoAutopilot;

    @Value("${mqttBroker.topic.log}")
    private String topicMqttLog;

    @Value("${mqttBroker.topic.arduinoReady}")
    private String arduinoReady;

    @Value("${logging.service.processoMessaggioMqtt.locomotive}")
    private boolean loggingAbilitatoLocomotive;

    @Value("${logging.service.processoMessaggioMqtt.convogli}")
    private boolean loggingAbilitatoConvogli;

    @Value("${logging.service.processoMessaggioMqtt.scambi}")
    private boolean loggingAbilitatoScambi;

    @Value("${logging.service.processoMessaggioMqtt.sensoriPosizione}")
    private boolean loggingAbilitatoSensoriPosizione;

    @Value("${logging.service.processoMessaggioMqtt.sezioni}")
    private boolean loggingAbilitatoSezioni;

    @Value("${logging.service.processoMessaggioMqtt.infoAutopilot}")
    private boolean loggingAbilitatoInfoAutopilot;

    @Value("${logging.service.processoMessaggioMqtt.log}")
    private boolean loggingAbilitatoLog;

    @Value("${logging.service.processoMessaggioMqtt.arduinoReady}")
    private boolean loggingAbilitatoArduinoReady;

    // Mantengo un buffer in memoria per comporre il messaggio per ogni sorgente
    private HashMap<String, MessaggioLog> messaggiLogBuffer = new HashMap<>();

    @PostConstruct
    public void init() {
        logger.info("URL Loki Server: {}", lokiServerUrl);
    }

    public void execute(Message<?> message) {
        String topic = (String) message.getHeaders().get("mqtt_receivedTopic");

        // Non posso usare lo switch perché non funziona con le stringhe topic in quanto non sono statiche
        if (Objects.equals(topic, topicMqttLocomotiva)) {
            gestiscoMessaggioLocomotiva(message);
        } else if (Objects.equals(topic, topicMqttConvoglio)) {
            gestiscoMessaggioConvoglio(message);
        } else if (Objects.equals(topic, topicMqttScambio)) {
            gestiscoMessaggioScambio(message);
        } else if (Objects.equals(topic, topicMqttSensorePosizione)) {
            gestiscoMessaggioSensorePosizione(message);
        } else if (Objects.equals(topic, topicMqttSezione)) {
            gestiscoMessaggioSezione(message);
        } else if (Objects.equals(topic, topicMqttInfoAutopilot)) {
            gestiscoMessaggioInfoAutopilot(message);
        } else if (Objects.equals(topic, topicMqttLog)) {
            gestiscoMessaggioLog(message);
        } else if (Objects.equals(topic, arduinoReady)) {
            gestiscoMessaggioArduinoReady(message);
        } else {
            logger.warn("Ricevuto messaggio MQTT su un topic sconosciuto: {}", topic);
        }
    }

    private void gestiscoMessaggioLocomotiva(Message<?> message) {
        if (loggingAbilitatoLocomotive) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttLocomotiva, message.getPayload());
        }

        MessaggioLocomotiva messaggioLocomotivaRicevuto = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioLocomotivaRicevuto = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioLocomotiva.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoLocomotive) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioLocomotivaRicevuto);
            }

            Locomotiva locomotiva = convertiMessaggioLocomotivaToLocomotiva(messaggioLocomotivaRicevuto);

            locomotiveStorage.salvaLocomotiva(locomotiva);
        }
    }

    private void gestiscoMessaggioConvoglio(Message<?> message) {
        if (loggingAbilitatoConvogli) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttConvoglio, message.getPayload());
        }

        MessaggioConvoglio messaggioConvoglio = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioConvoglio = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioConvoglio.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoConvogli) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioConvoglio);
            }

            ConvoglioInfoBase convoglioInfoBase = convertiMessaggioConvoglioToConvoglioInfoBase(messaggioConvoglio);

            convogliStorage.salvaConvoglio(convoglioInfoBase);
        }
    }

    private void gestiscoMessaggioScambio(Message<?> message) {
        if (loggingAbilitatoScambi) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttScambio, message.getPayload());
        }

        MessaggioScambio messaggioScambio = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioScambio = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioScambio.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoScambi) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioScambio);
            }

            Scambio scambio = convertiMessaggioScambioToScambio(messaggioScambio);

            scambiStorage.salvaScambio(scambio);
        }
    }

    private void gestiscoMessaggioSensorePosizione(Message<?> message) {
        if (loggingAbilitatoSensoriPosizione) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttSensorePosizione, message.getPayload());
        }

        MessaggioSensorePosizione messaggioSensorePosizioneRicevuto = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioSensorePosizioneRicevuto = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioSensorePosizione.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoSensoriPosizione) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioSensorePosizioneRicevuto);
            }

            SensorePosizione sensorePosizione = convertiMessaggioSensorePosizioneToSensorePosizione(messaggioSensorePosizioneRicevuto);

            sensoriPosizioneStorage.salvaSensorePosizione(sensorePosizione);
        }
    }

    private void gestiscoMessaggioSezione(Message<?> message) {
        if (loggingAbilitatoSezioni) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttSezione, message.getPayload());
        }

        MessaggioSezione messaggioSezioneRicevuto = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioSezioneRicevuto = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioSezione.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoSezioni) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioSezioneRicevuto);
            }

            Sezione sezione = convertiMessaggioSezioneToSezione(messaggioSezioneRicevuto);

            sezioniStorage.salvaSezione(sezione);
        }
    }

    private void gestiscoMessaggioInfoAutopilot(Message<?> message) {
        if (loggingAbilitatoInfoAutopilot) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttInfoAutopilot, message.getPayload());
        }

        MessaggioInfoAutopilot messaggioInfoAutopilot = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioInfoAutopilot = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioInfoAutopilot.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoSezioni) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioInfoAutopilot);
            }

            InfoAutopilot infoAutopilot = convertiMessaggioInfoAutopilotToInfoAutopilot(messaggioInfoAutopilot);

            infoAutopilotStorage.salvaInfoAutopilot(infoAutopilot);
        }

    }

    private void gestiscoMessaggioLog(Message<?> message) {
        if (loggingAbilitatoLog) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttLog, message.getPayload());
        }

        MessaggioLog messaggioLogRicevuto = null;
        boolean deserializzazioneMessaggioMqttConSuccesso;
        try {
            messaggioLogRicevuto = deserializzazioneMessaggiMqttFactory.creaOggettoDaPayloadMqtt(message, MessaggioLog.class);
            deserializzazioneMessaggioMqttConSuccesso = true;
        } catch (JsonProcessingException e) {
            deserializzazioneMessaggioMqttConSuccesso = false;
        }

        if (deserializzazioneMessaggioMqttConSuccesso) {
            if (loggingAbilitatoLog) {
                logger.debug("Deserializzato messaggio MQTT: {}", messaggioLogRicevuto);
            }

            if (!messaggiLogBuffer.containsKey(messaggioLogRicevuto.getSource())) {
                // Se non ho ancora ricevuto messaggi da questo source (è il primo messaggio), lo aggiungo alla mappa
                messaggiLogBuffer.put(messaggioLogRicevuto.getSource(), new MessaggioLog());
            }

            // Ottengo il messaggioLog corrispondente al source
            MessaggioLog messaggioLog = messaggiLogBuffer.get(messaggioLogRicevuto.getSource());

            if (messaggioLog.getMessage().isBlank()) {
                // Non posso fare semplicemente messaggioLog = messaggioRicevuto perché altrimenti la mappa non si aggiornerebbe
                messaggioLog.setSource(messaggioLogRicevuto.getSource());
                messaggioLog.setLevel(messaggioLogRicevuto.getLevel());
                messaggioLog.setEnvironment(messaggioLogRicevuto.getEnvironment());
                messaggioLog.setFile(messaggioLogRicevuto.getFile());
                messaggioLog.setLine(messaggioLogRicevuto.getLine());
                messaggioLog.setFunction(messaggioLogRicevuto.getFunction());
                messaggioLog.setMessage(messaggioLogRicevuto.getMessage());
                messaggioLog.setLastChunkMessage(messaggioLogRicevuto.isLastChunkMessage());

                if (loggingAbilitatoLog) {
                    logger.info("Messaggio di {} inizializzato: \"{}\"", messaggioLog.getSource(), messaggioLog.getMessage());
                }
            } else {
                // Altrimenti aggiungo soltanto il messaggio a quello già esistente
                messaggioLog.setMessage(messaggioLog.getMessage() + messaggioLogRicevuto.getMessage());
                if (loggingAbilitatoLog) {
                    logger.info("Messaggio di {} aggiornato: \"{}\"", messaggioLog.getSource(), messaggioLog.getMessage());
                }
            }

            // Se si tratta dell'ultimo messaggio della sequenza, devo anche pubblicare il messaggio
            // al server Loki
            if (messaggioLogRicevuto.isLastChunkMessage()) {
                RestTemplate restTemplate = new RestTemplate();

                LokiPushRequest lokiPushRequest = createLokiPushRequest(messaggioLog);

                try {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);

                    HttpEntity<LokiPushRequest> requestEntity = new HttpEntity<>(lokiPushRequest, headers);

                    ObjectMapper objectMapper = new ObjectMapper();
                    String headersJson = objectMapper.writeValueAsString(headers);
                    String bodyJson = objectMapper.writeValueAsString(lokiPushRequest);

                    if (loggingAbilitatoLog) {
                        logger.info("Pubblicazione del seguente messaggio di {} sul server Loki: \"{}\"", messaggioLog.getSource(), messaggioLog.getMessage());
                        logger.debug("Headers: {}", headersJson);
                        logger.debug("Body: {}", bodyJson);
                    }

                    String url = lokiServerUrl + endPointLokiServerPush;
                    restTemplate.postForObject(url, requestEntity, LokiPushRequest.class);

                    // Resetto il messaggio per la prossima iterazione
                    messaggioLog.setSource("");
                    messaggioLog.setLevel("");
                    messaggioLog.setEnvironment("");
                    messaggioLog.setFile("");
                    messaggioLog.setLine("");
                    messaggioLog.setFunction("");
                    messaggioLog.setMessage("");
                    messaggioLog.setLastChunkMessage(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void gestiscoMessaggioArduinoReady(Message<?> message) {
        if (loggingAbilitatoArduinoReady) {
            logger.debug("Ricevuto messaggio MQTT su topic {}: {}", topicMqttLog, message.getPayload());
        }

        // Non eseguo subito le chiamate REST ad Arduino: do il tempo affinché Arduino entri nel loop().
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.schedule(() -> salvoBinariStazioniCompatibiliConvogliService.execute(), 2, TimeUnit.SECONDS);
    }

    private Locomotiva convertiMessaggioLocomotivaToLocomotiva(MessaggioLocomotiva messaggioLocomotiva) {
        Locomotiva locomotiva = new Locomotiva();
        locomotiva.setId(messaggioLocomotiva.getId());
        locomotiva.setNome(messaggioLocomotiva.getNome());
        locomotiva.setCodice(messaggioLocomotiva.getCodice());
        return locomotiva;
    }

    private ConvoglioInfoBase convertiMessaggioConvoglioToConvoglioInfoBase(MessaggioConvoglio messaggioConvoglio) {
        ConvoglioInfoBase convoglioInfoBase = new ConvoglioInfoBase();
        convoglioInfoBase.setId(messaggioConvoglio.getId());
        convoglioInfoBase.setNome(messaggioConvoglio.getNome());
        convoglioInfoBase.setIdLocomotiva1(messaggioConvoglio.getIdLocomotiva1());
        convoglioInfoBase.setIdLocomotiva2(messaggioConvoglio.getIdLocomotiva2());
        convoglioInfoBase.setVelocitaMassima(messaggioConvoglio.getVelocitaMassima());
        convoglioInfoBase.setVelocitaImpostata(messaggioConvoglio.getVelocitaImpostata());
        convoglioInfoBase.setVelocitaAttuale(messaggioConvoglio.getVelocitaAttuale());
        convoglioInfoBase.setLuciAccese(messaggioConvoglio.isLuciAccese());
        convoglioInfoBase.setPresenteSulTracciato(messaggioConvoglio.isPresenteSulTracciato());
        convoglioInfoBase.setAutopilot(messaggioConvoglio.getAutopilot());

        return convoglioInfoBase;
    }

    private Scambio convertiMessaggioScambioToScambio(MessaggioScambio messaggioScambio) {
        Scambio scambio = new Scambio();
        scambio.setId(messaggioScambio.getId());
        scambio.setPosizione(messaggioScambio.getPosizione());
        return scambio;
    }

    private SensorePosizione convertiMessaggioSensorePosizioneToSensorePosizione(MessaggioSensorePosizione messaggioSensorePosizione) {
        SensorePosizione sensorePosizione = new SensorePosizione();
        sensorePosizione.setId(messaggioSensorePosizione.getId());
        sensorePosizione.setStato(messaggioSensorePosizione.isStato());
        sensorePosizione.setIdConvoglioInAttesa(messaggioSensorePosizione.getIdConvoglioInAttesa());
        return sensorePosizione;
    }

    private Sezione convertiMessaggioSezioneToSezione(MessaggioSezione messaggioSezione) {
        Sezione sezione = new Sezione();
        sezione.setId(messaggioSezione.getId());
        sezione.setStato(messaggioSezione.getStato());
        return sezione;
    }

    private InfoAutopilot convertiMessaggioInfoAutopilotToInfoAutopilot(MessaggioInfoAutopilot messaggioInfoAutopilot) {
        InfoAutopilot infoAutopilot = new InfoAutopilot();
        infoAutopilot.setModalitaAutopilot(messaggioInfoAutopilot.getModalitaAutopilot());
        infoAutopilot.setStatoAutopilot(messaggioInfoAutopilot.getStatoAutopilot());
        return infoAutopilot;
    }
}
