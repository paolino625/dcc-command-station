package service;

import connector.arduino.IsSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector;
import connector.arduino.dto.IsSezioneStazioneCompatibileConConvoglioConnectorRequest;
import connector.arduino.dto.IsSezioneStazioneCompatibileConConvoglioConnectorResponse;
import model.Convoglio;
import model.Sezione;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import storage.ConvogliStorage;
import storage.SezioniStorage;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalvoBinariStazioniCompatibiliConvogliService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private ConvogliStorage convogliStorage;

    @Autowired
    private SezioniStorage sezioniStorage;

    @Autowired
    private IsSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector isSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector;

    public void execute() {
        logger.info("Salvo i binari di stazione compatibili per ogni convoglio");
        List<Convoglio> convogli = convogliStorage.getTuttiConvogli();

        // Per ogni convoglio, salvo quali sono le stazioni compatibili
        for (Convoglio convoglio : convogli) {
            salvoBinariStazioneCompatibiliConvoglio(convoglio.getConvoglioInfoBase().getId());
        }
    }

    private void salvoBinariStazioneCompatibiliConvoglio(int idConvoglio) {
        logger.info("Salvo i binari di stazione compatibili per il convoglio con id {}", idConvoglio);
        Convoglio convoglio = convogliStorage.getConvoglio(idConvoglio);

        // Mi salvo le sezioni compatibili con il convoglio mano a mano che le trovo
        List<Sezione> sezioniCompatibiliConConvoglioModalitaAutopilot = new ArrayList<>();
        List<Sezione> sezioniCompatibiliConConvoglioModalitaManuale = new ArrayList<>();

        List<Sezione> sezioniDiStazione = sezioniStorage.getSezioniDiStazione();

        for (Sezione sezione : sezioniDiStazione) {
            IsSezioneStazioneCompatibileConConvoglioConnectorRequest request = new IsSezioneStazioneCompatibileConConvoglioConnectorRequest();
            request.setIdConvoglio(idConvoglio);
            request.setIdSezione(sezione.getId());
            IsSezioneStazioneCompatibileConConvoglioConnectorResponse isSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnectorResponse = isSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector.modalitaAutopilot(request);
            IsSezioneStazioneCompatibileConConvoglioConnectorResponse isSezioneStazioneCompatibileConConvoglioModalitaManualeConnectorResponse = isSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnector.modalitaManuale(request);
            if (isSezioneStazioneCompatibileConConvoglioModalitaAutopilotConnectorResponse.isResult()) {
                sezioniCompatibiliConConvoglioModalitaAutopilot.add(sezione);
            }
            if (isSezioneStazioneCompatibileConConvoglioModalitaManualeConnectorResponse.isResult()) {
                sezioniCompatibiliConConvoglioModalitaManuale.add(sezione);
            }
        }

        // Una volta trovate tutte le sezioni compatibili con il convoglio, le salvo nel convoglio
        convoglio.setSezioniStazioneCompatibiliModalitaAutopilot(sezioniCompatibiliConConvoglioModalitaAutopilot);
        convoglio.setSezioniStazioneCompatibiliModalitaManuale(sezioniCompatibiliConConvoglioModalitaManuale);

    }
}
