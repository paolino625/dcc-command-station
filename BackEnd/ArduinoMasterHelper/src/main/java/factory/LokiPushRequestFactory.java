package factory;

import connector.loki.push.LokiPushRequest;
import connector.loki.push.LokiStream;
import connector.loki.push.LokiStreams;
import model.mqtt.MessaggioLog;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class LokiPushRequestFactory {
    public static LokiPushRequest createLokiPushRequest(MessaggioLog messaggioLog) {
        LokiStream lokiStream = createLokiStream(messaggioLog);

        LokiStreams lokiStreams = new LokiStreams();
        lokiStreams.setStream(lokiStream);

        List<List<String>> arrayValues = new ArrayList<>();

        List<String> values = new ArrayList<>();
        long nanoSecondiDaEpoch = Instant.now().toEpochMilli() * 1_000_000;
        String nanoSecondiDaEpochString = String.valueOf(nanoSecondiDaEpoch);
        values.add(nanoSecondiDaEpochString);
        values.add(messaggioLog.getMessage());
        arrayValues.add(values);

        lokiStreams.setValues(arrayValues);

        LokiPushRequest lokiPushRequest = new LokiPushRequest();
        lokiPushRequest.setStreams(List.of(lokiStreams));
        return lokiPushRequest;
    }

    private static LokiStream createLokiStream(MessaggioLog messaggioLog) {
        LokiStream lokiStream = new LokiStream();
        lokiStream.setSource(messaggioLog.getSource());
        lokiStream.setLevel(messaggioLog.getLevel());
        lokiStream.setEnvironment(messaggioLog.getEnvironment());
        lokiStream.setFile(messaggioLog.getFile());
        lokiStream.setLine(messaggioLog.getLine());
        lokiStream.setFunction(messaggioLog.getFunction());
        return lokiStream;
    }
}
