package factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class DeserializzazioneMessaggiMqttFactory {
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public <T> T creaOggettoDaPayloadMqtt(Message<?> message, Class<T> targetClass) throws JsonProcessingException {
        T messaggioDeserializzato = null;
        String jsonPayload = null;
        try {
            jsonPayload = message.getPayload().toString();
            logger.debug("Payload ricevuto: " + jsonPayload);
            messaggioDeserializzato = objectMapper.readValue(jsonPayload, targetClass);
        } catch (Exception e) {
            logger.error("Errore nella deserializzazione del messaggio: {}", jsonPayload);
            e.printStackTrace();
            throw e;

        }
        return messaggioDeserializzato;
    }
}