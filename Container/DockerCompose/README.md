# Variabili d'ambiente

Le variabili d'ambiente sono salvate nei seguenti file a seconda dell'ambiente:
- svil.env: variabili d'ambiente di sviluppo
- prod.env: variabili d'ambiente di produzione

Di seguito un riepilogo delle variabili d'ambiente

# Azioni Docker Compose

## Mac

### Pull

- docker compose --env-file env/svil-mac.env pull
- docker compose --env-file env/prod-mac.env pull

### Esecuzione

- docker-compose --env-file env/svil-mac.env -f docker-compose.yaml up -d
- docker-compose --env-file env/prod-mac.env -f docker-compose.yaml up -d

Con -d specifico modalità detached.
Con --env-file specifico il file delle variabili d'ambiente da usare

### Fermare

- docker-compose down

## Portainer

Creare un nuovo stack con "Repository" come build method con le seguenti impostazioni:

- Nome:
    Sviluppo: dcc-command-station-svil
    Produzione: dcc-command-station-prod

### WebEditor

- Copia il contenuto di docker-compose.

- Carica file di env di Sviluppo o di Produzione del Synology.

### Repository

- Repository URL: https://gitlab.com/paolino625/dcc-command-station

- Repository reference:
    Sviluppo: refs/heads/develop
    Produzione: refs/heads/master

- Compose path: Container/DockerCompose/docker-compose.yaml

- Carica file di env di Sviluppo o di Produzione del Synology.