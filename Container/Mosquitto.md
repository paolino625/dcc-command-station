# Mosquitto

## Esecuzione

### Terminale

#### Subscriber

Now you can test the installation and ensure the server is running successfully. Open a new command window and start a
listener.
mosquitto_sub -t DccCommandStation/Log

#### Publisher

In another window, send a message to the listener.
mosquitto_pub -t DccCommandStation/Log -m "Hello World"

### MQTT Explorer

http://mqtt-explorer.com/img/screen3.png

### MQTTX

https://mqttx.app

## Setup Autenticazione WebSocket

Nel file di configurazione mosquitto.conf abbiamo configurato anche una connessione webSocket con autenticazione, per la webApp di Front-End.

Per poter creare username e password per l'autenticazione:

- Posizionarsi all'interno della cartella mosquitto
- mosquitto_passwd -c config/password Paolino625
Questo comando chiederà di inserire una password per l'utente Paolino625.