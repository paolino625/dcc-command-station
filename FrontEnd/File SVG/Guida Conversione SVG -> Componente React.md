°Guida Conversione SVG -> Componente React

- Esportare SVG da Affinity Designer con impostazione "SVG (Digitale-Massima Qualità)"
Non usare "Per esportazione" altrimenti l'SVG non viene renderizzato bene quando viene aperto con altre app/browser che non siano Affinity Designer.
- Togliere prima riga xml, lasciare solo <svg></svg>
- Convertire svg in componente React usando questo sito: https://svg2jsx.com
- Copiare il componente dal sito nel file corrispondente .tsx, cambiare poi il nome nel livello del tracciato corrispondente (1° Piano, 2° Piano ecc.) ove necessario.