// load-env.js
import dotenv from 'dotenv';
import {execSync} from 'child_process';

const envFile = process.argv[2];
dotenv.config({path: envFile});

const command = process.argv.slice(3).join(' ');
execSync(command, {stdio: 'inherit'});