# Creazione progetto React

Creare un nuovo progetto Vite su IntelliJ Ultimate, scegliere template React e settare TypeScript.

# Esecuzione progetto

Installazione dipendenze:

- npm install

## Sviluppo

- npm run dev

## Produzione

- npm run prod

# Installazione librerie

## MQTT

npm install mqtt --save

# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md)
  uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast
  Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
    // other rules...
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
        project: ['./tsconfig.json', './tsconfig.node.json'],
        tsconfigRootDir: __dirname,
    },
}
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked`
  or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and
  add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list

# Deploy

Eseguire questo comando per poter creare l'immagine Docker:

- docker build -t web-app:latest .

Eseguire immagine docker:

- docker run -p 5173:5173 --name WebApp web-app:latest

Per pushare la build:

- docker login registry.gitlab.com
- docker tag web-app registry.gitlab.com/paolino625/dcc-command-station/web-app:latest
- docker push registry.gitlab.com/paolino625/dcc-command-station/web-app:latest