export enum StileBottone {
    Normale = 'info',
    Attivo = 'success',
    // Quando il bottone è non attivo ma non blocca nessuna operazione (tipo visualizzazione sensori)
    NonAttivoOk = 'secondary',
    // Quando il bottone deve essere cliccato per poter procedere con il flusso (registrazione posizione convoglio)
    NonAttivoDaAttivare = 'error',
    Warning = 'warning',
    Errore = 'error',

    /*
    Primary = 'primary',
    Secondary = 'secondary',
    Success = 'success',
    Danger = 'danger',
    Warning = 'warning',
    Info = 'info',
    Light = 'light',
    Dark = 'dark'
     */
}