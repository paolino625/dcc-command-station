import React, {useState} from 'react';
import logger from "../utility/Logger.ts";
import {useNavigate} from "react-router-dom";
import {utenti} from "../model/Utenti.ts";
import {Utente} from "../model/Utente.ts";

interface LoginProps {
    setUtenteAutenticato: (utente: Utente | null) => void;
}

const Login: React.FC<LoginProps> = ({setUtenteAutenticato}) => {
    const [usernameInserito, setUsernameInserito] = useState('');
    const [passwordInserito, setPasswordInserito] = useState('');
    const [error, setError] = useState('');
    const navigate = useNavigate();

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();

        const user = utenti.find(user => user.username === usernameInserito && user.password === passwordInserito);

        if (user) {
            logger.info("Utente loggato con successo");
            setUtenteAutenticato(user);
            // Salvo nel local storage l'utente loggato, in modo tale da poter mantenere la sessione
            localStorage.setItem('utenteLoggato', user.username);
            setError('Utente loggato con successo');
            navigate('/home');
        } else {
            setError('Log In fallito!');
        }
    };

    return (
        <div className="login">
            <h2 style={{marginBottom: '20px'}}>Log In</h2>
            {error && <p>{error}</p>}
            <form onSubmit={handleSubmit} style={{display: 'flex', flexDirection: 'column', gap: '0px'}}>
                <input
                    type="text"
                    value={usernameInserito}
                    onChange={(e) => setUsernameInserito(e.target.value)}
                    placeholder="Username"
                    style={{marginBottom: '20px'}}
                />
                <input
                    type="password"
                    value={passwordInserito}
                    onChange={(e) => setPasswordInserito(e.target.value)}
                    placeholder="Password"
                    style={{marginBottom: '20px'}}
                />
                <button type="submit">Accedi</button>
            </form>
        </div>
    );
};

export default Login;