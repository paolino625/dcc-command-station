import React from 'react';
import {Navigate} from 'react-router-dom';
import {Utente} from "../model/Utente.ts";

interface PrivateRouteProps {
    children: React.ReactNode;
    utenteAutenticato: Utente | null;
    path: string;
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({children, utenteAutenticato}) => {
    // Se l'utente è autenticato, ritorno il componente, altrimenti reindirizzo alla pagina di login.
    return utenteAutenticato ? children : <Navigate to="/login"/>;
};

export default PrivateRoute;