// *** IMPORT *** //

import './App.css'
import React, {useEffect, useState} from "react";
import logger from "../utility/Logger.ts";
import {BrowserRouter as Router, Navigate, Route, Routes} from 'react-router-dom';
import Login from '../logIn/Login.tsx';
import PrivateRoute from "../logIn/PrivateRoute.tsx";
import Homepage from "../Homepage.tsx";
import {
    readyArduinoMasterHelper
} from '../comunicazione/restApi/arduinoMasterHelper/arduinoMasterHelper/get/ReadyArduinoMasterHelper.ts';
import {
    healthCheckArduinoMaster
} from '../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/get/HealthCheckArduinoMaster.ts';
import {
    healthCheckArduinoMasterHelper
} from "../comunicazione/restApi/arduinoMasterHelper/arduinoMasterHelper/get/HealthCheckArduinoMasterHelper.ts";
import {utenti} from "../model/Utenti.ts";
import {Utente} from "../model/Utente.ts";

const App: React.FC = () => { // Definiamo un componente funzionale React chiamato App

    // Faccio un check dello stato dei componenti esterni ogni 5 secondi
    const [utenteAutenticato, setUtenteAutenticato] = useState<Utente | null>(null);

    const [arduinoMasterHealthCheckStatus, setArduinoMasterHealthCheckStatus] = useState<boolean>(false);
    const [arduinoMasterHelperHealthCheckStatus, setArduinoMasterHelperHealthCheckStatus] = useState<boolean>(false);
    const [arduinoMasterHelperReadyStatus, setArduinoMasterHelperReadyStatus] = useState<boolean>(false);
    const [checkInizialeCompletato, setCheckInizialeCompletato] = useState<boolean>(false);

    // Lo eseguo solo una volta
    useEffect(() => {
        const utenteLoggatoSessione = localStorage.getItem('utenteLoggato');
        const utente = utenti.find(user => user.username === utenteLoggatoSessione);
        if (utente) {
            setUtenteAutenticato(utente);
            logger.info(`L'utente ${utente.username} è stato autenticato.`);
        } else {
            logger.info("Nessun utente autenticato.");
            setUtenteAutenticato(null);
        }
    }, []);

    useEffect(() => {
        // Controllo se ArduinoMaster è vivo e se ArduinoMasterHelper è pronto
        const checkStatoComponentiEsterni = async () => {
            try {
                // Faccio partire le due chiamate in parallelo, dato che una va ad ArduinoMaster (sempre passando tramite ArduinoMasterHelper) e l'altra va a ArduinoMasterHelper.
                let [healthCheckResponse, healthCheckHelperResponse, readyCheckResponse] = await Promise.all([
                    healthCheckArduinoMaster(),
                    healthCheckArduinoMasterHelper(),
                    readyArduinoMasterHelper()
                ]);

                // Se l'health check di Arduino Master non è andato a buon fine, vale la pena riprovare perché Arduino Master potrebbe essere stato impegnato in operazioni che lo hanno costretto a interrompere temporaneamente l'esecuzione del loop.
                if (!healthCheckResponse) {
                    logger.warn("Health check di Arduino Master non andato a buon fine. Riprovo tra 5 secondi.");
                    await new Promise(resolve => setTimeout(resolve, 5000));
                    healthCheckResponse = await healthCheckArduinoMaster();
                }

                // Imposto le variabili a seconda delle risposte
                setArduinoMasterHealthCheckStatus(healthCheckResponse);
                setArduinoMasterHelperHealthCheckStatus(healthCheckHelperResponse);
                setArduinoMasterHelperReadyStatus(readyCheckResponse);
            } catch (error) {
                setArduinoMasterHealthCheckStatus(false);
                setArduinoMasterHelperHealthCheckStatus(false);
                setArduinoMasterHelperReadyStatus(false);
            } finally {
                setCheckInizialeCompletato(true);
            }
        };

        // Eseguo la funzione subito la prima volta
        checkStatoComponentiEsterni();

        // Dopodiché eseguo la funzione ogni 5 secondi
        const intervalloCheckComponentiEsterni = setInterval(checkStatoComponentiEsterni, 1000);

        // Pulizia dell'intervallo al momento dello smontaggio del componente
        return () => clearInterval(intervalloCheckComponentiEsterni);
    }, []);

    if (!checkInizialeCompletato) {
        // Se non ho ancora effettuato il check dei componenti esterni, mostro una pagina vuota.
        // Altrimenti, si vedrebbe per un attimo l'avviso che ArduinoMaster non è attivo, finché non viene ricevuta la risposta da Arduino stesso.
        return <h3></h3>;
    }
    // Restituiamo il JSX che definisce l'interfaccia utente del componente
    return (
        <Router>
            <Routes>
                {/* Se ArduinoMaster o ArduinoMasterHelper non sono pronti, mostro un messaggio di errore */}
                {(!arduinoMasterHealthCheckStatus || !arduinoMasterHelperHealthCheckStatus || !arduinoMasterHelperReadyStatus) ? (
                    <>
                        {!arduinoMasterHealthCheckStatus &&
                            <Route path="*" element={<h3 style={{marginTop: '30px'}}>Errore: Arduino Master non è
                                attivo</h3>}/>} {!arduinoMasterHelperHealthCheckStatus &&
                        <Route path="*" element={<h3 style={{marginTop: '30px'}}>Errore: Arduino Master Helper non è
                            attivo</h3>}/>}
                        {!arduinoMasterHelperReadyStatus &&
                            <Route path="*" element={<h3 style={{marginTop: '30px'}}>Errore: Arduino Master Helper non è
                                pronto</h3>}/>}
                    </>
                ) : (
                    <>
                        <Route path="/" element={
                            utenteAutenticato ? <Navigate to="/home"/> : <Navigate to="/login"/>
                        }/>
                        <Route path="/login"
                               element={utenteAutenticato ? <Navigate to="/home"/> :
                                   <Login setUtenteAutenticato={setUtenteAutenticato}/>}/>
                        <Route path="/home/*" element={
                            <PrivateRoute utenteAutenticato={utenteAutenticato} path="/home">
                                <Homepage utenteAutenticato={utenteAutenticato}
                                          setUtenteAutenticato={setUtenteAutenticato}
                                />
                            </PrivateRoute>
                        }/>
                        <Route path="/healthCheck" element={<h3>Health Check OK</h3>}/>
                    </>
                )}
            </Routes>
        </Router>
    );
};

export default App;