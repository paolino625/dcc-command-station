import {IdSezioneTipo} from "../IdSezioneTipo.ts";
import {ValiditaPosizioneConvoglio} from "../../enumerazioni/ValiditaPosizioneConvoglio.ts";

export interface PosizioneConvoglio {
    validita: ValiditaPosizioneConvoglio; // Replace with the actual type of validita
    idSezioneCorrenteOccupata: IdSezioneTipo;
    direzionePrimaLocomotivaDestra: boolean;
}