import {PosizioneConvoglio} from "./PosizioneConvoglio.ts";
import {StatoAutopilotModalitaStazioneSuccessiva} from "../../enumerazioni/StatoAutopilotModalitaStazioneSuccessiva.ts";
import {StatoAutopilotModalitaApproccioCasuale} from "../../enumerazioni/StatoAutopilotModalitaApproccioCasuale.ts";
import {StatoInizializzazioneAutopilotConvoglio} from "../../enumerazioni/StatoInizializzazioneAutopilotConvoglio.ts";

export interface Autopilot {
    posizioneConvoglio: PosizioneConvoglio;
    statoInizializzazioneAutopilotConvoglio: StatoInizializzazioneAutopilotConvoglio;
    statoAutopilotModalitaStazioneSuccessiva: StatoAutopilotModalitaStazioneSuccessiva;
    statoAutopilotModalitaApproccioCasuale: StatoAutopilotModalitaApproccioCasuale;
}

export interface Convoglio {
    id: number;
    nome: String;
    idLocomotiva1: number;
    idLocomotiva2: number;
    velocitaMassima: number;
    velocitaImpostata: number;
    velocitaAttuale: number;
    luciAccese: boolean;
    isPresenteSulTracciato: boolean;
    autopilot: Autopilot;
}