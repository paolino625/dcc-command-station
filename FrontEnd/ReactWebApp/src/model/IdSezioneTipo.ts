export interface IdSezioneTipo {
    tipologia: number;
    id: number;
    specifica: number;
}

export const isValidIdSezioneTipo = (idSezioneTipo: IdSezioneTipo): boolean => {
    return idSezioneTipo.tipologia !== 0 || idSezioneTipo.id !== 0 || idSezioneTipo.specifica !== 0;
};

export const areIdSezioneTipoEqual = (a: IdSezioneTipo | null, b: IdSezioneTipo | null): boolean => {
    if (a === null || b === null) {
        return false;
    }
    return a.tipologia === b.tipologia && a.id === b.id && a.specifica === b.specifica;
};

export const isSezioneStazione = (idSezioneTipo: IdSezioneTipo | null): boolean => {
    if (idSezioneTipo !== null) {
        return idSezioneTipo.tipologia === 1;
    }
    return false;
}

export const isSezioneScambio = (idSezioneTipo: IdSezioneTipo | null): boolean => {
    if (idSezioneTipo !== null) {
        return idSezioneTipo.tipologia === 3;
    }
    return false;
}

export const isSezioneScambioSinistra = (idSezioneTipo: IdSezioneTipo | null): boolean => {
    if (idSezioneTipo !== null) {
        return idSezioneTipo.specifica === 1;
    }
    return false;
}

export const convertoSezioneInStringa = (idSezioneTipo: IdSezioneTipo | null): string => {
    if (idSezioneTipo !== null) {
        return `${idSezioneTipo.tipologia}.${idSezioneTipo.id}.${idSezioneTipo.specifica}`;
    }
    return "";
}