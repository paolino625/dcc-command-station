import {IdSezioneTipo} from "./IdSezioneTipo.ts";
import {StatoSezione} from "./StatoSezione.ts";

export interface Sezione {
    id: IdSezioneTipo;
    stato: StatoSezione;
}