export interface SensorePosizione {
    id: number;
    stato: boolean;
    idConvoglioInAttesa: number;
}