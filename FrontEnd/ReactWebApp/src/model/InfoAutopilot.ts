import {StatoAutopilotBackEnd} from "../enumerazioni/StatoAutopilotBackEnd.ts";
import {ModalitaAutopilot} from "../enumerazioni/ModalitaAutopilot.ts";

export interface InfoAutopilot {
    statoAutopilot: StatoAutopilotBackEnd;
    modalitaAutopilot: ModalitaAutopilot;
}