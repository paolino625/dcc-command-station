export class Utente {
    username: string;
    password: string;
    velocitaMassima: number;
    numeroMassimoConvogliModalitaManuale: number;

    constructor(username: string, password: string, velocitaMassima: number, numeroMassimoConvogliModalitaManuale: number) {
        this.username = username;
        this.password = password;
        this.velocitaMassima = velocitaMassima;
        this.numeroMassimoConvogliModalitaManuale = numeroMassimoConvogliModalitaManuale;
    }
}

