import {PosizioneScambio} from "./PosizioneScambio.ts";

export interface Scambio {
    id: number;
    posizione: PosizioneScambio;
}

