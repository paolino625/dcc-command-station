import {Utente} from "./Utente.ts";

/*
Posso decidere la velocità massima di un convoglio per ogni utente.
Anche nel caso dell'amministratore è utile mantenere una velocità massima e non lasciarla libera: alcune locomotive possono andare molto veloci il che rappresenta un rischio.
Inoltre, in questo modo si ha una normalizzazione dei valori degli slider; altrimenti, potremmo avere uno slider fino a 250 e un altro fino a 120.
*/
const VELOCITA_MASSIMA_CONVOGLIO_ALTA = 150;
const VELOCITA_MASSIMA_CONVOGLIO_MEDIA = 100;
const VELOCITA_MASSIMA_CONVOGLIO_BASSA = 50;

export const utenti: Utente[] = [
    new Utente('Paolo', 'MoQ.kdpa3yH7', VELOCITA_MASSIMA_CONVOGLIO_ALTA, Number.MAX_SAFE_INTEGER),
    new Utente('Aldo', '123', VELOCITA_MASSIMA_CONVOGLIO_MEDIA, 2),
    new Utente('Pasquale', '123', VELOCITA_MASSIMA_CONVOGLIO_MEDIA, 2),
    new Utente('Fiorella', '123', VELOCITA_MASSIMA_CONVOGLIO_BASSA, 2),
    new Utente('Riccardo', '123', VELOCITA_MASSIMA_CONVOGLIO_BASSA, 2),
];