import React, {useEffect, useRef, useState} from "react";
import {Convoglio} from "./model/convoglio/Convoglio.ts";
import logger from "./utility/Logger.ts";
import {Locomotiva} from "./model/Locomotiva.ts";
import {
    fetchLocomotiveConvoglioScambiSensoriPosizioneSezioni
} from "./comunicazione/restApi/arduinoMasterHelper/arduinoProxy/get/FetchLocomotiveConvoglioScambiSensoriPosizioneSezioni.ts";
import {
    fetchInfoAutopilotRestCall
} from "./comunicazione/restApi/arduinoMasterHelper/arduinoProxy/get/FetchStatoAutopilot.ts";
import {StatoAutopilotBackEnd} from "./enumerazioni/StatoAutopilotBackEnd.ts";
import {StatoAutopilotFrontEnd} from "./enumerazioni/StatoAutopilotFrontEnd.ts";
import {resetColoriBinariStazioni,} from "./componenti/tracciato/Stazioni.ts";
import {IdSezioneTipo, isValidIdSezioneTipo} from "./model/IdSezioneTipo.ts";
import {
    setPosizioneConvoglioRestCall
} from "./comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/preAvvio/SetPosizioneConvoglioRestCall.ts";
import {ModalitaAutopilot} from "./enumerazioni/ModalitaAutopilot.ts";
import {connettiBrokerMqtt} from "./comunicazione/mqtt/MqttArduino.ts";
import {MqttClient} from "mqtt";
import {Sezione} from "./model/Sezione.ts";
import {aggiornoCliccabilitaEListenerScambi, aggiornoColoriScambi} from "./componenti/tracciato/Scambi.ts";
import {Scambio} from "./model/scambio/Scambio.ts";
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {estraggoIdSezioneTipoDaStringa} from "./utility/EstrazioneIdSezioneDaElementiSvg.ts";
import BottoneAutopilot from "./componenti/bottoni/BottoneAutopilot.tsx";
import {
    handleFormSceltaModalitaAutopilotChiusura,
    handleFormSceltaModalitaAutopilotSalvataggio
} from "./componenti/autopilot/FormSceltaModalitaAutopilot.ts";
import TracciatoPrimoPiano from "./componenti/tracciato/svg/TracciatoPrimoPiano.tsx";
import BottoneVisualizzazioneSensori from "./componenti/bottoni/BottoneVisualizzazioneSensori.tsx";
import {
    stopEmergenzaConvogli
} from "./comunicazione/restApi/arduinoMasterHelper/arduinoProxy/put/StopEmergenzaConvogli.ts";
import {aggiornoCliccabilitaSensori, aggiornoStatoSensoriPosizione} from "./componenti/tracciato/SensoriPosizione.ts";
import {ValiditaPosizioneConvoglio} from "./enumerazioni/ValiditaPosizioneConvoglio.ts";
import QuadroComandiConvoglio from "./componenti/quadroComandiConvoglio/QuadroComandiConvoglio.tsx";
import {Utente} from "./model/Utente.ts";
import {
    aggiornoCliccabilitaSezioni,
    mostroSezioniOccupateDaConvoglio,
    mostroStatoSezioni,
    resetColoriSezioni
} from "./componenti/tracciato/Sezioni.ts";
import {BottoneVisualizzazioneSezioni} from "./componenti/bottoni/BottoneVisualizzazioneSezioni.tsx";
import BottoneDebuggingSensori from "./componenti/bottoni/BottoneDebuggingSensori.tsx";
import BottoneDebuggingSezioni from "./componenti/bottoni/BottoneDebuggingSezioni.tsx";
import BottoneStopEmergenza from "./componenti/bottoni/BottoneStopEmergenza.tsx";
import BottoneLogout from "./componenti/bottoni/BottoneUtenteLoggato.tsx";
import {StileBottone} from "./stile/StileBottone.ts";
import {isMobile} from 'react-device-detect';

const NUMERO_MASSIMO_QUADRI_COMANDO_CONVOGLIO_PER_RIGA = 3;

interface HomepageProps {
    utenteAutenticato: Utente | null;
    setUtenteAutenticato: React.Dispatch<React.SetStateAction<Utente | null>>;
}

const Homepage: React.FC<HomepageProps> = ({utenteAutenticato, setUtenteAutenticato}) => {
    // *** OGGETTI *** //

    // ** LOCOMOTIVE ** //

    const [locomotive, setLocomotive] = useState<Locomotiva[]>([]);

    useEffect(() => {
        logger.debug("Locomotive aggiornate: ", locomotive);
    }, [locomotive]);

    // ** CONVOGLI ** //

    const [convogli, setConvogli] = useState<Convoglio[]>([]);

    useEffect(() => {
        logger.debug("Convogli aggiornati: ", convogli);
    }, [convogli]);

    // Creare un ref per mantenere il valore corrente di convogli
    const convogliRef = useRef(convogli);

    // Aggiornare il valore del ref ogni volta che convogli cambia
    useEffect(() => {
        convogliRef.current = convogli;
    }, [convogli]);

    // ** SCAMBI ** //

    const [scambi, setScambi] = useState<Scambio[]>([]);

    useEffect(() => {
        logger.debug("Scambi aggiornati: ", scambi);
        aggiornoColoriScambi(scambi);
    }, [scambi]);

    // ** SENSORI POSIZIONE ** //

    // @ts-ignore // TODO Da togliere dopo averlo usato
    const [sensoriPosizione, setSensoriPosizione] = useState<SensorePosizione[]>([]);

    useEffect(() => {
        logger.debug("Sensori posizione aggiornati: ", sensoriPosizione);
    }, [sensoriPosizione]);

    // ** SEZIONI ** //

    const [sezioni, setSezioni] = useState<Sezione[]>([]);

    useEffect(() => {
        logger.debug("Sezioni aggiornate: ", sezioni);
    }, [sezioni]);

    // *** MQTT *** //

    const [mqttClient, setMqttClient] = useState<MqttClient | null>(null);

    // @ts-ignore
    const [mqttConnectStatus, setMqttConnectStatus] = useState('Disconnesso');

    // Effettuiamo la connessione al broker MQTT. Questo useEffect viene eseguito solo una volta all'avvio dell'applicazione.
    useEffect(() => {
        if (import.meta.env.VITE_MQTT_ATTIVO === 'true') {
            connettiBrokerMqtt(setMqttConnectStatus, setMqttClient, setLocomotive, setConvogli, setScambi, setSensoriPosizione, setSezioni, setStatoAutopilotBackEnd, setModalitaAutopilotBackEnd, richiestaRestProgrammataRef);
        }
    }, []);

    useEffect(() => {
        // Questo useEffect viene eseguito solo quando la variabile mqttClient cambia, dentro andiamo a controllare se la connessione MQTT è attiva
        return () => {
            if (mqttClient) {
                mqttClient.end();
            }
        };
    }, [mqttClient]);

    // *** CHIAMATE GET ARDUINO *** //

    // Effettuo le chiamate GET ad Arduino
    useEffect(() => {
        // Uso una funzione asincrona per poter utilizzare await e non effettuare più chiamate GET in parallelo.
        const fetchData = async () => {
            await fetchLocomotiveConvoglioScambiSensoriPosizioneSezioni(setLocomotive, setConvogli, setScambi, setSezioni, setSensoriPosizione, setError, setIsLoading);
            await fetchInfoAutopilotRestCall(setStatoAutopilotBackEnd, setModalitaAutopilotBackEnd);
        };

        fetchData();
    }, []);

    // *** AUTOPILOT *** //

    // ** STATO AUTOPILOT ** //

    const [statoAutopilotBackEnd, setStatoAutopilotBackEnd] = useState<StatoAutopilotBackEnd | null>(null);

    // Inizializzo a NON_ATTIVO per non avere problemi con null ma effettuo subito fetch per ottenere lo stato attuale da Arduino
    const [statoAutopilotFrontEnd, setStatoAutopilotFrontEnd] = useState<StatoAutopilotFrontEnd>(StatoAutopilotFrontEnd.NON_ATTIVO);

    // ** MODALITA' AUTOPILOT ** //

    // @ts-ignore
    const [modalitaAutopilotBackEnd, setModalitaAutopilotBackEnd] = useState<ModalitaAutopilot | null>(null);

    const [modalitaAutopilotFrontEnd, setModalitaAutopilotFrontEnd] = useState<ModalitaAutopilot>(ModalitaAutopilot.STAZIONE_SUCCESSIVA);

    // *** VARIABILI SCELTE DALL'UTENTE *** //

    // ** DEBUGGING ATTIVO ** //

    const [debuggingSensori, setDebuggingSensori] = useState(false);
    const [debuggingSezioni, setDebuggingSezioni] = useState(false);

    // ** AUTOPILOT ATTIVATO AUTOPILOT MODALITA' APPROCCIO CASUALE ** //

    const [autopilotAttivatoAutopilotModalitaApproccioCasuale, setAutopilotAttivatoAutopilotModalitaApproccioCasuale] = useState<{
        [idConvoglio: number]: boolean
    }>({});

    useEffect(() => {
        const initialState: { [idConvoglio: number]: boolean } = {};
        convogli.forEach(convoglio => {
            initialState[convoglio.id] = false;
        });
        setAutopilotAttivatoAutopilotModalitaApproccioCasuale(initialState);
    }, [convogli]);

    // ** NUMERO QUADRI COMANDI CONVOGLI ** //

    const [numeroQuadriComandiConvogli, setNumeroQuadriComandiConvogli] = useState(0);

    const numeroEsecuzioneAggiornamentoNumeroQuadriComandiConvogli = useRef<number>(0);

    // Imposto il num quadri solo dopo che i convogli sono stati caricati
    useEffect(() => {
        if (numeroEsecuzioneAggiornamentoNumeroQuadriComandiConvogli.current <= 2) {
            if (convogli.length > 0) {

                // Di default mostro al massimo n convogli, in modo tale che stiano in una sola riga e ci sia spazio anche per il tracciato.
                setNumeroQuadriComandiConvogli(Math.min(convogli.length, NUMERO_MASSIMO_QUADRI_COMANDO_CONVOGLIO_PER_RIGA, utenteAutenticato ? utenteAutenticato.numeroMassimoConvogliModalitaManuale : 0));
            }
        }
        numeroEsecuzioneAggiornamentoNumeroQuadriComandiConvogli.current += 1;
    }, [convogli]);

    // ** POSIZIONE DEL CONVOGLIO IN REGISTRAZIONE ** //

    // Memorizzo la posizione del convoglio in registrazione.
    const [posizioneConvoglioInRegistrazione, setPosizioneConvoglioInRegistrazione] = useState<{
        idConvoglio: number,
        idStazione: IdSezioneTipo,
        direzionePrimaLocomotivaDestra: boolean | undefined
    } | null>(null);

    useEffect(() => {
        // Dobbiamo controllare non soltanto che posizioneConvoglio sia definito, ma anche che idStazione sia definito, perché quando l'utente inserisce la direzione della prima locomotiva idStazione è impostato a ''.
        if (posizioneConvoglioInRegistrazione && isValidIdSezioneTipo(posizioneConvoglioInRegistrazione.idStazione) && posizioneConvoglioInRegistrazione.direzionePrimaLocomotivaDestra !== undefined) {
            logger.info(`La posizione del convoglio n° ${posizioneConvoglioInRegistrazione.idConvoglio} è completa.`);
            // Se entrambi i valori sono definiti, posso inviare la richiesta REST

            logger.info("Invio richiesta REST per registrare la posizione del convoglio");

            setPosizioneConvoglioRestCall(posizioneConvoglioInRegistrazione.idConvoglio, posizioneConvoglioInRegistrazione.idStazione, posizioneConvoglioInRegistrazione.direzionePrimaLocomotivaDestra);

            // Dopo aver inviato la richiesta REST, posso resettare la posizione del convoglio
            setPosizioneConvoglioInRegistrazione(null);
        }
    }, [posizioneConvoglioInRegistrazione]);

    // ** OTTIMIZZAZIONE ASSEGNAZIONE CONVOGLI AI BINARI ** //

    // Opzione inserita dall'utente per autopilot quando in modalità approccio casuale.
    const [ottimizzazioneAssegnazioneConvogliAiBinariAbilitata, setOttimizzazioneAssegnazioneConvogliAiBinariAbilitata] = useState(true);

    // ** MODALITA AUTOPILOT SELEZIONATA ** //

    const [modalitaAutopilotSelezionata, setModalitaAutopilotSelezionata] = useState<string>('');

    // ** VISUALIZAZZIONE SEZIONI OCCUPATE DA CONVOGLIO ** //

    const [mostraSezioniOccupateDaConvoglio, setMostraSezioniOccupateDaConvoglio] = useState<Map<number, boolean>>(new Map());

    // Inizializzo a false la mappa
    const [numeroEsecuzioniInizializzazioneMostraSezioniOccupateDaConvoglio, setNumeroEsecuzioniInizializzazioneMostraSezioniOccupateDaConvoglio] = useState(0);
    const inizializzaMostraSezioniOccupateDaConvoglio = () => {
        const newMap = new Map<number, boolean>();
        convogli.forEach(convoglio => {
            newMap.set(convoglio.id, false);
        });
        setMostraSezioniOccupateDaConvoglio(newMap);
    };

    useEffect(() => {
        if (numeroEsecuzioniInizializzazioneMostraSezioniOccupateDaConvoglio < 2) {
            inizializzaMostraSezioniOccupateDaConvoglio();
            setNumeroEsecuzioniInizializzazioneMostraSezioniOccupateDaConvoglio(prev => prev + 1);
        }
    }, [convogli]);

    // ** VISUALIZAZZIONE SEZIONE OCCUPATA DA CONVOGLIO ** //

    const [mostraSezioneOccupataDaConvoglio, setMostraSezioneOccupataDaConvoglio] = useState<Map<number, boolean>>(new Map());

    // Inizializzo a false la mappa
    const [numeroEsecuzioniInizializzazioneMostraSezioneOccupataDaConvoglio, setNumeroEsecuzioniInizializzazioneMostraSezioneOccupataDaConvoglio] = useState(0);
    const inizializzaMostraSezioneOccupataDaConvoglio = () => {
        const newMap = new Map<number, boolean>();
        convogli.forEach(convoglio => {
            newMap.set(convoglio.id, false);
        });
        setMostraSezioneOccupataDaConvoglio(newMap);
    }

    useEffect(() => {
        if (numeroEsecuzioniInizializzazioneMostraSezioneOccupataDaConvoglio < 2) {
            inizializzaMostraSezioneOccupataDaConvoglio();
            setNumeroEsecuzioniInizializzazioneMostraSezioneOccupataDaConvoglio(prev => prev + 1);
        }
    }, [convogli]);

    // ** VISUALIZAZZIONE STAZIONI COMPATIBILI ** //

    const [mostraStazioniCompatibiliConvoglio, setMostraStazioniCompatibiliConvoglio] = useState<Map<number, boolean>>(new Map());

    // Inizializzo a false la mappa
    const [numeroEsecuzioniInizializzazioneMostraStazioniCompatibiliConvoglio, setNumeroEsecuzioniInizializzazioneMostraStazioniCompatibiliConvoglio] = useState(0);
    const inizializzaMostraStazioniCompatibiliConvoglio = () => {
        const newMap = new Map<number, boolean>();
        convogli.forEach(convoglio => {
            newMap.set(convoglio.id, false);
        });
        setMostraStazioniCompatibiliConvoglio(newMap);
    };

    useEffect(() => {
        if (numeroEsecuzioniInizializzazioneMostraStazioniCompatibiliConvoglio < 5) {
            inizializzaMostraStazioniCompatibiliConvoglio();
            setNumeroEsecuzioniInizializzazioneMostraStazioniCompatibiliConvoglio(prev => prev + 1);
        }
    }, [convogli]);

    // ** VISUALIZZAZIONE SENSORI DI POSIZIONE ** //

    const [mostraSensori, setMostraSensori] = useState(false);
    const [mostraSezioni, setMostraSezioni] = useState(false);

    // *** LOGICA *** //

    useEffect(() => {
        logger.debug("Info autopilot aggiornato.");
    }, [statoAutopilotBackEnd, modalitaAutopilotBackEnd]);

    // In base allo stato autopilot ricevuto da Arduino, aggiorno lo stato dell'autopilot front-end
    useEffect(() => {
        switch (statoAutopilotBackEnd) {
            case StatoAutopilotBackEnd.NON_ATTIVO:
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.NON_ATTIVO);
                // Serve resettare i colori dei binari di stazione. Altrimenti, se l'utente sta scegliendo un binario di stazione e poi viene premuto il pulsante stop emergenza, il sinottico rimane colorato.
                resetColoriBinariStazioni();
                break;
            case StatoAutopilotBackEnd.TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO:
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO);
                break;
            case StatoAutopilotBackEnd.ATTIVO:
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.ATTIVO);

                // Se l'autopilot è attivo, è importante anche registrare la modalità dell'autopilot
                setModalitaAutopilotFrontEnd(modalitaAutopilotBackEnd as ModalitaAutopilot);
                break;
            default:
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.ERRORE);
                break;
        }
    }, [statoAutopilotBackEnd]);

    // Quando riceviamo da arduino l'oggetto convogli con tutti che hanno una posizione, possiamo attivare l'autopilot.
    useEffect(() => {
        // Controllo che convogli abbia almeno un elemento, altrimenti quando convogli è vuoto la condizione viene soddisfatta
        const tuttiConvogliPosizioneRegistrata = convogli.length > 0 && convogli.every(convoglio => convoglio !== null &&
            convoglio.autopilot.posizioneConvoglio.validita === ValiditaPosizioneConvoglio.POSIZIONE_VALIDA
        );

        // Se tutti i convogli hanno la posizione registrata e lo stato dell'autopilot è REGISTRAZIONE_POSIZIONE_CONVOGLI, significa che l'utente ha chiesto l'impostazione dell'autopilot, quindi posso passare allo stato REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETA
        if (tuttiConvogliPosizioneRegistrata && statoAutopilotFrontEnd == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI) {
            // Se la modalità dell'autopilot scelta è l'approccio casuale con scelta convogli, allora devo passare allo stato REGISTRAZIONE_CONVOGLI_SCELTA_AUTOPILOT_MODALITA_APPROCCIO_CASUALE
            if (modalitaAutopilotFrontEnd === ModalitaAutopilot.APPROCCIO_CASUALE_CON_SCELTA_CONVOGLI) {
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.REGISTRAZIONE_CONVOGLI_SCELTA_AUTOPILOT_MODALITA_APPROCCIO_CASUALE);
            } else {
                // Altrimenti, posso considerare la registrazione dei convogli completata
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA);
            }
        }
    }, [convogli, statoAutopilotFrontEnd]);

    // Modifico il numero di quadri comandi convoglio in base allo stato dell'autopilot
    useEffect(() => {
        // Voglio che quando lo stato dell'autopilot FrontEnd passi alla registrazione dei convogli, allora il numero di convogli mostrati sia totale. In questo modo l'utente può vedere a colpo d'occhio quali sono i convogli di cui bisogna registrare la posizione.
        if (statoAutopilotFrontEnd === StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI) {
            setNumeroQuadriComandiConvogli(Math.min(convogli.length, utenteAutenticato ? utenteAutenticato.numeroMassimoConvogliModalitaManuale : 0));
        } // Voglio che quando lo stato dell'autopilot FrontEnd passi alla registrazione dei convogli, allora si torni al numero di default, altrimenti il tracciato scende troppo giù.
        else if (statoAutopilotFrontEnd === StatoAutopilotFrontEnd.ATTIVO) {
            setNumeroQuadriComandiConvogli(Math.min(convogli.length, NUMERO_MASSIMO_QUADRI_COMANDO_CONVOGLIO_PER_RIGA));
        }
    }, [statoAutopilotFrontEnd]);

    // In base allo stato dell'autopilot, aggiorno gli scambi
    useEffect(() => {
        aggiornoCliccabilitaEListenerScambi(statoAutopilotFrontEnd, debuggingSezioni);
    }, [statoAutopilotFrontEnd, debuggingSezioni]);

    // Voglio che quando l'utente ha richiesto di vedere le sezioni occupate da un determinato convoglio, allora queste sezioni vengano mostrate.
    // Usiamo l'use effect in quanto le sezioni occupate cambiano in continuazione.
    useEffect(() => {
        // Cerchiamo l'ID del convoglio di cui l'utente ha chiesto di vedere le sezioni occupate, se esiste.
        const convoglioId = Array.from(mostraSezioniOccupateDaConvoglio.entries()).find(([_, value]) => value)?.[0];
        if (convoglioId !== undefined) {
            const convoglio = convogli.find(c => c.id === convoglioId);
            if (convoglio) {
                mostroSezioniOccupateDaConvoglio(convoglio, sezioni);
            }
        } else {
            resetColoriSezioni();
        }
    }, [sezioni, mostraSezioniOccupateDaConvoglio]);

    // Voglio che quando l'utente ha richiesto di vedere la sezione occupata da un determinato convoglio, allora questa sezione venga mostrata.
    // Usiamo l'use effect in quanto le sezioni occupate cambiano in continuazione.
    useEffect(() => {
        // Cerchiamo l'ID del convoglio di cui l'utente ha chiesto di vedere la sezione occupata, se esiste.
        const convoglioId = Array.from(mostraSezioneOccupataDaConvoglio.entries()).find(([_, value]) => value)?.[0];
        if (convoglioId !== undefined) {
            const convoglio = convogli.find(c => c.id === convoglioId);
            if (convoglio) {
                mostroSezioniOccupateDaConvoglio(convoglio, sezioni);
            }
        } else {
            resetColoriSezioni();
        }
        // Non aggiorno anche per sezioni, altrimenti quando questo useEffect interferisce con quello precedente
    }, [mostraSezioneOccupataDaConvoglio]);

    // Voglio che quando lo stato autopilot FrontEnd viene reimpostato in OFF, allora cancello la memoria di eventuali convogli che l'utente ha chiesto di vedere le sezioni occupate.
    useEffect(() => {
        if (statoAutopilotFrontEnd === StatoAutopilotFrontEnd.NON_ATTIVO) {
            inizializzaMostraSezioniOccupateDaConvoglio();
        }
    }, [statoAutopilotFrontEnd]);

    // Quando viene cambiato lo stato dei sensori di posizione oppure viene aggiornata la preferenza di visualizzazione dei sensori da parte dell'utente, aggiorno la visualizzazione dei sensori
    useEffect(() => {
        aggiornoStatoSensoriPosizione(sensoriPosizione, mostraSensori);
    }, [sensoriPosizione, mostraSensori]);

    // Quando cambia la preferenza di debugging dei sensori o la preferenza di visualizzazione dei sensori, aggiorno la cliccabilità dei sensori
    useEffect(() => {
        aggiornoCliccabilitaSensori(sensoriPosizione, debuggingSensori, mostraSensori);
    }, [debuggingSensori, mostraSensori]);

    // Mostro lo stato di tutte le sezioni
    useEffect(() => {
        // Aggiorno lo stato delle sezioni soltanto se attualmente non sto mostrando le sezioni occupate da un convoglio, altrimenti resetterei il colore delle sezioni occupate.
        // Verifica se nessuno dei valori nella mappa mostraSezioniOccupateDaConvoglio è true
        if (!Array.from(mostraSezioniOccupateDaConvoglio.values()).some(value => value)) {
            mostroStatoSezioni(sezioni, mostraSezioni);
        }
    }, [sezioni, mostraSezioni]);

    // Quando cambia la preferenza di debugging delle sezioni o la preferenza di visualizzazione delle sezioni, aggiorno la cliccabilità delle sezioni
    useEffect(() => {
        aggiornoCliccabilitaSezioni(sezioni, debuggingSezioni, mostraSezioni);
    }, [debuggingSezioni, mostraSezioni, sezioni]);

    // *** RICHIESTA REST PROGRAMMATA *** //

    // Variabile in uso per capire se è stata programmata una richiesta REST, in modo tale da non prendere in considerazione i messaggi MQTT Convoglio in arrivo nel frattempo.
    // Spiegazione dettagliata si trova nel file MQTTArduino.ts
    const [richiestaRestProgrammata, setRichiestaRestProgrammata] = useState<boolean>(false);

    // Dobbiamo usare un ref per mantenere il valore corrente di richiestaRestProgrammata, altrimenti mqttArduinoConnect continua a vedere il valore iniziale di richiestaRestProgrammata
    const richiestaRestProgrammataRef = useRef(richiestaRestProgrammata);

    useEffect(() => {
        richiestaRestProgrammataRef.current = richiestaRestProgrammata;
    }, [richiestaRestProgrammata]);

    // *** UTILITY *** //

    // ** VARIABILI ** //

    const [mostraFormSceltaModalitaAutopilot, setMostraFormSceltaModalitaAutopilot] = useState(false);

    // Variabile di stato per gestire eventuali errori
    const [error, setError] = useState<string | null>(null);

    // Variabile di stato per tenere traccia di quando la pagina è in fase di caricamento. Imposto a true all'avvio dell'applicazione
    const [isLoading, setIsLoading] = useState(true);

    const chunkArray = (array: any[], chunkSize: number): any[][] => {
        const chunks = [];
        for (let i = 0; i < array.length; i += chunkSize) {
            chunks.push(array.slice(i, i + chunkSize));
        }
        return chunks;
    };

    const righe = chunkArray(Array.from({length: numeroQuadriComandiConvogli}), NUMERO_MASSIMO_QUADRI_COMANDO_CONVOGLIO_PER_RIGA);

    // Se c'è un errore, mostriamo un messaggio di errore
    if (error) {
        return (
            <Container className="App">
                <Row>
                    <Col>
                        <div className="error">Errore durante l'avvio dell'App: <br/> {error}</div>
                    </Col>
                </Row>
            </Container>
        );
    }

    // ** FUNZIONI ** //

    const salvoStazioneCliccata = (idConvoglioSelezionato: number, idSezioneStazioneOggettoSvg: string) => {
        logger.info(`Memorizzo la stazione cliccata; Stazione cliccata: ${idSezioneStazioneOggettoSvg}; Convoglio selezionato: ${idConvoglioSelezionato}`);

        // Estraggo l'ID sezione dall'oggetto svg
        const idSezioneStazione = estraggoIdSezioneTipoDaStringa(idSezioneStazioneOggettoSvg);
        if (idSezioneStazione) {
            logger.debug(`IdSezione estratto: ${JSON.stringify(idSezioneStazione)}`);

            // Salvo nella mappa l'ID della stazione cliccata dall'utente per il convoglio selezionato
            setPosizioneConvoglioInRegistrazione(prevState => ({
                idConvoglio: idConvoglioSelezionato,
                idStazione: idSezioneStazione as IdSezioneTipo,
                direzionePrimaLocomotivaDestra: prevState?.direzionePrimaLocomotivaDestra
            }));
        } else {
            logger.error("Errore durante l'estrazione dell'IdSezione dal nome dell'oggetto svg");
        }

        setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI);

        // Dopo che l'utente ha selezionato la stazione per il convoglio nascondo i binari di stazione
        resetColoriBinariStazioni();
    };

    return (

        <Container fluid className={`App ${isLoading ? 'appLoading' : ''}`}>
            {isMobile
                ? (
                    <>
                        <Row className="mb-3">
                            <div className="titolo-mobile"
                                 style={{color: 'orange', fontWeight: 'bold'}}>
                                DCC Command Station
                            </div>
                        </Row>
                        {statoAutopilotFrontEnd === StatoAutopilotFrontEnd.NON_ATTIVO ? (
                            <Row className="mb-3">
                                <Col className="quadroComandiConvoglio-mobile">
                                    <QuadroComandiConvoglio
                                        convogli={convogli}
                                        locomotive={locomotive}
                                        setConvogli={setConvogli}
                                        setRichiestaRestProgrammata={setRichiestaRestProgrammata}
                                        idConvoglioDaMostrare={convogli[0] ? convogli[0].id : null}
                                        statoAutopilot={statoAutopilotFrontEnd}
                                        modalitaAutopilot={modalitaAutopilotFrontEnd}
                                        onStazioneClick={salvoStazioneCliccata}
                                        setPosizioneConvoglio={setPosizioneConvoglioInRegistrazione}
                                        setStatoAutopilot={setStatoAutopilotFrontEnd}
                                        sezioni={sezioni}
                                        autopilotAttivatoAutopilotModalitaApproccioCasuale={autopilotAttivatoAutopilotModalitaApproccioCasuale}
                                        setAutopilotAttivatoAutopilotModalitaApproccioCasuale={setAutopilotAttivatoAutopilotModalitaApproccioCasuale}
                                        mostraSezioniOccupateDaConvoglio={mostraSezioniOccupateDaConvoglio}
                                        setMostraSezioniOccupateDaConvoglio={setMostraSezioniOccupateDaConvoglio}
                                        utenteAutenticato={utenteAutenticato}
                                        setMostraSensori={setMostraSensori}
                                        setDebuggingSensori={setDebuggingSensori}
                                        setMostraSezioni={setMostraSezioni}
                                        setDebuggingSezioni={setDebuggingSezioni}
                                        mostraSezioni={mostraSezioni}
                                        mostraSezioneOccupataDaConvoglio={mostraSezioneOccupataDaConvoglio}
                                        setMostraSezioneOccupataDaConvoglio={setMostraSezioneOccupataDaConvoglio}
                                        mostraStazioniCompatibiliConvoglio={mostraStazioniCompatibiliConvoglio}
                                        setMostraStazioniCompatibiliConvoglio={setMostraStazioniCompatibiliConvoglio}
                                    />

                                </Col>
                            </Row>
                        ) : (
                            <Row className="mb-3">
                                <Col>
                                    <div className={"messaggio-autopilot-attivo-mobile"}>
                                        L'Autopilot è attivo!
                                    </div>
                                </Col>
                            </Row>
                        )}
                        {/** STOP EMERGENZA **/}
                        <Row className="mb-3 sticky-stop-emergenza-mobile">
                            <Col>
                                <BottoneStopEmergenza stopEmergenzaConvogli={stopEmergenzaConvogli}/>
                            </Col>
                        </Row>
                        {/** UTENTE LOGGATO **/}
                        <Row className="mb-3 sticky-login-mobile">
                            <Col>
                                <BottoneLogout
                                    setUtenteAutenticato={setUtenteAutenticato}
                                    utenteAutenticato={utenteAutenticato}
                                />
                            </Col>
                        </Row>
                    </>
                ) : (
                    <>
                        {/** TITOLO **/}
                        <Row className="mb-3">
                            <div className="titolo" style={{color: 'orange', fontWeight: 'bold', fontSize: '35px'}}>
                                DCC COMMAND STATION
                            </div>
                        </Row>
                        <Row className="mb-3">
                            {/** BOTTONE AUTOPILOT **/}
                            <Col>
                                <BottoneAutopilot
                                    statoAutopilotFrontEnd={statoAutopilotFrontEnd}
                                    modalitaAutopilotFrontEnd={modalitaAutopilotFrontEnd}
                                    convogli={convogli}
                                    setMostraFormSceltaModalitaAutopilot={setMostraFormSceltaModalitaAutopilot}
                                    setStatoAutopilotFrontEnd={setStatoAutopilotFrontEnd}
                                    ottimizzazioneAssegnazioneConvogliAiBinariAbilitata={ottimizzazioneAssegnazioneConvogliAiBinariAbilitata}
                                    autopilotAttivatoAutopilotModalitaApproccioCasuale={autopilotAttivatoAutopilotModalitaApproccioCasuale}
                                />
                            </Col>
                        </Row>
                        {/** FORM SCELTA MODALITA AUTOPILOT **/}
                        <Modal show={mostraFormSceltaModalitaAutopilot}
                               onHide={() => handleFormSceltaModalitaAutopilotChiusura(setMostraFormSceltaModalitaAutopilot)}>
                            <Modal.Header closeButton>
                                <Modal.Title>Seleziona la modalità desiderata</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    <Form.Group controlId="autopilotMode">
                                        <Form.Control as="select" value={modalitaAutopilotSelezionata}
                                                      onChange={(e) => setModalitaAutopilotSelezionata(e.target.value)}>
                                            <option value="stazioneSuccessiva">Stazione successiva</option>
                                            <option value="approccioCasuale">Approccio casuale</option>
                                            <option value="approccioCasualeConSceltaConvogli">Approccio casuale con
                                                scelta
                                                convogli
                                            </option>
                                        </Form.Control>
                                    </Form.Group>
                                    {(modalitaAutopilotSelezionata === 'approccioCasuale' || modalitaAutopilotSelezionata === 'approccioCasualeConSceltaConvogli') && (
                                        <Form.Group controlId="ottimizzazione">
                                            <Form.Label style={{marginRight: '10px', marginTop: '20px'}}>Vuoi
                                                ottimizzare
                                                l'assegnazione dei binari ai
                                                convogli?</Form.Label>
                                            <Button
                                                variant={ottimizzazioneAssegnazioneConvogliAiBinariAbilitata ? StileBottone.Attivo : StileBottone.Errore}
                                                onClick={() => setOttimizzazioneAssegnazioneConvogliAiBinariAbilitata(!ottimizzazioneAssegnazioneConvogliAiBinariAbilitata)}>
                                                {ottimizzazioneAssegnazioneConvogliAiBinariAbilitata ? 'Sì' : 'No'}
                                            </Button>
                                        </Form.Group>
                                    )}
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant={StileBottone.Errore}
                                        onClick={() => handleFormSceltaModalitaAutopilotChiusura(setMostraFormSceltaModalitaAutopilot)}>
                                    Chiudi
                                </Button>
                                <Button variant={StileBottone.Attivo}
                                        onClick={() => handleFormSceltaModalitaAutopilotSalvataggio(
                                            modalitaAutopilotSelezionata,
                                            setModalitaAutopilotFrontEnd,
                                            setStatoAutopilotFrontEnd,
                                            setMostraFormSceltaModalitaAutopilot,
                                            setMostraSensori,
                                            setDebuggingSensori,
                                            setMostraSezioni,
                                            setDebuggingSezioni
                                        )}> Salva
                                </Button>
                            </Modal.Footer>
                        </Modal>
                        {/** QUADRI CONTROLLO CONVOGLI **/}
                        <Row className="mb-3">
                            <Col>
                                <Form>
                                    <Form.Group controlId="numQuadri" className="d-flex align-items-center">
                                        <Form.Label>Numero convogli:</Form.Label>
                                        <Form.Select
                                            // Decido qui la distanza tra la scritta e il menù a tendina
                                            style={{marginLeft: '7px'}}
                                            value={numeroQuadriComandiConvogli}
                                            onChange={(event) => setNumeroQuadriComandiConvogli(Number(event.target.value))}
                                        >
                                            {Array.from({length: Math.min(convogli.length, utenteAutenticato ? utenteAutenticato.numeroMassimoConvogliModalitaManuale : 0)}, (_, index) => (
                                                <option key={index + 1} value={index + 1}>
                                                    {index + 1}
                                                </option>
                                            ))}
                                        </Form.Select>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        {righe.map((row, rowIndex) => (
                            <Row key={rowIndex}>
                                {row.map((_: any, colIndex: number) => {
                                    const index = rowIndex * NUMERO_MASSIMO_QUADRI_COMANDO_CONVOGLIO_PER_RIGA + colIndex;
                                    return (
                                        <Col key={index} className="quadroComandiConvoglio">
                                            <QuadroComandiConvoglio
                                                convogli={convogli}
                                                locomotive={locomotive}
                                                setConvogli={setConvogli}
                                                setRichiestaRestProgrammata={setRichiestaRestProgrammata}
                                                idConvoglioDaMostrare={convogli[index] ? convogli[index].id : null}
                                                statoAutopilot={statoAutopilotFrontEnd}
                                                modalitaAutopilot={modalitaAutopilotFrontEnd}
                                                onStazioneClick={salvoStazioneCliccata}
                                                setPosizioneConvoglio={setPosizioneConvoglioInRegistrazione}
                                                setStatoAutopilot={setStatoAutopilotFrontEnd}
                                                sezioni={sezioni}
                                                autopilotAttivatoAutopilotModalitaApproccioCasuale={autopilotAttivatoAutopilotModalitaApproccioCasuale}
                                                setAutopilotAttivatoAutopilotModalitaApproccioCasuale={setAutopilotAttivatoAutopilotModalitaApproccioCasuale}
                                                mostraSezioniOccupateDaConvoglio={mostraSezioniOccupateDaConvoglio}
                                                setMostraSezioniOccupateDaConvoglio={setMostraSezioniOccupateDaConvoglio}
                                                utenteAutenticato={utenteAutenticato}
                                                setMostraSensori={setMostraSensori}
                                                setDebuggingSensori={setDebuggingSensori}
                                                setMostraSezioni={setMostraSezioni}
                                                setDebuggingSezioni={setDebuggingSezioni}
                                                mostraSezioni={mostraSezioni}
                                                mostraSezioneOccupataDaConvoglio={mostraSezioneOccupataDaConvoglio}
                                                setMostraSezioneOccupataDaConvoglio={setMostraSezioneOccupataDaConvoglio}
                                                mostraStazioniCompatibiliConvoglio={mostraStazioniCompatibiliConvoglio}
                                                setMostraStazioniCompatibiliConvoglio={setMostraStazioniCompatibiliConvoglio}
                                            />
                                        </Col>
                                    );
                                })}
                            </Row>
                        ))}
                        {/** UTENTE LOGGATO **/}
                        <Row className="mb-3 sticky-login">
                            <Col>
                                <BottoneLogout
                                    setUtenteAutenticato={setUtenteAutenticato}
                                    utenteAutenticato={utenteAutenticato}
                                />
                            </Col>
                        </Row>
                        {/** STOP EMERGENZA **/}
                        <Row className="mb-3 sticky-stop-emergenza">
                            <Col>
                                <BottoneStopEmergenza stopEmergenzaConvogli={stopEmergenzaConvogli}/>
                            </Col>
                        </Row>
                        {/** VISUALIZZAZIONE SENSORI **/}
                        <Row className="mb-3">
                            <Col>
                                <div className="bottone-sensori">
                                    <BottoneVisualizzazioneSensori
                                        mostraSensori={mostraSensori}
                                        setMostraSensori={setMostraSensori}
                                    />
                                </div>
                            </Col>
                        </Row>
                        {/** VISUALIZZAZIONE SEZIONI **/}
                        <Row className="mb-3">
                            <Col>
                                <div className="bottone-sezioni">
                                    <BottoneVisualizzazioneSezioni
                                        mostraSezioni={mostraSezioni}
                                        setMostraSezioni={setMostraSezioni}
                                        mostraSezioniOccupateDaConvoglio={mostraSezioniOccupateDaConvoglio}
                                        mostraSezioneOccupataDaConvoglio={mostraSezioneOccupataDaConvoglio}
                                    />

                                </div>
                            </Col>
                        </Row>
                        {/** DEBUGGING - SENSORI **/}
                        <Row className="sticky-debugging-sensori">
                            <Col>
                                <BottoneDebuggingSensori
                                    debuggingSensori={debuggingSensori}
                                    setDebuggingSensori={setDebuggingSensori}
                                    setMostraSensori={setMostraSensori}
                                />
                            </Col>
                        </Row>
                        {/** DEBUGGING - SEZIONI**/}
                        <Row className="sticky-debugging-sezioni">
                            <Col>
                                <BottoneDebuggingSezioni
                                    debuggingSezioni={debuggingSezioni}
                                    setDebuggingSezioni={setDebuggingSezioni}
                                    setMostraSezioni={setMostraSezioni}
                                    mostraSezioniOccupateDaConvoglio={mostraSezioniOccupateDaConvoglio}
                                    mostraSezioneOccupataDaConvoglio={mostraSezioneOccupataDaConvoglio}
                                />
                            </Col>
                        </Row>
                        {/** SVG **/}
                        <Row>
                            <Col>
                                <div className="svg-container">
                                    <TracciatoPrimoPiano/>
                                </div>
                            </Col>
                        </Row>
                    </>
                )}
        </Container>
    );
}

export default Homepage;
