export enum DirezioneScambio {
    SINISTRA,
    DESTRA
}

export const direzioneScambioToString = (direzione: DirezioneScambio): string => {
    switch (direzione) {
        case DirezioneScambio.SINISTRA:
            return 'SINISTRA';
        case DirezioneScambio.DESTRA:
            return 'DESTRA';
        default:
            return '';
    }
};