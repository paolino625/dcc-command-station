export enum StatoAutopilotBackEnd {
    NON_ATTIVO,
    TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO,
    ATTIVO,
    ERRORE
}