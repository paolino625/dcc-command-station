import {Button} from "react-bootstrap";
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasualeProps {
    autopilotAttivatoAutopilotModalitaApproccioCasuale: { [idConvoglio: number]: boolean };
    setAutopilotAttivatoAutopilotModalitaApproccioCasuale: React.Dispatch<React.SetStateAction<{
        [idConvoglio: number]: boolean
    }>>;
    idConvoglioSelezionato: number;
}

const BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasuale: React.FC<BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasualeProps> = ({
                                                                                                                                                       autopilotAttivatoAutopilotModalitaApproccioCasuale,
                                                                                                                                                       setAutopilotAttivatoAutopilotModalitaApproccioCasuale,
                                                                                                                                                       idConvoglioSelezionato
                                                                                                                                                   }) => {

    const autopilotAttivato = autopilotAttivatoAutopilotModalitaApproccioCasuale[idConvoglioSelezionato];

    const ottengoTestoBottone = () => {
        if (autopilotAttivato) {
            return "Autopilot attivo";
        } else {
            return "Autopilot disattivo";
        }
    };

    const ottengoVariantBottone = () => {
        if (autopilotAttivato) {
            return StileBottone.Attivo;
        } else {
            return StileBottone.NonAttivoOk;
        }
    };

    const handleClick = () => {
        setAutopilotAttivatoAutopilotModalitaApproccioCasuale(prevState => ({
            ...prevState,
            [idConvoglioSelezionato]: !prevState[idConvoglioSelezionato]
        }));
    };

    return (
        <div className="attributoBottoneComuneConvoglio">
            <Button
                variant={ottengoVariantBottone()}
                onClick={handleClick}
                size="sm"
                className="text-left"
            >
                {ottengoTestoBottone()}
            </Button>
        </div>
    );
}

export default BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasuale;