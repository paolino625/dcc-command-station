import {ModalitaAutopilot} from "../../enumerazioni/ModalitaAutopilot.ts";
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import React from "react";

// Funzione che viene chiamata quando l'utente salva il form
export const handleFormSceltaModalitaAutopilotSalvataggio = (
    modalitaAutopilotSelezionata: string,
    setModalitaAutopilot: (value: ModalitaAutopilot) => void,
    setStatoAutopilot: (value: StatoAutopilotFrontEnd) => void,
    setMostraFormSceltaModalitaAutopilot: (value: boolean) => void,
    setMostraSensori: React.Dispatch<React.SetStateAction<boolean>>,
    setDebuggingSensori: React.Dispatch<React.SetStateAction<boolean>>,
    setMostraSezioni: React.Dispatch<React.SetStateAction<boolean>>,
    setDebuggingSezioni: React.Dispatch<React.SetStateAction<boolean>>
) => {
    // In base alla modalità selezionata dell'utente imposto lo stato dell'autopilot
    if (modalitaAutopilotSelezionata) {
        switch (modalitaAutopilotSelezionata) {
            case 'stazioneSuccessiva':
                setModalitaAutopilot(ModalitaAutopilot.STAZIONE_SUCCESSIVA);
                break;
            case 'approccioCasuale':
                setModalitaAutopilot(ModalitaAutopilot.APPROCCIO_CASUALE_TUTTI_CONVOGLI);
                break;
            case 'approccioCasualeConSceltaConvogli':
                setModalitaAutopilot(ModalitaAutopilot.APPROCCIO_CASUALE_CON_SCELTA_CONVOGLI);
                break;
            default:
                setModalitaAutopilot(ModalitaAutopilot.STAZIONE_SUCCESSIVA);
        }
    }

    setStatoAutopilot(StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI);
    setMostraFormSceltaModalitaAutopilot(false);
    // Voglio riportare il sinottico a una situazione pulita
    setMostraSensori(false);
    setMostraSezioni(false);
    setDebuggingSensori(false);
    setDebuggingSezioni(false);
};

// Funzione che viene chiamata quando l'utente chiude il form
export const handleFormSceltaModalitaAutopilotChiusura = (setMostraFormSceltaModalitaAutopilot: (value: boolean) => void) => {
    setMostraFormSceltaModalitaAutopilot(false);
};