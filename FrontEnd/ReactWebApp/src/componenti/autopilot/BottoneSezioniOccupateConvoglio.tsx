import {Button} from "react-bootstrap";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import {StileBottone} from "../../stile/StileBottone.ts";

interface Props {
    convoglio: Convoglio;
    mostraSezioniOccupateDaConvoglio: Map<number, boolean>;
    setMostraSezioniOccupateDaConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
    mostraSezioni: boolean;
}

const BottoneSezioniOccupateConvoglio: React.FC<Props> = ({
                                                              convoglio,
                                                              mostraSezioniOccupateDaConvoglio,
                                                              setMostraSezioniOccupateDaConvoglio, mostraSezioni
                                                          }) => {


    const getVariant = (): string => {
        if (mostraSezioniOccupateDaConvoglio.get(convoglio.id)) {
            return StileBottone.Attivo;
        }
        return StileBottone.Normale;
    }

    const getScritta = (): string => {
        return "Sezioni occupate";
    }

    const handleClick = () => {
        setMostraSezioniOccupateDaConvoglio(prevMap => {
            const newMap = new Map(prevMap);
            newMap.set(convoglio.id, !newMap.get(convoglio.id));
            return newMap;
        });

    }

    const isBottoneDisabilitato = (): boolean => {
        // Se l'utente ha scelto di mostrare tutte le sezioni, allora non disabilitiamo il bottone
        if (mostraSezioni) {
            return true;
        }

        // Vogliamo disabilitare il bottone se c'è un altro bottone attivo (ovvero se c'è un convoglio che sta mostrando le sezioni occupate)
        for (let [key, value] of mostraSezioniOccupateDaConvoglio.entries()) {
            if (value && key !== convoglio.id) {
                return true;
            }
        }
        return false;
    }

    return (
        <div className="attributoBottoneComuneConvoglio clickable">
            <Button variant={getVariant()} size="sm" onClick={handleClick} disabled={isBottoneDisabilitato()}>
                {getScritta()}
            </Button>
        </div>
    );
}

export default BottoneSezioniOccupateConvoglio;