import React, {useState} from "react";
import {Button} from "react-bootstrap";
import {StatoInizializzazioneAutopilotConvoglio} from "../../enumerazioni/StatoInizializzazioneAutopilotConvoglio.ts";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {mostroBinariStazioniRaggiungibiliDaPosizioneCorrenteConvoglio} from "../tracciato/Stazioni.ts";
import {Sezione} from "../../model/Sezione.ts";
import {StileBottone} from "../../stile/StileBottone.ts";

interface Props {
    convoglio: Convoglio;
    statoAutopilot: StatoAutopilotFrontEnd;
    setStatoAutopilotFrontEnd: React.Dispatch<React.SetStateAction<StatoAutopilotFrontEnd>>;
    sezioni: Sezione[];
    setMostraSensori: React.Dispatch<React.SetStateAction<boolean>>;
    setDebuggingSensori: React.Dispatch<React.SetStateAction<boolean>>;
    setMostraSezioni: React.Dispatch<React.SetStateAction<boolean>>;
    setDebuggingSezioni: React.Dispatch<React.SetStateAction<boolean>>;

    setMostraSezioniOccupateDaConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
}

const BottoneStatoInizializzazioneAutopilotConvoglio: React.FC<Props> = ({
                                                                             convoglio,
                                                                             statoAutopilot,
                                                                             setStatoAutopilotFrontEnd,
                                                                             sezioni,
                                                                             setMostraSensori,
                                                                             setDebuggingSensori,
                                                                             setMostraSezioni,
                                                                             setDebuggingSezioni,
                                                                             setMostraSezioniOccupateDaConvoglio
                                                                         }) => {

    const [sceltaBinarioStazioneDestinazione, setSceltaBinarioStazioneDestinazione] = useState<boolean>(false);

    const getScritta = (statoInizializzazione: StatoInizializzazioneAutopilotConvoglio): string => {
        if (sceltaBinarioStazioneDestinazione) {
            return 'Scelta binario';
        }

        switch (statoInizializzazione) {
            case StatoInizializzazioneAutopilotConvoglio.PRONTO_INIZIALIZZAZIONE_CONVOGLIO:
                return 'Non disponibile'; // Il bottone non dovrebbe essere mostrato quando in questo stato
            case StatoInizializzazioneAutopilotConvoglio.PRONTO_PER_CONFIGURAZIONE_AUTOPILOT:
                return 'Autopilot pronto';
            case StatoInizializzazioneAutopilotConvoglio.CONFIGURAZIONE_AUTOPILOT_COMPLETATA:
                return 'Autopilot in funzione'; // Il bottone non dovrebbe essere mostrato quando in questo stato
            default:
                return 'Stato sconosciuto';
        }
    };

    const getVariant = (statoInizializzazione: StatoInizializzazioneAutopilotConvoglio): string => {
        if (sceltaBinarioStazioneDestinazione) {
            return 'warning';
        }

        switch (statoInizializzazione) {
            case StatoInizializzazioneAutopilotConvoglio.PRONTO_INIZIALIZZAZIONE_CONVOGLIO:
                return StileBottone.NonAttivoOk;
            case StatoInizializzazioneAutopilotConvoglio.PRONTO_PER_CONFIGURAZIONE_AUTOPILOT:
                return StileBottone.Normale;
            case StatoInizializzazioneAutopilotConvoglio.CONFIGURAZIONE_AUTOPILOT_COMPLETATA:
                return StileBottone.Attivo;
            default:
                return StileBottone.Errore;
        }
    };

    const handleClick = () => {
        setSceltaBinarioStazioneDestinazione(true);
        setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO);

        // Mi assicuro di disattivare tutti i bottoni che vengono usati per mostrare la sezioni occupate correnti.
        setMostraSezioniOccupateDaConvoglio(prevMap => {
            const newMap = new Map(prevMap);
            prevMap.forEach((_, key) => {
                newMap.set(key, false);
            });

            return newMap;
        });

        // Voglio riportare il sinottico a una situazione pulita
        setMostraSensori(false);
        setMostraSezioni(false);
        setDebuggingSensori(false);
        setDebuggingSezioni(false);

        // Non posso eseguire subito la funzione, perché altrimenti dopo la sua esecuzione viene eseguito l'useEffect triggerato dal cambiamento di mostraSezioni che resetta lo stato di tutte le sezioni.
        // Vogliamo prima che l'useEffect venga eseguito, e soltanto dopo mostrare i binari delle stazioni.
        // Basta anche un millisecondo di attesa.
        setTimeout(() => {
            mostroBinariStazioniRaggiungibiliDaPosizioneCorrenteConvoglio(convoglio, sezioni, setStatoAutopilotFrontEnd);
        }, 1);
    };

    return (
        <div className="attributoBottoneComuneConvoglio clickable">
            <Button variant={getVariant(convoglio.autopilot.statoInizializzazioneAutopilotConvoglio)} size="sm"
                    onClick={handleClick}
                // Se l'autopilot è nello stato di registrazione della stazione destinazione ma non è questo il bottone che è stato azionato, lo disabilito.
                    disabled={!sceltaBinarioStazioneDestinazione && statoAutopilot === StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO}
            >
                {getScritta(convoglio.autopilot.statoInizializzazioneAutopilotConvoglio)}
            </Button>
        </div>
    );
};

export default BottoneStatoInizializzazioneAutopilotConvoglio;