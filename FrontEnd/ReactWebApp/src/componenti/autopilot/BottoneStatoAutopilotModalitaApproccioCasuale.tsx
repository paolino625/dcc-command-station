import React from 'react';
import {StatoAutopilotModalitaApproccioCasuale} from "../../enumerazioni/StatoAutopilotModalitaApproccioCasuale.ts";
import {Button} from "react-bootstrap";
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneStatoAutopilotModalitaApproccioCasualeProps {
    statoAutopilotModalitaApproccioCasuale: StatoAutopilotModalitaApproccioCasuale;
}

const BottoneStatoAutopilotModalitaApproccioCasuale: React.FC<BottoneStatoAutopilotModalitaApproccioCasualeProps> = ({statoAutopilotModalitaApproccioCasuale}) => {
    const ottengoTestoBottone = () => {
        switch (statoAutopilotModalitaApproccioCasuale) {
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA:
                return (
                    <>
                        Attesa momento<br/>partenza
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK:
                return (
                    <>
                        Scelta binario<br/>stazione e lock
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA:
                return (
                    <>
                        Salvataggio<br/>percorso
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO:
                return (
                    <>
                        Calcolo<br/>direzione
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE:
                return (
                    <>
                        Attesa lock<br/>sezioni
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.IN_PARTENZA_STAZIONE:
                return (
                    <>
                        In partenza
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE:
                return (
                    <>
                        In attesa<br/>1° sensore
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.IN_MOVIMENTO:
                return (
                    <>
                        In movimento
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.FERMO_SEZIONE_ATTESA_LOCK_SEZIONI:
                return (
                    <>
                        Attesa lock<br/>sezioni
                    </>
                );
            case StatoAutopilotModalitaApproccioCasuale.ARRIVATO_STAZIONE_DESTINAZIONE:
                return (
                    <>
                        Arrivato stazione
                    </>
                );
            default:
                return (
                    <>
                        Stato sconosciuto
                    </>
                );
        }
    };

    const ottengoVariantBottone = () => {
        switch (statoAutopilotModalitaApproccioCasuale) {
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA:
                return StileBottone.Warning;
            case StatoAutopilotModalitaApproccioCasuale.SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK:
                return StileBottone.Normale;
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA:
                return StileBottone.Normale;
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO:
                return StileBottone.Normale;
            case StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE:
                return StileBottone.Warning;
            case StatoAutopilotModalitaApproccioCasuale.IN_PARTENZA_STAZIONE:
                return StileBottone.Attivo;
            case StatoAutopilotModalitaApproccioCasuale.IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE:
                return StileBottone.Attivo;
            case StatoAutopilotModalitaApproccioCasuale.IN_MOVIMENTO:
                return StileBottone.Attivo;
            case StatoAutopilotModalitaApproccioCasuale.FERMO_SEZIONE_ATTESA_LOCK_SEZIONI:
                return StileBottone.Warning;
            case StatoAutopilotModalitaApproccioCasuale.ARRIVATO_STAZIONE_DESTINAZIONE:
                return StileBottone.Normale;
            default:
                return StileBottone.Errore;
        }
    };

    return (
        <div className="attributoBottoneComuneConvoglio">
            <Button
                variant={ottengoVariantBottone()}
                onClick={() => {
                }}
                size="sm"
                className="text-left"
            >
                {ottengoTestoBottone()}
            </Button>
        </div>
    );
};

export default BottoneStatoAutopilotModalitaApproccioCasuale;