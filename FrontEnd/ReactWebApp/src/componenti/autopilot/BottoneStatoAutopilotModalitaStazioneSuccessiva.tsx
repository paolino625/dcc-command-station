import React from 'react';
import {StatoAutopilotModalitaStazioneSuccessiva} from "../../enumerazioni/StatoAutopilotModalitaStazioneSuccessiva.ts";
import {Button} from "react-bootstrap";
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneStatoAutopilotModalitaStazioneSuccessivaProps {
    statoAutopilotModalitaStazioneSuccessiva: StatoAutopilotModalitaStazioneSuccessiva;
}

const BottoneStatoAutopilotModalitaStazioneSuccessiva: React.FC<BottoneStatoAutopilotModalitaStazioneSuccessivaProps> = ({statoAutopilotModalitaStazioneSuccessiva}) => {
    const ottengoTestoBottone = () => {
        switch (statoAutopilotModalitaStazioneSuccessiva) {
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA:
                return (
                    <>
                        Attesa momento<br/>partenza
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA:
                return (
                    <>
                        Salvataggio<br/>percorso
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO:
                return (
                    <>
                        Calcolo<br/>direzione
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE:
                return (
                    <>
                        Attesa lock<br/>sezioni
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.IN_PARTENZA_STAZIONE:
                return (
                    <>
                        In partenza
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE:
                return (
                    <>
                        In attesa<br/>1° sensore
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.IN_MOVIMENTO:
                return (
                    <>
                        In movimento
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_SEZIONE_ATTESA_LOCK_SEZIONI:
                return (
                    <>
                        Attesa lock<br/>sezioni
                    </>
                );
            case StatoAutopilotModalitaStazioneSuccessiva.ARRIVATO_STAZIONE_DESTINAZIONE:
                return (
                    <>
                        Arrivato stazione
                    </>
                );
            default:
                return "Stato Sconosciuto";
        }
    };

    const ottengoVariantBottone = () => {
        switch (statoAutopilotModalitaStazioneSuccessiva) {
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA:
                return StileBottone.Warning;
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA:
                return StileBottone.Normale;
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO:
                return StileBottone.Normale;
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE:
                return StileBottone.Warning;
            case StatoAutopilotModalitaStazioneSuccessiva.IN_PARTENZA_STAZIONE:
                return StileBottone.Attivo;
            case StatoAutopilotModalitaStazioneSuccessiva.IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE:
                return StileBottone.Attivo;
            case StatoAutopilotModalitaStazioneSuccessiva.IN_MOVIMENTO:
                return StileBottone.Attivo;
            case StatoAutopilotModalitaStazioneSuccessiva.FERMO_SEZIONE_ATTESA_LOCK_SEZIONI:
                return StileBottone.Warning;
            case StatoAutopilotModalitaStazioneSuccessiva.ARRIVATO_STAZIONE_DESTINAZIONE:
                return StileBottone.Normale;
            default:
                return StileBottone.Errore;
        }
    };

    return (
        <div className="attributoBottoneComuneConvoglio clickable">
            <Button
                variant={ottengoVariantBottone()}
                onClick={() => {
                }}
                size="sm"
                className="text-left"
            >
                {ottengoTestoBottone()}
            </Button>
        </div>
    );
};

export default BottoneStatoAutopilotModalitaStazioneSuccessiva;