import React, {useState} from 'react';
import {Col, Row} from 'react-bootstrap';
import SliderVelocitaConvoglio from './sottoComponenti/SliderVelocitaConvoglio.tsx';
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {PosizioneConvoglioStato} from "../../enumerazioni/PosizioneConvoglioStato.ts";
import {
    handleFormSceltaDirezioneConvoglioChiusura,
    handleFormSceltaDirezioneConvoglioSalvataggio
} from "./FormSceltaDirezioneConvoglio.ts";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import {Locomotiva} from "../../model/Locomotiva.ts";
import BottoneStatoPosizioneConvoglio from "./sottoComponenti/BottoneStatoPosizioneConvoglio.tsx";
import BottoneCambioDirezioneConvoglio from "./sottoComponenti/BottoneCambioDirezioneConvoglio.tsx";
import BottoneCambioLuciConvoglio from "./sottoComponenti/BottoneCambioLuci.tsx";
import TendinaSelezioneConvoglio from "./sottoComponenti/TendinaSelezioneConvoglio.tsx";
import {IdSezioneTipo} from "../../model/IdSezioneTipo.ts";
import {ModalitaAutopilot} from "../../enumerazioni/ModalitaAutopilot.ts";
import BottoneStatoAutopilotModalitaStazioneSuccessiva
    from "../autopilot/BottoneStatoAutopilotModalitaStazioneSuccessiva.tsx";
import BottoneStatoAutopilotModalitaApproccioCasuale
    from "../autopilot/BottoneStatoAutopilotModalitaApproccioCasuale.tsx";
import BottoneStatoInizializzazioneAutopilotConvoglio
    from "../autopilot/BottoneStatoInizializzazioneAutopilotConvoglio.tsx";
import {StatoInizializzazioneAutopilotConvoglio} from "../../enumerazioni/StatoInizializzazioneAutopilotConvoglio.ts";
import {Sezione} from "../../model/Sezione.ts";
import BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasuale
    from "../autopilot/BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasuale.tsx";
import BottoneSezioniOccupateConvoglio from "../autopilot/BottoneSezioniOccupateConvoglio.tsx";
import {Utente} from '../../model/Utente.ts';
import BottoneVisualizzazionePosizioneConvoglio from "./sottoComponenti/BottoneVisualizzazionePosizioneConvoglio.tsx";
import BottoneResetPosizioneConvoglio from "./sottoComponenti/BottoneResetPosizioneConvoglio.tsx";
import BottoneStazioniCompatibili from "./sottoComponenti/BottoneStazioniCompatibili.tsx";

interface QuadroComandiConvoglioProps {
    convogli: Convoglio[];
    locomotive: Locomotiva[];
    setConvogli: React.Dispatch<React.SetStateAction<Convoglio[]>>;
    setRichiestaRestProgrammata: React.Dispatch<React.SetStateAction<boolean>>;
    idConvoglioDaMostrare: number | null; // Può arrivarmi sia un numero che null
    statoAutopilot: StatoAutopilotFrontEnd;
    modalitaAutopilot: ModalitaAutopilot;
    onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void;
    setPosizioneConvoglio: React.Dispatch<React.SetStateAction<{
        idConvoglio: number,
        idStazione: IdSezioneTipo,
        direzionePrimaLocomotivaDestra: boolean | undefined
    } | null>>;
    setStatoAutopilot: React.Dispatch<React.SetStateAction<StatoAutopilotFrontEnd>>;
    sezioni: Sezione[];
    autopilotAttivatoAutopilotModalitaApproccioCasuale: { [idConvoglio: number]: boolean };
    setAutopilotAttivatoAutopilotModalitaApproccioCasuale: React.Dispatch<React.SetStateAction<{
        [idConvoglio: number]: boolean
    }>>;
    mostraSezioniOccupateDaConvoglio: Map<number, boolean>;
    setMostraSezioniOccupateDaConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
    utenteAutenticato: Utente | null;
    setMostraSensori: React.Dispatch<React.SetStateAction<boolean>>,
    setDebuggingSensori: React.Dispatch<React.SetStateAction<boolean>>,
    setMostraSezioni: React.Dispatch<React.SetStateAction<boolean>>,
    setDebuggingSezioni: React.Dispatch<React.SetStateAction<boolean>>
    mostraSezioni: boolean;
    mostraSezioneOccupataDaConvoglio: Map<number, boolean>;
    setMostraSezioneOccupataDaConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
    mostraStazioniCompatibiliConvoglio: Map<number, boolean>;
    setMostraStazioniCompatibiliConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
}

const QuadroComandiConvoglio: React.FC<QuadroComandiConvoglioProps> = ({
                                                                           convogli,
                                                                           setConvogli,
                                                                           setRichiestaRestProgrammata,
                                                                           idConvoglioDaMostrare,
                                                                           statoAutopilot,
                                                                           modalitaAutopilot,
                                                                           onStazioneClick,
                                                                           setPosizioneConvoglio,
                                                                           setStatoAutopilot,
                                                                           sezioni,
                                                                           autopilotAttivatoAutopilotModalitaApproccioCasuale,
                                                                           setAutopilotAttivatoAutopilotModalitaApproccioCasuale,
                                                                           mostraSezioniOccupateDaConvoglio,
                                                                           setMostraSezioniOccupateDaConvoglio,
                                                                           utenteAutenticato,
                                                                           setMostraSensori,
                                                                           setDebuggingSensori,
                                                                           setMostraSezioni,
                                                                           setDebuggingSezioni,
                                                                           mostraSezioni,
                                                                           mostraSezioneOccupataDaConvoglio,
                                                                           setMostraSezioneOccupataDaConvoglio,
                                                                           mostraStazioniCompatibiliConvoglio,
                                                                           setMostraStazioniCompatibiliConvoglio
                                                                       }) => {
    const [idConvoglioSelezionato, setIdConvoglioSelezionato] = useState<number>(idConvoglioDaMostrare as number);


    const convoglioSelezionato = convogli.find(convoglio => convoglio?.id === idConvoglioSelezionato) || null;


    const [posizioneConvoglioStato, setPosizioneConvoglioStato] = useState<PosizioneConvoglioStato>(PosizioneConvoglioStato.POSIZIONE_NON_REGISTRATA);

    const handleOnChangeConvoglioSelezionatoWrapper = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setIdConvoglioSelezionato(Number(event.target.value));
    };

    const [mostraFormSceltaDirezioneConvoglio, setmostraFormSceltaDirezioneConvoglio] = useState(false);

    const handleButtonePosizioneConvoglio = () => {
        if (posizioneConvoglioStato === PosizioneConvoglioStato.POSIZIONE_NON_REGISTRATA) {
            setmostraFormSceltaDirezioneConvoglio(true);
        }
    };


    return (
        <>
            {/** TENDINA SELEZIONE CONVOGLIO **/}

                <Row className="mb-3">
                    <TendinaSelezioneConvoglio
                        convogli={convogli}
                        idConvoglioSelezionato={idConvoglioSelezionato}
                        handleOnChangeConvoglioSelezionatoWrapper={handleOnChangeConvoglioSelezionatoWrapper}
                    />
                </Row>

            {convoglioSelezionato && (
                <>
                    <Row>
                        <Col>
                            {/** VELOCITA IMPOSTATA **/}
                            {statoAutopilot == StatoAutopilotFrontEnd.NON_ATTIVO && (
                                <Row>
                                    <div className="attributoBottoneComuneConvoglio"
                                         style={{width: '150px', height: '20px'}}>
                                        Vel. impostata: {convoglioSelezionato.velocitaImpostata}
                                    </div>
                                </Row>
                            )}
                            {/** VELOCITA ATTUALE **/}
                            <Row>
                                <div className="attributoBottoneComuneConvoglio">
                                    Vel. attuale: {convoglioSelezionato.velocitaAttuale}
                                </div>
                            </Row>
                            {/** LUCI ACCESE **/}
                            {/** Visualizzo il bottone soltanto se l'autopilot non è attivo o è attivo **/}
                            {(statoAutopilot == StatoAutopilotFrontEnd.NON_ATTIVO || statoAutopilot == StatoAutopilotFrontEnd.ATTIVO) && (
                                <Row>
                                    <BottoneCambioLuciConvoglio
                                        convoglioSelezionato={convoglioSelezionato}
                                        setConvogli={setConvogli}
                                    />
                                </Row>)}
                            {/** INVERTI MARCIA **/}
                            {statoAutopilot == StatoAutopilotFrontEnd.NON_ATTIVO && (
                                <Row>
                                    <BottoneCambioDirezioneConvoglio
                                        statoAutopilot={statoAutopilot}
                                        convoglioId={convoglioSelezionato.id}
                                    />
                                </Row>
                            )}
                            {/** STAZIONI COMPATIBILI **/}
                            {(statoAutopilot == StatoAutopilotFrontEnd.NON_ATTIVO) && (
                                <Row>
                                    <BottoneStazioniCompatibili
                                        convoglio={convoglioSelezionato}
                                        mostraStazioniCompatibiliConvoglio={mostraStazioniCompatibiliConvoglio}
                                        setMostraStazioniCompatibiliConvoglio={setMostraStazioniCompatibiliConvoglio}
                                        mostraSezioni={mostraSezioni}
                                    />
                                </Row>
                            )}
                            {/** STATO POSIZIONE **/}
                            {(statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI || statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLIO_IN_CORSO || statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA) && (

                                <Row>
                                    <BottoneStatoPosizioneConvoglio
                                        statoAutopilot={statoAutopilot}
                                        posizioneConvoglioStato={posizioneConvoglioStato}
                                        handleButtonePosizioneConvoglio={handleButtonePosizioneConvoglio}
                                        setmostraFormSceltaDirezioneConvoglio={setmostraFormSceltaDirezioneConvoglio}
                                        mostraFormSceltaDirezioneConvoglio={mostraFormSceltaDirezioneConvoglio}
                                        handleFormSceltaDirezioneConvoglioChiusura={handleFormSceltaDirezioneConvoglioChiusura}
                                        handleFormSceltaDirezioneConvoglioSalvataggio={handleFormSceltaDirezioneConvoglioSalvataggio}
                                        idConvoglioSelezionato={idConvoglioSelezionato}
                                        onStazioneClick={onStazioneClick}
                                        setPosizioneConvoglioStato={setPosizioneConvoglioStato}
                                        convogli={convogli}
                                        setPosizioneConvoglio={setPosizioneConvoglio}
                                        setStatoAutopilot={setStatoAutopilot}
                                        sezioni={sezioni}
                                    />
                                </Row>
                            )}

                            {/** VISUALIZZAZIONE POSIZIONE CONVOGLIO**/}
                            {/** Visualizzo il bottone soltanto se il convoglio ha la posizione registrata e se lo stato dell'autopilot riguarda la registrazione della posizione dei convogli **/}
                            {(posizioneConvoglioStato == PosizioneConvoglioStato.POSIZIONE_REGISTRATA && (statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI || statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA)) && (
                                <Row>
                                    <BottoneVisualizzazionePosizioneConvoglio
                                        convoglio={convoglioSelezionato}
                                        mostraSezioneOccupataDaConvoglio={mostraSezioneOccupataDaConvoglio}
                                        setMostraSezioneOccupataDaConvoglio={setMostraSezioneOccupataDaConvoglio}
                                        mostraSezioni={mostraSezioni}
                                        posizioneConvoglioStato={posizioneConvoglioStato}
                                    />
                                </Row>
                            )}
                            {/** RESET POSIZIONE CONVOGLIO**/}
                            {/** Visualizzo il bottone soltanto se il convoglio ha la posizione registrata e se lo stato dell'autopilot riguarda la registrazione della posizione dei convogli **/}
                            {(posizioneConvoglioStato == PosizioneConvoglioStato.POSIZIONE_REGISTRATA && (statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI || statoAutopilot == StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA)) && (
                                <Row>
                                    <BottoneResetPosizioneConvoglio
                                        convoglio={convoglioSelezionato}
                                    />
                                </Row>
                            )}
                            {/** SEZIONI OCCUPATE CONVOGLIO **/}
                            {}
                            {(statoAutopilot === StatoAutopilotFrontEnd.ATTIVO || statoAutopilot === StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO) && (
                                <Row>
                                    <BottoneSezioniOccupateConvoglio
                                        convoglio={convoglioSelezionato}
                                        mostraSezioniOccupateDaConvoglio={mostraSezioniOccupateDaConvoglio}
                                        setMostraSezioniOccupateDaConvoglio={setMostraSezioniOccupateDaConvoglio}
                                        mostraSezioni={mostraSezioni}
                                    />
                                </Row>
                            )}

                            {/** STATO INIZIALIZZAZIONE AUTOPILOT CONVOGLIO **/}
                            {modalitaAutopilot === ModalitaAutopilot.STAZIONE_SUCCESSIVA && (statoAutopilot === StatoAutopilotFrontEnd.ATTIVO || statoAutopilot === StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO) && convoglioSelezionato.autopilot.statoInizializzazioneAutopilotConvoglio === StatoInizializzazioneAutopilotConvoglio.PRONTO_PER_CONFIGURAZIONE_AUTOPILOT && (
                                <Row>
                                    <BottoneStatoInizializzazioneAutopilotConvoglio
                                        convoglio={convoglioSelezionato}
                                        statoAutopilot={statoAutopilot}
                                        setStatoAutopilotFrontEnd={setStatoAutopilot}
                                        sezioni={sezioni}
                                        setMostraSensori={setMostraSensori}
                                        setDebuggingSensori={setDebuggingSensori}
                                        setMostraSezioni={setMostraSezioni}
                                        setDebuggingSezioni={setDebuggingSezioni}
                                        setMostraSezioniOccupateDaConvoglio={setMostraSezioniOccupateDaConvoglio}
                                    />
                                </Row>
                            )}
                            {/** STATO AUTOPILOT MODALITA STAZIONE SUCCESSIVA **/
                                // Questo bottone lo mostro se l'autopilot è attivo, la modalità è stazione successiva e la configurazione dell'autopilot è completata.
                            }
                            {(statoAutopilot === StatoAutopilotFrontEnd.ATTIVO || statoAutopilot === StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO) && modalitaAutopilot === ModalitaAutopilot.STAZIONE_SUCCESSIVA && convoglioSelezionato.autopilot.statoInizializzazioneAutopilotConvoglio === StatoInizializzazioneAutopilotConvoglio.CONFIGURAZIONE_AUTOPILOT_COMPLETATA && (
                                <Row>
                                    <BottoneStatoAutopilotModalitaStazioneSuccessiva
                                        statoAutopilotModalitaStazioneSuccessiva={convoglioSelezionato.autopilot.statoAutopilotModalitaStazioneSuccessiva}/>
                                </Row>
                            )}
                            {/** STATO AUTOPILOT MODALITA APPROCCIO CASUALE TUTTI CONVOGLI **/}
                            {statoAutopilot === StatoAutopilotFrontEnd.ATTIVO && modalitaAutopilot === ModalitaAutopilot.APPROCCIO_CASUALE_TUTTI_CONVOGLI && (
                                <Row>
                                    <BottoneStatoAutopilotModalitaApproccioCasuale
                                        statoAutopilotModalitaApproccioCasuale={convoglioSelezionato.autopilot.statoAutopilotModalitaApproccioCasuale}/>
                                </Row>
                            )}


                            {/** STATO AUTOPILOT MODALITA APPROCCIO CASUALE SCELTA CONVOGLI**/}
                            {statoAutopilot === StatoAutopilotFrontEnd.ATTIVO && modalitaAutopilot === ModalitaAutopilot.APPROCCIO_CASUALE_CON_SCELTA_CONVOGLI && convoglioSelezionato.autopilot.statoInizializzazioneAutopilotConvoglio === StatoInizializzazioneAutopilotConvoglio.CONFIGURAZIONE_AUTOPILOT_COMPLETATA && (
                                <Row>
                                    <BottoneStatoAutopilotModalitaApproccioCasuale
                                        statoAutopilotModalitaApproccioCasuale={convoglioSelezionato.autopilot.statoAutopilotModalitaApproccioCasuale}/>
                                </Row>
                            )}

                            {/** BOTTONE ATTIVAZIONE CONVOGLIO AUTOPILOT MODALITA APPROCCIO CASUALE **/}
                            {modalitaAutopilot === ModalitaAutopilot.APPROCCIO_CASUALE_CON_SCELTA_CONVOGLI && statoAutopilot === StatoAutopilotFrontEnd.REGISTRAZIONE_CONVOGLI_SCELTA_AUTOPILOT_MODALITA_APPROCCIO_CASUALE && (

                                <Row>
                                    <BottoneAttivazioneConvoglioAutopilotModalitaApproccioCasuale
                                        autopilotAttivatoAutopilotModalitaApproccioCasuale={autopilotAttivatoAutopilotModalitaApproccioCasuale}
                                        setAutopilotAttivatoAutopilotModalitaApproccioCasuale={setAutopilotAttivatoAutopilotModalitaApproccioCasuale}
                                        idConvoglioSelezionato={idConvoglioSelezionato}
                                    />
                                </Row>
                            )}
                        </Col>
                        {/** SLIDER VELOCITA **/}
                        <Col>
                            <div>
                                <SliderVelocitaConvoglio convoglioId={convoglioSelezionato.id} convogli={convogli}
                                                         setConvogli={setConvogli}
                                                         setRichiestaRestProgrammata={setRichiestaRestProgrammata}
                                                         statoAutopilot={statoAutopilot}
                                                         utenteAutenticato={utenteAutenticato}
                                />
                            </div>
                        </Col>
                    </Row>
                </>
            )}
        </>
    );
};

export default QuadroComandiConvoglio;