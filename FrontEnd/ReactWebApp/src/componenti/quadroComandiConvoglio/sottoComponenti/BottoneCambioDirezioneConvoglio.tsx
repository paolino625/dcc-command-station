import React from 'react';
import {Button} from 'react-bootstrap';
import {StatoAutopilotFrontEnd} from "../../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {
    cambioDirezioneConvoglioRestCall
} from "../../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/CambioDirezioneConvoglio.ts";

interface BottoneCambioDirezioneProps {
    statoAutopilot: StatoAutopilotFrontEnd;
    convoglioId: number;
}

const BottoneCambioDirezioneConvoglio: React.FC<BottoneCambioDirezioneProps> = ({statoAutopilot, convoglioId}) => {
    if (statoAutopilot !== StatoAutopilotFrontEnd.NON_ATTIVO) {
        return null;
    }

    return (

        <div className="attributoBottoneComuneConvoglio clickable">
            <Button
                id="CambiaDirezione"
                className="button-orange"
                onClick={() => cambioDirezioneConvoglioRestCall(convoglioId)}
                size="sm"
            >
                Inverti marcia
            </Button>
        </div>

    );
};

export default BottoneCambioDirezioneConvoglio;