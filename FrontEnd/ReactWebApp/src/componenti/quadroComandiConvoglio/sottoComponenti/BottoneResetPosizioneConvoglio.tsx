import {Button} from "react-bootstrap";
import {StileBottone} from "../../../stile/StileBottone.ts";
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";
import {
    resetPosizioneConvoglioRestCall
} from "../../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/preAvvio/ResetPosizioneConvoglioRestCall.ts";

interface BottoneResetPosizioneConvoglioProps {
    convoglio: Convoglio;
}

const BottoneResetPosizioneConvoglio: React.FC<BottoneResetPosizioneConvoglioProps> = (convoglio) => {
    const getVariant = (): string => {
        return StileBottone.Warning;
    }

    const getScritta = (): React.JSX.Element => {
        return (
            <>
                Reset <br/>posizione
            </>
        );
    }

    const handleOnClick = () => {
        // Al click invio la richiesta di reset della posizione del convoglio
        resetPosizioneConvoglioRestCall(convoglio.convoglio.id);
    }

    return (
        <div className="attributoBottoneComuneConvoglio">
            <Button className={"text-left"} variant={getVariant()} size="sm" onClick={handleOnClick}
            >
                {getScritta()}
            </Button>
        </div>
    );
};

export default BottoneResetPosizioneConvoglio;