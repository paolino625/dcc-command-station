import React from 'react';
import Slider from '@mui/material/Slider';
import {
    invioEntitaAggiornataArduino
} from "../../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/put/InvioEntitaAggiornataArduino.ts";
import {StatoAutopilotFrontEnd} from "../../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";
import {Utente} from "../../../model/Utente.ts";

interface LocomotivaSpeedSliderProps {
    convoglioId: number;
    convogli: Convoglio[];
    setConvogli: React.Dispatch<React.SetStateAction<Convoglio[]>>;
    setRichiestaRestProgrammata: React.Dispatch<React.SetStateAction<boolean>>;
    statoAutopilot: StatoAutopilotFrontEnd;
    utenteAutenticato: Utente | null;
}

const DELAY_MILLISECONDI_SET_VARIABILE_RICHIESTA_REST_PROGRAMMATA = 50;

let updateTimeout: NodeJS.Timeout | null = null;

let timestampUltimoInvio: number | null = null;

const SliderVelocitaConvoglio: React.FC<LocomotivaSpeedSliderProps> = ({
                                                                           convoglioId,
                                                                           convogli,
                                                                           setConvogli,
                                                                           setRichiestaRestProgrammata,
                                                                           statoAutopilot, utenteAutenticato
                                                                       }) => {
    // Con -find trovo il convoglio con l'id corrispondente
    // => c: verifico che il convoglio non è null (cosa vera per l'elemento 0 del convoglio
    const convoglio = convogli.find(convoglio => convoglio && convoglio.id === convoglioId);

    const velocitaMassimaConvoglioAttuale = Math.min(utenteAutenticato ? utenteAutenticato.velocitaMassima : 0, convoglio?.velocitaMassima || 0);

    // @ts-ignore
    const handleSliderChange = (event: Event, newValue: number | number[]) => {
        const nuovaVelocita = newValue as number;
        const convogliTemporaneo = convogli.map(convoglio =>
            convoglio.id === convoglioId ? {...convoglio, velocitaImpostata: nuovaVelocita} : convoglio
        );
        setConvogli(convogliTemporaneo);

        const timestampAttuale = Date.now();
        // Se timestampUltimoInvio è stato inizializzato, controllo se è passato meno dell'intervallo richiesto dall'ultima chiamata REST
        if (timestampUltimoInvio && timestampAttuale - timestampUltimoInvio < import.meta.env.VITE_INTERVALLO_CHIAMATE_REST) {

            // Se è stata programmata una chiamata in precedenza, bisogna cancellarla prima di reimpostare quella nuova.
            if (updateTimeout) {
                clearTimeout(updateTimeout);
            }

            setRichiestaRestProgrammata(true);

            // Programmo una nuova chiamata REST per aggiornare la velocità del convoglio
            updateTimeout = setTimeout(() => {
                invioEntitaAggiornataArduino("convoglio", convoglioId, {velocitaImpostata: nuovaVelocita});
                // Aspetto un po' prima di reimpostare la variabile richiestaRestProgrammata a false.
                // Altrimenti potrebbe succedere che un messaggio MQTT inviato da Arduino appena prima di ricevere la nuova richiesta REST arrivi in questo momento e dunque venga preso in considerazione erroneamente.
                setTimeout(() => {
                    setRichiestaRestProgrammata(false);
                }, DELAY_MILLISECONDI_SET_VARIABILE_RICHIESTA_REST_PROGRAMMATA);
            }, import.meta.env.VITE_INTERVALLO_CHIAMATE_REST);


        } else {
            // Se è passato più di un intervallo di tempo dall'ultima chiamata REST, posso effettuare una nuova chiamata immediatamente
            invioEntitaAggiornataArduino("convoglio", convoglioId, {velocitaImpostata: nuovaVelocita});
            timestampUltimoInvio = timestampAttuale;
        }

    };

    return (
        <div className="slider-container">
            <div className="velocitaMassima">{velocitaMassimaConvoglioAttuale}</div>
            <Slider
                value={convoglio?.velocitaImpostata || 0}
                onChange={handleSliderChange}
                aria-labelledby="velocita-slider"
                min={0}
                max={velocitaMassimaConvoglioAttuale}
                orientation="vertical"
                disabled={statoAutopilot !== StatoAutopilotFrontEnd.NON_ATTIVO}
            />
            <div className="velocitaMinima">0</div>
        </div>
    );
};

export default SliderVelocitaConvoglio;