import React from 'react';
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";

interface SelezioneConvoglioProps {
    convogli: Convoglio[];
    idConvoglioSelezionato: number;
    handleOnChangeConvoglioSelezionatoWrapper: (event: React.ChangeEvent<HTMLSelectElement>) => void;
}

const TendinaSelezioneConvoglio: React.FC<SelezioneConvoglioProps> = ({
                                                                          convogli,
                                                                          idConvoglioSelezionato,
                                                                          handleOnChangeConvoglioSelezionatoWrapper,

                                                                      }) => {
    return (

        <select className="tendinaSelezioneConvoglio" onChange={handleOnChangeConvoglioSelezionatoWrapper}
                value={idConvoglioSelezionato ?? ''}>
            <option value="" disabled>Seleziona convoglio</option>
            {convogli.map(convoglio => {
                if (!convoglio) return null;
                return (
                    <option key={convoglio.id} value={convoglio.id}>
                        {convoglio.nome}
                    </option>
                );
            })}
        </select>

    );
};


export default TendinaSelezioneConvoglio;