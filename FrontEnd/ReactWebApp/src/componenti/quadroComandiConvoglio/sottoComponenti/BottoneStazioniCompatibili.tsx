import {Button} from "react-bootstrap";
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";
import {StileBottone} from "../../../stile/StileBottone.ts";
import {
    mostroBinariStazioniCompatibiliConvoglio, resetColoriBinariStazioni,
} from "../../tracciato/Stazioni.ts";

interface BottoneStazioniCompatibiliProps {
    convoglio: Convoglio;
    mostraStazioniCompatibiliConvoglio: Map<number, boolean>;
    setMostraStazioniCompatibiliConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
    mostraSezioni: boolean;
}

const BottoneStazioniCompatibili: React.FC<BottoneStazioniCompatibiliProps> = ({
                                                                                   convoglio,
                                                                                   mostraStazioniCompatibiliConvoglio,
                                                                                   setMostraStazioniCompatibiliConvoglio,
                                                                                   mostraSezioni
                                                                               }) => {


    const getVariant = (): string => {
        if (mostraStazioniCompatibiliConvoglio.get(convoglio.id)) {
            return StileBottone.Attivo;
        }
        return StileBottone.Normale;
    }

    const getScritta = (): React.ReactNode => {
        return (
            <>
                Stazioni<br/>compatibili
            </>
        );
    }

    const handleOnClick = () => {
        // Ottengo lo stato del bottone prima di aggiornarlo
        const statoPrecedenteBottone = mostraStazioniCompatibiliConvoglio.get(convoglio.id);

        setMostraStazioniCompatibiliConvoglio(prevMap => {
            const newMap = new Map(prevMap);
            newMap.set(convoglio.id, !newMap.get(convoglio.id));
            return newMap;
        });

        if (!statoPrecedenteBottone) {
            // Se il bottone passa da OFF -> ON, allora mostro i binari delle stazioni compatibili con il convoglio
            mostroBinariStazioniCompatibiliConvoglio(convoglio);
        } else {
            // Se il bottone passa da ON -> OFF, allora nascondo i binari delle stazioni compatibili con il convoglio
            resetColoriBinariStazioni();
        }


    }

    // Vogliamo disabilitare il bottone se c'è un altro bottone attivo (ovvero se c'è un convoglio che sta mostrando le sezioni occupate)
    const isBottoneDisabilitato = (): boolean => {
        // Se l'utente ha scelto di mostrare tutte le sezioni, allora non disabilitiamo il bottone
        if (mostraSezioni) {
            return true;
        }

        // Vogliamo disabilitare il bottone se c'è un altro bottone attivo (ovvero se c'è un convoglio che sta mostrando le stazioni compatibili)
        for (let [key, value] of mostraStazioniCompatibiliConvoglio.entries()) {
            if (value && key !== convoglio.id) {
                return true;
            }
        }

        return false;
    }

    return (
        <div className="attributoBottoneComuneConvoglio clickable">
            <Button variant={getVariant()} size="sm" onClick={handleOnClick}
                    disabled={isBottoneDisabilitato()}>
                {getScritta()}
            </Button>
        </div>
    );
}

export default BottoneStazioniCompatibili;