import React, {useEffect, useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import {StatoAutopilotFrontEnd} from "../../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {PosizioneConvoglioStato} from "../../../enumerazioni/PosizioneConvoglioStato.ts";
import {
    handleClickStazioneSceltaPosizioneInizialeConvoglio,
    mostroBinariStazioniLiberi
} from "../../tracciato/Stazioni.ts";
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";
import {IdSezioneTipo} from "../../../model/IdSezioneTipo.ts";
import {ValiditaPosizioneConvoglio} from "../../../enumerazioni/ValiditaPosizioneConvoglio.ts";
import {Sezione} from "../../../model/Sezione.ts";
import {StileBottone} from "../../../stile/StileBottone.ts";

interface BottonePosizioneConvoglioProps {
    statoAutopilot: StatoAutopilotFrontEnd;
    posizioneConvoglioStato: PosizioneConvoglioStato;
    handleButtonePosizioneConvoglio: () => void;
    setmostraFormSceltaDirezioneConvoglio: React.Dispatch<React.SetStateAction<boolean>>;
    mostraFormSceltaDirezioneConvoglio: boolean;
    handleFormSceltaDirezioneConvoglioChiusura: (setmostraFormSceltaDirezioneConvoglio: React.Dispatch<React.SetStateAction<boolean>>) => void;
    handleFormSceltaDirezioneConvoglioSalvataggio: (
        direzioneSelezionata: 'destra' | 'sinistra' | null,
        setmostraFormSceltaDirezioneConvoglio: React.Dispatch<React.SetStateAction<boolean>>,
        mostraStazioniAttive: (callback: (event: Event) => void, sezioni: Sezione[], idConvoglioSelezionato: number) => void,
        handleStazioneClick: (event: Event, idConvoglioSelezionato: number, onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void) => void,
        idConvoglioSelezionato: number,
        onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void,
        setPosizioneConvoglioStato: React.Dispatch<React.SetStateAction<PosizioneConvoglioStato>>,
        setPosizioneConvogli: React.Dispatch<React.SetStateAction<{
            idConvoglio: number,
            idStazione: IdSezioneTipo,
            direzionePrimaLocomotivaDestra: boolean | undefined
        } | null>>,
        setStatoAutopilot: (value: StatoAutopilotFrontEnd) => void,
        sezioni: Sezione[]
    ) => void;
    idConvoglioSelezionato: number;
    onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void;
    setPosizioneConvoglioStato: React.Dispatch<React.SetStateAction<PosizioneConvoglioStato>>;
    convogli: Convoglio[];
    setPosizioneConvoglio: React.Dispatch<React.SetStateAction<{
        idConvoglio: number,
        idStazione: IdSezioneTipo,
        direzionePrimaLocomotivaDestra: boolean | undefined
    } | null>>;
    setStatoAutopilot: (value: StatoAutopilotFrontEnd) => void;
    sezioni: Sezione[];
}

const BottoneStatoPosizioneConvoglio: React.FC<BottonePosizioneConvoglioProps> = ({
                                                                                      statoAutopilot,
                                                                                      posizioneConvoglioStato,
                                                                                      handleButtonePosizioneConvoglio,
                                                                                      setmostraFormSceltaDirezioneConvoglio,
                                                                                      mostraFormSceltaDirezioneConvoglio,
                                                                                      handleFormSceltaDirezioneConvoglioChiusura,
                                                                                      handleFormSceltaDirezioneConvoglioSalvataggio,
                                                                                      idConvoglioSelezionato,
                                                                                      onStazioneClick,
                                                                                      setPosizioneConvoglioStato,
                                                                                      convogli,
                                                                                      setPosizioneConvoglio,
                                                                                      setStatoAutopilot,
                                                                                      sezioni
                                                                                  }) => {
    const [direzioneSelezionata, setDirezioneSelezionata] = useState<'destra' | 'sinistra' | null>(null);

    // Visualizzo il bottone in base allo stato della posizione del convoglio (sempre aggiornata da Arduino)
    useEffect(() => {
        if (convogli) {
            const convoglioSelezionato = convogli.find((convoglio: Convoglio) => convoglio?.id === idConvoglioSelezionato);
            if (convoglioSelezionato) {
                if (convoglioSelezionato.autopilot.posizioneConvoglio.validita === ValiditaPosizioneConvoglio.POSIZIONE_VALIDA) {
                    setPosizioneConvoglioStato(PosizioneConvoglioStato.POSIZIONE_REGISTRATA);
                } else {
                    setPosizioneConvoglioStato(PosizioneConvoglioStato.POSIZIONE_NON_REGISTRATA);
                }
            }
        }
    }, [idConvoglioSelezionato, convogli]);

    return (
        <div className="attributoBottoneComuneConvoglio">
            <Button
                className={"text-left"}
                variant={
                    (() => {
                        switch (posizioneConvoglioStato) {
                            case PosizioneConvoglioStato.POSIZIONE_NON_REGISTRATA:
                                return "danger";
                            case PosizioneConvoglioStato.POSIZIONE_IN_REGISTRAZIONE:
                                return "warning";
                            case PosizioneConvoglioStato.POSIZIONE_REGISTRATA:
                                return "success";
                            default:
                                return "danger";
                        }
                    })()
                }
                onClick={handleButtonePosizioneConvoglio}
                disabled={posizioneConvoglioStato === PosizioneConvoglioStato.POSIZIONE_REGISTRATA || posizioneConvoglioStato === PosizioneConvoglioStato.POSIZIONE_IN_REGISTRAZIONE || (posizioneConvoglioStato == PosizioneConvoglioStato.POSIZIONE_NON_REGISTRATA && statoAutopilot === StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLIO_IN_CORSO)}
                size="sm"
            >
                {
                    (() => {
                        switch (posizioneConvoglioStato) {
                            case PosizioneConvoglioStato.POSIZIONE_NON_REGISTRATA:
                                return (
                                    <>
                                        Posizione <br/>non registrata
                                    </>
                                );
                            case PosizioneConvoglioStato.POSIZIONE_IN_REGISTRAZIONE:
                                return (
                                    <>
                                        Posizione <br/>in registrazione
                                    </>
                                );
                            case PosizioneConvoglioStato.POSIZIONE_REGISTRATA:
                                return (
                                    <>
                                        Posizione <br/>registrata
                                    </>
                                );
                            default:
                                return "Stato sconosciuto";
                        }
                    })()
                }
            </Button>
            <Modal show={mostraFormSceltaDirezioneConvoglio}
                   onHide={() => handleFormSceltaDirezioneConvoglioChiusura(setmostraFormSceltaDirezioneConvoglio)}>
                <Modal.Header>
                    <Modal.Title>Inserisci le informazioni</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group>
                            <Form.Label>Quale locomotiva ha le
                                luci
                                accese?</Form.Label>
                            <Form.Check
                                type="radio"
                                label="Destra"
                                name="direction"
                                value="destra"
                                onChange={() => setDirezioneSelezionata('destra')}
                            />
                            <Form.Check
                                type="radio"
                                label="Sinistra"
                                name="direction"
                                value="sinistra"
                                onChange={() => setDirezioneSelezionata('sinistra')}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant={StileBottone.Errore}
                            onClick={() => handleFormSceltaDirezioneConvoglioChiusura(setmostraFormSceltaDirezioneConvoglio)}>
                        Chiudi
                    </Button>
                    <Button variant={StileBottone.Attivo} onClick={() => handleFormSceltaDirezioneConvoglioSalvataggio(
                        direzioneSelezionata,
                        setmostraFormSceltaDirezioneConvoglio,
                        mostroBinariStazioniLiberi,
                        handleClickStazioneSceltaPosizioneInizialeConvoglio,
                        idConvoglioSelezionato,
                        onStazioneClick,
                        setPosizioneConvoglioStato,
                        setPosizioneConvoglio,
                        setStatoAutopilot,
                        sezioni
                    )}>
                        Salva
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default BottoneStatoPosizioneConvoglio;