import React from 'react';
import {Button} from 'react-bootstrap';
import {handleOnChangeCambioLuciLocomotiva} from "../../HandlersOnChange.tsx";
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";
import {StileBottone} from "../../../stile/StileBottone.ts";

interface BottoneCambioLuciProps {
    convoglioSelezionato: Convoglio;
    setConvogli: React.Dispatch<React.SetStateAction<Convoglio[]>>;
}

const BottoneCambioLuci: React.FC<BottoneCambioLuciProps> = ({convoglioSelezionato}) => {
    return (
        <div className="attributoBottoneComuneConvoglio clickable">
            <Button
                variant={convoglioSelezionato.luciAccese ? StileBottone.Attivo : StileBottone.NonAttivoOk}
                onClick={() => handleOnChangeCambioLuciLocomotiva(convoglioSelezionato)}
                size="sm"
            >
                {convoglioSelezionato.luciAccese ? "Luci: accese" : "Luci: spente"}
            </Button>
        </div>
    );
};

export default BottoneCambioLuci;