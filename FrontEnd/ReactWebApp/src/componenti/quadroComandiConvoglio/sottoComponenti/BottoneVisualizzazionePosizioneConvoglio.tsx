import {Button} from "react-bootstrap";
import {Convoglio} from "../../../model/convoglio/Convoglio.ts";
import {PosizioneConvoglioStato} from "../../../enumerazioni/PosizioneConvoglioStato.ts";
import {StileBottone} from "../../../stile/StileBottone.ts";

interface BottoneVisualizzaPosizioneConvoglioProps {
    convoglio: Convoglio;
    mostraSezioneOccupataDaConvoglio: Map<number, boolean>;
    setMostraSezioneOccupataDaConvoglio: React.Dispatch<React.SetStateAction<Map<number, boolean>>>;
    mostraSezioni: boolean;
    posizioneConvoglioStato: PosizioneConvoglioStato;
}

const BottoneVisualizzazionePosizioneConvoglio: React.FC<BottoneVisualizzaPosizioneConvoglioProps> = ({
                                                                                                          convoglio,
                                                                                                          mostraSezioneOccupataDaConvoglio,
                                                                                                          setMostraSezioneOccupataDaConvoglio,
                                                                                                          mostraSezioni,
                                                                                                          posizioneConvoglioStato
                                                                                                      }) => {

    const getVariant = (): string => {
        if (mostraSezioneOccupataDaConvoglio.get(convoglio.id)) {
            return StileBottone.Attivo;
        } else {
            return StileBottone.Normale;
        }
    }

    const getScritta = (): React.JSX.Element => {
        return (
            <>
                Visualizza <br/>posizione
            </>
        );
    }

    const handleOnClick = () => {
        setMostraSezioneOccupataDaConvoglio(prevMap => {
            const newMap = new Map(prevMap);
            newMap.set(convoglio.id, !newMap.get(convoglio.id));
            return newMap;
        });
    }

    // Vogliamo disabilitare il bottone se c'è un altro bottone attivo (ovvero se c'è un convoglio che sta mostrando la sezione occupata)
    const isBottoneDisabilitato = (): boolean => {
        // Se l'utente ha scelto di mostrare tutte le sezioni, allora non disabilitiamo il bottone
        if (mostraSezioni) {
            return true;
        }

        // Se la posizione del convoglio non è stata registrata, allora disabilitiamo il bottone
        if (!(posizioneConvoglioStato == PosizioneConvoglioStato.POSIZIONE_REGISTRATA)) {
            return true;
        }

        for (let [key, value] of mostraSezioneOccupataDaConvoglio.entries()) {
            if (value && key !== convoglio.id) {
                return true;
            }
        }
        return false;
    }

    return (
        <div className="attributoBottoneComuneConvoglio">
            <Button className={"text-left"} variant={getVariant()} size="sm" onClick={handleOnClick}
                    disabled={isBottoneDisabilitato()}>
                {getScritta()}
            </Button>
        </div>
    );
}

export default BottoneVisualizzazionePosizioneConvoglio;