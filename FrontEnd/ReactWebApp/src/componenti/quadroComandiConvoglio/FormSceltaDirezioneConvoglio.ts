import {PosizioneConvoglioStato} from "../../enumerazioni/PosizioneConvoglioStato.ts";
import {IdSezioneTipo} from "../../model/IdSezioneTipo.ts";
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {Sezione} from "../../model/Sezione.ts";

// Funzione che viene chiamata quando l'utente salva il form
export const handleFormSceltaDirezioneConvoglioSalvataggio = (
    direzioneSelezionata: 'destra' | 'sinistra' | null,
    setmostraFormSceltaDirezioneConvoglio: (value: boolean) => void,
    mostraStazioniAttive: (callback: (event: Event) => void, sezioni: Sezione[], idConvoglioSelezionato: number) => void,
    handleStazioneClick: (event: Event, idConvoglioSelezionato: number, onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void) => void,
    idConvoglioSelezionato: number,
    onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void,
    setPosizioneConvoglioStato: (value: PosizioneConvoglioStato) => void,
    setPosizioneConvogli: React.Dispatch<React.SetStateAction<{
        idConvoglio: number,
        idStazione: IdSezioneTipo,
        direzionePrimaLocomotivaDestra: boolean | undefined
    } | null>>,
    setStatoAutopilot: (value: StatoAutopilotFrontEnd) => void,
    sezioni: Sezione[]
) => {
    // Se l'utente ha selezionato una direzione procediamo, altrimenti non facciamo nulla
    if (direzioneSelezionata) {
        // Posso mostrare le stazioni attive
        // Devo passare handleStazioneClick a mostraStazioniAttive perché mostraStazioniAttive richiede una funzione di callback che verrà eseguita quando un evento di click si verifica su una stazione. La funzione handleStazioneClick è progettata per gestire questi eventi di click, estraendo l'ID della stazione cliccata e chiamando la funzione onStazioneClick con l'ID del convoglio selezionato e l'ID della stazione.
        mostraStazioniAttive((event) => handleStazioneClick(event, idConvoglioSelezionato, onStazioneClick), sezioni, idConvoglioSelezionato);
        // Imposto lo stato del convoglio in registrazione: ci manca la posizione del convoglio che l'utente deve inserire attraverso il sinottico
        setPosizioneConvoglioStato(PosizioneConvoglioStato.POSIZIONE_IN_REGISTRAZIONE);

        // Imposto l'autopilot in REGISTRAZIONE_POSIZIONE_CONVOGLIO_IN_CORSO, in questa maniera è possibile disattivare gli altri bottoni.
        setStatoAutopilot(StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLIO_IN_CORSO)

        console.log(`Direzione selezionata dall'utente: ${direzioneSelezionata}`);

        const idSezioneNonValido: IdSezioneTipo = {
            tipologia: 0,
            id: 0,
            specifica: 0
        };

        setPosizioneConvogli({
            idConvoglio: idConvoglioSelezionato,
            idStazione: idSezioneNonValido,
            direzionePrimaLocomotivaDestra: direzioneSelezionata === 'destra' ? true : direzioneSelezionata === 'sinistra' ? false : undefined
        });

        // Non mostriamo più il form
        setmostraFormSceltaDirezioneConvoglio(false);
    }
};

// Funzione che viene chiamata quando l'utente chiude il form
export const handleFormSceltaDirezioneConvoglioChiusura = (
    setmostraFormSceltaDirezioneConvoglio: (value: boolean) => void
) => {
    // Non mostriamo più il form
    setmostraFormSceltaDirezioneConvoglio(false);
};