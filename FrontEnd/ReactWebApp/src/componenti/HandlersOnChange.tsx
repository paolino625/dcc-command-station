import {
    invioEntitaAggiornataArduino
} from "../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/put/InvioEntitaAggiornataArduino.ts";
import {Convoglio} from "../model/convoglio/Convoglio.ts";

export const handleOnChangeCambioLuciLocomotiva = async (convoglio: Convoglio) => {
    invioEntitaAggiornataArduino("convoglio", convoglio.id, {luciAccese: !convoglio?.luciAccese});
}

