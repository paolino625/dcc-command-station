import {SensorePosizione} from "../../model/SensorePosizione.ts";
import logger from "../../utility/Logger.ts";
import {
    azionaSensoreRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/debugging/AzionaSensore.ts";

// Per semplicità, la logica per cui quando i sensori passano da true a false devono rimanere accesi per un tot secondi viene gestita direttamente durante l'aggiornamento del messaggio MQTT, quindi la logica non è sulla visualizzazione ma sui dati in memoria.

export const aggiornoStatoSensoriPosizione = (sensoriPosizione: SensorePosizione[], mostraSensoriPosizione: boolean) => {
    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        logger.error("Non ho trovato l'svg");
        return;
    }

    // Se i sensori di posizione devono essere mostrati
    if (mostraSensoriPosizione) {
        logger.info("Mostro i sensori di posizione");

        // Selezioniamo tutti i sensori di posizionare dal file svg
        const sensoriPosizioneSvg = svg.querySelectorAll('[id^="Sensore"]');

        sensoriPosizioneSvg.forEach((sensoreSvg) => {
            // Recupero l'id del sensore di posizione

            const sensorePosizioneIdStringa = sensoreSvg.id.split('-')[1];
            const sensorePosizioneIdNumber = parseInt(sensorePosizioneIdStringa);

            // Recupero il sensore di posizione corrispondente
            const sensorePosizione = sensoriPosizione.find((sensorePosizione) => sensorePosizione.id == sensorePosizioneIdNumber);

            if (sensorePosizione) {
                // In precedenza il sensore potrebbe essere stato nascosto, è necessario dunque rimuovere lo stile associato.
                sensoreSvg.classList.remove('sensoreNascosto');
                if (sensorePosizione.stato) {
                    // Se il sensore è in stato attivo allora lo mostro
                    sensoreSvg.classList.remove('sensoreInAttesa');
                    sensoreSvg.classList.remove('sensoreNonAttivo');
                    sensoreSvg.classList.add('sensoreAttivo');
                } else {
                    if (sensorePosizione.idConvoglioInAttesa != 0) {
                        // Se c'è un convoglio in attesa su quel sensore
                        sensoreSvg.classList.remove('sensoreAttivo');
                        sensoreSvg.classList.remove('sensoreNonAttivo');
                        sensoreSvg.classList.add('sensoreInAttesa');
                    } else {
                        // Se non c'è nessun convoglio in attesa su quel sensore
                        sensoreSvg.classList.remove('sensoreAttivo');
                        sensoreSvg.classList.remove('sensoreInAttesa');
                        sensoreSvg.classList.add('sensoreNonAttivo');
                    }

                }
            }
        });
    } else {
        logger.info("Nascondo i sensori di posizione");
        // Nascondo tutti i livelli dei sensori di posizione
        const sensoriSvg = svg.querySelectorAll('[id^="Sensore"]');
        sensoriSvg.forEach((sensoreSvg) => {
            sensoreSvg.classList.add('sensoreNascosto');
        });
    }
}

export const aggiornoCliccabilitaSensori = (sensori: SensorePosizione[], debuggingSensori: boolean, mostraSensori: boolean) => {
    logger.info("Aggiorno la cliccabilità dei sensori");

    const svg = document.querySelector('.svg-container svg');
    if (!svg) {
        logger.error("Non ho trovato l'svg");
        return;
    }

    const sensoriPosizioneSvg = svg.querySelectorAll('[id^="Sensore"]');
    sensoriPosizioneSvg.forEach((sensoreSvg) => {
        const sensoreIdStringa = sensoreSvg.id.split('-')[1];
        const sensoreIdNumber = parseInt(sensoreIdStringa);

        const sensore = sensori.find((sensore) => sensore.id == sensoreIdNumber);

        if (sensore) {
            if (debuggingSensori && mostraSensori) {
                sensoreSvg.classList.add('clickable');
                sensoreSvg.addEventListener('click', () => {
                    handleSensoreClick(sensore);
                });
            } else {
                sensoreSvg.classList.remove('clickable');
                const newElement = sensoreSvg.cloneNode(true);
                if (sensoreSvg.parentNode) {
                    sensoreSvg.parentNode.replaceChild(newElement, sensoreSvg);
                }
            }
        }
    });

}

const handleSensoreClick = (sensore: SensorePosizione) => {
    console.log(`Sensore cliccato: ${sensore.id}`);
    // Aziono due volte i sensori per simulare che sia passato un convoglio sopra il sensore (ovvero due locomotive)
    azionaSensoreRestCall(sensore.id);
    azionaSensoreRestCall(sensore.id);
}