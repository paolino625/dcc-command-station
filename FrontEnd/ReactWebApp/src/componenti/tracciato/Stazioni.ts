import logger from "../../utility/Logger.ts";
import {rimuovoTuttiEventListeners} from "../../utility/RimozioneEventListeners.ts";
import {Sezione} from "../../model/Sezione.ts";
import {estraggoIdSezioneTipoDaStringa} from "../../utility/EstrazioneIdSezioneDaElementiSvg.ts";
import {areIdSezioneTipoEqual, isSezioneStazione} from "../../model/IdSezioneTipo.ts";
import {
    esistePercorsoEntrambeDirezioni
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/EsistePercorsoEntrambeDirezioni.ts";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import {
    avvioAutopilotModalitaStazioneSuccessivaRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/postAvvio/InizializzoAutopilotConvoglioModalitaStazioneSuccessiva.ts";
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {
    fetchSezioniCompatibiliConvoglioModalitaAutopilotRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/get/FetchSezioniCompatibiliConvoglioModalitaAutopilot.ts";
import {
    fetchSezioniCompatibiliConvoglioModalitaManualeRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/get/fetchSezioniCompatibiliConvoglioModalitaManualeRestCall.ts";

export const mostroBinariStazioniLiberi = async (handleStazioneClickSceltaPosizioneInizialeConvoglio: (event: Event) => void, sezioni: Sezione[], idConvoglioSelezionato: number) => {
    logger.info("Mostro binari stazioni liberi");

    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        // logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    // Recupero le stazioni compatibili con il convoglio
    const sezioniCompatibiliConvoglio = await fetchSezioniCompatibiliConvoglioModalitaManualeRestCall(idConvoglioSelezionato);

    sezioniSvg.forEach(sezioneSvg => {
        const idSezione = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        // Prendo in considerazione solo le sezioni che sono di stazione
        if (isSezioneStazione(idSezione)) {

            // Capisco se la sezione presa in esame è compatibile con il convoglio
            const sezioneCompatibile = sezioniCompatibiliConvoglio.find((s: any) => areIdSezioneTipoEqual(s.id, idSezione));

            if (sezioneCompatibile) {

                // Individuo l'elemento nell'array sezioni corrispondente al binario di stazione preso in esame
                const sezione = sezioni.find((s: any) => s && areIdSezioneTipoEqual(s.id, idSezione));

                // Voglio mostrare la sezione solo se questa è libera
                if (sezione && !sezione.stato.statoLock) {
                    sezioneSvg.classList.add('binarioStazioneDisponibileECompatibileConvoglio');
                    sezioneSvg.classList.add('clickable');
                    sezioneSvg.addEventListener('click', handleStazioneClickSceltaPosizioneInizialeConvoglio);
                } else {
                    sezioneSvg.classList.add('binarioStazioneOccupato')
                }
            }
        }

    });
}

export const resetColoriBinariStazioni = () => {
    logger.info("Nascondo binari stazioni");

    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        // logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    sezioniSvg.forEach(sezioneSvg => {
        // Tolgo tutti gli stili che posso aver assegnato in precedenza
        sezioneSvg.classList.remove('binarioStazioneDisponibileECompatibileConvoglio');
        sezioneSvg.classList.remove('binarioStazioneNonCompatibileConvoglio');
        sezioneSvg.classList.remove('binarioStazioneOccupato');
        sezioneSvg.classList.remove('binarioStazioneNonRaggiungibile');
        sezioneSvg.classList.remove('binarioStazioneCorrente');
        sezioneSvg.classList.remove('clickable');
        // N.B. Avevo provato a rimuovere un event listener invece che tutti ma è più complicato del previsto
        rimuovoTuttiEventListeners(sezioneSvg);
    });
}

/*
Il parametro event rappresenta l'evento di click generato dall'utente.
Il parametro idConvoglioSelezionato è un numero o null, che rappresenta l'ID del convoglio selezionato.
Il parametro onStazioneClick è una funzione di callback che prende due argomenti: l'ID del convoglio selezionato e l'ID della stazione.
All'interno della funzione, event.currentTarget viene convertito in un Element e assegnato alla variabile stazione, che rappresenta l'elemento della stazione cliccato.
 */
export const handleClickStazioneSceltaPosizioneInizialeConvoglio = (
    event: Event,
    idConvoglioSelezionato: number,
    onStazioneClick: (idConvoglioSelezionato: number, stazioneId: string) => void
) => {
    const stazione = event.currentTarget as Element;
    const stazioneId = stazione.id;
    logger.info(`Stazione posizione iniziale convoglio cliccata: ${stazioneId}`);
    if (idConvoglioSelezionato !== null) {
        onStazioneClick(idConvoglioSelezionato, stazioneId);
    }
};

export const mostroBinariStazioniRaggiungibiliDaPosizioneCorrenteConvoglio = async (convoglio: Convoglio, sezioni: Sezione[], setStatoAutopilotFrontEnd: React.Dispatch<React.SetStateAction<StatoAutopilotFrontEnd>>) => {
    logger.info("Mostro binari delle stazioni raggiungibili dalla posizione corrente del convoglio");

    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        // logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    // Se effettuo le modifiche mano a mano, vedrei i binari cambiare colore uno alla volta.
    // Per evitare questo effetto, accumulo le modifiche da apportare in un oggetto Map.
    const modificheSvg = new Map<Element, string[]>();

    // Recupero le stazioni compatibili con il convoglio
    const sezioniCompatibiliConvoglio = await fetchSezioniCompatibiliConvoglioModalitaAutopilotRestCall(convoglio.id);

    for (const sezioneSvg of sezioniSvg) {
        const idSezione = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        // Prendo in considerazione solo le sezioni che sono di stazione
        if (isSezioneStazione(idSezione)) {
            logger.debug("Controllo raggiungibilità binario stazione: ", idSezione);

            if (idSezione) {
                // Individuo l'elemento nell'array sezioni corrispondente al binario di stazione preso in esame
                const sezione = sezioni.find((s: any) => s && areIdSezioneTipoEqual(s.id, idSezione));

                // Se la sezione selezionata è quella corrente del convoglio, la mostro in blu,
                if (areIdSezioneTipoEqual(idSezione, convoglio.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata)) {
                    if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                    modificheSvg.get(sezioneSvg)?.push('binarioStazioneCorrente');
                } else {
                    logger.debug("Controllo raggiungibilità binario stazione: ", idSezione);

                    // Verifico se il binario di stazione preso in esame è raggiungibile dalla posizione corrente del convoglio
                    const percorsoEsiste = await esistePercorsoEntrambeDirezioni(convoglio.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata, idSezione);

                    if (percorsoEsiste) {
                        logger.debug(`Il percorso esiste tra la sezione di partenza ${JSON.stringify(convoglio.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata)} e la sezione di arrivo ${JSON.stringify(idSezione)}.`);

                        // Capisco se la sezione presa in esame è compatibile con il convoglio
                        const sezioneCompatibile = sezioniCompatibiliConvoglio.find((s: any) => areIdSezioneTipoEqual(s.id, idSezione));

                        if (sezioneCompatibile) {
                            logger.debug("La sezione è compatibile con il convoglio");
                            // Voglio mostrare la sezione solo se questa è libera
                            if (sezione && !sezione.stato.statoLock) {
                                if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                                modificheSvg.get(sezioneSvg)?.push('binarioStazioneDisponibileECompatibileConvoglio', 'clickable');
                                sezioneSvg.addEventListener('click', (event) => handleClickStazioneDestinazioneConvoglio(event, convoglio.id, setStatoAutopilotFrontEnd));
                            } else {
                                if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                                modificheSvg.get(sezioneSvg)?.push('binarioStazioneOccupato');
                            }
                        } else {
                            logger.debug("La sezione non è compatibile con il convoglio");
                            // Se la sezione non può ospitare il convoglio assumerà un altro colore
                            if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                            modificheSvg.get(sezioneSvg)?.push('binarioStazioneNonCompatibileConvoglio');
                        }
                    } else {
                        logger.info(`Il percorso non esiste tra la sezione di partenza ${JSON.stringify(convoglio.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata)} e la sezione di arrivo ${JSON.stringify(idSezione)}.`);
                        if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                        modificheSvg.get(sezioneSvg)?.push('binarioStazioneNonRaggiungibile');
                    }
                }
            }
        }
    }

    // Applico tutte le modifiche in una volta sola
    modificheSvg.forEach((classes, sezioneSvg) => {
        classes.forEach(className => sezioneSvg.classList.add(className));
    });
}

export const mostroBinariStazioniCompatibiliConvoglio = async (convoglio: Convoglio) => {
    logger.info("Mostro i binari delle stazioni compatibili con il convoglio");

    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        // logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    // Se effettuo le modifiche mano a mano, vedrei i binari cambiare colore uno alla volta.
    // Per evitare questo effetto, accumulo le modifiche da apportare in un oggetto Map.
    const modificheSvg = new Map<Element, string[]>();

    // Recupero le stazioni compatibili con il convoglio
    const sezioniCompatibiliConvoglio = await fetchSezioniCompatibiliConvoglioModalitaManualeRestCall(convoglio.id);

    for (const sezioneSvg of sezioniSvg) {
        const idSezione = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        // Prendo in considerazione solo le sezioni che sono di stazione
        if (isSezioneStazione(idSezione)) {
            logger.debug("Controllo compatibilità binario stazione: ", idSezione);

            if (idSezione) {
                // Individuo l'elemento nell'array sezioni corrispondente al binario di stazione preso in esame
                logger.debug("Controllo compatibilità binario stazione: ", idSezione);

                // Capisco se la sezione presa in esame è compatibile con il convoglio
                const sezioneCompatibile = sezioniCompatibiliConvoglio.find((s: any) => areIdSezioneTipoEqual(s.id, idSezione));

                if (sezioneCompatibile) {
                    logger.debug("La sezione è compatibile con il convoglio");
                    // Voglio mostrare la sezione solo se questa è libera

                    if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                    modificheSvg.get(sezioneSvg)?.push('binarioStazioneDisponibileECompatibileConvoglio', 'clickable');
                } else {
                    logger.debug("La sezione non è compatibile con il convoglio");
                    // Se la sezione non può ospitare il convoglio assumerà un altro colore
                    if (!modificheSvg.has(sezioneSvg)) modificheSvg.set(sezioneSvg, []);
                    modificheSvg.get(sezioneSvg)?.push('binarioStazioneNonCompatibileConvoglio');
                }
            }

        }
    }

    // Applico tutte le modifiche in una volta sola
    modificheSvg.forEach((classes, sezioneSvg) => {
        classes.forEach(className => sezioneSvg.classList.add(className));
    });
}

export const handleClickStazioneDestinazioneConvoglio = async (
    event: Event,
    idConvoglioSelezionato: number,
    setStatoAutopilotFrontEnd: React.Dispatch<React.SetStateAction<StatoAutopilotFrontEnd>>
) => {
    const stazione = event.currentTarget as Element;
    const stazioneIdSvg = stazione.id;
    const stazioneId = estraggoIdSezioneTipoDaStringa(stazioneIdSvg);

    logger.info(`Stazione destinazione convoglio cliccata: ${stazioneId}`);

    if (stazioneId) {
        if (idConvoglioSelezionato !== null) {
            try {
                await avvioAutopilotModalitaStazioneSuccessivaRestCall(idConvoglioSelezionato, stazioneId);
                resetColoriBinariStazioni();
                setStatoAutopilotFrontEnd(StatoAutopilotFrontEnd.ATTIVO);
            } catch (error) {
                logger.error("Errore durante la chiamata avvioAutopilotModalitaStazioneSuccessivaRestCall: ", error);
            }
        }
    } else {
        logger.error("ID della stazione non valido");
    }
};