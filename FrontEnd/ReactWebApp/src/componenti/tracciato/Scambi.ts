import logger from '../../utility/Logger.ts';
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {rimuovoTuttiEventListeners} from "../../utility/RimozioneEventListeners.ts";
import {DirezioneScambio, direzioneScambioToString} from "../../enumerazioni/DirezioneScambio.ts";
import {Scambio} from "../../model/scambio/Scambio.ts";
import {
    invioEntitaAggiornataArduino
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/put/InvioEntitaAggiornataArduino.ts";
import {listaCoppieScambi} from "../../altro/CoppieScambi.ts";
import {PosizioneScambio} from "../../model/scambio/PosizioneScambio.ts";
import {estraggoIdSezioneTipoDaStringa} from "../../utility/EstrazioneIdSezioneDaElementiSvg.ts";
import {
    convertoSezioneInStringa,
    IdSezioneTipo,
    isSezioneScambio,
    isSezioneScambioSinistra
} from "../../model/IdSezioneTipo.ts";

const handleSinistraClick = (scambioIdNumero: number) => () => handleClickScambio(scambioIdNumero, DirezioneScambio.SINISTRA);
const handleDestraClick = (scambioIdNumero: number) => () => handleClickScambio(scambioIdNumero, DirezioneScambio.DESTRA);

export const aggiornoCliccabilitaEListenerScambi = (statoAutopilot: StatoAutopilotFrontEnd, debuggingSezioni: boolean) => {
    logger.info("Aggiorno cliccabilità e listener degli scambi");
    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    sezioniSvg.forEach(sezioneSvg => {

        const idSezione = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        if (idSezione) {

            // Prendo in considerazione solo le sezioni che sono degli scambi
            if (isSezioneScambio(idSezione)) {
                const scambioSinistra = isSezioneScambioSinistra(idSezione);

                // Se l'autopilot non è attivo e il debugging delle sezioni non è inserito, allora rendo cliccabili gli scambi.
                // Durante il debugging delle sezioni, il click degli scambi deve essere disattivato altrimenti va in conflitto con il click che l'utente può fare sulle sezioni per effettuare il lock/unlock delle sezioni
                if (statoAutopilot === StatoAutopilotFrontEnd.NON_ATTIVO && !debuggingSezioni) {
                    sezioneSvg.classList.add('clickable');
                    sezioneSvg.addEventListener('click', scambioSinistra ? handleSinistraClick(idSezione.id) : handleDestraClick(idSezione.id));
                } else {
                    sezioneSvg.classList.remove('clickable');
                    // N.B. Avevo provato a rimuovere un event listener invece che tutti ma è più complicato del previsto
                    rimuovoTuttiEventListeners(sezioneSvg);
                }
            }
        }
    });

}

export const aggiornoColoriScambi = (scambi: Scambio[]) => {
    logger.info("Aggiorno colori scambi");
    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        // logger.error("Non ho trovato l'svg");
        return;
    }

    scambi.forEach((scambio) => {
            const scambioId = scambio.id;

            const idSezioneScambioSinistra: IdSezioneTipo = {
                tipologia: 3, // Gli scambi hanno tipologia 3
                id: scambioId,
                specifica: 1 // Lo scambio è di sinistra
            };

            const idSezioneScambioDestra: IdSezioneTipo = {
                tipologia: 3, // Gli scambi hanno tipologia 3
                id: scambioId,
                specifica: 2 // Lo scambio è di destra
            };

            const idSezioneScambioSinistraStringa = convertoSezioneInStringa(idSezioneScambioSinistra);
            const idSezioneScambioDestraStringa = convertoSezioneInStringa(idSezioneScambioDestra);

            // In CSS i punti non sono considerati caratteri validi, quindi è necessario effettuare l'escape
            const scambioSinistraSvg = svg.querySelector(`#Sezione-${idSezioneScambioSinistraStringa.replace(/\./g, '\\.')}`);
            const scambioDestraSvg = svg.querySelector(`#Sezione-${idSezioneScambioDestraStringa.replace(/\./g, '\\.')}`);
            if (scambioSinistraSvg && scambioDestraSvg) {

                if (scambio.posizione == PosizioneScambio.SINISTRA) {

                    // Aggiorno stile scambio sinistra
                    scambioSinistraSvg.classList.add('scambioAttivo');
                    scambioSinistraSvg.classList.remove('scambioInattivo');

                    // Aggiorno stile scambio destra
                    scambioDestraSvg.classList.add('scambioInattivo');
                    scambioDestraSvg.classList.remove('scambioAttivo');

                    // Aggiorno livelli scambio
                    portoScambioAttivoInPrimoPiano(scambioSinistraSvg);


                } else {
                    // Aggiorno stile scambio sinistra
                    scambioSinistraSvg.classList.remove('scambioAttivo');
                    scambioSinistraSvg.classList.add('scambioInattivo');

                    // Aggiorno stile scambio destra
                    scambioDestraSvg.classList.remove('scambioInattivo');
                    scambioDestraSvg.classList.add('scambioAttivo');

                    // Aggiorno livelli scambio
                    portoScambioAttivoInPrimoPiano(scambioDestraSvg);
                }
            } else {
                // TODO RIABILITARE QUANDO L'SVG AVRA' TUTTI GLI SCAMBI. Altrimenti inquina i log.
                // logger.warn(`UpdateSvgColors: Non ho trovato destra e sinistra dello scambio ${index}`);
            }

        }
    );

}

const portoScambioAttivoInPrimoPiano = (element: Element) => {
    const parent = element.parentNode;
    if (parent) {
        parent.appendChild(element);
    }
};

// Funzione che gestisce il click su uno scambio
export const handleClickScambio = async (idScambio: number, direzioneScambio: DirezioneScambio) => {
    logger.info(`Scambio ${idScambio} direzione ${direzioneScambioToString(direzioneScambio)} azionato!`);

    // Devo inviare lo stato dello scambio ad Arduino
    const richiestaRest = invioEntitaAggiornataArduino("scambio", idScambio, {stato: direzioneScambioToString(direzioneScambio)});

    // N.B. Non aggiorno scambi, aspetto che Arduino mandi un messaggio MQTT con i nuovi dati dello scambio, in questo modo siamo sicuri che lo scambio sia effettivamente cambiato quando aggiorniamo la UI

    // Devo controllare se lo scambio selezionato fa parte di una coppia di scambi: in questo caso è necessario
    // cambiare anche lo stato dell'altro scambio della coppia

    const coppiaScambi = listaCoppieScambi.find(coppia => coppia.includes(idScambio));
    if (coppiaScambi) {
        const altroScambio = coppiaScambi.find(scambio => scambio !== idScambio);
        if (altroScambio) {
            logger.info(`Lo scambio selezionato fa parte di una coppia di scambi. Cambio anche lo stato dell'altro scambio con id ${altroScambio}.`);

            // Prima di inviare la seconda richiesta REST, aspetto che la richiesta precedente sia stata completata.
            // In questo modo evito di inviare due richieste contemporaneamente ad Arduino che non riuscirebbe a gestire.
            await richiestaRest;

            // La direzione dell'altro scambio è la stessa direzione dello scambio dove l'utente ha cliccato
            invioEntitaAggiornataArduino("scambio", altroScambio, {stato: direzioneScambioToString(direzioneScambio)});
        }
    }
};