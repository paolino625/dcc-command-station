import logger from "../../utility/Logger.ts";
import {estraggoIdSezioneTipoDaStringa} from "../../utility/EstrazioneIdSezioneDaElementiSvg.ts";
import {Sezione} from "../../model/Sezione.ts";
import {areIdSezioneTipoEqual, convertoSezioneInStringa, isSezioneScambio} from "../../model/IdSezioneTipo.ts";
import {
    lockSezioneRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/debugging/sezioni/LockSezione.ts";
import {
    unlockSezioneRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/debugging/sezioni/UnlockSezione.ts";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";

export const mostroStatoSezioni = (sezioni: Sezione[], mostraSezioni: boolean) => {
    logger.info("Mostro stato sezioni");

    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    sezioniSvg.forEach(sezioneSvg => {
        const idSezione = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        const sezione = sezioni.find(sezione => areIdSezioneTipoEqual(sezione.id, idSezione));

        if (mostraSezioni) {
            if (sezione?.stato.statoLock === false) {
                sezioneSvg.classList.add('sezioneLibera');
                sezioneSvg.classList.remove('sezioneOccupata');
            } else {
                sezioneSvg.classList.add('sezioneOccupata');
                sezioneSvg.classList.remove('sezioneLibera');
            }
        } else {
            // Se la modalità debugging sezioni non è abilitata, allora rimuovo tutti gli stili
            sezioneSvg.classList.remove('sezioneOccupata');
            sezioneSvg.classList.remove('sezioneLibera');
        }
    });
};

export const aggiornoCliccabilitaSezioni = (sezioni: Sezione[], debuggingSezioni: boolean, mostraSezioni: boolean) => {
    logger.info("Aggiorno la cliccabilità delle sezioni");

    const svg = document.querySelector('.svg-container svg');
    // Se l'svg non è stato ancora caricato, non faccio nulla
    if (!svg) {
        logger.error("Non ho trovato l'svg");
        return;
    }

    // Selezioniamo tutte le sezioni del file svg
    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    sezioniSvg.forEach(sezioneSvg => {
        const idSezione = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        const sezione = sezioni.find(sezione => areIdSezioneTipoEqual(sezione.id, idSezione));

        if (sezione) {

            if (debuggingSezioni && mostraSezioni) {
                // Cloniamo l'elemento sezioneSvg per rimuovere eventuali listener di eventi precedenti.
                const newElement = sezioneSvg.cloneNode(true) as Element;
                sezioneSvg.replaceWith(newElement);

                if (sezione.stato.statoLock) {
                    // Se lo stato è occupato, allora nel caso in cui venga cliccato devo effettuare l'unlock
                    newElement.classList.add('clickable');
                    newElement.addEventListener('click', () => handleSezioneUnlockClick(sezione));
                } else {
                    // Se lo stato è libero, allora nel caso in cui venga cliccato devo effettuare il lock
                    newElement.classList.add('clickable');
                    newElement.addEventListener('click', () => handleSezioneLockClick(sezione));
                }


            } else {
                // Se la sezione è di scambio, ci penserà la funzione aggiornoCliccabilitaEListenerScambi a gestire la cliccabilità.
                // Se rimuovo questo if, la funzione aggiornoCliccabilitaEListenerScambi viene avviata prima ma poi questa funzione toglie i clickable aggiunti
                if (!isSezioneScambio(sezione.id)) {
                    sezioneSvg.classList.remove('clickable');
                    const newElement = sezioneSvg.cloneNode(true);
                    if (sezioneSvg.parentNode) {
                        sezioneSvg.parentNode.replaceChild(newElement, sezioneSvg);
                    }
                }

            }
        }
    });
}

const handleSezioneLockClick = (sezione: Sezione) => {
    logger.info(`Sezione cliccata: ${convertoSezioneInStringa(sezione.id)}`);
    lockSezioneRestCall(sezione.id);
}

const handleSezioneUnlockClick = (sezione: Sezione) => {
    logger.info(`Sezione cliccata: ${convertoSezioneInStringa(sezione.id)}`);
    unlockSezioneRestCall(sezione.id);
}

export const resetColoriSezioni = () => {
    logger.info("Nascondo sezioni occupate");

    const svg = document.querySelector('.svg-container svg');
    if (!svg) {
        return;
    }

    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    sezioniSvg.forEach(sezioneSvg => {
        sezioneSvg.classList.remove('sezioneOccupata');
    });
}

export const mostroSezioniOccupateDaConvoglio = (convoglio: Convoglio, sezioni: Sezione[]) => {
    logger.info(`Mostro sezioni occupate dal convoglio con ID: ${convoglio.id}`);

    const svg = document.querySelector('.svg-container svg');
    if (!svg) {
        return;
    }

    const sezioniSvg = svg.querySelectorAll('[id^="Sezione"]');

    sezioniSvg.forEach(sezioneSvg => {
        // Estraggo l'ID della sezione dal suo ID SVG
        const sezioneId = estraggoIdSezioneTipoDaStringa(sezioneSvg.id);

        // Recupero la sezione corrispondente all'ID estratto
        const sezione = sezioni.find(s => s && areIdSezioneTipoEqual(s.id, sezioneId));

        // Solo se la sezione risulta occupata e il convoglio corrisponde a quello passato come parametro, allora coloro la sezione
        if (sezione && sezione.stato.statoLock && sezione.stato.idConvoglio === convoglio.id) {
            sezioneSvg.classList.add('sezioneOccupata');
        } else {
            sezioneSvg.classList.remove('sezioneOccupata');
        }
    });
}

