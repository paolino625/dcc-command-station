import {Button} from "react-bootstrap";
import {StileBottone} from "../../stile/StileBottone.ts";

interface DebuggingSensoriButtonProps {
    debuggingSensori: boolean;
    setDebuggingSensori: React.Dispatch<React.SetStateAction<boolean>>;
    setMostraSensori: React.Dispatch<React.SetStateAction<boolean>>;
}

const BottoneDebuggingSensori: React.FC<DebuggingSensoriButtonProps> = ({
                                                                            debuggingSensori,
                                                                            setDebuggingSensori,
                                                                            setMostraSensori
                                                                        }) => {
    const handleClick = () => {
        setDebuggingSensori(!debuggingSensori);
        // Quando si attiva il debugging dei sensori, si attiva anche la visualizzazione dei sensori e viceversa
        if (!debuggingSensori) {
            setMostraSensori(true);
        } else {
            setMostraSensori(false);
        }
    };

    const getVariant = (): string => {
        return debuggingSensori ? StileBottone.Attivo : StileBottone.NonAttivoOk;
    };

    const getScritta = (): string => {
        return 'Debugging sensori';
    };

    return (
        <Button
            variant={getVariant()}
            size="sm"
            onClick={handleClick}
            className="debugging-sensori-button"
            style={{width: '100%'}}
        >
            {getScritta()}
        </Button>
    );
};

export default BottoneDebuggingSensori;