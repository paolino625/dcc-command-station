import React from 'react';
import {Button} from 'react-bootstrap';

interface BottoneStopEmergenzaProps {
    stopEmergenzaConvogli: () => void;
}

const BottoneStopEmergenza: React.FC<BottoneStopEmergenzaProps> = ({stopEmergenzaConvogli}) => {
    const handleClick = () => {
        stopEmergenzaConvogli();
    };

    const getVariant = (): string => {
        return 'danger';
    };

    const getText = (): string => {
        return 'STOP EMERGENZA';
    };

    return (
        <Button
            variant={getVariant()}
            size="lg"
            onClick={handleClick}
            className="stop-emergenza-button"
            style={{width: '100%'}}
        >
            {getText()}
        </Button>
    );
};

export default BottoneStopEmergenza;