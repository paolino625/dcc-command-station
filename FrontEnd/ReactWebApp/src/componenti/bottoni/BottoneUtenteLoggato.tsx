import React from 'react';
import {Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import {Utente} from "../../model/Utente.ts";
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneLogoutProps {
    setUtenteAutenticato: React.Dispatch<React.SetStateAction<Utente | null>>;
    utenteAutenticato: Utente | null;
}

const BottoneLogout: React.FC<BottoneLogoutProps> = ({setUtenteAutenticato, utenteAutenticato}) => {
    const navigate = useNavigate();

    const handleClick = () => {
        localStorage.removeItem('utenteLoggato');
        setUtenteAutenticato(null);
        navigate('/login');
    };

    const getVariant = (): string => {
        return StileBottone.Normale;
    };

    const getText = (): string => {
        return `Utente: ${utenteAutenticato ? utenteAutenticato.username : ''}`;
    };

    return (
        <Button
            variant={getVariant()}
            size="lg"
            onClick={handleClick}
            style={{width: '100%'}}
        >
            {getText()}
        </Button>
    );
};

export default BottoneLogout;