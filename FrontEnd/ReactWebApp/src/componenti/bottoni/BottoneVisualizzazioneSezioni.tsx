import {Button} from "react-bootstrap";
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneVisualizzazioneSezioniProps {
    mostraSezioni: boolean;
    setMostraSezioni: React.Dispatch<React.SetStateAction<boolean>>;
    mostraSezioniOccupateDaConvoglio: Map<number, boolean>;
    mostraSezioneOccupataDaConvoglio: Map<number, boolean>;
}

export const BottoneVisualizzazioneSezioni: React.FC<BottoneVisualizzazioneSezioniProps> = ({
                                                                                                mostraSezioni,
                                                                                                setMostraSezioni,
                                                                                                mostraSezioniOccupateDaConvoglio,
                                                                                                mostraSezioneOccupataDaConvoglio
                                                                                            }) => {
    const handleClick = () => {
        if (mostraSezioni) {
            setMostraSezioni(false);
        } else {
            setMostraSezioni(true);
        }
    }

    const getVariant = (): string => {
        if (mostraSezioni) {
            return StileBottone.Attivo;
        }
        return StileBottone.NonAttivoOk;
    }

    const getScritta = (): string => {
        return "Sezioni";
    }

    // Vogliamo disabilitare il bottone se è in corso la visualizzazione di sezioni occupate da un convoglio
    const isBottoneDisabilitato = (): boolean => {
        for (let value of mostraSezioniOccupateDaConvoglio.values()) {
            if (value) {
                return true;
            }
        }

        for (let value of mostraSezioneOccupataDaConvoglio.values()) {
            if (value) {
                return true;
            }
        }

        return false;
    }

    return (
        <div className="bottoneSezioni clickable">
            <Button variant={getVariant()} size="sm" onClick={handleClick} disabled={isBottoneDisabilitato()}>
                {getScritta()}
            </Button>
        </div>
    )
}