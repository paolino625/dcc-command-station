// Function to toggle the autopilot visibility
import {StatoAutopilotFrontEnd} from "../../enumerazioni/StatoAutopilotFrontEnd.ts";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import React from "react";
import {Button} from "react-bootstrap";
import {ModalitaAutopilot} from "../../enumerazioni/ModalitaAutopilot.ts";
import {
    avvioAutopilotModalitaStazioneSuccessivaRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/avvio/AvvioAutopilotModalitaStazioneSuccessiva.ts";
import {
    fermoAutopilotRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/FermaAutopilot.ts";
import {
    avvioAutopilotModalitaApproccioCasualeRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/avvio/AvvioAutopilotModalitaApproccioCasuale.ts";
import {
    avvioAutopilotModalitaApproccioCasualeSceltaConvogliRestCall
} from "../../comunicazione/restApi/arduinoMasterHelper/arduinoProxy/post/autopilot/avvio/AvvioAutopilotModalitaApproccioCasualeSceltaConvogli.ts";
import {StileBottone} from "../../stile/StileBottone.ts";

interface AutopilotButtonProps {
    statoAutopilotFrontEnd: StatoAutopilotFrontEnd;
    modalitaAutopilotFrontEnd: ModalitaAutopilot,
    convogli: Convoglio[];
    setMostraFormSceltaModalitaAutopilot: (value: boolean) => void;
    setStatoAutopilotFrontEnd: (value: StatoAutopilotFrontEnd) => void;
    ottimizzazioneAssegnazioneConvogliAiBinariAbilitata: boolean;
    autopilotAttivatoAutopilotModalitaApproccioCasuale: { [idConvoglio: number]: boolean };
}

const BottoneAutopilot: React.FC<AutopilotButtonProps> = ({
                                                              statoAutopilotFrontEnd,
                                                              modalitaAutopilotFrontEnd,
                                                              convogli,
                                                              setMostraFormSceltaModalitaAutopilot,
                                                              setStatoAutopilotFrontEnd,
                                                              ottimizzazioneAssegnazioneConvogliAiBinariAbilitata,
                                                              autopilotAttivatoAutopilotModalitaApproccioCasuale,
                                                          }) => {
    const ottengoTestoBottone = (statoAutopilot: StatoAutopilotFrontEnd): React.ReactNode => {
        const autopilotText = <strong>AUTOPILOT</strong>;

        switch (statoAutopilot) {
            case StatoAutopilotFrontEnd.NON_ATTIVO:
                return (
                    <>
                        {autopilotText} - OFF
                    </>
                );
            case StatoAutopilotFrontEnd.SCELTA_MODALITA:
                return (
                    <>
                        {autopilotText}<br/>Scelta modalità
                    </>
                );
            case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI:
                return (
                    <>
                        {autopilotText}<br/>Registrazione<br/>posizione<br/>convogli
                    </>
                );
            case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLIO_IN_CORSO:
                return (
                    <>
                        {autopilotText}<br/>Registrazione<br/>posizione<br/>convoglio
                    </>
                );
            case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA:
                return (
                    <>
                        {autopilotText}<br/>Registrazione<br/>posizione<br/>convogli<br/>completata
                    </>
                );
            case StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO:
                return (
                    <>
                        {autopilotText}<br/>Registrazione<br/>stazione<br/>destinazione
                    </>
                );
            case StatoAutopilotFrontEnd.REGISTRAZIONE_CONVOGLI_SCELTA_AUTOPILOT_MODALITA_APPROCCIO_CASUALE:
                return (
                    <>
                        {autopilotText}<br/>Scelta convogli
                    </>
                );
            case StatoAutopilotFrontEnd.TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO:
                return (
                    <>
                        {autopilotText}<br/>Disattivazione
                    </>
                );
            case StatoAutopilotFrontEnd.ATTIVO:
                return (
                    <>
                        {autopilotText} - ON<br/>
                        {modalitaAutopilotFrontEnd === ModalitaAutopilot.STAZIONE_SUCCESSIVA ? "Stazione successiva" : "Approccio casuale"}
                    </>
                );
            default:
                return (
                    <>
                        Stato sconosciuto
                    </>
                );
        }
    };

    const getVariant = (statoAutopilotFrontEnd: StatoAutopilotFrontEnd): string => {
        switch (statoAutopilotFrontEnd) {
            case StatoAutopilotFrontEnd.NON_ATTIVO:
                return StileBottone.NonAttivoOk;
            case StatoAutopilotFrontEnd.SCELTA_MODALITA:
                return StileBottone.Warning;
            case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI:
                return StileBottone.Warning;
            case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLIO_IN_CORSO:
                return StileBottone.Warning;
            case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA:
                return StileBottone.Attivo;
            case StatoAutopilotFrontEnd.REGISTRAZIONE_STAZIONE_DESTINAZIONE_CONVOGLIO:
                return StileBottone.Warning;
            case StatoAutopilotFrontEnd.REGISTRAZIONE_CONVOGLI_SCELTA_AUTOPILOT_MODALITA_APPROCCIO_CASUALE:
                return StileBottone.Warning;
            case StatoAutopilotFrontEnd.TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO:
                return StileBottone.Warning;
            case StatoAutopilotFrontEnd.ATTIVO:
                return StileBottone.Attivo;
            default:
                return StileBottone.Errore;
        }
    };

    return (
        <Button
            variant={getVariant(statoAutopilotFrontEnd)}
            onClick={() => handleAutopilotBottone(
                statoAutopilotFrontEnd,
                modalitaAutopilotFrontEnd,
                convogli,
                setMostraFormSceltaModalitaAutopilot,
                setStatoAutopilotFrontEnd,
                ottimizzazioneAssegnazioneConvogliAiBinariAbilitata,
                autopilotAttivatoAutopilotModalitaApproccioCasuale,
            )}
            className="autopilot-button"
        >

            {ottengoTestoBottone(statoAutopilotFrontEnd)}
        </Button>
    );
};

export default BottoneAutopilot;

export const handleAutopilotBottone = (
    statoAutopilotFrontEnd: StatoAutopilotFrontEnd,
    modalitaAutopilotFrontEnd: ModalitaAutopilot,
    convogli: Convoglio[],
    setMostraFormSceltaModalitaAutopilot: (value: boolean) => void,
    setStatoAutopilot: (value: StatoAutopilotFrontEnd) => void,
    ottimizzazioneAssegnazioneConvogliAiBinariAbilitata: boolean,
    autopilotAttivatoAutopilotModalitaApproccioCasuale: {
        [idConvoglio: number]: boolean,
    }
) => {
    switch (statoAutopilotFrontEnd) {
        case StatoAutopilotFrontEnd.NON_ATTIVO:
            const tuttiConvogliFermi = convogli.every(convoglio => convoglio.velocitaAttuale === 0 && convoglio.velocitaImpostata === 0);

            if (tuttiConvogliFermi) {
                setMostraFormSceltaModalitaAutopilot(true);

            } else {
                alert("Tutti i convogli devono essere fermi per abilitare l'autopilot.");
            }
            break;

        case StatoAutopilotFrontEnd.REGISTRAZIONE_POSIZIONE_CONVOGLI_COMPLETATA:
            const confermaAttivazione = window.confirm("Vuoi attivare l'autopilot?");
            if (confermaAttivazione) {

                // In base alla modalità dell'autopilot da attivare effettuo la chiamata REST corrispondente.
                switch (modalitaAutopilotFrontEnd) {
                    // In base alla modalità dell'autopilot da attivare effettuo la chiamata REST corrispondente.
                    case ModalitaAutopilot.STAZIONE_SUCCESSIVA:

                        avvioAutopilotModalitaStazioneSuccessivaRestCall();

                        // Non setto io lo stato dell'autopilot a Front-End, ma aspetto la risposta da Arduino

                        break;

                    case ModalitaAutopilot.APPROCCIO_CASUALE_TUTTI_CONVOGLI:
                        avvioAutopilotModalitaApproccioCasualeRestCall(ottimizzazioneAssegnazioneConvogliAiBinariAbilitata);

                        // Non setto io lo stato dell'autopilot a Front-End, ma aspetto la risposta da Arduino

                        break;

                }

            } else {
                const confermaDisattivazione = window.confirm("Allora vuoi disabilitare l'autopilot?");
                if (confermaDisattivazione) {
                    // In questo caso non ho ancora inviato la chiamata ad Arduino per attivare ufficialmente l'autopilot, quindi è inutile passare per lo stato IN_DISATTIAZIONE.
                    setStatoAutopilot(StatoAutopilotFrontEnd.NON_ATTIVO);
                }
            }
            break;
        case StatoAutopilotFrontEnd.REGISTRAZIONE_CONVOGLI_SCELTA_AUTOPILOT_MODALITA_APPROCCIO_CASUALE:
            // Mi assicuro che almeno un convoglio sia stato selezionato prima di attivare l'autopilot
            const almenoUnConvoglioSelezionato = Object.values(autopilotAttivatoAutopilotModalitaApproccioCasuale).some(value => value);

            if (almenoUnConvoglioSelezionato) {
                const conferma = window.confirm("Sei sicuro di voler abilitare l'autopilot?");
                if (conferma) {
                    // Avvio l'autopilot in modalità approccio casuale solo per i convogli selezionati
                    avvioAutopilotModalitaApproccioCasualeSceltaConvogliRestCall(ottimizzazioneAssegnazioneConvogliAiBinariAbilitata, autopilotAttivatoAutopilotModalitaApproccioCasuale);
                }

                // Non setto io lo stato dell'autopilot a Front-End, ma aspetto la risposta da Arduino
                break;
            } else {
                alert("Seleziona almeno un convoglio per attivare l'autopilot.");
            }
            break;

        case StatoAutopilotFrontEnd.ATTIVO:
            const conferma = window.confirm("Sei sicuro di voler disabilitare l'autopilot?");
            if (conferma) {
                fermoAutopilotRestCall();
            }
            break;

        default:
            // In tutti gli altri casi non faccio nulla
            break
    }
};




