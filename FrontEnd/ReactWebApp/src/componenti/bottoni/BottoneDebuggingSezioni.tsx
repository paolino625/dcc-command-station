import React from 'react';
import {Button} from 'react-bootstrap';
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneDebuggingSezioniProps {
    debuggingSezioni: boolean;
    setDebuggingSezioni: React.Dispatch<React.SetStateAction<boolean>>;
    setMostraSezioni: React.Dispatch<React.SetStateAction<boolean>>;
    mostraSezioniOccupateDaConvoglio: Map<number, boolean>;
    mostraSezioneOccupataDaConvoglio: Map<number, boolean>;
}

const BottoneDebuggingSezioni: React.FC<BottoneDebuggingSezioniProps> = ({
                                                                             debuggingSezioni,
                                                                             setDebuggingSezioni,
                                                                             setMostraSezioni,
                                                                             mostraSezioniOccupateDaConvoglio,
                                                                             mostraSezioneOccupataDaConvoglio
                                                                         }) => {
    const handleClick = () => {
        setDebuggingSezioni(!debuggingSezioni);
        // Quando si attiva il debugging delle sezioni, si attiva anche la visualizzazione delle sezioni e viceversa
        if (!debuggingSezioni) {
            setMostraSezioni(true);
        } else {
            setMostraSezioni(false);
        }
    };

    const getVariant = (): string => {
        return debuggingSezioni ? StileBottone.Attivo : StileBottone.NonAttivoOk;
    };

    const getText = (): string => {
        return 'Debugging sezioni';
    };

    // Vogliamo disabilitare il bottone se è in corso la visualizzazione di sezioni occupate da un convoglio
    const isBottoneDisabilitato = (): boolean => {
        for (let value of mostraSezioniOccupateDaConvoglio.values()) {
            if (value) {
                return true;
            }
        }

        for (let value of mostraSezioneOccupataDaConvoglio.values()) {
            if (value) {
                return true;
            }
        }

        return false;
    }

    return (
        <Button
            variant={getVariant()}
            size="sm"
            onClick={handleClick}
            disabled={isBottoneDisabilitato()}
            className="debugging-sezioni-button"
            style={{width: '100%'}}
        >
            {getText()}
        </Button>
    );
};

export default BottoneDebuggingSezioni;