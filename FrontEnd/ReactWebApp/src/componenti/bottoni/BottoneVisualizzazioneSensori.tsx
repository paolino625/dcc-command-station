import {Button} from "react-bootstrap";
import {StileBottone} from "../../stile/StileBottone.ts";

interface BottoneAttivazioneSensoriDiPosizioneProps {
    mostraSensori: boolean;
    setMostraSensori: React.Dispatch<React.SetStateAction<boolean>>;
}

const BottoneVisualizzazioneSensori: React.FC<BottoneAttivazioneSensoriDiPosizioneProps> = ({
                                                                                                mostraSensori,
                                                                                                setMostraSensori
                                                                                            }) => {

    const getVariant = (): string => {
        if (mostraSensori) {
            return StileBottone.Attivo;
        }
        return StileBottone.NonAttivoOk;
    }

    const getScritta = (): string => {
        return "Sensori";
    }

    const handleClick = () => {
        if (mostraSensori) {
            setMostraSensori(false);
        } else {
            setMostraSensori(true);
        }
    }

    return (
        <div className="bottoneSensori clickable">
            <Button variant={getVariant()} size="sm" onClick={handleClick}>
                {getScritta()}
            </Button>
        </div>
    )
}

export default BottoneVisualizzazioneSensori;