// mqttService.ts
import mqtt, {MqttClient} from 'mqtt';
import logger from "../../utility/Logger.ts";
import {Locomotiva} from "../../model/Locomotiva.ts";
import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import {Scambio} from "../../model/scambio/Scambio.ts";
import {StatoAutopilotBackEnd} from "../../enumerazioni/StatoAutopilotBackEnd.ts";
import {ModalitaAutopilot} from "../../enumerazioni/ModalitaAutopilot.ts";
import {
    mapJsonToConvoglio,
} from "../mappingJsonToModel/MapJsonToConvoglio.ts";
import {Sezione} from "../../model/Sezione.ts";
import {areIdSezioneTipoEqual} from "../../model/IdSezioneTipo.ts";
import {mapJsonToScambio} from "../mappingJsonToModel/MapJsonToScambio.ts";
import {mapJsonToLocomotiva} from "../mappingJsonToModel/MapJsonToLocomotiva.ts";
import {mapJsonToSezione} from "../mappingJsonToModel/MapJsonToSezione.ts";
import {mapJsonToInfoAutopilot} from "../mappingJsonToModel/MapJsonToInfoAutopilot.ts";
import {mapJsonToSensorePosizione} from "../mappingJsonToModel/MapJsonToSensorePosizione.ts";
import {SensorePosizione} from "../../model/SensorePosizione.ts";

type SetLocomotive = (locomotive: Locomotiva[] | ((prevLocomotive: Locomotiva[]) => Locomotiva[])) => void;
type SetConvogli = (convogli: Convoglio[] | ((prevConvogli: Convoglio[]) => Convoglio[])) => void;
type SetScambi = (scambi: Scambio[] | ((prevScambi: Scambio[]) => Scambio[])) => void;
type SetSensorePosizione = (sensorePosizione: SensorePosizione[] | ((prevSensorePosizione: SensorePosizione[]) => SensorePosizione[])) => void;
type SetSezioni = (sezioni: Sezione[] | ((prevSezioni: Sezione[]) => Sezione[])) => void;

const delayAggiornamentoSensorePosizioneDaTrueAFalse = 1000;

export const connettiBrokerMqtt = (
    setMqttConnectStatus: (status: string) => void,
    setMqttClient: (client: MqttClient | null) => void,
    setLocomotive: SetLocomotive,
    setConvogli: SetConvogli,
    setScambi: SetScambi,
    setSensorePosizione: SetSensorePosizione,
    setSezioni: SetSezioni,
    setStatoAutopilotBackEnd: React.Dispatch<React.SetStateAction<StatoAutopilotBackEnd | null>>,
    setModalitaAutopilotBackEnd: React.Dispatch<React.SetStateAction<ModalitaAutopilot | null>>,
    richiestaRestProgrammataRef: React.MutableRefObject<boolean>
) => {

    const mqttUrl = import.meta.env.VITE_URL_MQTT_BROKER;
    const mqttOptions = {
        username: import.meta.env.VITE_MQTT_USERNAME,
        password: import.meta.env.VITE_MQTT_PASSWORD,
        path: '/mqtt'
    };
    const topics = ['DccCommandStation/Locomotiva', 'DccCommandStation/Convoglio', 'DccCommandStation/Scambio', 'DccCommandStation/Sezione', 'DccCommandStation/SensorePosizione', 'DccCommandStation/InfoAutopilot'];

    setMqttConnectStatus('Connecting');

    logger.info('Connessione al broker MQTT con indirizzo ', mqttUrl);
    const mqttClient = mqtt.connect(mqttUrl, mqttOptions);

    mqttClient.on('connect', () => {
        setMqttConnectStatus('Connesso');
        logger.info('Connessione al broker MQTT... OK');
        setMqttClient(mqttClient);

        topics.forEach((topic) => {
            mqttClient.subscribe(topic, (err) => {
                if (!err) {
                    logger.info(`Sottoscrizione al topic ${topic} riuscita`);
                } else {
                    logger.error(`Errore nella sottoscrizione al topic ${topic}:`, err);
                }
            });
        });
    });

    mqttClient.stream.on('error', (err) => {
        logger.error('Errore connessione al broker MQTT.', err);
        setMqttConnectStatus('Error');
        // Se non voglio continuare i tentativi di connessione posso chiudere il client
        // mqttClient.end()
    });

    mqttClient.on('reconnect', () => {
        setMqttConnectStatus('Reconnecting');
    });

    mqttClient.on('message', (topic, message) => {
        logger.info(`Messaggio MQTT ricevuto: ${message.toString()} from topic: ${topic}`);

        if (topic === 'DccCommandStation/Locomotiva') {
            const locomotivaAggiornata = mapJsonToLocomotiva(JSON.parse(message.toString()));

            setLocomotive((prevLocomotive: Locomotiva[]) => {
                const nuoveLocomotive = [...prevLocomotive];
                nuoveLocomotive[locomotivaAggiornata.id] = locomotivaAggiornata;
                return nuoveLocomotive;
            });
        } else if (topic === 'DccCommandStation/Convoglio') {
            // Se è stata programmata una richiesta REST, devo ignorare il parametro velocitaImpostata dell'oggetto convoglio che ricevo su MQTT.
            // Questo per evitare problemi con lo slider che dipende da velocitàImpostata.
            // Per capire meglio prendiamo un esempio: l'utente ha spostato lo slider della velocità del convoglio da 0 a 100: viene inviata una richiesta REST quando lo slider ha assunto il valore a 1.
            // Ciò significa che appena Arduino riceve la richiesta REST, la velocità del convoglio viene impostata a 1 e invia un messaggio MQTT con velocità impostata a 1.
            // Il Front-End riceve il messaggio MQTT e aggiorna lo slider a 1, ma nel frattempo l'utente ha spostato lo slider a 100.
            if (richiestaRestProgrammataRef.current) {
                const data = JSON.parse(message.toString());
                const convoglioAggiornato = mapJsonToConvoglio(data);

                setConvogli((prevConvogli: Convoglio[]) => {
                    return prevConvogli.map(convoglio =>
                        convoglio.id === convoglioAggiornato.id
                            ? {...convoglioAggiornato, velocitaImpostata: convoglio.velocitaImpostata}
                            : convoglio
                    );
                });
                return;
            } else {
                // Se non è stata programmata una richiesta REST, posso considerare tutto l'oggetto Convoglio come buono.
                const data = JSON.parse(message.toString());
                const convoglioAggiornato = mapJsonToConvoglio(data);

                setConvogli((prevConvogli: Convoglio[]) => {
                    return prevConvogli.map(convoglio =>
                        convoglio.id === convoglioAggiornato.id ? convoglioAggiornato : convoglio
                    );
                });
            }
        } else if (topic === 'DccCommandStation/Scambio') {
            const scambioAggiornato = mapJsonToScambio(JSON.parse(message.toString()));

            setScambi((prevScambi: Scambio[]) => {
                const nuoviScambi = [...prevScambi];
                nuoviScambi[scambioAggiornato.id] = scambioAggiornato;
                return nuoviScambi;
            });
        } else if (topic === 'DccCommandStation/Sezione') {
            const sezioneAggiornata = mapJsonToSezione(JSON.parse(message.toString()));

            // Se la sezione è già presente nell'array delle sezioni, la aggiorno, altrimenti la aggiungo.
            setSezioni((prevSezioni: Sezione[]) => {
                return prevSezioni.map(sezione =>
                    areIdSezioneTipoEqual(sezione.id, sezioneAggiornata.id) ? sezioneAggiornata : sezione
                );
            });
        } else if (topic === 'DccCommandStation/SensorePosizione') {
            const sensorePosizioneAggiornato = mapJsonToSensorePosizione(JSON.parse(message.toString()));

            setSensorePosizione((prevSensorePosizione) => {
                return prevSensorePosizione.map(sensore => {
                    if (sensore.id === sensorePosizioneAggiornato.id) {
                        if (sensore.stato && !sensorePosizioneAggiornato.stato) {
                            // Lo stato cambia da true a false, aspetto 2 seocndi prima di aggiornare.
                            setTimeout(() => {
                                setSensorePosizione((prevSensorePosizione) => {
                                    return prevSensorePosizione.map(s =>
                                        s.id === sensorePosizioneAggiornato.id ? sensorePosizioneAggiornato : s
                                    );
                                });
                            }, delayAggiornamentoSensorePosizioneDaTrueAFalse);
                            return sensore; // Return the current state until the timeout completes
                        }
                        return sensorePosizioneAggiornato; // Update immediately for other cases
                    }
                    return sensore;
                });
            });
        } else if (topic === 'DccCommandStation/InfoAutopilot') {
            const infoAutopilot = mapJsonToInfoAutopilot(JSON.parse(message.toString()));

            setStatoAutopilotBackEnd(infoAutopilot.statoAutopilot);
            setModalitaAutopilotBackEnd(infoAutopilot.modalitaAutopilot);
        }
    });
};