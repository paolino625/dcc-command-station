import {Sezione} from "../../model/Sezione.ts";

export const mapJsonToSezione = (data: any): Sezione => {
    return {
        id: data.id,
        stato: {
            statoLock: data.stato.statoLock,
            idConvoglio: data.stato.idConvoglio
        }
    };
}