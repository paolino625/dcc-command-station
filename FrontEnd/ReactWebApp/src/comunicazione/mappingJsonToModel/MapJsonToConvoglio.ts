import {Convoglio} from "../../model/convoglio/Convoglio.ts";
import {ValiditaPosizioneConvoglio} from "../../enumerazioni/ValiditaPosizioneConvoglio.ts";
import {StatoInizializzazioneAutopilotConvoglio} from "../../enumerazioni/StatoInizializzazioneAutopilotConvoglio.ts";
import {StatoAutopilotModalitaStazioneSuccessiva} from "../../enumerazioni/StatoAutopilotModalitaStazioneSuccessiva.ts";
import {StatoAutopilotModalitaApproccioCasuale} from "../../enumerazioni/StatoAutopilotModalitaApproccioCasuale.ts";

export const mapValiditaPosizioneConvoglio = (str: string): ValiditaPosizioneConvoglio => {
    switch (str) {
        case "POSIZIONE_VALIDA":
            return ValiditaPosizioneConvoglio.POSIZIONE_VALIDA;
        case "POSIZIONE_NON_VALIDA":
            return ValiditaPosizioneConvoglio.POSIZIONE_NON_VALIDA;
        default:
            throw new Error(`Errore conversione per l'enumerazione: ${str}`);
    }
};

export const mapStatoInizializzazioneAutopilotConvoglio = (str: string): StatoInizializzazioneAutopilotConvoglio => {
    switch (str) {
        case "PRONTO_INIZIALIZZAZIONE_CONVOGLIO":
            return StatoInizializzazioneAutopilotConvoglio.PRONTO_INIZIALIZZAZIONE_CONVOGLIO;
        case "PRONTO_PER_CONFIGURAZIONE_AUTOPILOT":
            return StatoInizializzazioneAutopilotConvoglio.PRONTO_PER_CONFIGURAZIONE_AUTOPILOT;
        case "CONFIGURAZIONE_AUTOPILOT_COMPLETATA":
            return StatoInizializzazioneAutopilotConvoglio.CONFIGURAZIONE_AUTOPILOT_COMPLETATA;
        default:
            throw new Error(`Errore conversione per l'enumerazione: ${str}`);
    }
};

export const mapStatoAutopilotModalitaStazioneSuccessiva = (str: string): StatoAutopilotModalitaStazioneSuccessiva => {
    switch (str) {
        case "FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA":
            return StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA;
        case "FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA":
            return StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA;
        case "FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO":
            return StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO;
        case "FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE":
            return StatoAutopilotModalitaStazioneSuccessiva.FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE;
        case "IN_PARTENZA_STAZIONE":
            return StatoAutopilotModalitaStazioneSuccessiva.IN_PARTENZA_STAZIONE;
        case "IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE":
            return StatoAutopilotModalitaStazioneSuccessiva.IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE;
        case "IN_MOVIMENTO":
            return StatoAutopilotModalitaStazioneSuccessiva.IN_MOVIMENTO;
        case "FERMO_SEZIONE_ATTESA_LOCK_SEZIONI":
            return StatoAutopilotModalitaStazioneSuccessiva.FERMO_SEZIONE_ATTESA_LOCK_SEZIONI;
        case "ARRIVATO_STAZIONE_DESTINAZIONE":
            return StatoAutopilotModalitaStazioneSuccessiva.ARRIVATO_STAZIONE_DESTINAZIONE;
        default:
            throw new Error(`Errore conversione per l'enumerazione: ${str}`);
    }
};

export const mapStatoAutopilotModalitaApproccioCasuale = (str: string): StatoAutopilotModalitaApproccioCasuale => {
    switch (str) {
        case "FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA":
            return StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_ATTESA_MOMENTO_PARTENZA;
        case "SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK":
            return StatoAutopilotModalitaApproccioCasuale.SCELTA_BINARIO_STAZIONE_SUCCESSIVA_E_LOCK;
        case "FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA":
            return StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_SALVATAGGIO_PERCORSO_DA_STAZIONE_CORRENTE_A_STAZIONE_SUCCESSIVA;
        case "FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO":
            return StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_CALCOLO_DIREZIONE_CONVOGLIO;
        case "FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE":
            return StatoAutopilotModalitaApproccioCasuale.FERMO_STAZIONE_ATTESA_LOCK_SEZIONI_FINO_A_PROSSIMA_SEZIONE_CAPIENTE;
        case "IN_PARTENZA_STAZIONE":
            return StatoAutopilotModalitaApproccioCasuale.IN_PARTENZA_STAZIONE;
        case "IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE":
            return StatoAutopilotModalitaApproccioCasuale.IN_MOVIMENTO_IN_ATTESA_PRIMO_SENSORE;
        case "IN_MOVIMENTO":
            return StatoAutopilotModalitaApproccioCasuale.IN_MOVIMENTO;
        case "FERMO_SEZIONE_ATTESA_LOCK_SEZIONI":
            return StatoAutopilotModalitaApproccioCasuale.FERMO_SEZIONE_ATTESA_LOCK_SEZIONI;
        case "ARRIVATO_STAZIONE_DESTINAZIONE":
            return StatoAutopilotModalitaApproccioCasuale.ARRIVATO_STAZIONE_DESTINAZIONE;
        default:
            throw new Error(`Errore conversione per l'enumerazione: ${str}`);
    }
};


export const mapJsonToConvoglio = (data: any): Convoglio => {
    const autopilot = {
        posizioneConvoglio: {
            validita: mapValiditaPosizioneConvoglio(data.autopilot.posizioneConvoglio.validita),
            idSezioneCorrenteOccupata: {
                tipologia: data.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata.tipologia,
                id: data.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata.id,
                specifica: data.autopilot.posizioneConvoglio.idSezioneCorrenteOccupata.specifica
            },
            direzionePrimaLocomotivaDestra: data.autopilot.posizioneConvoglio.direzionePrimaLocomotivaDestra
        },
        statoInizializzazioneAutopilotConvoglio: mapStatoInizializzazioneAutopilotConvoglio(data.autopilot.statoInizializzazioneAutopilotConvoglio),
        statoAutopilotModalitaStazioneSuccessiva: mapStatoAutopilotModalitaStazioneSuccessiva(data.autopilot.statoAutopilotModalitaStazioneSuccessiva),
        statoAutopilotModalitaApproccioCasuale: mapStatoAutopilotModalitaApproccioCasuale(data.autopilot.statoAutopilotModalitaApproccioCasuale)
    };

    return {
        id: data.id,
        nome: data.nome,
        idLocomotiva1: data.idLocomotiva1,
        idLocomotiva2: data.idLocomotiva2,
        velocitaMassima: data.velocitaMassima,
        velocitaImpostata: data.velocitaImpostata,
        velocitaAttuale: data.velocitaAttuale,
        luciAccese: data.luciAccese,
        isPresenteSulTracciato: data.isPresenteSulTracciato,
        autopilot: autopilot
    };
}