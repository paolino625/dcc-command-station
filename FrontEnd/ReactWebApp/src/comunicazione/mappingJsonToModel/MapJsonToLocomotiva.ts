import {Locomotiva} from "../../model/Locomotiva.ts";

export const mapJsonToLocomotiva = (data: any): Locomotiva => {
    return {
        id: data.id,
        nome: data.nome,
        codice: data.codice
    };
};