import {SensorePosizione} from "../../model/SensorePosizione.ts";

export const mapJsonToSensorePosizione = (data: any): SensorePosizione => {
    return {
        id: data.id,
        stato: data.stato,
        idConvoglioInAttesa: data.idConvoglioInAttesa,
    };
}