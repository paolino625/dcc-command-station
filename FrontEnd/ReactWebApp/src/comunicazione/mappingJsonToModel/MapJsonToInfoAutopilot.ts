import {ModalitaAutopilot} from "../../enumerazioni/ModalitaAutopilot.ts";
import {StatoAutopilotBackEnd} from "../../enumerazioni/StatoAutopilotBackEnd.ts";
import {InfoAutopilot} from "../../model/InfoAutopilot.ts";

export function mapJsonToInfoAutopilot(data: any): InfoAutopilot {
    let statoAutopilot: StatoAutopilotBackEnd;
    let modalitaAutopilot: ModalitaAutopilot;

    switch (data.statoAutopilot) {
        case 'NON_ATTIVO':
            statoAutopilot = StatoAutopilotBackEnd.NON_ATTIVO;
            break;
        case 'TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO':
            statoAutopilot = StatoAutopilotBackEnd.TRANSIZIONE_DA_ATTIVO_A_NON_ATTIVO;
            break;
        case 'ATTIVO':
            statoAutopilot = StatoAutopilotBackEnd.ATTIVO;
            break;
        default:
            statoAutopilot = StatoAutopilotBackEnd.ERRORE;
            break;
    }

    switch (data.modalitaAutopilot) {
        case 'STAZIONE_SUCCESSIVA':
            modalitaAutopilot = ModalitaAutopilot.STAZIONE_SUCCESSIVA;
            break;
        case 'APPROCCIO_CASUALE_TUTTI_CONVOGLI':
            modalitaAutopilot = ModalitaAutopilot.APPROCCIO_CASUALE_TUTTI_CONVOGLI;
            break;
        case 'APPROCCIO_CASUALE_SCELTA_CONVOGLI':
            modalitaAutopilot = ModalitaAutopilot.APPROCCIO_CASUALE_CON_SCELTA_CONVOGLI;
            break;
        default:
            modalitaAutopilot = ModalitaAutopilot.ERRORE;
            break;
    }

    return {statoAutopilot, modalitaAutopilot};
}