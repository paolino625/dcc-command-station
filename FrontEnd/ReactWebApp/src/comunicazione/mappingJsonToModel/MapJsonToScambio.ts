import {Scambio} from "../../model/scambio/Scambio.ts";
import {PosizioneScambio} from "../../model/scambio/PosizioneScambio.ts";

export const mapJsonToScambio = (data: any): Scambio => {
    const posizioneMap: { [key: string]: PosizioneScambio } = {
        "DESTRA": PosizioneScambio.DESTRA,
        "SINISTRA": PosizioneScambio.SINISTRA,
        "POSIZIONE_NON_CONOSCIUTA": PosizioneScambio.POSIZIONE_NON_CONOSCIUTA
    };

    return {
        id: data.id,
        posizione: posizioneMap[data.posizione]
    };
};