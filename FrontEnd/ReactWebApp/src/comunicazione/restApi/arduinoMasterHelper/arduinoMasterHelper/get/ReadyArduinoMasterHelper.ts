import logger from "../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../../arduinoProxy/authorization/AuthorizationToken.ts";

export const readyArduinoMasterHelper = async () => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/utility/ready`;
    try {
        if (import.meta.env.VITE_LOG_ATTIVI_ARDUINO_MASTER_HELPER_READY === 'true') {
            logger.info(`Chiamata GET: ${url}`, "Body vuoto");
        }

        const inviaRichiestaGet = async () => {
            return await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                }
            });
        };

        const response = await inviaRichiestaGet();

        if (response.ok) {
            const responseBody = await response.json();
            if (import.meta.env.VITE_LOG_ATTIVI_ARDUINO_MASTER_HELPER_READY === 'true') {
                logger.info(`La chiamata GET ${url} ha avuto successo.`, responseBody);
            }
            // Ritorno lo stato della variabile ready presente nel body
            return responseBody.ready;

        } else {
            logger.error(`La chiamata GET ${url} non è andata a buon fine.`, response.status);
            return false;
        }
    } catch (error) {
        logger.error(`Errore nella chiamata GET ${url}: `, error);
        return false;
    }
}