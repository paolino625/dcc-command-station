export const getAuthorizationHeader = () => {
    return {
        'Authorization': import.meta.env.VITE_AUTHORIZATION_TOKEN_REST_API_ARDUINO
    };
};