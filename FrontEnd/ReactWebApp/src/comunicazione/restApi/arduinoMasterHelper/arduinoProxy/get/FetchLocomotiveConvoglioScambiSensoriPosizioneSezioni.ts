import logger from "../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../authorization/AuthorizationToken.ts";
import {Convoglio} from "../../../../../model/convoglio/Convoglio.ts";
import {Locomotiva} from "../../../../../model/Locomotiva.ts";
import {Scambio} from "../../../../../model/scambio/Scambio.ts";
import {mapJsonToConvoglio} from "../../../../mappingJsonToModel/MapJsonToConvoglio.ts";
import {mapJsonToLocomotiva} from "../../../../mappingJsonToModel/MapJsonToLocomotiva.ts";
import {mapJsonToScambio} from "../../../../mappingJsonToModel/MapJsonToScambio.ts";
import {Sezione} from "../../../../../model/Sezione.ts";
import {mapJsonToSezione} from "../../../../mappingJsonToModel/MapJsonToSezione.ts";
import {mapJsonToSensorePosizione} from "../../../../mappingJsonToModel/MapJsonToSensorePosizione.ts";

export const fetchLocomotiveConvoglioScambiSensoriPosizioneSezioni = async (
    setLocomotive: React.Dispatch<React.SetStateAction<Locomotiva[]>>,
    setConvogli: React.Dispatch<React.SetStateAction<Convoglio[]>>,
    setScambi: React.Dispatch<React.SetStateAction<Scambio[]>>,
    setSezioni: React.Dispatch<React.SetStateAction<Sezione[]>>,
    setSensoriPosizione: React.Dispatch<React.SetStateAction<any[]>>,
    setError: React.Dispatch<React.SetStateAction<string | null>>,
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
) => {
    logger.info("Effettuo il fetch dei dati da Arduino");
    // Fetch dei dati da Arduino solo se la connessione MQTT è attiva.
    // Se si toglie questo IF appena si apre la pagina vengono fatti 3 fetch inutili
    const fetchDatiInContemporanea = async () => {
        try {
            await effettuoChiamataGetArduino(`${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/locomotive`, setLocomotive, mapJsonToLocomotiva, setError);
            await effettuoChiamataGetArduino(`${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/convogli`, setConvogli, mapJsonToConvoglio, setError);
            await effettuoChiamataGetArduino(`${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/scambi`, setScambi, mapJsonToScambio, setError);
            await effettuoChiamataGetArduino(`${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/autopilot/sensoriPosizione`, setSensoriPosizione, mapJsonToSensorePosizione, setError);
            await effettuoChiamataGetArduino(`${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/autopilot/sezioni`, setSezioni, mapJsonToSezione, setError);
            setIsLoading(false);
        } catch (error) {
            setIsLoading(false);
        }
    }
    await fetchDatiInContemporanea();
};
const effettuoChiamataGetArduino = async (url: string, setState: React.Dispatch<React.SetStateAction<any[]>>, mapFunction: (data: any) => any, setError: React.Dispatch<React.SetStateAction<string | null>>) => {
    try {
        logger.info(`Chiamata GET: ${url}`);

        // Mi assicuro che se la chiamata GET non riceve risposta entro un certo intervallo di tempo, venga abortita, in modo tale che venga scatenato un errore.
        const controller = new AbortController();
        const timeoutId = setTimeout(() => controller.abort(), import.meta.env.VITE_TIMEOUT_CHIAMATA_GET_MILLISECONDI);

        const response = await fetch(url, {
            method: 'GET',
            headers: {'Content-Type': 'application/json', ...getAuthorizationHeader()},
            signal: controller.signal

        });

        clearTimeout(timeoutId);

        if (response.ok) {

            const responseBody = await response.json();
            logger.info(`La chiamata GET ${url} ha avuto successo.`, responseBody);

            // Calcolo il numero di elementi nella response
            const numeroElementiResponse = responseBody.length;

            // Creo l'array
            const mappedArray = new Array(numeroElementiResponse).fill(null);

            // Viene iterato ogni elemento del response body, per ogni elemento viene applicata una funzione di mappatura per trasformare l'elemento.
            // L'elemento viene poi posizionato nel mappedArray all'indice corrispondente al suo ID.
            responseBody.forEach((item: any, index: number) => {
                const mappedItem = mapFunction(item);
                mappedArray[index] = mappedItem;
            });

            setState(mappedArray);
        } else {
            const errorMessage = `La chiamata GET ${url} non è andata a buon fine: ${response.status}`;
            logger.error(errorMessage);
            setError(errorMessage);
        }
    } catch (error) {
        const errorMessage = `Chiamata GET ${url} non è andata a buon fine: ${error}`;
        logger.error(errorMessage);
        setError(errorMessage);
    }
};