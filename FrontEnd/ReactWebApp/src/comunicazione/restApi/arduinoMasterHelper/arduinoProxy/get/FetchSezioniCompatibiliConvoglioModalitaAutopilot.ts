import logger from "../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../authorization/AuthorizationToken.ts";

3
export const fetchSezioniCompatibiliConvoglioModalitaAutopilotRestCall = async (idConvoglio: number): Promise<any> => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/convoglio/${idConvoglio}/sezioniStazioneCompatibiliModalitaAutopilot`;
    try {
        logger.info(`Chiamata GET: ${url}`, "Body vuoto");

        const inviaRichiestaGet = async () => {
            return await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                }
            });
        };

        const response = await inviaRichiestaGet();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata GET ${url} ha avuto successo.`, responseBody);

            return responseBody;

        } else {
            logger.error(`La chiamata GET ${url} non è andata a buon fine.`, response.status);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata GET ${url}: `, error);
    }
}