import logger from "../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../authorization/AuthorizationToken.ts";
import {StatoAutopilotBackEnd} from "../../../../../enumerazioni/StatoAutopilotBackEnd.ts";
import {ModalitaAutopilot} from "../../../../../enumerazioni/ModalitaAutopilot.ts";
import {mapJsonToInfoAutopilot} from "../../../../mappingJsonToModel/MapJsonToInfoAutopilot.ts";

export const fetchInfoAutopilotRestCall = async (
    setStatoAutopilotBackEnd: React.Dispatch<React.SetStateAction<StatoAutopilotBackEnd | null>>,
    setModalitaAutopilotBackEnd: React.Dispatch<React.SetStateAction<ModalitaAutopilot | null>>
) => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/autopilot/info`;
    try {
        logger.info(`Chiamata GET: ${url}`, "Body vuoto");

        const inviaRichiestaGet = async () => {
            return await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                }
            });
        };

        const response = await inviaRichiestaGet();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata GET ${url} ha avuto successo.`, responseBody);

            const convertedInfoAutopilot = mapJsonToInfoAutopilot(responseBody);

            setStatoAutopilotBackEnd(convertedInfoAutopilot.statoAutopilot);
            setModalitaAutopilotBackEnd(convertedInfoAutopilot.modalitaAutopilot);

        } else {
            logger.error(`La chiamata GET ${url} non è andata a buon fine.`, response.status);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata GET ${url}: `, error);
    }
}