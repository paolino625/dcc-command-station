import logger from "../../../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../../../authorization/AuthorizationToken.ts";
import {IdSezioneTipo} from "../../../../../../../model/IdSezioneTipo.ts";

export const avvioAutopilotModalitaStazioneSuccessivaRestCall = async (idConvoglio: number, idSezioneStazioneArrivo: IdSezioneTipo) => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/arduinoProxy/autopilotConvoglio/inizializzaAutopilotConvoglioModalitaStazioneSuccessiva`;
    try {
        logger.info(`Chiamata POST: ${url}`, "Body vuoto");

        const inviaRichiestaPost = async () => {
            return await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                },
                body: JSON.stringify({
                    idConvoglio,
                    idSezioneStazioneArrivo,
                    // Per il momento impostiamo questo valore sempre a 0, ovvero non c'è ritardo nella partenza
                    delayAttesaPartenzaStazioneLocale: 0
                })
            });
        };

        const response = await inviaRichiestaPost();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata POST ${url} ha avuto successo.`, responseBody);
        } else {
            throw new Error(`La chiamata POST ${url} non è andata a buon fine. Status: ${response.status}`);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata POST ${url}: `, error);
    }
}

