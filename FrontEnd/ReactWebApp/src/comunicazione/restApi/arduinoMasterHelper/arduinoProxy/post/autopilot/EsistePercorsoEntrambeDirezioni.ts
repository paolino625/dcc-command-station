import logger from "../../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../../authorization/AuthorizationToken.ts";
import {IdSezioneTipo} from "../../../../../../model/IdSezioneTipo.ts";

export const esistePercorsoEntrambeDirezioniRestCall = async (sezione1: IdSezioneTipo, sezione2: IdSezioneTipo): Promise<any> => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/percorso/esiste/entrambeDirezioni`;
    try {
        logger.info(`Chiamata POST: ${url}`, "Body vuoto");

        const inviaRichiestaPost = async () => {
            return await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                },
                body: JSON.stringify({
                    idSezionePartenza: sezione1,
                    idSezioneArrivo: sezione2
                })
            });
        };

        const response = await inviaRichiestaPost();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata POST ${url} ha avuto successo.`, responseBody);
            // Restituiamo il valore booleano che ci dice se il percorso esiste o meno
            return responseBody;
        } else {
            throw new Error(`La chiamata POST ${url} non è andata a buon fine. Status: ${response.status}`);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata POST ${url}: `, error);
    }

    // Assicuriamoci di restituire sempre un valore booleano
    return false;
}

export const esistePercorsoEntrambeDirezioni = async (sezione1: IdSezioneTipo, sezione2: IdSezioneTipo): Promise<boolean> => {
    const responseBody = await esistePercorsoEntrambeDirezioniRestCall(sezione1, sezione2);
    // Prendo la risposta della chiamata REST e restituisco il valore booleano che mi dice se il percorso esiste o meno
    return responseBody.percorsoEsiste;
}