import logger from "../../../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../../../authorization/AuthorizationToken.ts";

export const avvioAutopilotModalitaApproccioCasualeSceltaConvogliRestCall = async (ottimizzazioneAssegnazioneConvogliAiBinari: boolean, autopilotAttivatoAutopilotModalitaApproccioCasuale: {
    [idConvoglio: number]: boolean
}) => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/arduinoProxy/autopilot/avvia/modalitaAutopilotApproccioCasualeSceltaConvogli`;

    try {

        // Creo un array con gli ID dei convogli selezionati
        const idConvogliSelezionati: number[] = Object.keys(autopilotAttivatoAutopilotModalitaApproccioCasuale)
            .filter(key => autopilotAttivatoAutopilotModalitaApproccioCasuale[Number(key)])
            .map(Number);

        const requestBody = JSON.stringify({
            ottimizzazioneAssegnazioneConvogliAiBinari: ottimizzazioneAssegnazioneConvogliAiBinari,
            arrayIdConvogli: idConvogliSelezionati
        })

        logger.info(`Chiamata POST: ${url}`, "Request Body:", JSON.parse(requestBody));

        const inviaRichiestaPost = async () => {

            return await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                },
                body: requestBody
            });
        };

        const response = await inviaRichiestaPost();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata POST ${url} ha avuto successo.`, responseBody);
        } else {
            throw new Error(`La chiamata POST ${url} non è andata a buon fine. Status: ${response.status}`);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata POST ${url}: `, error);
    }
}