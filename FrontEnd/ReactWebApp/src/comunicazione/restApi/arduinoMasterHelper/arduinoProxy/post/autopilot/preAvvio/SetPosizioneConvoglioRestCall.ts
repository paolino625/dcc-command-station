import logger from "../../../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../../../authorization/AuthorizationToken.ts";
import {IdSezioneTipo} from "../../../../../../../model/IdSezioneTipo.ts";

export const setPosizioneConvoglioRestCall = async (
    idConvoglio: number,
    idSezioneCorrenteOccupata: IdSezioneTipo,
    direzionePrimaLocomotivaDestra: boolean
) => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/arduinoProxy/autopilot/setPosizioneConvoglio`;
    try {
        const requestBody = JSON.stringify({
            idConvoglio,
            idSezioneCorrenteOccupata,
            direzionePrimaLocomotivaDestra
        });

        logger.info(`Chiamata POST: ${url}`, "Request Body:", JSON.parse(requestBody));

        const inviaRichiestaPOST = async () => {
            return await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                },
                body: requestBody,
            });
        };

        const response = await inviaRichiestaPOST();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata POST ${url} ha avuto successo.`, responseBody);
        } else {
            logger.error(`La chiamata POST ${url} non è andata a buon fine.`, response.status);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata POST ${url}: `, error);
    }
}