import logger from "../../../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../../../authorization/AuthorizationToken.ts";

export const avvioAutopilotModalitaStazioneSuccessivaRestCall = async () => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/arduinoProxy/autopilot/avvia/modalitaAutopilotStazioneSuccessiva`;
    try {
        logger.info(`Chiamata POST: ${url}`, "Body vuoto");

        const inviaRichiestaPost = async () => {
            return await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                }
            });
        };

        const response = await inviaRichiestaPost();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata POST ${url} ha avuto successo.`, responseBody);
        } else {
            throw new Error(`La chiamata POST ${url} non è andata a buon fine. Status: ${response.status}`);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata POST ${url}: `, error);
    }
}