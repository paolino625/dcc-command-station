import logger from "../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../authorization/AuthorizationToken.ts";

export const stopEmergenzaConvogli = async () => {
    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/arduinoProxy/stop`;
    try {
        logger.info(`Chiamata PUT: ${url}`, "Body vuoto");

        const inviaRichiestaPut = async () => {
            return await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                }
            });
        };

        const response = await inviaRichiestaPut();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata PUT ${url} ha avuto successo.`, responseBody);
        } else {
            logger.error(`La chiamata PUT ${url} non è andata a buon fine.`, response.status);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata POST ${url}: `, error);
    }
}