import logger from "../../../../../utility/Logger.ts";
import {getAuthorizationHeader} from "../authorization/AuthorizationToken.ts";

interface UpdateParams {
    // Mappa generica in modo tale da poter aggiornare una qualsiasi entità
    [key: string]: any;
}


export const invioEntitaAggiornataArduino = async (entityType: string, entityId: number, params: UpdateParams) => {

    const url = `${import.meta.env.VITE_URL_ARDUINO_MASTER_HELPER}/arduinoProxy/${entityType}/${entityId}`;
    try {
        const requestBody = JSON.stringify(params);

        logger.info(`Chiamata PUT: ${url}`, "Request Body:", JSON.parse(requestBody));

        const inviaRichiestaPut = async () => {
            const response = await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    ...getAuthorizationHeader(),
                },
                body: requestBody,
            });
            return response;
        };

        const response = await inviaRichiestaPut();

        if (response.ok) {
            const responseBody = await response.json();
            logger.info(`La chiamata PUT ${url} ha avuto successo.`, responseBody);
        } else {
            logger.error(`La chiamata PUT ${url} non è andata a buon fine.`, response.status);
        }
    } catch (error) {
        logger.error(`Errore nella chiamata PUT ${url}: `, error);
    }
};