export const rimuovoTuttiEventListeners = (element: Element) => {
    const clone = element.cloneNode(true);
    element.parentNode?.replaceChild(clone, element);
};