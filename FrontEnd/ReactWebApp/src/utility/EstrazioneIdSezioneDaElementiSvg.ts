import {IdSezioneTipo} from "../model/IdSezioneTipo.ts";

export const estraggoIdSezioneTipoDaStringa = (str: string): IdSezioneTipo | null => {
    // Controllo prima se l'ID è in formato completo (ovvero 3 numeri)
    let regex = /(?:.*-)?(\d+)\.(\d+)\.(\d+)/;
    let match = str.match(regex);

    if (match) {
        const [_, tipologia, id, specifica] = match;
        return {
            tipologia: parseInt(tipologia, 10),
            id: parseInt(id, 10),
            specifica: parseInt(specifica, 10),
        };
    } else {
        // Se non è in formato completo, controllo se è in formato semplificato (2 numeri)
        regex = /(?:.*-)?(\d+)\.(\d+)/;
        match = str.match(regex);
        if (match) {
            const [_, tipologia, id] = match;
            return {
                tipologia: parseInt(tipologia, 10),
                id: parseInt(id, 10),
                specifica: 0,
            };
        }
    }

    return null;
};