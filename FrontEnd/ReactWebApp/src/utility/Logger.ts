import pino from "pino";

export enum LogLevel {
    DEBUG,
    INFO,
    WARN,
    ERROR,
    CRITICAL,
}

const logger = pino({
    level: import.meta.env.VITE_LIVELLO_LOGGER,
    transport: {
        target: 'pino-pretty', // Use pino-pretty for better console output
        options: {
            colorize: true, // Optional: colorize the output
        },
    },
});

/*
const loggerWrapper = (level: LogLevel, message: string) => {
    // Call the logger
    switch (level) {
        case LogLevel.DEBUG:
            logger.debug(message);
            break;
        case LogLevel.INFO:
            logger.info(message);
            break;
        case LogLevel.WARN:
            logger.warn(message);

            break;
        case LogLevel.ERROR:
            logger.error(message);
            break;
        case LogLevel.CRITICAL:
            logger.error(message);

            break;
    }
};
*/

export default logger;