import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    build: {outDir: 'build',},
    server: {
        // Consente a tutti gli host di accedere al server di sviluppo Vite
        // Senza questa opzione, incorrerei in un errore CORS
        allowedHosts: [
            'web-app.svil.dcc-command-station.docker.vm.proxmox.torino.local-server.paolocalcagni.com',
            'web-app.prod.dcc-command-station.docker.vm.proxmox.torino.local-server.paolocalcagni.com',
            'localhost'
        ]
    }
})
